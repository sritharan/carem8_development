//
//  ViewController.swift
//  VideoDownloadTest
//
//  Created by Proitzen iOS Dev on 2/4/19.
//  Copyright © 2019 Proitzen iOS Dev. All rights reserved.
//

import UIKit
import MBProgressHUD

class PhotoDownloadViewController: UIViewController,URLSessionDownloadDelegate {
    
//    @IBOutlet weak var btnDownloadVideo: UIButton!
    var progress: Float = 0.0
    var task: URLSessionTask!
    
    var percentageWritten:Float = 0.0
    var taskTotalBytesWritten = 0
    var taskTotalBytesExpectedToWrite = 0
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    var filePath: String?
    
    lazy var session : URLSession = {
        let config = URLSessionConfiguration.default
        // config.allowsCellularAccess = false
        let session = URLSession(configuration: config, delegate: self as! URLSessionDelegate, delegateQueue: OperationQueue.main)
        return session
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        filePath = delegate.filePath
        downloadPhoto()
    }
    
    @IBAction func btnDownloadTapped(_ sender: Any) {
        downloadPhoto()
    }
    
    func downloadPhoto(){
        let hud =  MBProgressHUD.showAdded(to: self.view, animated: true)
        // Set the bar determinate mode to show task progress.
        progress = 0.0
        hud.mode = MBProgressHUDMode.determinateHorizontalBar
        hud.isUserInteractionEnabled = true;
        hud.labelText = NSLocalizedString("Downloading...", comment: "HUD loading title")
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            // Do something useful in the background and update the HUD periodically.
            self.doSomeWorkWithProgress()
            DispatchQueue.main.async(execute: {() -> Void in
                //hud?.hide(true)
                hud.labelText = NSLocalizedString("Just Wait...", comment: "HUD loading title")
            })
        })
        
        
        print(filePath)
        //let urlData = NSData(contentsOf: NSURL(string:"\(getDataArray["video_url"] as! String)")! as URL)
        let url = NSURL(string:filePath!)!
        let req = NSMutableURLRequest(url:url as URL)
        let config = URLSessionConfiguration.default
        let task = self.session.downloadTask(with: req as URLRequest)
        self.task = task
        task.resume()
        
    }
    func showAlert() {
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getImage(fromSourceType sourceType: UIImagePickerControllerSourceType) {
        
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
        
    }
    
    //MARK:- share video
    func doSomeWorkWithProgress() {
        // This just increases the progress indicator in a loop.
        while progress < 1.0 {
            DispatchQueue.main.async(execute: {() -> Void in
                // Instead we could have also passed a reference to the HUD
                // to the HUD to myProgressTask as a method parameter.
                print(self.progress)
                MBProgressHUD(for: self.view)?.progress = self.progress
            })
            usleep(50000)
        }
    }
    
    //MARK:- URL Session delegat
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        print("downloaded \(100*totalBytesWritten/totalBytesExpectedToWrite)")
        taskTotalBytesWritten = Int(totalBytesWritten)
        taskTotalBytesExpectedToWrite = Int(totalBytesExpectedToWrite)
        percentageWritten = Float(taskTotalBytesWritten) / Float(taskTotalBytesExpectedToWrite)
        print(percentageWritten)
        //  let x = Double(percentageWritten).rounded(toPlaces: 2)
        let x = String(format:"%.2f", percentageWritten)
        print(x)
        self.progress = Float(x)!
        print(progress)
        
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64) {
        // unused in this example
    }
    
    
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        print("completed: error: \(error)")
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("Finished downloading!")
        let fileManager = FileManager()
        // this can be a class variable
        let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        print(directoryURL)
        let docDirectoryURL = NSURL(fileURLWithPath: "\(directoryURL)")
        print(docDirectoryURL)
        // Get the original file name from the original request.
        //print(lastPathComponent)
        let destinationFilename = downloadTask.originalRequest?.url?.lastPathComponent
        print(destinationFilename!)
        // append that to your base directory
        let destinationURL =  docDirectoryURL.appendingPathComponent("\(destinationFilename!)")
        print(destinationURL!)
        /* check if the file exists, if so remove it. */
        if let path = destinationURL?.path {
            if fileManager.fileExists(atPath: path) {
                do {
                    try fileManager.removeItem(at: destinationURL!)
                    
                } catch let error as NSError {
                    print(error.debugDescription)
                }
                
            }
        }
        
        do
        {
            try fileManager.copyItem(at: location, to: destinationURL!)
        }
        catch {
            print("Error while copy file")
            
        }
        DispatchQueue.main.async(execute: {() -> Void in
            MBProgressHUD.hide(for: self.view, animated: true)
        })
        // let videoLink = NSURL(fileURLWithPath: filePath)
        let objectsToShare = [destinationURL!] //comment!, imageData!, myWebsite!]
        
        let imageUi = getImageFromDir(destinationFilename!);
        
        UIImageWriteToSavedPhotosAlbum(imageUi!, self, nil, nil)
        dismiss(animated: false, completion: nil)
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func getImageFromDir(_ imageName: String) -> UIImage? {
        
        if let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = documentsUrl.appendingPathComponent(imageName)
            do {
                let imageData = try Data(contentsOf: fileURL)
                return UIImage(data: imageData)
            } catch {
                print("Not able to load image")
            }
        }
        return nil
    }
}

