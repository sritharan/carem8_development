//
//  ViewController.swift
//  add_to_calendar
//
//  Created by admin on 3/5/19.
//  Copyright © 2019 admin. All rights reserved.
//

import Foundation
import UIKit
import EventKit
import EventKitUI

class CalendarViewController: UIViewController {
    let store = EKEventStore()
    
    let delegate = UIApplication.shared.delegate as! AppDelegate
    var titleEvent: String = ""
    var descriptionEvent: String = ""
    var startDate: String = ""
    var endDate: String = ""
    var location: String = ""
    var email: String = ""
    
//    @IBAction func openInAppBrowser(_ sender: Any) {
//        let webViewVC = WebViewController()
//        webViewVC.urlString = "https://www.glennvon.com/"
//        let navCon = UINavigationController(rootViewController: webViewVC)
//        self.navigationController?.present(navCon, animated: true, completion: nil)
//
//    }
    
    override func viewDidLoad() {
        createEvent()
        titleEvent = delegate.titleEvent
        descriptionEvent = delegate.descriptionEvent
        startDate = delegate.startDate
        endDate = delegate.endDate
        location = delegate.location
        email = delegate.email
    }
    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    @IBOutlet var close: UIButton!
//
//    func click() {
//        close.performClick(nil)
//    }
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//    }
//
//    @IBAction func close(sender: AnyObject) {
//        navigationController?.popViewController(animated: true)
//    }

    func createEvent() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd"
        let stDate = dateFormatter.date(from: self.startDate)
        let enDate = dateFormatter.date(from: self.endDate)
        
        // create the event object
        
        let event = EKEvent(eventStore: store)
        event.title = self.titleEvent
//        event.description = self.descriptionEvent
        event.startDate = stDate//NSDate() as Date
        event.endDate = enDate//NSDate() as Date
        
        // prompt user to add event (to whatever calendar they want)
        
        let controller = EKEventEditViewController()
        controller.event = event
        controller.eventStore = store
        controller.editViewDelegate = self as EKEventEditViewDelegate
        present(controller, animated: true)
    }
    
    
}

extension CalendarViewController: EKEventEditViewDelegate {
    
    func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
        controller.dismiss(animated: true)
    }
}
