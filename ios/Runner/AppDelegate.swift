import UIKit
import Flutter
import AWSS3
//import AWSCore
import AWSCognito

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {//, URLSessionDownloadDelegate
    var imagePath = [String : AnyObject]()
    var filePath: String = ""
    
    var titleEvent: String = ""
    var descriptionEvent: String = ""
    var startDate: String = ""
    var endDate: String = ""
    var location: String = ""
    var email: String = ""
    
    //    var task: URLSessionTask!
    
    //    lazy var session : URLSession = {
    //        let config = URLSessionConfiguration.default
    //        // config.allowsCellularAccess = false
    //        let session = URLSession(configuration: config, delegate: (self as URLSessionDelegate), delegateQueue: OperationQueue.main)
    //        return session
    //    }()
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
        ) -> Bool {
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        let s3UploadChannel = FlutterMethodChannel(name: "kinder.flutter/s3",
                                                   binaryMessenger: controller)
        
        s3UploadChannel.setMethodCallHandler({
            (call: FlutterMethodCall, result: FlutterResult) -> Void in
            s3UploadChannel.setMethodCallHandler({
                [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
                self?.imagePath = call.arguments as! [String : AnyObject]
                if call.method == "uploadImage" {
                    self?.uploadImage(result: result, imagePath: self?.imagePath["path"] as! String, awsFolder: self?.imagePath["aws_folder"] as! String, clientId: self?.imagePath["client_id"] as! String)
                } else if call.method == "showDialog"{
                    self?.showDialog(result: result, imagePath: self?.imagePath["imagePath"] as! String, clientId: self?.imagePath["clientId"] as! String)
                }
                else if call.method == "addToCalendar" {
                    
                    self?.titleEvent = self?.imagePath["title"] as! String
                    self?.descriptionEvent = self?.imagePath["description"] as! String
                    self?.startDate = self?.imagePath["start_date"] as! String
                    self?.endDate = self?.imagePath["end_date"] as! String
                    self?.location = self?.imagePath["location"] as! String
                    //                    self?.location = self?.imagePath["email"] as! String
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        self?.window?.rootViewController = nil
                        
                        let viewToPush = CalendarViewController()
                        
                        let navigationController = UINavigationController(rootViewController: controller)
                        
                        self?.window = UIWindow(frame: UIScreen.main.bounds)
                        self?.window?.makeKeyAndVisible()
                        self?.window?.backgroundColor = UIColor.white; self?.window.rootViewController = navigationController
                        navigationController.isNavigationBarHidden = false
                        navigationController.pushViewController(viewToPush, animated: false)
                        
                    })
                } else if call.method == "getVideoDownloadLevel" {
                    //                    self?.downloadVideo(url: self?.imagePath["url"] as! String)
                    self?.filePath = self?.imagePath["url"] as! String
                    print(self?.filePath)
                    UIView.animate(withDuration: 0.5, animations: {
                        self?.window?.rootViewController = nil
                        
                        let viewToPush = VideoDownloadViewController()
                        
                        let navigationController = UINavigationController(rootViewController: controller)
                        
                        self?.window = UIWindow(frame: UIScreen.main.bounds)
                        self?.window?.makeKeyAndVisible()
                        self?.window?.backgroundColor = UIColor.white; self?.window.rootViewController = navigationController
                        navigationController.isNavigationBarHidden = false
                        navigationController.pushViewController(viewToPush, animated: false)
                        
                    })
                } else if call.method == "getPhotoDownloadLevel" {
                    //                    self?.downloadImage(url: self?.imagePath["url"] as! String)
                    self?.filePath = self?.imagePath["url"] as! String
                    print(self?.filePath)
                    UIView.animate(withDuration: 0.5, animations: {
                        self?.window?.rootViewController = nil

                        let viewToPush = PhotoDownloadViewController()

                        let navigationController = UINavigationController(rootViewController: controller)

                        self?.window = UIWindow(frame: UIScreen.main.bounds)
                        self?.window?.makeKeyAndVisible()
                        self?.window?.backgroundColor = UIColor.white; self?.window.rootViewController = navigationController
                        navigationController.isNavigationBarHidden = true
                        navigationController.pushViewController(viewToPush, animated: false)

                    })
                    
                } else if call.method == "pdfDownload"{
                     self?.filePath = self?.imagePath["url"] as! String
                     print(self?.filePath)
                     UIView.animate(withDuration: 0.5, animations: {
                         self?.window?.rootViewController = nil

                         let viewToPush = PdfFileDownloadController()

                         let navigationController = UINavigationController(rootViewController: controller)

                         self?.window = UIWindow(frame: UIScreen.main.bounds)
                         self?.window?.makeKeyAndVisible()
                         self?.window?.backgroundColor = UIColor.white; self?.window.rootViewController = navigationController
                         navigationController.isNavigationBarHidden = false
                         navigationController.pushViewController(viewToPush, animated: false)

                     })


                 } else {
                    result(FlutterMethodNotImplemented)
                    return
                }
                
            })
        })
        
        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
   
    
    private func uploadImage(result: @escaping FlutterResult, imagePath: String, awsFolder: String, clientId: String) {
        let POOL_ID = "ap-southeast-2:4ebdae8f-00cd-4ffb-b3a1-44173b9b7dda"
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: AWSRegionType.APSoutheast2, identityPoolId: POOL_ID);
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSoutheast2, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        print("path: \(imagePath)")
        var imageAmazonUrl = ""
        let url = NSURL(fileURLWithPath: imagePath)
        
        let remoteName = nameGenerator(awsFolder: awsFolder, clientId: clientId)
        let S3BucketName = "proitzencloudcdn"
        print(remoteName)
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = url as URL
        uploadRequest.key = remoteName
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = "image/jpeg"
        uploadRequest.acl = .publicReadWrite
        
        let transferManager = AWSS3TransferManager.default()
        
        transferManager.upload(uploadRequest).continueWith { (task) -> AnyObject! in
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .cancelled, .paused:
                            print("upload failed .cancelled, .paused:")
                            break;
                        default:
                            print("upload() failed: defaultd [\(error)]")
                            break;
                        }
                    } else {
                        print("upload() failed: [\(error)]")
                    }
                } else {
                    //upload failed
                    print("upload() failed: domain [\(error)]")
                    
                    //                    result(FlutterError(code: "UNAVAILABLE",
                    //                                        message: "Battery info unavailable",
                    //                                        details: nil))
                }
            }
            
            if task.result != nil {
                imageAmazonUrl = "https://d212imxpbiy5j1.cloudfront.net/\(remoteName)"
                print("✅ Upload successed (\(imageAmazonUrl))")
                result(imageAmazonUrl)
            }
            return nil
        }
    }
    
    
    // Thirubaran User profile pic change
    
    
    
    private func showDialog (result: @escaping FlutterResult, imagePath: String, clientId: String){
        let POOL_ID = "ap-southeast-2:4ebdae8f-00cd-4ffb-b3a1-44173b9b7dda"
        
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: AWSRegionType.APSoutheast2, identityPoolId: POOL_ID);
        
        let configuration = AWSServiceConfiguration(region: AWSRegionType.APSoutheast2, credentialsProvider: credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        var pathArr = imagePath.split{$0 == "/"}.map(String.init)
        
        let remoteName = "img/uploads/\(clientId)_\(pathArr[pathArr.count - 1])"//nameGenerator(awsFolder: pathArr[pathArr.count - 1], clientId: clientId)
        
        let S3BucketName = "proitzencloudcdn"
        
        let url = NSURL(fileURLWithPath: imagePath)
        
        var imageAmazonUrl = ""
        
        print(remoteName)
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        
        uploadRequest.body = url as URL
        
        uploadRequest.key = remoteName
        
        uploadRequest.bucket = S3BucketName
        
        uploadRequest.contentType = "image/jpeg"
        
        uploadRequest.acl = .publicReadWrite
    
        print("user upload function")
        
        let transferManager = AWSS3TransferManager.default()
        
        
        
        transferManager.upload(uploadRequest).continueWith { (task) -> AnyObject! in
            
            if let error = task.error as NSError? {
                
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        
                        switch (errorCode) {
                            
                        case .cancelled, .paused:
                            
                            print("upload failed .cancelled, .paused:")
                            
                            break;
                            
                        default:
                            
                            print("upload() failed: defaultd [\(error)]")
                            
                            print("upload problem have to check 1")
                            
                            break;
                            
                        }
                        
                    } else {
                        
                        print("upload() failed: [\(error)]")
                        
                        print("upload problem have to check 2")
                        
                    }
                    
                } else {
                    
                    //upload failed
                    
                    print("upload() failed: domain [\(error)]")
                    
                    print("upload problem have to check 3")
                }
                
            }
            
            if task.result != nil {
                
                imageAmazonUrl = "https://d212imxpbiy5j1.cloudfront.net/\(remoteName)"
                
                print("✅ Upload successed (\(imageAmazonUrl))")
                
                result(imageAmazonUrl)
                
            }
            return nil
            
        }
    }
    
    public func nameGenerator(awsFolder: String, clientId: String) -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyy"
        let result = formatter.string(from: date)
        return "\(awsFolder)/\(clientId)_img_" + result + String(Int64(date.timeIntervalSince1970 * 1000)) + ".jpeg"
    }
}
