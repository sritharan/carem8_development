package com.carem8.proitzen;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.CognitoCredentialsProvider;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {

    private static final String CHANNEL = "proitzen.kinderm8/s3_image";
    MethodChannel.Result s3ParentResult;

    CognitoCredentialsProvider credentialsProvider;
    ClientConfiguration clientConfiguration = new ClientConfiguration();
    public static final String POOL_ID = "ap-southeast-2:4ebdae8f-00cd-4ffb-b3a1-44173b9b7dda";
    public static final Regions REGION = Regions.AP_SOUTHEAST_2;

    public static final String BUCKET_NAME = "proitzencloudcdn";

    final int[] sendStatus = {0};
    String key = "";
    BroadcastReceiver receiver;

    long queueid;
    DownloadManager dm;
    MethodChannel.Result videoParentResult;

    Uri downloadedUrl;
    String downloadUrl;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    //Asanka method channel call for s3

    new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
            new MethodChannel.MethodCallHandler() {
              @Override
              public void onMethodCall(MethodCall call, MethodChannel.Result result) {
                s3ParentResult = result;
                if (call.method.equals("addToCalendar")) {
                  addToCalendar(call.argument("start_date") + "", call.argument("end_date") + "", call.argument("title") + "", call.argument("description") + "", call.argument("location") + "", call.argument("email") + "");
                } else if (call.method.equals("getVideoDownloadLevel")) {
                  //Thiruban video download broadcast reciever
                  receiver = new BroadcastReceiver() {
                    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
                    @Override
                    public void onReceive(Context context, Intent intent) {
                      String action = intent.getAction();
                      if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {

                        DownloadManager.Query req_query = new DownloadManager.Query();
                        req_query.setFilterById(queueid);

                        Cursor c = dm.query(req_query);
                        if (c.moveToFirst()) {

                          int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);

                          if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                            System.out.println("Downlaod completed");
                          }
                        }
                      }
                    }
                  };

                  downloadUrl = call.argument("url");
                  downloadedUrl = getvideoDownloadLevel();
                  videoParentResult = result;
                } else if (call.method.equals("getPhotoDownloadLevel")) {
                    receiver = new BroadcastReceiver() {
                        @TargetApi(Build.VERSION_CODES.GINGERBREAD)
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            String action = intent.getAction();
                            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                                System.out.println("download err");
                                DownloadManager.Query req_query = new DownloadManager.Query();
                                req_query.setFilterById(queueid);

                                Cursor c = dm.query(req_query);
                                if (c.moveToFirst()) {

                                    int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);

                                    if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                                        System.out.println("Downlod completed");
                                    }
                                }
                            }
                        }
                    };

                    String url = call.argument("url");
                    System.out.println("url: "+url);
                    downloadedUrl = getphotoDownloadLevel(url);
                    videoParentResult = result;
                } else if(call.method.equals("changeProfileImage")){
                    changeProfileImage(call.argument("imagePath").toString());
                } else if (call.method.equals("pdfDownload")){
                    receiver = new BroadcastReceiver() {
                        @TargetApi(Build.VERSION_CODES.GINGERBREAD)
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            String action = intent.getAction();

                        }
                    };

                    String url = call.argument("url");
                    new DownloadTask(MainActivity.this, url);
//                            downloadedUrl = Uri.fromFile(task.getFile());
                    videoParentResult = result;
                    System.out.println("pdf download url - "+url);
                } else {
                  result.notImplemented();
                }
              }
            });

    registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    GeneratedPluginRegistrant.registerWith(this);
  }

  //Thiruban photo dowload get download level method
  @TargetApi(Build.VERSION_CODES.HONEYCOMB)
  Uri getphotoDownloadLevel(String url) {

      dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
      DownloadManager.Request request = new DownloadManager.Request(Uri.parse
              (url));

      request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

      request.setAllowedNetworkTypes(
              DownloadManager.Request.NETWORK_WIFI
                      | DownloadManager.Request.NETWORK_MOBILE)
              .setAllowedOverRoaming(true)
              .setAllowedOverMetered(true)
              .setTitle("Carem8 Image")
              .setDescription("Carem8 Image Is Downloading...")
              .setDestinationInExternalPublicDir("/carem8Images", "carem8_image.png");

      queueid = dm.enqueue(request);

      return dm.getUriForDownloadedFile(queueid);
  }

  //Thiruban video dowload get download level method

  @TargetApi(Build.VERSION_CODES.HONEYCOMB)
  Uri getvideoDownloadLevel() {

    dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
    DownloadManager.Request request = new DownloadManager.Request(Uri.parse
            (downloadUrl));
    // Environment.getExternalStorageDirectory(Environment().MEDIA_MOUNTED);
    //  request.setDestinationInExternalFilesDir(getApplicationContext(), Environment.DIRECTORY_DOWNLOADS , "video.mp4");

    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

    request.setAllowedNetworkTypes(
            DownloadManager.Request.NETWORK_WIFI
                    | DownloadManager.Request.NETWORK_MOBILE)
            .setAllowedOverRoaming(false).setTitle("Video")
            .setDescription("Carem8 Video Is Downloading.")
            .setDestinationInExternalPublicDir("/carem8Videos", "videos.mp4");

    queueid = dm.enqueue(request);

    return dm.getUriForDownloadedFile(queueid);
  }

  private void addToCalendar(String beginTime, String endTime, String title, String description, String location, String email) {
//        Calendar cal = Calendar.getInstance();//cal.getTimeInMillis()
    Date startDate = new Date(), endDate = new Date();
    try {
      startDate = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(beginTime);
      endDate = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(endTime);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    if (Build.VERSION.SDK_INT >= 14) {
      Intent intent = new Intent(Intent.ACTION_INSERT)
              .setData(CalendarContract.Events.CONTENT_URI)
              .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startDate)
              .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endDate)
              .putExtra(CalendarContract.Events.TITLE, title)
              .putExtra(CalendarContract.Events.DESCRIPTION, description)
              .putExtra(CalendarContract.Events.EVENT_LOCATION, location)
              .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
//                    .putExtra(Intent.EXTRA_EMAIL, email);
      startActivity(intent);
    } else {
      Intent intent = new Intent(Intent.ACTION_EDIT);
      intent.setType("vnd.android.cursor.item/event");
      intent.putExtra("beginTime", startDate);
      intent.putExtra("allDay", true);
      intent.putExtra("rrule", "FREQ=YEARLY");
      intent.putExtra("endTime", endDate);
      intent.putExtra("title", title);
      startActivity(intent);
    }
  }


    // Thirubaran  user profile picture change
    public void changeProfileImage(String path) {
        credentialsProvider = new CognitoCredentialsProvider(POOL_ID, REGION, clientConfiguration);
        final TransferUtility transferUtility1 = TransferUtility.builder().context(getApplicationContext())
                .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                .s3Client(new AmazonS3Client(credentialsProvider)).build();

        String [] temp = path.split("/");
        key = "img/uploads/"+temp[temp.length-1];

        final TransferObserver transferObserver1 = transferUtility1
                .upload(BUCKET_NAME, key, new File(path), CannedAccessControlList.PublicRead);

        transferObserver1.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.equals(TransferState.COMPLETED)) {
                    sendStatus[0] = 1;
                    s3ParentResult.success(key);
                } else if (state.equals(TransferState.WAITING)) {
                    sendStatus[0] = 0;
                } else if (state.equals(TransferState.FAILED)) {
                    sendStatus[0] = -1;
                    s3ParentResult.success(null);
                } else {
                    sendStatus[0] = -2;
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                sendStatus[0] = 0;
            }

            @Override
            public void onError(int id, Exception ex) {
                sendStatus[0] = -3;
            }
        });
    }
}