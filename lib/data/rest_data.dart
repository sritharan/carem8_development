import 'dart:async';
import 'dart:convert';

import 'package:carem8/models/appuser.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RestData {
  NetworkUtil _netUtil = new NetworkUtil();
  static final BASE_URL = 'https://apicare.carem8.com/v2.1.0/setupuseroneapp';

  // User
  Future<User> login(result) {
    var email = result["email"];
    var password = result["password"];
    var client_id = result["client_id"];
    var center = result["center"];
    print("$email,$password,$center,$client_id");

    return _netUtil.post(BASE_URL, body: {
      "email": email,
      "password": password,
      "client_id": client_id
    }).then((dynamic resp) async {

      List res = json.decode(resp);
      var outRes = resp.toString();
      var output = res[0];
      var appUser = new AppUser.fromJson(output);

//      String jwt = output["jwt"];
//      var user = output["user"];
//      List children = output["children"];
//      var client = user["client"];

      var globalConfig = output["globalconfig"];
      await saveGlobalConfig(globalConfig);

      var dummy = {
        "email": email,
        "password": password,
        "client_id": client_id,
        "center": outRes,
        "children": appUser.children
      };
      return new Future.value(new User.map(dummy));
    });
  }

  Future saveGlobalConfig(var configToSave) async {
    String encodedClientSectionConfig = json.encode(configToSave["client_section"]);
    String encodedNavigationConfig = json.encode(configToSave["navigation_set"]);
    String encodedProgramPlanSectionConfig = json.encode(configToSave["program_planning_section"]);
    String encodedNotificationSectionConfig = json.encode(configToSave["notification"]["nav_notification_index_title"]);
    String encodedPrivateMessageSectionConfig = json.encode(configToSave["private_message_section"]);
    String encodedChildProfileConfig = json.encode(configToSave["child_profile"]["nav_centermanagement_quicklink_label"]);
    String encodedStaffDocumentsSection = json.encode(configToSave["staff_documents_upload_section"]);
    String encodedNewsFeedSection = json.encode(configToSave["newsfeed_section"]["nav_newsfeed_label"]);
    String encodedAppSettings = json.encode(configToSave["application_settings"]);
    SharedPreferences prefs = await SharedPreferences.getInstance();
//    print('savedGlobalConfig $savedGlobalConfig');

    await prefs.setString("carem8_globalConfig_clientSection", encodedClientSectionConfig);
    await prefs.setString("carem8_globalConfig_navigationSet", encodedNavigationConfig);
    await prefs.setString("carem8_globalConfig_programPlanSection", encodedProgramPlanSectionConfig);
    await prefs.setString("carem8_globalConfig_notificationSection", encodedNotificationSectionConfig);
    await prefs.setString("carem8_globalConfig_privateMessageSection", encodedPrivateMessageSectionConfig);
    await prefs.setString("carem8_globalConfig_childProfileSection", encodedChildProfileConfig);
    await prefs.setString("carem8_globalConfig_StaffDocumentsSection", encodedStaffDocumentsSection);
    await prefs.setString("carem8_globalConfig_newsFeedSection", encodedNewsFeedSection);
//    await prefs.setString("globalConfig_journeyAdditionalTextFieldOne", encodedJourneyAdditionalTextFieldOne);
//    await prefs.setString("globalConfig_journeyAdditionalTextFieldTwo", encodedJourneyAdditionalTextFieldTwo);
    await prefs.setString("carem8_globalConfig_newsFeedDetailLabel", json.encode(configToSave["newsfeed_section"]["nav_newsfeed_tab_wall"]));
    await prefs.setString("carem8_globalConfig_appSettings", encodedAppSettings);
  }
}