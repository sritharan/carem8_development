import 'dart:io';
import 'package:carem8/models/children.dart';
import 'package:carem8/models/user.dart';
import 'package:path/path.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    print('CALED get');
    print(_db);
    if (_db != null) {
      print("checking Db 8");
      return  _db;
    }
    print("Db8.1");
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    print("create Database kinderm8");
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, "main.db");
    var ourDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return ourDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE User(uid INTEGER PRIMARY KEY, email TEXT, password TEXT,clientId TEXT,center TEXT,children TEXT)");
    print('User table created');
    await db.execute(
        "CREATE TABLE AppUser(cid INTEGER PRIMARY KEY,appdata TEXT)");
    print('AppUser table created');
    await db.execute(
        "CREATE TABLE Children(cid INTEGER PRIMARY KEY,childrendata TEXT)");
  }

//insertion
  Future<int> saveUser(User user) async {
    print("saveuser details");
    var dbClient = await db;
    int res = await dbClient.insert("User", user.toMap());
    print("Table updated $res");
    return res;
  }
  Future<int> saveAppUserData(Children children) async {
    print("savechild *spl*");
    var dbClient = await db;
    List mappedChildren = [];
    int res = await dbClient.insert("AppUser", children.toMap());
    print("Table updated $res");
    return res;
  }
  Future<int> saveChildrenData(Children children) async {
    print("savechild *spl*");
    var dbClient = await db;
    int res = await dbClient.insert("Children", children.toMap());
    print("Table updated $res");
    return res;
  }



  //retrieve

  // User
  Future<User> getFirstUser() async {
    print("getting first id");
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM User');
    if (list.isNotEmpty) {
      var element = list.elementAt(0);
      return new User(element["email"], element["password"],
          element["clientId"], element["center"],element["children"]);
    } else {
      return null;
    }
  }

//  update jwt
  ///only tested in notification List;
  ///
  Future<int> updateJwt(String jwt) async {
    print("updating............!");
    print(jwt);
    var dbClient = await db;
//    return await dbClient.update(tableNote, jwt.toMap(), where: "$columnId = ?", whereArgs: [note.id]);
    return await dbClient
        .rawUpdate('UPDATE User SET children = $jwt WHERE uid = ${1}');
  }

  //deletion
  Future<int> deleteUser() async {
    print("delete db 13");
    var dbClient = await db;
    int res = await dbClient.delete("User");
    getFirstUser();
    return res;
  }

  Future<bool> isLoggedIn() async {
    var dbClient = await db;
    var res = await dbClient.query("User");
    print('isLoggedIn >>>>>>>>> $res');
    return res.length > 0 ? true : false;
  }
}
