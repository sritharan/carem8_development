import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:carem8/Theme.dart' as Kinderm8Theme;
import 'package:carem8/components/page_transformer.dart';
import 'package:carem8/components/route_animations.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/modals/child_photos_modal.dart';
import 'package:carem8/utils/commonutils/zoomable_image_page_journal.dart';
import 'package:carem8/utils/commonutils/zoomable_image_url_only.dart';

/* Followup Page view */
class FollowupPageView extends StatelessWidget {
  FollowupPageView({
    @required this.followupitem,
    @required this.pageVisibility,
  });

  final followupitem;
  final PageVisibility pageVisibility;

  Widget _applyTextEffects({
    @required double translationFactor,
    @required Widget child,
  }) {
    final double xTranslation = pageVisibility.pagePosition * translationFactor;

    return Opacity(
      opacity: pageVisibility.visibleFraction,
      child: Transform(
        alignment: FractionalOffset.topLeft,
        transform: Matrix4.translationValues(
          xTranslation,
          0.0,
          0.0,
        ),
        child: child,
      ),
    );
  }

  _buildTextContainer(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;
    var followuptitleText = _applyTextEffects(
      translationFactor: 300.0,
      child: Text(
        followupitem['followup_text'].toUpperCase(),
        maxLines: 4,
        overflow: TextOverflow.ellipsis,
        style: textTheme.caption.copyWith(
          color: Kinderm8Theme.Colors.app_white[300],
          fontWeight: FontWeight.bold,
          letterSpacing: 2.0,
          fontSize: 18.0,
        ),
        textAlign: TextAlign.left,
      ),
    );
    var followupdate = _applyTextEffects(
      translationFactor: 300.0,
      child: Container(
        padding: EdgeInsets.only(top: 15.0),
        child: Text(
          'Date : ' + followupitem['followup_date'],
          style: textTheme.caption.copyWith(
            color: Kinderm8Theme.Colors.lightGrey,
            fontWeight: FontWeight.bold,
            letterSpacing: 2.0,
            fontSize: 13.0,
          ),
          textAlign: TextAlign.left,
        ),
      ),
    );

    var subtitleText = _applyTextEffects(
      translationFactor: 200.0,
      child: Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: Text(
          followupitem['what_happened'],
          maxLines: 3,
          overflow: TextOverflow.ellipsis,
          style: textTheme.title.copyWith(
            color: Kinderm8Theme.Colors.lightGrey,
            fontWeight: FontWeight.bold,
            fontSize: 13.0,
          ),
          textAlign: TextAlign.left,
        ),
      ),
    );

    return Positioned(
      bottom: 56.0,
      left: 32.0,
      right: 32.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          followuptitleText,
          subtitleText,
          followupdate,
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var image;
    var singlefollowupimagelist = List();
    if (followupitem['followup_gallery'].length > 0) {
      for (int j = 0; j < followupitem['followup_gallery'].length; j++) {
        /*print('Single followup_gallery : ${followupitem['followup_gallery'][j]['url']}');*/
        var fullurl =
            "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${followupitem['followup_gallery'][j]['url']}";
        /*print(fullurl);*/
        singlefollowupimagelist.add(fullurl);
        print(fullurl);
      }
      image = Image.network(
        singlefollowupimagelist[0],
        fit: BoxFit.cover,
        alignment: FractionalOffset(
          0.5 + (pageVisibility.pagePosition / 3),
          0.5,
        ),
      );
    }

    var imageOverlayGradient = DecoratedBox(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: FractionalOffset.bottomCenter,
          end: FractionalOffset.topCenter,
          colors: [
            const Color(0xFF000000),
            const Color(0x00000000),
          ],
        ),
      ),
    );

    return GestureDetector(
      onTap: () {
        print('clicked');
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) =>
                    new StoryScreen(singlefolllowup: followupitem)));
//        return FadeInRoute(builder: (context) {
//          return StoryCard(singlefolllowup:followupitem);
//        });
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 16.0,
          horizontal: 8.0,
        ),
        child: Material(
          elevation: 4.0,
          borderRadius: BorderRadius.circular(8.0),
          child: Stack(
            fit: StackFit.expand,
            children: [
              followupitem['followup_gallery'].length > 0
                  ? Image.network(
                      singlefollowupimagelist[0],
                      fit: BoxFit.cover,
                      alignment: FractionalOffset(
                        0.5 + (pageVisibility.pagePosition / 3),
                        0.5,
                      ),
                    )
                  : Image.asset(
                      'assets/cube_feed_noimage.png',
                      fit: BoxFit.cover,
                      alignment: FractionalOffset(
                        0.5 + (pageVisibility.pagePosition / 3),
                        0.5,
                      ),
                    ),
              imageOverlayGradient,
              _buildTextContainer(context),
            ],
          ),
        ),
      ),
    );
  }
}

//class StoryCard extends StatelessWidget {
//  const StoryCard({
//    Key key,
//    @required this.singlefolllowup,
//  }) : super(key: key);
//
//  final singlefolllowup;
//
//  @override
//  Widget build(BuildContext context) {
//    var finalImages = List();
//    var singlefollowupimagelist = List();
//    for (int j = 0; j < singlefolllowup['followup_gallery'].length; j++) {
//      /*print('Single followup_gallery : ${followupitem['followup_gallery'][j]['url']}');*/
//      var fullurl = "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${singlefolllowup['followup_gallery'][j]['url']}";
//      /*print(fullurl);*/
//      singlefollowupimagelist.add(fullurl);
//    }
//    for (int i = 0; i < singlefollowupimagelist.length; i++) {
//      finalImages.add(new CachedNetworkImageProvider(singlefollowupimagelist[i]));
//    }
//
//    print(singlefollowupimagelist);
//    final Size screenSize = MediaQuery.of(context).size;
//
//    return Scaffold(
//      body: SingleChildScrollView(
//        child: Column(
//          children : <Widget>[
//            Card(
//            child: Container(
//              height: screenSize.height,
//              child: Padding(
//                padding: const EdgeInsets.all(20.0),
//                child: Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: [
//                    Container(
//                      padding: EdgeInsets.symmetric(vertical: 3.0, horizontal: 8.0),
//                      margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 20.0),
////                decoration: BoxDecoration(
////                  gradient: new LinearGradient(
////                    begin: Alignment.centerLeft,
////                    end: Alignment.centerRight,
////                    colors: [
////                      Colors.teal.shade300,
////                      Colors.teal,
////                    ],
////                  ),
////                ),
//                      child: Text(
//                        singlefolllowup['followup_text'],
//                        style: TextStyle(
//                          color: Kinderm8Theme.Colors.boldText,
//                          fontSize: 12.0,
//                          // fontWeight: FontWeight.bold,
//                        ),
//                      ),
//                    ),
//
//                    Container(
//                      height: 50.0,
//                      child: Text(
//                        singlefolllowup['followup_date'],
//                        style: TextStyle(
//                          color: Kinderm8Theme.Colors.boldText,
//                          fontSize: 18.0,
//                          fontWeight: FontWeight.w600,
//                        ),
//                      ),
//                    ),
//
//                    Container(
//                      margin: EdgeInsets.symmetric(vertical: 10.0),
//                      child: Text(
//                        singlefolllowup['what_happened'],
//                        textAlign: TextAlign.left,
//                        maxLines: 5,
//                        style: TextStyle(
//                          fontSize: 12.0,
//                          color: Kinderm8Theme.Colors.bodyText,
//                        ),
//                      ),
//                    ),
//
//                    Expanded(
//                      child: Container(
////                  decoration: BoxDecoration(
////                    borderRadius: BorderRadius.all(Radius.circular(6.0)),
////                    image: DecorationImage(
////                      fit: BoxFit.cover,
////                      image: NetworkImage(singlefollowupimagelist[0]),
////                    ),
////                  ),
//                        margin: EdgeInsets.symmetric(vertical: 10.0),
//                        child: FlatButton(
//                         child: Container(
//                            child: Text("hiii"),
//                          ),
//                            /*child: new SizedBox(
//                                height: 200.0,
//                                width: 350.0,
//                                child: new Carousel(
//                                  animationCurve: ElasticInCurve(10.0),
//                                  images: finalImages,
//                                  dotSize: 4.0,
//                                  dotSpacing: 15.0,
//                                  dotColor: Colors.white,
//                                  indicatorBgPadding: 5.0,
////                                dotBgColor: Colors.purple.withOpacity(0.5),
//                                  autoplay: false,
//                                  borderRadius: true,
//                                  radius: Radius.circular(5.0),
//                                )
//                            ),*/
//                            onPressed: () {
//                              print("Have to Navigate image view..");
//
////                              Navigator.push(
////                                  context,
////                                  new MaterialPageRoute(
////                                      builder: (context) =>
////                                      new ZoomableImagePage_Url(finalImages)));
//                            }),
//                      ),
//                    ),
//                    Divider(
//                      height: 20.0,
//                      color: Kinderm8Theme.Colors.bodyText,
//                    ),
//                  ],
//                ),
//              ),
//            ),
//          ),
//            ]
//
//        ),
//      ),
//    );
//  }
//}

class StoryScreen extends StatelessWidget {
  final singlefolllowup;
  StoryScreen({Key key, @required this.singlefolllowup}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    List finalImages = List();
    List<ChildPhotoModal> singlefollowupimagelist = List();
    if (singlefolllowup['followup_gallery'].length > 0) {
      for (int j = 0; j < singlefolllowup['followup_gallery'].length; j++) {
        /*print('Single followup_gallery : ${followupitem['followup_gallery'][j]['url']}');*/
        var fullurl =
            "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${singlefolllowup['followup_gallery'][j]['url']}";
        /*print(fullurl);*/
        singlefollowupimagelist.add(ChildPhotoModal(fileUrl: fullurl, date: "", caption: ""));
        finalImages.add(new CachedNetworkImageProvider(fullurl));
      }
    }

    print(singlefollowupimagelist);
    final Size screenSize = MediaQuery.of(context).size;

    return Material(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: buildAppBar(context),
        body: Stack(
          children: <Widget>[
            Container(
              foregroundDecoration: BoxDecoration(
                backgroundBlendMode: BlendMode.screen,
                gradient: new LinearGradient(
                  begin: Alignment(0.0, -0.4),
                  end: Alignment(0.00, 1.0),
                  colors: [
                    Colors.transparent,
                    Colors.white,
                  ],
                ),
              ),
              child: SingleChildScrollView(
                child: Container(
//                  padding: EdgeInsets.all(16.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                        /* journal date text */
                        singlefolllowup['journal_date'] != '' &&
                            singlefolllowup['journal_date'] != null
                            ? Container(
                          padding: EdgeInsets.only(top:5.0,left: 10.0,right: 10.0,bottom: 5.0),
                          child: Row(
                            children: <Widget>[
                              new Text('${labelsConfig["journalDateLabel"]} : ',
                                  textAlign: TextAlign.start,
                                  style: new TextStyle(
                                      fontSize: 14.0,
                                      color: Kinderm8Theme.Colors.darkGrey,
                                      fontWeight: FontWeight.w500)),
                              new Text(singlefolllowup['journal_date'],
                                  textAlign: TextAlign.start,
                                  style: new TextStyle(
                                      fontSize: 14.0,
                                      color: Kinderm8Theme.Colors.bodyText,
                                      fontWeight: FontWeight.w400)
                              ), //
                            ],
                          ),
                        ): Container(
                          padding: EdgeInsets.all(0.0)
                        ),

                        /* journal date text */
                        singlefolllowup['followup_date'] != '' &&
                            singlefolllowup['followup_date'] != null
                            ? Container(
                          padding: EdgeInsets.only(top:5.0,left: 10.0,right: 10.0,bottom: 5.0),
                          child: Row(
                            children: <Widget>[
                              new Text('${labelsConfig["followUpDateLabel"]} : ',
                                  textAlign: TextAlign.start,
                                  style: new TextStyle(
                                      fontSize: 14.0,
                                      color: Kinderm8Theme.Colors.darkGrey,
                                      fontWeight: FontWeight.w500)),
                              new Text(singlefolllowup['followup_date'],
                                  textAlign: TextAlign.start,
                                  style: new TextStyle(
                                      fontSize: 14.0,
                                      color: Kinderm8Theme.Colors.bodyText,
                                      fontWeight: FontWeight.w400)
                              ), //
                            ],
                          ),
                        ): Container(
                            padding: EdgeInsets.all(0.0)
                        ),

                        /* followup text */
                        singlefolllowup['followup_text'] != '' &&
                            singlefolllowup['followup_text'] != null
                            ? new Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              width: screenSize.width,
                              padding: const EdgeInsets.only(
                                  top: 5.0, bottom: 5.0),
                              color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                              child: new Text(labelsConfig["followUpLabel"],
                                  style: new TextStyle(
                                      fontSize: 16.0,
                                      color: Kinderm8Theme
                                          .Colors.app_white,
                                      fontWeight: FontWeight.w500)),
                            ),
                            new Container(
                              margin: new EdgeInsets.only(
                                  top: 5.0,
                                  left: 10.0,
                                  right: 10.0,
                                  bottom: 5.0),
                              child: new Text(
                                  singlefolllowup['followup_text'],
                                  style: new TextStyle(
                                      fontSize: 13.0,
                                      color: Kinderm8Theme
                                          .Colors.darkGrey,
                                      fontWeight: FontWeight.w400)),
                            ),
                          ],
                        )
                            : Container(
                          padding: EdgeInsets.all(0.0),
                        ),


                        /* What Happened */
                        singlefolllowup['what_happened'] != '' &&
                            singlefolllowup['what_happened'] != null
                            ? new Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              width: screenSize.width,
                              padding: const EdgeInsets.only(
                                  top: 5.0, bottom: 5.0),
                              color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                              child: new Text(labelsConfig["whatHappendLabel"],
                                  style: new TextStyle(
                                      fontSize: 16.0,
                                      color: Kinderm8Theme
                                          .Colors.app_white,
                                      fontWeight: FontWeight.w500)),
                            ),
                            new Container(
                              margin: new EdgeInsets.only(
                                  top: 10.0,
                                  left: 20.0,
                                  right: 20.0,
                                  bottom: 10.0),
                              child: new Text(
                                  singlefolllowup['what_happened'],
                                  style: new TextStyle(
                                      fontSize: 13.0,
                                      color: Kinderm8Theme
                                          .Colors.darkGrey,
                                      fontWeight: FontWeight.w400)),
                            ),
                          ],
                        )
                            : Container(
                          padding: EdgeInsets.all(0.0),
                        ),

                        /* Interpretation */
                        singlefolllowup['interpretation'] != '' &&
                            singlefolllowup['interpretation'] != null
                            ? new Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              width: screenSize.width,
                              padding: const EdgeInsets.only(
                                  top: 5.0, bottom: 5.0),
                              color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                              child: new Text('Interpretation',
                                  style: new TextStyle(
                                      fontSize: 16.0,
                                      color: Kinderm8Theme
                                          .Colors.app_white,
                                      fontWeight: FontWeight.w500)),
                            ),
                            new Container(
                              margin: new EdgeInsets.only(
                                  top: 10.0,
                                  left: 20.0,
                                  right: 20.0,
                                  bottom: 10.0),
                              child: new Text(
                                  singlefolllowup['interpretation'],
                                  style: new TextStyle(
                                      fontSize: 13.0,
                                      color: Kinderm8Theme
                                          .Colors.darkGrey,
                                      fontWeight: FontWeight.w400)),
                            ),
                          ],
                        )
                            : Container(
                          padding: EdgeInsets.all(0.0),
                        ),


                        singlefolllowup['followup_gallery'].length > 0
                            ? Column(
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.center,
                                  width: screenSize.width,
                                  padding: const EdgeInsets.only(
                                      top: 5.0, bottom: 5.0),
                                  color: Kinderm8Theme.Colors.appcolour,
//                                    margin: new EdgeInsets.only(top:10.0),
                                  child: new Text(labelsConfig["followUpPhotoLabel"],
                                      style: new TextStyle(
                                          fontSize: 16.0,
                                          color: Kinderm8Theme
                                              .Colors.app_white,
                                          fontWeight: FontWeight.w500)),
                                ),
                                Container(
                                    height: 220.0,
                                    margin: EdgeInsets.symmetric(vertical: 5.0),
                                    child: FlatButton(
                                        child: new SizedBox(
                                            height: 200.0,
                                            child: new Carousel(
                                              animationCurve: ElasticInCurve(10.0),
                                              images: finalImages,
                                              dotSize: 4.0,
                                              dotSpacing: 15.0,
                                              dotColor: Colors.white,
                                              indicatorBgPadding: 5.0,
            //                                dotBgColor: Colors.purple.withOpacity(0.5),
                                              autoplay: false,
                                              borderRadius: true,
                                              radius: Radius.circular(5.0),
                                            )),
                                        onPressed: () {
                                          print("Navigate list image viewer..");
                                          Navigator.push(
                                              context,
                                              new MaterialPageRoute(
                                                  builder: (context) =>
                                                      new ZoomableImagePage_Journal(
                                                          singlefollowupimagelist)));
                                        }),
                                  ),
                              ],
                            )
                            : Container(),

                        /* last element section bottom */
                        new Container(
                          margin:
                              new EdgeInsets.only(bottom: screenSize.width / 5),
                        ),
                      ]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildAppBar(BuildContext context) {
    return AppBar(
      leading: CupertinoButton(
          child: Icon(
            CupertinoIcons.back,
            color: Kinderm8Theme.Colors.appcolour,
          ),
          onPressed: () {
            Navigator.pop(context);
          }),
      actions: <Widget>[
//        CupertinoButton(
//          child: Icon(
//            Icons.favorite_border,
//            size: 22.0,
//            color: Kinderm8Theme.Colors.appcolour,
//          ),
//          onPressed: () {},
//        ),

      ],
      elevation: 0.0,
      backgroundColor: Colors.transparent,
    );
  }
}
