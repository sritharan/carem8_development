import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:carem8/Theme.dart' as Theme;
import 'package:flutter/material.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/dailyjournal/dailyjournaldata.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:carem8/utils/commonutils/emptybody.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';

class DailyJournal extends StatefulWidget {
  final ChildViewModal childData;
  final jwt;
  DailyJournal(this.childData, this.jwt);
  @override
  DailyJournalState createState() => DailyJournalState();
}

class DailyJournalState extends State<DailyJournal>
    implements HomePageContract {
  var k, appuser, jwt, id, clientId;
  List data;
  var childId;
  bool isLoading;
  bool load = true;
  HomePagePresenter _presenter;
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var dailyJournalData;
  DailyJournalState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(labelsConfig["dailyJournalsLabel"],
            style: TextStyle(
              color: Colors.white,
            )),
        backgroundColor: Colors.deepPurple,
        centerTitle: true,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              tooltip: 'Refresh',
              onPressed: () {
                print("Reload..");
                setState(() {
                  load = true;
                });
                fetchDailyJournalData(0);
              })
        ],
      ),
//      drawer: new CommonDrawer(),
      body: new RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: handleRefresh,
        child: new Center(
          child: load
              ? progress
              : data.length > 0
                  ? new ListView.builder(
                      itemCount: this.data != null ? (this.data.length + 1) : 0,
                      itemBuilder: (context, i) {
                        if (i == k) {
                          if (dailyJournalData != null && dailyJournalData.length < 10) {
                            return Container();
                          } else {
                            return _buildCounterButton();
                          }
                        } else {
                          final singleJournalData = this.data[i];

                          return DailyJournalData(singleJournalData, jwt);
                        }
                      },
                    )
                  : EmptyBody("No Journals yet."),
        ),
      ),
    );
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appdarkcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: isLoading
            ? new CupertinoActivityIndicator()
            : const Text('Load more...',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: Theme.Colors.app_white)),
        color: Theme.Colors.appcolour.withOpacity(0.75),
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
        });
        fetchDailyJournalData(k);
      };
    }
  }

  Future<String> fetchDailyJournalData(int s) async {
    ///data from GET method
    print("data fetched");
    childId = widget.childData.id;
    String _dailyJournalUrl =
        'https://apicare.carem8.com/v2.1.1/journal/getjournal?childid=$childId&step=$s&clientid=$clientId&userid=$id';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_dailyJournalUrl, headers: headers).then((response) {
      var dailyJournalData;
      try {
        dailyJournalData = json.decode(response.body);
        print(dailyJournalData.length);
        print('res get ${response.body}');
        print('dailyJournalUrlData $dailyJournalData');
      } catch (e) {
        print('That string was null!');
      }

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(isLoading);
        isLoading = false;
        print(isLoading);
        if (s == 0) {
          setState(() {
            load = false;
            this.data = dailyJournalData;
          });
        } else {
          setState(() {
            load = false;
            data.addAll(dailyJournalData);
          });
        }
        k = data.length;
      } else if (response.statusCode == 500 &&
          dailyJournalData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        fetchDailyJournalData(0);
      }
    });
    return null;
  }

  getRefreshToken() {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      }catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (k != null) {
        fetchDailyJournalData(k);
      } else {
        fetchDailyJournalData(0);
      }
    });
  }

  Future<Null> handleRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      load = true;
      fetchDailyJournalData(0);
    });
    return null;
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = widget.jwt.toString();
//      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

      print("iddd $id");
      print(clientId);

      fetchDailyJournalData(0);

    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
