import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:dio/dio.dart';
import 'package:carem8/Theme.dart' as Kinderm8Theme;

class ViewComment extends StatefulWidget {
  final data;
  final user;
  final String jwt;
  final Function commentCountFunction;
  final Function decreaseCommentCountFunction;
  ViewComment(this.data, this.user, this.jwt, {this.commentCountFunction, this.decreaseCommentCountFunction});
  @override
  ViewCommentState createState() => ViewCommentState(data, user, jwt);
}

class ViewCommentState extends State<ViewComment> {
  final data, user;
  String jwt;
  final formKey = new GlobalKey<FormState>();
  var commentData;
  bool load = false;
  var image, fullname, singlecomment, date, commentlist, cliId;
  var postfeedId, userId;
  final TextEditingController _textController = new TextEditingController();
  bool _isComposing = false;
  var commentId;

  var progress = new ProgressBar(
    color: Kinderm8Theme.Colors.appcolour,
    containerColor: Kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  ViewCommentState(this.data, this.user, this.jwt) {
    getCommentData();
  }

  getCommentData() {
    print("getCommentData");
    postfeedId = data["id"];
    cliId = user["client_id"];
    userId = user["id"];
    String getCommentUrl =
        'https://apicare.carem8.com/v2.1.0/newsfeedcomment/$postfeedId?clientid=$cliId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(getCommentUrl, headers: headers).then((response) {
      print('res get ${response.body}');
      commentData = json.decode(response.body);

      print('map $commentData');
      print(response.statusCode);
      if (response.statusCode == 200) {
      commentlist = commentData["comment"];

      setState(() {
        load = true;
      });
      } else if (response.statusCode == 500 &&
          commentData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      }
    });
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$cliId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      getCommentData();
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return load
        ? new Container(
            child: Column(
              children: <Widget>[
                new Expanded(
                    child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Expanded(
                      child: new Container(
                        width: screenSize.width,
                        child: new ListView.builder(
                          itemCount: this.commentlist != null
                              ? (this.commentlist.length)
                              : 0,
                          itemBuilder: (context, i) {
                            final singlecomment = this.commentlist[i];
                            var singlecommentDetails = singlecomment["comment"];
                            var userDetails = singlecomment["user"];

                            var commentUserId = userDetails["id"];
                            var image = userDetails["image"];
                            var fullname = userDetails["fullname"];
                            var comments = singlecommentDetails["comment"];
                            var parsedDate = DateTime.parse(
                                singlecommentDetails["created_at"]);
                            var date = new DateFormat.yMMMMEEEEd()
                                .add_jm()
                                .format(parsedDate);
                            commentId = singlecommentDetails["id"];

                            return new Center(
                                child: Column(
                              children: <Widget>[
                                new ListTile(
                                    leading: image != null
                                        ? CircleAvatar(
                                            backgroundImage: NetworkImage(image,
                                                scale: 10.0),
                                            radius: 25.0,
                                          )
                                        : CircleAvatar(
                                            backgroundImage: AssetImage(
                                                "assets/nophoto.jpg"),
                                            radius: 25.0,
                                          ),
                                    isThreeLine: true,
                                    trailing: userId == commentUserId
                                        ? new GestureDetector(
                                            child: Icon(Icons.delete),
                                            onTap: () {
                                              showModalBottomSheet<void>(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return Container(
                                                        height: 150.0,
                                                        width: screenSize.width,
                                                        child: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(32.0),
                                                          child: Column(
                                                            children: <Widget>[
                                                              new Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .all(
                                                                        2.0),
                                                                child: new Text(
                                                                  "Do you wish to delete",
                                                                  style: new TextStyle(
                                                                      fontSize:
                                                                          22.0,
                                                                      color: Kinderm8Theme
                                                                          .Colors
                                                                          .appsupportlabel,
                                                                      fontStyle:
                                                                          FontStyle
                                                                              .normal),
                                                                ),
                                                              ),
                                                              new GestureDetector(
                                                                onTap: () {
                                                                  print(
                                                                      singlecommentDetails);
                                                                  print(
                                                                      singlecommentDetails[
                                                                          'id']);
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                  if (widget.decreaseCommentCountFunction != null) {
                                                                    widget.decreaseCommentCountFunction();
                                                                  }
                                                                  deleteComment(
                                                                      singlecommentDetails[
                                                                          'id']);
                                                                },
                                                                child:
                                                                    new Container(
                                                                  width: screenSize
                                                                          .width /
                                                                      2,
                                                                  margin: new EdgeInsets
                                                                          .only(
                                                                      top: 10.0,
                                                                      left: 5.0,
                                                                      right:
                                                                          5.0,
                                                                      bottom:
                                                                          10.0),
                                                                  height: 30.0,
                                                                  decoration: new BoxDecoration(
                                                                      color: Kinderm8Theme
                                                                          .Colors
                                                                          .buttonicon_deletecolor,
                                                                      borderRadius: new BorderRadius
                                                                          .all(new Radius
                                                                              .circular(
                                                                          10.0))),
                                                                  child:
                                                                      new Center(
                                                                          child:
                                                                              new Text(
                                                                    "Delete",
                                                                    style: new TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            22.0),
                                                                  )),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ));
                                                  });
                                            },
                                          )
                                        : null,
                                    title: new Text(
                                      fullname,
                                      style: new TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14.0),
                                    ),
                                    subtitle: Container(
                                        child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(top: 5.0),
                                          child: new Text(
                                            date,
                                            style: new TextStyle(
                                                color: Colors.grey.shade500,
                                                fontSize: 10.0),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(top: 5.0),
                                          child: Row(
                                            children: <Widget>[
                                              Flexible(
                                                child: Text(" $comments",
                                                    style: new TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 12.0)),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ))),
                                Divider(),
                              ],
                            ));

                            ////
                          },
                        ),
                      ),
                    ),
                  ],
                )),
                new Container(
                  padding: EdgeInsets.all(5.0),
                  child: new Row(
                    children: <Widget>[
                      new Flexible(
                        child: new Form(
                            key: formKey,
                             child: TextFormField(
                              maxLines: 1,
                              controller: _textController,
                              validator: (val) {
                                return val.length == 0
                                    ? "Comment must not be null"
                                    : null;
                              },
                              decoration: new InputDecoration(
//                                  border:
                                  fillColor: Colors.grey[300],
//                                  filled: true,
                                  contentPadding: new EdgeInsets.fromLTRB(
                                      10.0, 30.0, 10.0, 10.0),
                                  border: new OutlineInputBorder(
                                    borderRadius: new BorderRadius.circular(0.0),
                                  ),
                              ),
                            )),
                      ),
                      new RaisedButton(
                        onPressed: () {
                          if (widget?.commentCountFunction != null) {
                            widget.commentCountFunction();
                          }
                          submitComment();
                        },
                        color: Kinderm8Theme.Colors.appmenuicon,
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          // Replace with a Row for horizontal icon + text
                          children: <Widget>[
                            Icon(
                              Icons.send,
                              color: Kinderm8Theme.Colors.buttonicon_darkcolor,
                            ),
                            Text("Submit",
                                style: new TextStyle(
                                    fontSize: 12.0,
                                    color:
                                        Kinderm8Theme.Colors.buttonicon_color))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        : progress;
  }

  Widget _buildTextComposer() {
    return new IconTheme(
      data: new IconThemeData(color: Theme.of(context).accentColor),
      child: new Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          child: new Row(children: <Widget>[
            new Flexible(
              child: new TextField(
                controller: _textController,
                onChanged: (String text) {
                  setState(() {
                    _isComposing = text.length > 0;
                  });
                },
                onSubmitted: submitComment(),
                decoration:
                new InputDecoration.collapsed(hintText: "Send a message"),
              ),
            ),
            new Container(
                margin: new EdgeInsets.symmetric(horizontal: 4.0),
                child: Theme.of(context).platform == TargetPlatform.iOS
                    ? new CupertinoButton(
                        child: new Text(
                          "Send",
                          style: new TextStyle(
                              fontSize: 10.0,
                              color: Kinderm8Theme.Colors.appsupportlabel,
                              fontStyle: FontStyle.italic),
                        ),
                        onPressed: _isComposing ? () => submitComment() : null,
                      )
                    : new IconButton(
                        icon: new Icon(Icons.send),
                        onPressed: _isComposing ? () => submitComment() : null,
                      )),
          ]),
          decoration: Theme.of(context).platform == TargetPlatform.iOS
              ? new BoxDecoration(
              border:
              new Border(top: new BorderSide(color: Colors.grey[200])))
              : null),
    );
  }

  void deleteComment(deletecommentid) async {
    setState(() => load = false);
    String deleteUrl =
        'https://apicare.carem8.com/v2.1.0/newsfeedcomment?clientid=$cliId';
    var body = {"id": deletecommentid, "user_id": userId};
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.apiDeleteRequest(deleteUrl, headers: headers, body: json.encode(body)).then((res) {
      getCommentData();
    });
  }

  submitComment() {
    final form = formKey.currentState;
    if (form.validate()) {
      setState(() => load = false);
      form.save();

      final submitcommentUrl =
          'https://apicare.carem8.com/v2.1.0/newsfeedcomment?clientid=$cliId';
      var body = {
        "user_id": userId,
        "postfeed_id": postfeedId,
        "comment": _textController.text
      };
      var headers = {"x-authorization": jwt.toString()};

      NetworkUtil _netUtil = new NetworkUtil();
      _netUtil
          .post(submitcommentUrl,
              body: body, headers: headers, encoding: jwt.toString())
          .then((res) {
        print(res);
//        var result = json.decode(res);
        _textController.clear();
        getCommentData();
      });
    }
  }
}

//*******************
/*
class ShowComment extends StatefulWidget {
  final comment;
  final needed;
  ShowComment(this.comment, this.needed);
  @override
  ShowCommentState createState() => ShowCommentState(comment, needed);
}
*/

/*//class ShowCommentState extends State<ShowComment> {
//  final comment;
//  final needed;
//  ShowCommentState(this.comment, this.needed);
//  var user_id, clientId, postfeedId, commentId, jwt;
//
//  @override
//  Widget build(BuildContext context) {
//    var image, fullname, comments, date, center, commentUser_id;
//    var commentDetails = comment["comment"];
//    var userDetails = comment["user"];
//    user_id = needed["user_id"];
//    clientId = needed["client_id"];
//    postfeedId = needed["postfeed_id"];
//    jwt = needed["jwt"];
//    commentId = commentDetails["id"];
//
//    commentUser_id = userDetails["id"];
//    image = userDetails["image"];
//    fullname = userDetails["fullname"];
//    comments = commentDetails["comment"];
//    var parsedDate = DateTime.parse(commentDetails["created_at"]);
//    date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);
//
//    center = new Center(
//        child: Column(
//      children: <Widget>[
//        new ListTile(
//          leading: image != null
//              ? CircleAvatar(
//                  backgroundImage: NetworkImage(image, scale: 10.0),
//                  radius: 25.0,
//                )
//              : CircleAvatar(
//                  backgroundImage: AssetImage("assets/nophoto.jpg"),
//                  radius: 25.0,
//                ),
//          trailing: user_id == commentUser_id
//              ? new GestureDetector(
//                  child: Icon(Icons.delete),
//                  onTap: () {
//                    showModalBottomSheet<Null>(
//                        context: context,
//                        builder: (BuildContext context) {
//                          return Container(
//                            height: 200.0,
//                            child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.stretch,
//                              children: <Widget>[
//                                Text(
//                                    "Do you really want to delete this comment?"),
//                                Divider(),
//                                new RaisedButton(
//                                  onPressed: () {
//                                    deleteComment();
//                                    Navigator.of(context).pop();
//                                  },
//                                  child: Text("DELETE"),
//                                ),
//                              ],
//                            ),
//                          );
//                        });
//                  },
//                )
//              : null,
//          title: new Text(
//            fullname,
//            style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 14.0),
//          ),
//          subtitle: new Text(
//            comments,
//            style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 10.0),
//          ),
//        ),
//        Divider(),
//      ],
//    ));
//
//    return center;
//  }
//
//  void deleteComment() async {
//    print("delete here");
//    print(user_id);
//    print(clientId);
//    print(commentId);
//    print(jwt);
//
//    String deleteUrl =
//        'http://13.210.72.124:7070/v2.1.0/newsfeedcomment?clientid=$clientId';
//    var body = {"id": commentId, "user_id": user_id};
//    var headers = {"x-authorization": jwt.toString()};
//
//    NetworkUtil _netutil = new NetworkUtil();
//    _netutil.delete(deleteUrl, headers: headers, body: body).then((res) {
//      print(res);
////      var result = json.decode(res);
////      print(result);
////      getCommentData();
////      setState(() {});
//    });
//  }
//}*/

/*
class ShowComment extends StatelessWidget {
  final comment;
  final needed;
  ShowComment(this.comment, this.needed);

  @override
  Widget build(BuildContext context) {
    var user_id = needed["user_id"];
    var image, fullname, comments, date, center, commentUser_id;
    var commentDetails = comment["comment"];
    var userDetails = comment["user"];
    commentUser_id = userDetails["id"];

    image = userDetails["image"];
    fullname = userDetails["fullname"];
    comments = commentDetails["comment"];
    var parsedDate = DateTime.parse(commentDetails["created_at"]);
    date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);

    center = new Center(
        child: Column(
      children: <Widget>[
        new ListTile(
          leading: image != null
              ? CircleAvatar(
                  backgroundImage: NetworkImage(image, scale: 10.0),
                  radius: 25.0,
                )
              : CircleAvatar(
                  backgroundImage: AssetImage("assets/nophoto.jpg"),
                  radius: 25.0,
                ),
          trailing: user_id == commentUser_id
              ? new GestureDetector(
                  child: Icon(Icons.delete),
                  onTap: () {
                    showModalBottomSheet<Null>(
                        context: context,
                        builder: (BuildContext context) {
                          return Container(
                            height: 200.0,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Text(
                                    "Do you really want to delete this comment?"),
                                Divider(),
                                new RaisedButton(
                                  onPressed: (){
                                    deleteComment();
                                    Navigator.of(context).pop();
                                  },
                                  child: Text("DELETE"),
                                ),
                              ],
                            ),
                          );
                        });
                  },
                )
              : null,
          title: new Text(
            fullname,
            style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 14.0),
          ),
          subtitle: new Text(
            comments,
            style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 10.0),
          ),
        ),
        Divider(),
      ],
    ));

    return center;
  }

  void deleteComment() {
    print("delete here");
  }
}
*/
