import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carem8/auth.dart';

import 'package:carem8/models/user.dart';
import 'package:carem8/pages/calendar/events.dart';
import 'package:carem8/pages/childrenmenu.dart';
import 'package:carem8/pages/commondrawer.dart';
import 'package:carem8/pages/home/dailysummarydatalist.dart';
import 'package:carem8/pages/home/data/config.dart';

import 'package:carem8/pages/home/home.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:carem8/pages/newsfeed/newsfeeddata.dart';
import 'package:carem8/utils/commonutils/emptybody.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/fromjson/jsonObj.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:carem8/Theme.dart' as Theme;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/scheduler.dart';

class NewsFeedList extends StatefulWidget {
  NewsFeedList({this.childrenData});

  final List<ChildViewModal> childrenData;

  @override
  State<StatefulWidget> createState() {
    return new NewsFeedListState();
  }
}

class NewsFeedListState extends State<NewsFeedList>
    implements HomePageContract, AuthStateListener {
  BuildContext _ctx;
  var appuser, jwt, id, client_id, children, users, newsfeedSectionConfig;
  var email, password;
  var load = true;
  bool isLoading;
  HomePagePresenter _presenter;
  var data;
  var k, newsfeedData;
  ScrollController controller;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  NewsFeedListState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }


  //TODO: where's my readability: cannot divide for now bcuz of setState
  void getGlobalConfig() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String savedClientConfig = prefs.getString('carem8_globalConfig_clientSection') ?? null;
    final decodedClientConfigs = json.decode(savedClientConfig);
    String savedNavigationConfig = prefs.getString("carem8_globalConfig_navigationSet") ?? null;
    final decodedNavigationConfigs = json.decode(savedNavigationConfig);
    String savedProgramConfig = prefs.getString('carem8_globalConfig_programPlanSection') ?? null;
    final decodedProgramConfig = json.decode(savedProgramConfig);
    String savedNotificationConfig = prefs.getString("carem8_globalConfig_notificationSection") ?? null;
    final decodedNotificationConfig = json.decode(savedNotificationConfig);
    String savedPrivateMessageConfig = prefs.getString("carem8_globalConfig_privateMessageSection") ?? null;
    final decodedPrivateMessageConfig = json.decode(savedPrivateMessageConfig);
    String savedChildProfileConfig = prefs.getString("carem8_globalConfig_childProfileSection") ?? null;
    final decodedChildProfileConfig = json.decode(savedChildProfileConfig);
    String savedStaffDocumentsSection = prefs.getString("carem8_globalConfig_StaffDocumentsSection") ?? null;
    final decodedStaffDocumentsSection = json.decode(savedStaffDocumentsSection);
//    String savedMedicationSection = prefs.getString("carem8_globalConfig_medicationSection") ?? null;
//    final decodedMedicationSection = json.decode(savedMedicationSection);
//    String savedNonPrescribedMedicationSection = prefs.getString("carem8_globalConfig_nonPrescribedMedicationSection") ?? null;
//    final decodedNonPrescribedMedicationSection = json.decode(savedNonPrescribedMedicationSection);
//    String savedCenterManagementSection = prefs.getString("carem8_globalConfig_centerManagementJournalSection");
//    final decodedCenterManagementJournalSection = json.decode(savedCenterManagementSection);
//    String savedCenterManagementJourneySection = prefs.getString("carem8_globalConfig_centerManagementJournalSection");
//    final decodedCenterManagementJourneySection = json.decode(savedCenterManagementJourneySection);
    String savedNewsFeedSection = prefs.getString("carem8_globalConfig_newsFeedSection");
    final decodedNewsFeedSection = json.decode(savedNewsFeedSection);
    String savedNewsFeedDetail = prefs.getString("carem8_globalConfig_newsFeedDetailLabel");
    final decodedNewsFeedDetailLabel = json.decode(savedNewsFeedDetail);
    String savedAppSettings = prefs.getString("carem8_globalConfig_appSettings");
    final decodedAppSettings = json.decode(savedAppSettings);

    //If upcoming configs are null or if there is error, I should show default labels again.
    Map<String, String> backup = labelsConfig;
    Map<String, dynamic> appSettingsBackup = appSettingsConfig;


    setState(() {
      //client config
      labelsConfig["dailySummaryLabel"] = decodedClientConfigs["nav_client_daily_summary"] ?? backup["dailySummaryLabel"];
      labelsConfig["learningStoryLabel"] = decodedClientConfigs["nav_client_photos_story_tab"] ?? backup["learningStoryLabel"];
      labelsConfig["dailyJournalsLabel"] = decodedClientConfigs["nav_client_journal"] ?? backup["dailyJournalsLabel"];
      labelsConfig["observationLabel"] = decodedClientConfigs["nav_client_journey"] ?? backup["observationLabel"];
      labelsConfig["dailyChartLabel"] = decodedClientConfigs["nav_client_dailychart"] ?? backup["dailyChartLabel"];
      labelsConfig["medicationLabel"] = decodedClientConfigs["dashboard_notifications_medication"] ?? backup["medicationLabel"];
      labelsConfig["noteToCenterLabel"] = decodedClientConfigs["nav_centermanagement_remarks_tab_label_s"] ?? backup["noteToCenterLabel"];
      labelsConfig["photosLabel"] = decodedClientConfigs["nav_client_photos"] ?? backup["photosLabel"];
      labelsConfig["calendarLabel"] = decodedClientConfigs["nav_client_events"] ?? backup["calendarLabel"];
      labelsConfig["eventsLabel"] = decodedClientConfigs["nav_client_events_tab"] ?? backup["eventsLabel"];
      labelsConfig["journalsLabel"] = decodedClientConfigs["nav_client_mobile_journal"] ?? backup["journalsLabel"];

      //newsfeed config
      labelsConfig["newsFeedLabel"] = decodedNewsFeedSection ?? backup["newsFeedLabel"];
      labelsConfig["newsFeedDetailLabel"] = decodedNewsFeedDetailLabel ?? backup["newsFeedDetailLabel"];

      //programs config
      labelsConfig["programsLabel"] = decodedProgramConfig["label_program_plan_client_list_tab_heading"] ?? backup["programsLabel"];
      labelsConfig["programSubjectLabel"] = decodedProgramConfig["label_program_plan_subjectarea"] ?? backup["programSubjectLabel"];
      labelsConfig["programTypeLabel"] = decodedProgramConfig["label_program_plan_type"] ?? backup["programTypeLabel"];
      labelsConfig["programLearningOutcomeLabel"] = decodedProgramConfig["label_program_plan_learningtags"] ??backup["programLearningOutcomeLabel"];
      labelsConfig["programStartDate"] = decodedProgramConfig["label_program_plan_starting_date"] ??backup["programStartDate"];
      labelsConfig["programEndDate"] = decodedProgramConfig["label_program_plan_ending_date"] ?? backup["programEndDate"];

      //navigation_set configs
      labelsConfig["newsletterLabel"] = decodedNavigationConfigs["nav_newsletter"] ?? backup["newsletterLabel"];
      labelsConfig["generalFormsLabel"] = decodedNavigationConfigs["nav_generalforms"] ?? backup["generalFormsLabel"];
      labelsConfig["policiesLabel"] = decodedNavigationConfigs["nav_policies"] ?? backup["policiesLabel"];
      labelsConfig["settingsLabel"] = decodedNavigationConfigs["nav_user_settings"] ?? backup["settingsLabel"];
      labelsConfig["userSettingsLabel"] = decodedNavigationConfigs["nav_user_settings"] ?? backup["userSettingsLabel"];

      //notification config
      labelsConfig["notificationsLabel"] = decodedNotificationConfig ?? backup["notificationsLabel"];

      //privateMessage config
      labelsConfig["privateMessageLabel"] = decodedPrivateMessageConfig["nav_messageportal_active_log_label"] ?? backup["privateMessageLabel"];
      labelsConfig["composeMessageLabel"] = decodedPrivateMessageConfig["nav_messageportal_button_new"] ?? backup["composeMessageLabel"];
      labelsConfig["messageLabel"] = decodedPrivateMessageConfig["nav_messageportal_label"] ?? backup["messageLabel"];

      //childProfile config
      labelsConfig["childProfileLabel"] = decodedChildProfileConfig ?? backup["childProfileLabel"];

      appSettingsConfig["enable_master_nav_learning_story"] = decodedAppSettings["enable_master_nav_learning_story"] ?? appSettingsBackup["enable_master_nav_learning_story"];
      appSettingsConfig["enable_manad_plus_integration"] = decodedAppSettings["enable_manad_plus_integration"] ?? appSettingsBackup["enable_master_nav_learning_story"];
      appSettingsConfig["manad_storge_aws_baseurl"] = decodedAppSettings["manad_storge_aws_baseurl"] ?? appSettingsBackup["manad_storge_aws_baseurl"];
      print("manad_storage ${appSettingsConfig["manad_storge_aws_baseurl"]} manad_enabled ${appSettingsConfig["enable_manad_plus_integration"]}");
    });
  }

  Future<String> fetchNewsFeedData(int s) async {
    ///data from GET method
    print("data fetched");

    String _newsfeedUrl =
        "https://apicare.carem8.com/carem8/v2.1.1/newsfeed/$id?step=$s&clientid=$client_id";
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_newsfeedUrl, headers: headers).then((response) {
      print('res get ${response.body}');
      try {
        newsfeedData = json.decode(response.body);
      } catch (ex) {
        print(ex);
        newsfeedData = [];
        return;
      }
      print('map $newsfeedData');

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(isLoading);
        isLoading = false;
        if (s == 0) {
          setState(() {
            load = false;
            this.data = newsfeedData;
          });
        } else {
          setState(() {
            load = false;
            data.addAll(newsfeedData);
          });
        }
        k = data.length;
      } else if (response.statusCode == 500 &&
          newsfeedData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        print("other errors");
        fetchNewsFeedData(0);
      }
    });
    return null;
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$client_id';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken = json.decode(response.body);
      this.jwt = refreshJwtToken;

      if (k != null) {
        fetchNewsFeedData(k);
      } else {
        fetchNewsFeedData(0);
      }
    });
  }

  @override
  void initState() {
    isLoading = false;
    getGlobalConfig();
//    controller = new ScrollController()..addListener(_scrollListener);
    super.initState();
  }

  @override
  void dispose() {
//    controller = new ScrollController()..removeListener(_scrollListener);
    super.dispose();
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appdarkcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );
///
///
  void _openchildrenpopup() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return ChildrenDrawer(widget.childrenData);
        },
        fullscreenDialog: true));
  }
///
///

  // Pop to ask do you wish to exit
  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          child: new AlertDialog(
            title: new Text('Are you sure?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => exit(0),
//                    Navigator.pushReplacementNamed(context, "/home"),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ?? false;
	///
	///
  }

  void _modalBottomSheetMenu(BuildContext context){
    showModalBottomSheet(
        context: context,
        builder: (builder){
          return new Container(
            height: 100.0,
            color: Colors.transparent,
            child: new Container(
                decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(10.0),
                        topRight: const Radius.circular(10.0))),
                child: new Center(
                  child: ChildrenDrawer(widget.childrenData),
                )),
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(labelsConfig["newsFeedLabel"],
            style: TextStyle(
              color: Colors.white,
            )),
        backgroundColor: Theme.Colors.appdarkcolour,
        centerTitle: true,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              tooltip: 'Refresh',
              onPressed: () {
                print("Reload..");
                setState(() {
                  k = 0;
                  load = true;
                });
                fetchNewsFeedData(0);
              })
        ],
      ),
      drawer: CommonDrawer(childrenData: widget.childrenData),
      body: WillPopScope(
        onWillPop: _onWillPop,
        child: new RefreshIndicator(
          key: refreshIndicatorKey,
          onRefresh: handleRefresh,
          child: new Center(
            child: load
                ? progress
                : data.length > 0
                    ? new ListView.builder(addAutomaticKeepAlives: true,
                        controller: controller,
                        itemCount:
                            this.data != null ? (this.data.length + 1) : 0,
                        itemBuilder: (context, i) {
                          if (i == k) {
                            if (newsfeedData.length < 5) {
                              return Container();
                            } else {
                              return _buildCounterButton();
                            }
                          } else {
                            final singleData = this.data[i];
                            return Data(singleData, users, jwt,);
                          }
                        },
                      )
                    : EmptyBody("No Newsfeeed yet."),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.menu),
        backgroundColor: Theme.Colors.appcolour,
        onPressed: () {
            _modalBottomSheetMenu(context);
          },
        ),
        bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          notchMargin: 4.0,
          child: new Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
//              new FlatButton(
//                onPressed: () {
////                Navigator.pushNamed(context, "/home");
//                Navigator.pushReplacement(
//                    context,
//                    new MaterialPageRoute(
//                        builder: (context) => new DailySummary(childrenData: widget.childrenData,)));
//              },
//              padding: EdgeInsets.all(10.0),
//              child: new Column(
//                mainAxisSize: MainAxisSize.min,
//                children: <Widget>[
//                  new Icon(Icons.format_list_bulleted),
////                  new Text("Dailysummary")
//                ],
//              ),
//            ),
            new FlatButton(
              onPressed: () {
//                Navigator.pushNamed(context, "/newsfeed");
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new NewsFeedList(childrenData: widget.childrenData,)));
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.chrome_reader_mode),
//                  new Text("Newsfeed")
                ],
              ),
            ),
            new FlatButton(
              onPressed: () {
//                EventsData(childrenData: widget.childrenData,);
//                Navigator.pushReplacementNamed(context, "/calendar");
                Navigator.pushReplacement(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new EventsData(childrenData: widget.childrenData,)));
              },
              padding: EdgeInsets.all(10.0),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Icon(Icons.date_range),
//                  new Text("Calendar")
                ],
              ),
            ),
//            new FlatButton(
//              onPressed: () {
//                CommonDrawer();
////                Navigator.push(context, new MaterialPageRoute(
////                    builder: (context) =>
////                    new EventsList())
////                );
//              },
//              padding: EdgeInsets.all(10.0),
//              child: new Column(
//                mainAxisSize: MainAxisSize.min,
//                children: <Widget>[
//                  new Icon(Icons.free_breakfast),
////                  new Text("Family ..")
//                ],
//              ),
//            ),
//            IconButton(
//              icon: Icon(Icons.format_list_bulleted),
//              onPressed: () {
//                Dailysummary();
//              },
//            ),
//            IconButton(
//                icon: Icon(Icons.chrome_reader_mode),
//                onPressed: () {
//                  NewsfeedList();
//                },
//            ),
//            IconButton(
//              icon: Icon(Icons.date_range),
//              onPressed: () {
//                EventsList();
////                showModalBottomSheet<Null>(
////                  context: context,
////                  builder: (BuildContext context) => const _DemoDrawer(),
////                );
//              },
//            ),
//            IconButton(
//              icon: Icon(Icons.menu),
//              onPressed: () {
//                CommonDrawer();
////                showModalBottomSheet<Null>(
////                  context: context,
////                  builder: (BuildContext context) => CommonDrawer(),
////                );
//              },
//            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: isLoading
            ? new CupertinoActivityIndicator()
            : const Text('Load more...',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: Theme.Colors.app_white)),
        color: Theme.Colors.appcolour.withOpacity(0.75),
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
        });
        fetchNewsFeedData(k);
      };
    }
  }

  Future<Null> handleRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      k = 0;
      load = true;
      fetchNewsFeedData(0);
    });
    return null;
  }

  @override
  void onDisplayUserInfo(User user) {
    SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
      password = user.password;
      appuser = user.center;
      children = user.children;

      print("children $children");
      print("appuser $appuser");

      try {
        final parsed = json.decode(appuser);
        var appusers = parsed[0];
        print(appusers);
        print("******${appusers["jwt"]}");
        jwt = appusers["jwt"];
        users = appusers["user"];
        var appConfig = appusers["globalconfig"];

        var child = appusers["children"];

        client_id = users["client_id"];

        id = users["id"];
        email = users["email"];

        fetchNewsFeedData(0);
      } catch (e) {
        print(e);
      }
    }));
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
  }

  @override
  void onAuthStateChanged(AuthState state) {
    print("Navigate");
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(_ctx)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
  }
}
