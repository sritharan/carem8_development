import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carem8/pages/newsfeed/newsfeedlist.dart';
import 'package:carem8/pages/newsfeed/viewcomment.dart';
import 'package:carem8/pages/newsfeed/viewlike.dart';
import 'package:carem8/Theme.dart' as Kinderm8Theme;

class Comment extends StatefulWidget {
  final data;
  final user;
  final String jwt;
  final int i;
  final Function commentCountFunction;
  final Function decreaseCommentCountFunction;

  Comment(this.data, this.user, this.jwt, this.i, {this.commentCountFunction, this.decreaseCommentCountFunction});

  @override
  CommentState createState() => CommentState(data, user, jwt);
}

class CommentState extends State<Comment> with SingleTickerProviderStateMixin {
  final data;
  final user;
  final String jwt;

  TabController tabController;

  CommentState(this.data, this.user, this.jwt);

  @override
  void initState() {
    tabController =
        new TabController(length: 2, vsync: this, initialIndex: widget.i);
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Kinderm8Theme.Colors.appdarkcolour,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.close),
              tooltip: 'close',
              onPressed: () {
                Navigator.of(context).pop();
              })
        ],
        bottom: TabBar(
          controller: tabController,
          tabs: <Widget>[
            new Tab(
//              text: "COMMENTS",
//              child: Text("COMMENTS",style: new TextStyle(
//                  color: Kinderm8Theme.Colors.logintext, fontSize: 18.0),),
              icon: Icon(Icons.comment),
            ),
            new Tab(
//              text: "LIKES",
//              child: Text("LIKES"),
              icon: Icon(Icons.favorite),
            )
          ],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: <Widget>[
          ViewComment(data, user, jwt, commentCountFunction: widget.commentCountFunction, decreaseCommentCountFunction: widget.decreaseCommentCountFunction),
          ViewLike(data, user, jwt)
        ],
      ),
    );
  }
}
