import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:http/http.dart' as http;



class NewsfeedAPI {
  List id, client_id, center, image = List();

  static NewsfeedAPI _instance = new NewsfeedAPI.internal();
  NewsfeedAPI.internal();
  factory NewsfeedAPI() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();

  Future<dynamic> get(String url) {
    return http.get(url).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
      return _decoder.convert(res);
    });
  }


  Future<dynamic> post(String url, {Map headers, body, encoding}) async {
    print(body);
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/x-www-form-urlencoded');
    request.add(utf8.encode(json.encode(body)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    ///    Single client
    if (response.statusCode == 200) {
      var reply = await response.transform(utf8.decoder).join();
//      List decodedList = json.decode(reply);
//      print(decodedList.length);
//      print("******$reply");
      httpClient.close();
      return reply;
    }
    ///     Multiple client
    else if (response.statusCode == 202) {
      var reply = await response.transform(utf8.decoder).join();
//      List decodedList = json.decode(reply);
//      print(decodedList.length);
      httpClient.close();
      return reply;
    } else {
      print("error");
      return null;
    }
  }
}
