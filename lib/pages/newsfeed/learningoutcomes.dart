import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:carem8/Theme.dart' as Kinderm8Theme;
import 'package:html2md/html2md.dart' as html2md;

class LearningOutcomes extends StatelessWidget {
  final learningOutcomeDetail;
  LearningOutcomes(this.learningOutcomeDetail) {
    groupLearningOutComes();
  }
  var title, description;
  var learning_set_title = "";

  List learningSetTitle = new List();

  void groupLearningOutComes() {
    for (int i = 0; i < learningOutcomeDetail.length; i++) {
      if (learning_set_title ==
          learningOutcomeDetail[i]["learning_set_title"]) {
      } else {
        learning_set_title = learningOutcomeDetail[i]["learning_set_title"];

        learningSetTitle.add(learningOutcomeDetail[i]["learning_set_title"]);
      }
    }
    print("learningSetTitle______- $learningSetTitle");
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Learning Outcomes"),
        centerTitle: true,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.close),
              tooltip: 'close',
              onPressed: () {
                Navigator.of(context).pop();
              })
        ],
      ),
      body: Container(
          padding: EdgeInsets.all(10.0),
          child: new ListView.builder(
            itemCount: learningSetTitle.length,
            itemBuilder: (context, i) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "${learningSetTitle[i]}",
                        style: TextStyle(
                            color: Kinderm8Theme.Colors.app_blue[300],
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Container(height: 8.0),
                      ListView.builder(
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        itemCount: learningOutcomeDetail.length,
                        itemBuilder: (context, j) {
                          var description;
                          if (learningOutcomeDetail[j]["description"] != null) {
                            description = html2md.convert(
                                learningOutcomeDetail[j]["description"]);
                          }

                          for (int k = 0;
                              k < learningOutcomeDetail.length;
                              k++) {
                            return Padding(
                              padding: EdgeInsets.all(0.0),
                              child: learningSetTitle[i] ==
                                      learningOutcomeDetail[j]
                                          ["learning_set_title"]
                                  ? Card(
                                      elevation: 4.0,
                                      margin: EdgeInsets.all(2.0),
                                      child: Container(
                                        padding: EdgeInsets.all(10.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              learningOutcomeDetail[j]["title"],
                                              style: TextStyle(
                                                  color: Kinderm8Theme
                                                      .Colors.app_dark[400],
                                                  fontSize: 13.0,
                                                  fontWeight: FontWeight.w600),
                                            ),
                                            learningOutcomeDetail[j]
                                                        ["description"] !=
                                                    null
                                                ? Html(
                                                    data: description,
                                                    defaultTextStyle: TextStyle(
                                                      color: Kinderm8Theme
                                                          .Colors.app_dark[200],
                                                      fontSize: 12.0,
                                                    ),
                                                  )
                                                /*Text(
                                                description,
                                                    style: TextStyle(
                                                      color: Kinderm8Theme
                                                          .Colors.app_dark[200],
                                                      fontSize: 12.0,
                                                    ))*/
                                                : Text(""),
                                          ],
                                        ),
                                      ))
                                  : null,
                            );
                          }
                        },
                      )
                    ],
                  ),
                  Container(
                    height: 10.0,
                  )
                ],
              );
            },
          )),
    );
  }
}

//
//void groupLearningOutComes() {
//  for (int i = 0; i < learningOutcomeDetail.length; i++) {
//    if (learning_set_title ==
//        learningOutcomeDetail[i]["learning_set_title"]) {
////        print("learning_set_title $learning_set_title");
//
//    } else {
//      print("______learning_set_title$i $learning_set_title");
//      learning_set_title = learningOutcomeDetail[i]["learning_set_title"];
//      print("learning_set_title$i $learning_set_title");
//
//      learningSetTitle.add(learningOutcomeDetail[i]["learning_set_title"]);
//    }
//  }
//  print("learningSetTitle______- $learningSetTitle");
//}
//
//learningOutcomes() {
//  for (int j = 0; j < learningSetTitle.length; j++) {
//    return Container(
//      width: 350.0,
//      height: 350.0,
//      child: Column(children: <Widget>[
////          Text("${learningSetTitle[j]}"),
//        new ListView.builder(
//          itemCount: learningOutcomeDetail.length,
//          itemBuilder: (context, i) {
//            if (learningSetTitle[j] ==
//                learningOutcomeDetail[i]["learning_set_title"]) {
//              print(learningSetTitle[j]);
//              print(learningOutcomeDetail[i]["title"]);
//              print(learningOutcomeDetail[i]["description"]);
//
////                return Column(
////                  crossAxisAlignment: CrossAxisAlignment.start,
////                  children: <Widget>[
////                    Text("${learningOutcomeDetail[i]["title"]}"),
////                    Text("${learningOutcomeDetail[i]["description"]}"),
////                    Divider(),
////                  ],
////                );
//            }
//
////          return Column(
////            crossAxisAlignment: CrossAxisAlignment.start,
////            children: <Widget>[
//////                Text("aHi")
////              Text("${learningOutcomeDetail[i]["learning_set_title"]}"),
////              Text("${learningOutcomeDetail[i]["title"]}"),
////              Text("${learningOutcomeDetail[i]["description"]}"),
////              Divider(),
////            ],
////          );
//          },
//        )
//      ]),
//    );
//  }
//}
//
//@override
//Widget build(BuildContext context) {
//  return Scaffold(
//    appBar: AppBar(
//      automaticallyImplyLeading: false,
//      title: Text("Learning Outcomes"),
//      centerTitle: true,
//      actions: <Widget>[
//        new IconButton(
//            icon: new Icon(Icons.close),
//            tooltip: 'close',
//            onPressed: () {
//              Navigator.of(context).pop();
//            })
//      ],
//    ),
//    body: Container(child: learningOutcomes()
//
//      /*new ListView.builder(
//        itemCount: learningOutcomeDetail.length,
//        itemBuilder: (context, i) {
//          for (int j = 0; j < learningSetTitle.length; j++) {
//            if (learningSetTitle[j] ==
//                learningOutcomeDetail[i]["learning_set_title"]) {
//              print(learningSetTitle[j]);
//              print(learningOutcomeDetail[i]["title"]);
//              print(learningOutcomeDetail[i]["description"]);
//
//              return Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
////                Text("aHi")
//                  Text("${learningSetTitle[j]}"),
//                  Text("${learningOutcomeDetail[i]["title"]}"),
//                  Text("${learningOutcomeDetail[i]["description"]}"),
//                  Divider(),
//                ],
//              );
//            }
//          }
//
////          return Column(
////            crossAxisAlignment: CrossAxisAlignment.start,
////            children: <Widget>[
//////                Text("aHi")
////              Text("${learningOutcomeDetail[i]["learning_set_title"]}"),
////              Text("${learningOutcomeDetail[i]["title"]}"),
////              Text("${learningOutcomeDetail[i]["description"]}"),
////              Divider(),
////            ],
////          );
//        },
//      )*/
//
//    ),
//  );
//}
