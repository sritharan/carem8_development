import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carem8/utils/network_util.dart';

class ViewLike extends StatefulWidget {
  final data;
  final user;
  final String jwt;
  ViewLike(this.data, this.user, this.jwt);
  @override
  ViewLikeState createState() => ViewLikeState(data, user, jwt);
}

class ViewLikeState extends State<ViewLike> {
  final data, user;
  String jwt;
  var likeData, cliId, user_id;
  bool load = false;
  var image, fullname, like;
  ViewLikeState(this.data, this.user, this.jwt) {
    getLikeData();
  }

  getLikeData() {
    print("getLikeData");
    var postfeed_id = data["id"];
    cliId = user["client_id"];
    user_id = user["id"];
    String getLikeUrl =
        'https://apicare.carem8.com/v2.1.0/newsfeedlike/$postfeed_id/$user_id?clientid=$cliId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(getLikeUrl, headers: headers).then((response) {
      print('res get ${response.body}');
      likeData = json.decode(response.body);

      print('map $likeData');

      print('jwt $jwt');
      print(response.statusCode);

      if (response.statusCode == 200) {
        like = likeData["user"];
        print(like);
        setState(() {
          load = true;
        });
      } else if (response.statusCode == 500 &&
          likeData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      }
    });
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$user_id&clientid=$cliId';

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      getLikeData();
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return likeData != null
        ? new Container(
            child: Column(
              children: <Widget>[
                new Expanded(
                    child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: new ListView.builder(
                        scrollDirection: Axis.vertical,
//                        shrinkWrap: true,
//                        physics: ClampingScrollPhysics(),
                        itemCount: this.like != null ? (this.like.length) : 0,
                        itemBuilder: (context, i) {
                          return ShowLike(like[i]);
                        },
                      ),
//                      height: screenSize.height-130.0,
//                      width: screenSize.width,
                    )
                  ],
                )),
              ],
            ),
          )
        : CupertinoActivityIndicator();
  }
}

class ShowLike extends StatelessWidget {
  final like;
  ShowLike(this.like);

  @override
  Widget build(BuildContext context) {
    var image, fullname, center;
    image = like["image"];
    fullname = like["fullname"];
    center = new Center(
        child: Column(
      children: <Widget>[
        new Padding(
          padding: EdgeInsets.only(top: 10.0),
        ),
        new ListTile(
          leading: image != null
              ? CircleAvatar(
                  backgroundImage: NetworkImage(image, scale: 10.0),
                  radius: 25.0,
                )
              : CircleAvatar(
                  backgroundImage: AssetImage("assets/nophoto.jpg"),
                  radius: 25.0,
                ),
          title: new Text(
            fullname,
            style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 15.0),
          ),
        ),
        Divider(),
      ],
    ));

    return center;
  }
}
