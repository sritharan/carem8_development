import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:carem8/pages/newsfeed/detailedview.dart';
import 'package:carem8/utils/network_util.dart';
//import 'author.dart';

class NotificationData extends StatelessWidget {
  final data;
  int i;

  NotificationData(this.data, this.i);

//
//  Future<String> viewNotificationData(int s) async {
//
//    ///data from GET method
//    print("data fetched");
//
//    String viewNotificationUrl =
//        'http://13.210.72.124:7070/v2.1.0/notification/getdetailsnotificationoneapp?userid=$id&step=$s&clientid=$clientId';
//    var headers = {"x-authorization": jwt.toString()};
//
//    NetworkUtil _netutil = new NetworkUtil();
//    _netutil.get(_notificationUrl, headers: headers).then((response) {
//      var notificationData;
//      try {
//        notificationData = json.decode(response.body);
//        print(notificationData.length);
//        print('res get ${response.body}');
//        print('notificationData $notificationData');
//      } catch (e) {
//        print('That string was null!');
//      }
//
//      print('jwt### $jwt');
//      print(response.statusCode);
//      if (response.statusCode == 200) {
//        print(isLoading);
//        isLoading = false;
//        print(isLoading);
//        if (s == 0) {
//          setState(() {
//            load = false;
//            this.data = notificationData;
//          });
//        } else {
//          setState(() {
//            load = false;
//            data.addAll(notificationData);
//          });
//        }
//        k = data.length;
//      } else if (response.statusCode == 500 &&
//          notificationData["errorType"] == 'ExpiredJwtException') {
//        print("retrying...");
//        getRefreshToken();
//      } else {
//        fetchNotificationData(0);
//      }
//    });
//    return null;
//  }

  @override
  Widget build(BuildContext context) {
    var parsedDate = DateTime.parse(data["created_at"]);
    var date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);
    var sender = data["sender"];
//    print(sender["image"]);

    var center = new Center(
        child: Column(
      children: <Widget>[
        new ListTile(
          leading: sender["image"] != null
              ? CircleAvatar(
                  backgroundImage: NetworkImage(sender["image"], scale: 10.0),
                  radius: 25.0,
                )
              : CircleAvatar(
                  backgroundImage: AssetImage("assets/nophoto.jpg"),
                  radius: 25.0,
                ),
          title: new Text(
            data['notification'],
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 14.0),
          ),
          subtitle: Container(
            padding: EdgeInsets.only(bottom: 3.0),
            child: new Text(
              date,
              style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 10.0),
            ),
          ),
          onTap: () {
            print(data["id"]);
            print("Have to navigate to ${data["navigator"]}");
          },
        ),
        Divider(),
      ],
    ));

    return center;
  }
}
