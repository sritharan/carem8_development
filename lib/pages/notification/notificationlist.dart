import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'dart:convert';

import 'package:carem8/Theme.dart' as Theme;
import 'package:carem8/pages/calendar/detailevent.dart';
import 'package:carem8/pages/calendar/detailprogram.dart';
import 'package:carem8/pages/centrenotes/centrenoteslist.dart';
import 'package:carem8/pages/commondrawer.dart';
import 'package:carem8/pages/dailyjournal/detailedjournalpage.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/newsfeed/detailedview.dart';
import 'package:carem8/pages/notification/notificationdata.dart';
import 'package:carem8/pages/observation/detailedobservationpage.dart';
import 'package:carem8/utils/commonutils/emptybody.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';

class Notifications extends StatefulWidget {
  Notifications({this.childrenData});

  final List<ChildViewModal> childrenData;
  @override
  NotificationsState createState() => NotificationsState();
}

class NotificationsState extends State<Notifications>
    implements HomePageContract {
  List data;
  var k, appuser, jwt, id, clientId, users;
  var notificationId, sourceId, navigator;
  bool isLoading;
  bool load = true;
  bool loading = true;
  var notificationData;
  HomePagePresenter _presenter;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  NotificationsState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  Future<String> fetchNotificationData(int s) async {
    ///data from GET method
    String _notificationUrl =
        'https://apicare.carem8.com/v2.1.0/notification/getdetailsnotificationoneapp?userid=$id&step=$s&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_notificationUrl, headers: headers).then((response) {
      try {
        notificationData = json.decode(response.body);

        if (response.statusCode == 200) {
          isLoading = false;
          if (s == 0) {
            setState(() {
              load = false;
              this.data = notificationData;
            });
          } else {
            setState(() {
              load = false;
              data.addAll(notificationData);
            });
          }
          k = data.length;
        } else if (response.statusCode == 500 &&
            notificationData["errorType"] == 'ExpiredJwtException') {
          getRefreshToken();
        } else {
          print("notificationData errorType ${notificationData["errorType"]}");
        }
      } catch (e) {
        print('That string was null!');
      }
    });
    return null;
  }

  Future<String> viewNotificationData() {
    ///data from GET method
    String viewNotificationUrl;

    viewNotificationUrl =
        'https://apicare.carem8.com/v2.1.1/notification/$navigator?id=$sourceId&clientid=$clientId&notification_id=$notificationId&userid=$id';

    var headers = {"x-authorization": jwt.toString()};
    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(viewNotificationUrl, headers: headers).then((response) {
      var sourceData;
      try {
        sourceData = json.decode(response.body);
        print(sourceData);
        if (response.statusCode == 200) {
          if (navigator == 'journal') {
            Navigator.of(context).push(new MaterialPageRoute<Null>(
              builder: (BuildContext context) {
                return JournalDetails(sourceData, jwt);
              },
            ));
          } else if (navigator == 'journey') {
            Navigator.of(context).push(new MaterialPageRoute<Null>(
              builder: (BuildContext context) {
                return ObservationDetails(sourceData, jwt);
              },
            ));
          } else if (navigator == 'newsfeed') {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) =>
                        new NewsfeedDetails(sourceData, users, jwt)));
          } else if (navigator == 'event') {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => new EventDetails(sourceData,jwt)));
          } else if (navigator == 'program') {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => new ProgramDetails(sourceData)));
          } else if (navigator == 'childnote') {
            print("child dataa ${sourceData['child']}");
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) =>
                        new CentreNotesList(ChildViewModal(id: sourceData['child']['id']), jwt)));
          }
          setState(() {
            load = false;
          });
        } else if (response.statusCode == 500 &&
            sourceData["errorType"] == 'ExpiredJwtException') {
          getRefreshToken();
        } else {
          print('error ${sourceData["errorType"]}');
        }
      } catch (e) {
        print('That string was null!');
      }
    });
    return null;
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
//        lastly wanna check;
//        _presenter.updateJwt(refreshJwtToken);
      } catch (e) {
//        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (k != null) {
        fetchNotificationData(k);
      } else {
        fetchNotificationData(0);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(labelsConfig["notificationsLabel"],
            style: TextStyle(
              color: Colors.white,
            )),
//        backgroundColor: Color(0xFF31adbd),
        backgroundColor: Theme.Colors.appdarkcolour,

        centerTitle: true,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              tooltip: 'Refresh',
              onPressed: () {
                setState(() {
                  k = 0;
                  load = true;
                });
                fetchNotificationData(0);
              })
        ],
      ),
      drawer: CommonDrawer(childrenData: widget.childrenData,),
      body: new RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: handleRefresh,
        child: new Center(
          child: load
              ? progress
              : data.length > 0
                  ? new ListView.builder(
                      itemCount: this.data != null ? (this.data.length + 1) : 0,
                      itemBuilder: (context, i) {
                        if (i == k) {
                          if (notificationData.length < 5) {
                            return Container();
                          } else {
                            return _buildCounterButton();
                          }
                        } else {
                          final singleNotificationData = this.data[i];

                          var parsedDate = DateTime.parse(
                              singleNotificationData["created_at"]);
                          var date = new DateFormat.yMMMMEEEEd()
                              .add_jm()
                              .format(parsedDate);
                          var sender = singleNotificationData["sender"];

                          var center = new Center(
                              child: Column(
                            children: <Widget>[
                              new ListTile(
                                leading: sender["image"] != null
                                    ? CircleAvatar(
                                        backgroundImage: NetworkImage(
                                            sender["image"],
                                            scale: 10.0),
                                        radius: 25.0,
                                      )
                                    : CircleAvatar(
                                        backgroundImage:
                                            AssetImage("assets/nophoto.jpg"),
                                        radius: 25.0,
                                      ),
                                title: new Text(
                                  singleNotificationData['notification'],
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14.0),
                                ),
                                subtitle: Container(
                                  padding: EdgeInsets.only(bottom: 3.0),
                                  child: new Text(
                                    date,
                                    style: new TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 10.0),
                                  ),
                                ),
                                onTap: () {
                                  notificationId = singleNotificationData['id'];
                                  sourceId =
                                      singleNotificationData['source_id'];
                                  navigator =
                                      singleNotificationData['navigator'];

                                  print(notificationId);
                                  print(sourceId);
                                  print("Have to navigate to $navigator");
                                  if (navigator == 'journal' ||
                                      navigator == 'journey' ||
                                      navigator == 'newsfeed' ||
                                      navigator == 'event' ||
                                      navigator == 'childnote' ||
                                      navigator == 'program') {
                                    viewNotificationData();
                                    setState(() {
                                      load = true;
                                    });
                                  }
                                },
                              ),
                              Divider(),
                            ],
                          ));

                          return center;

//                      return NotificationData(singleNotificationData, i);
                        }
                      },
                    )
                  : EmptyBody("No Notifications yet."),
        ),
      ),
    );
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: isLoading
            ? new CupertinoActivityIndicator()
            : const Text('Load more...',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: Theme.Colors.app_white)),
        color: Theme.Colors.appcolour.withOpacity(0.75),
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
        });
        fetchNotificationData(k);
      };
    }
  }

  Future<Null> handleRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      load = true;
      k = 0;
      fetchNotificationData(0);
    });
    return null;
  }

//  Future<String> getnotificationdata(int id, int step, String clientid) async {
//    var appuser, jwt, id, client_id;
//    var url =
//        "http://13.210.72.124:7070//v2.1.0/notification/getdetailsnotificationoneapp?userid=$id&step=$step&clientid=$clientid";
//    var response =
//        await http.get(Uri.encodeFull(url), headers: {"x-authorization": jwt});
//    setState() {
//      var convertedNotificationdata = jsonDecode(response.body);
//      return 'success';
//    }
//  }

  /// from - inclusive, to - exclusive
  /*Future<List<int>> fakeRequest(int from, int to) async {
    return Future.delayed(Duration(seconds: 2), () {
      return List.generate(to - from, (i) => i + from);
    });
  }*/

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    print(user.center);
    print(user.children);
    print(user.email);
    print(user.password);
    print(user.client_id);

    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = appusers["jwt"];
      print("******$jwt");
      users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

//      print("iddd $id");
//      print(clientId);
      fetchNotificationData(0);
    } catch (e) {
//      print(e);
//      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
