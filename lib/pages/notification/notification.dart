import 'dart:convert';
import 'package:meta/meta.dart';

class Notifications {
  Notifications({
    @required this.avatar,
    @required this.notification,
    @required this.createdat,
    @required this.email,
  });

  final String avatar;
  final String notification;
  final String createdat;
  final String email;

  static List<Notifications> allFromResponse(String response) {
    var decodedJson = json.decode(response).cast<String, dynamic>();

    return decodedJson['results']
        .cast<Map<String, dynamic>>()
        .map((obj) => Notifications.fromMap(obj))
        .toList()
        .cast<Notifications>();
  }

  static Notifications fromMap(Map map) {
    var name = map['name'];

    return new Notifications(
//      avatar: map['picture']['large'],
      avatar: map['sender']['image'],
      notification: map['notification'],
      createdat: map['created_at'],
      email: map['email'],
    );
  }

  static String _capitalize(String input) {
    return input.substring(0, 1).toUpperCase() + input.substring(1);
  }
}
