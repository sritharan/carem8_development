import 'dart:convert';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:flutter/material.dart';
import 'package:carem8/auth.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/generalforms/generalformslist.dart';
import 'package:carem8/pages/home/dailysummarydatalist.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:carem8/pages/login/login.dart';
import 'package:carem8/pages/newsfeed/newsfeedlist.dart';
import 'package:carem8/pages/newsletter/newsletterlist.dart';
import 'package:carem8/pages/notification/notificationlist.dart';
import 'package:carem8/pages/policies/policylist.dart';
import 'package:carem8/pages/privatemessage/inboxlist.dart';
import 'package:carem8/pages/settings/profilo_ui.dart';
import 'package:carem8/pages/splashScreen.dart';

class CommonDrawer extends StatefulWidget {
  List<ChildViewModal> childrenData;

  CommonDrawer({this.childrenData});

  @override
  State<StatefulWidget> createState() {
    return new CommonDrawerState();
  }
}

class CommonDrawerState extends State<CommonDrawer>
    implements HomePageContract, AuthStateListener {
  var appuser, name, email, password, client_id, center;
  var jwt, id, cliId, children, image;
  var deviceSize;
  BuildContext _ctx;
  HomePagePresenter _presenter;
  bool load = true;

  CommonDrawerState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  logout() {
    Navigator.pushAndRemoveUntil(
        context, new MaterialPageRoute(builder: (context) => new LoginPage()), (Route<dynamic> route) => false);
    _presenter.getUserLogout();
  }

  Widget build(BuildContext context) {
    var userImage;
    if (image == null) {
      userImage = AssetImage("assets/nophoto.jpg");
    } else {
      precacheImage(NetworkImage(image), context, onError: (err, sTrace) => userImage = AssetImage("assets/nophoto.jpg"));
      userImage = NetworkImage(image);
    }
    return new Drawer(
      child: new ListView(
        children: <Widget>[
          load
              ? CircularProgressIndicator()
              : new UserAccountsDrawerHeader(
                  decoration: new BoxDecoration(
                    color: kinderm8Theme.Colors.appcolour,
//              borderRadius:
//              new BorderRadius.circular(25.0),
//              border: new Border.all(
//                width: 2.0,
//                color: kinderm8Theme.Colors.appcolour,
//              ),
                  ),
                  accountName: new Text("$name"),
                  accountEmail: new Text("$email"),
                  currentAccountPicture: new CircleAvatar(
                      backgroundColor: kinderm8Theme.Colors.appcolour,
                      backgroundImage: userImage),
//                  otherAccountsPictures: <Widget>[
//                new CircleAvatar(
//                    backgroundColor: kinderm8Theme.Colors.appcolour,
//                    backgroundImage: new AssetImage("assets/nophoto.jpg")),
//                new CircleAvatar(
//                    backgroundColor: kinderm8Theme.Colors.appcolour,
//                    backgroundImage: new AssetImage("assets/nophoto.jpg")),
//            ],
//            onDetailsPressed: () {},
          ),
//          new ListTile(
//              title: new Text(labelsConfig["dailySummaryLabel"],
//                  style: new TextStyle(
//                    fontSize: 16.0,
//                    fontWeight: FontWeight.w600,
//                  )),
//              trailing: new Image.asset("assets/iconsetpng/house.png",
//                  width: 30.0, height: 30.0),
//              onTap: () {
//                setState(() {
//                  //                Navigator.of(context).pop();
//                  //                Navigator.pushNamed(context, '/notification');
//                  Navigator.push(
//                      context,
//                      new MaterialPageRoute(
//                          builder: (context) => new DailySummary(childrenData: widget.childrenData)));
//                });
//              }),
//          new ListTile(
//            title: new Text(labelsConfig["privateMessageLabel"],
//                style: new TextStyle(
//                  fontSize: 16.0,
//                  fontWeight: FontWeight.w600,
//                )),
//            trailing: new Image.asset("assets/iconsetpng/emaillog.png",
//                width: 30.0, height: 30.0),
//              onTap: () {
//                setState(() {
//                  //                Navigator.of(context).pop();
//                  //                Navigator.pushNamed(context, '/notification');
//                  Navigator.push(
//                      context,
//                      new MaterialPageRoute(
//                          builder: (context) => new PrivatemessageList()));
//                });
//              }
//          ),
          new ListTile(
              title: new Text(labelsConfig["newsFeedLabel"],
                  style: new TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.w600,
                  )),
              trailing: new Image.asset("assets/iconsetpng/computer_nsfeed.png",
                  width: 30.0, height: 30.0),
              onTap: () {
                setState(() {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new NewsFeedList(childrenData: widget.childrenData)));
                });
              }
          ),
          new ListTile(
            title: new Text(labelsConfig["newsletterLabel"],
                style: new TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                )),
            trailing: new Image.asset("assets/iconsetpng/newspaper.png",
                width: 30.0, height: 30.0),
              onTap: () {
                setState(() {
//                Navigator.of(context).pop();
//                Navigator.pushNamed(context, '/notification');
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new Newsletter(childrenData: widget.childrenData,)));
                });
              }
          ),
          new ListTile(
            title: new Text(labelsConfig["generalFormsLabel"],
                style: new TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                )),
            trailing: new Image.asset("assets/iconsetpng/gform.png",
                width: 30.0, height: 30.0),
              onTap: () {
                setState(() {
//                Navigator.of(context).pop();
//                Navigator.pushNamed(context, '/notification');
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new GeneralForm(childrenData: widget.childrenData,)));
                });
              }
          ),
          new ListTile(
            title: new Text(labelsConfig["policiesLabel"],
                style: new TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                )),
            trailing: new Image.asset("assets/iconsetpng/pforms.png",
                width: 30.0, height: 30.0),
              onTap: () {
                setState(() {
//                Navigator.of(context).pop();
//                Navigator.pushNamed(context, '/notification');
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new Policy(childrenData: widget.childrenData)));
                });
              }
          ),
          new ListTile(
            title: new Text(labelsConfig["settingsLabel"],
                style: new TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                )),
            trailing: new Image.asset("assets/iconsetpng/settings.png",
                width: 30.0, height: 30.0),
              onTap: () {
                setState(() {
//                Navigator.of(context).pop();
//                Navigator.pushNamed(context, '/notification');
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new UserSettings(userImage, name, email)));

                });
              }
          ),
          new ListTile(
              title: new Text(labelsConfig["notificationsLabel"],
                  style: new TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.w600,
                  )),
              trailing: new Image.asset("assets/iconsetpng/notification.png",
                  width: 30.0, height: 30.0),
              onTap: () {
                setState(() {
//                Navigator.of(context).pop();
//                Navigator.pushNamed(context, '/notification');
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new Notifications(childrenData: widget.childrenData)));
                });
              }),
          new Center(
            child: new ListTile(
              title: new Text(labelsConfig["logoutLabel"],
                  style: new TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.w600,
                  )),
              trailing: Icon(Icons.play_circle_outline,
                  size: 30.0, color: Colors.red[300]),
              onTap: () {
                logout();
              },
            ),
          )
//            new ClipRect(
//              child: new DrawerItem(
//                icon: new CircleAvatar(child: new Text("A")),
//                child: new Text('Drawer item A'),
//                onPressed: () => {},
//              ),
//            ),
        ],
      ),
    );
  }

  Widget _buildCard(String title, IconData icon, Color backgroundColor,
      {GestureTapCallback onTap}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 4.0,
        color: backgroundColor,
        child: InkWell(
          onTap: onTap,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Icon(
                icon,
                size: 50.0,
                color: Colors.white,
              ),
              Text(
                title,
                style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List list = [
    {
      "id": "Newsletters",
      "name": "Newsletters",
      "icon": Icons.work,
      "image": "assets/iconsetpng/newspaper.png",
      "color": Colors.teal
    },
    {
      "id": "Policies",
      "name": "Policies",
      "icon": Icons.smartphone,
      "image": "assets/iconsetpng/pforms.png",
      "color": Colors.grey[600]
    },
    {
      "id": "General Forms",
      "name": "General Forms",
      "icon": Icons.nature_people,
      "image": "assets/iconsetpng/gform.png",
      "color": Colors.green[600]
    },
    {
      "id": "Centre Notes",
      "name": "Centre Notes",
      "icon": Icons.directions_bike,
      "image": "assets/iconsetpng/user.png",
      "color": Colors.deepOrange
    },
    {
      "id": "Daily Journal",
      "name": "Daily Journal",
      "icon": Icons.videogame_asset,
      "image": "assets/iconsetpng/cubes.png",
      "color": Colors.orange
    },
    {
      "id": "Observations",
      "name": "Observations",
      "icon": Icons.people,
      "image": "assets/iconsetpng/game.png",
      "color": Colors.cyan
    },
    {
      "id": "Leanring Story",
      "name": "Leanring Story",
      "icon": Icons.local_movies,
      "image": "assets/iconsetpng/abacus.png",
      "color": Colors.purple
    },
    {
      "id": "Daily Chart",
      "name": "Daily Chart",
      "icon": Icons.local_hospital,
      "image": "assets/iconsetpng/notepad_dailychart.png",
      "color": Colors.red
    },
    {
      "id": "Medications",
      "name": "Medications",
      "icon": Icons.music_note,
      "image": "assets/iconsetpng/first-aid-kit.png",
      "color": Colors.amber
    },
    {
      "id": "Proivate Message",
      "name": "Proivate Message",
      "icon": Icons.assistant_photo,
      "image": "assets/iconsetpng/emaillog.png",
      "color": Colors.blueGrey
    },
    {
      "id": "Settings",
      "name": "Settings",
      "icon": Icons.assistant_photo,
      "image": "assets/iconsetpng/settings.png",
      "color": Colors.blueGrey
    },
    {
      "id": "Notifications",
      "name": "Notifications",
      "icon": Icons.assistant_photo,
      "image": "assets/iconsetpng/notification.png",
      "color": Colors.blueGrey,
      "navigator": "/notification"
    },
  ];

  Widget build__(BuildContext context) {
    return new Scaffold(
      body: new GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3, mainAxisSpacing: 20.0),
        padding: const EdgeInsets.only(top: 15.0),
        itemCount: list.length,
        shrinkWrap: false,
        physics: ClampingScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return new GridTile(
            footer: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Flexible(
                    child: new SizedBox(
                      height: 16.0,
                      width: 100.0,
                      child: new Text(
                        list[index]["name"],
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  )
                ]),
            child: new Container(
              height: 500.0,
              child: new GestureDetector(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      new SizedBox(
                        height: 100.0,
                        width: 100.0,
                        child: new Row(
                          children: <Widget>[
                            new Stack(
                              children: <Widget>[
                                new SizedBox(
                                  child: new Container(
                                    child: new CircleAvatar(
                                      backgroundColor: Colors.white,
                                      radius: 40.0,
                                      child: new Image.asset(
                                        list[index]['image'],
                                        width: 50.0,
                                        height: 50.0,
                                      ),
//                                      child: new Icon(
//                                          list[index]
//                                          ["icon"],
//                                          size: 40.0,
//                                          color: list[index]
//                                          ["color"]),
                                    ),
                                    padding: const EdgeInsets.only(
                                        left: 10.0, right: 10.0),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    Navigator.of(context).push(list[index]['navigator']);
//                    Navigator.push(
//                        context,
//                        new MaterialPageRoute(
//                            builder: (_) =>
//                            new ArticleSourceScreen.ArticleSourceScreen(
//                              sourceId: categoriesList.list[index]
//                              ['id'],
//                              sourceName: categoriesList.list[index]
//                              ["name"],
//                              isCategory: true,
//                            )));
//                    },
                  }),
            ),
          );
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.close),
        backgroundColor: kinderm8Theme.Colors.appcolour,
        onPressed: () {
//            showModalBottomSheet<Null>(
//                context: context,
//                builder: (BuildContext context) {
//                  return ChildrenDrawer();
//                });
          Navigator.pop(context);
//            Navigator.pushReplacementNamed(context, "/home");
//            Navigator.push(
//                context, new MaterialPageRoute(builder: (context) => new ChildrenDrawer()));
        },
      ),
    );
  }

  Widget build_(BuildContext context) {
    Widget commonmoduleList = Container(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Card(
          elevation: 2.0,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  DashboardMenuRow(
                    firstIcon: Icons.menu,
                    firstLabel: "Home",
                    firstIconCircleColor: Colors.blue,
                    secondIcon: Icons.menu,
                    secondLabel: "Message",
                    secondIconCircleColor: Colors.orange,
                    thirdIcon: Icons.menu,
                    thirdLabel: "Note to Centre",
                    thirdIconCircleColor: Colors.purple,
                    fourthIcon: Icons.menu,
                    fourthLabel: "Newsletters",
                    fourthIconCircleColor: Colors.indigo,
                  ),
                  DashboardMenuRow(
                    firstIcon: Icons.menu,
                    firstLabel: "General Forms",
                    firstIconCircleColor: Colors.red,
                    secondIcon: Icons.menu,
                    secondLabel: "Ploicies",
                    secondIconCircleColor: Colors.teal,
                    thirdIcon: Icons.menu,
                    thirdLabel: "Daily Journal",
                    thirdIconCircleColor: Colors.lime,
                    fourthIcon: Icons.menu,
                    fourthLabel: "Observation",
                    fourthIconCircleColor: Colors.amber,
                  ),
                  DashboardMenuRow(
                    firstIcon: Icons.menu,
                    firstLabel: "Medication",
                    firstIconCircleColor: Colors.cyan,
                    secondIcon: Icons.menu,
                    secondLabel: "Daily Chart",
                    secondIconCircleColor: Colors.redAccent,
                    thirdIcon: Icons.menu,
                    thirdLabel: "Larning Story",
                    thirdIconCircleColor: Colors.pink,
                    fourthIcon: Icons.menu,
                    fourthLabel: "Photos",
                    fourthIconCircleColor: Colors.brown,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
    Widget childmoduleList = Container();

    Widget childrenList = Container(
//      color: Colors.green,
      height: 80.0,
      child: ListView.builder(
        itemCount: children.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext contcext, int index) {
//          return Text("Text $index");
//        print(children[index]["image"]);
          return Container(
            width: 80.0,
            height: 80.0,
            child: Container(
              margin: const EdgeInsets.all(5.0),
//            padding: const EdgeInsets.all(26.0),
              decoration: new BoxDecoration(
                color: const Color(0xff7c94b6),
                image: new DecorationImage(
                  image: NetworkImage(
                      'http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${children[index]["image"]}'),
                  fit: BoxFit.cover,
                ),
                borderRadius: new BorderRadius.all(new Radius.circular(50.0)),
                border: new Border.all(
                  color: kinderm8Theme.Colors.appcolour,
                  width: 2.0,
                ),
              ),
            ),
          );
        },
      ),
    );

    return Scaffold(
//        appBar: AppBar(
//          title: Text("Test title"),
//        ),
        body: Column(
      children: <Widget>[
        Container(
          height: 30.0,
        ),
//        childrenList,
        commonmoduleList,
//          childmoduleList,
        Divider(),
        FlatButton(
          onPressed: () {
            print("Notification");
            Navigator.of(context).pop();
            Navigator.pushNamed(context, '/notification');
          },
          child: new Container(
            width: 50.0,
            height: 50.0,
            decoration: new BoxDecoration(
              color: const Color(0xff7c94b6),
              image: new DecorationImage(
                image: AssetImage("assets/iconsetpng/notification.png"),
                fit: BoxFit.cover,
              ),
              borderRadius: new BorderRadius.all(new Radius.circular(50.0)),
              border: new Border.all(
                color: Colors.red,
                width: 1.0,
              ),
            ),
          ),
        ),
        new ListTile(
            title: Text(
              "Home",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.home,
              color: kinderm8Theme.Colors.appmenuicon,
            ),
            onTap: () {
//              setState(() {
//                Navigator.of(context).pop();
//                Navigator.pushNamed(context, '/home');
              Navigator.push(context,
                  new MaterialPageRoute(builder: (context) => new HomePage()));
//              });
            }),
        new ListTile(
            title: Text(
              "Notifications",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.notifications,
              color: kinderm8Theme.Colors.appmenuicon,
            ),
            onTap: () {
              setState(() {
//                Navigator.of(context).pop();
//                Navigator.pushNamed(context, '/notification');
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new Notifications()));
              });
            }),
        new ListTile(
            title: Text(
              "Newsfeed",
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
            leading: Icon(
              Icons.chrome_reader_mode,
              color: kinderm8Theme.Colors.appmenuicon,
            ),
            onTap: () {
              setState(() {
//                Navigator.of(context).pop();
//                Navigator.pushNamed(context, '/notification');
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new NewsFeedList(childrenData: widget.childrenData,)));
              });
            }),
        new ListTile(
          title: Text(
            "Settings",
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
          ),
          leading: Icon(
            Icons.settings,
            color: kinderm8Theme.Colors.appmenuicon,
          ),
        ),
        new ListTile(
          title: Text(
            "LogOut",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
          ),
          leading: Icon(
            Icons.keyboard_return,
            color: Colors.redAccent,
          ),
          onTap: () {
            logout();
          },
        ),
        Divider(),
        new Padding(
          padding: new EdgeInsets.all(10.0),
          child: new BackButton(
            color: kinderm8Theme.Colors.appcolour,
          ),
        ),
        new Text(
          'Go Back',
          style: Theme.of(context).textTheme.headline.copyWith(
              color: kinderm8Theme.Colors.appmenuicon, fontSize: 26.0),
        ),
      ],
//          color: Colors.grey[400],

//          padding: EdgeInsets.all(20.0),
//          margin: EdgeInsets.all(20.0),
    ));
//    return Scaffold(
//      body: new Container(
//        child: ListView(
//          scrollDirection: Axis.vertical,
//          children: <Widget>[
//            Container(width: 100.0,color: Colors.redAccent),
//            Container(width: 100.0,color: Colors.amberAccent),
//            Container(width: 100.0,color: Colors.cyan),
//            Container(width: 100.0,color: Colors.black),
//
//          ],
//        ),
//      ),
//    );
  }

//  @override
//  Widget build_(BuildContext context) {
//    _ctx = context;
//    deviceSize = MediaQuery.of(context).size;
//    return new Drawer(
//      child: ListView(
//        padding: EdgeInsets.zero,
//        children: <Widget>[
//          Container(
//            child: ListView(
//              scrollDirection: Axis.vertical,
//              children: <Widget>[
//                Container(width: 100.0, color: Colors.redAccent),
//                Container(width: 100.0, color: Colors.amberAccent),
//                Container(width: 100.0, color: Colors.cyan),
//                Container(width: 100.0, color: Colors.black),
//              ],
//            ),
////            child: ListView.builder(
////              scrollDirection: Axis.horizontal,
////              itemCount: this.children != null ? this.children.length : 0,
////              itemBuilder: (context, i) {
////                final children = this.children[i];
////                return new Container(
////                  child: new Text(children["firstname"],
////                      style: new TextStyle(fontWeight: FontWeight.w500)),
////                );
////              },
////            ),
//          ),
//
//          Container(
//            height: deviceSize.height * 0.24,
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: <Widget>[
//                SizedBox(
//                  height: 10.0,
//                ),
//                Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                  children: <Widget>[
//                    GestureDetector(
//                      onTap: () {
//                        print('notification');
//                        Navigator.of(context).pop();
//                        Navigator.pushNamed(context, "/notification");
//                      },
//                      child: new Container(
//                        width: 50.0,
//                        height: 50.0,
//                        decoration: new BoxDecoration(
//                          color: const Color(0xff7c94b6),
//                          image: new DecorationImage(
//                            image: AssetImage(
//                                "assets/iconsetpng/notification.png"),
//                            fit: BoxFit.cover,
//                          ),
//                          borderRadius:
//                              new BorderRadius.all(new Radius.circular(50.0)),
//                          border: new Border.all(
//                            color: Colors.red,
//                            width: 1.0,
//                          ),
//                        ),
//                      ),
//                    ),
//                    Container(
//                      decoration: BoxDecoration(
//                        borderRadius:
//                            new BorderRadius.all(new Radius.circular(50.0)),
//                        border: new Border.all(
//                          color: Colors.black,
//                          width: 2.0,
//                        ),
//                      ),
//                      child: CircleAvatar(
//                        backgroundImage: AssetImage("assets/Kinderm8.png"),
//                        foregroundColor: Colors.black,
//                        radius: 40.0,
//                      ),
//                    ),
//                    Container(
//                      width: 50.0,
//                      height: 50.0,
//                      margin: const EdgeInsets.all(6.0),
//                      padding: const EdgeInsets.all(26.0),
//                      decoration: new BoxDecoration(
//                        color: const Color(0xff7c94b6),
//                        image: new DecorationImage(
//                          image: AssetImage("assets/iconsetpng/settings.png"),
//                          fit: BoxFit.contain,
//                        ),
//                        borderRadius:
//                            new BorderRadius.all(new Radius.circular(50.0)),
//                        border: new Border.all(
//                          color: Colors.red,
//                          width: 2.0,
//                        ),
//                      ),
//                    ),
//                  ],
//                )
//              ],
//            ),
//          ),
//          Container(
//            child: Column(
//              children: <Widget>[Text("$email"), Text("$client_id")],
//            ),
//          ),
//          Divider(),
//          Container(
//            child: new Column(
//              children: <Widget>[
//                new Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                  children: <Widget>[
//                    FlatButton(
//                      onPressed: () {
//                        Navigator.pushNamed(context, "/home");
//                        print("home");
//                      },
//                      child: Column(
//                        children: <Widget>[
//                          new Container(
//                            width: 50.0,
//                            height: 50.0,
//                            decoration: new BoxDecoration(
//                              color: const Color(0xff7c94b6),
//                              image: new DecorationImage(
//                                image: new AssetImage(
//                                    'assets/iconsetpng/house.png'),
//                                fit: BoxFit.contain,
//                              ),
//                              borderRadius: new BorderRadius.all(
//                                  new Radius.circular(5.0)),
////                              border: new Border.all(
////                                color: Colors.purpleAccent,
////                                width: 1.0,
////                              ),
//                            ),
//                          ),
//                          new Text(
//                            "Home",
//                            style: TextStyle(fontSize: 13.0),
//                          ),
//                        ],
//                      ),
//                    ),
//                    FlatButton(
//                      onPressed: () {
//                        print("home");
//                      },
//                      child: Column(
//                        children: <Widget>[
//                          new Container(
//                            width: 50.0,
//                            height: 50.0,
//                            decoration: new BoxDecoration(
//                              color: const Color(0xff7c94b6),
//                              image: new DecorationImage(
//                                image: new AssetImage('assets/nophoto.jpg'),
//                                fit: BoxFit.cover,
//                              ),
//                              borderRadius: new BorderRadius.all(
//                                  new Radius.circular(100.0)),
//                              border: new Border.all(
//                                color: Colors.purpleAccent,
//                                width: 1.0,
//                              ),
//                            ),
//                          ),
//                          new Text(
//                            "Message",
//                            style: TextStyle(fontSize: 13.0),
//                          ),
//                        ],
//                      ),
//                    ),
//                    FlatButton(
//                      onPressed: () {
//                        print("home");
//                      },
//                      child: Column(
//                        children: <Widget>[
//                          new Container(
//                            width: 50.0,
//                            height: 50.0,
//                            decoration: new BoxDecoration(
//                              color: const Color(0xff7c94b6),
//                              image: new DecorationImage(
//                                image: new AssetImage('assets/icon.jpg'),
//                                fit: BoxFit.cover,
//                              ),
//                              borderRadius: new BorderRadius.all(
//                                  new Radius.circular(100.0)),
//                              border: new Border.all(
//                                color: Colors.purpleAccent,
//                                width: 1.0,
//                              ),
//                            ),
//                          ),
//                          new Text(
//                            "Centre to Note",
//                            style: TextStyle(fontSize: 13.0),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ],
//                ),
////
//                Padding(
//                  padding: EdgeInsets.all(2.0),
//                ),
//                new Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                  children: <Widget>[
//                    FlatButton(
//                      onPressed: () {
//                        print("home");
//                      },
//                      child: Column(
//                        children: <Widget>[
//                          new Container(
//                            width: 50.0,
//                            height: 50.0,
//                            decoration: new BoxDecoration(
//                              color: const Color(0xff7c94b6),
//                              image: new DecorationImage(
//                                image: new AssetImage('assets/nophoto.jpg'),
//                                fit: BoxFit.cover,
//                              ),
//                              borderRadius: new BorderRadius.all(
//                                  new Radius.circular(50.0)),
//                              border: new Border.all(
//                                color: Colors.purpleAccent,
//                                width: 1.0,
//                              ),
//                            ),
//                          ),
//                          new Text(
//                            "Newsletter",
//                            style: TextStyle(fontSize: 13.0),
//                          ),
//                        ],
//                      ),
//                    ),
//                    FlatButton(
//                      onPressed: () {
//                        print("home");
//                      },
//                      child: Column(
//                        children: <Widget>[
//                          new Container(
//                            width: 50.0,
//                            height: 50.0,
//                            decoration: new BoxDecoration(
//                              color: const Color(0xff7c94b6),
//                              image: new DecorationImage(
//                                image: new AssetImage('assets/nophoto.jpg'),
//                                fit: BoxFit.cover,
//                              ),
//                              borderRadius: new BorderRadius.all(
//                                  new Radius.circular(100.0)),
//                              border: new Border.all(
//                                color: Colors.purpleAccent,
//                                width: 1.0,
//                              ),
//                            ),
//                          ),
//                          new Text(
//                            "Gerneral forms",
//                            style: TextStyle(fontSize: 13.0),
//                          ),
//                        ],
//                      ),
//                    ),
//                    FlatButton(
//                      onPressed: () {
//                        print("home");
//                      },
//                      child: Column(
//                        children: <Widget>[
//                          new Container(
//                            width: 50.0,
//                            height: 50.0,
//                            decoration: new BoxDecoration(
//                              color: const Color(0xff7c94b6),
//                              image: new DecorationImage(
//                                image: new AssetImage('assets/icon.jpg'),
//                                fit: BoxFit.cover,
//                              ),
//                              borderRadius: new BorderRadius.all(
//                                  new Radius.circular(100.0)),
//                              border: new Border.all(
//                                color: Colors.purpleAccent,
//                                width: 1.0,
//                              ),
//                            ),
//                          ),
//                          new Text(
//                            "Policies",
//                            style: TextStyle(fontSize: 13.0),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ],
//                ),
//              ],
//            ),
//          ),
////
//          Divider(),
//
//          new ListTile(
//            title: Text(
//              "Profile",
//              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//            ),
//            leading: Icon(
//              Icons.person,
//              color: Colors.blue,
//            ),
//          ),
//          new ListTile(
//            title: Text(
//              "Dashboard",
//              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//            ),
//            leading: Icon(
//              Icons.dashboard,
//              color: Colors.red,
//            ),
//          ),
//          new ListTile(
//            title: Text(
//              "Timeline",
//              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//            ),
//            leading: Icon(
//              Icons.timeline,
//              color: Colors.cyan,
//            ),
//          ),
//          Divider(),
//          new ListTile(
//            title: Text(
//              "Settings",
//              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
//            ),
//            leading: Icon(
//              Icons.settings,
//              color: Colors.brown,
//            ),
//          ),
//          new ListTile(
//            title: Text(
//              "LogOut",
//              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
//            ),
//            leading: Icon(
//              Icons.keyboard_return,
//              color: Colors.redAccent,
//            ),
//            onTap: () {
//              logout();
//            },
//          ),
//          Divider(),
////          MyAboutTile()
//        ],
//      ),
//    );
//
//    //      Drawer(
////      child: ListView(
////        padding: EdgeInsets.zero,
////        children: <Widget>[
////          UserAccountsDrawerHeader(
////            accountName: Text(
////              "Pawan Kumar",
////            ),
////            accountEmail: Text(
////              "mtechviral@gmail.com",
////            ),
////            currentAccountPicture: new CircleAvatar(
//////              backgroundImage: new AssetImage("http://i.pravatar.cc/300"),
////            ),
////          ),
////          new ListTile(
////            title: Text(
////              "Profile",
////              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
////            ),
////            leading: Icon(
////              Icons.person,
////              color: Colors.blue,
////            ),
////          ),
////          new ListTile(
////            title: Text(
////              "Shopping",
////              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
////            ),
////            leading: Icon(
////              Icons.shopping_cart,
////              color: Colors.green,
////            ),
////          ),
////          new ListTile(
////            title: Text(
////              "Dashboard",
////              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
////            ),
////            leading: Icon(
////              Icons.dashboard,
////              color: Colors.red,
////            ),
////          ),
////          new ListTile(
////            title: Text(
////              "Timeline",
////              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
////            ),
////            leading: Icon(
////              Icons.timeline,
////              color: Colors.cyan,
////            ),
////          ),
////          Divider(),
////          new ListTile(
////            title: Text(
////              "Settings",
////              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
////            ),
////            leading: Icon(
////              Icons.settings,
////              color: Colors.brown,
////            ),
////          ),
////          Divider(),
//////          MyAboutTile()
////        ],
////      ),
////    );
//  }

  @override
  void onDisplayUserInfo(User user) {
    email = user.email;
    password = user.password;
    client_id = user.client_id;
    center = user.center;
    setState(() {
      try {
        final parsed = json.decode(center);
        var details = parsed[0];
        var users = details["user"];
        jwt = details["jwt"];
        users = details["user"];
        id = users["id"];
        cliId = users["client_id"];
        children = details["children"];

        id = users["id"];
        name = users["fullname"];
        email = users["email"];
        image = users['image'];
        load = false;
      } catch (e) {
        print('That string was null!');
      }
    });
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
    // TODO: implement onLogoutUser
  }

  @override
  void onAuthStateChanged(AuthState state) {
    print("Navigatesssss");
    print("state $state");
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(_ctx)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
    // TODO: implement onAuthStateChanged
  }
}

class LabelBelowIcon extends StatelessWidget {
  final label;
  final IconData icon;
  final iconColor;
  final onPressed;
  final circleColor;
  final isCircleEnabled;
  final betweenHeight;

  LabelBelowIcon(
      {this.label,
      this.icon,
      this.onPressed,
      this.iconColor = Colors.white,
      this.circleColor,
      this.isCircleEnabled = true,
      this.betweenHeight = 5.0});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onPressed,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          isCircleEnabled
              ? CircleAvatar(
                  backgroundColor: circleColor,
                  radius: 20.0,
                  child: Icon(
                    icon,
                    size: 12.0,
                    color: iconColor,
                  ),
                )
              : Icon(
                  icon,
                  size: 23.0,
                  color: iconColor,
                ),
          SizedBox(
            height: betweenHeight,
          ),
          Text(
            label,
            textAlign: TextAlign.center,
//            style: TextStyle(fontFamily:f),
          )
        ],
      ),
    );
  }
}

class DashboardMenuRow extends StatelessWidget {
  final firstLabel;
  final IconData firstIcon;
  final firstIconCircleColor;
  final secondLabel;
  final IconData secondIcon;
  final secondIconCircleColor;
  final thirdLabel;
  final IconData thirdIcon;
  final thirdIconCircleColor;
  final fourthLabel;
  final IconData fourthIcon;
  final fourthIconCircleColor;

  const DashboardMenuRow(
      {Key key,
      this.firstLabel,
      this.firstIcon,
      this.firstIconCircleColor,
      this.secondLabel,
      this.secondIcon,
      this.secondIconCircleColor,
      this.thirdLabel,
      this.thirdIcon,
      this.thirdIconCircleColor,
      this.fourthLabel,
      this.fourthIcon,
      this.fourthIconCircleColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          LabelBelowIcon(
            icon: firstIcon,
            label: firstLabel,
            circleColor: firstIconCircleColor,
          ),
          LabelBelowIcon(
            icon: secondIcon,
            label: secondLabel,
            circleColor: secondIconCircleColor,
          ),
          LabelBelowIcon(
            icon: thirdIcon,
            label: thirdLabel,
            circleColor: thirdIconCircleColor,
          ),
          LabelBelowIcon(
            icon: fourthIcon,
            label: fourthLabel,
            circleColor: fourthIconCircleColor,
          ),
        ],
      ),
    );
  }
}

/*

class MyAboutTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AboutListTile(
      applicationIcon: FlutterLogo(
        colors: Colors.yellow,
      ),
      icon: FlutterLogo(
        colors: Colors.yellow,
      ),
      aboutBoxChildren: <Widget>[
        SizedBox(
          height: 10.0,
        ),
        Text(
          "Developed By Proitzen",
        ),
      ],
//      applicationName: UIData.appName,
//      applicationVersion: "1.0.1",
//      applicationLegalese: "Apache License 2.0",
    );
  }
}
*/
