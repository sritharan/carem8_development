import 'package:carem8/Theme.dart' as Theme;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:carem8/pages/calendar/detailprogram.dart';

class ProgramData extends StatelessWidget {
  final singleprogramdata;
  var color, splitColor, tag;
  int colorCode;

  ProgramData(this.singleprogramdata);

  Widget build(BuildContext context) {
//      return new Text(singleprogramdata.toString());
    color = singleprogramdata["program_color"].toString();
    splitColor = '0xFF' + color.substring(1);
    colorCode = int.parse(splitColor);

    String readTimestamp(int timestamp) {
      var now = new DateTime.now();
      var format = new DateFormat('HH:mm a');
      var date = new DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
      var diff = now.difference(date);
      var time = '';

      if (diff.inSeconds <= 0 ||
          diff.inSeconds > 0 && diff.inMinutes == 0 ||
          diff.inMinutes > 0 && diff.inHours == 0 ||
          diff.inHours > 0 && diff.inDays == 0) {
        time = format.format(date);
      } else if (diff.inDays > 0 && diff.inDays < 7) {
        if (diff.inDays == 1) {
          time = diff.inDays.toString() + ' DAY AGO';
        } else {
          time = diff.inDays.toString() + ' DAYS AGO';
        }
      } else {
        if (diff.inDays == 7) {
          time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
        } else {
          time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
        }
      }

      return time;
    }

    var programStartDate = DateTime.parse(singleprogramdata["start_date"]);
    var programEndDate = DateTime.parse(singleprogramdata["end_date"]);

    var timeStamp = new DateFormat("d MMM");
    String formatProgramStartDate = timeStamp.format(programStartDate);

    String formatProgramEndDate = timeStamp.format(programEndDate);

    final sdate = singleprogramdata['start_date'];
    String dateWithYear = sdate.substring(0, 4);
    String dateWithMonth = sdate.substring(5, 7);
    String dateWithDate = sdate.substring(8, 10);

    var formatedstartdate = '$dateWithYear$dateWithMonth$dateWithDate';
    DateTime dateTimeStart = DateTime.parse(formatedstartdate);

    int intFormatedStartDate = int.parse(formatedstartdate);
    DateTime date =
        new DateTime.fromMillisecondsSinceEpoch(intFormatedStartDate);

    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => new ProgramDetails(singleprogramdata)));
      },
      child: Card(
        elevation: 1.0,
        color: Color(colorCode).withOpacity(0.5),
        margin: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 4.0),
        child: new Row(
          children: <Widget>[
            new Container(width: 5.0, height: 150.0, color: Color(colorCode)),
            new Expanded(
              child: new Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 10.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.symmetric(vertical: 10.0),
                      decoration: new BoxDecoration(
                        border: new Border(
                          left: new BorderSide(
                            width: 4.0,
                            color: Color(colorCode),
                          ),
                        ),
                      ),
                    ),
                    new Text(
                      singleprogramdata['title'].toString(),
                      style: new TextStyle(
                        fontSize: 16.0,
                        color: Theme.Colors.app_dark[700],
                        fontWeight: FontWeight.bold,
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    new Row(
                      children: <Widget>[
                        new Text('${labelsConfig["programTypeLabel"]}:',
                            style: new TextStyle(
                              fontSize: 12.0,
                              color: Theme.Colors.app_dark[400],
                              fontWeight: FontWeight.w300,
                            )),
                        new Text(
                          singleprogramdata['program_type'].toString(),
                          style: new TextStyle(
                            fontSize: 12.0,
                            color: Theme.Colors.app_dark[400],
                            fontWeight: FontWeight.w600,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                    new Padding(
                      padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                      child: new Text(
                        singleprogramdata['description'],
                        style: new TextStyle(
                          color: Theme.Colors.app_dark[300],
                          fontSize: 12.0,
                        ),
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    new Row(
                      children: <Widget>[
                        new Text(
                          formatProgramStartDate.toString() + ' - ',
                          style: new TextStyle(
                            fontSize: 12.0,
                            color: Theme.Colors.app_dark[400],
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        new Text(
                          formatProgramEndDate.toString(),
                          style: new TextStyle(
                            fontSize: 12.0,
                            color: Theme.Colors.app_dark[400],
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
