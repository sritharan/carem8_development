import 'dart:io';
import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:carem8/Theme.dart' as Theme;

//import 'package:photo_view/photo_view.dart';
//import 'package:device_calendar/device_calendar.dart';
import 'Package:intl/Date_symbol_data_local.Dart';
import 'package:date_format/date_format.dart';
//import 'package:add_2_calendar/add_2_calendar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/calendar/eventslist.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/utils/network_util.dart';

class EventDetails extends StatefulWidget {
  final eventdata;
  final jwt;
//  Calendar _calendar;
//  Event _event;
  EventDetails(this.eventdata,this.jwt);
  @override
  _EventPageState createState() {
    return new _EventPageState(eventdata, jwt);
  }

//  @override
//  _EventPageState createState() => new _EventPageState(eventdata);
}

class _EventPageState extends State<EventDetails>
    with SingleTickerProviderStateMixin implements HomePageContract {
  AnimationController animationController;
  ScrollController scrollController;
  Animation<Color> colorTween1;
  Animation<Color> colorTween2;
  final eventdata;
  var jwt;
//  DeviceCalendarPlugin _deviceCalendarPlugin;
  var appuser, userId, clientId;

  HomePagePresenter _presenter;
  _EventPageState(this.eventdata,this.jwt) {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }
  final double statusBarSize = 24.0;
  final double imageSize = 264.0;
  double readPerc = 0.0;

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

//  Calendar _calendar;

  final TextEditingController _textControllerGuest =
      new TextEditingController();
  var now = DateTime.now();
  var berlinWallFell = DateTime.utc(1989, 11, 9);
  var moonLanding = DateTime.parse("1969-07-20 20:18:04Z");

  // Event
//  Event _event;
  DateTime _startDate;
  TimeOfDay _startTime;

  DateTime _endDate;
  TimeOfDay _endTime;

  final platform = const MethodChannel('proitzen.kinderm8/s3_image');
  var isrsvpclicked;
  bool isSelected;
  var isColor;
  //status_for_api
  var status;


  List<RadioModel> sampleData = new List<RadioModel>();
  int _itemCount;
  bool bool_Yes;
  bool bool_Maybe;
  bool bool_No;

  bool _autovalidate = false;


  @override
  void initState() {
    super.initState();
    print('---------------------------------------$jwt-------------------------------------------------------------');
    // rsvp_initiate
//    isrsvpclicked = 1;
    print('#######################');
    print(eventdata);
    print('#######################');
    //if(this.eventdata['is_rsvp'] == false){
    if(this.eventdata['rsvp'] == null){
      bool_Yes = false;
      bool_Maybe = false;
      bool_No = false;
    }
    // isrsvpclicked = this.eventdata['is_rsvp'] != null ? this.eventdata['is_rsvp'] == true ? 1 : 0 : 0;
    isrsvpclicked = this.eventdata['rsvp'] == null ? 0 : 1;
    print('-------------------------------isrsvpclicked = ${isrsvpclicked}-------------------------------');

    isColor ="";
    _itemCount = eventdata["rsvp"]!= null ? eventdata["rsvp"]['num_of_attendees'] : 0;
    setState(() {
      if(eventdata["rsvp"]!= null) {
        eventdata["rsvp"]['status'] == 1 ? status = 1 : 0 ;
        eventdata["rsvp"]['status'] == 2 ? status = 2 : 0 ;
        eventdata["rsvp"]['status'] == 3 ? status = 3 : 0 ;

        bool_Yes = eventdata["rsvp"]['status'] == 1 ? true : false;
        bool_Maybe = eventdata["rsvp"]['status'] == 2 ? true : false;
        bool_No = eventdata["rsvp"]['status'] == 3 ? true : false;
      }
    });

    sampleData.add(new RadioModel(bool_Yes, 'Yes', 0));
    sampleData.add(new RadioModel(bool_Maybe, 'Maybe',1));
    sampleData.add(new RadioModel(bool_No, 'No',2));

//    _deviceCalendarPlugin = new DeviceCalendarPlugin();
    // Create the appbar colors animations
    animationController = new AnimationController(
        duration: new Duration(milliseconds: 500), vsync: this);
    animationController.addListener(() => setState(() {}));

    colorTween1 =
        new ColorTween(begin: Colors.black12, end: new Color(0xFF0018C8))
            .animate(new CurvedAnimation(
                parent: animationController, curve: Curves.easeIn));
    colorTween2 =
        new ColorTween(begin: Colors.transparent, end: new Color(0xFF001880))
            .animate(new CurvedAnimation(
                parent: animationController, curve: Curves.easeIn));

    scrollController = new ScrollController();
    scrollController.addListener(() {
      setState(() => readPerc =
          scrollController.offset / scrollController.position.maxScrollExtent);

      // Change the appbar colors
      if (scrollController.offset > imageSize - statusBarSize) {
        if (animationController.status == AnimationStatus.dismissed)
          animationController.forward();
      } else if (animationController.status == AnimationStatus.completed)
        animationController.reverse();
    });
  }

  void _toEnd() {                                                     // NEW
    scrollController.animateTo(                                      // NEW
      scrollController.position.maxScrollExtent,                     // NEW
      duration: const Duration(milliseconds: 500),                    // NEW
      curve: Curves.ease,                                             // NEW
    );                                                                // NEW
  }

  void _incrementCounter(int i) {
    setState(() {
      _itemCount = _itemCount + i;
    });
  }
  void _decrementCounter(int i) {
    setState(() {
      if(_itemCount <= 1) {
        _itemCount = _itemCount - i;
      }
    });
  }

  // submit rsvp for any user option
  submitRSVP() async {
    final submitRSVPUrl =
        "https://apicare.carem8.com/v4.4.5/createEventsRsvp?userid=$userId&clientid=$clientId";

    var body = {
      "event_id": eventdata["id"],
      "status": status,
      "num_of_attendees": _itemCount,
    }; //encodedImagePathsString
    print("--------------------------- submitRSVP $body --------------------------- ");
    var headers = {"x-authorization": jwt.toString()};
    print("<<<<< submitRSVP request body to send: $body");
    NetworkUtil _netUtil = new NetworkUtil();
    _netUtil
        .post(submitRSVPUrl,
        body: body, headers: headers, encoding: jwt.toString())
        .then((res) {
      print("--------------------------- submitRSVP  resss  $res");
      Fluttertoast.showToast(msg: 'RSPV sucessfully submited!',toastLength: Toast.LENGTH_SHORT);
      if(eventdata["rsvp"] != null) {
        setState(() {
          eventdata["rsvp"]['status'] = status;
          eventdata["rsvp"]['num_of_attendees'] = _itemCount;
        });
      }
//      Navigator.pushReplacement(context,
//          MaterialPageRoute(builder: (context) => EventsList()));
      if (res ==  1) {
        String _refreshTokenUrl =
            'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

        NetworkUtil _netUtil = new NetworkUtil();
        _netUtil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          var refreshJwtToken = json.decode(response.body);
          this.jwt = refreshJwtToken;
          submitRSVP();
        });
      } else if (res == '') {
        print("may be 200");
        setState(() {
//          isImageUploading = false;
        });

      } else {
        print("error");
      }
    });
  }
  // Update on any user action
  updateRSVP(){
    final submitRSVPUrl =
        "https://apicare.carem8.com/v4.4.5/updateEventsRsvp?userid=$userId&clientid=$clientId";

    var body = {
      "id": eventdata["rsvp"]["id"],
      "event_id": eventdata["id"],
      "status": status,
      "num_of_attendees": _itemCount,
    }; //encodedImagePathsString
    print("--------------------------- updateRSVP $body --------------------------- ");
    var headers = {"x-authorization": jwt.toString()};
    print("<<<<< updateRSVP request body to send: $body");
    NetworkUtil _netUtil = new NetworkUtil();
    _netUtil
        .post(submitRSVPUrl,
        body: body, headers: headers, encoding: jwt.toString())
        .then((res) {
      print("--------------------------- updateRSVP resss  $res");
      if(eventdata["rsvp"] != null) {
        setState(() {
          eventdata["rsvp"]['status'] = status;
          eventdata["rsvp"]['num_of_attendees'] = _itemCount;
        });
      }

      Fluttertoast.showToast(msg: 'RSPV sucessfully submited!',toastLength: Toast.LENGTH_SHORT);
//      Navigator.pushReplacement(context,
//          MaterialPageRoute(builder: (context) => EventsList()));
      if (res ==  1) {
        String _refreshTokenUrl =
            'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

        NetworkUtil _netUtil = new NetworkUtil();
        _netUtil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          var refreshJwtToken = json.decode(response.body);
          this.jwt = refreshJwtToken;
          updateRSVP();
        });
      } else if (res == '') {
        print("may be 200");
        setState(() {
//          isImageUploading = false;

        });

      } else {
        print("error");
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    var S3URL = "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/";
    var title = eventdata["title"];
    var description = eventdata["description"] != null ? eventdata["description"] : "";
    var eventimage = eventdata["image_url"] != null ?eventdata["image_url"] : "";

    var EventStartDate = (eventdata["date"] != null) ? DateTime.parse(eventdata["date"]) : DateTime.tryParse("");
    var EventEndDate = (eventdata["to_date"] != null) ? DateTime.parse(eventdata["to_date"]) : DateTime.tryParse("");
    DateTime eventEndDate;

    String radiovalue;
    int _radioValue1 = -1;

    void _handleRadioValueChange1(int value) {
      setState(() {
        _radioValue1 = value;

        switch (_radioValue1) {
          case 0:
            Fluttertoast.showToast(msg: 'Yes',toastLength: Toast.LENGTH_SHORT);
            break;
          case 1:
            Fluttertoast.showToast(msg: 'Maybe!',toastLength: Toast.LENGTH_SHORT);
            break;
          case 2:
            Fluttertoast.showToast(msg: 'No',toastLength: Toast.LENGTH_SHORT);
            break;
        }
      });
    }

    final Size screenSize = MediaQuery.of(context).size;
    var is_rsvp_facility = eventdata["is_rsvp"] != null ? eventdata["is_rsvp"] == true ? true : false : false;
    var is_rsvp_created = eventdata["rsvp"] == null ? false : true;
    print('----------------------------------------------------------------------------------------------------');
    print('eventdata["rsvp"]${eventdata["rsvp"]}');
    print('is_rsvp_created > $is_rsvp_created');
    print('----------------------------------------------------------------------------------------------------');

    /* // Format */
    var timeStamp = new DateFormat("yMMMEd");
    String formatEventStartDate;
    String formatEventEndDate;
    if (eventdata["date"] != '' && eventdata["date"] != null) {
      formatEventStartDate = timeStamp.format(EventStartDate);
      eventEndDate = DateTime.parse(eventdata["date"]);

      formatEventEndDate = timeStamp.format(eventEndDate);
    } else {
      formatEventEndDate = "";
    }

    print('EventDetails $eventdata');
    return new Scaffold(
        body: new Hero(
          tag: 'DetilEvents',
          child: new Material(
            color: Colors.white,
            child:
                new Stack // I need to use a stack because otherwise the appbar blocks the content even if transparent
                    (
              children: <Widget>[
                /// Image and text
                new ListView(
                  padding: new EdgeInsets.all(0.0),
                  controller: scrollController,
                  children: <Widget>[
                    eventdata['image_url'] != ''
                        ? new SizedBox.fromSize(
                            size: new Size.fromHeight(imageSize),
                            child: new Hero(
                              tag: 'Image',
//                      child: new Image.asset('res/img1.png', fit: BoxFit.cover),
                              child: new CachedNetworkImage(
                                imageUrl: S3URL + eventdata['image_url'],
                                placeholder: new CupertinoActivityIndicator(),
//                errorWidget: new Icon(Icons.error),
                                fadeOutDuration: new Duration(seconds: 1),
                                fadeInDuration: new Duration(seconds: 3),
                              ),
                            ),
                          )
                        : new Container(
                            height: screenSize.width / 3,
//                      margin: EdgeInsets.only(top: screenSize.width/3),
                          ),
//                    new Padding(
//                      padding: new EdgeInsets.symmetric(vertical: 12.0),
//                      child: new Text('INVISION PRESENTS',
//                          textAlign: TextAlign.center,
//                          style: new TextStyle(
//                              fontSize: 11.0,
//                              color: Colors.black,
//                              fontWeight: FontWeight.w700)),
//                    ),
                    new Padding(
                      padding: new EdgeInsets.symmetric(horizontal: 32.0),
                      child: new Hero(
                        tag: 'Title',
                        child: new Text(title,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                                fontSize: 20.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600)),
                      ),
                    ),
//                  new Padding
//                    (
//                    padding: new EdgeInsets.only(top: 12.0, bottom: 24.0),
//                    child: new Text(eventdata['date'], textAlign: TextAlign.center, style: new TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.w700)),
//                  ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              margin: new EdgeInsets.only(left: 5.0),
                              child: new IconButton(
                                onPressed: () {},
                                icon: new Icon(
                                  Icons.today,
                                  color: Theme.Colors.appcolour,
                                  size: 35.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
//                              margin: new EdgeInsets.only(left: 5.0),
                              child: Row(
                                children: <Widget>[
                                  new Text('Start Date : ',
                                      textAlign: TextAlign.start,
                                      style: new TextStyle(
                                          fontSize: 14.0,
                                          color: Theme.Colors.darkGrey,
                                          fontWeight: FontWeight.w500)),
                                  new Text(formatEventStartDate.toString(),
                                      textAlign: TextAlign.start,
                                      style: new TextStyle(
                                          fontSize: 14.0,
                                          color: Theme.Colors.darkGrey,
                                          fontWeight: FontWeight.w400)),
                                  eventdata['time'] != ''
                                      ? new Text(' at ',
                                          style: new TextStyle(
                                              fontSize: 14.0,
                                              color: Theme.Colors.darkGrey,
                                              fontWeight: FontWeight.w400))
                                      : new Text(''),
                                  new Text(eventdata['time'],
                                      textAlign: TextAlign.start,
                                      style: new TextStyle(
                                          fontSize: 14.0,
                                          color: Theme.Colors.darkGrey,
                                          fontWeight: FontWeight.w400))
                                ],
                              ),
//                            margin: new EdgeInsets.only(left: 5.0),
//                            child: new Text('Start date'+eventdata['time'], textAlign: TextAlign.center,
//                                style: new TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.w700)),
                            ),
                            Container(
//                                margin: new EdgeInsets.only(left: 15.0),
                                child: Row(children: <Widget>[
                                  formatEventEndDate != "" ? new Text('End Date : ',
                                  textAlign: TextAlign.start,
                                  style: new TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.darkGrey,
                                      fontWeight: FontWeight.w500)) :SizedBox(),
                              new Text(formatEventEndDate,
                                  textAlign: TextAlign.start,
                                  style: new TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.darkGrey,
                                      fontWeight: FontWeight.w400)),
                              eventdata['to_time'] != ''
                                  ? new Text(' at ',
                                      style: new TextStyle(
                                          fontSize: 14.0,
                                          color: Theme.Colors.darkGrey,
                                          fontWeight: FontWeight.w400))
                                  : new Text(''),
                                  eventdata['to_time'] != '' ? new Text(eventdata['to_time'],
                                  textAlign: TextAlign.start,
                                  style: new TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.darkGrey,
                                      fontWeight: FontWeight.w400)) : SizedBox(),
//                            margin: new EdgeInsets.only(left: 5.0),
//                            child: new Text(eventdata['to_time'], textAlign: TextAlign.center,
//                                style: new TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.w700)),
                            ]))
                          ],
                        )
                      ],
                    ),

                    is_rsvp_facility && is_rsvp_created == false ? new Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                      child: isrsvpclicked == 0 ?new MaterialButton(
                          height: 25.0,
                          color: Theme.Colors.appcolour,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Icon(Icons.arrow_downward, color: Theme.Colors.app_white,),
                              Text(" Go to RSVP", style: TextStyle(fontSize:20.0,fontWeight: FontWeight.w400,color: Colors.white)),
                            ],
                          ),
                          onPressed: () {
                            _toEnd();
                            setState(() {
                              isrsvpclicked = 1;
                              print(isrsvpclicked);
                            });

                          }) : new Container(),
                    ): new Container(),

                    Container(height:4.0),
                    new Container(
                      margin: new EdgeInsets.only(
                          left: 20.0, right: 20.0, bottom: 10.0),
                      child: new Text(description,
                          style: new TextStyle(fontSize: 16.0,color: Colors.black,fontWeight: FontWeight.w500)),
                    ),


                    // RSVP Status from DB
                    is_rsvp_facility ? new Container(
                      child: new Column(
                        children: <Widget>[

//                          new Text('is_rsvp_facility : $is_rsvp_facility'),
//                          new Text('is_rsvp_created :$is_rsvp_created'),
//                          new Text('isrsvpclicked :$isrsvpclicked'),
//                          new Text('status selected $status'),
                          new Divider(),
//                          eventdata["rsvp"] != null ? new Text(eventdata["rsvp"]['status']): new Container(),
//                          eventdata["rsvp"] != null ? new Text(eventdata["rsvp"]['num_of_attendees']) : new Container(),

                        ],
                      ),
                    ) : new Container(),

                     // /* Code moved into icon button after the dates */
//                    is_rsvp_facility && is_rsvp_created == false ? new Padding(
//                      padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
//                      child: isrsvpclicked == 0 ?new MaterialButton(
//                        height: 50.0,
//                        color: Theme.Colors.appcolour,
//                        child: Text("RSVP", style: TextStyle(fontSize:20.0,color: Colors.white)),
//                        onPressed: () {
//                          setState(() {
//                            isrsvpclicked = 1;
//                            print(isrsvpclicked);
//                          });
//
//                        }) : new Container(),
//                    ): new Container(),

                    is_rsvp_facility ? Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                      isrsvpclicked == 1 ? Stack(
                          alignment: Alignment.bottomCenter,
                          children: <Widget>[
                            Container(color: Colors.white,height: 22.0,),
                            Container(padding: EdgeInsets.symmetric(horizontal: 20.0,vertical: 8.0),child: Text("Will you be attending?",style: TextStyle(color: Colors.white,fontSize: 18.0,fontWeight: FontWeight.w600),),decoration:BoxDecoration(borderRadius:BorderRadius.circular(50.0) ,color: Colors.black87,) )
                          ],
                        ): new Container(),
                        Container(
                          height: 220.0,
                          child: isrsvpclicked == 1 ? new Column(
                            children: <Widget>[
//                              new Padding(
//                                padding: const EdgeInsets.all(5.0),
//                                child: new Text(
//                                  "Will you be attending?",
//                                  style: new TextStyle(fontSize: 28.0, color: Theme.Colors.appsupportlabel, fontStyle: FontStyle.normal),
//                                ),
//                              ),
                              new Padding(
                                padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
                                child: Container(
                                  height: 100.0,
                                  child: new ListView.builder(
                                    itemCount: sampleData.length,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (BuildContext context, int index) {
                                      return new InkWell(
                                        //highlightColor: Colors.red,
                                        splashColor: index.toString() == "0" ? new Color(0xFF77DD77) : index.toString() == "1" ? new Color(0xFFFFC433): new Color(0xFFff6f69),
                                        onTap: () {
                                          setState(() {
                                            if(index == 0){ status = 1; print('selected rsvp button is Yes $index');}
                                            if(index == 1){ status = 2; print('selected rsvp button is Maybe $index');}
                                            if(index == 2){ status = 3; print('selected rsvp button is No $index');}
                                            print('selected rsvp button is $index');
                                            print('selected value ${sampleData[index].isSelected = false}');
                                            if(is_rsvp_created){
                                              // New record
                                              print('update api call');
                                              updateRSVP();
                                            }else{
                                              // update existing record
                                              print('create api call');
                                              submitRSVP();
                                            }
                                            sampleData.forEach((element) => element.isSelected = false);
                                            sampleData[index].isSelected = true;
                                            _handleRadioValueChange1(index);
                                          });
                                        },
                                        child: new RadioItem(sampleData[index]),
                                      );
                                    },
                                  ),
                                ),
                              ),

                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
                                child: new Divider(
                                  height: 5.0,
                                  color: Colors.black,
                                ),
                              ),
                              sampleData[0].isSelected == true || sampleData[1].isSelected == true ? new Center(
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(bottom:3.0),
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(Icons.supervised_user_circle),
                                          new Text("Additional guests: ",style: new TextStyle(color: Theme.Colors.app_dark, fontSize: 20.0),)
                                        ],
                                      ),
                                    ),
                                    new Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        _itemCount!= 0 ? new RawMaterialButton(
                                          onPressed: () {},
                                          shape: new CircleBorder(),
                                          elevation: 2.0,
                                          fillColor: Theme.Colors.appcolour,
                                          child: _itemCount!= 0 ?
                                          new  IconButton(padding: const EdgeInsets.all(8.0) ,icon: new Icon(Icons.remove,color: Colors.white,),
                                            onPressed: (){
                                              setState(()=>_itemCount--);
                                              updateRSVP();
                                              print("update api count --");
                                            },)
                                              :new Container(),
                                        ) : new Container(),
                                        new Text(_itemCount.toString(),style: new TextStyle(color: Theme.Colors.app_dark, fontSize: 25.0),),
                                        new RawMaterialButton(
                                          onPressed: () {},
                                          shape: new CircleBorder(),
                                          elevation: 2.0,
                                          fillColor: Theme.Colors.appcolour,
//                                  padding: const EdgeInsets.all(2.0),
                                          child: new IconButton(icon: new Icon(Icons.add,color: Colors.white,),
                                              onPressed: (){
                                                setState(()=>_itemCount++);
                                                updateRSVP();
                                                print("update api count ++");
                                              }),
                                        )
                                      ],
                                    ),

                                  ],
                                ),
                              ): new Container(),

                            ],
                          ) : new Container(),
                        ),
                      ],
                    ) : new Container(),

                    SizedBox(
                      height: 80.0,
                    )
                  ],
                ),

                /// Appbar
                new Align(
                  alignment: Alignment.topCenter,
                  child: new SizedBox.fromSize(
                    size: new Size.fromHeight(90.0),
                    child: new Stack(
                      alignment: Alignment.centerLeft,
                      children: <Widget>[
                        new SizedBox
                                .expand // If the user scrolled over the image
                            (
                          child: new Material(
                            color: colorTween1.value,
                          ),
                        ),
                        new SizedBox.fromSize // TODO: Animate the reveal
                            (
                          size: new Size.fromWidth(
                              MediaQuery.of(context).size.width * readPerc),
                          child: new Material(
                            color: colorTween2.value,
                          ),
                        ),
                        new Align(
                            alignment: Alignment.center,
                            child: new SizedBox.expand(
                              child:
                                  new Material // So we see the ripple when clicked on the iconbutton
                                      (
                                color: Colors.transparent,
                                child: new Container(
                                    margin: new EdgeInsets.only(
                                        top: 24.0), // For the status bar
                                    child: new Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        new IconButton(
                                          onPressed: () =>
                                              Navigator.of(context).pop(),
                                          icon: new Icon(Icons.arrow_back,
                                              color: Colors.white),
                                        ),
//                                        new Hero(
//                                            tag: 'Logo',
//                                            child: new Icon(Icons.event,
//                                                color: Colors.white,
//                                                size: 48.0)),
//                                        new IconButton(
//                                          onPressed: () =>
//                                              Navigator.of(context).pop(),
//                                          icon: new Icon(Icons.event_note,
//                                              color: Colors.white),
//                                        ),
                                      ],
                                    )),
                              ),
                            ))
                      ],
                    ),
                  ),
                ),

                /// Fade bottom text
                new Align(
                  alignment: Alignment.bottomCenter,
                  child: new SizedBox.fromSize(
                      size: new Size.fromHeight(50.0),
                      child: new Container(
                        decoration: new BoxDecoration(
                            gradient: new LinearGradient(
                                colors: [Colors.white12, Colors.white],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                stops: [0.0, 1.0])),
                      )),
                )
              ],
            ),
          ),
        ),
        floatingActionButton: new FloatingActionButton(
          onPressed: () async {
//            Add2Calendar.addEvent2Cal(event);
            if (Platform.isAndroid) {
              addToCalendar(platform);
            } else {
              final iosPlatform = const MethodChannel('kinder.flutter/s3');
              addToCalendar(iosPlatform);
            }
            // final FormState form = _formKey.currentState;
//             if (!form.validate()) {
//               _autovalidate = true; // Start validating on every change.
//               showInSnackBar('Please fix the errors in red before submitting.');
//             } else {
//               form.save();
// //              var createEventResult =
// //              await _deviceCalendarPlugin.createOrUpdateEvent(_event);
// //              if (createEventResult.isSuccess) {
// //                Navigator.pop(context, true);
// //              } else {
// //                showInSnackBar(createEventResult.errorMessages.join(' | '));
// //              }
//             }
          },
          child: new Icon(Icons.event),
        ));
  }

  void addToCalendar(MethodChannel channel) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("title", () => eventdata["title"]);
    args.putIfAbsent("description", () => eventdata["description"]);
    args.putIfAbsent("start_date", () => eventdata["date"]);
    args.putIfAbsent("end_date", () => eventdata["to_date"]);
    args.putIfAbsent("email", () => eventdata["email"]);
    args.putIfAbsent("location", () => '');

    await channel.invokeMethod('addToCalendar', args);
  }

  String _validateTitle(String value) {
    if (value.isEmpty) {
      return 'Name is required.';
    }

    return null;
  }

  DateTime _combineDateWithTime(DateTime date, TimeOfDay time) {
    final dateWithoutTime =
        DateTime.parse(new DateFormat("y-MM-dd 00:00:00").format(_startDate));
    return dateWithoutTime
        .add(new Duration(hours: time.hour, minutes: time.minute));
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  @override
  void onDisplayUserInfo(User user) {
    print("craete");
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
//      jwt = widget.jwt.toString();
//      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];
      print("****************************************** $userId");

    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }
}


class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Container(
      margin: new EdgeInsets.all(5.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
            height: 50.0,
            width: screenSize.width/4,
            child: new Center(
              child: new Text(_item.buttonText,
                  style: new TextStyle(
                      color:
                      _item.isSelected ? Colors.white : Colors.black,
                      //fontWeight: FontWeight.bold,
                      fontSize: 18.0)),
            ),
            decoration: new BoxDecoration(
              color: _item.isSelected
                  ? _item.index.toString() == "0" ? new Color(0xFF77DD77) : _item.index.toString() == "1" ? new Color(0xFFFFC433): new Color(0xFFff6f69)
                  : Colors.transparent,
              border: new Border.all(
                  width: 1.0,
                  color: _item.isSelected
//                      ? Colors.blueAccent
                      ? _item.index.toString() == "0" ? new Color(0xFF77DD77) : _item.index.toString() == "1" ? new Color(0xFFFFC433): new Color(0xFFff6f69)
                      : Colors.grey),
              borderRadius: const BorderRadius.all(const Radius.circular(10.0)),
            ),
          ),
          new SizedBox(width: 5.0,)
//          new Container(
//            margin: new EdgeInsets.only(left: 2.0),
//            child: new Text("${_item.index.toString()}"),
//          )
        ],
      ),
    );
  }
}

class RadioModel {
  bool isSelected;
  final String buttonText;
  final int index;

  RadioModel(this.isSelected, this.buttonText,this.index);
}