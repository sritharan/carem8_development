import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:carem8/Theme.dart' as Kinderm8Theme;
import 'package:flutter/material.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/dailychart/dailychartdata.dart';
import 'package:carem8/pages/dailyjournal/dailyjournaldata.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:flutter_calendar/flutter_calendar.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:flutter_html_textview/flutter_html_textview.dart';

class DailyChart extends StatefulWidget {
  final ChildViewModal childData;
  final jwt;
  DailyChart(this.childData, this.jwt);
  @override
  DailyChartState createState() => DailyChartState();
}

class DailyChartState extends State<DailyChart> implements HomePageContract {
  var k, appuser, jwt, id, clientId;
  List data;
  var childId;
  bool isLoading = false;
  HomePagePresenter _presenter;
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var dailychartdate;
  var dailychartcollection,
      foodcollection,
      bottlefeedcollection,
      nappycollection,
      restcollection,
      sunscreencollection;

  var selectedChild;
  var selectedChildId;

  bool enabled = false;
  bool expanded = false,
      foodexpanded = false,
      nappyexpanded = false,
      bottleexpanded = false,
      restexpanded = false,
      sunscreenexpanded = false,
      sleepCheck = false;

  DailyChartState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }
  @override
  void initState() {
    var date = new DateTime.now();
    fetchDailyChartData(date);
    super.initState();
  }

  void handleNewDate(date) {
    setState(() {
      isLoading = false;
    });
//    print("handleNewDate ${date}");
    var timeStamp = new DateFormat("dd-MM-yyyy");
    String formatdailychartdate = timeStamp.format(date);
//    print("handleNewDate>>formatdailychartdate ${formatdailychartdate}");
    fetchDailyChartData(date);
  }

  var progress = new ProgressBar(
    color: Kinderm8Theme.Colors.appcolour,
    containerColor: Kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  // 27-09-2018
  Future<String> fetchDailyChartData(date) async {
    var timeStamp = new DateFormat("dd-MM-yyyy");
    String formatdailychartdate = timeStamp.format(date);

    ///data from GET method
    print("data fetched");
    childId = widget.childData.id;
    String _dailyJournalUrl =
        'https://apicare.carem8.com/v2.1.1/getdailychartenteries/$childId?date=$formatdailychartdate&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_dailyJournalUrl, headers: headers).then((response) {
      var dailyChartData;
      try {
        print(response.statusCode);
        dailyChartData = json.decode(response.body) ?? '';

        print('dailychartcollection UrlData $dailychartcollection');
      } catch (e) {
        print('That string was null!');
      }

      if (response.statusCode == 200) {
        print('isLoading$isLoading');
        dailychartcollection = dailyChartData;
        print('dailychartcollection UrlData $dailychartcollection');
        setState(() {
          isLoading = true;
        });
      } else if (response.statusCode == 500 &&
          dailyChartData["errorType"] == 'ExpiredJwtException') {
        getRefreshToken(date);
      } else {
        fetchDailyChartData(date);
      }
    });
    return null;
  }

  getRefreshToken(date) {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;
      if (date != null) {
        fetchDailyChartData(date);
      }
    });
  }

  /* ---------------- */
  /* Food collection list */
  /* --------------- */
  foodcollectionUIList() {
    if (dailychartcollection['dailyChartFoodList'][0]['dailychartfoodlist']
        .length >
        0) {
      return Row(
        children: <Widget>[
          new Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemCount: dailychartcollection['dailyChartFoodList'][0]
              ['dailychartfoodlist'] !=
                  null
                  ? dailychartcollection['dailyChartFoodList'][0]
              ['dailychartfoodlist']
                  .length
                  : 0,
              itemBuilder: (context, index) {
                var foodMenu = html2md.convert(
                    dailychartcollection['dailyChartFoodList'][0]
                    ['dailychartfoodlist'][index]['FoodMenu']);
                var menuCategory = html2md.convert(
                    dailychartcollection['dailyChartFoodList'][0]
                    ['dailychartfoodlist'][index]['MenuCategory']);
                return Card(
                  elevation: 2.0,
                  child: ListTile(
                    title: Html(
                      data: menuCategory + ' - ' + foodMenu,
                      defaultTextStyle: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    /*Text(
                        dailychartcollection['dailyChartFoodList'][0]
                                ['dailychartfoodlist'][index]['MenuCategory'] +
                            ' - ' +
                            dailychartcollection['dailyChartFoodList'][0]
                                ['dailychartfoodlist'][index]['FoodMenu'],
                        style: TextStyle()),*/
                    subtitle: Text(
                        dailychartcollection['dailyChartFoodList'][0]
                        ['dailychartfoodlist'][index]['Servetype'],
                        style: TextStyle()),
                  ),
                );
              },
            ),
          )
        ],
      );
    }
  }

  /* ---------------- */
  /* Nappy collection list */
  /* --------------- */
  nappycollectionUIList() {
    if (dailychartcollection['dailyChartNappyList'][0]['dailyChartNappyList']
        .length >
        0) {
      return Row(
        children: <Widget>[
          new Expanded(
            child: ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: dailychartcollection['dailyChartNappyList'][0]
              ['dailyChartNappyList'] !=
                  null
                  ? dailychartcollection['dailyChartNappyList'][0]
              ['dailyChartNappyList']
                  .length
                  : 0,
              itemBuilder: (context, index) {
                var note = html2md.convert(
                    dailychartcollection['dailyChartNappyList'][0]
                    ['dailyChartNappyList'][index]['note']);

                return Card(
                  elevation: 2.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                            dailychartcollection['dailyChartNappyList'][0]
                            ['dailyChartNappyList'][index]['catagory'],
                            style: TextStyle()),
                        contentPadding: EdgeInsets.only(left: 16.0),
                        trailing: Text(
                            dailychartcollection['dailyChartNappyList'][0]
                            ['dailyChartNappyList'][index]
                            ['trainingtype'] +
                                ' @ ' +
                                dailychartcollection['dailyChartNappyList'][0]
                                ['dailyChartNappyList'][index]['time'],
                            style: TextStyle()),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16.0),
                        child: new Html(data: note),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Icon(FontAwesomeIcons.userCircle,
                              size: 12.0, color: Colors.grey),
                          Text(
                            ' ' +
                                dailychartcollection['dailyChartNappyList'][0]
                                ['dailyChartNappyList'][index]['trainer'],
                            style: TextStyle(
                              color: Kinderm8Theme.Colors.darkGrey,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          )
        ],
      );
    }
  }

  /* ---------------- */
  /* Sleep and rest collection list */
  /* --------------- */
  restcollectionUIList() {
    final Size screenSize = MediaQuery.of(context).size;

    if (dailychartcollection['dailyChartRestList'][0]['dailyRestTimeList']
        .length >
        0) {
      return Row(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemCount: dailychartcollection['dailyChartRestList'][0]
              ['dailyRestTimeList'] !=
                  null
                  ? dailychartcollection['dailyChartRestList'][0]
              ['dailyRestTimeList']
                  .length
                  : 0,
              itemBuilder: (context, index) {
                var restType = html2md.convert(
                    dailychartcollection['dailyChartRestList'][0]
                    ['dailyRestTimeList'][index]['resttype']);
                return Card(
                  elevation: 2.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                            dailychartcollection['dailyChartRestList'][0]
                            ['dailyRestTimeList'][index]['start_time'] +
                                ' - ' +
                                dailychartcollection['dailyChartRestList'][0]
                                ['dailyRestTimeList'][index]['end_time'],
                            style: TextStyle(
                                color: Kinderm8Theme.Colors.darkGrey)),
                        subtitle: Html(
                          data: restType,
                          defaultTextStyle: TextStyle(
                              color: Kinderm8Theme.Colors.darkGrey
                                  .withOpacity(.75)),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Icon(FontAwesomeIcons.userCircle,
                              size: 12.0, color: Colors.grey),
                          Text(
                            ' ' +
                                dailychartcollection['dailyChartRestList'][0]
                                ['dailyRestTimeList'][index]['trainer'],
                            style: TextStyle(
                              color: Kinderm8Theme.Colors.darkGrey,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          )
        ],
      );
    }
  }

  /* ---------------- */
  /* Bottlefeed collection list */
  /* --------------- */
  bottlecollectionUIList() {
    if (dailychartcollection['dailyChartBottlefeedList'][0]
    ['dailyChartBottlefeedList']
        .length >
        0) {
      return Row(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: dailychartcollection['dailyChartBottlefeedList'][0]
              ['dailyChartBottlefeedList'] !=
                  null
                  ? dailychartcollection['dailyChartBottlefeedList'][0]
              ['dailyChartBottlefeedList']
                  .length
                  : 0,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 2.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      ListTile(
                        subtitle: Text(
                            dailychartcollection['dailyChartBottlefeedList'][0]
                            ['dailyChartBottlefeedList'][index]
                            ['bottle_qty'] +
                                ' (ml)',
                            style: TextStyle()),
                        title: Text(
                            dailychartcollection['dailyChartBottlefeedList'][0]
                            ['dailyChartBottlefeedList'][index]
                            ['bottletype'],
                            style: TextStyle()),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Icon(FontAwesomeIcons.userCircle,
                              size: 12.0, color: Colors.grey),
                          Text(
                            ' ' +
                                dailychartcollection['dailyChartBottlefeedList']
                                [0]['dailyChartBottlefeedList'][index]
                                ['trainer'],
                            style: TextStyle(
                              color: Kinderm8Theme.Colors.darkGrey,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          )
        ],
      );
    }
  }

  /* ---------------- */
  /* Sunscreen collection list */
  /* --------------- */
  suscreencollectionUIList() {
    if (dailychartcollection['dailyChartSunScreenList'][0]
    ['dailySunScreenLists']
        .length >
        0) {
      return Row(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: dailychartcollection['dailyChartSunScreenList'][0]
              ['dailySunScreenLists'] !=
                  null
                  ? dailychartcollection['dailyChartSunScreenList'][0]
              ['dailySunScreenLists']
                  .length
                  : 0,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 2.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                            dailychartcollection['dailyChartSunScreenList'][0]
                            ['dailySunScreenLists'][index]['category_name'],
                            style: TextStyle()),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Icon(FontAwesomeIcons.userCircle,
                              size: 12.0, color: Colors.grey),
                          Text(
                            dailychartcollection['dailyChartSunScreenList'][0]
                            ['dailySunScreenLists'][index]['educator_name'],
                            style: TextStyle(
                              color: Kinderm8Theme.Colors.darkGrey,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          )
        ],
      );
    }
  }

  /* ---------------- */
  /* Sleep Check collection list */
  /* --------------- */
  sleepCheckCollectionUIList() {
    if (dailychartcollection['dailyChartSleepCheckList'][0]
    ['dailyChartSleepCheckList']
        .length >
        0) {
      return Row(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              physics: ClampingScrollPhysics(),
              shrinkWrap: true,
              itemCount: dailychartcollection['dailyChartSleepCheckList'][0]
              ['dailyChartSleepCheckList'] !=
                  null
                  ? dailychartcollection['dailyChartSleepCheckList'][0]
              ['dailyChartSleepCheckList']
                  .length
                  : 0,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 2.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                            dailychartcollection['dailyChartSleepCheckList'][0]
                            ['dailyChartSleepCheckList'][index]['time'],
                            style: TextStyle()),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Icon(FontAwesomeIcons.userCircle,
                              size: 12.0, color: Colors.grey),
                          Text(
                            dailychartcollection['dailyChartSleepCheckList'][0]
                            ['dailyChartSleepCheckList'][index]['trainer'],
                            style: TextStyle(
                              color: Kinderm8Theme.Colors.darkGrey,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          )
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: scaffoldKey,
        appBar: new AppBar(
          title: new Text(labelsConfig["dailyChartLabel"],
              style: TextStyle(
                color: Colors.white,
              )),
          backgroundColor: Kinderm8Theme.Colors.appdarkcolour,
          centerTitle: true,
        ),
        body: new Container(
          margin: new EdgeInsets.symmetric(
            horizontal: 5.0,
            vertical: 10.0,
          ),
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              new Calendar(
                onSelectedRangeChange: (range) =>
                    print("Range is ${range.item1}, ${range.item2}"),
                onDateSelected: (date) => handleNewDate(date),isExpandable: true,
              ),
              new Divider(
                height: 50.0,
              ),

              // Daily Chart //
              /**/
              isLoading
                  ? Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  children: <Widget>[
                    /* Food */
                    dailychartcollection['dailyChartFoodList'].length > 0
                        ? new ExpansionPanelList(
                      expansionCallback: (i, bool val) {
                        setState(() {
                          foodexpanded = !val;
                        });
                      },
                      children: [
                        new ExpansionPanel(
                          body: new Container(
                              child: foodcollectionUIList()),
                          headerBuilder:
                              (BuildContext context, bool val) {
                            return new Center(
                                child: new ListTile(
                                    leading: new CircleAvatar(
                                      foregroundColor: Kinderm8Theme
                                          .Colors.appcolour,
                                      backgroundImage: AssetImage(
                                          "assets/iconsetpng/groceries.png"),
                                      radius: 20.0,
                                    ),
                                    title: dailychartcollection[
                                    'dailyChartFoodList']
                                    [0]['date'] !=
                                        null
                                        ? Text(
                                      '${dailychartcollection['dailyChartFoodList'][0]['date']}',
                                      textAlign:
                                      TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          color: Kinderm8Theme
                                              .Colors
                                              .appcolour),
                                    )
                                        : Text("")));
                          },
                          isExpanded: foodexpanded,
                        ),
                      ],
                    )
                        : new Container(),

                    /* Nappy */
                    dailychartcollection['dailyChartNappyList'].length > 0
                        ? new ExpansionPanelList(
                      expansionCallback: (i, bool val) {
                        setState(() {
                          nappyexpanded = !val;
                        });
                      },
                      children: [
                        new ExpansionPanel(
                          body: new Container(
                              child: nappycollectionUIList()),
                          headerBuilder:
                              (BuildContext context, bool val) {
                            return new Center(
                                child: new ListTile(
                                    leading: new CircleAvatar(
                                      foregroundColor: Kinderm8Theme
                                          .Colors.appcolour,
                                      backgroundImage: AssetImage(
                                          "assets/iconsetpng/crib.png"),
                                      radius: 20.0,
                                    ),
                                    title: dailychartcollection[
                                    'dailyChartNappyList']
                                    [0]['date'] !=
                                        null
                                        ? Text(
                                      '${dailychartcollection['dailyChartNappyList'][0]['date']}',
                                      textAlign:
                                      TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          color: Kinderm8Theme
                                              .Colors
                                              .appcolour),
                                    )
                                        : Text("")));
                          },
                          isExpanded: nappyexpanded,
                        ),
                      ],
                    )
                        : new Container(),

                    /* Sleep and Rest */
                    dailychartcollection['dailyChartRestList'].length > 0
                        ? new ExpansionPanelList(
                      expansionCallback: (i, bool val) {
                        setState(() {
                          restexpanded = !val;
                        });
                      },
                      children: [
                        new ExpansionPanel(
                          body: new Container(
                              child: restcollectionUIList()),
                          headerBuilder:
                              (BuildContext context, bool val) {
                            return new Center(
                                child: new ListTile(
                                    leading: new CircleAvatar(
                                      foregroundColor: Kinderm8Theme
                                          .Colors.appcolour,
                                      backgroundImage: AssetImage(
                                          "assets/iconsetpng/alarm-clock.png"),
                                      radius: 20.0,
                                    ),
                                    title: Text(
                                        '${dailychartcollection['dailyChartRestList'][0]['date']}',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Kinderm8Theme
                                                .Colors
                                                .appcolour))));
                          },
                          isExpanded: restexpanded,
                        ),
                      ],
                    )
                        : new Container(),

                    /* Bottle Feed */
                    dailychartcollection['dailyChartBottlefeedList']
                        .length >
                        0
                        ? new ExpansionPanelList(
                      expansionCallback: (i, bool val) {
                        setState(() {
                          bottleexpanded = !val;
                        });
                      },
                      children: [
                        new ExpansionPanel(
                          body: new Container(
                              child: dailychartcollection[
                              'dailyChartBottlefeedList']
                              [0]['date'] !=
                                  null
                                  ? bottlecollectionUIList()
                                  : null),
                          headerBuilder:
                              (BuildContext context, bool val) {
                            return new Center(
                                child: new ListTile(
                                    leading: new CircleAvatar(
                                      foregroundColor: Kinderm8Theme
                                          .Colors.appcolour,
                                      backgroundImage: AssetImage(
                                          "assets/iconsetpng/bottle-feed.png"),
                                      radius: 20.0,
                                    ),
                                    title: dailychartcollection[
                                    'dailyChartBottlefeedList']
                                    [0]['date'] !=
                                        null
                                        ? Text(
                                      '${dailychartcollection['dailyChartBottlefeedList'][0]['date']}',
                                      textAlign:
                                      TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          color: Kinderm8Theme
                                              .Colors
                                              .appcolour),
                                    )
                                        : Text("")));
                          },
                          isExpanded: bottleexpanded,
                        ),
                      ],
                    )
                        : new Container(),

                    /* SunScreen */
                    dailychartcollection['dailyChartSunScreenList']
                        .length >
                        0
                        ? new ExpansionPanelList(
                      expansionCallback: (i, bool val) {
                        setState(() {
                          sunscreenexpanded = !val;
                        });
                      },
                      children: [
                        new ExpansionPanel(
                          body: new Container(
                              child: dailychartcollection[
                              'dailyChartSunScreenList']
                              [0]['date'] !=
                                  null
                                  ? suscreencollectionUIList()
                                  : Container()),
                          headerBuilder:
                              (BuildContext context, bool val) {
                            return new Center(
                                child: new ListTile(
                                    leading: new CircleAvatar(
                                      foregroundColor: Kinderm8Theme
                                          .Colors.appcolour,
                                      backgroundImage: AssetImage(
                                          "assets/iconsetpng/sunscreen.png"),
                                      radius: 20.0,
                                    ),
                                    title: dailychartcollection[
                                    'dailyChartSunScreenList']
                                    [0]['date'] !=
                                        null
                                        ? Text(
                                      '${dailychartcollection['dailyChartSunScreenList'][0]['date']}',
                                      textAlign:
                                      TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          color: Kinderm8Theme
                                              .Colors
                                              .appcolour),
                                    )
                                        : Text("")));
                          },
                          isExpanded: sunscreenexpanded,
                        ),
                      ],
                    )
                        : new Container(),

                    /* Sleep Check */
                    dailychartcollection['dailyChartSleepCheckList']
                        .length >
                        0
                        ? new ExpansionPanelList(
                      expansionCallback: (i, bool val) {
                        setState(() {
                          sleepCheck = !val;
                        });
                      },
                      children: [
                        new ExpansionPanel(
                          body: new Container(
                              child: dailychartcollection[
                              'dailyChartSleepCheckList']
                              [0]['date'] !=
                                  null
                                  ? sleepCheckCollectionUIList()
                                  : Container()),
                          headerBuilder:
                              (BuildContext context, bool val) {
                            return new Center(
                                child: new ListTile(
                                    leading: new CircleAvatar(
                                      foregroundColor: Kinderm8Theme
                                          .Colors.appcolour,
                                      backgroundImage: AssetImage(
                                          "assets/iconsetpng/sunscreen.png"),
                                      radius: 20.0,
                                    ),
                                    title: dailychartcollection[
                                    'dailyChartSleepCheckList']
                                    [0]['date'] !=
                                        null
                                        ? Text(
                                      '${dailychartcollection['dailyChartSleepCheckList'][0]['date']}',
                                      textAlign:
                                      TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          color: Kinderm8Theme
                                              .Colors
                                              .appcolour),
                                    )
                                        : Text("")));
                          },
                          isExpanded: sleepCheck,
                        ),
                      ],
                    )
                        : new Container(),
                  ],
                ),
              )
                  : progress
            ],
          ),
        ));
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = widget.jwt.toString();
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
