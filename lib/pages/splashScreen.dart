import 'dart:async';
import 'dart:convert';
import 'package:carem8/pages/home/modals/child_summary_modal.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:carem8/pages/home/services/daily_summary_service.dart';
import 'package:carem8/pages/newsfeed/newsfeedlist.dart';
import 'package:flutter/material.dart';
import 'package:carem8/auth.dart';
import 'package:carem8/Theme.dart' as Theme;
import 'package:carem8/data/database_helper.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart' as http;

class UserChildDataInheritedWidget extends InheritedWidget {
  final _SplashPageState data;
  const UserChildDataInheritedWidget ({
             Key key,
             this.data,
             @required Widget child,
           })
      : assert(child != null),
        super(key: key, child: child);

  static UserChildDataInheritedWidget of (BuildContext context) {
    return context.inheritFromWidgetOfExactType(UserChildDataInheritedWidget) as UserChildDataInheritedWidget;
  }

  @override
  bool updateShouldNotify (UserChildDataInheritedWidget old) {
    return old.child != child;
  }
}

class SplashPage extends StatefulWidget {
  @override
  State createState() => new _SplashPageState();
}

class _SplashPageState extends State<SplashPage> implements HomePageContract,AuthStateListener {
  _SplashPageState() {
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  var email, password, client_id, center;
  var childid, appuser, jwt, userId, clientId;
  var childrendata, image;
  var devicetoken;
  //fetch children data to a List from db
  BuildContext ctx;
  List<ChildViewModal> childrenData = [];
  bool isLoading;

  // Push
  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  @override
  void initState() {
    isLoading = true;
    super.initState();

    // TODO: get push token and send to login
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message){
        print('onMessage $message');
      },
      onResume: (Map<String, dynamic> message){
        print('onResume $message');
      },
      onLaunch: (Map<String, dynamic> message){
        print('onLaunch $message');
      },
    );
    _firebaseMessaging.getToken().then((token){
      print('device token : $token');
      devicetoken = token;
    });
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  @override
  Widget build(BuildContext context) {
    ctx = context;
    return Scaffold(
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(color: Theme.Colors.appsplashscreen),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Image(
                          image: AssetImage(
                              "assets/logo.png"),
                          height: 100.0,
                          width: 400.0,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.0),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(backgroundColor: Colors.amber,),
                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Text(
                        'Welcome to Care M8',
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0,
                            color: Colors.white),
                      )
                    ],
                  ),
                )
              ],
            )
          ],
        ),
    );
  }

  Future fetchData(List children) async {
    bool isImageAvailable = true;

    if (children.length > 0) {
      await getRefreshToken(childId: children[0]["id"].toString());
    }

    childrenData = children.map<ChildViewModal>((element) {
      precacheImage(NetworkImage("http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${element["image"]}"), ctx, onError: (err, stackT) => isImageAvailable = false);
      return ChildViewModal(
          id: element["id"],
          iconAssetPath: isImageAvailable ? element["image"] : '',
          firstName: element["firstname"],
          lastName: element["lastname"],
          room: element["room"],
          gender: element["gender"],
          attendance: element["attendance"],
          dob: element["dob"],);
    }).toList();

    setState(() {
      isLoading = false;
    });
  }

  @override
  Future onAuthStateChanged(AuthState state) async {
    var db = new DatabaseHelper();
    User user = await db.getFirstUser();
    await checkIsPasswordChanged(user).then((flag) {
      if (flag) {
        state = AuthState.LOGGED_OUT;
        db.deleteUser();
      }
    });
    if (state == AuthState.LOGGED_IN) {
      final parsed = json.decode(user.center);
      var childDetails = parsed[0];
      await fetchData(childDetails['children']);
      Navigator.pushReplacement(
          ctx,
          new MaterialPageRoute(
              builder: (context) => new NewsFeedList(
                childrenData: childrenData,
              )));
    }else{
      Timer(Duration(seconds: 3),
              () => Navigator.of(ctx).pushReplacementNamed('/login'));
    }
  }

  @override
  void onDisplayUserInfo(User user) {
    email = user.email;
    password = user.password;
    client_id = user.client_id;
    var appUser = user.center;
    setState(() {
      try {
        final parsed = json.decode(appUser);
        var details = parsed[0];
        var users = details["user"];
        jwt = details["jwt"];
        userId = users["id"];
        clientId = users["client_id"];
        childrendata = details["children"];
        // Setup first child to load daily summary
        if (users['id'] != null) {
          print('calling setup route');
          print(users.toString());
        }
      } catch (e) {
        print('That string was null!');
      }
    });
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  Future<bool> checkIsPasswordChanged(User user) {
    final baseUrl = 'https://apicare.carem8.com/v2.1.0/setupuseroneapp';
    String requestBody = json.encode({"email": user?.email, "password": user?.password, "client_id": clientId});
    return http.post(baseUrl, body: requestBody).then((resp) {
      if (resp.statusCode == 401) {
        return true;
      } else {
        return false;
      }
    });
  }
}
