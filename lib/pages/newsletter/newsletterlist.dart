import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';

import 'package:carem8/Theme.dart' as Theme;
import 'package:carem8/pages/commondrawer.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/newsletter/newsletterdata.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';

class Newsletter extends StatefulWidget {
  Newsletter({this.childrenData});

  final List<ChildViewModal> childrenData;

@override
  NewsletterState createState() => NewsletterState();
}

class NewsletterState extends State<Newsletter>
    implements HomePageContract {
  List Newsletterdata;
  var k, appuser, jwt, id, clientId;
  ScrollController _scrollController = new ScrollController();
  bool isLoading;
  bool load = true;
  HomePagePresenter _presenter;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  NewsletterState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  @override
  void initState() {
    super.initState();
    fetchNewsletterData(0);
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  Future<String> fetchNewsletterData(int s) async {
    ///data from GET method
    print("Newsletter data fetched");

    String _notificationUrl =
        'https://apicare.carem8.com/v2.1.1/getnewsletters?step=$s&userid=$id&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_notificationUrl, headers: headers).then((response) {
      var NewsletterData;
      try {
        NewsletterData = json.decode(response.body);
        print(NewsletterData.length);
        print('res get ${response.body}');
        print('notificationData $NewsletterData');
      } on FormatException catch (e) {
        print("That string didn't look like Json.");
      } on NoSuchMethodError catch (e) {
        print('That string was null!');
      }

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(isLoading);
        isLoading = false;
        print(isLoading);
        if (s == 0) {
          setState(() {
            load = false;
            this.Newsletterdata = NewsletterData;
          });
        } else {
          setState(() {
            load = false;
            Newsletterdata.addAll(NewsletterData);
          });
        }
        k = Newsletterdata.length;
      } else if (response.statusCode == 500 &&
          NewsletterData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        fetchNewsletterData(0);
      }
    });
    return null;
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } on FormatException catch (e) {
        print("That string didn't look like Json.");
      } on NoSuchMethodError catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (k != null) {
        fetchNewsletterData(k);
      } else {
        fetchNewsletterData(0);
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(labelsConfig["newsletterLabel"],
            style: TextStyle(
              color: Colors.white,
            )
        ),
        backgroundColor: Theme.Colors.appdarkcolour,
        centerTitle: true,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.refresh),
              tooltip: 'Refresh',
              onPressed: () {
                print("Reload..");
                setState(() {
                  load = true;
                });
                fetchNewsletterData(0);
              })
        ],
      ),
      drawer: CommonDrawer(childrenData: widget.childrenData,),
      body: new RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: handleRefresh,
        child: new Center(
          child: load
              ? progress
              : new ListView.builder(
                  itemCount: this.Newsletterdata != null ? (this.Newsletterdata.length + 1) : 0,
                  itemBuilder: (context, i) {
                    if (i == k) {
                      return k > 4 ? _buildCounterButton() : SizedBox();
                    } else {
                      final data1 = this.Newsletterdata[i];
                      return NewsletterData(data1);
                    }
                  },
                ),
        ),
      ),
    );
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: isLoading
            ? new CupertinoActivityIndicator()
            : const Text('Load more...',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: Theme.Colors.app_white)),
        color: Theme.Colors.appseconderycolour,
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: _counterButtonPress());
  }

  Function _counterButtonPress() {
    if (isLoading) {
      return null;
    } else {
      return () {
        setState(() {
          isLoading = true;
        });
        fetchNewsletterData(k);
      };
    }
  }

  Future<Null> handleRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      load = true;
      fetchNewsletterData(0);
    });
    return null;
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

      print("iddd $id");
      print(clientId);
    } on FormatException catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }
}
