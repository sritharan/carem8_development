import 'dart:convert';
import 'dart:io';

import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:carem8/auth.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/settings/editemail.dart';
import 'package:carem8/pages/settings/image_picker_handler.dart';
import 'package:carem8/pages/settings/resetpassword.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class UserSettings extends StatefulWidget {
  @override
  UserSettingsState createState() =>
      new UserSettingsState(image, displayname, displayemail);
  final ImageProvider image;
  final displayname;
  final displayemail;

  UserSettings(this.image, this.displayname, this.displayemail);
}

class UserSettingsState extends State<UserSettings>
    with TickerProviderStateMixin, ImagePickerListener
    implements HomePageContract, AuthStateListener {
  BuildContext _ctx;
  final ImageProvider image;
  final displayname;
  final displayemail;

  var appuser, id, jwt, client_id, children, users;
  HomePagePresenter presenter;

  var email, password;
  var load = true;
  bool isLoading;
  HomePagePresenter _presenter;
  ScrollController controller;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  // profile pic change variables

  File _image;
  AnimationController _controller;
  ImagePickerHandler imagePicker;
  BuildContext ctx;
  var awsFolder;
  bool isImageUploading = false;
  final platform = const MethodChannel('proitzen.kinderm8/s3_image');

  //  init state for time duration

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
    imagePicker = new ImagePickerHandler(this, _controller);
    imagePicker.init();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  var progress = new ProgressBar(
    //    backgroundColor: Theme.Colors.progressbackground ,
    color: kinderm8Theme.Colors.appcolour,
    containerColor: kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  UserSettingsState(this.image, this.displayname, this.displayemail) {
    presenter = new HomePagePresenter(this);
    presenter.getUserInfo();
  }

  @override
  userImage(File _image) async {
    setState(() {
      this._image = _image;
    });
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => AlertDialog(
              title: Center(child: CircularProgressIndicator()),
              content: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Uploading...'),
                ],
              ),
            ));
    String path;

    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("imagePath", () => _image.path);

    args.putIfAbsent("clientId", () => client_id);

    String result = await platform.invokeMethod('changeProfileImage', args);

    if (result != null) {
      final resetUrl =
          "https://apicare.carem8.com/v2.1.0/updateusersettingsoneapp?clientid=$client_id";

      String imageName = _image.path.split("/").last;

      var body = {"client_user_id": id, "image": "img/uploads/$imageName"};
      print("print $body");
      var headers = {"x-authorization": jwt.toString(), "clientid": client_id};
      NetworkUtil _netUtil = new NetworkUtil();

      await _netUtil
          .post(resetUrl, body: body, headers: headers, encoding: jwt.toString())
          .then((res) {
        print("ress $res");
        if (res == 1) {
          String _refreshTokenUrl =
              'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$client_id';

          print(_refreshTokenUrl);

          _netUtil.get(_refreshTokenUrl).then((response) {
            print("refresh get ${response.body}");
            var refreshJwtToken;
            try {
              refreshJwtToken = json.decode(response.body);
            } catch (e) {
              print('That string was null!');
            }
            this.jwt = refreshJwtToken;
          });
        } else {
          print("error");
        }
      });
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    ctx = context;
    print("hey.........$_image");
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          elevation: 0.0,
          title: new Text(labelsConfig["settingsLabel"]),
          backgroundColor:
              kinderm8Theme.Colors.appdarkcolour, //centerTitle: true,
        ),
        floatingActionButton: new FloatingActionButton(
            elevation: 0.0,
            child: new Icon(Icons.edit),
            backgroundColor: kinderm8Theme.Colors.appcolour,
            onPressed: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new EmailChange()));
            }),
        body: new ListView(
          children: <Widget>[
            Container(
              color: kinderm8Theme.Colors.appdarkcolour,
              padding: EdgeInsets.only(top: 10.0, bottom: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    width: screenSize.width / 4,
                    height: screenSize.width / 4,
                    decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: (_image != null) ? ExactAssetImage(_image.path) : image,
                      ),
                      border: Border.all(
                          style: BorderStyle.solid,
                          width: 4.0,
                          color: kinderm8Theme.Colors.app_white),
                    ),
//                    child: Container(
//                      alignment: Alignment.bottomRight,
//                      child: Container(
//                        decoration: BoxDecoration(
//                            color: Colors.white,
//                            shape: BoxShape.circle,
//                            border: Border.all(
//                                style: BorderStyle.solid,
//                                width: 2.0,
//                                color: Colors.white)),
//                        child: new InkWell(
//                          child: new Icon(
//                            Icons.camera_alt,
//                            color: kinderm8Theme.Colors.appcolour,
//                            size: 25.0,
//                          ),
//                          onTap: () => imagePicker.showDialog(context),
//                        ),
//                      ),
//                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 20.0,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: new Text(
                          labelsConfig["userSettingsLabel"],
                          style: TextStyle(
                            color: kinderm8Theme.Colors.appcolour,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              height: 10.0,
            ),
            Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  child: Card(
                    elevation: 1.0,
//                    height: 80.0,
//                    color: kinderm8Theme.Colors.progressbackground,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          //   mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    new Icon(
                                      Icons.account_box,
                                      size: 20.0,
                                      color: Colors.black38,
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                    new Text(
                                      "Full Name:",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0,
                                          color:
                                              kinderm8Theme.Colors.appcolour),
                                      //textScaleFactor: 1.0
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: Row(
                                  children: <Widget>[
                                    new Icon(
                                      Icons.email,
                                      size: 20.0,
                                      color: Colors.black38,
                                    ),
                                    Container(
                                      width: 5.0,
                                    ),
                                    new Text("Email:",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                            color: kinderm8Theme
                                                .Colors.appcolour)),
                                    Container(
                                      width: 5.0,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new Text(
                                widget.displayname,
                                style: TextStyle(
                                  color: Colors.black,
                                  //  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new Text(
                                widget.displayemail,
                                style: TextStyle(
                                  color: Colors.black,
                                  //  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: Container(
                        alignment: Alignment.topRight,
                        decoration: ShapeDecoration(
                            shape: CircleBorder(
                                side: BorderSide(
                          width: 2.0,
                          color: Colors.white,
                        ))),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              height: 10.0,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                        color: kinderm8Theme.Colors.appcolour,
                        child: new Center(
                          child: new Row(
                              // Repl
                              mainAxisAlignment: MainAxisAlignment.center,
                              // ace with a Row for horizontal icon + text
                              children: <Widget>[
                                Icon(
                                  Icons.vpn_key,
                                  color: Colors.white,
                                ),
                                Text(" Change password",
                                    style: TextStyle(
                                        fontSize: 20.0, color: Colors.white)),
                              ]),
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new ResetPassword()));
                        }),
                  )
                ],
              ),
            ),
          ],
        ));
  }

  @override
  void onDisplayUserInfo(User user) {
    setState(() {
      password = user.password;
      appuser = user.center;
      children = user.children;

      print("children $children");
      print("appuser $appuser");

      try {
        final parsed = json.decode(appuser);
        var appusers = parsed[0];
        print(appusers);
        print("******${appusers["jwt"]}");
        jwt = appusers["jwt"];
        users = appusers["user"];
        var child = appusers["children"];

        client_id = users["client_id"];
//        _image = users["image"];

        id = users["id"];
        email = users["email"];
        print("###########");
        print("child ${child[0]}");
      } catch (e) {
        print(e);
      }
    });
  }

  @override
  void onErrorUserInfo() {}

  @override
  void onLogoutUser() {
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
  }

  @override
  void onAuthStateChanged(AuthState state) {
    print("Navigate");
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(_ctx)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
  }

  @override
  void onUpdateJwt() {}
}
