import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/cupertino.dart';
import 'package:carem8/auth.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'dart:convert';

import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/utils/network_util.dart';

class ResetPassword extends StatefulWidget {
  @override
  ResetPasswordState createState() => ResetPasswordState();
}

class ResetPasswordState extends State<ResetPassword>
    with SingleTickerProviderStateMixin
    implements HomePageContract, AuthStateListener {
  AnimationController _iconAnimationController;
  Animation<double> _iconAnimation;
  final TextEditingController _textController1 = new TextEditingController();
  final TextEditingController _textController2 = new TextEditingController();
  final TextEditingController _textController3 = new TextEditingController();
  HomePagePresenter _presenter;

  bool error_oldpass = false;
  bool error_newpass = false;
  bool error_confirmpass = false;
  bool _obscureText1 = true;
  bool _obscureText2 = true;
  bool _obscureText3 = true;

  final formKey = new GlobalKey<FormState>();
  void _toggleReset1() {
    setState(() {
      _obscureText1 = !_obscureText1;
    });
  }

  void _toggleReset2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }

  void _toggleReset3() {
    setState(() {
      _obscureText3 = !_obscureText3;
    });
  }

  var email, token, status, username;

  var password;
  var newpassword;
  var confirmpassword;
  var appuser, jwt, clientId, userId, logedinpassword;
  HomePagePresenter presenter;

  ResetPasswordState() {
    presenter = new HomePagePresenter(this);

    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
    presenter.getUserInfo();
  }

  logout() {
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    presenter.getUserLogout();
    alertDialog(context);
  }

  @override
//  void initState() {
//    super.initState();
//    _iconAnimationController = new AnimationController(
//        vsync: this, duration: new Duration(microseconds: 500));
//    _iconAnimation = new CurvedAnimation(
//        parent: _iconAnimationController, curve: Curves.easeOut);
//    _iconAnimation.addListener(() => this.setState(() {}));
//    _iconAnimationController.forward();
//  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor:kinderm8Theme.Colors.appdarkcolour,
        centerTitle: true,
        elevation: 0.0,
        title: new Text("Change Password"),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop(null),
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: new Column(
          children: <Widget>[
            Container(
              height: 20.0,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: new Form(
                key: formKey,
                child: new Column(
                  children: <Widget>[
                    new Text(
                      "Enter Old Password",
                      style: TextStyle(
                        fontSize: 20.0,
                        letterSpacing: 0.50,
                      ),
                    ),
                    Container(
                      height: 5.0,
                    ),
                    new Container(
//                        height: 60.0,
//                        margin: new EdgeInsets.only(top: 5.0),
                      child: new Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: new Container(
                          width: screenSize.width,
                          margin: new EdgeInsets.only(
                              left: 10.0, right: 10.0, bottom: 2.0),
                          height: 60.0,
                          decoration: new BoxDecoration(
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(10.0))),
                          child: new TextFormField(
                            controller: _textController1,
//                            focusNode: myFocusNodePasswordLogin,
                            obscureText: _obscureText1,
//                            obscureText: true,
                            style: new TextStyle(
                                color: kinderm8Theme.Colors.logintext,
                                fontSize: 18.0),
                            decoration: new InputDecoration(
                              prefixIcon: new Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: new Icon(
                                  Icons.lock,
                                  size: 20.0,
                                ),
                              ),
                              contentPadding: EdgeInsets.all(12.0),
                              labelText: "Old Password",
                              labelStyle: new TextStyle(
                                  fontSize: 20.0,
                                  color: kinderm8Theme.Colors.textboxborder),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                  borderSide: new BorderSide(
                                      color: kinderm8Theme.Colors.textboxborder)),
                              hintText: "*********",
                              suffixIcon: GestureDetector(
                                onTap: _toggleReset1,
                                child: Icon(
                                  Icons.remove_red_eye,
                                  size: 25.0,
                                  color: kinderm8Theme.Colors.appcolour,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    error_oldpass
                        ? Text(
                            "Password can't be empty & wrong",
                            style: TextStyle(color: Colors.redAccent),
                          )
                        : Container(),
//                    new Divider(),
                    Container(height: 10.0),
                    new Text(
                      "Enter New Password",
                      style: TextStyle(
                        fontSize: 20.0,
                        letterSpacing: 0.50,
                      ),
                    ),
                    Container(
                      height: 5.0,
                    ),
                    new Container(
//                        height: 60.0,
//                        margin: new EdgeInsets.only(top: 5.0),
                      child: new Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: new Container(
                          width: screenSize.width,
                          margin: new EdgeInsets.only(
                              left: 10.0, right: 10.0, bottom: 2.0),
                          height: 60.0,
                          decoration: new BoxDecoration(
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(10.0))),
                          child: new TextFormField(
                            controller: _textController2,
//                            focusNode: myFocusNodePasswordLogin,
                            obscureText: _obscureText2,
//                            obscureText: true,
                            style: new TextStyle(
                                color: kinderm8Theme.Colors.logintext,
                                fontSize: 18.0),
                            decoration: new InputDecoration(
                              prefixIcon: new Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: new Icon(
                                  Icons.lock,
                                  size: 20.0,
                                ),
                              ),
                              contentPadding: EdgeInsets.all(12.0),
                              labelText: "New Password",
                              labelStyle: new TextStyle(
                                  fontSize: 20.0,
                                  color: kinderm8Theme.Colors.textboxborder),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                  borderSide: new BorderSide(
                                      color: kinderm8Theme.Colors.textboxborder)),
                              hintText: "*********",
                              suffixIcon: GestureDetector(
                                onTap: _toggleReset2,
                                child: Icon(
                                  Icons.remove_red_eye,
                                  size: 25.0,
                                  color: kinderm8Theme.Colors.appcolour,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
//                    new TextFormField(
//                      controller: _textController2,
//                      obscureText: _obscureText2,
//                      decoration: new InputDecoration(
//                        border: new OutlineInputBorder(
//                          borderRadius: const BorderRadius.all(
//                            const Radius.circular(30.0),
//                          ),
//                          borderSide: new BorderSide(
//                            color: Colors.black,
//                            width: 2.0,
//                          ),
//                        ),
//                        suffixIcon: GestureDetector(
//                          onTap: _toggleReset2,
//                          child: Icon(
//                            Icons.remove_red_eye, size: 25.0,
//                            //   color: Theme.Colors.appcolour,
//                          ),
//                        ),
////                        hintText: "*********",
//                      ),
//                      textAlign: TextAlign.center,
//                      keyboardType: TextInputType.text,
//                    ),
                    error_newpass
                        ? Text(
                            "Password is not empty & 6 letters",
                            style: TextStyle(color: Colors.redAccent),
                          )
                        : Container(),
//                    new Divider(),
                    Container(height: 10.0),
                    new Text(
                      "Confirm Password",
                      style: TextStyle(
                        fontSize: 20.0,
                        letterSpacing: 0.50,
                      ),
                    ),
                    Container(
                      height: 5.0,
                    ),

                    new Container(
//                        height: 60.0,
//                        margin: new EdgeInsets.only(top: 5.0),
                      child: new Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: new Container(
                          width: screenSize.width,
                          margin: new EdgeInsets.only(
                              left: 10.0, right: 10.0, bottom: 2.0),
                          height: 60.0,
                          decoration: new BoxDecoration(
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(10.0))),
                          child: new TextFormField(
                            controller: _textController3,
//                            focusNode: myFocusNodePasswordLogin,
                            obscureText: _obscureText3,
//                            obscureText: true,
                            style: new TextStyle(
                                color: kinderm8Theme.Colors.logintext,
                                fontSize: 18.0),
                            decoration: new InputDecoration(
                              prefixIcon: new Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: new Icon(
                                  Icons.lock,
                                  size: 20.0,
                                ),
                              ),
                              contentPadding: EdgeInsets.all(12.0),
                              labelText: "Password",
                              labelStyle: new TextStyle(
                                  fontSize: 20.0,
                                  color: kinderm8Theme.Colors.textboxborder),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                  borderSide: new BorderSide(
                                      color: kinderm8Theme.Colors.textboxborder)),
                              hintText: "*********",
                              suffixIcon: GestureDetector(
                                onTap: _toggleReset3,
                                child: Icon(
                                  Icons.remove_red_eye,
                                  size: 25.0,
                                  color: kinderm8Theme.Colors.appcolour,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
//                    new TextFormField(
//                      controller: _textController3,
//                      obscureText: _obscureText3,
//                      decoration: new InputDecoration(
//                        border: new OutlineInputBorder(
//                          borderRadius: const BorderRadius.all(
//                            const Radius.circular(30.0),
//                          ),
//                          borderSide: new BorderSide(
//                            color: Colors.black,
//                            width: 1.0,
//                          ),
//                        ),
//                        suffixIcon: GestureDetector(
//                          onTap: _toggleReset3,
//                          child: Icon(
//                            Icons.remove_red_eye, size: 25.0,
//                            //   color: Theme.Colors.appcolour,
//                          ),
//                        ),
////                        hintText: "*********",
//                      ),
//                      keyboardType: TextInputType.text,
//                      textAlign: TextAlign.center,
//                      //  obscureText: true,
//                    ),
                    error_confirmpass
                        ? Text(
                            "ConfirmPassword is not match",
                            style: TextStyle(color: Colors.redAccent),
                          )
                        : Container(),
                    new Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: RaisedButton(
                                color: kinderm8Theme.Colors.appcolour,
                                child: Text("Submit",style: TextStyle(
                                    fontSize: 20.0, color: Colors.white)),
                                onPressed: () {
                                  if (_textController1.text.length == 0) {
                                    setState(() {
                                      error_oldpass = true;
                                    });
                                  } else if (_textController1.text != logedinpassword) {
                                    setState(() {
                                      error_oldpass = true;
                                    });
                                  } else if (_textController2.text.length == 0) {
                                    setState(() {
                                      error_newpass = true;
                                      error_oldpass = false;
                                    });
                                  } else if (_textController2.text.length < 6) {
                                    setState(() {
                                      error_newpass = true;
                                    });
                                  } else if (_textController3.text.length == 0) {
                                    setState(() {
                                      error_confirmpass = true;
                                      error_newpass = false;
                                    });
                                  } else if (_textController3.text !=
                                      _textController2.text) {
                                    setState(() {
                                      error_confirmpass = true;
                                    });
                                  } else {
                                    setState(() {
                                      error_confirmpass = false;
                                    });
                                    resetPassword();
                                    logout();
                                  }
                                }
                            ),
                          )
                        ],
                      ),
                    ),
//                    new MaterialButton(
//                        elevation: 2.0,
//                        color: Colors.deepPurple,
//                        textColor: Colors.white,
//                        child: new Text("Save"),
//                        onPressed: () {
//                          if (_textController1.text.length == 0) {
//                            setState(() {
//                              error_oldpass = true;
//                            });
//                          } else if (_textController1.text != logedinpassword) {
//                            setState(() {
//                              error_oldpass = true;
//                            });
//                          } else if (_textController2.text.length == 0) {
//                            setState(() {
//                              error_newpass = true;
//                              error_oldpass = false;
//                            });
//                          } else if (_textController2.text.length < 6) {
//                            setState(() {
//                              error_newpass = true;
//                            });
//                          } else if (_textController3.text.length == 0) {
//                            setState(() {
//                              error_confirmpass = true;
//                              error_newpass = false;
//                            });
//                          } else if (_textController3.text !=
//                              _textController2.text) {
//                            setState(() {
//                              error_confirmpass = true;
//                            });
//                          } else {
//                            setState(() {
//                              error_confirmpass = false;
//                            });
//                            resetPassword();
//                            logout();
//
//                            print("hiii............");
//                          }
//                        }),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<bool> alertDialog(BuildContext context) {
//    await Future.delayed(Duration(seconds: 5));
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: Text("PasswordChange"),
            content: Text("successfully"),
            actions: <Widget>[
              FlatButton(
                child: const Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          );
        });
  }

  resetPassword() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      FocusScope.of(context).requestFocus(new FocusNode());

      final resetUrl =
          "https://apicare.carem8.com/v2.1.0/changepasswordoneapp?clientid=$clientId";

      var body = {
        "userid": userId,
        "email": email,
        "token": token,
        "status": status,
        "username": username,
        "password": _textController1.text,
        "newpassword": _textController2.text,
        "confirmpassword": _textController3.text
      };
      print(body);
      var headers = {"x-authorization": jwt.toString()};
      NetworkUtil _netUtil = new NetworkUtil();
      _netUtil
          .post(resetUrl,
              body: body, headers: headers, encoding: jwt.toString())
          .then((res) {
        print("ress $res");
        if (res == 1) {
          String _refreshTokenUrl =
              'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

          print(_refreshTokenUrl);
          NetworkUtil _netutil = new NetworkUtil();

          _netutil.get(_refreshTokenUrl).then((response) {
            print("refresh get ${response.body}");
            var refreshJwtToken;
            try {
              refreshJwtToken = json.decode(response.body);
            } catch (e) {
              print('That string was null!');
            }
            this.jwt = refreshJwtToken;
            resetPassword();
          });
        } else if (res == "[true]") {
          print("may be 200");
          print("^^^^^^^^^^^^^^$clientId");
          Navigator.of(context).pop();
        } else {
          print("error");
        }
      });
    } else {
      print("else");
    }
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    logedinpassword = user.password;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];
      token = users["token"];
      username = users["username"];
      status = users["status"];
      email = users["email"];

      print("iddd $userId");
      print(clientId);
    } catch (e) {
      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }

  @override
  void onAuthStateChanged(AuthState state) {
    print("Navigatesssss");
    print("state $state");
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
    // TODO: implement onAuthStateChanged
  }
}
