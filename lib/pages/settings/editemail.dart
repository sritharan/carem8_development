import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/cupertino.dart';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:carem8/auth.dart';
import 'package:carem8/data/database_helper.dart';
import 'package:carem8/models/user.dart';
import 'dart:convert';

import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/login/login.dart';
import 'package:carem8/utils/network_util.dart';

class EmailChange extends StatefulWidget {
  @override
  EmailChangeState createState() => EmailChangeState();
}

class EmailChangeState extends State<EmailChange>
    with SingleTickerProviderStateMixin
    implements HomePageContract, AuthStateListener {
  final TextEditingController _textController1 = new TextEditingController();
  final TextEditingController _textController2 = new TextEditingController();
  final formKey = new GlobalKey<FormState>();

  bool errorname = false;
  bool errormail = false;
  var client_user_id, fullname, email;
  var appuser, jwt, clientId, userId;
  bool load = false;
  HomePagePresenter presenter;
  BuildContext _ctx;

  EmailChangeState() {
    presenter = new HomePagePresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
    presenter.getUserInfo();
  }

  logout() {
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    Navigator.push(
        context, new MaterialPageRoute(builder: (context) => new LoginPage()));
    presenter.getUserLogout();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textController1.text = fullname;
    _textController2.text = email;
    print(fullname);
    print(email);
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      appBar: new AppBar(
        //  centerTitle: true,
        backgroundColor: kinderm8Theme.Colors.appdarkcolour,
        elevation: 0.0,
        title: new Text(
          "Edit Profile",
          style: TextStyle(fontSize: 18.0),
        ),

      ),
      backgroundColor: Colors.white,
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: SingleChildScrollView(
          child: load
              ? new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 50.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Form(
                        key: formKey,
                        child: new Column(
                          children: <Widget>[

                            Container(
                              width: 10.0,
                              height: 10.0,
                            ),
                            new Container(
                              height: 60.0,
                              margin: new EdgeInsets.only(top: 5.0),
                              child: new Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: new Container(
                                  width: screenSize.width,
                                  margin: new EdgeInsets.only(
                                      left: 10.0, right: 10.0, bottom: 2.0),
                                  height: 60.0,
                                  decoration: new BoxDecoration(
                                      borderRadius: new BorderRadius.all(
                                          new Radius.circular(20.0))),
                                  child: new TextFormField(
                                    controller: _textController1,
                                    keyboardType: TextInputType.text,
                                    style: new TextStyle(
                                        color: kinderm8Theme.Colors.logintext,
                                        fontSize: 18.0),
                                    decoration: new InputDecoration(
                                      prefixIcon: new Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: new Icon(
                                          Icons.person_pin,
                                          size: 20.0,
                                        ),
                                      ),
                                      contentPadding: EdgeInsets.all(12.0),
                                      labelText: "Full Name",
                                      labelStyle: new TextStyle(
                                          fontSize: 20.0,
                                          color: kinderm8Theme.Colors.textboxborder),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10.0),
                                          borderSide: new BorderSide(
                                              color: kinderm8Theme.Colors.textboxborder)),
                                    ),
                                  ),
                                ),
                              ),
                            ),

                            errorname
                                ? Text(
                                    "Please Write Name ",
                                    style: TextStyle(color: Colors.redAccent),
                                  )
                                : Container(),
                            Container(
                              height: 18.0,
                            ),

                            Container(
                              height: 10.0,
                            ),
                            new Container(
                              height: 60.0,
                              margin: new EdgeInsets.only(top: 5.0),
                              child: new Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: new Container(
                                  width: screenSize.width,
                                  margin: new EdgeInsets.only(
                                      left: 10.0, right: 10.0, bottom: 2.0
                                  ),
                                  height: 60.0,
                                  decoration: new BoxDecoration(
                                      borderRadius: new BorderRadius.all(
                                          new Radius.circular(20.0))
                                  ),
                                  child: new TextFormField(
                                    controller: _textController2,
                                    keyboardType: TextInputType.emailAddress,
                                    style: new TextStyle(
                                        color: kinderm8Theme.Colors.logintext,
                                        fontSize: 18.0),
                                    decoration: new InputDecoration(
                                      prefixIcon: new Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: new Icon(
                                          Icons.email,
                                          size: 20.0,
                                        ),
                                      ),
                                      contentPadding: EdgeInsets.all(12.0),
                                      labelText: "Email",
                                      labelStyle: new TextStyle(
                                          fontSize: 20.0,
                                          color: kinderm8Theme.Colors.textboxborder),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(10.0),
                                          borderSide: new BorderSide(
                                              color: kinderm8Theme.Colors.textboxborder)),
                                    ),
                                  ),
                                ),
                              ),
                            ),

                            errormail
                                ? Text(
                                    "Please Check Email",
                                    style: TextStyle(color: Colors.redAccent),
                                  )
                                : Container(),
                            Container(
                              height: 10.0,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: RaisedButton(
                                        color: kinderm8Theme.Colors.appcolour,
                                        child: Text("Update",style: TextStyle(
                                            fontSize: 20.0, color: Colors.white)),
                                        onPressed: () {
                                          print("$fullname");

                                          if (_textController1.text.length == 0) {
                                            setState(() {
                                              errorname = true;
                                            });
                                          } else {
                                            errorname = false;
                                          }

                                          print("$email");

                                          if (_textController2.text.length == 0) {
                                            setState(() {
                                              errormail = true;
                                            });
                                          } else {
                                            errormail = false;
                                          }
                                          if (errorname == false && errormail == false) {
                                            editemail();
                                          }
                                        }
                                        ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              : CircularProgressIndicator(),
        ),
      ),
    );
  }

  Future<bool> alertDialog(BuildContext context) {
//    await Future.delayed(Duration(seconds: 5));
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: Text("UsernameChange"),
            content: Text(
              "successfully",
              style: TextStyle(fontSize: 15),
            ),
            actions: <Widget>[
              FlatButton(
                child: const Text('OK'),
                onPressed: () {
                  logout();
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          );
        });
  }

  editemail() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      FocusScope.of(context).requestFocus(new FocusNode());

      final resetUrl =
          "https://apicare.carem8.com/v2.1.0/updateusersettingsoneapp?clientid=$clientId";
      var body = {
        "client_user_id": userId,
        "fullname": _textController1.text,
        "email": _textController2.text,
      };
      print(body);
      var headers = {"x-authorization": jwt.toString()};
      NetworkUtil _netUtil = new NetworkUtil();
      _netUtil
          .post(resetUrl,
              body: body, headers: headers, encoding: jwt.toString())
          .then((res) {
        print("ress $res");
        if (res == 1) {
          String _refreshTokenUrl =
              'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

          print(_refreshTokenUrl);
          NetworkUtil _netutil = new NetworkUtil();

          _netutil.get(_refreshTokenUrl).then((response) {
            print("refresh get ${response.body}");
            var refreshJwtToken;
            try {
              refreshJwtToken = json.decode(response.body);
            } catch (e) {
              print('That string was null!');
            }
            this.jwt = refreshJwtToken;
            editemail();
          });
        } else if (res != null) {
          print("may be 200");
          alertDialog(context);
//          Navigator.of(context).pop();
        } else {
          print("error man");
        }
      });
    } else {
      print("else");
    }
    print("hii iii");
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    //  logedinpassword = user.password;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      userId = users["id"];
      clientId = users["client_id"];
      fullname = users["fullname"];
      email = users["email"];

      print("iddd $fullname");
      print(userId);
      print(email);
      setState(() {
        load = true;
        _textController1.text = fullname;
        _textController2.text = email;
      });
    } catch (e) {
      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
    var authStateProvider = new AuthStateProvider();
    setState(() {
      authStateProvider.notify(AuthState.LOGGED_OUT);
      authStateProvider.dispose(this);
    });
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }

  @override
  void onAuthStateChanged(AuthState state) {
    // TODO: implement onAuthStateChanged
    if (state == AuthState.LOGGED_OUT) {
      Navigator.of(_ctx)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }
  }
}
