import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:carem8/components/googlewebview_url.dart';
import 'dart:async';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class GeneralFormsData extends StatefulWidget {
  final gerneralformsdata;

  GeneralFormsData(this.gerneralformsdata);

  @override
  GeneralFormsState createState() => GeneralFormsState(gerneralformsdata);
}

class GeneralFormsState extends State<GeneralFormsData> {
  final gerneralformsdata;
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  GeneralFormsState(this.gerneralformsdata) {}


  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


//  Future<Null> _launchInWebViewWithJavaScript(String url) async {
//    var googledocurl = "https://docs.google.com/gview?embedded=true&url="+url;
//    if (await canLaunch(url)) {
//      await launch(
//        url,
//        forceSafariVC: true,
//        forceWebView: true,
////        enableJavaScript: true,
//      );
//    } else {
//      throw 'Could not launch $url';
//    }
//  }


  // /* Using webview*/ //
  openBrowserTabInWebview(String url) async {
    return new WebviewScaffold(
      url: url,
      appBar: new AppBar(
        title: const Text('Widget webview'),
      ),
      withZoom: true,
      withLocalStorage: true,
    );
//    flutterWebviewPlugin.launch(url, hidden: true);
  }


  @override
  Widget build(BuildContext context) {
    var parsedDate = DateTime.parse(gerneralformsdata["created_at"]);
    var date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);
    var sender = gerneralformsdata["sender"];
    var fileurlencode = (gerneralformsdata['fileurl'] != null) ? "https://docs.google.com/gview?embedded=true&url="+gerneralformsdata['fileurl'] : "";


    var center = new Center(
        child: Column(
          children: <Widget>[
            new ListTile(
              leading: CircleAvatar(
                child: Container(
                  height: 45.0,
//                  padding: EdgeInsets.all(5.0),
                  margin: const EdgeInsets.all(10.0),
                  decoration: new BoxDecoration(
                    color: kinderm8Theme.Colors.appBarGradientEnd,
                    image: new DecorationImage(
                      image: new ExactAssetImage("assets/iconsetpng/gform.png"),
                      fit: BoxFit.cover,
                    ),
                    border: Border.all(
                        color: kinderm8Theme.Colors.appBarGradientEnd, width: 2.0),
                    borderRadius: new BorderRadius.all(const Radius.circular(80.0)
                    ),
                  ),
//                  child: ClipOval(
//                      child: Image.asset(
//                        "assets/iconsetpng/gform.png",
//                        fit: BoxFit.contain,
////                      width: 90.0,
////                      height: 90.0,
//                      )
//                  ),
                ),
//                backgroundImage: AssetImage("assets/iconsetpng/gform.png"),
//                radius: 25.0,
                backgroundColor: kinderm8Theme.Colors.appBarGradientEnd,
              ),
              title: Container(
                padding: EdgeInsets.only(top: 5.0),
                child: new Text(
                  gerneralformsdata['filename'],
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: new TextStyle(
                      fontWeight: FontWeight.w500, fontSize: 14.0),
                ),
              ),
              subtitle: Container(
                padding: EdgeInsets.only(top: 5.0),
                child: new Text(
                  date,
                  style: new TextStyle(
                      fontWeight: FontWeight.w500, fontSize: 10.0),
                ),
              ),
              onTap: () {
                Navigator.push(
                    context, new MaterialPageRoute(builder: (context) => new WebUrlPreview(fileurlencode, fileUrl: gerneralformsdata['fileurl'])));
//                WebUrlPreview(fileurlencode);
//                _launchInWebViewOrVC(fileurlencode);
//                openBrowserTab(fileurlencode);
//                openBrowserTabInWebview(fileurlencode);
//                flutterWebviewPlugin.launch(fileurlencode);
//                return new WebviewScaffold(
//                  url: fileurlencode,
//                  appBar: new AppBar(
//                    title: const Text('Widget webview'),
//                  ),
//                  withZoom: true,
//                  withLocalStorage: true,
//                );
//                InAppBrowser.openWithSystemBrowser(fileurlencode);
                print("Open url >> ${fileurlencode}");
              },
            ),
            Divider(),
          ],
        ));


    return center;
  }


}
