import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:carem8/components/googlewebview_url.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/newsfeed/videoplay.dart';
import 'package:carem8/pages/privatemessage/replyprivatemessage.dart';
import 'package:carem8/pages/privatemessage/replystaffselection.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:zoomable_image/zoomable_image.dart';

class DetailedMessage extends StatefulWidget {
  final singlePrivateMessageData;

  DetailedMessage(this.singlePrivateMessageData);

  @override
  DetailedMessageState createState() => DetailedMessageState();
}

class DetailedMessageState extends State<DetailedMessage>
    implements HomePageContract {
  var singlePrivateMessageData, messageId;
  var appuser, jwt, userId, clientId;
  List<Widget> mainAttachments = [];
  List<Widget> replyAttachments = [];
  List<Widget> replies = [];

  ScrollController detailMessageController;
  int apiPaginateSize, len = 0;

  var privateMessageReplies;
  HomePagePresenter presenter;
  bool load = true;

  DetailedMessageState() {
    presenter = new HomePagePresenter(this);
    presenter.getUserInfo();
  }
  var progress = new ProgressBar(
    color: kinderm8Theme.Colors.appdarkcolour,
    containerColor: kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    detailMessageController =ScrollController()..addListener(_scrollListener);



    singlePrivateMessageData = widget.singlePrivateMessageData;
    messageId = singlePrivateMessageData['id'];
  }

  @override
  void dispose() {
    detailMessageController.removeListener(_scrollListener);
    detailMessageController.dispose();
    super.dispose();
  }

  _scrollListener() {
//    if (detailMessageController.position.extentAfter <= 0 && isLoading == false) {
//      detailMessageController(apiPaginateSize);
//    }
  }

  Future<String> getPrivateMessageReplies() async {
    ///data from GET method
    print("PrivateMessageReplies data fetched");

    String _privatemessageUrl =
        'http://13.210.72.124:7070/v4.4.2/getprivatemessagereplies/$messageId?pmuserid=$userId&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_privatemessageUrl, headers: headers).then((response) {
      var privateReplyData;
      privateReplyData = json.decode(response.body);
      print('res get ${response.body}');
      print('privateReplyData $privateReplyData');

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        setState(() {
          load = false;
        });
        privateMessageReplies = privateReplyData;
        addReplies();
      } else if (response.statusCode == 500 &&
          privateReplyData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      } else {
        print("error");
      }
    });
    return null;
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } on FormatException catch (e) {
        print("That string didn't look like Json.");
      } on NoSuchMethodError catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      getPrivateMessageReplies();
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    var parsedcreatedDate =
    DateTime.parse(singlePrivateMessageData["created_at"]);
    var formattedDate =
    new DateFormat.yMMMMEEEEd().add_jm().format(parsedcreatedDate);

    var mainMessage = html2md.convert(singlePrivateMessageData['message']);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: kinderm8Theme.Colors.appdarkcolour,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                singlePrivateMessageData["subject"],
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    color: kinderm8Theme.Colors.darkGrey),
              ),
              Container(height: 5.0),
              Container(
                padding: EdgeInsets.all(4.0),
                decoration: new BoxDecoration(
//                color: kinderm8Theme.Colors.appcolour,
                  borderRadius: new BorderRadius.circular(25.0),
                  border: new Border.all(
                    width: 2.0,
                    color: kinderm8Theme.Colors.appcolour,
                  ),
                ),
                child: Text(
                  "$formattedDate",
                  style: TextStyle(
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                      color: kinderm8Theme.Colors.darkGrey.withOpacity(0.7)),
                ),
//                                      width: ((screenSize.width   / 5)),
              ),
              Container(height: 5.0),
              Card(
                elevation: 1.0,
                child: Column(
                  children: <Widget>[
                    ListTile(
                      leading: CircleAvatar(
                        radius: 25.0,
//                      backgroundColor: kinderm8Theme.Colors.appcolour,
                        backgroundImage:
                        singlePrivateMessageData['user']['image'] != null
                            ? CachedNetworkImageProvider(
                            'http://carem8bucket.s3-ap-southeast-2.amazonaws.com/' +
                                singlePrivateMessageData['user']
                                ['image'])
                            : AssetImage("assets/nophoto.jpg"),
                      ),
                      title: userId != singlePrivateMessageData['user']['id']
                          ? Text(
                          "${singlePrivateMessageData['user']['fullname']} to me.")
                          : singlePrivateMessageData['initialmsgreceiver']
                          .length ==
                          1
                          ? Text(
                          "${singlePrivateMessageData['user']['fullname']} to ${singlePrivateMessageData['initialmsgreceiver'][0]['fullname']}.")
                          : Text(
                          "${singlePrivateMessageData['user']['fullname']} to ${singlePrivateMessageData['initialmsgreceiver'][0]['fullname']} and ${singlePrivateMessageData['initialmsgreceiver'].length - 1} other."),

//
                      subtitle: Html(
                        data: mainMessage,
                        padding: EdgeInsets.all(0.0),
                      ),
                      trailing: userId == singlePrivateMessageData['user']['id']
                          ? Container(
                        width: 0.0,
                      )
                          : IconButton(
                          icon: Icon(Icons.reply),
                          onPressed: () {
                            print("have to do replies");
                            List replyStaff = List();
                            replyStaff
                                .add(singlePrivateMessageData['user']);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ReplyPrivateMessage(
                                      singlePrivateMessageData,
                                      replyStaff)),
                            );
                          }),
                    ),
                    Container(
                      height: 800.0,width: 400,
                      //width: double.infinity,
                      child: Column(

                        children: mainAttachments,
                      ),
                      padding:
                      EdgeInsets.only(left: 80.0, bottom: 10.0, top: 10.0),
                    ),
                  ],
                ),
              ),
              load
                  ? progress
                  : Column(
                children: replies,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void deleteMessage(deleteMessageId, index) async {
//    setState(() {
//      delete = false;
//    });

    String deleteUrl =
        'http://13.210.72.124:7070/deleteprivatemessage?clientid=$clientId';
    var body = {
      "message_id": messageId,
      "reply_id": deleteMessageId,
      "user": userId
    };

    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.delete(deleteUrl, headers: headers, body: body).then((res) {
      print("res$res");
      print(res.statusCode);
      if (res.statusCode == 200) {
        setState(() {
          replies.removeAt(index);
        });
      } else if (res.statusCode == 500) {
        print("refreshing Token..");
        String _refreshTokenUrl =
            'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

        NetworkUtil _netutil = new NetworkUtil();

        _netutil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          var refreshJwtToken = json.decode(response.body);
          this.jwt = refreshJwtToken;
          deleteMessage(deleteMessageId, index);
        });
      }
    });
  }

  addReplies() {
    if (privateMessageReplies['messagereplies'].length > 0) {
      for (int i = 0; i < privateMessageReplies['messagereplies'].length; i++) {
        final messageReplies = privateMessageReplies['messagereplies'][i];
//
        replyAttachments = [];
        if (messageReplies['attachments_set'] != null) {
          createReplyAttachment(messageReplies['attachments_set'][0]);
        }
        var replyMessage = html2md.convert(messageReplies['message']);
//
        replies.add(Container(
            child: userId == messageReplies['user']['id']
                ? Slidable(
              key: Key(messageReplies['id'].toString()),
              closeOnScroll: true,

              delegate: SlidableDrawerDelegate(),
              actionExtentRatio: 0.2,
              secondaryActions: <Widget>[
                new IconSlideAction(
                  caption: 'Delete',
                  color: Colors.red,
                  icon: Icons.delete,
                  onTap: () {
//                          print(messageReplies['id']);
                    deleteMessage(messageReplies['id'], i);
                  },
                )
              ],
              child: Card(
                elevation: 1.0,
                child: Column(
                  children: <Widget>[
                    ListTile(
                      leading: CircleAvatar(
                        radius: 25.0,
                        backgroundImage: messageReplies['user']
                        ['image'] !=
                            null
                            ? CachedNetworkImageProvider(
                            'http://carem8bucket.s3-ap-southeast-2.amazonaws.com/' +
                                messageReplies['user']['image'])
                            : AssetImage("assets/nophoto.jpg"),
                      ),
                      title:
                      Text("${messageReplies['user']['fullname']}"),
                      subtitle: Html(data: replyMessage),
                      trailing: Container(width: 0.0),
                    ),
                    Container(
                      child: Column(
                        children: replyAttachments,
                      ),
                      padding: EdgeInsets.only(
                          left: 80.0, bottom: 10.0, top: 10.0),
                    ),
                  ],
                ),
              ),
            )
                : Card(
              elevation: 1.0,
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: CircleAvatar(
                      radius: 25.0,
                      backgroundImage: messageReplies['user']['image'] !=
                          null
                          ? CachedNetworkImageProvider(
                          'http://carem8bucket.s3-ap-southeast-2.amazonaws.com/' +
                              messageReplies['user']['image'])
                          : AssetImage("assets/nophoto.jpg"),
                    ),
                    title: Text("${messageReplies['user']['fullname']}"),
                    subtitle: Html(data: replyMessage),
                    trailing: IconButton(
                        icon: Icon(Icons.reply),
                        onPressed: () {
                          print("have to do replies");
                          List repliedStaffId = List();
                          List repliedStaff = List();

                          for (int j = 0;
                          j <
                              privateMessageReplies['messagereplies']
                                  .length;
                          j++) {
                            if (privateMessageReplies['messagereplies'][j]
                            ['user']['id'] ==
                                userId ||
                                repliedStaffId.contains(
                                    privateMessageReplies[
                                    'messagereplies'][j]['user']
                                    ['id'])) {
                            } else {
                              print(
                                  privateMessageReplies['messagereplies']
                                  [j]['user']['id']);

                              repliedStaff.add(
                                  privateMessageReplies['messagereplies']
                                  [j]['user']);
                              repliedStaffId.add(
                                  privateMessageReplies['messagereplies']
                                  [j]['user']['id']);
                            }

//            print(repliedStaff[j]['id']);
                          }
                          print(repliedStaff);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ReplyStaffSelection(
                                    repliedStaff,
                                    singlePrivateMessageData)),
                          );
                        }),
                  ),
                  Container(
                    child: Column(
                      children: replyAttachments,
                    ),
                    padding: EdgeInsets.only(
                        left: 80.0, bottom: 10.0, top: 10.0),
                  ),
                ],
              ),
            )));
//
      }
    } else {
      print("no replies");
    }
  }

  createMainAttachment() {
    if (singlePrivateMessageData['isAttachment'] == 1) {
      var attachmentsSet = singlePrivateMessageData['attachments_set'][0];

//
      if (attachmentsSet['pdf'] != null) {
        for (int i = 0; i < attachmentsSet['pdf'].length; i++) {
          mainAttachments.add(FlatButton(
            onPressed: () {
              var fileurlencode =
                  "https://docs.google.com/gview?embedded=true&url=" +
                      attachmentsSet['pdf'][i]['filepath'];
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new WebUrlPreview(fileurlencode)));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.picture_as_pdf,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['pdf'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['jpg'] != null) {
        for (int i = 0; i < attachmentsSet['jpg'].length; i++) {
          mainAttachments.add(FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ViewImagePage(
                          attachmentsSet['jpg'][i]['filepath'])
                  )
              );
            },
            child: Container( height: 70.0,width: 70.0,
              decoration: BoxDecoration(border: Border.all(width: 1.0)),
              child: Image.network(attachmentsSet['jpg'][i]['filepath'], fit: BoxFit.cover,),

            ),
          )
          );
        }
      }
//
      if (attachmentsSet['png'] != null) {
        for (int i = 0; i < attachmentsSet['png'].length; i++) {
          mainAttachments.add(FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ViewImagePage(
                          attachmentsSet['png'][i]['filepath'])));
            },
            child: Container(  height: 70.0,width: 70.0,
              decoration: BoxDecoration(border: Border.all(width: 1.0)),
              child: Image.network(attachmentsSet['png'][i]['filepath'], fit: BoxFit.cover,),

            ),
          ));
        }
      }
//
      if (attachmentsSet['txt'] != null) {
        for (int i = 0; i < attachmentsSet['txt'].length; i++) {
          mainAttachments.add(FlatButton(
            onPressed: () {
              var fileurlencode =
                  "https://docs.google.com/gview?embedded=true&url=" +
                      attachmentsSet['txt'][i]['filepath'];
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new WebUrlPreview(fileurlencode)));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.textsms,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['txt'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['gif'] != null) {
        for (int i = 0; i < attachmentsSet['gif'].length; i++) {
          mainAttachments.add(FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ViewImagePage(
                          attachmentsSet['gif'][i]['filepath'])));
            },
            child: Container(width: 70.0, height: 70.0,
              decoration: BoxDecoration(border: Border.all(width: 1.0)),
              child: Image.network(attachmentsSet['gif'][i]['filepath'], fit: BoxFit.cover,),

            ),
          ));
        }
      }
//
      if (attachmentsSet['jpeg'] != null) {
        for (int i = 0; i < attachmentsSet['jpeg'].length; i++) {
          mainAttachments.add(FlatButton(
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new ViewImagePage(
                            attachmentsSet['jpeg'][i]['filepath'])));
              },
              child: Container(width: 70.0, height: 70.0,
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: Image.network(attachmentsSet['jpeg'][i]['filepath'], fit: BoxFit.cover,),

              )
          ));
        }
      }
//
      if (attachmentsSet['doc'] != null) {
        for (int i = 0; i < attachmentsSet['doc'].length; i++) {
          mainAttachments.add(FlatButton(
            onPressed: () {
              var fileurlencode =
                  "https://docs.google.com/gview?embedded=true&url=" +
                      attachmentsSet['doc'][i]['filepath'];
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new WebUrlPreview(fileurlencode)));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.image,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['doc'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['docx'] != null) {
        for (int i = 0; i < attachmentsSet['docx'].length; i++) {
          mainAttachments.add(FlatButton(
            onPressed: () {
              var fileurlencode =
                  "https://docs.google.com/gview?embedded=true&url=" +
                      attachmentsSet['docx'][i]['filepath'];
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new WebUrlPreview(fileurlencode)));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.image,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['docx'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour
                    ),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['zip'] != null) {
        for (int i = 0; i < attachmentsSet['zip'].length; i++) {
          mainAttachments.add(FlatButton(
            onPressed: () {
              print("Zip file");
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.image,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['zip'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['other'] != null) {
        for (int i = 0; i < attachmentsSet['other'].length; i++) {
          mainAttachments.add(FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => new VideoExample(
                          attachmentsSet['other'][i]['filepath'])));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.image,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['other'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
    }
  }

  createReplyAttachment(attachmentsSet) {
    if (attachmentsSet.length != null) {
//
      if (attachmentsSet['pdf'] != null) {
        for (int i = 0; i < attachmentsSet['pdf'].length; i++) {
          replyAttachments.add(FlatButton(
            onPressed: () {
              var fileurlencode =
                  "https://docs.google.com/gview?embedded=true&url=" +
                      attachmentsSet['pdf'][i]['filepath'];
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new WebUrlPreview(fileurlencode)));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.picture_as_pdf,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['pdf'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['jpg'] != null) {
        for (int i = 0; i < attachmentsSet['jpg'].length; i++) {
          replyAttachments.add(FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ViewImagePage(
                          attachmentsSet['jpg'][i]['filepath'])));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.image,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['jpg'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['png'] != null) {
        for (int i = 0; i < attachmentsSet['png'].length; i++) {
          replyAttachments.add(FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ViewImagePage(
                          attachmentsSet['png'][i]['filepath'])));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.picture_in_picture,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['png'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['txt'] != null) {
        for (int i = 0; i < attachmentsSet['txt'].length; i++) {
          replyAttachments.add(FlatButton(
            onPressed: () {
              var fileurlencode =
                  "https://docs.google.com/gview?embedded=true&url=" +
                      attachmentsSet['txt'][i]['filepath'];
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new WebUrlPreview(fileurlencode)));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.textsms,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['txt'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['gif'] != null) {
        for (int i = 0; i < attachmentsSet['gif'].length; i++) {
          replyAttachments.add(FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ViewImagePage(
                          attachmentsSet['gif'][i]['filepath'])));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.image,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['gif'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['jpeg'] != null) {
        for (int i = 0; i < attachmentsSet['jpeg'].length; i++) {
          replyAttachments.add(FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ViewImagePage(
                          attachmentsSet['jpeg'][i]['filepath'])));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.image,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['jpeg'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['doc'] != null) {
        for (int i = 0; i < attachmentsSet['doc'].length; i++) {
          replyAttachments.add(FlatButton(
            onPressed: () {
              var fileurlencode =
                  "https://docs.google.com/gview?embedded=true&url=" +
                      attachmentsSet['doc'][i]['filepath'];
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new WebUrlPreview(fileurlencode)));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.image,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['doc'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['docx'] != null) {
        for (int i = 0; i < attachmentsSet['docx'].length; i++) {
          replyAttachments.add(FlatButton(
            onPressed: () {
              var fileurlencode =
                  "https://docs.google.com/gview?embedded=true&url=" +
                      attachmentsSet['docx'][i]['filepath'];
              Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new WebUrlPreview(fileurlencode)));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.image,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['docx'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['zip'] != null) {
        for (int i = 0; i < attachmentsSet['zip'].length; i++) {
          replyAttachments.add(FlatButton(
            onPressed: () {
              print("Zip file");
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.image,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['zip'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }
//
      if (attachmentsSet['other'] != null) {
        for (int i = 0; i < attachmentsSet['other'].length; i++) {
          replyAttachments.add(FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => new VideoExample(
                          attachmentsSet['other'][i]['filepath'])));
            },
            child: Container(
                decoration: BoxDecoration(border: Border.all(width: 1.0)),
                child: ListTile(
                  leading: Icon(
                    Icons.image,
                    size: 45.0,
                  ),
                  title: Text(
                    "${attachmentsSet['other'][i]['filename']}",
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.w200,
                        color: kinderm8Theme.Colors.appcolour),
                  ),
                )),
          ));
        }
      }

//
    }
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];
      setState(() {});
      print("iddd $userId");
      getPrivateMessageReplies();
      createMainAttachment();
    } catch (e) {
      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}

class ViewImagePage extends StatefulWidget {
  final data;

  ViewImagePage(this.data);

  @override
  ViewImagePageState createState() {
    return new ViewImagePageState();
  }
}

class ViewImagePageState extends State<ViewImagePage> {
  @override
  Widget build(BuildContext context) {
    var finalURL = widget.data;

    List<Widget> widgets = [];
    widgets.add(
      Scaffold(
        appBar: AppBar(
          title: Text("View attachments"),
          centerTitle: true,
          elevation: 0.0,
        ),
        body: new Container(
          child: ZoomableImage(
            NetworkImage(finalURL),
            placeholder: const Center(child: const CircularProgressIndicator()),
          ),
        ),
      ),
    );
    return new Scaffold(
      backgroundColor: kinderm8Theme.Colors.app_dark[900],
      body: new SizedBox.expand(
        child: new PageView(children: widgets),
      ),
    );
  }
}