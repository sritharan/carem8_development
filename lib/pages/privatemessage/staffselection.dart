import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:flutter/material.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/privatemessage/createmessage.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:carem8/Theme.dart' as kinderm8Theme;

class StaffSelection extends StatefulWidget {
  List<ChildViewModal> childrenData;

  StaffSelection({this.childrenData});
  @override
  StaffSelectionState createState() => StaffSelectionState();
}

class StaffSelectionState extends State<StaffSelection>
    implements HomePageContract {
  List staffByParentList, roomWithStaffList;
  bool load = true;

  var appuser, jwt, userId, clientId;
  List<String> rooms = [];
  String selectedValue;
  HomePagePresenter _presenter;

  bool checkboxValueAll = true;
  bool checkBoxValue = false;
  List _finalSelectedStaff = List();
  List _selectedStaff = List();

  StaffSelectionState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  void onChanged(bool value) {
    setState(() {
      checkBoxValue = value;
    });
  }

  Future<String> getStaffByParent() async {
    ///data from GET method
    print("getStaffByParentData data fetched");

    String _getStaffByParentUrl =
        'https://apicare.carem8.com/getstaffbyparent?userid=$userId&clientid=$clientId';

    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_getStaffByParentUrl, headers: headers).then((response) {
      var getStaffByParentData;
      getStaffByParentData = json.decode(response.body);
//      print(getStaffByParentData.length);
//      print('res get ${response.body}');
      print('getStaffByParentData $getStaffByParentData');

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        staffByParentList = getStaffByParentData;
//        getStaff();
        setState(() {
          load = false;
        });
      } else if (response.statusCode == 500 &&
          getStaffByParentData["errorType"] == 'ExpiredJwtException') {
        String _refreshTokenUrl =
            'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

        NetworkUtil _netutil = new NetworkUtil();
        var refreshJwtToken;
        _netutil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          try {
            refreshJwtToken = json.decode(response.body);
            this.jwt = refreshJwtToken;
            getStaffByParent();
          } catch (e) {
            print('That string was null!');
          }
        });
      } else {
        print("error");
      }
    });
    return null;
  }

  Future<String> getRoomWithStaff() async {
    ///data from GET method
    print("getRoomWithStaffData data fetched");

    String _getRoomWithStaffUrl =
        'https://apicare.carem8.com/getroomwithstaff?userid=$userId&clientid=$clientId';

    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_getRoomWithStaffUrl, headers: headers).then((response) {
      var getRoomWithStaffData;
      getRoomWithStaffData = json.decode(response.body);
//      print(getRoomWithStaffData.length);
//      print('res get ${response.body}');
      print('getRoomWithStaffData $getRoomWithStaffData');

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        roomWithStaffList = getRoomWithStaffData;
        setState(() {
          load = false;
        });
        getRooms();
      } else if (response.statusCode == 500 &&
          getRoomWithStaffData["errorType"] == 'ExpiredJwtException') {
        String _refreshTokenUrl =
            'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

        NetworkUtil _netutil = new NetworkUtil();
        var refreshJwtToken;
        _netutil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          try {
            refreshJwtToken = json.decode(response.body);
            this.jwt = refreshJwtToken;
            getRoomWithStaff();
          } catch (e) {
            print('That string was null!');
          }
        });
      } else {
        print("error");
      }
    });
    return null;
  }

/*  getRefreshToken() {
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();
    var refreshJwtToken;
    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');

      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

//      getStaffByParent();
    });
//    return refreshJwtToken;
  }*/

  getRooms() {
    rooms.add('All');
    for (int i = 0; i < roomWithStaffList.length; i++) {
      rooms.add(roomWithStaffList[i]['title'].toString());
    }

    print("rooms $rooms");
  }

  void _onStaffSelected(bool selected, staff) {
    if (selected == true) {
      setState(() {
        _finalSelectedStaff.add(staff);
      });
    } else {
      setState(() {
        _finalSelectedStaff.remove(staff);
      });
    }
    print(_finalSelectedStaff);
  }

  @override
  Widget build_(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
//        actions: <Widget>[
//          _finalSelectedStaff.length == 0
//              ? Container()
//              : new IconButton(
//              icon: new Icon(
//                Icons.add_circle_outline,
//                size: 45.0,
//              ),
//              onPressed: () {
//                print(_finalSelectedStaff);
//                Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (context) =>
//                          CreateMessage(_finalSelectedStaff)),
//                );
//              })
//        ],
      ),
//
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
            child: Center(
              child: Container(
                decoration: new BoxDecoration(
                    color: Colors.deepPurple[100],
                    borderRadius: BorderRadius.circular(20.0),
                    border: Border.all(
                        color: Color.fromRGBO(112, 112, 112, 1.0), width: 1.0)),
                child: new DropdownButton<String>(
                    elevation: 0,
                    hint: Center(child: Text('Select by room')),
                    onChanged: (String changedValue) {
                      selectedValue = changedValue;
                      setState(() {
                        _finalSelectedStaff = [];
                        _selectedStaff = [];
//
                        if (selectedValue == "All") {
                          for (int i = 0; i < staffByParentList.length; i++) {
                            _finalSelectedStaff.add(staffByParentList[i]);
                            _selectedStaff.add(staffByParentList[i]);
                            print(i);
                          }
                        } else {
                          for (int i = 0; i < roomWithStaffList.length; i++) {
                            if (selectedValue ==
                                roomWithStaffList[i]['title']) {
                              for (int j = 0;
                              j < roomWithStaffList[i]['staff'].length;
                              j++) {
                                _finalSelectedStaff
                                    .add(roomWithStaffList[i]['staff'][j]);
                                _selectedStaff
                                    .add(roomWithStaffList[i]['staff'][j]);
                              }
                            }
                          }
                        }
                        print(_finalSelectedStaff);
                        print(_selectedStaff);
                        print("inside DropdownButton $selectedValue");
                      });
                    },
                    value: selectedValue,
                    items: rooms.map((String value) {
                      return new DropdownMenuItem<String>(
                        value: value,
                        child: new Text(value),
                      );
                    }).toList()), //DropDownButton
              ),
            ),
          ),
          load
              ? CircularProgressIndicator()
              : Expanded(
              child: Container(
                child: selectedValue == null || selectedValue == 'All'
                    ? ListView.builder(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: staffByParentList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return CheckboxListTile(
                        value: _finalSelectedStaff
                            .contains(staffByParentList[index]),
                        onChanged: (bool selected) {
                          _onStaffSelected(
                              selected, staffByParentList[index]);
                        },
                        title: Row(
                          children: <Widget>[
                            CircleAvatar(
                              backgroundImage: staffByParentList[index]
                              ['image'] !=
                                  null
                                  ? NetworkImage(
                                  "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${staffByParentList[index]['image']}")
                                  : AssetImage("assets/nophoto.jpg"),
                              radius: 25.0,
                            ),
                            Container(width: 10.0),
                            Text(staffByParentList[index]['fullname'])
                          ],
                        ),
                      );
                    })
                    : ListView.builder(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemCount: _selectedStaff.length,
                    itemBuilder: (BuildContext context, int index) {
                      return CheckboxListTile(
                        value: _finalSelectedStaff
                            .contains(_selectedStaff[index]),
                        controlAffinity: ListTileControlAffinity.leading,
                        onChanged: (bool selected) {
                          _onStaffSelected(
                              selected, _selectedStaff[index]);
                        },
                        title: Row(
                          children: <Widget>[
                            CircleAvatar(
                              backgroundImage: _selectedStaff[index]
                              ['image'] !=
                                  null
                                  ? NetworkImage(
                                  "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${_selectedStaff[index]['image']}")
                                  : AssetImage("assets/nophoto.jpg"),
                              radius: 25.0,
                            ),
                            Container(width: 10.0),
                            Text(_selectedStaff[index]['fullname'])
                          ],
                        ),
                      );
                    }),
              ))
        ],
      ),

    );
  }



  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = appusers["jwt"] ;
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];

      print(" after the + click iddd  $userId");
      print(clientId);

      getStaffByParent();
      getRoomWithStaff();
    } catch (e) {
      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }

  @override
  Widget build__(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
//        actions: <Widget>[
//          _finalSelectedStaff.length == 0
//              ? Container()
//              : new IconButton(
//              icon: new Icon(
//                Icons.add_circle_outline,
//                size: 45.0,
//              ),
//              onPressed: () {
//                print(_finalSelectedStaff);
//                Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (context) =>
//                          CreateMessage(_finalSelectedStaff)),
//                );
//              })
//        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/UI2.jpg"), fit: BoxFit.cover)),
          child: Column(
            children: <Widget>[
              Container(
                height: 70.0,
              ),
              Container(
                alignment: Alignment.topLeft,
                height: 150.0,
                width: 150.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(150.5),
                  border: Border.all(
                      color: Colors.white,
                      style: BorderStyle.solid,
                      width: 3.0),
                  image: new DecorationImage(
                    image: CachedNetworkImageProvider(
                        'http://carem8bucket.s3-ap-southeast-2.amazonaws.com/img/uploads/production_parentpic1535089492661-c2i_1772018113715.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
                child: Center(
                  child: Container(
                    decoration: new BoxDecoration(
                        color: Colors.deepPurple[100],
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                            color: Color.fromRGBO(112, 112, 112, 1.0),
                            width: 1.0)),
                    child: new DropdownButton<String>(
                        elevation: 0,
                        hint: Center(child: Text('Select by room')),
                        onChanged: (String changedValue) {
                          selectedValue = changedValue;
                          setState(() {
                            _finalSelectedStaff = [];
                            _selectedStaff = [];
//
                            if (selectedValue == "All") {
                              for (int i = 0;
                              i < staffByParentList.length;
                              i++) {
                                _finalSelectedStaff.add(staffByParentList[i]);
                                _selectedStaff.add(staffByParentList[i]);
                                print(i);
                              }
                            } else {
                              for (int i = 0;
                              i < roomWithStaffList.length;
                              i++) {
                                if (selectedValue ==
                                    roomWithStaffList[i]['title']) {
                                  for (int j = 0;
                                  j < roomWithStaffList[i]['staff'].length;
                                  j++) {
                                    _finalSelectedStaff
                                        .add(roomWithStaffList[i]['staff'][j]);
                                    _selectedStaff
                                        .add(roomWithStaffList[i]['staff'][j]);
                                  }
                                }
                              }
                            }
                            print(_finalSelectedStaff);
                            print(_selectedStaff);
                            print("inside DropdownButton $selectedValue");
                          });
                        },
                        value: selectedValue,
                        items: rooms.map((String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList()), //DropDownButton
                  ),
                ),
              ),
              load
                  ? Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircularProgressIndicator(),
              )
                  : Row(
                children: <Widget>[
                  Expanded(
                      child: Container(
                        child: selectedValue == null || selectedValue == 'All'
                            ? ListView.builder(
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            itemCount: staffByParentList.length,
                            itemBuilder:
                                (BuildContext context, int index) {
                              return CheckboxListTile(
                                value: _finalSelectedStaff
                                    .contains(staffByParentList[index]),
                                onChanged: (bool selected) {
                                  _onStaffSelected(
                                      selected, staffByParentList[index]);
                                },
                                title: Row(
                                  children: <Widget>[
                                    CircleAvatar(
                                      backgroundImage: staffByParentList[
                                      index]['image'] !=
                                          null
                                          ? NetworkImage(
                                          "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${staffByParentList[index]['image']}")
                                          : AssetImage(
                                          "assets/nophoto.jpg"),
                                      radius: 25.0,
                                    ),
                                    Container(width: 10.0),
                                    Text(staffByParentList[index]
                                    ['fullname'])
                                  ],
                                ),
                              );
                            })
                            : ListView.builder(
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            itemCount: _selectedStaff.length,
                            itemBuilder:
                                (BuildContext context, int index) {
                              return CheckboxListTile(
                                value: _finalSelectedStaff
                                    .contains(_selectedStaff[index]),
                                controlAffinity:
                                ListTileControlAffinity.leading,
                                onChanged: (bool selected) {
                                  _onStaffSelected(
                                      selected, _selectedStaff[index]);
                                },
                                title: Row(
                                  children: <Widget>[
                                    CircleAvatar(
                                      backgroundImage: _selectedStaff[
                                      index]['image'] !=
                                          null
                                          ? NetworkImage(
                                          "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${_selectedStaff[index]['image']}")
                                          : AssetImage(
                                          "assets/nophoto.jpg"),
                                      radius: 25.0,
                                    ),
                                    Container(width: 10.0),
                                    Text(
                                        _selectedStaff[index]['fullname'])
                                  ],
                                ),
                              );
                            }),
                      ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
//        actions: <Widget>[
//          _finalSelectedStaff.length == 0
//              ? Container()
//              : new IconButton(
//              icon: new Icon(
//                Icons.add,
//                size: 40.0,
//              ),
//              onPressed: () {
//                print(_finalSelectedStaff);
//                Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                      builder: (context) =>
//                          CreateMessage(_finalSelectedStaff)),
//                );
//              })
//        ],
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Image.asset("assets/BGI.png"),
            Column(
              children: <Widget>[
                Container(
                  height: 50.0,
                ),
                Container(
                  alignment: Alignment.topLeft,
                  height: 150.0,
                  width: 150.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(150.0),
                    border: Border.all(
                        color: Colors.white,
                        style: BorderStyle.solid,
                        width: 3.0),
                    image: new DecorationImage(
                      image: CachedNetworkImageProvider(
                          'http://carem8bucket.s3-ap-southeast-2.amazonaws.com/img/uploads/production_parentpic1535089492661-c2i_1772018113715.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Padding(
                  padding:
                  const EdgeInsets.only(left: 15.0, right: 15.0, top: 15.0,bottom: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Container(padding: EdgeInsets.only(left: 20.0),
                        height: 25.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            border: Border.all(style: BorderStyle.solid)),
                        child: DropdownButtonHideUnderline(
                          child: new DropdownButton<String>(
                              elevation: 0,
                              hint: Center(child: Text('Select by room')),
                              value: selectedValue,
                              onChanged: (String changedValue) {
                                selectedValue = changedValue;
                                setState(() {
                                  _finalSelectedStaff = [];
                                  _selectedStaff = [];
//
                                  if (selectedValue == "All") {
                                    for (int i = 0;
                                    i < staffByParentList.length;
                                    i++) {
                                      _finalSelectedStaff
                                          .add(staffByParentList[i]);
                                      _selectedStaff.add(staffByParentList[i]);
                                      print(i);
                                    }
                                  } else {
                                    for (int i = 0;
                                    i < roomWithStaffList.length;
                                    i++) {
                                      if (selectedValue ==
                                          roomWithStaffList[i]['title']) {
                                        for (int j = 0;
                                        j <
                                            roomWithStaffList[i]['staff']
                                                .length;
                                        j++) {
                                          _finalSelectedStaff.add(
                                              roomWithStaffList[i]['staff'][j]);
                                          _selectedStaff.add(
                                              roomWithStaffList[i]['staff'][j]);
                                        }
                                      }
                                    }
                                  }
                                  print(_finalSelectedStaff);
                                  print(_selectedStaff);
                                  print("inside DropdownButton $selectedValue");
                                });
                              },
                              items: rooms.map((String value) {
                                return new DropdownMenuItem<String>(
                                  value: value,
                                  child: new Text(value),
                                );
                              }).toList()),
                        ),
                      )
                    ],
                  ),
                ),
                load
                    ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircularProgressIndicator(),
                )
                    : Row(
                  children: <Widget>[
                    Expanded(
                        child: Container(
                          child: selectedValue == null ||
                              selectedValue == 'All'
                              ? ListView.builder(
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemCount: staffByParentList?.length ?? 0,
                              itemBuilder:
                                  (BuildContext context, int index) {
                                return CheckboxListTile(
                                  value: _finalSelectedStaff
                                      .contains(staffByParentList[index]),
                                  onChanged: (bool selected) {
                                    _onStaffSelected(selected,
                                        staffByParentList[index]);
                                  },
                                  title: Row(
                                    children: <Widget>[
                                      CircleAvatar(
                                        backgroundImage: staffByParentList[
                                        index]['image'] !=
                                            null
                                            ? NetworkImage(
                                            "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${staffByParentList[index]['image']}")
                                            : AssetImage(
                                            "assets/nophoto.jpg"),
                                        radius: 25.0,
                                      ),
                                      Container(width: 10.0),
                                      Text(staffByParentList[index]
                                      ['fullname'])
                                    ],
                                  ),
                                );
                              })
                              : ListView.builder(
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemCount: _selectedStaff.length,
                              itemBuilder:
                                  (BuildContext context, int index) {
                                return CheckboxListTile(
                                  value: _finalSelectedStaff
                                      .contains(_selectedStaff[index]),
                                  controlAffinity:
                                  ListTileControlAffinity.leading,
                                  onChanged: (bool selected) {
                                    _onStaffSelected(
                                        selected, _selectedStaff[index]);
                                  },
                                  title: Row(
                                    children: <Widget>[
                                      CircleAvatar(
                                        backgroundImage: _selectedStaff[
                                        index]['image'] !=
                                            null
                                            ? NetworkImage(
                                            "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${_selectedStaff[index]['image']}")
                                            : AssetImage(
                                            "assets/nophoto.jpg"),
                                        radius: 25.0,
                                      ),
                                      Container(width: 10.0),
                                      Text(_selectedStaff[index]
                                      ['fullname'])
                                    ],
                                  ),
                                );
                              }),
                        ))
                  ],
                )
              ],
            )
          ],
        ),
      ),
      floatingActionButton: _finalSelectedStaff.length == 0
          ? Container()
          : _getMailFloatingActionButton(),
    );
  }

  FloatingActionButton _getMailFloatingActionButton() {
    return FloatingActionButton(

      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  CreateMessage(_finalSelectedStaff,staffByParentList, childrenData: widget.childrenData,)),
        );
      },
      child: Icon(Icons.message,size: 30.0,),
      backgroundColor: kinderm8Theme.Colors.appcolour,
    );
  }

}