import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/privatemessage/replyprivatemessage.dart';

class ReplyStaffSelection extends StatefulWidget {
  final repliedStaff;
  final singlePrivateMessageData;
  ReplyStaffSelection(this.repliedStaff, this.singlePrivateMessageData);
  @override
  ReplyStaffSelectionState createState() =>
      ReplyStaffSelectionState(repliedStaff, singlePrivateMessageData);
}

class ReplyStaffSelectionState extends State<ReplyStaffSelection> {
  final repliedStaff;
  final singlePrivateMessageData;
  List _finalSelectedStaff = List();

  ReplyStaffSelectionState(this.repliedStaff, this.singlePrivateMessageData);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Staff"),
          elevation: 0.0,
          actions: <Widget>[
            _finalSelectedStaff.length == 0
                ? Container()
                : new IconButton(
                    icon: new Icon(
                      Icons.add_circle_outline,
                      size: 45.0,
                    ),
                    onPressed: () {
                      print(_finalSelectedStaff);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ReplyPrivateMessage(
                                singlePrivateMessageData, _finalSelectedStaff)),
                      );
                    })
          ],
        ),
        body: Center(
            child: Column(
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
                child: Container(height: 5.0)),
            Expanded(
                child: ListView.builder(
                    itemCount: repliedStaff.length,
                    itemBuilder: (context, index) {
                      return CheckboxListTile(
                        value:
                            _finalSelectedStaff.contains(repliedStaff[index]),
                        onChanged: (bool selected) {
                          _onStaffSelected(selected, repliedStaff[index]);
                        },
                        title: Row(
                          children: <Widget>[
                            CircleAvatar(
                              backgroundImage: repliedStaff[index]['image'] !=
                                      null
                                  ? NetworkImage(
                                      "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${repliedStaff[index]['image']}")
                                  : AssetImage("assets/nophoto.jpg"),
                              radius: 25.0,
                            ),
                            Container(width: 10.0),
                            Text(repliedStaff[index]['fullname'])
                          ],
                        ),
                      );
                    }))
          ],
        )));
  }

  void _onStaffSelected(bool selected, staff) {
    if (selected == true) {
      setState(() {
        _finalSelectedStaff.add(staff);
      });
    } else {
      setState(() {
        _finalSelectedStaff.remove(staff);
      });
    }
    print(_finalSelectedStaff);
  }
}
