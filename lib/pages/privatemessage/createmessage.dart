import 'dart:async';
import 'dart:convert';
import 'dart:io';

//import 'package:file_picker/file_picker.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/privatemessage/inboxlist.dart';
import 'package:carem8/utils/network_util.dart';
//import 'package:flutter_document_picker/flutter_document_picker.dart';

class CreateMessage extends StatefulWidget {
  final selectedStaffData;
  final staffByParentList;
  List _finalSelectedStaff = List();
  List<ChildViewModal> childrenData;

  CreateMessage(this.selectedStaffData, this.staffByParentList, {this.childrenData});

  @override
  CreateMessageState createState() => CreateMessageState(selectedStaffData);
}

class CreateMessageState extends State<CreateMessage>
    implements HomePageContract {
  final selectedStaffData;
  var appUser, jwt, userId, clientId;
  List<Widget> receiver = [];
  List<String> receiverId = [];

  // add new persons
  List<Widget> newreceiver = [];
  List<String> newreceiverId = [];

  final formKey = new GlobalKey<FormState>();
  final TextEditingController _textController1 = new TextEditingController();
  final TextEditingController _textController2 = new TextEditingController();
  bool errorMessage = false;
  bool errorSubject = false;
  var awsFolder;
  String imagePath;
  bool uploading = false;
  final platform = const MethodChannel('proitzen.kinderm8/s3_image');

  //To hold image paths after uploading to s3 for adding to db
  List<String> imagePaths = [];
  String encodedImagePathsString = "[]";

  // attachment
  List<File> fileList = new List();
  Future<File> _imageFile;

  String _path = '-';
  bool _pickFileInProgress = false;
  bool _iosPublicDataUTI = true;
  bool _checkByCustomExtension = false;
  bool _checkByMimeType = false;

  List<String> images = [];
  List<File> cameraImages = List<File>();
  String _error = "";
  String _filePath = "";

  //Asanka 3/22/2019
  Map<String, bool> selectedImagesActives = {};
  Map<String, bool> selectedCameraImagesActives = {};

  List<bool> isSelecteds = [];
  List<Widget> staffChips = [];

  //to ensure image is uploading from the native android
  bool isImageUploading = false;

  final _utiController =
      TextEditingController(text: 'com.sidlatau.example.mwfbak');
  final _extensionController = TextEditingController(text: 'mwfbak');
  final _mimeTypeController = TextEditingController(text: 'application/*');

  HomePagePresenter presenter;

  CreateMessageState(this.selectedStaffData) {
    presenter = new HomePagePresenter(this);
    presenter.getUserInfo();
  }

  List selectedReceiverChips = [];

  @override
  void initState() {
    super.initState();

//    widget._finalSelectedStaff.addAll(widget.selectedStaffData);
    selectedReceiverChips = List.from(widget.selectedStaffData);
    selectedReceiverChips.forEach((value) {
      if (!receiverId.contains(value['id'].toString()))
        receiverId.add(value['id'].toString());
    });
    staffChips = selectedReceiverChips.map((f) {
      return Padding(
        padding: const EdgeInsets.all(1.0),
        child: new Chip(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          avatar: CircleAvatar(
            backgroundImage: f['image'] != null
                ? NetworkImage(
                    "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${f['image']}")
                : AssetImage("assets/nophoto.jpg"),
            radius: 25.0,
          ),
          label: Text(
            '${f['fullname']}',
            style: TextStyle(color: Colors.white),
          ),

          backgroundColor: Colors.cyan,
        ),
      );
    }).toList();
//    getNewReceivers();
  }

  @override
  void dispose() {
    super.dispose();
//    images.clear();
//    fileList.clear();
//    imagePaths.clear();
  }

//  void getFilePath() async {
//    try {
//      String filePath = await FilePicker.getFilePath(type: FileType.ANY);
//      if (filePath == '') {
//        return;
//      }
//      setState(() {
//        this._filePath = filePath;
//      });
//    } on PlatformException catch (e) {
//      print("Error while picking the file: " + e.toString());
//    }
//  }

  Future<void> pickImages() async {
    final List result = await platform.invokeMethod('pickImages');
    List<String> resultList = [];
    try {
      if (result != null) {
        resultList = result.cast<String>();
//          images = new List<String>.from(result);
      } else {
        print("error occured");
      }
    } on PlatformException catch (e) {
      print("The problem is: ${e.toString()}");
    }

    if (!mounted) return;

    setState(() {
      images = resultList;
    });
    //Asanka 22/3/2019

    images.forEach((f) => selectedImagesActives[f] = false);
  }

  // Pick image
  pickImage(ctx) {
    // check size
    num size = fileList.length;
    if (size >= 9) {
      Scaffold.of(ctx).showSnackBar(new SnackBar(
        content: new Text("Only add up to 9 pictures!"),
      ));
      return;
    }
    showModalBottomSheet<void>(context: context, builder: _bottomSheetBuilder);
  }

  Widget _bottomSheetBuilder(BuildContext context) {
    return new Container(
        height: 182.0,
        child: new Padding(
          padding: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 30.0),
          child: new Column(
            children: <Widget>[
              _renderBottomMenuItem(
                  Icons.camera_alt, "Camera photo", ImageSource.camera),
              new Divider(
                height: 2.0,
              ),
              _renderBottomMenuItem(
                  Icons.image, "Gallery photo", ImageSource.gallery)
            ],
          ),
        ));
  }

  _renderBottomMenuItem(icon, title, ImageSource source) {
    var item = new Container(
      height: 60.0,
      child: Row(
        children: <Widget>[
          new Padding(
              padding: const EdgeInsets.all(10.0),
              child: new Icon(icon,
                  size: 50.0, color: kinderm8Theme.Colors.appcolour)),
          new Center(
              child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: new Text(
                    title,
                    style: new TextStyle(
                        fontSize: 22.0,
                        color: kinderm8Theme.Colors.appdarkcolour,
                        fontStyle: FontStyle.normal),
                  ))),
        ],
      ),
    );
    return new InkWell(
      child: item,
      onTap: () async {
        Navigator.of(context).pop();
        if (source == ImageSource.camera) {
          _imageFile = ImagePicker.pickImage(source: source);

          _imageFile.then((f) {
            setState(() {
              if (f != null) cameraImages.add(f);
            });
            cameraImages.forEach((f) => selectedCameraImagesActives[f.path] = false);
          });
        } else {
          if (Platform.isAndroid) {
            pickImages();
          } else if (Platform.isIOS) {
            File image = await ImagePicker.pickImage(source: source);
            fileList.add(image);

            fileList.forEach((f) => selectedImagesActives[f.path] = false);
          }
        }
      },
    );
  }

  Widget buildCameraImageGrid(BuildContext context) {
    return GridView.count(
      crossAxisCount: 4,
      crossAxisSpacing: 2.0,
      children: List.generate(cameraImages.length, (index) {
        return InkWell(
          onTap: () {
            setState(() {
              selectedCameraImagesActives[cameraImages[index].path] = !selectedCameraImagesActives[cameraImages[index].path];
            });
          },
          child: new Container(
              decoration: BoxDecoration(
                color: const Color(0xFFECECEC),
                border: (selectedCameraImagesActives[cameraImages[index].path] ?? false)
                    ? Border.all(width: 2.0, color: kinderm8Theme.Colors.appcolour)
                    : Border.all(color: Colors.transparent),
              ),
              margin: const EdgeInsets.all(2.0),
              width: 80.0,
              height: 80.0,
              child: Image.file(cameraImages[index])),
        );
      }),
    );
  }

  Widget buildGridView(BuildContext context) {
    return GridView.count(
      crossAxisCount: 4,
      crossAxisSpacing: 2.0,
      children: List.generate(
          Platform.isAndroid ? images.length + 1 : fileList.length + 1,
          (index) {
        //TODO: images
        // This method body is used to generate an item in the GridView.item
        var content;
        if (index == 0) {
          // Add picture button
          var addCell = new Center(
            child: new Icon(Icons.add,
                size: 50.0, color: kinderm8Theme.Colors.appcolour),
          );
          content = new GestureDetector(
            onTap: () {
              // add pictures
              pickImage(context);
            },
            child: addCell,
          );
        } else {
          // Selected image
          content = InkWell(
            onTap: () {
              setState(() {
                selectedImagesActives[Platform.isIOS ? fileList[index - 1].path : images[index-1]] =
                    !selectedImagesActives[Platform.isIOS ? fileList[index - 1].path : images[index-1]];
              });
            },
            child: Container(
              decoration: BoxDecoration(
                color: const Color(0xFFECECEC),
                border: (selectedImagesActives[Platform.isIOS ? fileList[index - 1].path : images[index-1]] ?? false)
                    ? Border.all(width: 2.0, color: kinderm8Theme.Colors.appcolour)
                    : Border.all(color: Colors.transparent),
              ),
              child: Image.file(
                Platform.isAndroid
                    ? File(images[index - 1])
                    : fileList[index - 1],
                fit: BoxFit.cover,
              ),
            ),
          );
        }
        return new Container(
          margin: const EdgeInsets.all(2.0),
          width: 80.0,
          height: 80.0,
          color: const Color(0xFFECECEC),
          child: content,
        );
      }),
    );
  }

  Widget deleteActionIconWidget(Function action) {
    return Padding(
      padding: const EdgeInsets.only(right: 10.0),
      child: IconButton(
          icon: Icon(
            Icons.delete,
            color: Colors.redAccent,
          ),
          onPressed: action,
      ),
    );
  }

  Widget decideDeleteOrSendActionWidget() {

    Function imagesDeleteAction = () {
      if (Platform.isIOS) {
        var tempList = fileList.toList();
        selectedImagesActives.keys.forEach((key) {
          setState(() {
            tempList.removeWhere((tValue) => (key == tValue.path) && selectedImagesActives[key]);
            selectedImagesActives[key] = false;
          });
        });
        fileList = tempList;
      } else {
        var tempList = images.toList();
        selectedImagesActives.keys.forEach((key) {
          setState(() {
            tempList.removeWhere((tValue) => (key == tValue) && selectedImagesActives[key]);
            selectedImagesActives[key] = false;
          });
        });
        images = tempList;
      }
    };

    Function cameraImagesDeleteAction = () {
      var tempCamList = cameraImages.toList();
      selectedCameraImagesActives.keys.forEach((key) {
        setState(() {
          tempCamList.removeWhere((tValue) => (key == tValue.path) && selectedCameraImagesActives[key]);
          selectedCameraImagesActives[key] = false;
        });
      });
      cameraImages = tempCamList;
    };

    if ((selectedImagesActives.length > 0 && selectedImagesActives.containsValue(true)) && (selectedCameraImagesActives.length > 0 && selectedCameraImagesActives.containsValue(true))) {
      return deleteActionIconWidget ( () {
        imagesDeleteAction();
        cameraImagesDeleteAction();
      });
    } else if (selectedImagesActives.length > 0 && selectedImagesActives.containsValue(true)) {
      return deleteActionIconWidget ( () {
        imagesDeleteAction();
      });
    } else if (selectedCameraImagesActives.length > 0 && selectedCameraImagesActives.containsValue(true)) {
      return deleteActionIconWidget ( () {
        cameraImagesDeleteAction();
        }
      );
    } else {
      return Padding(
        padding: const EdgeInsets.only(right: 13.0),
        child: IconButton(
            icon: Icon(
              Icons.send,
              color: Colors.white,
            ),
            onPressed: () {
              if (_textController1.text.length == 0) {
                setState(() {
                  errorSubject = true;
                });
              } else {
                setState(() {
                  errorSubject = false;
                });
                if (_textController2.text.length == 0) {
                  setState(() {
                    errorMessage = true;
                  });
                } else {
                  setState(() {
                    errorMessage = false;
                  });
                  print(_textController1.text);
                  print(_textController2.text);

                  submitMessage();
                }
              }
            }),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kinderm8Theme.Colors.appdarkcolour,
        title: new Text(
          labelsConfig["composeMessageLabel"],
          style: TextStyle(
            color: Colors.white,
            fontSize: 21.0,
            fontWeight: FontWeight.w400,
          ),
        ),
        actions: <Widget>[
          decideDeleteOrSendActionWidget(),
        ],
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.only(left: 13.0, right: 13.0, top: 15.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FloatingActionButton(
                        child: Icon(
                          Icons.person_add,
                          size: 30.0,
                        ),
                        backgroundColor: kinderm8Theme.Colors.appcolour,
                        onPressed: () {
                          widget.staffByParentList
                              .forEach((_) => isSelecteds.add(false));

                          if (widget.staffByParentList.length > 0) {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return MyDialog(
                                      widget.selectedStaffData,
                                      widget.staffByParentList,
                                      isSelecteds,
                                      _onStaffSelected);
                                });
                          }
                        })
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(left: 13.0, right: 13.0, top: 15.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(child: Text("To:")),
                    Container(width: 5.0),
                    Expanded(
                      child: Wrap(children: staffChips),
                      // Expanded(child: Wrap(children: receiver),)
                    ),
                  ],
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.only(left: 13.0, right: 13.0, top: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Subject " + "*",
                      style: TextStyle(
                          color: errorSubject ? Colors.redAccent : Colors.black,
                          fontSize: 14.0),
                    ),
                    TextField(
                        controller: _textController1,
                        maxLines: null,
                        decoration: InputDecoration(
                            hintText: "Subject",
                            border: OutlineInputBorder(),
                            hintStyle: TextStyle(
                                color: Colors.black54, fontSize: 11.0))),
                  ],
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.only(left: 13.0, right: 13.0, top: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "${labelsConfig["messageLabel"]} *",
                      style: TextStyle(
                          color: errorMessage ? Colors.redAccent : Colors.black,
                          fontSize: 14.0),
                    ),
                    TextField(
                        controller: _textController2,
                        maxLines: 10,
                        decoration: InputDecoration(
                            hintText: "Type your message here...",
                            border: OutlineInputBorder(),
                            hintStyle: TextStyle(
                                color: Colors.black54, fontSize: 11.0))),
                  ],
                ),
              ),
              SingleChildScrollView(
                child: new Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 10.0, vertical: 5.0),
                  margin: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
                  height: Platform.isAndroid
                      ? images.length < 4
                          ? MediaQuery.of(context).size.height / 6
                          : 200.0
                      : fileList.length < 4
                          ? MediaQuery.of(context).size.height / 6
                          : 200.0, //TODO: 120.0 TODO: images
                  child: new Builder(
                    builder: (ctx) {
                      return buildGridView(context);
                    },
                  ),
                ),
              ),
              SingleChildScrollView(
                  child: new Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 2.0),
                      height: cameraImages.length == 0
                          ? 0.0
                          : cameraImages.length < 4 ? 100.0 : 200.0,
                      child: Builder(
                          builder: (context) =>
                              buildCameraImageGrid(context)))),
            ],
          ),
        ),
      ),
    );
  }

  Future<String> _uploadImage(File file, int number) async {
    var awsPathSplited = awsFolder['folder'].toString();
    String path = awsPathSplited;

    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("imagePath", () => file.path);
    args.putIfAbsent("imageNumber", () => number);
    args.putIfAbsent("awsFolder", () => path);
    args.putIfAbsent("clientId", () => clientId);
    args.putIfAbsent("fileExtension", () => '.jpg');

    String result = null;

    if (result == null) {
      setState(() {
        isImageUploading = true;
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) => AlertDialog(
                  title: Center(child: CircularProgressIndicator()),
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Uploading...'),
                    ],
                  ),
                ));
      });
      result = await platform.invokeMethod('sendImage', args);
    }
    Navigator.of(context).pop();
    return result;
  }

  void _onStaffSelected(bool selected, staff) {
    if (selected == true) {
      setState(() {
//        widget._finalSelectedStaff.add(staff);
        selectedReceiverChips.add(staff);
//        receiverId.add(staff['id'].toString());
        selectedReceiverChips.forEach((value) {
          if (!receiverId.contains(value['id'].toString()))
            receiverId.add(value['id'].toString());
        });

        staffChips = selectedReceiverChips.map((f) {
          return Padding(
            padding: const EdgeInsets.all(1.0),
            child: new Chip(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              avatar: CircleAvatar(
                backgroundImage: f['image'] != null
                    ? NetworkImage(
                        "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${f['image']}")
                    : AssetImage("assets/nophoto.jpg"),
                radius: 25.0,
              ),
              label: Text(
                '${f['fullname']}',
                style: TextStyle(color: Colors.white),
              ),

              backgroundColor: Colors.cyan,
            ),
          );
        }).toList();
      });
    } else {
      setState(() {
        selectedReceiverChips.remove(staff);
        receiverId.remove(staff['id'].toString());


        staffChips = selectedReceiverChips.map((f) {
          return Padding(
            padding: const EdgeInsets.all(1.0),
            child: new Chip(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              avatar: CircleAvatar(
                backgroundImage: f['image'] != null
                    ? NetworkImage(
                        "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${f['image']}")
                    : AssetImage("assets/nophoto.jpg"),
                radius: 25.0,
              ),
              label: Text(
                '${f['fullname']}',
                style: TextStyle(color: Colors.white),
              ),

              backgroundColor: Colors.cyan,
            ),
          );
        }).toList();
      });
    }
  }

  Future<Null> uploadAllImages() async {
//    fileList..addAll(cameraImages); // if do this first gallery image ignored
    for (int i = 0; i < images.length; i++) {
      await _uploadImage(File(images[i]), i).then((path) {
        imagePaths.add(path.split('/').last);
        print("image result: $path");
      });
    }

    for (int i = 0; i < cameraImages.length; i++) {
      await _uploadImage(cameraImages[i], i).then((path) {
        imagePaths.add(path.split('/').last);
        print("camera image result: $path");
      });
    }
  }

  Future<String> _uploadIosImage(File file, int index) async {
    const platform = const MethodChannel('kinder.flutter/s3');

    var awsPathSplited = awsFolder['folder'].toString();
    String path = awsPathSplited;

    String fileName = file.path.split("/").last;

    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("path", () => file.path);
    args.putIfAbsent("fileName", () => fileName);
    args.putIfAbsent("aws_folder", () => path);
    args.putIfAbsent("client_id", () => clientId);

    String result = null;

    if (result == null) {
      setState(() {
        isImageUploading = true;
        showDialog(
            context: context,
            barrierDismissible: true,
            //TODO: Make this false after check on double tap
            builder: (context) => AlertDialog(
                  title: Center(child: CircularProgressIndicator()),
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Uploading...'),
                    ],
                  ),
                ));
      });

      try {
        result = await platform.invokeMethod('uploadImage', args);
      } on PlatformException catch (e) {
        print("Failed :'${e.message}'.");
      }
    }
    Navigator.of(context).pop();
    return result;
  }

  Future<Null> uploadAllIosImages() async {
    for (int i = 0; i < fileList.length; i++) {
      await _uploadIosImage(fileList[i], i).then((path) {
        imagePaths.add(path.split('/').last);
        print("image ios result: $path");
      });
    }

    for (int i = 0; i < cameraImages.length; i++) {
      await _uploadImage(cameraImages[i], i).then((path) {
        imagePaths.add(path.split('/').last);
        print("camera image ios result: $path");
      });
    }
  }

  submitMessage() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (Platform.isAndroid) {
      await uploadAllImages();
    } else if (Platform.isIOS) {
      await uploadAllIosImages();
    }

    if (imagePaths.isNotEmpty) {
      encodedImagePathsString = imagePaths.join('|');
    }

    final submitMessageUrl =
        "https://apicare.carem8.com/createprivatemessagenew?clientid=$clientId";

    var body = {
      "subject": _textController1.text,
      "message": _textController2.text,
      "author_id": userId,
      "isread": "0",
      "attachments": "",
      "receiver": receiverId.join(","),
      "folder": awsFolder['folder'].toString(),
      "files": encodedImagePathsString
    }; //encodedImagePathsString
    var headers = {"x-authorization": jwt.toString()};
    print("request body to send: $body");
    NetworkUtil _netUtil = new NetworkUtil();
    _netUtil
        .post(submitMessageUrl,
            body: body, headers: headers, encoding: jwt.toString())
        .then((res) {
      print("resss  $res");
      if (res == 1) {
        String _refreshTokenUrl =
            'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

        NetworkUtil _netUtil = new NetworkUtil();
        _netUtil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          var refreshJwtToken = json.decode(response.body);
          this.jwt = refreshJwtToken;
          submitMessage();
        });
      } else if (res == '') {
        print("may be 200");
        setState(() {
          isImageUploading = false;
        });
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => PrivatemessageList(childrenData: widget.childrenData,)));
      } else {
        print("error");
      }
    });
  }

  Future<String> getFolder() async {
    print("Folder data fetched");

    String _awsfoldernameUrl =
        'https://apicare.carem8.com/awsfoldername?clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};
    var body = {"user_id": userId};
    NetworkUtil _netUtil = new NetworkUtil();
    _netUtil
        .post(_awsfoldernameUrl,
            body: body, headers: headers, encoding: jwt.toString())
        .then((res) {
      print('jwt### $jwt');
      print('aws folder: $res');

      if (res == 1) {
        print("retrying...");
        getRefreshToken();
      } else if (res.length > 7) {
        awsFolder = json.decode(res);
        setState(() {});
      } else {
        print("error");
      }
    });
    return null;
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

    NetworkUtil _netUtil = new NetworkUtil();
    _netUtil.get(_refreshTokenUrl).then((response) {
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } on FormatException catch (e) {
        print(e.toString());
      } on NoSuchMethodError catch (e) {
        print(e.toString());
      }
      this.jwt = refreshJwtToken;
      getFolder();
    });
  }

  @override
  void onDisplayUserInfo(User user) {
    appUser = user.center;
    try {
      final parsed = json.decode(appUser);
      var appUsers = parsed[0];
      jwt = appUsers["jwt"] + "hii";
      print("******$jwt");
      var users = appUsers["user"];
      clientId = users["client_id"];
      userId = users["id"];

      print("iddd $userId");
      print(clientId);
      getFolder();
    } catch (e) {
      print("That string didn't look like Json.");
    }
  }

  @override
  void onErrorUserInfo() {}

  @override
  void onUpdateJwt() {}

  @override
  void onLogoutUser() {}
}

class Param extends StatelessWidget {
  final bool isEnabled;
  final ValueChanged<bool> onEnabledChanged;
  final TextEditingController controller;
  final String description;
  final String textLabel;

  Param({
    @required this.isEnabled,
    this.onEnabledChanged,
    this.controller,
    this.description,
    this.textLabel,
  })  : assert(isEnabled != null),
        assert(onEnabledChanged != null),
        assert(description != null),
        assert(textLabel != null),
        assert(controller != null);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: Text(
                  description,
                  softWrap: true,
                ),
              ),
            ),
            Checkbox(
              value: isEnabled,
              onChanged: onEnabledChanged,
            ),
          ],
        ),
        TextField(
          controller: controller,
          enabled: isEnabled,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: textLabel,
          ),
        ),
      ],
    );
  }
}

class ParamsCard extends StatelessWidget {
  final String title;
  final List<Widget> children;

  ParamsCard({
    @required this.title,
    @required this.children,
  })  : assert(title != null),
        assert(children != null);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 24.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: Text(
                  title,
                  style: Theme.of(context).textTheme.headline,
                ),
              ),
            ]..addAll(children),
          ),
        ),
      ),
    );
  }
}

class MyDialog extends StatefulWidget {
  final selectedStaffData;
  final staffByParentList;
  final List _finalSelectedStaff = List();
  final List<bool> isSelecteds;
  final Function onStaffSelected;

  MyDialog(this.selectedStaffData, this.staffByParentList, this.isSelecteds,
      this.onStaffSelected);

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  List selectedStaffData;
  List staffByParentList;
  List _finalSelectedStaff = List();
  List<bool> isSelecteds;
  Function onStaffSelected;

  @override
  void initState() {
    selectedStaffData = widget.selectedStaffData;
    staffByParentList = widget.staffByParentList;
    isSelecteds = widget.isSelecteds;
    onStaffSelected = widget.onStaffSelected;

    selectedStaffData.forEach((item) {
      setState(() {
        staffByParentList
            .removeWhere((test) => test['fullname'] == item['fullname']);
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: ListView.builder(
        itemCount: staffByParentList.length,
        itemBuilder: (BuildContext context, int index) {
          return CheckboxListTile(
            value: isSelecteds[index],
            onChanged: (bool selected) {
              setState(() {
                isSelecteds[index] = !isSelecteds[index];
              });
              onStaffSelected(isSelecteds[index], staffByParentList[index]);
            },
            title: Row(
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: staffByParentList[index]['image'] != null
                      ? NetworkImage(
                          "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${staffByParentList[index]['image']}")
                      : AssetImage("assets/nophoto.jpg"),
                  radius: 25.0,
                ),
                Container(width: 10.0),
                Expanded(
                  child: Text(staffByParentList[index]['fullname']),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
