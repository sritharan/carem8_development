import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:carem8/Theme.dart' as Kinderm8Theme;
import 'package:flutter/material.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/dailychart/dailychartdata.dart';
import 'package:carem8/pages/dailyjournal/dailyjournaldata.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';

class Medications extends StatefulWidget {
  final childData;
  final jwt;
  Medications(this.childData, this.jwt);
  @override
  MedicationsState createState() => MedicationsState();
}

class MedicationsState extends State<Medications>
    implements HomePageContract {
  var k, appuser, jwt, id, clientId;
  List data;
  var childId;
  bool isLoading;
  bool load = true;
  HomePagePresenter _presenter;
  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  var medidate;
  var casualattendancedate;
  var medicationcollection,prescribed_medi_collection,nonprescribed_medi_collection;
  var casualattendancedata;
  bool enabled = false;
  bool expanded = false, foodexpanded = false, nappyexpanded = false,bottleexpanded = false, restexpanded = false,sunscreenexpanded = false;

  DailyChartState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }
  var medicationData;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var date = new DateTime.now();
    isLoading = false;
    load = false;

    // Get Casualattendance data while load view
    fetchCasualattendanceData(clientId,date);
  }

  void handleNewDate(date) {
    print("handleNewDate ${date}");
    var timeStamp = new DateFormat("yyyy-MM-dd");
    medidate = new DateTime(date.year, date.month, date.day);
    String formatmedidate = timeStamp.format(medidate);
    print("handleNewDate>>formatdailychartdate ${formatmedidate}");
//    fetchMedicationData(date);
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Kinderm8Theme.Colors.appcolour,
    containerColor: Kinderm8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  // get Casualattendance
  Future<String> fetchCasualattendanceData(client,date) async {
    var timeStamp = new DateFormat("yyyy-MM-dd");
    casualattendancedate = new DateTime(date.year, date.month, date.day);
    String formatdailychartdate = timeStamp.format(casualattendancedate);
    print("formatdailychartdate ${formatdailychartdate}");
    ///data from GET method
    print("data fetched");
    childId = widget.childData["id"];
    String _dailyJournalUrl =
        'https://apicare.carem8.com/v4.4.0/getcasualattendancedates/$childId?clientid=$clientId&date=$formatdailychartdate';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_dailyJournalUrl, headers: headers).then((response) {
      var casualattendancedata;
      try {
        casualattendancedata = json.decode(response.body);
        print(casualattendancedata.length);
        print('res get ${response.body}');
        print('casualattendancedata UrlData $casualattendancedata');
      } catch (e) {
        print('That string was null!');
      }

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(isLoading);
        isLoading = false;
        print(isLoading);
        k = casualattendancedata.length;
      } else if (response.statusCode == 500 &&
      casualattendancedata["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        isLoading = false;
        if(clientId !=null) {
          getRefreshToken_Casualattendance(date);
        }
      } else {
        fetchCasualattendanceData(clientId,date);
      }
    });
    return null;
  }
  getRefreshToken_Casualattendance(date) {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      }catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;
      isLoading = false;
      if ( date != null) {
        fetchCasualattendanceData(clientId,date);
      }
    });
  }


  Future<String> fetchMedicationData(date,int s) async {
    var timeStamp = new DateFormat("yyyy-MM-dd");
    medidate = new DateTime(date.year, date.month, date.day);
    String formatdailychartdate = timeStamp.format(medidate);
    print("formatdailychartdate ${formatdailychartdate}");
    ///data from GET method
    print("data fetched");
    childId = widget.childData["id"];
    String _dailyJournalUrl =
        'https://apicare.carem8.com/v2.1.1/getdailychartenteries/$childId?date=$formatdailychartdate&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_dailyJournalUrl, headers: headers).then((response) {
      var dailyChartData;
      try {
        dailyChartData = json.decode(response.body);
        print(dailyChartData.length);
        print('res get ${response.body}');
        print('dailyChart UrlData $dailyChartData');
      } catch (e) {
        print('That string was null!');
      }

      print('jwt### $jwt');
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(isLoading);
        isLoading = false;
        print(isLoading);
        if (s == 0) {
          setState(() {
            load = false;
            this.medicationcollection = medicationcollection;
          });
        } else {
          setState(() {
            load = false;
            medicationcollection.addAll(medicationcollection);
          });
        }
        k = medicationcollection.length;
      } else if (response.statusCode == 500 &&
          dailyChartData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        isLoading = false;
//        if(clientId !=null) {
          getRefreshToken(date);
//        }
      } else {
        fetchMedicationData(date,0);
      }
    });
    return null;
  }
  getRefreshToken(date) {

    print("refreshing Token..");
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      }catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;
      isLoading = false;
      if ( date != null) {
        fetchMedicationData(date,0);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    print('singledailychartdata $medicationData');
    medicationcollection = medicationData;
    print("inside build ${medicationcollection}");


    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text("Medications",
            style: TextStyle(
              color: Colors.white,
            )),
        backgroundColor: Kinderm8Theme.Colors.appcolour,
        centerTitle: true,
      ),
//      drawer: new CommonDrawer(),
//      body: load
//              ? progress
//              : DailyChartData(dailychartData, jwt),
      body: isLoading? new Container(
        margin: new EdgeInsets.symmetric(
          horizontal: 5.0,
          vertical: 10.0,
        ),
        child: new ListView(
          shrinkWrap: true,
          children: <Widget>[
//            new Text(isLoading.toString()),

          ],
        ),
      ) : progress,

    );
  }




  Future<Null> handleRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      load = true;
      fetchMedicationData(medidate,0);
    });
    return null;
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = widget.jwt.toString();
//      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

      print("iddd $id");
      print(clientId);
      fetchMedicationData(medidate,0);

    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
