import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:carem8/pages/medication/widgets/page_dragger.dart';

import 'package:flutter/scheduler.dart';

class DatePickPageInheritedWidget extends InheritedWidget {
  final _DatePickPagerState data;

  DatePickPageInheritedWidget({Key key, this.data, Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(DatePickPageInheritedWidget old) {
    return true;
  }
}

class DatePickPager extends StatefulWidget {
  DatePickPager({Key key,
    this.child,
    this.updateMedicationDataByDay,
    this.whenPrevBtPress,
    this.isDateHolderActive,
    this.isPrevActive,
    this.isPreviouslyPrescribed,
    this.dayAvailabilityMap,
    this.toggleIsPrevActive,
    this.toggleIsDateHolderActive,
  }) : super(key: key);

  final Widget child;
  final ValueChanged<int> updateMedicationDataByDay;
  final ValueChanged<String> whenPrevBtPress;
  final bool isDateHolderActive;
  final bool isPrevActive;
  final bool isPreviouslyPrescribed;
  final Map<int, bool> dayAvailabilityMap;
  final Function toggleIsPrevActive;
  final Function toggleIsDateHolderActive;

  @override
  _DatePickPagerState createState() => _DatePickPagerState();
}

class _DatePickPagerState extends State<DatePickPager>
    with TickerProviderStateMixin {
  DateTime date;
  int _selectedDay;
  int monthDateCount;

  //Toggle between prev and day holders
  bool isDateHolderActive;
  bool isPrevActive;
  bool isPreviouslyPrescribed;

  Map<int, bool> dayAvailabilityMap;

  StreamController<SlideUpdate> slideUpdateStream;
  AnimatedPageDragger animatedPageDragger;

  ScrollController dateIndicatorController;

  int activeIndex = 0;
  int nextPageIndex = 0;
  SlideDirection slideDirection = SlideDirection.none;
  double slidePercent = 0.0;

  SlideUpdate slideUpdate =
      SlideUpdate(UpdateType.doneDragging, SlideDirection.none, 0.0);

  _DatePickPagerState() {
    slideUpdateStream = new StreamController<SlideUpdate>();

    slideUpdateStream.stream.listen((SlideUpdate event) {
      setState(() {
        slideUpdate = event;
        if (event.updateType == UpdateType.dragging) {
          slideDirection = event.direction;
          slidePercent = event.slidePercent;

          if (slideDirection == SlideDirection.leftToRight) {
            nextPageIndex = _selectedDay - 1;
            if (slidePercent > 0.5) {
              moveRight();
            }
          } else if (slideDirection == SlideDirection.rightToLeft) {
            nextPageIndex = _selectedDay + 1;
            if (slidePercent > 0.5) {
              moveLeft();
            }
          } else {
            nextPageIndex = _selectedDay;
          }
        } else if (event.updateType == UpdateType.doneDragging) {
          if (slidePercent > 0.5) {
            animatedPageDragger = new AnimatedPageDragger(
              slideDirection: slideDirection,
              transitionGoal: TransitionGoal.open,
              slidePercent: slidePercent,
              slideUpdateStream: slideUpdateStream,
              vsync: this,
            );
          } else {
            animatedPageDragger = new AnimatedPageDragger(
              slideDirection: slideDirection,
              transitionGoal: TransitionGoal.close,
              slidePercent: slidePercent,
              slideUpdateStream: slideUpdateStream,
              vsync: this,
            );

            nextPageIndex = _selectedDay;
          }

          animatedPageDragger.run();
        } else if (event.updateType == UpdateType.animating) {
          slideDirection = event.direction;
          slidePercent = event.slidePercent;
        } else if (event.updateType == UpdateType.doneAnimating) {
          _selectedDay = nextPageIndex;

          slideDirection = SlideDirection.none;
          slidePercent = 0.0;

          animatedPageDragger.dispose();
        }
      });
      isPrevActive = false;
      isDateHolderActive = true;
      widget.toggleIsDateHolderActive(true);
      widget.updateMedicationDataByDay(_selectedDay);
    });
  }

  moveRight() {
    dateIndicatorController.animateTo(dateIndicatorController.offset - 50,
        curve: Curves.linear, duration: Duration(milliseconds: 300));
  }

  moveLeft() {
    dateIndicatorController.animateTo(dateIndicatorController.offset + 50,
        curve: Curves.linear, duration: Duration(milliseconds: 300));
  }

  void toggleDateHolderState(bool flag) {
    widget.toggleIsDateHolderActive(flag);
  }

  void togglePrevState(bool flag) {
    widget.toggleIsPrevActive(flag);
  }

  void _incrementCounter(int value) {
    setState(() {
      _selectedDay = value;
      nextPageIndex = _selectedDay + 1;
      isDateHolderActive = true;
      isPrevActive = false;
    });
    widget.updateMedicationDataByDay(value);
  }

  void prevBtPressedAction() {
    setState(() {
      isDateHolderActive = false;
      isPrevActive = true;
      _selectedDay = 0;
      activeIndex = 0;
      nextPageIndex = 1;
    });
    widget.whenPrevBtPress("previous");
//    widget.updateMedicationDataByDay(_selectedDay);
  }

  @override
  void initState() {
    dateIndicatorController = ScrollController();
    date = DateTime.now();
    setState(() {
      _selectedDay = date.day;
      activeIndex = date.day;
      final DateTime dateForValues = new DateTime(date.year, date.month + 1, 0);
      monthDateCount = dateForValues.day;

      isDateHolderActive = widget.isDateHolderActive;
      isPrevActive = widget.isPrevActive;
      isPreviouslyPrescribed = widget.isPreviouslyPrescribed;

      dayAvailabilityMap = widget.dayAvailabilityMap;
    });
    SchedulerBinding.instance.addPostFrameCallback((_) => setState((){
      dateIndicatorController.jumpTo(45.0 * activeIndex);
      widget.updateMedicationDataByDay(activeIndex);
    }));
    super.initState();
  }

  @override
  void dispose() {
    slideUpdateStream.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
//    print('This is the day 1 medication: $medicationCardDataGroup');
    return new DatePickPageInheritedWidget(
      data: this,
      child: ListView(
        children: <Widget>[
          DateIndicator(),
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height / 1.35,
            child: PageDragger(
              canDragLeftToRight: _selectedDay > 1,
              canDragRightToLeft: _selectedDay < monthDateCount - 1,
              slideUpdateStream: this.slideUpdateStream,
              child: widget.child,
            ),
          ),
        ],
      ),
    );
  }
}

class DateIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final myInheritedWidget = context
        .ancestorInheritedElementForWidgetOfExactType(
            DatePickPageInheritedWidget)
        .widget as DatePickPageInheritedWidget;
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 68.0,
      padding:
          const EdgeInsets.only(left: 7.0, right: 3.0, top: 2.0, bottom: 2.0),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor.withAlpha(210),
        boxShadow: [
          BoxShadow(
              color: Colors.black12,
              offset: Offset(0.0, 3.0),
              blurRadius: 1.0,
              spreadRadius: 0.2),
        ],
      ),
      child: ListView.builder(
          controller: myInheritedWidget.data.dateIndicatorController,
          scrollDirection: Axis.horizontal,
          itemCount: myInheritedWidget.data.monthDateCount + 1,
          itemBuilder: (BuildContext context, int index) {
            return (index == 0)
                ? CircleButtonHolder(
                    labelText: "Prv", holderIcon: Icons.arrow_left, isActive: myInheritedWidget.data.isPreviouslyPrescribed,)
                : DateHolder(
                    index); //(myInheritedWidget.data.dayAvailabilityMap[index] ?? true) ?
          }),
    );
  }
}

class DateHolder extends StatelessWidget {
  DateHolder(this.index);

  final int index;

  final Widget activeBubble = Container(
    width: 15.0,
    height: 15.0,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      color: Colors.orange,
    ),
  );

  @override
  Widget build(BuildContext context) {
    final myInheritedWidget = context
        .ancestorInheritedElementForWidgetOfExactType(
            DatePickPageInheritedWidget)
        .widget as DatePickPageInheritedWidget;
    return InkWell(
      onTap: () {
        myInheritedWidget.data._incrementCounter(index);
        myInheritedWidget.data.togglePrevState(false);
        myInheritedWidget.data.toggleDateHolderState(true);
      },
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.only(right: 5.0),
                  child: Text(
                    "${DateFormat('EEEE').format(DateTime(myInheritedWidget.data.date.year, myInheritedWidget.data.date.month, index)).substring(0, 1)}",
                    style: TextStyle(color: Colors.white, fontSize: 12.0),
                  )),
              Container(
                width: 45.0,
                height: 45.0,
                margin: const EdgeInsets.only(right: 5.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.blue,
                  border: (index == (myInheritedWidget.data._selectedDay) &&
                          myInheritedWidget.data.isDateHolderActive == true)
                      ? Border.all(width: 2.0, color: Colors.white)
                      : Border.all(color: Colors.transparent),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      "$index",
                      style: TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
          (myInheritedWidget.data.dayAvailabilityMap[index] ?? false)
              ? Positioned(right: 8.0, bottom: 5.0, child: activeBubble)
              : Container(),
        ],
      ),
    );
  }
}

class CircleButtonHolder extends StatefulWidget {
  CircleButtonHolder({
    this.labelText,
    this.holderIcon,
    this.onTapFunction,
    this.isActive = false,
  });

  final String labelText;
  final IconData holderIcon;
  final ValueChanged<String> onTapFunction;
  final bool isActive;

  @override
  _CircleButtonHolderState createState() => _CircleButtonHolderState();
}

class _CircleButtonHolderState extends State<CircleButtonHolder> {
  @override
  void initState() {
    super.initState();
  }

  final Widget activeBubble = Container(
    width: 15.0,
    height: 15.0,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      color: Colors.orange,
    ),
  );

  @override
  Widget build(BuildContext context) {
    final myInheritedWidget = context
        .ancestorInheritedElementForWidgetOfExactType(
            DatePickPageInheritedWidget)
        .widget as DatePickPageInheritedWidget;
    return InkWell(
      onTap: () {
        myInheritedWidget.data.prevBtPressedAction();
        myInheritedWidget.data.togglePrevState(true);
        myInheritedWidget.data.toggleDateHolderState(false);
      },
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.only(right: 5.0),
                  child: Text(
                    widget.labelText,
                    style: TextStyle(color: Colors.white, fontSize: 12.0),
                  )),
              Container(
                width: 45.0,
                height: 45.0,
                margin: const EdgeInsets.only(right: 5.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.blue,
                  border: myInheritedWidget.data.isPrevActive
                      ? Border.all(width: 2.0, color: Colors.white)
                      : Border.all(color: Colors.transparent),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Icon(
                      widget.holderIcon,
                      size: 22.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
          (myInheritedWidget.data.isPreviouslyPrescribed)
              ? Positioned(right: 8.0, bottom: 5.0, child: activeBubble)
              : Container(),
        ],
      ),
    );
  }
}
