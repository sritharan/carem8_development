
import 'package:flutter/material.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/medication/detailviewnonpriscribeddata.dart';
import 'package:carem8/pages/medication/detailviewpriscribed.dart';
import 'package:carem8/pages/medication/edit_prescribed.dart';
import 'package:carem8/pages/medication/repeat_prescribed.dart';
import 'package:carem8/pages/medication/repeat_prescribed_detaillist.dart';
import 'package:carem8/pages/medication/updatenonprescribed.dart';

class MedicationOptionsDialog extends StatelessWidget {

  MedicationOptionsDialog({this.childData, this.jwt, this.medData, this.deleteMedi,
                            this.canMediDelete,
                            this.canMediEdit,}) {
    //TODO: ensure the last medication
    if (medData["repeat_medication"] != null && medData["repeat_medication"].length > 0) lastMedicationDates.add(medData["repeat_medication"][0]["medicationtimes"].reversed.toList()[0]);
  }

  final childData;
  final jwt;
  final medData;
  final List<String> lastMedicationDates = [];
  final Function deleteMedi;
  final int canMediDelete;
  final int canMediEdit;

  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
        child: Container(
          width: double.infinity,
//                    margin: EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.purple.withAlpha(200),
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 12.0, bottom: 0.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 14.0, right: 12.0, bottom: 10.0),
                  child: (medData["medication_name"] != null)
                    ? Row(
                        children: <Widget>[
                          Expanded(child: Text(medData["medication_name"], style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),)),
                          (canMediDelete == 1) ? IconButton(
                            icon: Icon(Icons.delete, color: Colors.red,),
                            onPressed: () {
                              (canMediDelete == 1)
                              ? showDialog(
                                context: context,
                                builder: (BuildContext context) =>
                                    AlertDialog(
                                      title: Text("Are you sure?", style: TextStyle(fontWeight: FontWeight.w300),),
                                      content: Text("${medData["medication_name"]} will be permanently deleted!"),
                                      actions: <Widget>[
                                        RaisedButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                            deleteMedi(medData["medication_id"]);
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 12.0),
                                            child: Text("Yes", style: TextStyle(color: Colors.white)),
                                          ),
                                        ),
                                        RaisedButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 12.0),
                                            child: Text("No", style: TextStyle(color: Colors.white)),
                                          ),
                                        ),
                                      ],
                                    )
                              ) : SizedBox();
                            }
                          ) : SizedBox(),
                          IconButton(
                            icon: Icon(Icons.edit, color: Colors.green,),
                            onPressed: () {
                              Navigator.pop(context);
                              Navigator.push(context, MaterialPageRoute(
                                builder: (context) =>
                                    EditPrescribed(childData, medData, jwt),
                              ));
                            }
                          ),
                        ],
                      )
                    : SizedBox(),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 14.0, right: 12.0, bottom: 8.0),
                  child: (medData["started_on"] != null)
                    ? Row(
                        children: <Widget>[
                          Text("${labelsConfig["medicationStartingOnLabel"]}: "),
                          Expanded(child: Text(medData["started_on"])),
                        ],
                      )
                    : SizedBox(),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 14.0, right: 12.0, bottom: 8.0),
                  child: (medData["dosage"] != null)
                      ? Row(
                          children: <Widget>[
                            Text("${labelsConfig["dosageLabel"]}: "),
                            Expanded(child: Text(medData["dosage"])),
                          ],
                        )
                      : SizedBox(),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 14.0, right: 12.0, bottom: 8.0),
                  child: (lastMedicationDates.length > 0 && lastMedicationDates[0] != null)
                      ? Row(
                          children: <Widget>[
                            Text("${labelsConfig["lastMedicationOnLabel"]} on: "),
                            Expanded(child: Text(lastMedicationDates[0])),
                          ],
                        )
                      : SizedBox(),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10.0))),
                        onPressed: (){
                          Navigator.pop(context);
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (context) =>
//                                      ViewRepeatPrescribed(childData,
//                                          medData, jwt)));
                        //TODO: ask to ensure what is the correct one
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ViewPrescribed(medData)),
                          );
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 18.0),
                          child: Text(labelsConfig["administeredMedicationLabel"]),
                        ),
                      ),
                    ),
                    Expanded(
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomRight: Radius.circular(10.0))),
                        onPressed: (){
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      CreateRepeatPrescribed(childData,
                                          medData, jwt)));
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 18.0),
                          child: Text(labelsConfig["repeatMedicationLabel"].split(" ")[0]),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
  }
}

class NonMedicationOptionsDialog extends StatelessWidget {

  NonMedicationOptionsDialog({this.childData, this.jwt, this.medData, this.deleteMedi,
                            this.canMediDelete,
                            this.canMediEdit,}) {
    //TODO: ensure the last medication
    if (medData["repeat_medication"] != null && medData["repeat_medication"].length > 0) lastMedicationDates.add(medData["repeat_medication"][0]["medicationtimes"].reversed.toList()[0]);
  }

  final childData;
  final jwt;
  final medData;
  final List<String> lastMedicationDates = [];
  final Function deleteMedi;
  final int canMediDelete;
  final int canMediEdit;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        width: double.infinity,
//                    margin: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.purple.withAlpha(200),
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 12.0, bottom: 0.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 14.0, right: 12.0, bottom: 10.0),
                child: (medData["medication_to_administer"] != null)
                    ? Row(
                  children: <Widget>[
                    Expanded(child: Text(medData["medication_to_administer"], style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),)),
                    (canMediDelete == 1) ? IconButton(
                        icon: Icon(Icons.delete, color: Colors.red,),
                        onPressed: () {
                          (canMediDelete == 1)
                              ? showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  AlertDialog(
                                    title: Text("Are you sure?", style: TextStyle(fontWeight: FontWeight.w300),),
                                    content: Text("${medData["medication_to_administer"]} will be permanently deleted!"),
                                    actions: <Widget>[
                                      RaisedButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          deleteMedi(medData["medication_id"]);
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 12.0),
                                          child: Text("Yes", style: TextStyle(color: Colors.white)),
                                        ),
                                      ),
                                      RaisedButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 12.0),
                                          child: Text("No", style: TextStyle(color: Colors.white)),
                                        ),
                                      ),
                                    ],
                                  )
                          ) : SizedBox();
                        }
                    ) : SizedBox(),
                    (canMediEdit == 1) ? IconButton(
                        icon: Icon(Icons.edit, color: Colors.green,),
                        onPressed: () {
                          Navigator.pop(context);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => UpdateNonPrescribed(
                                          medData,
                                          childData,
                                          jwt)));
                        }
                    ) : SizedBox(),
                  ],
                )
                    : SizedBox(),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 14.0, right: 12.0, bottom: 8.0),
                child: (medData["authorized_date"] != null)
                    ? Row(
                  children: <Widget>[
                    Text("${labelsConfig["dateAuthorizedLabel"]}: "),
                    Expanded(child: Text(medData["authorized_date"])),
                  ],
                )
                    : SizedBox(),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 14.0, right: 12.0, bottom: 8.0),
                child: (medData["dosage"] != null)
                    ? Row(
                  children: <Widget>[
                    Text("${labelsConfig["dosageLabel"]}: "),
                    Expanded(child: Text(medData["dosage"])),
                  ],
                )
                    : SizedBox(),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 14.0, right: 12.0, bottom: 8.0),
                child: (lastMedicationDates.length > 0 && lastMedicationDates[0] != null)
                    ? Row(
                  children: <Widget>[
                    Text("${labelsConfig["lastMedicationOnLabel"]}: "),
                    Expanded(child: Text(lastMedicationDates[0])),
                  ],
                )
                    : SizedBox(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10.0), bottomRight: Radius.circular(10.0))),
                      onPressed: (){
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  ViewNonPrescribed(medData)),
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 18.0),
                        child: Text(labelsConfig["administeredLabel"]),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
