import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carem8/Theme.dart' as kinderM8Theme;
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:carem8/pages/medication/prescribedmedicationData.dart';
import 'package:carem8/pages/medication/widgets/date_pick_pager.dart';
import 'package:carem8/pages/medication/widgets/floating_actions.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:flutter/scheduler.dart';

class Prescribed extends StatefulWidget {
  final ChildViewModal childData;
  final jwt;

  Prescribed(this.childData, this.jwt);

  @override
  PrescribedState createState() => PrescribedState();
}

class PrescribedState extends State<Prescribed>
    with SingleTickerProviderStateMixin
    implements HomePageContract {
  int apiPaginateSize, len = 0;
  var users, jwt, id, clientId;
  var childId;
  bool isLoading;
  bool load = true;
  HomePagePresenter _presenter;

  NetworkUtil _netUtil;

  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  //To assign day relevant data that coming from parent widget(cannot use generics: MedicationCardData again the way written, have to do more work)
  List medicationCardDataGroup = [];
  List prevMonthMedicationList = [];
  List nextMonthMedicationList = [];
  ScrollController prescribedScrollController;
  List longLiveList = [];

  var temp;
  var pagerDate;

  PrescribedState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  var medicationData;

  var progress = new ProgressBar(
    color: kinderM8Theme.Colors.appcolour,
    containerColor: kinderM8Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  //Asanka's edits cannot add top, all are mess

  bool isPreviousButtonActive;
  bool isDateHolderActive;

  var medData;

  //Keeping a map to display date holders if med available
  Map<int, bool> medicationAvailabilityMap = {};
  bool isPreviouslyPrescribed = false;

  @override
  void initState() {
    prescribedScrollController = ScrollController()..addListener(_scrollListener);
    temp = DateTime.now().toUtc();
    pagerDate = DateTime.utc(temp.year,temp.month);
    _netUtil = new NetworkUtil();
    isPreviousButtonActive = false;
    isDateHolderActive = true;
    apiPaginateSize = 0;
    fetchMedicationData(apiPaginateSize);
    super.initState();
  }

  @override
  void dispose() {
    prescribedScrollController.removeListener(_scrollListener);
    prescribedScrollController.dispose();
    super.dispose();
  }

  void toggleIsPrevActive(bool flag) {
    setState(() {
      isPreviousButtonActive = flag;
    });
  }

  void toggleDateHolderActive(bool flag) {
    setState(() {
      isDateHolderActive = flag;
    });
  }

  void updateMedicationDataBasedOnAction([int selectedDay = 0]) {
    DateTime pagerDateWithDay = DateTime.now();
    final int today = pagerDateWithDay.day;
    toggleDateHolderActive(true);
    toggleIsPrevActive(false);
    setState(() {
      medicationCardDataGroup = [];
    });
    if (medData != null) {
      if (!(medData is Map)) {
        if (selectedDay != -1) {
          SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
            medicationCardDataGroup = longLiveList.where((value) {
              DateTime upcomingDate = DateTime.parse(value["started_on"]);
              print("This is upcomingDate: $upcomingDate");
              print("Selected day: $selectedDay");//pagerDateWithDay.month
              return (upcomingDate.year == pagerDateWithDay.year) && (upcomingDate.month == pagerDate.month) &&
                  (upcomingDate.day ==
                      ((selectedDay == 0) ? today : selectedDay));
            }).toList();
          }));
        }
      }
      print("Here is the available data: ${medicationCardDataGroup.length}");
    }
    print("Here is the available data after fetch prev bt: ${medicationCardDataGroup.length}");
  }

  void whenPreviousButtonPressed([String type = "previous"]) {
    toggleDateHolderActive(false);
    toggleIsPrevActive(true);
// setState(() {
////      medicationCardDataGroup = [];
//    });
//    SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
//      if (medData != null) {
//        if (!(medData is Map)) {
//          medicationCardDataGroup = medData.where((value) {
//            DateTime upcomingDateUtc = DateTime.parse(value["started_on"]).toUtc();
//            DateTime upcomingDate = DateTime.utc(upcomingDateUtc.year, upcomingDateUtc.month);
//            return (upcomingDate.compareTo(pagerDate) < 0);
//          }).toList();
//        }
//      }
//    }));
    print("Here is the available data after fetch prev bt: ${prevMonthMedicationList.length}");
  }

  void saveMedicationAvailabilityMap() {
    //for to display day holder
    if (medData != null) {
      if (!(medData is Map)) {
        int index = 1;
        medData.forEach((item) {
          if (item["started_on"] != null) {
            DateTime upcomingDate = DateTime.parse(item["started_on"]);
            if (upcomingDate.year == pagerDate.year && upcomingDate.month == pagerDate.month) {
              setState(() {
                medicationAvailabilityMap[upcomingDate.day] = true;
              });
            }

            if (upcomingDate.month < pagerDate.month) {
              setState(() {
                isPreviouslyPrescribed = true;
              });
            }
          } else {
            setState(() {
              medicationAvailabilityMap[index] = false;
            });
          }
          index++;
        });
      }
    }
  }

  Future<String> fetchMedicationData(int paginateLength) async {
    childId = widget.childData.id;
    String _medicationUrl =
        'https://apicare.carem8.com/v2.1.1/medication/$childId?step=$paginateLength&clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};
    _netUtil.get(_medicationUrl, headers: headers).then((response) {
      try {
        medData = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }

      saveMedicationAvailabilityMap();

      if (response.statusCode == 200) {
        longLiveList.addAll(medData);
        isLoading = false;
        if (paginateLength == 0) {
          setState(() {
            load = false;
            len = longLiveList.length;
          });
          medData.forEach((item) {
            if (item["started_on"] != null) {
//              DateTime upcomingDate = DateTime.parse(item["started_on"]);
            DateTime upcomingDateUtc = DateTime.parse(item["started_on"]).toUtc();
            DateTime upcomingDate = DateTime.utc(upcomingDateUtc.year, upcomingDateUtc.month);
              if (upcomingDate.compareTo(pagerDate) == 0) {
                setState(() {
                  medicationCardDataGroup.add(item);
//                  apiPaginateSize = medicationCardDataGroup.length;
                });
              }

//              if (upcomingDate.month < pagerDate.month) {
//              print("compare ${upcomingDate.compareTo(pagerDate)}");
              if (upcomingDate.compareTo(pagerDate) < 0) {
                setState(() {
                  prevMonthMedicationList.add(item);
//                  apiPaginateSize = prevMonthMedicationList.length;
                });
              }

              if (upcomingDate.compareTo(pagerDate) > 0) {
                setState(() {
                  nextMonthMedicationList.add(longLiveList);
//                  apiPaginateSize = nextMonthMedicationList.length;
                });
              }
            }
          });
          setState(() {
            apiPaginateSize = longLiveList.length;
          });
        } else {
          setState(() {
            load = false;
            len = medData.length;
//            medicationCardDataGroup.addAll(medData);
          });

          medData.forEach((item) {
            if (item["started_on"] != null) {
              DateTime upcomingDateUtc = DateTime.parse(item["started_on"]).toUtc();
              DateTime upcomingDate = DateTime.utc(upcomingDateUtc.year, upcomingDateUtc.month);
              if (upcomingDate.compareTo(pagerDate) == 0) {
                setState(() {
                  medicationCardDataGroup.add(item);
//                  medicationCardDataGroup.addAll(longLiveList);
//                  apiPaginateSize = medicationCardDataGroup.length;
                });
              }

//              if (upcomingDate.month < pagerDate.month) {
              if (upcomingDate.compareTo(pagerDate) < 0) {
                setState(() {
                  prevMonthMedicationList.add(item);
//                  apiPaginateSize = prevMonthMedicationList.length;
                });
              }

              if (upcomingDate.compareTo(pagerDate) > 0) {
                setState(() {
                  nextMonthMedicationList.add(item);
//                  nextMonthMedicationList.addAll(longLiveList);
//                  apiPaginateSize = nextMonthMedicationList.length;
                });
              }
            }
          });
        }
        setState(() {
          apiPaginateSize = longLiveList.length;
        });
      } else if (response.statusCode == 500 &&
          medData["errorType"] == 'ExpiredJwtException') {
        setState(() {
          isLoading = true;
        });
        getRefreshToken();
      } else {
        print("error");
      }
    });

    setState(() {
      isLoading = false;
    });

    print("api paginate step: $apiPaginateSize");
    return null;
  }

  getRefreshToken() {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netUtil = new NetworkUtil();

    _netUtil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      if (apiPaginateSize != null) {
        fetchMedicationData(apiPaginateSize);
      } else {
        fetchMedicationData(apiPaginateSize);
      }
    });
  }

  Widget _buildCounterButton() {
    return new RaisedButton(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text('Please wait..',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: kinderM8Theme.Colors.app_white)),
            SizedBox(width: 6.0),
            Container(
              width: 20.0,
              height: 20.0,
              child: CircularProgressIndicator(strokeWidth: 2.0, backgroundColor: Colors.white24,
                valueColor: new AlwaysStoppedAnimation(Colors.white),),
            ),
          ],
        ),
        color: kinderM8Theme.Colors.appcolour.withOpacity(0.85),
//        splashColor: Colors.red,
        elevation: 4.0,
        onPressed: (){});
  }

  _scrollListener() {
    if (prescribedScrollController.position.extentAfter <= 0 && isLoading == false) {
      fetchMedicationData(apiPaginateSize);
    }
  }

  Widget buildMedicationList(List data) {
    return data.length > 0
        ? ListView.builder(
          controller: prescribedScrollController,
          itemCount: data.length,
          itemBuilder: (context, index) {
            if (data.length > 3 && data.length == index+1) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  PrescribedMediData(widget.childData,
                      data[index], jwt, users,
                      medicationDataGroupByDay: data,
                      isPrevMonths: isPreviousButtonActive),
                  _buildCounterButton(),
                ],
              );
            } else if (data.length > index) {
              return PrescribedMediData(widget.childData,
                  data[index], jwt, users,
                  medicationDataGroupByDay: data,
                  isPrevMonths: isPreviousButtonActive);
            }
          },
        )
      : Container(
          height: MediaQuery.of(context).size.height / 1.5,
          padding: EdgeInsets.all(8.0),
          child: Text(
            "No medications are available in this day",
            textAlign: TextAlign.center,
          ),
        );
  }

  Widget buildDataChild() {
    if (isPreviousButtonActive && prevMonthMedicationList.length > 0) {
        return buildMedicationList(prevMonthMedicationList);
    } else if (isDateHolderActive && medicationCardDataGroup.length > 0) {
        return buildMedicationList(medicationCardDataGroup);
    } else {
      return Container(
        height: MediaQuery.of(context).size.height / 1.5,
        padding: EdgeInsets.all(8.0),
        child: Text(
          "No medications are available in this day",
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
//    PrescribedMediData(
//        widget.childData, this.medicationCardDataGroup[0], jwt, users, medicationDataGroupByDay: medicationCardDataGroup)
    return Scaffold(
      key: scaffoldKey,
      body: load
              ? progress
              : new RefreshIndicator(
          key: refreshIndicatorKey,
          onRefresh: handleRefresh,
          child: DatePickPager(
                  child: buildDataChild(),
                  updateMedicationDataByDay: updateMedicationDataBasedOnAction,
                  whenPrevBtPress: whenPreviousButtonPressed,
                  isDateHolderActive: isDateHolderActive,
                  isPrevActive: isPreviousButtonActive,
                  isPreviouslyPrescribed: isPreviouslyPrescribed,
                  dayAvailabilityMap: medicationAvailabilityMap,
                  toggleIsPrevActive: toggleIsPrevActive,
                  toggleIsDateHolderActive: toggleDateHolderActive,
                ) //Sending whole data may not be efficient but I cant reproduced this bcuz of the way have done everything
          ),
      floatingActionButton:
          MyFloatingMenuPage(childData: widget.childData, jwt: widget.jwt),
    );
  }

  Future<String> handleRefresh() async {
    setState(() {
      longLiveList = [];
      medicationCardDataGroup = [];
      load = true;
    });
    return await fetchMedicationData(0);
  }

  @override
  void onDisplayUserInfo(User user) {
    var appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];

      jwt = widget.jwt.toString();
      print("******$jwt");
      users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }
  }

  @override
  void onErrorUserInfo() {}

  @override
  void onLogoutUser() {}
}