import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:flutter/material.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/dailychart/dailychartdata.dart';
import 'package:carem8/pages/dailyjournal/dailyjournaldata.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/medication/create_prescribed.dart';
import 'package:carem8/pages/medication/nonprescribedlist.dart';
import 'package:carem8/pages/medication/prescribedlist.dart';
import 'package:carem8/pages/medication/prescribedmedicationData.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:flutter_calendar/flutter_calendar.dart';

//class Medications extends StatefulWidget {
//  final childData;
//  final jwt;
//  Medications(this.childData, this.jwt);
//  @override
//  MedicationsState createState() => MedicationsState();
//}
//
//class MedicationsState extends State<Medications>
//    with SingleTickerProviderStateMixin
//    implements HomePageContract {
//  var k, appuser, jwt, id, clientId;
//  List data;
//  var childId;
//  bool   isLoading = false;
//  bool load = true;
//  HomePagePresenter _presenter;
//  var refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
//  final scaffoldKey = new GlobalKey<ScaffoldState>();
//  var medidate;
//  var casualattendancedate;
//  List Medicationcollection;
//  List prescribed_medi_collection, nonprescribed_medi_collection;
//  var casualattendancedata;
//  bool enabled = false;
//  bool expanded = false;
//
//  TabController tabController;
//
//  MedicationsState() {
//    _presenter = new HomePagePresenter(this);
//    _presenter.getUserInfo();
//  }
//  var medicationData;
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    tabController = new TabController(length: 2, vsync: this, initialIndex: 0);
//  }
//
//  var progress = new ProgressBar(
////    backgroundColor: Theme.Colors.progressbackground ,
//    color: kinderm8Theme.Colors.appcolour,
//    containerColor: kinderm8Theme.Colors.appcolour,
//    borderRadius: 5.0,
//    text: 'Loading...',
//  );
//
////  // get Casualattendance
////  Future<String> fetchCasualattendanceData(client,date) async {
////    var timeStamp = new DateFormat("yyyy-MM-dd");
////    casualattendancedate = new DateTime(date.year, date.month, date.day);
////    String formatdailychartdate = timeStamp.format(casualattendancedate);
////    print("formatdailychartdate ${formatdailychartdate}");
////    ///data from GET method
////    print("data fetched");
////    childId = widget.childData["id"];
////    String _dailyJournalUrl =
////        'https://apicare.carem8.com/v4.4.0/getcasualattendancedates/$childId?clientid=$clientId&date=$formatdailychartdate';
////    var headers = {"x-authorization": jwt.toString()};
////
////    NetworkUtil _netutil = new NetworkUtil();
////    _netutil.get(_dailyJournalUrl, headers: headers).then((response) {
////      var casualattendancedata;
////      try {
////        casualattendancedata = json.decode(response.body);
////        print(casualattendancedata.length);
////        print('res get ${response.body}');
////        print('casualattendancedata UrlData $casualattendancedata');
////      } catch (e) {
////        print('That string was null!');
////      }
////
////      print('jwt### $jwt');
////      print(response.statusCode);
////      if (response.statusCode == 200) {
////        print(isLoading);
////        isLoading = false;
////        print(isLoading);
////        k = casualattendancedata.length;
////      } else if (response.statusCode == 500 &&
////      casualattendancedata["errorType"] == 'ExpiredJwtException') {
////        print("retrying...");
////        isLoading = false;
////        if(clientId !=null) {
////          getRefreshToken_Casualattendance(date);
////        }
////      } else {
////        fetchCasualattendanceData(clientId,date);
////      }
////    });
////    return null;
////  }
////  getRefreshToken_Casualattendance(date) {
////    print("refreshing Token..");
////    String _refreshTokenUrl =
////        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';
////
////    NetworkUtil _netutil = new NetworkUtil();
////
////    _netutil.get(_refreshTokenUrl).then((response) {
////      print('refresh get ${response.body}');
////      var refreshJwtToken;
////      try {
////        refreshJwtToken = json.decode(response.body);
////      }catch (e) {
////        print('That string was null!');
////      }
////      this.jwt = refreshJwtToken;
////      isLoading = false;
////      if ( date != null) {
////        fetchCasualattendanceData(clientId,date);
////      }
////    });
////  }
//
//  Future<String> fetchMedicationData(int s) async {
////    print('fetchMedicationData $childId');
////    print('fetchMedicationData $clientId');
//    var timeStamp = new DateFormat("yyyy-MM-dd");
////    medidate = new DateTime(date.year, date.month, date.day);
////    String formatdailychartdate = timeStamp.format(medidate);
////    print("formatdailychartdate ${formatdailychartdate}");
//    ///data from GET method
//    print("data fetched");
//    childId = widget.childData["id"];
//    String _MedicationUrl =
//        'https://apicare.carem8.com/v2.1.1/medication/$childId?step=$s&clientid=$clientId';
//    var headers = {"x-authorization": jwt.toString()};
////    print(_MedicationUrl);
//    NetworkUtil _netutil = new NetworkUtil();
//    _netutil.get(_MedicationUrl, headers: headers).then((response) {
//      var medicationData;
//      try {
////        print('res get ${response.body}');
//        medicationData = json.decode(response.body);
//        print(medicationData.length);
//        print('medication UrlData $medicationData');
////        print('jwt### $jwt');
////        print(response.statusCode);
//
//        if (response.statusCode == 200) {
//
//          if (s == 0) {
//            setState(() {
//              load = false;
//              this.Medicationcollection = medicationData;
//            });
//          } else {
//            setState(() {
//              load = false;
//              Medicationcollection.addAll(medicationData);
//            });
//          }
//          k = Medicationcollection.length;
//        } else if (response.statusCode == 500 &&
//            medicationData["errorType"] == 'ExpiredJwtException') {
////          print("retrying...");
//          getRefreshToken();
//        } else {
//          fetchMedicationData(0);
//        }
//      } catch (e) {
//        print('That string was null!');
//      }
//    });
//    return null;
//  }
//
//  getRefreshToken() {
////    print("refreshing Token..");
//    String _refreshTokenUrl =
//        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';
//
//    NetworkUtil _netutil = new NetworkUtil();
//
//    _netutil.get(_refreshTokenUrl).then((response) {
//      var refreshJwtToken;
//      try {
//        refreshJwtToken = json.decode(response.body);
//      } catch (e) {
//        print('That string was null!');
//      }
//      this.jwt = refreshJwtToken;
//
//      if (k != null) {
//        fetchMedicationData(k);
//      } else {
//        fetchMedicationData(0);
//      }
//    });
//  }
//
//  Widget _buildCounterButton() {
//    return new RaisedButton(
//        child: isLoading
//            ? new CupertinoActivityIndicator()
//            : const Text('Load more...',
//                style: TextStyle(
//                    fontWeight: FontWeight.bold,
//                    fontSize: 14.0,
//                    color: kinderm8Theme.Colors.app_white)),
//        color: kinderm8Theme.Colors.appcolour.withOpacity(0.75),
//        elevation: 4.0,
//        onPressed: _counterButtonPress());
//  }
//
//  Function _counterButtonPress() {
//    if (isLoading) {
//      return null;
//    } else {
//      return () {
//        setState(() {
//          isLoading = true;
//        });
//        fetchMedicationData(k);
//      };
//    }
//  }
//
//  @override
//  Widget build(BuildContext context) {
////    print('medi data $Medicationcollection');
////    Medicationcollection = medicationData;
//
//    print("inside build ${Medicationcollection}");
//
//    return new Scaffold(
//      key: scaffoldKey,
//      appBar: new AppBar(
//        title: new Text("Medications",
//            style: TextStyle(
//              color: Colors.white,
//            )),
//        backgroundColor: kinderm8Theme.Colors.appcolour,
//        centerTitle: true,
//        bottom: TabBar(
//          controller: tabController,
//          tabs: <Widget>[
//            new Tab(
////              text: "COMMENTS",
//              child: Text(
//                "Prescribed",
//                style: new TextStyle(
//                    color: kinderm8Theme.Colors.app_white, fontSize: 18.0),
//              ),
////              icon: Icon(Icons.comment),
//            ),
//            new Tab(
////              text: "LIKES",
//              child: Text(
//                "Non-Prescribed",
//                style: new TextStyle(
//                    color: kinderm8Theme.Colors.app_white, fontSize: 18.0),
//              ),
////              icon: Icon(Icons.favorite),
//            )
//          ],
//        ),
//        actions: <Widget>[
//          /*GestureDetector(
//              onTap: () {
//                Navigator.push(
//                  context,
//                  MaterialPageRoute(builder: (context) => CreatePrescribed(widget.childData)),
//                );
//              },
//              child: Padding(
//                padding: const EdgeInsets.only(right: 13.0),
//                child: Icon(
//                  Icons.note_add,
//                  size: 25.0,
//                  color: Colors.white,
//                ),
//              ),
//            ),*/
//        ],
//      ),
//      body: TabBarView(
//        controller: tabController,
//        children: <Widget>[
////          Prescribed(widget.childData, jwt),
////          NonPrescribed(widget.childData, jwt),
//        ],
//      ),
//
////        body: new RefreshIndicator(
////          key: refreshIndicatorKey,
////          onRefresh: handleRefresh,
////          child: new Center(
////            child: load
////                ? progress
//////                : new Text(this.Medicationcollection.toString())
////                : new ListView.builder(
////              itemCount: this.Medicationcollection != null ? (this.Medicationcollection.length + 1) : 0,
////              itemBuilder: (context, index) {
//////                print('this.Medicationcollection[i]${this.Medicationcollection[index]}');
////                if (index == k) {
////                  return _buildCounterButton();
////                } else {
////                  final singlemedicationData = this.Medicationcollection[index];
////                  return PrescribedMediData(singlemedicationData, jwt);
////                }
////              },
////            ),
////          ),
////        )
//    );
//  }
//
//  Future<Null> handleRefresh() async {
//    await Future.delayed(Duration(milliseconds: 1000));
//    setState(() {
//      load = true;
//      fetchMedicationData(0);
//    });
//    return null;
//  }
//
//  @override
//  void onDisplayUserInfo(User user) {
//    appuser = user.center;
//    try {
//      final parsed = json.decode(appuser);
//      var appusers = parsed[0];
//      jwt = widget.jwt.toString();
//      var users = appusers["user"];
//      clientId = users["client_id"];
//      id = users["id"];
//
//      fetchMedicationData(0);
//    } catch (e) {
//      print("That string didn't look like Json.");
//    }
//
//    // TODO: implement onDisplayUserInfo
//  }
//
//  @override
//  void onErrorUserInfo() {
//    // TODO: implement onErrorUserInfo
//  }
//
//  @override
//  void onLogoutUser() {
//    // TODO: implement onLogoutUser
//  }
//
//  @override
//  void onUpdateJwt() {
//    // TODO: implement onUpdateJwt
//  }
//}


class Medications extends StatefulWidget {
  final childData;
  final jwt;
  final int i;
  Medications(this.childData, this.jwt,this.i);
  @override
  MedicationsState createState() => MedicationsState();
}

class MedicationsState extends State<Medications>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  @override
  void initState() {
    tabController =
    new TabController(length: 2, vsync: this, initialIndex:widget.i);
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          child: new AppBar(
            title: new Text(labelsConfig["medicationLabel"],
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                )),
            elevation: 0.0,
            backgroundColor: kinderm8Theme.Colors.appdarkcolour,
            centerTitle: true,
            bottom: TabBar(
              controller: tabController,
              tabs: <Widget>[
                new Tab(
                  child: Text(
                    labelsConfig["prescribedMedicationLabel"].split(" ")[0],
                    style: new TextStyle(
                        color: kinderm8Theme.Colors.app_white, fontSize: 16.0),
                  ),
                ),
                new Tab(
                  child: Text(
                    labelsConfig["nonPrescribedMedicationLabel"].split(" ")[0],
                    style: new TextStyle(
                        color: kinderm8Theme.Colors.app_white, fontSize: 16.0),
                  ),
                )
              ],
            ),
          ),
          preferredSize: Size(double.infinity, 90.0)
      ),
      body: TabBarView(
        controller: tabController,
        children: <Widget>[
          Prescribed(widget.childData, widget.jwt),
          NonPrescribed(widget.childData, widget.jwt),
        ],
      ),
    );
  }
    }