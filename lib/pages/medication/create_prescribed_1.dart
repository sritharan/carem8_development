import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:async/async.dart';
import 'package:groovin_widgets/groovin_expansion_tile.dart';
import 'package:groovin_widgets/modal_drawer_handle.dart';
import 'package:groovin_widgets/outline_dropdown_button.dart';
import 'package:intl/intl.dart';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:carem8/pages/commondrawer.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/medication/medicationlist.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:flutter/services.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:datetime_picker_formfield/time_picker_formfield.dart';

class CreatePrescribed extends StatefulWidget {
  final selectedChildData;

  CreatePrescribed(this.selectedChildData);

  @override
  CreatePrescribedState createState() => CreatePrescribedState(selectedChildData);
}

class CreatePrescribedState extends State<CreatePrescribed>
    implements HomePageContract {
  final selectedChildData;
  var appuser, jwt, userId, clientId;
  List<Widget> receiver = [];
  List<String> receiverId = [];
  final formKey = new GlobalKey<FormState>();
  bool errorMessage = false;
  bool errorSubject = false;
  var awsFolder;
  String imagePath;
  bool uploading = false;

  var value;
  bool isExpanded = false;

  DateTime _date;
  TextEditingController startTimeController;
  TextEditingController finishTimeController;
  TextEditingController medidateInputController;
  TextEditingController dateInputController_lastadministrated;
  TextEditingController dateInputController_priscribed;
  TextEditingController dateInputController_expiry;
  bool isButtonEnabled = false;
  bool enabled = false; // practitioner/chemist

  final dateFormat = DateFormat("EEEE, MMMM d, yyyy 'at' h:mma");
  final timeFormat = DateFormat("h:mm a");

  var _selection = new List<String>();
  var _controllers;

  List<TextEditingController> listTimeController;

  final TextEditingController _textController_medication_name = new TextEditingController();
  final TextEditingController _textController_medic_original_pack = new TextEditingController();
  final TextEditingController _textController_medical = new TextEditingController();
  final TextEditingController _textController_reason = new TextEditingController();
  final TextEditingController _textController_dosage = new TextEditingController();
  final TextEditingController _textController_instructions = new TextEditingController();
  final TextEditingController _textController_manner = new TextEditingController();
  final TextEditingController _textController_storage = new TextEditingController();
  final TextEditingController _textController_methodtobeadministered = new TextEditingController();


  List<TextField> medicationtimes ;
  TimeOfDay _startTime;
  TimeOfDay _finishTime;

//  String _times;
  int _times;
  // attachment
  List<File> fileList = new List();
  Future<File> _imageFile;
  var timesEditingControllers = <TextEditingController>[];
//  var textFields = <TextField>[];
  var textFields;
  HomePagePresenter presenter;

  CreatePrescribedState(this.selectedChildData) {
    presenter = new HomePagePresenter(this);
    presenter.getUserInfo();
  }
  submitparentmedicationsData() {
    FocusScope.of(context).requestFocus(new FocusNode());

    final submitMessageUrl =
        "https://apicare.carem8.com/v2.1.1/medication/create?clientid=$clientId";
    var body = {
      "child_id": selectedChildData['id'],
      "medication_name": _textController_medication_name.value,
      "medic_original_pack": _textController_medic_original_pack.value == true ? 1 :0,
      "medical": _textController_medical.value,
      "date_prescribed": dateInputController_priscribed.value,
      "expiry_date": dateInputController_expiry.value,
      "reason": _textController_reason.value,
      "dosage": _textController_dosage.value,
      "parent_notified": 0,
      "notified_parent_id": userId ,
      "request_status": 0,
      "instructions": _textController_instructions.value,
      "manner": _textController_manner.value,
      "times": _times,
      "main_date": medidateInputController.value,
      "ending_on": medidateInputController.value,
      "storage": _textController_storage.value,
      "last_administered_datetime": dateInputController_lastadministrated.value,
      "medication_time": ["2018-09-27 18:39:46","2018-09-27 22:39:46"]
    };
    var headers = {"x-authorization": jwt.toString()};
    print(body);

    NetworkUtil _netUtil = new NetworkUtil();
    _netUtil
        .post(submitMessageUrl,
        body: body, headers: headers, encoding: jwt.toString())
        .then((res) {
      print("resss  $res");
      if (res == 1) {
        String _refreshTokenUrl =
            'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';
        NetworkUtil _netutil = new NetworkUtil();
        _netutil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          var refreshJwtToken = json.decode(response.body);
          this.jwt = refreshJwtToken;
          submitparentmedicationsData();
        });
      } else if (res == '') {
        print("may be 200");
        submitparentmedicationsData();
//        Navigator.pushReplacement(context,
//            MaterialPageRoute(builder: (context) => Medications()));
      } else {
        print("error");
      }
    });
  }

  void _setButtonState() {
    setState(() {
      if (_date != null && _startTime != null) {
        print("setButtonState: Button enabled");
        isButtonEnabled = true;
      } else {
        print("setButtonState: Button diabled");
        isButtonEnabled = false;
      }
    });
  }

  Future<Null> _showStartTimeDialog() async {
    final TimeOfDay picked =
    await showTimePicker(context: context, initialTime: TimeOfDay.now());

    if (picked != null) {
      setState(() {
        _startTime = picked;
        startTimeController =
        new TextEditingController(text:
        "${picked.hour}:${picked.minute}");
      });
      _setButtonState();
    }
  }

  Future<Null> _showDatePicker() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(DateTime.now()
            .year - 1, 1),
        lastDate: DateTime(DateTime
            .now()
            .year, 12));

    if (picked != null) {
      setState(() {
        _date = picked;
        medidateInputController =
        new TextEditingController(text:
        "${picked.day}/${picked.month}/${picked.year}");
      });
    }
  }

  Future<Null> _showFinishTimeDialog() async {
    final TimeOfDay picked =
    await showTimePicker(context: context, initialTime: TimeOfDay.now());

    if (picked != null) {
      setState(() {
        _finishTime = picked;
        finishTimeController =
        new TextEditingController(text:
        "${picked.hour}:${picked.minute}");
        calculateDuration(
            date: DateTime.now(),
            startTime: _startTime,
            finishTime: _finishTime);
      });
    }
  }

  TimeOfDay calculateDuration(
      {DateTime date, TimeOfDay startTime, TimeOfDay finishTime}) {
    DateTime s = new DateTime(date.year, date.month, date.day, startTime.hour, startTime.minute);
    DateTime f = new DateTime(date.year, date.month, date.day, finishTime.hour, finishTime.minute);
    int milisecs = f.millisecond - s.millisecond;
    print("$milisecs");
    DateTime total = new DateTime.fromMillisecondsSinceEpoch(milisecs);
    print("${total.hour}:${total.minute}");
  }


  Future<Null> _showTimeDialog(selectedtime,x) async {
    print(x);
    final TimeOfDay picked =
    await showTimePicker(context: context, initialTime: TimeOfDay.now());

    if (picked != null) {
      setState(() {
        _startTime = picked;
        listTimeController.add(new TextEditingController(text:
        "${picked.hour}:${picked.minute}")); ;
        startTimeController =
        new TextEditingController(text:
        "${picked.hour}:${picked.minute}");
      });
      _setButtonState();
    }
  }

  List<Widget> widgets = [];

  Widget generatetimes(times){
    print('Going to generate time');
    print(times);
    var selectedtime = int.parse(times);
    _controllers = new List.generate(selectedtime, (i) => new TextEditingController());
//    _selection.addAll(_items);
    var textFields = new List.generate(selectedtime, (i) => i)
        .map(
          (i) => new Column(
        children: [
//          new DropdownButton<String>(
//            value: _selection[i],
//            items: _items.map((String item) {
//              return DropdownMenuItem<String>(
//                value: item,
//                child: new Text(item),
//              );
//            }).toList(),
//            onChanged: (s) {
//              setState(() {
//                _selection[i] = s;
//              });
//            },
//          ),
          new TextField(
            autocorrect: false,
            controller: _controllers[i],
            textAlign: TextAlign.center,
            keyboardType: TextInputType.number,
            decoration: new InputDecoration(
                hintText: "credit ${i + 1}", hintStyle: new TextStyle(color: Colors.deepOrangeAccent[100])),
          )
        ],
      ),
    ).toList(growable: true);
    // For loop start

//    for (int index = 0; index < selectedtime; index++) {
//     print(times);
//     widgets.add(
//       Container(
////                                    padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 8.0),
//           child: GestureDetector(  //Thid widgets wraps every other widget
//             onTap: () {
//               print("onTap start");
//               _showDatePicker();
//             },
//             child: new Row(
//               children: <Widget>[
//                 Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Icon(Icons.event),
//                 ),
//                 Container(
//                   child: new Flexible(
//                     fit: FlexFit.tight,
//                     child: Container(
//                    decoration: BoxDecoration(
//                        border: Border.all(
//                          color: Theme.of(context).disabledColor,
//                          width: 2.0,
//                          style: BorderStyle.solid,
//                        ),
//                        borderRadius: BorderRadius.circular(2.0)),
//                       child: Padding(
//                           padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//                           child: new TextField(
//                               enabled: false,
//                               decoration: InputDecoration(
//                                 hintText: "Time${index}",
//                                 contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
////                                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))
//                               ),
//                               maxLines: 1,
//                               controller: dateInputController)
//                       ),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           )),
//     );
//   } // for loop end
  }

  // Sample Date field //
  /* new Row(children: <Widget>[
  new Expanded(
  child: new TextFormField(
  decoration: new InputDecoration(
  icon: const Icon(Icons.calendar_today),
  hintText: 'Enter your date of birth',
  labelText: 'Dob',
  ),
  controller: _controller,
  keyboardType: TextInputType.datetime,
  )),
  new IconButton(
  icon: new Icon(Icons.more_horiz),
  tooltip: 'Choose date',
  onPressed: (() {
  _chooseDate(context, _controller.text);
  }),
  )
  ]),
  */
  // Sample Date field end

  final TextEditingController _controller = new TextEditingController();
  DateTime convertToDate(String input) {
    try
    {
      var d = new DateFormat.yMd().parseStrict(input);
      return d;
    } catch (e) {
      return null;
    }
  }
  Future _chooseDate(BuildContext context, String initialDateString) async {
    var now = new DateTime.now();
    var initialDate = convertToDate(initialDateString) ?? now;
    initialDate = (initialDate.year >= 1900 && initialDate.isBefore(now) ? initialDate : now);

    var result = await showDatePicker(
        context: context,
        initialDate: initialDate,
        firstDate: new DateTime(1900),
        lastDate: new DateTime.now());

    if (result == null) return;

    setState(() {
      _controller.text = new DateFormat.yMd().format(result);
    });
  }

  // Textfield
  createTexttextfields (int d){
    var textEditingControllers = <TextEditingController>[];

    var textFields = <TextField>[];
    var list = new List<int>.generate(d, (i) =>i + 1 );
    print(list);

    list.forEach((i) {
      var textEditingController = new TextEditingController(text: "test $i");
      textEditingControllers.add(textEditingController);
      return textFields.add(new TextField(controller: textEditingController));
    });
    return textFields;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    createTextFields_Widget(1);
  }

  List<Widget> listwidget = new List<Widget>();
  Map<String, int> quantities = {};
  // Map times
  void takeNumber(String text, String itemId) {
    try {
      int number = int.parse(text);
      quantities[itemId] = null;
      quantities[itemId] = number;
      print(quantities);
    } on FormatException {}
  }


  List<TimePickerFormField> textt = <TimePickerFormField>[];
  var showtime_section = false;
  // Textfield_with Time
  createTextFields_Widget(d) {
    var textEditingControllers = <TextEditingController>[];
    var textFields1 = <TimePickerFormField>[];
    if(d >= 1){
      showtime_section = true;
    }
    var list = new List<int>.generate(d, (i) => i + 1);
    print(list);
//    List<int>
    final currentTime = TimeOfDay.now();
    DateTime current_date = DateTime.now();
    String formattedDate = DateFormat('kk:mm:ss').format(current_date);

    list.forEach((i) {
      var textEditingController = new TextEditingController();
      textEditingControllers.add(textEditingController);

      List<TimePickerFormField> textt = <TimePickerFormField>[];

      return textFields1.add(new TimePickerFormField(
        onChanged: (t){
          takeNumber(i.toString(), t.format(context));
          print('onChanged${t.format(context)}');
          print('dateInputController${medidateInputController.value.text}');
//          var d = dateInputController.value.text;
//          DateTime now = DateTime.parse(d);
//          var parsedDate = DateTime.parse(d);
//          var date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);
//          var nowx = TimeOfDay.fromDateTime(DateTime.parse(dateInputController.value.text)); // 4:30pm
//          print('now$now'); //
//          print('d>>>>>$date'); //
//          String dateSlug ="${now.year.toString()}-${now.month.toString().padLeft(2,'0')}-${now.day.toString().padLeft(2,'0')}";
//          print(dateSlug);


        },
        format: timeFormat,
        controller: textEditingController,
        initialValue: currentTime,
        style: new TextStyle(
            color: kinderm8Theme.Colors.appcolour, fontSize: 20.0),
      ));
    });
    print(textFields1);
    textFields = textFields1;
    return textFields1;
  }

  var selectedtime;
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    var selectedtime = 1;
    _controllers = new List.generate(selectedtime, (i) => new TextEditingController());
//    _selection.addAll(_items);

//    var textFields_ui = new List.generate(selectedtime, (i) => i)
//        .map(
//          (i) => new Stack(
//        children: [
//          GestureDetector(  //Thid widgets wraps every other widget
//            onTap: () {
//              print("onTap start");
//              var x = _controllers;
//              _showTimeDialog(selectedtime,x);
//            },
//            child: new Row(
//              children: <Widget>[
//                new Text('Time ${i + 1}:'),
//                Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Icon(Icons.access_time),
//                ),
//                Container(
//                  child: new Flexible(
//                    fit: FlexFit.tight,
//                    child: Container(
//                      color: kinderm8Theme.Colors.appcolour,
////                    decoration: BoxDecoration(
////                        border: Border.all(
////                          color: Theme.of(context).disabledColor,
////                          width: 2.0,
////                          style: BorderStyle.solid,
////                        ),
//////                        borderRadius: BorderRadius.circular(2.0)
////                    ),
//                      child: Padding(
//                        padding: const EdgeInsets.all(0.0),
//                        child: new TextField(
//                          autocorrect: false,
//                          enabled: false,
//                          controller: _controllers[i],
//                          textAlign: TextAlign.center,
//                          keyboardType: TextInputType.datetime,
//                          decoration: new InputDecoration(
//                              hintText: _startTime != null
//                                  ? "${_startTime.format(context)}"
//                                  : "Time ${_startTime.format(context)}", hintStyle: new TextStyle(color: kinderm8Theme.Colors.app_white[100])),
//                        ),
//                      ),
//                    ),
//                  ),
//                ),
//              ],
//            ),
//          ),
////          new TextField(
////            autocorrect: false,
////            controller: _controllers[i],
////            textAlign: TextAlign.center,
////            keyboardType: TextInputType.datetime,
////            decoration: new InputDecoration(
////                hintText: "Time ${i + 1}", hintStyle: new TextStyle(color: Colors.deepOrangeAccent[100])),
////          )
//        ],
//      ),
//    ).toList(growable: false);

    return Scaffold(
      appBar: AppBar(
        title: Text("Create Prescribed Medication"),
        centerTitle: true,
        elevation: 0.0,
        actions: <Widget>[
//          IconButton(
//            icon: Icon(Icons.more_vert),
//            onPressed: () {
//              showModalBottomSheet(
//                context: context,
//                builder: (builder) {
//                  return Container(
//                    height: 250.0,
//                    child: Column(
//                      children: <Widget>[
//                        Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child: ModalDrawerHandle(
//                            handleColor: Colors.indigoAccent,
//                          ),
//                        ),
//                      ],
//                    ),
//                  );
//                },
//              );
//            },
//          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        padding: const EdgeInsets.only(top: 10.0,bottom: 10.0),
        child: Center(
          child: Column(
            children: <Widget>[
              // First section
              Padding(
                padding: const EdgeInsets.only(
                  left: 16.0,
                  right: 16.0,
                ),
                child: Material(
                  elevation: 2.0,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  child: GroovinExpansionTile(
                    defaultTrailingIconColor: kinderm8Theme.Colors.appcolour,
//                  leading: CircleAvatar(
//                    backgroundColor: Colors.indigoAccent,
//                    child: Icon(
//                      Icons.person,
//                      color: Colors.white,
//                    ),
//                  ),
                    title: Text("Medication Details", style: TextStyle(color: Colors.black),textAlign: TextAlign.center,),
//                  subtitle: Text("123-456-7890"),
                    onExpansionChanged: (value) {
                      setState(() {
                        isExpanded = value;
                      });
                    },
                    inkwellRadius: !isExpanded
                        ? BorderRadius.all(Radius.circular(8.0))
                        : BorderRadius.only(
                      topRight: Radius.circular(8.0),
                      topLeft: Radius.circular(8.0),
                    ),
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(5.0),
                          bottomRight: Radius.circular(5.0),
                        ),
                        child: Column(
                          children: <Widget>[
                            new Container(
                                padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text("Name of the Medication"),
                                        new Icon(Icons.star,size: 8.0,color: kinderm8Theme.Colors.secondsubtitle,)
                                      ],
                                    ),
                                    TextField(maxLines: 2,
                                        controller: _textController_medication_name,
                                        decoration: InputDecoration(
//                                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(2.0)),
                                          contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//                                          hintText: "Name of the Medication"
                                        )
                                    )
                                  ],
                                )
                            ),
                            new Container(
                                padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text("Reason for Medication"),
                                      ],
                                    ),
                                    TextField(maxLines: 2,
                                        controller: _textController_reason,
                                        decoration: InputDecoration(
//                                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(2.0)),
                                          contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//                                          hintText: "Name of the Medication"
                                        ))
                                  ],
                                )
                            ),

                            new Container(
                                padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text("Medical practitioner/chemist :"),
                                        new Switch(
                                          onChanged: (bool val) {
                                            setState(() {
                                              enabled = val;
                                              print(enabled);
                                            });
                                          },
                                          activeColor: Colors.green,
                                          activeTrackColor: Colors.greenAccent[400],
                                          value: enabled,
                                        ),
                                      ],
                                    ),
                                  ],
                                )
                            ),

                            new Padding(
                              padding: const EdgeInsets.only(left: 4.0, right: 4.0,bottom: 5.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
//                                    padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 8.0),
                                      child: GestureDetector(  //Thid widgets wraps every other widget
                                        onTap: () {
                                          print("onTap start");
                                          _showDatePicker();
                                        },
                                        child: new Row(
                                          children: <Widget>[
                                            new Text('Date Priscribed:'),
                                            Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Icon(Icons.event),
                                            ),
                                            Container(
                                              child: new Flexible(
                                                fit: FlexFit.tight,
                                                child: Container(
//                                                  decoration: BoxDecoration(
//                                                      border: Border.all(
//                                                        color: Theme.of(context).disabledColor,
//                                                        width: 2.0,
//                                                        style: BorderStyle.solid,
//                                                      ),
//                                                      borderRadius: BorderRadius.circular(2.0)),
                                                  child: Padding(
                                                      padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                                      child: new TextField(
                                                          enabled: false,
                                                          decoration: InputDecoration(hintText: "Date",
                                                            contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//                                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))
                                                          ),
                                                          maxLines: 1,
                                                          controller: dateInputController_priscribed)
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ))
                                ],
                              ),
                            ),
                            new Padding(
                              padding: const EdgeInsets.only(left: 4.0, right: 4.0,bottom: 5.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
//                                    padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 8.0),
                                      child: GestureDetector(  //Thid widgets wraps every other widget
                                        onTap: () {
                                          print("onTap start");
                                          _showDatePicker();
                                        },
                                        child: new Row(
                                          children: <Widget>[
                                            new Text('Expiry Date :'),
                                            Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Icon(Icons.event),
                                            ),
                                            Container(
                                              child: new Flexible(
                                                fit: FlexFit.tight,
                                                child: Container(
//                                                  decoration: BoxDecoration(
//                                                      border: Border.all(
//                                                        color: Theme.of(context).disabledColor,
//                                                        width: 2.0,
//                                                        style: BorderStyle.solid,
//                                                      ),
//                                                      borderRadius: BorderRadius.circular(2.0)),
                                                  child: Padding(
                                                      padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                                      child: new TextField(
                                                          enabled: false,
                                                          decoration: InputDecoration(hintText: "Date",
                                                            contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//                                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))
                                                          ),
                                                          maxLines: 1,
                                                          controller: dateInputController_expiry)
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ))
                                ],
                              ),
                            ),

                            new Container(
                                padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text("Storage:"),
                                      ],
                                    ),
                                    TextField(maxLines: 1,
                                        controller: _textController_storage,
                                        decoration: InputDecoration(
//                                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(2.0)),
                                          contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//                                          hintText: "Name of the Medication"
                                        ))
                                  ],
                                )
                            ),

                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              // First section end

              // Second section
              Padding(
                padding: const EdgeInsets.only(
                  top:16.0,
                  left: 16.0,
                  right: 16.0,
                ),
                child: Material(
                  elevation: 2.0,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  child: GroovinExpansionTile(
                    defaultTrailingIconColor: kinderm8Theme.Colors.appcolour,
//                  leading: CircleAvatar(
//                    backgroundColor: Colors.indigoAccent,
//                    child: Icon(
//                      Icons.person,
//                      color: Colors.white,
//                    ),
//                  ),
                    title: Text("Last Administered Details", style: TextStyle(color: Colors.black),textAlign: TextAlign.center,),
                    onExpansionChanged: (value) {
                      setState(() {
                        isExpanded = value;
                      });
                    },
                    inkwellRadius: !isExpanded
                        ? BorderRadius.all(Radius.circular(8.0))
                        : BorderRadius.only(
                      topRight: Radius.circular(8.0),
                      topLeft: Radius.circular(8.0),
                    ),
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(5.0),
                          bottomRight: Radius.circular(5.0),
                        ),
                        child: Column(
                          children: <Widget>[
                            new Padding(
                              padding: const EdgeInsets.only(left: 4.0, right: 4.0,bottom: 5.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
//                                    padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 8.0),
                                      child: GestureDetector(  //Thid widgets wraps every other widget
                                        onTap: () {
                                          print("onTap start");
                                          _showDatePicker();
                                        },
                                        child: new Row(
                                          children: <Widget>[
                                            new Text('Date:'),
                                            Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Icon(Icons.event),
                                            ),
                                            Container(
                                              child: new Flexible(
                                                fit: FlexFit.tight,
                                                child: Container(
//                                                  decoration: BoxDecoration(
//                                                      border: Border.all(
//                                                        color: Theme.of(context).disabledColor,
//                                                        width: 2.0,
//                                                        style: BorderStyle.solid,
//                                                      ),
//                                                      borderRadius: BorderRadius.circular(2.0)),
                                                  child: Padding(
                                                      padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                                      child: new TextField(
                                                          enabled: false,
                                                          decoration: InputDecoration(hintText: "Date",
                                                            contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//                                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))
                                                          ),
                                                          maxLines: 1,
                                                          controller: dateInputController_lastadministrated)
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ))
                                ],
                              ),
                            ),
                            new Padding(
                              padding: const EdgeInsets.only(left: 4.0, right: 4.0,bottom: 5.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
//                                    padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 8.0),
                                      child: GestureDetector(  //Thid widgets wraps every other widget
                                        onTap: () {
                                          print("onTap start");
                                          _showStartTimeDialog();
                                        },
                                        child: new Row(
                                          children: <Widget>[
                                            new Text('Time:'),
                                            Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Icon(Icons.access_time),
                                            ),
                                            Container(
                                              child: new Flexible(
                                                fit: FlexFit.tight,
                                                child: Container(
//                                                  decoration: BoxDecoration(
//                                                      border: Border.all(
//                                                        color: Theme.of(context).disabledColor,
//                                                        width: 2.0,
//                                                        style: BorderStyle.solid,
//                                                      ),
//                                                      borderRadius: BorderRadius.circular(2.0)),
                                                  child: Padding(
                                                    padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                                    child: new Text(
                                                      _startTime != null
//                                                          ? "${_startTime.hour}:${_startTime.minute} "
                                                          ? "${_startTime.format(context)}"
                                                          : "Time",
                                                      style: TextStyle(),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ))
                                ],
                              ),
                            ),
                            new Container(
                                padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text("Dosage of medication"),
                                      ],
                                    ),
                                    TextField(maxLines: 2,
                                        controller: _textController_dosage,
                                        decoration: InputDecoration(
//                                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(2.0)),
                                          contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//                                          hintText: "Name of the Medication"
                                        ))
                                  ],
                                )
                            ),
                            new Container(
                                padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text("Method to be administered"),
                                      ],
                                    ),
                                    TextField(maxLines: 2,
                                        controller: _textController_methodtobeadministered,
                                        decoration: InputDecoration(
//                                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(2.0)),
                                          contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//                                          hintText: "Name of the Medication"
                                        ))
                                  ],
                                )
                            ),
                            Container(
                                padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0),
                                child: new Text("Medication Times", style: TextStyle(color: Colors.black),textAlign: TextAlign.center,)
                            ),

                            new Padding(
                              padding: const EdgeInsets.only(left: 4.0, right: 4.0,bottom: 5.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
//                                    padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 8.0),
                                      child: GestureDetector(  //Thid widgets wraps every other widget
                                        onTap: () {
                                          print("onTap start");
                                          _showDatePicker();
                                        },
                                        child: new Row(
                                          children: <Widget>[
                                            new Text('Date:'),
                                            Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Icon(Icons.event),
                                            ),
                                            Container(
                                              child: new Flexible(
                                                fit: FlexFit.tight,
                                                child: Container(
//                                                  decoration: BoxDecoration(
//                                                      border: Border.all(
//                                                        color: Theme.of(context).disabledColor,
//                                                        width: 2.0,
//                                                        style: BorderStyle.solid,
//                                                      ),
//                                                      borderRadius: BorderRadius.circular(2.0)),
                                                  child: Padding(
                                                      padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                                                      child: new TextField(
                                                          enabled: false,
                                                          decoration: InputDecoration(hintText: "Date",
                                                            contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//                                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))
                                                          ),
                                                          maxLines: 1,
                                                          controller: medidateInputController)
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ))
                                ],
                              ),
                            ),
                            new Padding(
                              padding: const EdgeInsets.only(left: 4.0, right: 4.0,bottom: 5.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[

                                  Container(
//                                    padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 8.0),
                                      child: GestureDetector(  //Thid widgets wraps every other widget
                                        onTap: () {
                                          print("onTap start");
                                          _showDatePicker();
                                        },
                                        child: new Row(
                                          children: <Widget>[
                                            new Text('Number of times:'),
//                                            Padding(
//                                              padding: const EdgeInsets.all(8.0),
//                                              child: Icon(Icons.event),
//                                            ),
                                            Container(
                                              padding: EdgeInsets.only(left: 5.0, bottom: 5.0),
                                              child: new Column(
                                                children: <Widget>[
                                                  new DropdownButton<int>(
//                                                  new DropdownButton<String>(
//                                                    items: <String>['1', '2','3','4','5','6'].map((String value) {
//                                                      return new DropdownMenuItem<String>(
//                                                        value: value,
//                                                        child: new Text(value),
//                                                      );
//                                                    }).toList(),
                                                    items: <int>[1, 2, 3, 4, 5, 6]
                                                        .map((int value) {
                                                      return new DropdownMenuItem<
                                                          int>(
                                                        value: value,
                                                        child: new Text("$value"),
                                                      );
                                                    }).toList(),
                                                    hint: new Text("Select time"),
                                                    value:_times,
                                                    onChanged: (newVal) {
                                                      setState(() {
                                                        _times = newVal;
                                                        // Generate time widget
//                                                        generatetimes(_times);
                                                        setState(() {
                                                          selectedtime = _times;
                                                          print(selectedtime);
                                                          createTextFields_Widget(
                                                              selectedtime);
                                                        });
                                                      });
                                                    },

                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      )),

//                                  textFields_ui.length != 0
//                                      ? Container(
//                                    width: screenSize.width / 2,
//                                    child: new Column(
//                                      children: textFields_ui,
//                                    ),
//                                  )
//                                      : Container(),

                                  selectedtime >= 1 ?
                                  textFields.length != 0
                                        ? Container(
                                      width: screenSize.width / 2,
                                      child: new Column(
                                        children: textFields,
                                      ),
                                    ) : Container()
                                      : Container(),

//                                  new Column(
//                                    children: createTexttextfields(6),
//                                  ),
//                                  new Container(
//                                    height: 240.0,
//                                    decoration: new BoxDecoration(border: new Border.all(color: Colors.transparent, width: 5.0)),
//                                    child: new ListView(
//                                      children: textFields,
//                                    ),
//                                  ),

                                ],
                              ),
                            ),
//                            new Column(
//                              children: textFields,
//                            ),
                            new Container(
                                padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text("Instructions:"),
                                      ],
                                    ),
                                    TextField(maxLines: 1,
                                        controller: _textController_instructions,
                                        decoration: InputDecoration(
//                                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(2.0)),
                                          contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
//                                          hintText: "Name of the Medication"
                                        ))
                                  ],
                                )
                            ),

                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              // Second section end
              new Container(
                  padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                  child: new Text('I request that the above medication to be given in accordance with the instructions given.')
              ),

              new Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
                    child: Material(
                      borderRadius: BorderRadius.circular(10.0),
//                      shadowColor: isButtonEnabled
//                          ? Colors.orangeAccent.shade100
//                          : Colors.grey.shade100,
//                      elevation: 2.0,
                      child: MaterialButton(
//                        minWidth: 200.0,
//                        height: 50.0,
                        onPressed: () {
                          if (isButtonEnabled) {
                            print("Button Clicked");
                          }
                        },
                        color: isButtonEnabled ? kinderm8Theme.Colors.appcolour : Colors.grey,
                        child: Text("Submit", style: TextStyle(fontSize:20.0,color: Colors.white)),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onDisplayUserInfo(User user) {
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

}