import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:groovin_widgets/groovin_expansion_tile.dart';
import 'package:intl/intl.dart';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/medication/medicationlist.dart';
import 'package:carem8/utils/network_util.dart';

class CreatePrescribed extends StatefulWidget {
  final selectedChildData;
  final jwt;

  CreatePrescribed(this.selectedChildData, this.jwt);

  @override
  CreatePrescribedState createState() =>
      CreatePrescribedState(selectedChildData);
}

class CreatePrescribedState extends State<CreatePrescribed>
    implements HomePageContract {
  HomePagePresenter _presenter;

  CreatePrescribedState(this.selectedChildData) {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  var globalConfig;
  var application_settings;
  final selectedChildData;
  var appuser, jwt, userId, clientId;
  List<Widget> receiver = [];
  List<String> receiverId = [];
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  bool submit = true;

  var value;
  bool isExpanded = false;

  DateTime _lastadministrated;
  DateTime d1, d2, d3, d4, d5, d6;
  var dtarray = {};

  TextEditingController startTimeController;
  TextEditingController medidateInputController;
  TextEditingController dateInputController_lastadministrated;
  TextEditingController dateInputController_priscribed;
  TextEditingController dateInputController_expiry;
  bool isButtonEnabled = false;
  bool medic_original_packenabled = false; // practitioner/chemist

  List<TextEditingController> listTimeController;

  final TextEditingController _textController_medication_name =
      new TextEditingController();
  final TextEditingController _textController_medic_original_pack =
      new TextEditingController();
  final TextEditingController _textController_medical =
      new TextEditingController();
  final TextEditingController _textController_reason =
      new TextEditingController();
  final TextEditingController _textController_dosage =
      new TextEditingController();
  final TextEditingController _textController_instructions =
      new TextEditingController();
  final TextEditingController _textController_manner =
      new TextEditingController();
  final TextEditingController _textController_storage =
      new TextEditingController();
  final TextEditingController _textController_methodtobeadministered =
      new TextEditingController();

  TextEditingController textEditingController1 = new TextEditingController();
  TextEditingController textEditingController2 = new TextEditingController();
  TextEditingController textEditingController3 = new TextEditingController();
  TextEditingController textEditingController4 = new TextEditingController();
  TextEditingController textEditingController5 = new TextEditingController();
  TextEditingController textEditingController6 = new TextEditingController();

  TimeOfDay _startTime;
  var timeStamp = new DateFormat("yyyy-MM-dd");
  int _times = 1;
  List<String> finaldatetimearray = [];
  bool errorTimes = false;
  String childattendance;

  // Set this two values from config
  var enable_sunday = true;
  var enable_saturday = true;

  // Showing widget condiation
  var isWidgethide = false;

  var myattandance = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    checkattendance();
    medidateInputController =
        new TextEditingController(text: timeStamp.format(_date).toString());
  }

  void checkattendance() {
    print(application_settings);
//    print(globalConfig);
    print(
        '----------------------------checkattendance------------------------------------------');
//    print(globalConfig);
    print(application_settings);

    print(selectedChildData['attendance']);
    childattendance = selectedChildData['attendance'];
    var attarray = childattendance.split(',');
    print(attarray);
    for (int x = 0; x < attarray.length; x++) {
//      DateTime listvalue = DateTime.parse(attarray[x]);
      var LowerCasevalue = attarray[x].toLowerCase();
      print(attarray[x].toLowerCase());
      switch (LowerCasevalue) {
        case "sunday":
          myattandance.add(0);
          break;
        case "monday":
          myattandance.add(1);
          break;
        case "tuesday":
          myattandance.add(2);
          break;
        case "wednesday":
          myattandance.add(3);
          break;
        case "thursday":
          myattandance.add(4);
          break;
        case "friday":
          myattandance.add(5);
          break;
        case "saturday":
          myattandance.add(6);
      }
    }

    print('########### myattandance ###########$myattandance');

    var timeStamp = new DateFormat("yyyy-MM-dd");
    var statusvalue = checkAttendanceDate(timeStamp.format(_date));
    setState(() {
      isWidgethide = statusvalue == true ? true : false;
    });
    print('isWidgethide>> $isWidgethide');
  }

  var casualattendancedata;

  Future<String> fetchCasualattendanceData() async {
    ///data from GET method
    print("data fetched");
    var date = new DateTime.now();
    var casualattendancedate = new DateTime(date.year, date.month, date.day);
    String formatDate = timeStamp.format(casualattendancedate);

    var childId = selectedChildData['id'];
    String _dailyJournalUrl =
        'https://apicare.carem8.com/v4.4.0/getcasualattendancedates/$childId?clientid=$clientId&date=$formatDate';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_dailyJournalUrl, headers: headers).then((response) {
      try {
        print('jwt### $jwt');
        print(response.statusCode);
        print('res get ${response.body}');
        casualattendancedata = json.decode(response.body);
        print(casualattendancedata.length);
        print('casualattendancedata UrlData $casualattendancedata');
      } catch (e) {
        print('That string was null!');
      }

      if (response.statusCode == 200) {
        checkattendance();
//        print(isLoading);
//        isLoading = false;
//        print(isLoading);
//        k = casualattendancedata.length;
      } else if (response.statusCode == 500 &&
          casualattendancedata["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken_Casualattendance();
      } else {
        print("error....");
//        fetchCasualattendanceData();
      }
    });
    return null;
  }

  getRefreshToken_Casualattendance() {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;
//      isLoading = false;
      fetchCasualattendanceData();
    });
  }

  Future _asyncConfirmDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Thank you for your request!",
              style: TextStyle(
                  color: kinderm8Theme.Colors.appcolour,
                  fontWeight: FontWeight.w300,
                  fontSize: 20.0)),
          content: new Text("Your request has been sumitted sucessfully.",
              style: TextStyle(
                  color: kinderm8Theme.Colors.appcolour,
                  fontWeight: FontWeight.w300,
                  fontSize: 14.0)),
          actions: <Widget>[
            new RaisedButton(
              child: const Text('OK'),
              color: Theme.of(context).backgroundColor,
              elevation: 4.0,
              splashColor: Colors.purple.shade100,
              onPressed: () {
//                Navigator.of(context).pop(ConfirmAction.ACCEPT);
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            Medications(selectedChildData, jwt, 0)));
                submit = true;
              },
            ),
//            FlatButton(
//              child: const Text('Ok'),
//              onPressed: () {
////                Navigator.of(context).pop(ConfirmAction.ACCEPT);
//                Navigator.pushReplacement(
//                    context,
//                    MaterialPageRoute(
//                        builder: (context) => Medications(selectedChildData, jwt, 0)));
//                submit = true;
//              },
//            )
          ],
        );
      },
    );
  }

  checkAttendanceDate(date) {
    var medi_status;
    print('myattandance$myattandance');
    var timeStamp = new DateFormat("yyyy-MM-dd");
    var todayattendance = timeStamp.parse(date);
    print('todayattendance $todayattendance');

    var attendancedate = new DateTime(
        todayattendance.year, todayattendance.month, todayattendance.day);
    String formatDate = timeStamp.format(attendancedate);
    print("formated date $formatDate");
    print(attendancedate.weekday);

    for (int s = 0; s < myattandance.length; s++) {
      var selected = myattandance[s];
      print(selected);
      print(attendancedate.weekday);
      if (attendancedate.weekday == selected &&
          todayattendance.difference(attendancedate).inDays == 0) {
        var difference = attendancedate.difference(todayattendance).inDays;
        print('difference>> $difference');
        if (difference == 0) {
          medi_status = true;
        }
        print(
            'attendancedate${attendancedate} - today${todayattendance} | attn-wek${attendancedate.weekday} today-week${selected} selected medi_status >> $medi_status');
      }
      print(
          '2222 attendancedate${attendancedate} - today${todayattendance} | attn-wek${attendancedate.weekday} today-week${selected} selected medi_status >> $medi_status');
      print("#####$casualattendancedata");
      if (casualattendancedata.length > 0) {
        for (int t = 0; t < casualattendancedata.length; t++) {
          var selectedday = casualattendancedata[t]['date'];
          print('checkAttendanceDate${casualattendancedata[t]['date']}');
          var loopingattendancedate = DateTime.parse(selectedday);
          print(loopingattendancedate.weekday);
          if (attendancedate.weekday == loopingattendancedate.weekday &&
              attendancedate.difference(loopingattendancedate).inDays == 0) {
            var difference =
                attendancedate.difference(loopingattendancedate).inDays;
            print('difference>> $difference');
            if (difference == 0) {
              medi_status = true;
            }
            print(
                'attendancedate${attendancedate} - loop${loopingattendancedate} |  attn-wek${attendancedate.weekday} loop-week${loopingattendancedate.weekday} loop medi_status >> $medi_status');
          }
        }
      }
    }
    print('checkAttendanceDate >>$medi_status');
    return medi_status;
  }

  void _setButtonState() {
    setState(() {
      if (_date != null && _startTime != null) {
        if (_times == 1 && time1 != null) {
          isButtonEnabled = true;
        } else if (_times == 2 && time1 != null && time2 != null) {
          isButtonEnabled = true;
        } else if (_times == 3 &&
            time1 != null &&
            time2 != null &&
            time3 != null) {
          isButtonEnabled = true;
        } else if (_times == 4 &&
            time1 != null &&
            time2 != null &&
            time3 != null &&
            time4 != null) {
          isButtonEnabled = true;
        } else if (_times == 5 &&
            time1 != null &&
            time2 != null &&
            time3 != null &&
            time4 != null &&
            time5 != null) {
          isButtonEnabled = true;
        } else if (_times == 6 &&
            time1 != null &&
            time2 != null &&
            time3 != null &&
            time4 != null &&
            time5 != null &&
            time6 != null) {
          isButtonEnabled = true;
        } else {
          isButtonEnabled = false;
        }

        /*if (time1 != null ||
            time2 != null ||
            time3 != null ||
            time4 != null ||
            time5 != null ||
            time6 != null) {
          print("testing3");

          print("setButtonState: Button enabled");
          isButtonEnabled = true;
        }else {
          print("setButtonState: Button diabled");
          isButtonEnabled = false;
        }*/
      }
    });
  }

  Future<Null> _showStartTimeDialog__lastadministrated() async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());

    if (picked != null) {
      setState(() {
        _startTime = picked;
        DateTime time = DateTime(0, 0, 0, _startTime.hour, _startTime.minute);
        print("TIME $time");
        String formattedDate = DateFormat('kk:mm a').format(time);
        startTimeController = new TextEditingController(text: formattedDate);
      });
    }
  }

  DateTime _date = DateTime.now();

  Future<Null> _showDatePicker() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date != null ? _date : DateTime.now(),
        firstDate: DateTime(DateTime.now().year - 1, 1),
        lastDate: DateTime(DateTime.now().year, 12));
    // Check casual
    var timeStamp = new DateFormat("yyyy-MM-dd");
//    var todayattendance = timeStamp.parse(timeStamp.format(picked).toString());
    var statusvalue = checkAttendanceDate(timeStamp.format(picked).toString());
    print('statusvalue>> $statusvalue');
    isWidgethide = statusvalue == true ? true : false;
    print('isWidgethide>> $isWidgethide');
    if (picked != null) {
      setState(() {
        _date = picked;
        medidateInputController = new TextEditingController(
            text: timeStamp.format(picked).toString());
      });
    }
  }

  DateTime lastAdministratedDate;

  Future<Null> _showDatePicker_lastadministrated() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: lastAdministratedDate != null
            ? lastAdministratedDate
            : DateTime.now(),
        firstDate: DateTime(DateTime.now().year - 1, 1),
        lastDate: DateTime(DateTime.now().year, 12));
    if (picked != null) {
      setState(() {
        lastAdministratedDate = picked;
        dateInputController_lastadministrated = new TextEditingController(
            text: timeStamp.format(picked).toString());
        print(
            "^***^^*^*^*^^*^**^*^*^^^*^*^*^*^*^*^*_lastadministrated $lastAdministratedDate");
      });
    }
  }

  var priscribedDate;

  Future<Null> _showDatePicker_priscribed() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(DateTime.now().year - 1, 1),
        lastDate: DateTime(DateTime.now().year, 12));

    if (picked != null) {
      setState(() {
        priscribedDate = timeStamp.format(picked);
        print(
            "^***^^*^*^*^^*^**^*^*^^^*^*^*^*^*^*^*priscribedDate $priscribedDate");
        dateInputController_priscribed =
            new TextEditingController(text: priscribedDate.toString());
      });
    }
  }

  var expireDate;

  Future<Null> _showDatePicker_expiry() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(DateTime.now().year - 1, 1),
        lastDate: DateTime(DateTime.now().year, 12));

    if (picked != null) {
      setState(() {
        expireDate = timeStamp.format(picked);
        print("^***^^*^*^*^^*^**^*^*^^^*^*^*^*^*^*^*expireDate $expireDate");
        dateInputController_expiry =
            new TextEditingController(text: expireDate.toString());
      });
    }
  }

  void CheckTimer(DateTime dt) {
    for (int x = 0; x < dtarray.length; x++) {
      DateTime listvalue = DateTime.parse(dtarray[x]);
      print(listvalue);
      dt.difference(listvalue).inMinutes;
      print(dt.difference(listvalue));
      if (dt.isAfter(listvalue) && dt.isBefore(listvalue)) {
        print(dt.isAfter(listvalue) && dt.isBefore(listvalue));
        // do something
      }
    }
  }

  /// Time clocks
  ///
  ///

  bool isSame1 = false,
      isSame2 = false,
      isSame3 = false,
      isSame4 = false,
      isSame5 = false,
      isSame6 = false;
  TimeOfDay time1, time2, time3, time4, time5, time6;

  Future<Null> _showTimePicker1() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time1) {
      setState(() {
        d1 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');
        String _toJson(DateTime date) => _dateFormatter.format(date);

        if (dtarray.containsValue(_dateFormatter.format(d1))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame1 = true;
          print(isSame1);
        } else {
          time1 = picked;
          dtarray[0] = _toJson(d1);
          print("dtarray ->$dtarray");
          isSame1 = false;
//Assign value
          String formattedDate = DateFormat('kk:mm a').format(d1);
          textEditingController1.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame1 = false;
      });
      print("else");
    }
  }

  Future<Null> _showTimePicker2() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time2) {
      setState(() {
        d2 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');

        String _toJson(DateTime date) => _dateFormatter.format(date);
        print("to json ->${_toJson(d2)}");

        if (dtarray.containsValue(_dateFormatter.format(d2))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame2 = true;
        } else {
          time2 = picked;
          dtarray[1] = _toJson(d2);
          print("dtarray ->$dtarray");
          isSame2 = false;
          String formattedDate = DateFormat('kk:mm a').format(d2);
          textEditingController2.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame2 = false;
      });
      print("else");
    }
  }

  Future<Null> _showTimePicker3() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time3) {
      setState(() {
        d3 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');
        String _toJson(DateTime date) => _dateFormatter.format(date);
        print("to json ->${_toJson(d3)}");

        if (dtarray.containsValue(_dateFormatter.format(d3))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame3 = true;
        } else {
          time3 = picked;
          dtarray[2] = _toJson(d3);
          print("dtarray ->$dtarray");
          isSame3 = false;
//Assign value
          String formattedDate = DateFormat('kk:mm a').format(d3);
          textEditingController3.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame3 = false;
      });
      print("else");
    }
  }

  Future<Null> _showTimePicker4() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time4) {
      setState(() {
        d4 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');
        String _toJson(DateTime date) => _dateFormatter.format(date);
        print("to json ->${_toJson(d4)}");

        if (dtarray.containsValue(_dateFormatter.format(d4))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame4 = true;
        } else {
          time4 = picked;
          dtarray[3] = _toJson(d4);
          print("dtarray ->$dtarray");
          isSame4 = false;

          //Assign value
          String formattedDate = DateFormat('kk:mm a').format(d4);
          textEditingController4.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame4 = false;
      });
      print("else");
    }
  }

  Future<Null> _showTimePicker5() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time5) {
      setState(() {
        d5 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');
        String _toJson(DateTime date) => _dateFormatter.format(date);
        print("to json ->${_toJson(d5)}");

        if (dtarray.containsValue(_dateFormatter.format(d5))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame5 = true;
        } else {
          time5 = picked;
          dtarray[4] = _toJson(d5);
          print("dtarray ->$dtarray");
          isSame5 = false;

          //Assign value
          String formattedDate = DateFormat('kk:mm a').format(d5);
          textEditingController5.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame5 = false;
      });
      print("else");
    }
  }

  Future<Null> _showTimePicker6() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time6) {
      setState(() {
        d6 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');
        String _toJson(DateTime date) => _dateFormatter.format(date);
        print("to json ->${_toJson(d6)}");
        if (dtarray.containsValue(_dateFormatter.format(d6))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame6 = true;
        } else {
          time6 = picked;
          dtarray[5] = _toJson(d6);
          print("dtarray ->$dtarray");
          isSame6 = false;
          //Assign value
          String formattedDate = DateFormat('kk:mm a').format(d6);
          textEditingController6.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame6 = false;
      });
      print("else");
    }
  }

  ///
  ///
  /// End of Clocks

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    _setButtonState();

    if (dtarray.length != _times) {
      setState(() {
        errorTimes = true;
      });
    } else {
      setState(() {
        errorTimes = false;
      });
    }

//
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(labelsConfig["createPrescribedMediLabel"]),
        elevation: 0.5,
      ),
      body: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Center(
          child: GestureDetector(
            onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
            child: Column(
              children: <Widget>[
                /// First section
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 18.0),
                  child: Material(
                    elevation: 2.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0))),
                    child: GroovinExpansionTile(
                      defaultTrailingIconColor: kinderm8Theme.Colors.appcolour,
                      initiallyExpanded: true,
                      title: Text(
                        "Medication Details",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                      ),
                      onExpansionChanged: (value) {
                        setState(() {
                          isExpanded = value;
                        });
                      },
                      inkwellRadius: !isExpanded
                          ? BorderRadius.all(Radius.circular(8.0))
                          : BorderRadius.only(
                              topRight: Radius.circular(8.0),
                              topLeft: Radius.circular(8.0),
                            ),
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(5.0),
                            bottomRight: Radius.circular(5.0),
                          ),
                          child: Column(
                            children: <Widget>[
                              new Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 2.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        Text(labelsConfig["nameOfMedicationLabel"]),
                                        Padding(
                                          padding: EdgeInsets.only(bottom: 5.0),
                                          child: new Icon(
                                            Icons.star,
                                            size: 10.0,
                                            color: kinderm8Theme
                                                .Colors.form_requiredicon,
                                          ),
                                        )
                                      ],
                                    ),
                                    TextField(
                                      controller:
                                          _textController_medication_name,
                                    ),
                                  ],
                                ),
                              ),
                              new Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 2.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Text("${labelsConfig["originalPackagingLabel"]}:"),
                                    new Switch(
                                      onChanged: (bool val) {
                                        setState(() {
                                          medic_original_packenabled = val;
                                          print(medic_original_packenabled);
                                        });
                                      },
                                      activeColor: Colors.green,
                                      activeTrackColor: Colors.greenAccent[400],
                                      value: medic_original_packenabled,
                                    ),
                                  ],
                                ),
                              ),
                              new Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 2.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text(
                                            "${labelsConfig["medicalPractitionerLabel"]} :"),
                                      ],
                                    ),
                                    TextField(
                                      controller: _textController_medical,
                                    ),
                                  ],
                                ),
                              ),
                              new Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 14.0),
                                child: InkWell(
                                  onTap: () {
                                    _showDatePicker_priscribed();
                                  },
                                  child: new Row(
                                    children: <Widget>[
                                      new Text('${labelsConfig["datePrescribedLabel"]}:'),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12.0, right: 4.0),
                                        child: Icon(Icons.event),
                                      ),
                                      Container(
                                        child: new Flexible(
                                          fit: FlexFit.tight,
                                          child: new TextField(
                                              enabled: false,
                                              decoration: InputDecoration(
                                                hintText: labelsConfig["dateLabel"],
                                              ),
                                              controller:
                                                  dateInputController_priscribed),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 4.0),
                              new Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 14.0),
                                child: InkWell(
                                  onTap: () {
                                    _showDatePicker_expiry();
                                  },
                                  child: new Row(
                                    children: <Widget>[
                                      new Text('${labelsConfig["expiryDateLabel"]} :'),
                                      SizedBox(
                                        width: 20.0,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12.0, right: 4.0),
                                        child: Icon(Icons.event),
                                      ),
                                      Container(
                                        child: new Flexible(
                                          fit: FlexFit.tight,
                                          child: new TextField(
                                              enabled: false,
                                              decoration: InputDecoration(
                                                hintText: labelsConfig["dateLabel"],
                                              ),
                                              controller:
                                                  dateInputController_expiry),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 2.0),
                              new Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 4.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Text("${labelsConfig["medicationStorageLabel"]}:"),
                                    TextField(
                                      controller: _textController_storage,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 2.0),
                              new Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 4.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Text(labelsConfig["reasonForMedicationLabel"]),
                                    TextField(
                                      maxLines: 2,
                                      controller: _textController_reason,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 6.0,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                // First section end, Second section start
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Material(
                    elevation: 2.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0))),
                    child: GroovinExpansionTile(
                      defaultTrailingIconColor: kinderm8Theme.Colors.appcolour,
                      title: Text(
                        "${labelsConfig["lastMedicationOnLabel"]} Details",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                      ),
                      onExpansionChanged: (value) {
                        setState(() {
                          isExpanded = value;
                        });
                      },
                      inkwellRadius: !isExpanded
                          ? BorderRadius.all(Radius.circular(8.0))
                          : BorderRadius.only(
                              topRight: Radius.circular(8.0),
                              topLeft: Radius.circular(8.0),
                            ),
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(5.0),
                            bottomRight: Radius.circular(5.0),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 14.0),
                                child: InkWell(
//                                    Thid widgets wraps every other widget
                                  onTap: () {
                                    _showDatePicker_lastadministrated();
                                  },
                                  child: new Row(
                                    children: <Widget>[
                                      new Text('${labelsConfig["dateLabel"]}:',
                                          style: TextStyle(
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.w300)),
                                      new Padding(
                                        padding: EdgeInsets.only(bottom: 5.0),
                                        child: new Icon(
                                          Icons.star,
                                          size: 10.0,
                                          color: kinderm8Theme
                                              .Colors.form_requiredicon,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Icon(Icons.event),
                                      ),
                                      Container(
                                        child: new Flexible(
                                          fit: FlexFit.tight,
                                          child: Container(
                                            child: new TextField(
                                                enabled: false,
                                                decoration: InputDecoration(
                                                  hintText: labelsConfig["dateLabel"],
                                                ),
                                                controller:
                                                    dateInputController_lastadministrated),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 4.0),
                              new Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 2.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                        child: InkWell(
//                                    Thid widgets wraps every other widget
                                      onTap: () {
                                        _showStartTimeDialog__lastadministrated();
                                      },
                                      child: new Row(
                                        children: <Widget>[
                                          new Text(
                                            'Time:',
                                            style: TextStyle(
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.w300),
                                          ),
                                          new Padding(
                                            padding:
                                                EdgeInsets.only(bottom: 5.0),
                                            child: new Icon(
                                              Icons.star,
                                              size: 10.0,
                                              color: kinderm8Theme
                                                  .Colors.form_requiredicon,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Icon(Icons.access_time),
                                          ),
                                          Container(
                                            child: new Flexible(
                                              fit: FlexFit.tight,
                                              child: new TextField(
                                                  enabled: false,
                                                  decoration: InputDecoration(
                                                    hintText: labelsConfig["dateLabel"],
                                                  ),
                                                  controller:
                                                      startTimeController),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ))
                                  ],
                                ),
                              ),
                              SizedBox(height: 4.0),
                              new Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 2.0),
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text(labelsConfig["dosageLabel"]),
                                      ],
                                    ),
                                    TextField(
                                      controller: _textController_dosage,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 6.0),
                              new Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 2.0),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text(labelsConfig["methodToBeAdministeredLabel"]),
                                      ],
                                    ),
                                    TextField(
                                      maxLines: 2,
                                      controller:
                                          _textController_methodtobeadministered,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 6.0),
                                child: new Text(
                                  "Medication ${labelsConfig["timesLabel"]}",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16.0,
                                  ),
                                ),
                              ),
                              new Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 6.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () {
                                        _showDatePicker();
                                      },
                                      child: new Row(
                                        children: <Widget>[
                                          new Text(
                                            '${labelsConfig["dateLabel"]}:',
                                            style: TextStyle(
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.w300),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Icon(Icons.event),
                                          ),
                                          Container(
                                            child: new Flexible(
                                              fit: FlexFit.tight,
                                              child: new TextField(
                                                  enabled: false,
                                                  decoration: InputDecoration(
                                                    hintText: labelsConfig["dateLabel"],
                                                  ),
                                                  controller:
                                                      medidateInputController),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              new Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 14.0, vertical: 6.0),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                        child: InkWell(
                                      onTap: () {},
                                      child: new Row(
                                        children: <Widget>[
                                          new Text(
                                            'Number of ${labelsConfig["timesLabel"]}:',
                                            style: TextStyle(
                                                fontSize: 17.0,
                                                fontWeight: FontWeight.w300),
                                          ),
                                          SizedBox(width: 14.0),
                                          Expanded(
                                            child: new Column(
                                              children: <Widget>[
                                                new DropdownButtonFormField<
                                                    int>(
                                                  items: <int>[1, 2, 3, 4, 5, 6]
                                                      .map((int value) {
                                                    return new DropdownMenuItem<
                                                        int>(
                                                      value: value,
                                                      child: new Text("$value"),
                                                    );
                                                  }).toList(),
                                                  hint: new Text("Select time"),
                                                  value: _times,
                                                  onChanged: (newVal) {
                                                    setState(() {
                                                      _times = newVal;
                                                      dtarray = {};
                                                      textEditingController1
                                                          .clear();
                                                      time1 = null;
                                                      textEditingController2
                                                          .clear();
                                                      time2 = null;
                                                      textEditingController3
                                                          .clear();
                                                      time3 = null;
                                                      textEditingController4
                                                          .clear();
                                                      time4 = null;
                                                      textEditingController5
                                                          .clear();
                                                      time5 = null;
                                                      textEditingController6
                                                          .clear();
                                                      time6 = null;
                                                    });
                                                    _setButtonState();
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )),
                                    isWidgethide
                                        ? new Container()
                                        : new Container(
                                            padding: const EdgeInsets.all(16.0),
                                            decoration: new BoxDecoration(
                                              color: kinderm8Theme
                                                  .Colors.appBarGradientEnd,
                                              borderRadius:
                                                  new BorderRadius.all(
                                                      Radius.circular(10.0)),
                                              border: new Border.all(
                                                  color: Color.fromRGBO(
                                                      0, 0, 0, 0.0)),
                                            ),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                new Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    new Text(
                                                      'This day is not booked, do you want to request medication?',
                                                      style: new TextStyle(
                                                        color: kinderm8Theme
                                                            .Colors.app_white,
                                                        fontWeight:
                                                            FontWeight.w300,
                                                        fontSize: 16.0,
//                                                  fontFamily: 'Roboto',
                                                      ),
                                                    ),
                                                    new Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: <Widget>[
                                                        new RaisedButton(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(8.0),
                                                          textColor: Colors.white,
                                                          color: Colors.green.shade400,
                                                          onPressed: () {
                                                            setState(() {
                                                              isWidgethide = true;
                                                            });
                                                          },
                                                          child:
                                                              new Text("Yes"),
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                    isWidgethide
                                        ? Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              style: BorderStyle.solid,
                                              color: Colors.deepPurple)),
                                      child: _times != null
                                          ? Column(
                                        children: <Widget>[
//  1--
                                          _times > 0
                                              ? InkWell(
                                            onTap: () {
                                              print(
                                                  "onTap start");
                                              _showTimePicker1();
                                            },
                                            child:
                                            Container(
                                              child: Row(
                                                children: <
                                                    Widget>[
                                                  Padding(
                                                      padding:
                                                      const EdgeInsets.all(
                                                          8.0),
                                                      child:
                                                      Icon(
                                                        Icons
                                                            .event,
                                                        color: Colors
                                                            .deepPurple,
                                                      ),
                                                    ),
                                                  Expanded(
                                                      child:
                                                      TextFormField(
                                                        controller:
                                                        textEditingController1,
                                                        enabled:
                                                        false,
                                                        decoration: InputDecoration(
                                                            hintText:
                                                            'Time',
                                                            fillColor:
                                                            Colors.grey[
                                                            300],
                                                            filled:
                                                            true,
                                                            border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(
                                                                    0.0)),
                                                            contentPadding: EdgeInsets.fromLTRB(
                                                                10.0,
                                                                10.0,
                                                                10.0,
                                                                10.0)),
                                                      ))
                                                ],
                                              ),),)
                                              : Container(),
                                          SizedBox(height: 2.0),
                                          _times > 1
                                              ? InkWell(
                                              onTap: () {
                                                _showTimePicker2();
                                              },
                                              child: Container(
                                            child: Row(
                                              children: <
                                                  Widget>[
                                                Padding(
                                                    padding:
                                                    const EdgeInsets.all(
                                                        8.0),
                                                    child:
                                                    Icon(
                                                      Icons
                                                          .event,
                                                      color: Colors
                                                          .deepPurple,
                                                    ),
                                                  ),
                                                Expanded(
                                                    child:
                                                    TextFormField(
                                                      controller:
                                                      textEditingController2,
                                                      enabled:
                                                      false,
                                                      decoration: InputDecoration(
                                                          hintText:
                                                          'Time',
                                                          fillColor:
                                                          Colors.grey[
                                                          300],
                                                          filled:
                                                          true,
                                                          border: OutlineInputBorder(
                                                              borderRadius: BorderRadius.circular(
                                                                  0.0)),
                                                          contentPadding: EdgeInsets.fromLTRB(
                                                              10.0,
                                                              10.0,
                                                              10.0,
                                                              10.0)),
                                                    ))
                                              ],
                                            ),
                                          ),
                                          )
                                              : Container(),
                                          SizedBox(height: 2.0),
                                          _times > 2
                                              ? InkWell(
                                              onTap: () {
                                                _showTimePicker3();
                                              },
                                              child: Container(
                                            child: Row(
                                              children: <
                                                  Widget>[
                                                Padding(
                                                    padding:
                                                    const EdgeInsets.all(
                                                        8.0),
                                                    child:
                                                    Icon(
                                                      Icons
                                                          .event,
                                                      color: Colors
                                                          .deepPurple,
                                                    ),
                                                  ),
                                                Expanded(
                                                    child:
                                                    TextFormField(
                                                      controller:
                                                      textEditingController3,
                                                      enabled:
                                                      false,
                                                      decoration: InputDecoration(
                                                          hintText:
                                                          'Time',
                                                          fillColor:
                                                          Colors.grey[
                                                          300],
                                                          filled:
                                                          true,
                                                          border: OutlineInputBorder(
                                                              borderRadius: BorderRadius.circular(
                                                                  0.0)),
                                                          contentPadding: EdgeInsets.fromLTRB(
                                                              10.0,
                                                              10.0,
                                                              10.0,
                                                              10.0)),
                                                    ),),
                                              ],
                                            ),
                                          ),)
                                              : Container(),
                                          SizedBox(height: 2.0),
                                          _times > 3
                                              ? InkWell(
                                            onTap: () {
                                              print(
                                                  "onTap start");
                                              _showTimePicker4();
                                            },
                                            child:
                                            Container(
                                            child: Row(
                                              children: <
                                                  Widget>[
                                                Padding(
                                                    padding:
                                                    const EdgeInsets.all(
                                                        8.0),
                                                    child:
                                                    Icon(
                                                      Icons
                                                          .event,
                                                      color: Colors
                                                          .deepPurple,
                                                    ),
                                                  ),
                                                Expanded(
                                                    child:
                                                    TextFormField(
                                                      controller:
                                                      textEditingController4,
                                                      enabled:
                                                      false,
                                                      decoration: InputDecoration(
                                                          hintText:
                                                          'Time',
                                                          fillColor:
                                                          Colors.grey[
                                                          300],
                                                          filled:
                                                          true,
                                                          border: OutlineInputBorder(
                                                              borderRadius: BorderRadius.circular(
                                                                  0.0)),
                                                          contentPadding: EdgeInsets.fromLTRB(
                                                              10.0,
                                                              10.0,
                                                              10.0,
                                                              10.0)),
                                                    ),),
                                              ],
                                            ),
                                          ),)
                                              : Container(),
                                          SizedBox(height: 2.0),
                                          _times > 4
                                              ? InkWell(
                                              onTap: () {
                                                print(
                                                    "onTap start");
                                                _showTimePicker5();
                                              },
                                              child:
                                              Container(
                                            child: Row(
                                              children: <
                                                  Widget>[
                                                Padding(
                                                    padding:
                                                    const EdgeInsets.all(
                                                        8.0),
                                                    child:
                                                    Icon(
                                                      Icons
                                                          .event,
                                                      color: Colors
                                                          .deepPurple,
                                                    ),
                                                  ),
                                                Expanded(
                                                    child:
                                                    TextFormField(
                                                      controller:
                                                      textEditingController5,
                                                      enabled:
                                                      false,
                                                      decoration: InputDecoration(
                                                          hintText:
                                                          'Time',
                                                          fillColor:
                                                          Colors.grey[
                                                          300],
                                                          filled:
                                                          true,
                                                          border: OutlineInputBorder(
                                                              borderRadius: BorderRadius.circular(
                                                                  0.0)),
                                                          contentPadding: EdgeInsets.fromLTRB(
                                                              10.0,
                                                              10.0,
                                                              10.0,
                                                              10.0)),
                                                    ),),
                                              ],
                                            ),
                                          ),)
                                              : Container(),
                                          SizedBox(height: 2.0),
                                          _times > 5
                                              ? InkWell(
                                              onTap: () {
                                                print(
                                                    "onTap start");
                                                _showTimePicker6();
                                              },
                                              child:
                                              Container(
                                            child: Row(
                                              children: <
                                                  Widget>[
                                                Padding(
                                                    padding:
                                                    const EdgeInsets.all(
                                                        8.0),
                                                    child:
                                                    Icon(
                                                      Icons
                                                          .event,
                                                      color: Colors
                                                          .deepPurple,
                                                    ),
                                                  ),
                                                Expanded(
                                                    child:
                                                    TextFormField(
                                                      controller:
                                                      textEditingController6,
                                                      enabled:
                                                      false,
                                                      decoration: InputDecoration(
                                                          hintText:
                                                          'Time',
                                                          fillColor:
                                                          Colors.grey[
                                                          300],
                                                          filled:
                                                          true,
                                                          border: OutlineInputBorder(
                                                              borderRadius: BorderRadius.circular(
                                                                  0.0)),
                                                          contentPadding: EdgeInsets.fromLTRB(
                                                              10.0,
                                                              10.0,
                                                              10.0,
                                                              10.0)),
                                                    ),),
                                              ],
                                            ),
                                          ),)
                                              : Container(),
                                        ],
                                      )
                                          : Container(),
                                    )
                                        : new Container(),

                                    ///
                                    ///

                                    ///
                                    ///
                                  ],
                                ),
                              ),
                              Container(
                                    child: isSame1 ||
                                          isSame2 ||
                                          isSame3 ||
                                          isSame4 ||
                                          isSame5 ||
                                          isSame6
                                          ? Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 2.0),
                                        child: Text("Check the times",
                                          style: TextStyle(color: Colors.red),),)
                                          : Container(),
                                    ),
                              new Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 14.0, vertical: 6.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Text("Instructions:"),
                                        ],
                                      ),
                                      TextField(
                                          maxLines: 1,
                                          controller:
                                          _textController_instructions,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 10.0, 10.0, 2.0),
                                          ),),
                                      SizedBox(height: 4.0),
                                    ],
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

                /// Second section end
                ///

                new Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 6.0, horizontal: 20.0),
                      child: new Text(
                          'I request that the above medication to be given in accordance with the instructions given.')),

                new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 16.0),
                      child: Material(
                        borderRadius: BorderRadius.circular(10.0),
                        child: MaterialButton(
                          color: isButtonEnabled
                              ? kinderm8Theme.Colors.appcolour
                              : Colors.grey,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text("SUBMIT",
                                style: TextStyle(letterSpacing: 1.1,
                                    fontSize: 20.0, color: Colors.white)),
                          ),
                          onPressed: () {
                            print("Button Clicked");
                            if (isButtonEnabled && isWidgethide) {
                              if (lastAdministratedDate != null) {
                                _lastadministrated = new DateTime(
                                    lastAdministratedDate.year,
                                    lastAdministratedDate.month,
                                    lastAdministratedDate.day,
                                    _startTime.hour,
                                    _startTime.minute);
                              }
                              if (!isSame1 &&
                                  !isSame2 &&
                                  !isSame3 &&
                                  !isSame4 &&
                                  !isSame5 &&
                                  !isSame6) {
                                if (submit) {
                                  setState(() {
                                    submit = false;
                                  });
                                  submitMedi();
                                  print("submit");
                                } else {
                                  submitMedi();
                                  print("else//");
                                }
                              }
                            } else {
                              {
                                showInSnackBar(
                                    "Check the attendance of the child.");
                              }
                            }
                          },
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

//
  submitMedi() {
    FocusScope.of(context).requestFocus(new FocusNode());
    finaldatetimearray = [];
    dtarray.forEach((k, v) {
      print(v);
      finaldatetimearray.add(v.toString());
    });

    print(
        "---------------------finaldatetimearray--------------------$finaldatetimearray");

    print("dt $dtarray");
    print("dateInputController_priscribed $priscribedDate");

    var body = {
      "child_id": selectedChildData.id,
      "medication_name": _textController_medication_name.text.toString(),
      "medic_original_pack": medic_original_packenabled ? 1 : 0,
      "medical": _textController_medical.text.toString(),
      "expiry_date": expireDate == null ? null : expireDate,
      "date_prescribed": priscribedDate == null ? null : priscribedDate,
      "reason": _textController_reason.text.toString(),
      "dosage": _textController_dosage.text.toString(),
      "parent_notified": 0,
      "notified_parent_id": userId,
      "request_status": 0,
      "instructions": _textController_instructions.text.toString(),
      "manner": _textController_methodtobeadministered.text.toString(),
      "times": _times,
      "main_date": medidateInputController.text.toString(),
      "ending_on": medidateInputController.text.toString(),
      "storage": _textController_storage.text.toString(),
      "requested_by": userId,
      "medication_type": 0,
      "parent_medication_id": 0,
      "last_administered_datetime": _lastadministrated.toString(),
      "medication_time": finaldatetimearray
    };

    print("Submitting data >>> $body");

    final submitMediUrl =
        "https://apicare.carem8.com/v2.1.1/medication/create?clientid=$clientId";

    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netUtil = new NetworkUtil();
    _netUtil
        .post(submitMediUrl,
        body: body, headers: headers, encoding: jwt.toString())
        .then((res) {
      print("________ ${res}");

      if (res == 1) {
        String _refreshTokenUrl =
            'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';
        NetworkUtil _netutil = new NetworkUtil();
        _netutil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          var refreshJwtToken = json.decode(response.body);
          this.jwt = refreshJwtToken;
          submitMedi();
        });
      } else if (res != null) {
        _asyncConfirmDialog(context);
//        Dialog_created_ackAlert(context);
        print("may be 200");
//        makeDialog_created();
//        Navigator.pushReplacement(
//            context,
//            MaterialPageRoute(
//                builder: (context) => Medications(selectedChildData, jwt, 0)));
        submit = true;
      } else {
        submit = true;
        print("error");
      }
    });
  }

//
  @override
  void onDisplayUserInfo(User user) {
    print("craete");
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = widget.jwt.toString();
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];

      print(
          '>>>>>>>>>>>>>>>>>>globalconfig >>>>>>>>>>>>>>${appusers["globalconfig"]}');
      // Get config
      globalConfig = appusers["globalconfig"];
//      application_settings=globalConfig["application_settings"];

      fetchCasualattendanceData();
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}