import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/medication/medicationlist.dart';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:carem8/pages/medication/nonprescribedlist.dart';
import 'package:carem8/utils/network_util.dart';

class UpdateNonPrescribed extends StatefulWidget {
  final childData;
  final jwt;
  final singlemedicationData;
  UpdateNonPrescribed(this.singlemedicationData, this.childData, this.jwt);
  @override
  UpdateNonPrescribedState createState() => UpdateNonPrescribedState();
}

class UpdateNonPrescribedState extends State<UpdateNonPrescribed>
    implements HomePageContract {
  var appuser, jwt, clientId, id;
  final formKey1 = new GlobalKey<FormState>();
  final formKey2 = new GlobalKey<FormState>();
  TextEditingController _textController1 = new TextEditingController();
  TextEditingController _textController2 = new TextEditingController();
  TextEditingController dateInputController = new TextEditingController();

  DateTime _date;
  bool update = true;
  bool errorCal = false;

  HomePagePresenter _presenter;

  UpdateNonPrescribedState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textController1.text =
        widget.singlemedicationData['medication_to_administer'];
    _textController2.text = widget.singlemedicationData['circumstances'];
    dateInputController.text =
        widget.singlemedicationData['authorized_date'].toString();

    var date = widget.singlemedicationData['authorized_date'];
    _date = DateTime.parse(date);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Non-Prescribed Medication"),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Container(
                height: 20.0,
              ),
              Container(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Medication to apply/administer :",
                          style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.black,
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          " * ",
                          style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.redAccent,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    Container(
                      child: Form(
                          key: formKey1,
                          child: TextFormField(
                            textInputAction: TextInputAction.done,
                            maxLines: 1,
                            controller: _textController1,
                            validator: (val) {
                              return val.length == 0
                                  ? "This field must not be null"
                                  : null;
                            },
                            decoration: InputDecoration(
                                hintText: 'Medication to administer'),
                          )),
                    ),
                  ],
                ),
              ),
              Container(height: 20.0),
//
              Row(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        "Date authorized",
                        style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w500),
                      ),
                      Text(
                        " * ",
                        style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.redAccent,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      print("onTap start");
                      _showDatePicker();
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.event,
                        color: errorCal ? Colors.redAccent : Colors.deepPurple,
                      ),
                    ),
                  ),
                  Expanded(
                      child: TextFormField(
                    controller: dateInputController,
                    enabled: false,
                    decoration: InputDecoration(
                        fillColor: Colors.grey[300],
                        filled: true,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0.0)),
                        contentPadding:
                            EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0)),
                  ))
                ],
              ),
              Container(height: 20.0),
//
              Container(
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Give under following circumstance :",
                          style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.black,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    Container(
                      child: Form(
                          key: formKey2,
                          child: TextFormField(
                            textInputAction: TextInputAction.done,
                            maxLines: 1,
                            controller: _textController2,
                            validator: (val) {
                              return val.length == 0
                                  ? "This field must not be null"
                                  : null;
                            },
                            decoration: InputDecoration(
                              hintText: "Give under following circumstance :",
                            ),
                          )),
                    ),
                    Container(height: 20.0),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: RaisedButton(
                              color: Colors.deepPurple,
                              child: Padding(
                                padding: const EdgeInsets.all(14.0),
                                child: Text(
                                  "UPDATE",
                                  style: TextStyle(color: Colors.white, fontSize: 20.0),
                                ),
                              ),
                              onPressed: () {
                                if (update) {
                                  setState(() {
                                    update = false;
                                  });
                                  updateMedi();
                                  print("update");
                                } else {
                                  print("else//");
                                }
                              }),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  updateMedi() {
    final form1 = formKey1.currentState;
    if (form1.validate() && _date != null) {
      form1.save();
      FocusScope.of(context).requestFocus(new FocusNode());
      var mediId = widget.singlemedicationData["id"];

      var timeStamp = new DateFormat("yyyy-MM-dd");
      String formatdailychartdate = timeStamp.format(_date);

      var body = {
        "medication_to_administer": _textController1.text.toString(),
        "circumstances": _textController2.text.toString(),
        "id": mediId,
        "authorized_date": formatdailychartdate
      };

      print(body);
      final submitMediUrl =
          'https://apicare.carem8.com/v2.1.0/medication/non-prescribed/update?clientid=$clientId';
      var headers = {"x-authorization": jwt.toString()};

      print(body);
      update = true;

      NetworkUtil _netUtil = new NetworkUtil();
      _netUtil
          .post(submitMediUrl,
              body: body, headers: headers, encoding: jwt.toString())
          .then((res) {
        print("________ ${res.length}");
        if (res == null) {
          setState(() {
            update = true;
          });
        } else if (res == 1) {
          print("500");
          ///
          /// have to do the refresh token  part
          ///
        } else {
          _asyncConfirmDialog(context);
//          Navigator.pushReplacement(
//              context,
//              MaterialPageRoute(
//                  builder: (context) => Medications(widget.childData, jwt,1)));

          _textController1.clear();
          _textController2.clear();
          dateInputController.clear();
        }
      });
    } else {
      setState(() {
        if (_date == null) {
          errorCal = true;
        }
      });
      print("else");
      update = true;
    }
  }

  Future _asyncConfirmDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Thank you for your request!",
              style: TextStyle(
                  color: kinderm8Theme.Colors.appcolour,
                  fontWeight: FontWeight.w300,
                  fontSize: 20.0)),
          content: new Text("Your request has been sumitted sucessfully.",
              style: TextStyle(
                  color: kinderm8Theme.Colors.appcolour,
                  fontWeight: FontWeight.w300,
                  fontSize: 14.0)),
          actions: <Widget>[
            new RaisedButton(
              child: const Text('OK'),
              color: Theme.of(context).backgroundColor,
              elevation: 4.0,
              splashColor: Colors.purple.shade100,
              onPressed: () {
//                Navigator.of(context).pop(ConfirmAction.ACCEPT);

                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Medications(widget.childData, jwt,1)));
              },
            ),
//            FlatButton(
//              child: const Text('Ok'),
//              onPressed: () {
////                Navigator.of(context).pop(ConfirmAction.ACCEPT);
//                Navigator.pushReplacement(
//                    context,
//                    MaterialPageRoute(
//                        builder: (context) => Medications(selectedChildData, jwt, 0)));
//                submit = true;
//              },
//            )
          ],
        );
      },
    );
  }

  _showDatePicker() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(DateTime.now().year - 1, 1),
        lastDate: DateTime(DateTime.now().year, 12));

    if (picked != null) {
      setState(() {
        _date = picked;
        setState(() {
          errorCal = false;
        });
        print("%%%%%%%%%%% $_date");
        dateInputController = new TextEditingController(
            text: "${picked.year}-${picked.month}-${picked.day}");
        print(dateInputController.text);
      });
    }
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = widget.jwt.toString();
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }
    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }

  @override
  void onUpdateJwt() {
    // TODO: implement onUpdateJwt
  }
}
