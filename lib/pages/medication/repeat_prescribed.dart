import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:async/async.dart';
import 'package:groovin_widgets/groovin_expansion_tile.dart';
import 'package:groovin_widgets/modal_drawer_handle.dart';
import 'package:groovin_widgets/outline_dropdown_button.dart';
import 'package:intl/intl.dart';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:carem8/pages/commondrawer.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/medication/medicationlist.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:flutter/services.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:datetime_picker_formfield/time_picker_formfield.dart';

class CreateRepeatPrescribed extends StatefulWidget {
  final selectedChildData;
  final singlemedicationData;
  final jwt;
  CreateRepeatPrescribed(
      this.selectedChildData, this.singlemedicationData, this.jwt);

  @override
  CreateRepeatPrescribedState createState() =>
      CreateRepeatPrescribedState(selectedChildData, singlemedicationData);
}

class CreateRepeatPrescribedState extends State<CreateRepeatPrescribed>
    implements HomePageContract {
  final selectedChildData;
  final singlemedicationData;

  CreateRepeatPrescribedState(
      this.selectedChildData, this.singlemedicationData) {
    presenter = new HomePagePresenter(this);
    presenter.getUserInfo();
  }

  var appuser, jwt, userId, clientId;
  List<Widget> receiver = [];
  List<String> receiverId = [];
  final formKey = new GlobalKey<FormState>();
  bool errorMessage = false;
  bool errorSubject = false;
  var awsFolder;
  String imagePath;
  bool uploading = false;

  bool submit = true;

  var value;
  bool isExpanded = false;

  DateTime _lastadministrated;
  DateTime d1, d2, d3, d4, d5, d6;
  var dtarray = {};

  TextEditingController startTimeController;
  TextEditingController medidateInputController;
  TextEditingController dateInputController_lastadministrated;
  TextEditingController dateInputController_priscribed;

  List<TextEditingController> listTimeController;

  final TextEditingController _textController_medication_name =
      new TextEditingController();
  final TextEditingController _textController_medic_original_pack =
      new TextEditingController();
  final TextEditingController _textController_medical =
      new TextEditingController();
  final TextEditingController _textController_reason =
      new TextEditingController();
  final TextEditingController _textController_dosage =
      new TextEditingController();
  final TextEditingController _textController_instructions =
      new TextEditingController();
  final TextEditingController _textController_manner =
      new TextEditingController();
  final TextEditingController _textController_storage =
      new TextEditingController();
  final TextEditingController _textController_methodtobeadministered =
      new TextEditingController();

  TextEditingController textEditingController1 = new TextEditingController();
  TextEditingController textEditingController2 = new TextEditingController();
  TextEditingController textEditingController3 = new TextEditingController();
  TextEditingController textEditingController4 = new TextEditingController();
  TextEditingController textEditingController5 = new TextEditingController();
  TextEditingController textEditingController6 = new TextEditingController();

  TextEditingController dateInputController;
  bool isButtonEnabled = false;
  bool enabled = false; // practitioner/chemist
  bool medic_original_packenabled = false; // practitioner/chemist
  final dateFormat = DateFormat("EEEE, MMMM d, yyyy 'at' h:mma");
  final timeFormat = DateFormat("h:mm a");

  var _selection = new List<String>();
  var _controllers;

  TimeOfDay _startTime;
  var dateStamp = new DateFormat("yyyy-MM-dd");
  int _times = 1;
  List<String> finaldatetimearray = [];
  bool errorTimes = false;

  // attachment
  List<File> fileList = new List();
  Future<File> _imageFile;
  var timesEditingControllers = <TextEditingController>[];
//  var textFields = <TextField>[];
  var textFields;
  HomePagePresenter presenter;

  //
  submitMedi() {
    FocusScope.of(context).requestFocus(new FocusNode());
    finaldatetimearray = [];
    dtarray.forEach((k, v) {
      print(v);
      finaldatetimearray.add(v.toString());
    });

    print(
        "---------------------finaldatetimearray--------------------$finaldatetimearray");
    print(
        "---------------------singlemedicationData--------------------$singlemedicationData");
    print(dtarray);

    var body = {
      "dosage": _textController_dosage.text.toString(),
      "ismain": false, // Repeat
      "last_administered_datetime": _lastadministrated.toString(),
      "main_date": medidateInputController.text.toString(),
      "manner": _textController_methodtobeadministered.text.toString(),
      "medication_time": finaldatetimearray,
      "parent_medication_id": singlemedicationData["medication_id"],
      "requested_by": userId,
      "medical": _textController_medical.text.toString(),
      "times": _times,
//      "child_id": selectedChildData['id'],
//      "medication_name": _textController_medication_name.text.toString(),
//      "medic_original_pack": medic_original_packenabled ? 1 : 0,
//      "expiry_date": dateInputController_expiry.text.toString(),
//      "date_prescribed": dateInputController_priscribed.text.toString(),
//      "reason": _textController_reason.text.toString(),
//      "notified_parent_id": userId,
//      "request_status": 0,
//      "instructions": _textController_instructions.text.toString(),
//      "ending_on": medidateInputController.text.toString(),
//      "storage": _textController_storage.text.toString(),
//      "medication_type": 0,
    };

    print("Submitting data >>> $body");

    final submitMediUrl =
        "https://apicare.carem8.com/v2.1.1/medication/create?clientid=$clientId";

    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netUtil = new NetworkUtil();
    _netUtil
        .post(submitMediUrl,
            body: body, headers: headers, encoding: jwt.toString())
        .then((res) {
      print("________ $res");

      if (res == 1) {
        String _refreshTokenUrl =
            'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';
        NetworkUtil _netutil = new NetworkUtil();
        _netutil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          var refreshJwtToken = json.decode(response.body);
          this.jwt = refreshJwtToken;
          submitMedi();
        });
      } else if (res != null) {
        _asyncConfirmDialog(context);
        print("may be 200");
        submit = true;
      } else {
        submit = true;
        print("error");
      }
    });
  }

  submitparentmedicationsData() {
    FocusScope.of(context).requestFocus(new FocusNode());

    final submitMessageUrl =
        "https://apicare.carem8.com/v2.1.1/medication/create?clientid=$clientId";
    var body = {
      "child_id": selectedChildData['id'],
      "medication_name": userId,
      "medic_original_pack": "0",
      "medical": "",
      "date_prescribed": receiverId.join(","),
      "expiry_date": awsFolder['folder'].toString(),
      "reason": "",
      "dosage": "",
      "parent_notified": "",
      "notified_parent_id": "",
      "request_status": "",
      "instructions": "",
      "manner": "",
      "times": "",
      "main_date": "",
      "ending_on": "",
      "last_administered_datetime": "",
      "medication_time": ""
    };
    var headers = {"x-authorization": jwt.toString()};
    print(body);

    NetworkUtil _netUtil = new NetworkUtil();
    _netUtil
        .post(submitMessageUrl,
            body: body, headers: headers, encoding: jwt.toString())
        .then((res) {
      print("resss  $res");
      if (res == 1) {
        String _refreshTokenUrl =
            'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';
        NetworkUtil _netutil = new NetworkUtil();
        _netutil.get(_refreshTokenUrl).then((response) {
          print('refresh get ${response.body}');
          var refreshJwtToken = json.decode(response.body);
          this.jwt = refreshJwtToken;
          submitparentmedicationsData();
        });
      } else if (res == '') {
        print("may be 200");
      } else {
        print("error");
      }
    });
  }

  void _setButtonState() {
    setState(() {
      if (_date != null && _startTime != null) {
        if (_times == 1 && time1 != null) {
          isButtonEnabled = true;
        } else if (_times == 2 && time1 != null && time2 != null) {
          isButtonEnabled = true;
        } else if (_times == 3 &&
            time1 != null &&
            time2 != null &&
            time3 != null) {
          isButtonEnabled = true;
        } else if (_times == 4 &&
            time1 != null &&
            time2 != null &&
            time3 != null &&
            time4 != null) {
          isButtonEnabled = true;
        } else if (_times == 5 &&
            time1 != null &&
            time2 != null &&
            time3 != null &&
            time4 != null &&
            time5 != null) {
          isButtonEnabled = true;
        } else if (_times == 6 &&
            time1 != null &&
            time2 != null &&
            time3 != null &&
            time4 != null &&
            time5 != null &&
            time6 != null) {
          isButtonEnabled = true;
        } else {
          isButtonEnabled = false;
        }

        /*if (time1 != null ||
            time2 != null ||
            time3 != null ||
            time4 != null ||
            time5 != null ||
            time6 != null) {
          print("testing3");

          print("setButtonState: Button enabled");
          isButtonEnabled = true;
        }else {
          print("setButtonState: Button diabled");
          isButtonEnabled = false;
        }*/
      }
    });
  }

  Future<Null> _showStartTimeDialog__lastadministrated() async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());

    if (picked != null) {
      setState(() {
        _startTime = picked;
        DateTime time = DateTime(0, 0, 0, _startTime.hour, _startTime.minute);
        print("TIME $time");
        String formattedDate = DateFormat('kk:mm a').format(time);
        startTimeController = new TextEditingController(text: formattedDate);
      });
    }
  }

  DateTime lastAdministratedDate;
  Future<Null> _showDatePicker_lastadministrated() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(DateTime.now().year - 1, 1),
        lastDate: DateTime(DateTime.now().year, 12));
    if (picked != null) {
      setState(() {
        lastAdministratedDate = picked;
        dateInputController_lastadministrated = new TextEditingController(
            text: dateStamp.format(picked).toString());
        print(
            "^***^^*^*^*^^*^**^*^*^^^*^*^*^*^*^*^*_lastadministrated $lastAdministratedDate");
      });
    }
  }

  DateTime _date = DateTime.now();
  Future<Null> _showDatePicker() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(DateTime.now().year - 1, 1),
        lastDate: DateTime(DateTime.now().year, 12));

    // Check casual
    var timeStamp = new DateFormat("yyyy-MM-dd");
    var statusvalue = checkAttendanceDate(timeStamp.format(picked).toString());
    print('statusvalue>> $statusvalue');
    isWidgethide = statusvalue == true ? true : false;
    print('isWidgethide>> $isWidgethide');

    if (picked != null) {
      setState(() {
        _date = picked;
        medidateInputController = new TextEditingController(
            text: timeStamp.format(picked).toString());
      });
    }
  }

  var priscribedDate;
  Future<Null> _showDatePicker_priscribed() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(DateTime.now().year - 1, 1),
        lastDate: DateTime(DateTime.now().year, 12));

    if (picked != null) {
      setState(() {
        priscribedDate = dateStamp.format(picked);
        print(
            "^***^^*^*^*^^*^**^*^*^^^*^*^*^*^*^*^*priscribedDate $priscribedDate");
        dateInputController_priscribed =
            new TextEditingController(text: priscribedDate.toString());
      });
    }
  }

  Future<Null> _showStartTimeDialog() async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());

    if (picked != null) {
      setState(() {
        _startTime = picked;
        startTimeController =
            new TextEditingController(text: "${picked.hour}:${picked.minute}");
      });
      _setButtonState();
    }
  }

  /// Time clocks
  ///
  ///

  bool isSame1 = false,
      isSame2 = false,
      isSame3 = false,
      isSame4 = false,
      isSame5 = false,
      isSame6 = false;
  TimeOfDay time1, time2, time3, time4, time5, time6;

  Future<Null> _showTimePicker1() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time1) {
      setState(() {
        d1 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');
        String _toJson(DateTime date) => _dateFormatter.format(date);

        if (dtarray.containsValue(_dateFormatter.format(d1))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame1 = true;
          print(isSame1);
        } else {
          time1 = picked;
          dtarray[0] = _toJson(d1);
          print("dtarray ->$dtarray");
          isSame1 = false;
//Assign value
          String formattedDate = DateFormat('kk:mm a').format(d1);
          textEditingController1.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame1 = false;
      });
      print("else");
    }
  }

  Future<Null> _showTimePicker2() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time2) {
      setState(() {
        d2 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');

        String _toJson(DateTime date) => _dateFormatter.format(date);
        print("to json ->${_toJson(d2)}");

        if (dtarray.containsValue(_dateFormatter.format(d2))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame2 = true;
        } else {
          time2 = picked;
          dtarray[1] = _toJson(d2);
          print("dtarray ->$dtarray");
          isSame2 = false;
          String formattedDate = DateFormat('kk:mm a').format(d2);
          textEditingController2.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame2 = false;
      });
      print("else");
    }
  }

  Future<Null> _showTimePicker3() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time3) {
      setState(() {
        d3 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');
        String _toJson(DateTime date) => _dateFormatter.format(date);
        print("to json ->${_toJson(d3)}");

        if (dtarray.containsValue(_dateFormatter.format(d3))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame3 = true;
        } else {
          time3 = picked;
          dtarray[2] = _toJson(d3);
          print("dtarray ->$dtarray");
          isSame3 = false;
//Assign value
          String formattedDate = DateFormat('kk:mm a').format(d3);
          textEditingController3.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame3 = false;
      });
      print("else");
    }
  }

  Future<Null> _showTimePicker4() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time4) {
      setState(() {
        d4 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');
        String _toJson(DateTime date) => _dateFormatter.format(date);
        print("to json ->${_toJson(d4)}");

        if (dtarray.containsValue(_dateFormatter.format(d4))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame4 = true;
        } else {
          time4 = picked;
          dtarray[3] = _toJson(d4);
          print("dtarray ->$dtarray");
          isSame4 = false;

          //Assign value
          String formattedDate = DateFormat('kk:mm a').format(d4);
          textEditingController4.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame4 = false;
      });
      print("else");
    }
  }

  Future<Null> _showTimePicker5() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time5) {
      setState(() {
        d5 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');
        String _toJson(DateTime date) => _dateFormatter.format(date);
        print("to json ->${_toJson(d5)}");

        if (dtarray.containsValue(_dateFormatter.format(d5))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame5 = true;
        } else {
          time5 = picked;
          dtarray[4] = _toJson(d5);
          print("dtarray ->$dtarray");
          isSame5 = false;

          //Assign value
          String formattedDate = DateFormat('kk:mm a').format(d5);
          textEditingController5.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame5 = false;
      });
      print("else");
    }
  }

  Future<Null> _showTimePicker6() async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );

    if (picked != time6) {
      setState(() {
        d6 = new DateTime(
            _date.year, _date.month, _date.day, picked.hour, picked.minute);
        final _dateFormatter = new DateFormat('yyyy-MM-dd kk:mm:ss');
        String _toJson(DateTime date) => _dateFormatter.format(date);
        print("to json ->${_toJson(d6)}");
        if (dtarray.containsValue(_dateFormatter.format(d6))) {
          print("data exist");
          print("dtarray ->$dtarray");
          isSame6 = true;
        } else {
          time6 = picked;
          dtarray[5] = _toJson(d6);
          print("dtarray ->$dtarray");
          isSame6 = false;
          //Assign value
          String formattedDate = DateFormat('kk:mm a').format(d6);
          textEditingController6.text = formattedDate;
        }
      });
    } else {
      setState(() {
        isSame6 = false;
      });
      print("else");
    }
  }

  ///
  ///
  /// End of Clocks

  var globalConfig;
  var application_settings;

  // Set this two values from config
  var enable_sunday = true;
  var enable_saturday = true;

  // Showing widget condiation
  var isWidgethide = false;
  String childattendance;
  var myattandance = [];

  void checkattendance() {
    print(application_settings);
//    print(globalConfig);
    print(
        '----------------------------checkattendance------------------------------------------');
//    print(globalConfig);
    print(application_settings);

    print(selectedChildData['attendance']);
    childattendance = selectedChildData['attendance'];
    var attarray = childattendance.split(',');
    print(attarray);
    for (int x = 0; x < attarray.length; x++) {
//      DateTime listvalue = DateTime.parse(attarray[x]);
      var LowerCasevalue = attarray[x].toLowerCase();
      print(attarray[x].toLowerCase());
      switch (LowerCasevalue) {
        case "sunday":
          myattandance.add(0);
          break;
        case "monday":
          myattandance.add(1);
          break;
        case "tuesday":
          myattandance.add(2);
          break;
        case "wednesday":
          myattandance.add(3);
          break;
        case "thursday":
          myattandance.add(4);
          break;
        case "friday":
          myattandance.add(5);
          break;
        case "saturday":
          myattandance.add(6);
      }
    }

    print('########### myattandance ###########$myattandance');

    var timeStamp = new DateFormat("yyyy-MM-dd");
    var statusvalue = checkAttendanceDate(timeStamp.format(_date));
    setState(() {
      isWidgethide = statusvalue == true ? true : false;
    });
    print('isWidgethide>> $isWidgethide');
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    medidateInputController =
        new TextEditingController(text: dateStamp.format(_date).toString());
  }

  // get Casualattendance
  var casualattendancedata;
  Future<String> fetchCasualattendanceData() async {
    ///data from GET method
    print("data fetched");
    var date = new DateTime.now();
    var casualattendancedate = new DateTime(date.year, date.month, date.day);
    String formatDate = dateStamp.format(casualattendancedate);
    var childId = selectedChildData['id'];
    String _dailyJournalUrl =
        'https://apicare.carem8.com/v4.4.0/getcasualattendancedates/$childId?clientid=$clientId&date=$formatDate';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_dailyJournalUrl, headers: headers).then((response) {
      try {
        casualattendancedata = json.decode(response.body);

        print('jwt### $jwt');
        print(response.statusCode);
        if (response.statusCode == 200) {
          checkattendance();
        } else if (response.statusCode == 500 &&
            casualattendancedata["errorType"] == 'ExpiredJwtException') {
          getRefreshToken();
        } else {
          print("error....!");
        }
      } catch (e) {
        print('That string was null!');
      }
    });
    return null;
  }

  getRefreshToken() {
    print("refreshing Token..");
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      refreshJwtToken = json.decode(response.body);
      this.jwt = refreshJwtToken;
      fetchCasualattendanceData();
    });
  }

  Future _asyncConfirmDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Thank you for your request!",
              style: TextStyle(
                  color: kinderm8Theme.Colors.appcolour,
                  fontWeight: FontWeight.w300,
                  fontSize: 20.0)),
          content: new Text("Your request has been sumitted sucessfully.",
              style: TextStyle(
                  color: kinderm8Theme.Colors.appcolour,
                  fontWeight: FontWeight.w300,
                  fontSize: 14.0)),
          actions: <Widget>[
            new RaisedButton(
              child: const Text('OK'),
              color: Theme.of(context).backgroundColor,
              elevation: 4.0,
              splashColor: Colors.purple.shade100,
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            Medications(selectedChildData, jwt, 0)));
                submit = true;
              },
            ),
          ],
        );
      },
    );
  }

  checkAttendanceDate(date) {
    var medi_status;
    print('myattandance$myattandance');
    var timeStamp = new DateFormat("yyyy-MM-dd");
    var todayattendance = timeStamp.parse(date);

    var attendancedate = new DateTime(
        todayattendance.year, todayattendance.month, todayattendance.day);
    String formatdailychartdate = timeStamp.format(attendancedate);
    print("formated date ${formatdailychartdate}");
    print(attendancedate.weekday);
    print(myattandance.length);
    for (int s = 0; s < myattandance.length; s++) {
      var selected = myattandance[s];
      print(selected);
      print(attendancedate.weekday);
      if (attendancedate.weekday == selected &&
          todayattendance.difference(attendancedate).inDays == 0) {
        var difference = attendancedate.difference(todayattendance).inDays;
        print('difference>> $difference');
        if (difference == 0) {
          medi_status = true;
        }
        print(
            'attendancedate${attendancedate} - today${todayattendance} | attn-wek${attendancedate.weekday} today-week${selected} selected medi_status >> $medi_status');
      }
      if (casualattendancedata.length > 0) {
        for (int t = 0; t < casualattendancedata.length; t++) {
          var selectedday = casualattendancedata[t]['date'];
          print('checkAttendanceDate${casualattendancedata[t]['date']}');
          var loopingattendancedate = DateTime.parse(selectedday);
          print(loopingattendancedate.weekday);
          if (attendancedate.weekday == loopingattendancedate.weekday &&
              attendancedate.difference(loopingattendancedate).inDays == 0) {
            var difference =
                attendancedate.difference(loopingattendancedate).inDays;
            print('difference>> $difference');
            if (difference == 0) {
              medi_status = true;
            }
            print(
                'attendancedate${attendancedate} - loop${loopingattendancedate} |  attn-wek${attendancedate.weekday} loop-week${loopingattendancedate.weekday} loop medi_status >> $medi_status');
          }
        }
      }
    }
    print('checkAttendanceDate >>$medi_status');
    return medi_status;
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    _setButtonState();

    if (dtarray.length != _times) {
      setState(() {
        errorTimes = true;
      });
    } else {
      setState(() {
        errorTimes = false;
      });
    }
//    var singlemedicationData = widget.singlemedicationData;
//    var selectedtime = 1;
//    _controllers = new List.generate(selectedtime, (i) => new TextEditingController());
//    _textController_dosage.text = singlemedicationData['dosage'];
//    _textController_methodtobeadministered.text = singlemedicationData['manner'];
//    _selection.addAll(_items);

//    var textFields_ui = new List.generate(selectedtime, (i) => i)
//        .map(
//          (i) => new Stack(
//        children: [
//          GestureDetector(  //Thid widgets wraps every other widget
//            onTap: () {
//              print("onTap start");
//              var x = _controllers;
//              _showTimeDialog(selectedtime,x);
//            },
//            child: new Row(
//              children: <Widget>[
//                new Text('Time ${i + 1}:'),
//                Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Icon(Icons.access_time),
//                ),
//                Container(
//                  child: new Flexible(
//                    fit: FlexFit.tight,
//                    child: Container(
//                      color: kinderm8Theme.Colors.appcolour,
////                    decoration: BoxDecoration(
////                        border: Border.all(
////                          color: Theme.of(context).disabledColor,
////                          width: 2.0,
////                          style: BorderStyle.solid,
////                        ),
//////                        borderRadius: BorderRadius.circular(2.0)
////                    ),
//                      child: Padding(
//                        padding: const EdgeInsets.all(0.0),
//                        child: new TextField(
//                          autocorrect: false,
//                          enabled: false,
//                          controller: _controllers[i],
//                          textAlign: TextAlign.center,
//                          keyboardType: TextInputType.datetime,
//                          decoration: new InputDecoration(
//                              hintText: _startTime != null
//                                  ? "${_startTime.format(context)}"
//                                  : "Time ${_startTime.format(context)}", hintStyle: new TextStyle(color: kinderm8Theme.Colors.app_white[100])),
//                        ),
//                      ),
//                    ),
//                  ),
//                ),
//              ],
//            ),
//          ),
////          new TextField(
////            autocorrect: false,
////            controller: _controllers[i],
////            textAlign: TextAlign.center,
////            keyboardType: TextInputType.datetime,
////            decoration: new InputDecoration(
////                hintText: "Time ${i + 1}", hintStyle: new TextStyle(color: Colors.deepOrangeAccent[100])),
////          )
//        ],
//      ),
//    ).toList(growable: false);

    return Scaffold(
      appBar: AppBar(
        title: Text("Create ${labelsConfig["repeatPrescribedLabel"]}"),
        centerTitle: true,
        elevation: 0.0,
        actions: <Widget>[
//          IconButton(
//            icon: Icon(Icons.more_vert),
//            onPressed: () {
//              showModalBottomSheet(
//                context: context,
//                builder: (builder) {
//                  return Container(
//                    height: 250.0,
//                    child: Column(
//                      children: <Widget>[
//                        Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child: ModalDrawerHandle(
//                            handleColor: Colors.indigoAccent,
//                          ),
//                        ),
//                      ],
//                    ),
//                  );
//                },
//              );
//            },
//          ),
        ],
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: SingleChildScrollView(
//          physics: const AlwaysScrollableScrollPhysics(),
//        padding: const EdgeInsets.only(top: 10.0,bottom: 10.0),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
              (singlemedicationData['medication_name'] != null && singlemedicationData['medication_name'] != "")
              ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 18.0, top: 4.0),
                            child: Text(
                              labelsConfig["nameOfMedicationLabel"],
                              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 18.0, top: 4.0),
                              child: Text(
                                  "${singlemedicationData['medication_name']}"),
                            ),
                          ),
                        ],
                      ) : SizedBox(),
                SizedBox(height: 4.0),

                singlemedicationData['medic_original_pack'] == ""
                    ? Container()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 18.0, top: 4.0),
                            child: Text(
                              labelsConfig["originalPackagingLabel"],
                              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 18.0, top: 4.0),
                              child: Text(
                                  "${singlemedicationData["medic_original_pack"] == 0 ? 'No' : 'Yes'}"),
                            ),
                          ),
                        ],
                      ),
                SizedBox(height: 4.0,),
                (singlemedicationData['medical'] == "" || singlemedicationData['medical'] == null)
                    ? Container()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 18.0, top: 4.0),
                            child: Text(
                              "${labelsConfig["medicalPractitionerLabel"]} :",
                              style: TextStyle(fontSize: 16.0),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 18.0, vertical: 4.0),
                              child: Text("${singlemedicationData['medical']}"),
                            ),
                          ),
                        ],
                      ),
                SizedBox(height: 2.0,),
                (singlemedicationData['date_prescribed'] == null || singlemedicationData['date_prescribed'] == "")
                    ? Container()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 14.0, right: 14.0, top: 6.0),
                            child: Text(
                              "${labelsConfig["datePrescribedLabel"]}:",
                              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 14.0, right: 14.0, top: 4.0),
                              child: Text(
                                  "${singlemedicationData['date_prescribed']}"),
                            ),
                          ),
                        ],
                      ),
                SizedBox(height: 2.0,),
                singlemedicationData['expiry_date'] == null
                    ? Container()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 14.0, right: 14.0, top: 6.0),
                            child: Text(
                              "${labelsConfig["expiryDateLabel"]}:",
                              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 14.0, right: 14.0, top: 4.0),
                              child: Text(
                                  "${singlemedicationData['expiry_date']}"),
                            ),
                          ),
                        ],
                      ),
                SizedBox(height: 2.0),
                (singlemedicationData['storage'] == "" || singlemedicationData['storage'] == null)
                    ? Container()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 14.0, right: 14.0, top: 6.0),
                            child: Text(
                              "${labelsConfig["medicationStorageLabel"]} :",
                              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 14.0, right: 14.0, top: 4.0),
                              child: Text("${singlemedicationData['storage']}"),
                            ),
                          ),
                        ],
                      ),
                SizedBox(height: 2.0),
                (singlemedicationData['reason'] == "" || singlemedicationData['reason'] == null)
                    ? Container()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 14.0, right: 14.0, top: 6.0),
                            child: Text(
                              "${labelsConfig["reasonForMedicationLabel"]} :",
                              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 14.0, right: 14.0, top: 4.0),
                              child: Text("${singlemedicationData['reason']}"),
                            ),
                          ),
                        ],
                      ),
                SizedBox(height: 2.0),
                (singlemedicationData['manner'] == "" || singlemedicationData['manner'] == null)
                    ? Container()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 14.0, right: 14.0, top: 6.0),
                            child: Text(
                              "${labelsConfig["methodToBeAdministeredLabel"]} :",
                              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 14.0, right: 14.0, top: 4.0),
                              child: Text("${singlemedicationData['manner']}"),
                            ),
                          ),
                        ],
                      ),
                SizedBox(height: 2.0),
                (singlemedicationData['dosage'] == "" || singlemedicationData['dosage'] == null)
                    ? Container()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 14.0, right: 14.0, top: 6.0),
                            child: Text(
                              "${labelsConfig["dosageLabel"]}:",
                              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 14.0, right: 14.0, top: 4.0),
                              child: Text("${singlemedicationData['dosage']}"),
                            ),
                          ),
                        ],
                      ),
                SizedBox(height: 2.0),
                (singlemedicationData['instructions'] == "" || singlemedicationData['instructions'] == null)
                    ? Container()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 14.0, right: 14.0, top: 6.0),
                            child: Text(
                              "${labelsConfig["medicationInstructionLabel"]} :",
                              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 14.0, right: 14.0, top: 4.0),
                              child: Text(
                                  "${singlemedicationData['instructions']}"),
                            ),
                          ),
                        ],
                      ),

                // First section end

                ///
                /// Second section
                ///
                Padding(
                  padding: const EdgeInsets.only(
                    top: 8.0,
                    left: 4.0,
                    right: 4.0,
                  ),
                  child: Material(
                    elevation: 2.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8.0))),
                    child: GroovinExpansionTile(
                      defaultTrailingIconColor: kinderm8Theme.Colors.appcolour,
                      title: Text(
                        "${labelsConfig["lastMedicationOnLabel"]} Details",
                        style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
                      ),
                      onExpansionChanged: (value) {
                        setState(() {
                          isExpanded = value;
                        });
                      },
                      inkwellRadius: !isExpanded
                          ? BorderRadius.all(Radius.circular(8.0))
                          : BorderRadius.only(
                              topRight: Radius.circular(8.0),
                              topLeft: Radius.circular(8.0),
                            ),
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(5.0),
                            bottomRight: Radius.circular(5.0),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Padding(
                                padding: const EdgeInsets.only(
                                    left: 14.0, right: 14.0, bottom: 0.0),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                        child: InkWell(
//                                    Thid widgets wraps every other widget
                                      onTap: () {
                                        print("onTap start");
                                        _showDatePicker_lastadministrated();
                                      },
                                      child: new Row(
                                        children: <Widget>[
                                          new Text('${labelsConfig["dateLabel"]}:'),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Icon(Icons.event),
                                          ),
                                          Container(
                                            child: new Flexible(
                                              fit: FlexFit.tight,
                                              child: Container(
                                                child: Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            10.0,
                                                            10.0,
                                                            10.0,
                                                            10.0),
                                                    child: new TextField(
                                                        enabled: false,
                                                        decoration:
                                                            InputDecoration(
                                                          hintText: "Date",
                                                        ),
                                                        maxLines: 1,
                                                        controller:
                                                            dateInputController_lastadministrated)),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ))
                                  ],
                                ),
                              ),
                              new Padding(
                                padding: const EdgeInsets.only(
                                    left: 14.0, right: 14.0, bottom: 0.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                        child: InkWell(
//                                    Thid widgets wraps every other widget
                                      onTap: () {
                                        print("onTap start");
                                        _showStartTimeDialog__lastadministrated();
                                      },
                                      child: new Row(
                                        children: <Widget>[
                                          new Text('Time:'),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Icon(Icons.access_time),
                                          ),
                                          Container(
                                            child: new Flexible(
                                              fit: FlexFit.tight,
                                              child: Container(
                                                child: Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            10.0,
                                                            10.0,
                                                            10.0,
                                                            10.0),
                                                    child: new TextField(
                                                        enabled: false,
                                                        decoration:
                                                            InputDecoration(
                                                          hintText: "Date",
                                                        ),
                                                        maxLines: 1,
                                                        controller:
                                                            startTimeController)),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ))
                                  ],
                                ),
                              ),
                              new Container(
                                    padding: const EdgeInsets.only(
                                        left: 14.0, right: 14.0, bottom: 0.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Text(labelsConfig["dosageLabel"]),
                                        ],
                                      ),
                                      TextField(
                                          maxLines: 1,
                                          controller: _textController_dosage,)
                                    ],
                                  )),
                              new Container(
                                    padding: const EdgeInsets.only(
                                        left: 14.0, right: 14.0, bottom: 0.0, top: 8.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Text(labelsConfig["methodToBeAdministeredLabel"]),
                                        ],
                                      ),
                                      TextField(
                                          maxLines: 2,
                                          controller:
                                              _textController_methodtobeadministered,)
                                    ],
                                  )),
                              SizedBox(height: 8.0,),
                              Container(
                                  padding: const EdgeInsets.only(
                                      left: 14.0, right: 14.0, bottom: 0.0),
                                  child: new Text(
                                    "Medication ${labelsConfig["timesLabel"]}",
                                    style: TextStyle(color: Colors.black),
                                  )),
                              new Padding(
                                padding: const EdgeInsets.only(
                                    left: 14.0, right: 14.0, bottom: 0.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                        child: InkWell(
//                                    Thid widgets wraps every other widget
                                      onTap: () {
                                        print("onTap start");
                                        _showDatePicker();
                                      },
                                      child: new Row(
                                        children: <Widget>[
                                          new Text('${labelsConfig["dateLabel"]}:'),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Icon(Icons.event),
                                          ),
                                          Container(
                                            child: new Flexible(
                                              fit: FlexFit.tight,
                                              child: Container(
                                                child: Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            10.0,
                                                            10.0,
                                                            10.0,
                                                            10.0),
                                                    child: new TextField(
                                                        enabled: false,
                                                        decoration:
                                                            InputDecoration(
                                                          hintText: "${labelsConfig["dateLabel"]}",
                                                        ),
                                                        maxLines: 1,
                                                        controller:
                                                            medidateInputController)),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ))
                                  ],
                                ),
                              ),
                              new Padding(
                                padding: const EdgeInsets.only(
                                    left: 14.0, right: 14.0, bottom: 0.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                        child: InkWell(
//                                    Thid widgets wraps every other widget
                                      onTap: () {
                                        print("onTap start");
                                      },
                                      child: new Row(
                                        children: <Widget>[
                                          new Text('Number of ${labelsConfig["timesLabel"]}:'),
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: 5.0, bottom: 5.0),
                                            child: new Column(
                                              children: <Widget>[
                                                new DropdownButton<int>(
                                                  items: <int>[1, 2, 3, 4, 5, 6]
                                                      .map((int value) {
                                                    return new DropdownMenuItem<
                                                        int>(
                                                      value: value,
                                                      child: new Text("$value"),
                                                    );
                                                  }).toList(),
                                                  hint: new Text("Select time"),
                                                  value: _times,
                                                  onChanged: (newVal) {
                                                    setState(() {
                                                      _times = newVal;
                                                      dtarray = {};
                                                      textEditingController1
                                                          .clear();
                                                      time1 = null;
                                                      textEditingController2
                                                          .clear();
                                                      time2 = null;
                                                      textEditingController3
                                                          .clear();
                                                      time3 = null;
                                                      textEditingController4
                                                          .clear();
                                                      time4 = null;
                                                      textEditingController5
                                                          .clear();
                                                      time5 = null;
                                                      textEditingController6
                                                          .clear();
                                                      time6 = null;
                                                    });
                                                    _setButtonState();
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )),

                                    ///
                                    ///

//                                    isWidgethide
//                                        ? new Container()
//                                        : new Container(
//                                            padding: const EdgeInsets.all(16.0),
//                                            decoration: new BoxDecoration(
//                                              color: kinderm8Theme
//                                                  .Colors.appBarGradientEnd,
//                                              borderRadius:
//                                                  new BorderRadius.all(
//                                                      Radius.circular(10.0)),
//                                              border: new Border.all(
//                                                  color: Color.fromRGBO(
//                                                      0, 0, 0, 0.0)),
//                                            ),
//                                            child: Column(
//                                              mainAxisAlignment:
//                                                  MainAxisAlignment.start,
//                                              crossAxisAlignment:
//                                                  CrossAxisAlignment.start,
//                                              children: <Widget>[
//                                                new Column(
//                                                  mainAxisAlignment:
//                                                      MainAxisAlignment.center,
//                                                  children: <Widget>[
//                                                    new Text(
//                                                      'This day is not booked, do you want to request medication?',
//                                                      style: new TextStyle(
//                                                        color: kinderm8Theme
//                                                            .Colors.app_white,
//                                                        fontWeight:
//                                                            FontWeight.w300,
//                                                        fontSize: 16.0,
////                                                  fontFamily: 'Roboto',
//                                                      ),
//                                                    ),
//                                                    new Row(
//                                                      mainAxisAlignment:
//                                                          MainAxisAlignment
//                                                              .spaceEvenly,
//                                                      children: <Widget>[
//                                                        new RaisedButton(
//                                                          padding:
//                                                              const EdgeInsets
//                                                                  .all(8.0),
//                                                          textColor:
//                                                              Colors.white,
//                                                          color: Colors
//                                                              .green.shade400,
//                                                          onPressed: () {
//                                                            setState(() {
//                                                              isWidgethide =
//                                                                  true;
//                                                            });
//                                                          },
//                                                          child:
//                                                              new Text("Yes"),
//                                                        ),
////                                                  new RaisedButton(
////                                                    onPressed: (){},
////                                                    textColor: Colors.white,
////                                                    color: kinderm8Theme.Colors.eventrsvp_no,
////                                                    padding: const EdgeInsets.all(8.0),
////                                                    child: new Text(
////                                                      "No",
////                                                    ),
////                                                  ),
//                                                      ],
//                                                    )
//                                                  ],
//                                                )
//                                              ],
//                                            ),
//                                          ),

                                    isWidgethide
                                        ? Container(
                                            width: screenSize.width / 2,
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    style: BorderStyle.solid,
                                                    color: Colors.deepPurple)),
                                            child: _times != null
                                                ? Column(
                                                    children: <Widget>[
//  1--
                                                      _times > 0
                                                          ? Container(
                                                              width: screenSize
                                                                      .width /
                                                                  2,
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  GestureDetector(
                                                                    onTap: () {
                                                                      print(
                                                                          "onTap start");
                                                                      _showTimePicker1();
                                                                    },
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          const EdgeInsets.all(
                                                                              8.0),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .event,
                                                                        color: Colors
                                                                            .deepPurple,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                      child:
                                                                          TextFormField(
                                                                    controller:
                                                                        textEditingController1,
                                                                    enabled:
                                                                        false,
                                                                    decoration: InputDecoration(
                                                                        hintText:
                                                                            'Time',
                                                                        fillColor:
                                                                            Colors.grey[
                                                                                300],
                                                                        filled:
                                                                            true,
                                                                        border: OutlineInputBorder(
                                                                            borderRadius: BorderRadius.circular(
                                                                                0.0)),
                                                                        contentPadding: EdgeInsets.fromLTRB(
                                                                            10.0,
                                                                            10.0,
                                                                            10.0,
                                                                            10.0)),
                                                                  ))
                                                                ],
                                                              ))
                                                          : Container(),
// 2--
                                                      _times > 1
                                                          ? Container(
                                                              width: screenSize
                                                                      .width /
                                                                  2,
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  GestureDetector(
                                                                    onTap: () {
                                                                      print(
                                                                          "onTap start");
                                                                      _showTimePicker2();
                                                                    },
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          const EdgeInsets.all(
                                                                              8.0),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .event,
                                                                        color: Colors
                                                                            .deepPurple,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                      child:
                                                                          TextFormField(
                                                                    controller:
                                                                        textEditingController2,
                                                                    enabled:
                                                                        false,
                                                                    decoration: InputDecoration(
                                                                        hintText:
                                                                            'Time',
                                                                        fillColor:
                                                                            Colors.grey[
                                                                                300],
                                                                        filled:
                                                                            true,
                                                                        border: OutlineInputBorder(
                                                                            borderRadius: BorderRadius.circular(
                                                                                0.0)),
                                                                        contentPadding: EdgeInsets.fromLTRB(
                                                                            10.0,
                                                                            10.0,
                                                                            10.0,
                                                                            10.0)),
                                                                  ))
                                                                ],
                                                              ),
                                                            )
                                                          : Container(),
// 3--
                                                      _times > 2
                                                          ? Container(
                                                              width: screenSize
                                                                      .width /
                                                                  2,
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  GestureDetector(
                                                                    onTap: () {
                                                                      print(
                                                                          "onTap start");
                                                                      _showTimePicker3();
                                                                    },
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          const EdgeInsets.all(
                                                                              8.0),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .event,
                                                                        color: Colors
                                                                            .deepPurple,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                      child:
                                                                          TextFormField(
                                                                    controller:
                                                                        textEditingController3,
                                                                    enabled:
                                                                        false,
                                                                    decoration: InputDecoration(
                                                                        hintText:
                                                                            'Time',
                                                                        fillColor:
                                                                            Colors.grey[
                                                                                300],
                                                                        filled:
                                                                            true,
                                                                        border: OutlineInputBorder(
                                                                            borderRadius: BorderRadius.circular(
                                                                                0.0)),
                                                                        contentPadding: EdgeInsets.fromLTRB(
                                                                            10.0,
                                                                            10.0,
                                                                            10.0,
                                                                            10.0)),
                                                                  ))
                                                                ],
                                                              ),
                                                            )
                                                          : Container(),
// 4--
                                                      _times > 3
                                                          ? Container(
                                                              width: screenSize
                                                                      .width /
                                                                  2,
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  GestureDetector(
                                                                    onTap: () {
                                                                      print(
                                                                          "onTap start");
                                                                      _showTimePicker4();
                                                                    },
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          const EdgeInsets.all(
                                                                              8.0),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .event,
                                                                        color: Colors
                                                                            .deepPurple,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                      child:
                                                                          TextFormField(
                                                                    controller:
                                                                        textEditingController4,
                                                                    enabled:
                                                                        false,
                                                                    decoration: InputDecoration(
                                                                        hintText:
                                                                            'Time',
                                                                        fillColor:
                                                                            Colors.grey[
                                                                                300],
                                                                        filled:
                                                                            true,
                                                                        border: OutlineInputBorder(
                                                                            borderRadius: BorderRadius.circular(
                                                                                0.0)),
                                                                        contentPadding: EdgeInsets.fromLTRB(
                                                                            10.0,
                                                                            10.0,
                                                                            10.0,
                                                                            10.0)),
                                                                  ))
                                                                ],
                                                              ),
                                                            )
                                                          : Container(),
// 5--
                                                      _times > 4
                                                          ? Container(
                                                              width: screenSize
                                                                      .width /
                                                                  2,
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  GestureDetector(
                                                                    onTap: () {
                                                                      print(
                                                                          "onTap start");
                                                                      _showTimePicker5();
                                                                    },
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          const EdgeInsets.all(
                                                                              8.0),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .event,
                                                                        color: Colors
                                                                            .deepPurple,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                      child:
                                                                          TextFormField(
                                                                    controller:
                                                                        textEditingController5,
                                                                    enabled:
                                                                        false,
                                                                    decoration: InputDecoration(
                                                                        hintText:
                                                                            'Time',
                                                                        fillColor:
                                                                            Colors.grey[
                                                                                300],
                                                                        filled:
                                                                            true,
                                                                        border: OutlineInputBorder(
                                                                            borderRadius: BorderRadius.circular(
                                                                                0.0)),
                                                                        contentPadding: EdgeInsets.fromLTRB(
                                                                            10.0,
                                                                            10.0,
                                                                            10.0,
                                                                            10.0)),
                                                                  ))
                                                                ],
                                                              ),
                                                            )
                                                          : Container(),
// 6--
                                                      _times > 5
                                                          ? Container(
                                                              width: screenSize
                                                                      .width /
                                                                  2,
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  GestureDetector(
                                                                    onTap: () {
                                                                      print(
                                                                          "onTap start");
                                                                      _showTimePicker6();
                                                                    },
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          const EdgeInsets.all(
                                                                              8.0),
                                                                      child:
                                                                          Icon(
                                                                        Icons
                                                                            .event,
                                                                        color: Colors
                                                                            .deepPurple,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                      child:
                                                                          TextFormField(
                                                                    controller:
                                                                        textEditingController6,
                                                                    enabled:
                                                                        false,
                                                                    decoration: InputDecoration(
                                                                        hintText:
                                                                            'Time',
                                                                        fillColor:
                                                                            Colors.grey[
                                                                                300],
                                                                        filled:
                                                                            true,
                                                                        border: OutlineInputBorder(
                                                                            borderRadius: BorderRadius.circular(
                                                                                0.0)),
                                                                        contentPadding: EdgeInsets.fromLTRB(
                                                                            10.0,
                                                                            10.0,
                                                                            10.0,
                                                                            10.0)),
                                                                  ))
                                                                ],
                                                              ),
                                                            )
                                                          : Container(),
                                                    ],
                                                  )
                                                : Container(),
                                          )
                                        : new Container(),

                                    ///
                                    ///

                                    ///
                                    ///
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    top: 5.0,
                                    left: 5.0,
                                    right: 5.0,
                                    bottom: 5.0),
                                child: Container(
                                    child: isSame1 ||
                                            isSame2 ||
                                            isSame3 ||
                                            isSame4 ||
                                            isSame5 ||
                                            isSame6
                                        ? Text("Check the ${labelsConfig["timesLabel"]}",
                                            style: TextStyle(color: Colors.red))
                                        : Container()),
                              ),
                              new Container(
                                  padding: const EdgeInsets.only(
                                      left: 14.0, right: 14.0, bottom: 0.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          new Text("${labelsConfig["medicationInstructionLabel"]}:"),
                                        ],
                                      ),
                                      TextField(
                                          maxLines: 1,
                                          controller:
                                              _textController_instructions,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.fromLTRB(
                                                10.0, 10.0, 10.0, 10.0),
                                          ))
                                    ],
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(height: 6.0),
                      ],
                    ),
                  ),
                ),

                /// Second section end

                new Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 12.0, horizontal: 18.0),
                    child: new Text(labelsConfig["acknowledgmentLabel"])),

                new Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 4.0, horizontal: 4.0),
                      child: Material(
                        borderRadius: BorderRadius.circular(10.0),
                        child: MaterialButton(
                          color: isButtonEnabled
                              ? kinderm8Theme.Colors.appcolour
                              : Colors.grey,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text("SUBMIT",
                                style: TextStyle(
                                  letterSpacing: 1.2,
                                    fontSize: 20.0, color: Colors.white)),
                          ),
                          onPressed: () {
                            print("Button Clicked");
                            if (isButtonEnabled && isWidgethide) {
                              if (lastAdministratedDate != null) {
                                _lastadministrated = new DateTime(
                                    lastAdministratedDate.year,
                                    lastAdministratedDate.month,
                                    lastAdministratedDate.day,
                                    _startTime.hour,
                                    _startTime.minute);
                              }
                              if (!isSame1 &&
                                  !isSame2 &&
                                  !isSame3 &&
                                  !isSame4 &&
                                  !isSame5 &&
                                  !isSame6) {
                                if (submit) {
                                  setState(() {
                                    submit = false;
                                  });
                                  submitMedi();
                                  print("submit");
                                } else {
                                  print("else//");
                                }
                              }
                            } else {
//                              Have to hide the widget done in edit medication
                            }
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void onDisplayUserInfo(User user) {
    print("craete");
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      jwt = widget.jwt.toString();
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];
      fetchCasualattendanceData();
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }
}
