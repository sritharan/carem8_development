import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/medication/medicationlist.dart';
import 'package:carem8/utils/network_util.dart';

class ViewNonPrescribedHistory extends StatefulWidget {
  final singlemedicationData;
  ViewNonPrescribedHistory(this.singlemedicationData);
  @override
  ViewNonPrescribedHistoryState createState() =>
      ViewNonPrescribedHistoryState(singlemedicationData);
}

class ViewNonPrescribedHistoryState extends State<ViewNonPrescribedHistory> {
  final singlemedicationData;

  var report;
  List<Widget> history = [];
  ViewNonPrescribedHistoryState(this.singlemedicationData);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    report = singlemedicationData['report'];
    createReport();
  }

  createReport() {
    if (report.length > 0) {
      for (int i = 0; i < report.length; i++) {
        history.add(Column(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  Text(
                    "Date given",
                    style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(height: 5.0),
                  Container(
                    child: Text(
                      "${report[i]['given_date']}",
                      style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 5.0,
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Text(
                    "Time given",
                    style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(height: 5.0),
                  Container(
                    child: Text(
                      "${report[i]['given_time']}",
                      style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 5.0,
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Text(
                    "Dosage",
                    style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(height: 5.0),
                  Container(
                    child: Text(
                      "${report[i]['dosage']}",
                      style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                children: <Widget>[
                  Text(
                    "Medicine given by",
                    style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(height: 5.0),
                  Container(
                    child: Text(
                      "${report[i]['given_user']}",
                      style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    print(singlemedicationData['report']);
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("View Non-Prescribed Medication"),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Container(
                height: 20.0,
              ),
//

              ///
              /// have to validate

              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 5.0, top: 5.0, bottom: 5.0),
                    child: Text(
                      "Medication to apply/administer :",
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: screenSize.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 16.0),
                            child: Text(
                                "${singlemedicationData['medication_to_administer']}"),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//                      child: Row(
//                        children: <Widget>[
//                          Text("Use my location")
//                        ],
//                      ),
//                    ),
                        ],
                      ),
                      color: Colors.white,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 5.0, top: 5.0, bottom: 5.0),
                    child: Text(
                      "Date authorized :",
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: screenSize.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 16.0),
                            child: Text(
                                "${singlemedicationData['authorized_date']}"),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//                      child: Row(
//                        children: <Widget>[
//                          Text("Use my location")
//                        ],
//                      ),
//                    ),
                        ],
                      ),
                      color: Colors.white,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 5.0, top: 5.0, bottom: 5.0),
                    child: Text(
                      "Give under following circumstance :",
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Container(
                      width: screenSize.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 16.0),
                            child: Text(
                                "${singlemedicationData['circumstances']}"),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
//                      child: Row(
//                        children: <Widget>[
//                          Text("Use my location")
//                        ],
//                      ),
//                    ),
                        ],
                      ),
                      color: Colors.white,
                    ),
                  ),
                ],
              ),

              new Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Card(
                      color: Colors.green,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 5.0, horizontal: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              child: Icon(
                                Icons.local_hospital,
                                size: 60.0,
                                color: Colors.white,
                              ),
                              flex: 1,
                            ),
                            Expanded(
                              child: Text(
                                "History",
                                style: TextStyle(
                                    fontSize: 22.0, color: Colors.white),
                              ),
                              flex: 3,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
//                      Text(singlemedicationData['report'].toString())
//                      new ListView.builder(
//                        itemCount:singlemedicationData['report'].length,
//                        itemBuilder: (context,i){
//                          return new Text(singlemedicationData['report'].toString());
//                        }
//
//                      )
                    ],
                  )



                ],
              )

              ///
              ///
            ],
          ),
        ),
      ),
    );
  }
}
