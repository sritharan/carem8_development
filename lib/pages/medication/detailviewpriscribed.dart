import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/medication/medicationlist.dart';
import 'package:carem8/pages/medication/prescribedhistorydetail.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:carem8/Theme.dart' as Theme;

class ViewPrescribed extends StatefulWidget {
  final singlemedicationData;
  ViewPrescribed(this.singlemedicationData);
  @override
  ViewPrescribedState createState() =>
      ViewPrescribedState(singlemedicationData);
}

class ViewPrescribedState extends State<ViewPrescribed> {
  final singlemedicationData;
  ViewPrescribedState(this.singlemedicationData);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Widget _buildList() {
    return Row(
      children: <Widget>[
        Expanded(
          child: ListView.builder(
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            itemCount: singlemedicationData['repeat_medication_report'].length,
            itemBuilder: (context, int) {
              final medicationReport =
                  singlemedicationData["repeat_medication_report"][int];

              return GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              new PrescribedHistoryDetail(medicationReport)));
                },
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        /* // To_be_administered */
                        Padding(
                          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
                          child: new Text("To be administered :",
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Theme.Colors.app_dark[400],
                                fontWeight: FontWeight.w600,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 2.0),
                          child: Text(
                            "${medicationReport["to_be_administered"]}",
                            style: TextStyle(
                                fontSize: 14.0, color: Theme.Colors.app_dark),
                          ),
                        ),

                        /*// educator_administering */
                        medicationReport["name_of_educator_administering"] !=
                                null
                            ? Padding(
                                padding: const EdgeInsets.only(
                                    top: 2.0, bottom: 2.0),
                                child: new Text("Staff who administered :",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.app_dark[400],
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.left),
                              )
                            : Container(),
                        medicationReport["name_of_educator_administering"] !=
                                null
                            ? Container(
                                padding: EdgeInsets.only(top: 2.0),
                                child: Text(
                                  "${medicationReport["name_of_educator_administering"]}",
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.app_dark),
                                ),
                              )
                            : Container(),
                        /*// Comments */
                        medicationReport["comments"] != null
                            ? Padding(
                                padding: const EdgeInsets.only(
                                    top: 2.0, bottom: 2.0),
                                child: new Text("Further comments :",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.app_dark[400],
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.left),
                              )
                            : Container(),
                        medicationReport["comments"] != null
                            ? Container(
                                padding: EdgeInsets.only(top: 2.0),
                                child: Text(
                                  "${medicationReport["comments"]}",
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.app_dark),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(labelsConfig["viewPrescribedMedicationLabel"]),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 5.0,
              ),

              ///
              /// have to validate
              ///

              singlemedicationData['medication_name'] == ""
                  ? Container()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                          child: Text(
                            labelsConfig["nameOfMedicationLabel"],
                            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 8.0),
                            child: Text(
                                "${singlemedicationData['medication_name']}"),
                          ),
                        ),
                      ],
                    ),

              singlemedicationData['medic_original_pack'] == ""
                  ? Container()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                          child: Text(
                            labelsConfig["originalPackagingLabel"],
                            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 8.0),
                            child: Text(
                                "${singlemedicationData["medic_original_pack"] == 0 ? 'No' : 'Yes'}"),
                          ),
                        ),
                      ],
                    ),

              singlemedicationData['medical'] == ""
                  ? Container()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                          child: Text(
                            "Medical practitioner/chemist :",
                            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 8.0),
                            child: Text("${singlemedicationData['medical']}"),
                          ),
                        ),
                      ],
                    ),

              singlemedicationData['date_prescribed'] == null
                  ? Container()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                          child: Text(
                            "Date prescribed:",
                            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 8.0),
                            child: Text(
                                "${singlemedicationData['date_prescribed']}"),
                          ),
                        ),
                      ],
                    ),

              singlemedicationData['expiry_date'] == null
                  ? Container()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                          child: Text(
                            "Expiry Date:",
                            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 8.0),
                            child:
                                Text("${singlemedicationData['expiry_date']}"),
                          ),
                        ),
                      ],
                    ),

              singlemedicationData['storage'] == ""
                  ? Container()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                          child: Text(
                            "Storage :",
                            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 8.0),
                            child: Text("${singlemedicationData['storage']}"),
                          ),
                        ),
                      ],
                    ),

              singlemedicationData['reason'] == ""
                  ? Container()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                          child: Text(
                            "Reason for medication:",
                            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 8.0),
                            child: Text("${singlemedicationData['reason']}"),
                          ),
                        ),
                      ],
                    ),

              singlemedicationData['instructions'] == ""
                  ? Container()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                          child: Text(
                            "Instructions:",
                            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                          ),
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 8.0),
                            child:
                                Text("${singlemedicationData['instructions']}"),
                          ),
                        ),
                      ],
                    ),

              ///History
              ///
              Card(
                elevation: 0.0,
                color: Colors.deepPurple.shade50,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 1.0, horizontal: 5.0),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.local_hospital,
                        size: 50.0,
                        color: Colors.black,
                      ),
                      SizedBox(width: 10.0),
                      Expanded(
                        child: Text(
                          labelsConfig["medicationHistoryLabel"],
                          style: TextStyle(fontSize: 20.0, color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              Container(child: _buildList())

              ///
              ///
            ],
          ),
        ),
      ),
    );
  }
}
