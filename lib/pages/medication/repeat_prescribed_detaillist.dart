import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/medication/edit_repeat_prescribed.dart';
import 'package:carem8/pages/medication/medicationlist.dart';
import 'package:carem8/pages/medication/prescribedhistorydetail.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:carem8/Theme.dart' as Theme;

class ViewRepeatPrescribed extends StatefulWidget {
  final childData;
  final singlemedicationData;
  final jwt;
  ViewRepeatPrescribed(this.childData, this.singlemedicationData, this.jwt);
  @override
  ViewRepeatPrescribedState createState() =>
      ViewRepeatPrescribedState(childData, singlemedicationData, jwt);
}

class ViewRepeatPrescribedState extends State<ViewRepeatPrescribed>
    implements HomePageContract {
  final singlemedicationData;
  final childData;
  var k, appuser, jwt, id, clientId;
  bool load = false;
  ViewRepeatPrescribedState(
      this.childData, this.singlemedicationData, this.jwt);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void deleteMedication(medi_id) async {
    print('main_medi_id >>> $medi_id');
    setState(() => load = false);
    String deleteUrl =
        'https://apicare.carem8.com/v2.1.1/medication/delete?clientid=$clientId';
    var body = {"id": medi_id, "requested_by": id, "child_id": childData['id']};
    var headers = {"x-authorization": jwt.toString()};
    print(body);
    NetworkUtil _netutil = new NetworkUtil();
    _netutil.delete(deleteUrl, headers: headers, body: body).then((res) {
      print(res);
    });
  }

  Widget _buildList() {
    return Row(children: <Widget>[
      Expanded(
        child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemCount: singlemedicationData['repeat_medication'].length,
          itemBuilder: (context, int) {
            final repeatMedication =
                singlemedicationData["repeat_medication"][int];

            var canEdit;
            if (repeatMedication["canedit"] == true) {
              canEdit = 1;
            } else {
              canEdit = 0;
            }
            var canDelete;
            if (repeatMedication['candelete'] == true) {
              canDelete = 1;
            } else {
              canDelete = 0;
            }

            return Card(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  ///
                  /// First Row
                  ///
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
//
                        /* // To_be_administered */
                        Padding(
                          padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
                          child: new Text("Medication Date :",
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Theme.Colors.app_dark[400],
                                fontWeight: FontWeight.w600,
                              ),
                              textAlign: TextAlign.left),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 2.0),
                          child: Text(
                            "${repeatMedication["main_date"]}",
                            style: TextStyle(
                                fontSize: 14.0, color: Theme.Colors.app_dark),
                          ),
                        ),
//
                        /*// last_administered_datetime */
                        repeatMedication["last_administered_datetime"] != null
                            ? Padding(
                                padding: const EdgeInsets.only(
                                    top: 2.0, bottom: 2.0),
                                child: new Text("Last administered :",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.app_dark[400],
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.left),
                              )
                            : Container(),
                        repeatMedication["last_administered_datetime"] != null
                            ? Container(
                                padding: EdgeInsets.only(top: 2.0),
                                child: Text(
                                  "${repeatMedication["last_administered_datetime"]}",
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.app_dark),
                                ),
                              )
                            : Container(),
//
                        /*// dosage */
                        repeatMedication["dosage"] != null
                            ? Padding(
                                padding: const EdgeInsets.only(
                                    top: 2.0, bottom: 2.0),
                                child: new Text("Dosage of medication :",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.app_dark[400],
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.left),
                              )
                            : Container(),
                        repeatMedication["dosage"] != ""
                            ? Container(
                                padding: EdgeInsets.only(top: 2.0),
                                child: Text(
                                  "${repeatMedication["dosage"]}",
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.app_dark),
                                ),
                              )
                            : Container(),
//
                        /*// manner */
                        repeatMedication["manner"] != null
                            ? Padding(
                                padding: const EdgeInsets.only(
                                    top: 2.0, bottom: 2.0),
                                child: new Text("Method to be administered:",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.app_dark[400],
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.left),
                              )
                            : Container(),
                        repeatMedication["manner"] != ""
                            ? Container(
                                padding: EdgeInsets.only(top: 2.0),
                                child: Text(
                                  "${repeatMedication["manner"]}",
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      color: Theme.Colors.app_dark),
                                ),
                              )
                            : Container(),

                        new Row(
                          children: <Widget>[
                            /*// Acknowledged */
                            repeatMedication["acknowledged_status"] != null
                                ? Padding(
                                    padding: const EdgeInsets.only(
                                        top: 2.0, bottom: 2.0),
                                    child: new Text("Acknowledged : ",
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          color: Theme.Colors.app_dark[400],
                                          fontWeight: FontWeight.w600,
                                        ),
                                        textAlign: TextAlign.left),
                                  )
                                : Container(),
                            repeatMedication["acknowledged_status"] != null
                                ? Container(
                                    padding: EdgeInsets.only(top: 2.0),
                                    child: Text(
                                      "${repeatMedication["acknowledged_status"]}",
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          color:
                                              Theme.Colors.medication_pending),
                                    ),
                                  )
                                : Container(),
                          ],
                        ),
//
//                        new Text("${repeatMedication["medication_status"]}"),

                        // Medication Status
                        repeatMedication["medication_status"] == "Completed"
                            ? new Row(
                                children: <Widget>[
                                  Chip(
                                    backgroundColor:
                                        Theme.Colors.medication_completed,
                                    label: new Text(
                                        repeatMedication["medication_status"],
                                        style: new TextStyle(
                                            fontSize: 15.0,
                                            color: Colors.blueGrey)),
                                    labelPadding: EdgeInsets.all(2.0),
                                  )
                                ],
                              )
                            : repeatMedication["medication_status"] == "Pending"
                                ? new Row(
                                    children: <Widget>[
                                      new Container(
                                        child: new Chip(
                                          backgroundColor: Theme.Colors.medication_pending.withOpacity(0.4),
                                          label: new Text(
                                              "${repeatMedication['medication_status']}",
                                              style: new TextStyle(
                                                  fontSize: 15.0,
                                                  color: Colors.blueGrey)),
                                        ),
                                      )
                                    ],
                                  )
                                : repeatMedication["medication_status"] ==
                                        'Incomplete'
                                    ? new Row(
                                        children: <Widget>[
                                          Chip(
                                            label: new Text(
                                                "${repeatMedication['medication_status']}",
                                                style: new TextStyle(
                                                    fontSize: 15.0,
                                                    color: Colors.blueGrey)),
                                            labelPadding: EdgeInsets.all(2.0),
                                            backgroundColor: Theme
                                                .Colors.medication_acknowledged,
                                          )
                                        ],
                                      )
                                    : Container(),
                      ],
                    ),
                  ),

                  ///
                  ///Next Row
                  ///

                  new Column(
                    children: <Widget>[
                      canEdit == 1
                          ? GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            EditRepeatPrescribed(childData, int,
                                                singlemedicationData, jwt)));
                              },
                              child: Container(
                                margin: const EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
//                                padding: EdgeInsets.all(5.0),
                                child: Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: new Icon(
                                    FontAwesomeIcons.edit,
                                    color: Theme.Colors.app_white,
                                    size: 30.0,
                                  ),
                                ),
                                color: Theme.Colors.appBarGradientEnd,
                              ),
                            )
                          : Container(),

//                       Delete Prescribed
                      /*canDelete == 1
                          ? GestureDetector(
                              onTap: () {
                                showModalBottomSheet<void>(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return Container(
                                          height: 150.0,
                                          child: Padding(
                                            padding: const EdgeInsets.all(32.0),
                                            child: Column(
                                              children: <Widget>[
                                                new Padding(
                                                  padding:
                                                      const EdgeInsets.all(2.0),
                                                  child: new Text(
                                                    "Do you wish to delete",
                                                    style: new TextStyle(
                                                        fontSize: 22.0,
                                                        color: Theme.Colors
                                                            .appsupportlabel,
                                                        fontStyle:
                                                            FontStyle.normal),
                                                  ),
                                                ),
                                                new GestureDetector(
                                                  onTap: () {
                                                    Navigator.of(context).pop();
                                                    deleteMedication(
                                                        singlemedicationData[
                                                            'medication_id']);
                                                  },
                                                  child: new Container(
                                                    margin: new EdgeInsets.only(
                                                        top: 10.0,
                                                        left: 5.0,
                                                        right: 5.0,
                                                        bottom: 10.0),
                                                    height: 30.0,
                                                    decoration: new BoxDecoration(
                                                        color: Theme.Colors
                                                            .buttonicon_deletecolor,
                                                        borderRadius:
                                                            new BorderRadius
                                                                .all(new Radius
                                                                    .circular(
                                                                10.0))),
                                                    child: new Center(
                                                        child: new Text(
                                                      "Delete",
                                                      style: new TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 22.0),
                                                    )),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ));
                                    });
                              },
                              child: Container(
                                margin: const EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                child: Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Icon(
                                    Icons.delete,
                                    color: Theme.Colors.app_white,
                                    size: 30.0,
                                  ),
                                ),
                                color: Theme.Colors.form_requiredicon,
                              ),
                            )
                          : Container(),*/
//
                    ],
                  )
                ],
              ),
            );
          },
        ),
      )
    ]);
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Prescribed Medication History"),
        centerTitle: true,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Container(
                height: 5.0,
              ),
//

              ///
              /// have to validate

              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  singlemedicationData['medication_name'] == ""
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Text(
                                "Name of medication",
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ),
                            Container(
                              width: screenSize.width,
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 16.0),
                                child: Text(
                                    "${singlemedicationData['medication_name']}"),
                              ),
                            ),
                          ],
                        ),
                  singlemedicationData['medic_original_pack'] == ""
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Text(
                                "Medication in original packaging",
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ),
                            Container(
                              width: screenSize.width,
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 16.0),
                                child: Text(
                                    "${singlemedicationData["medic_original_pack"] == 0 ? 'No' : 'Yes'}"),
                              ),
                            ),
                          ],
                        ),
                  singlemedicationData['medical'] == ""
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Text(
                                "Medical practitioner/chemist :",
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ),
                            Container(
                              width: screenSize.width,
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 16.0),
                                child:
                                    Text("${singlemedicationData['medical']}"),
                              ),
                            ),
                          ],
                        ),
                  singlemedicationData['date_prescribed'] == null
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Text(
                                "Date prescribed:",
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ),
                            Container(
                              width: screenSize.width,
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 16.0),
                                child: Text(
                                    "${singlemedicationData['date_prescribed']}"),
                              ),
                            ),
                          ],
                        ),
                  singlemedicationData['expiry_date'] == null
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Text(
                                "Expiry Date:",
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ),
                            Container(
                              width: screenSize.width,
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 16.0),
                                child: Text(
                                    "${singlemedicationData['expiry_date']}"),
                              ),
                            ),
                          ],
                        ),
                  singlemedicationData['storage'] == ""
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Text(
                                "Storage :",
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ),
                            Container(
                              width: screenSize.width,
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 16.0),
                                child:
                                    Text("${singlemedicationData['storage']}"),
                              ),
                            ),
                          ],
                        ),
                  singlemedicationData['reason'] == ""
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Text(
                                "Reason for medication:",
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ),
                            Container(
                              width: screenSize.width,
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 16.0),
                                child:
                                    Text("${singlemedicationData['reason']}"),
                              ),
                            ),
                          ],
                        ),
                  singlemedicationData['instructions'] == ""
                      ? Container()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 5.0),
                              child: Text(
                                "Instructions:",
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ),
                            Container(
                              width: screenSize.width,
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 16.0),
                                child: Text(
                                    "${singlemedicationData['instructions']}"),
                              ),
                            ),
                          ],
                        ),
                  Container(child: _buildList()),
                ],
              )

              ///
              ///
            ],
          ),
        ),
      ),
    );
  }

  @override
  void onDisplayUserInfo(User user) {
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
//      print(appusers);
      jwt = widget.jwt.toString();
//      jwt = appusers["jwt"];
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      id = users["id"];

//      print("iddd $id");
//      print(clientId);

    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }
}
