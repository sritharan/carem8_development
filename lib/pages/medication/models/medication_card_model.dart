import 'package:intl/intl.dart';

class MedicationCardData {
  MedicationCardData ({
    this.dateMonthHeader = "Date",
    this.name = "",
    this.time = "",
    this.dosage,
    this.status,
  });

  String dateMonthHeader;
  final String name;
  final String time;
  final String dosage;
  final int status;


  MedicationCardData.fromJson(Map<String, dynamic> map) :
        name = map['medication_name'],
        time = map['started_on'],
        dosage = map["dosage"],
        status = map["pending_request_count"] {
    if (map["started_on"] != null &&
        map["started_on"] != "") {
      DateTime dateTime = DateTime.parse(map["started_on"]);
      String generatedMonth = DateFormat('MMMM')
          .format(DateTime(dateTime.year, dateTime.month, dateTime.day))
          .substring(0, 3);
      dateMonthHeader = "${dateTime.day} $generatedMonth";
    }
  }

  @override
  String toString() {
    return "dateMonthHeader = $dateMonthHeader, name = $name, started_on = $time dosage = $dosage, pending_request_count= $status";
  }
}
