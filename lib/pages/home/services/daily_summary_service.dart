import 'dart:convert';

import 'package:carem8/pages/home/modals/child_note_modal.dart';
import 'package:carem8/pages/home/modals/child_photos_modal.dart';
import 'package:carem8/pages/home/modals/child_summary_modal.dart';
import 'package:carem8/pages/home/modals/daily_chart_modal.dart';
import 'package:carem8/pages/home/modals/journal_modal.dart';
import 'package:carem8/pages/home/modals/journey_modal.dart';
import 'package:carem8/pages/home/modals/learning_story_modal.dart';
import 'package:carem8/pages/home/modals/medication_modal.dart';
import 'package:http/http.dart' as http;

final domain = "https://apicare.carem8.com";
String userId;
var jwt;

Future<ChildSummaryModal> fetchDailySummaryData(
    {int childId = 32, String clientId = "preproduction", String token}) async {
  String _dailySummaryApiUrl =
      '$domain/v4.4.0/getchildsummary/$childId?clientid=$clientId';
  var headers = {"x-authorization": jwt.toString()};
  var responseData;

  JournalModal journal;
  JourneyModal journey;
  ChildNoteModal childNote;
  DailyChartModal dailyChartModal;
  LearningStoryModal learningStoryModal;
  MedicationModal medicationModal;
  List<ChildPhotoModal> childPhotoModals = [];

  await http.get(_dailySummaryApiUrl, headers: headers).then((response) {
    try {
      responseData = json.decode(response.body);
    } catch (e) {
      print(e);
    }

    if (response.statusCode == 200) {
      if (responseData["journal"] != null) {
        journal = JournalModal.fromJson(responseData["journal"]);
      }

      if (responseData["journey"] != null) {
        journey = JourneyModal.fromJson(responseData["journey"]);
      }

      if (responseData["childnote"] != null) {
        childNote = ChildNoteModal.fromJson(responseData["childnote"]);
      }

      if (responseData["dailychart"] != null) {
        final dailychart = responseData["dailychart"];
        List<DailyChartFoodList> dailyChartFoodList =
        (dailychart["dailyChartFoodList"] != null)
            ? dailychart["dailyChartFoodList"]
            .map<DailyChartFoodList>(
                (element) => DailyChartFoodList.fromJson(element))
            .toList()
            : [];
        List<DailyChartSleepCheckList> dailyChartSleepCheckList =
        (dailychart["dailyChartSleepCheckList"] != null)
            ? dailychart["dailyChartSleepCheckList"]
            .map<DailyChartSleepCheckList>(
                (element) => DailyChartSleepCheckList.fromJson(element))
            .toList()
            : [];
        List<DailyChartNappyList> dailyChartNappyList =
        (dailychart["dailyChartNappyList"] != null)
            ? dailychart["dailyChartNappyList"]
            .map<DailyChartNappyList>(
                (element) => DailyChartNappyList.fromJson(element))
            .toList()
            : [];
        List<DailyChartSunScreenList> dailyChartSunScreenList =
        (dailychart["dailyChartSunScreenList"] != null)
            ? dailychart["dailyChartSunScreenList"]
            .map<DailyChartSunScreenList>(
                (element) => DailyChartSunScreenList.fromJson(element))
            .toList()
            : [];
        List<DailyChartBottleFeedList> dailyChartBottleFeedList =
        (dailychart["dailyChartBottlefeedList"] != null)
            ? dailychart["dailyChartBottlefeedList"]
            .map<DailyChartBottleFeedList>(
                (element) => DailyChartBottleFeedList.fromJson(element))
            .toList()
            : [];
        List<DailyChartRestList> dailyChartRestList =
        (dailychart["dailyChartRestList"] != null)
            ? dailychart["dailyChartRestList"]
            .map<DailyChartRestList>(
                (element) => DailyChartRestList.fromJson(element))
            .toList()
            : [];

        dailyChartModal = DailyChartModal(
          dailyChartFoodList: dailyChartFoodList,
          dailyChartNappyList: dailyChartNappyList,
          dailyChartSunScreenList: dailyChartSunScreenList,
          dailyChartSleepCheckList: dailyChartSleepCheckList,
          dailyChartBottleFeedList: dailyChartBottleFeedList,
          dailyChartRestList: dailyChartRestList,
        );
      }

      if (responseData["learningstory"] != null) {
        learningStoryModal =
            LearningStoryModal.fromJson(responseData["learningstory"]);
      }

      if (responseData["medication"] != null) {
        medicationModal = MedicationModal.fromJson(responseData["medication"]);
      }

      if (responseData["images"] != null) {
        childPhotoModals = responseData["images"]
            .map<ChildPhotoModal>((element) => ChildPhotoModal.fromJson(element))
            .toList();
      }
    } else if (response.statusCode == 500 &&
        responseData["errorType"] == 'ExpiredJwtException') {
      getRefreshToken(childId: childId.toString());
      print("retrying...$childId");
    } else {
      fetchDailySummaryData();
      print(
          'After getRefreshToken fetchDailysummaryData with childid = $childId');
    }
  });

  return ChildSummaryModal(
    childId: childId.toString(),
    userId: userId,
    journal: journal,
    journey: journey,
    childNote: childNote,
    dailyChartModal: dailyChartModal,
    learningStoryModal: learningStoryModal,
    medicationModal: medicationModal,
    childPhotoModals: childPhotoModals,
  );
}

Future getChildren(
    {String childId = "32",
    String userId = "78",
    String clientId = "preproduction",
    var bodyData}) async {

  userId = userId;
  var children;
  await getRefreshToken(childId: childId);
  String url = "$domain/v2.1.1/getinitialload?userid=$userId&clientid=$clientId";

  final dat = {
    "email": "deepika@proitzen.com",
    "password": "ucsc@123",
    "client_id": "preproduction"
  };

  await http.post(url, headers: {"x-authorization": jwt}, body: json.encode(dat))
    .then((response) {
      if (response.statusCode == 200) {
        try {
          children = json.decode(response.body);
        } catch (e) {
          print(e);
        }
      }
  });
  return children;
}

Future getRefreshToken(
    {String userId = "32",
    String clientId = "preproduction",
    String childId}) async {
  String _refreshTokenUrl =
      '$domain/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';
  await http.get(_refreshTokenUrl).then((response) {
    try {
      if (response.statusCode == 200) {
        if (!(response.body is Map)) {
          String refreshJwtToken = json.decode(response.body);
          print('getRefreshToken child id : $refreshJwtToken');
          jwt = refreshJwtToken.toString();
        } else {
          String errorType = json.decode(response.body)["errorType"];
          print("error type $errorType");
        }
      }
    } catch (e) {
      print(e);
    }
  });
}
