import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:carem8/pages/home/dailysummarydatalist.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';

class ChildIndicator extends StatelessWidget {
  final ChildIndicatorViewModel viewModel;

  ChildIndicator({
    this.viewModel,
  });

  @override
  Widget build(BuildContext context) {
    String childName = (viewModel.childs.length > 1)
        ? viewModel.childs[viewModel.activeIndex].firstName
        : "";
    List<ChildPicContainer> childsIndicators = [];
    for (var i = 0; i < viewModel.childs.length; ++i) {
      final child = viewModel.childs[i];

      var percentActive;
      if (i == viewModel.activeIndex) {
        percentActive = 1.0 - viewModel.slidePercent;
        childName = (viewModel.childs.length > 0) ? viewModel.childs[viewModel.activeIndex].firstName : "";
      } else if (i == viewModel.activeIndex - 1 &&
          viewModel.slideDirection == SlideDirection.leftToRight) {
        percentActive = viewModel.slidePercent;
        childName = (viewModel.childs.length > 1) ? viewModel.childs[viewModel.activeIndex - 1].firstName : "";
      } else if (i == viewModel.activeIndex + 1 &&
          viewModel.slideDirection == SlideDirection.rightToLeft) {
        percentActive = viewModel.slidePercent;
        childName = (viewModel.childs.length > 0) ? viewModel.childs[viewModel.activeIndex + 1].firstName : "";
      } else {
        percentActive = 0.0;
      }

      bool isHollow = viewModel.activeIndex != i;

      childsIndicators.add(
        new ChildPicContainer(
          viewModel: new ChildPicContainerViewModel(
            child.iconAssetPath,
            child.color,
            isHollow,
            percentActive,
            i,
          ),
          childName: childName,
        ),
      );
    }

    final bubbleWidth = 80.0;
    final screenWidth = MediaQuery.of(context).size.width;
    final baseTranslation = (screenWidth > 500) ? screenWidth /2.25 : screenWidth / 2.8;
//    (viewModel.childs.length > 3)
//        ? ((viewModel.childs.length * bubbleWidth) / 5.9)
//        : (viewModel.childs.length > 2)
//            ? (viewModel.childs.length * bubbleWidth) / 1.53
//            : ((viewModel.childs.length * bubbleWidth) / 1.1); //changed for diff length
    var translation = (viewModel.childs.length > 3)
        ? (baseTranslation - (bubbleWidth * viewModel.activeIndex)) / 1.1
        : (viewModel.childs.length > 2) ? baseTranslation - (viewModel.activeIndex * bubbleWidth) : baseTranslation - (viewModel.activeIndex * bubbleWidth) / 1.2; //changed for diff length
    if (viewModel.slideDirection == SlideDirection.leftToRight) {
      translation += bubbleWidth * viewModel.slidePercent;
    } else if (viewModel.slideDirection == SlideDirection.rightToLeft) {
      translation -= bubbleWidth * viewModel.slidePercent;
    }
    return Container(
      width: MediaQuery.of(context).size.width - 10.0,
      height: 120.0,
      child: Transform(
        transform: new Matrix4.translationValues(translation, 0.0, 0.0),
        child: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: childsIndicators,
                ),
            ],
          ),
        ],
      ),
      ),
    );
  }
}

enum SlideDirection {
  leftToRight,
  rightToLeft,
  none,
}

class ChildIndicatorViewModel {
  final List<ChildViewModal> childs;
  final int activeIndex;
  final SlideDirection slideDirection;
  final double slidePercent;

  ChildIndicatorViewModel(
    this.childs,
    this.activeIndex,
    this.slideDirection,
    this.slidePercent,
  );
}

class ChildPicContainer extends StatelessWidget {
  final ChildPicContainerViewModel viewModel;
  final String childName;

  ChildPicContainer({
    this.viewModel,
    this.childName,
  });

  @override
  Widget build(BuildContext context) {
    final dailySummaryInheritedWidget = DailySummaryStateContainer.of(context);
    return InkWell(
      onTap: () {
        dailySummaryInheritedWidget.data
            .changeSummaryWhenIndicatorTap(viewModel.containerIdentifier);
//        print(dailySummaryInheritedWidget.data.activeIndex);
//        print(viewModel.containerIdentifier);
      },
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 6.0),
            child: new ClipOval(
              child: new Center(
                child: new Container(
                  width: lerpDouble(60.0, 90.0, viewModel.activePercent),
                  height: lerpDouble(60.0, 90.0, viewModel.activePercent),
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    color: viewModel.isHollow
                        ? Color(0xFFB39DDB)
                            .withAlpha((0x88 * viewModel.activePercent).round())
                        : Color(0xFFB39DDB),
                    border: new Border.all(
                      color: !viewModel.isHollow
                          ? Color(0xFFB39DDB).withAlpha(
                              (0x88 * (1.0 - viewModel.activePercent)).round())
                          : Colors.transparent,
                      width: 3.0,
                    ),
                  ),
                  child: new Opacity(
                    opacity: viewModel.activePercent < 0.7
                        ? 0.7
                        : viewModel.activePercent,
                    child: Container(
                      child: ClipOval(
                        child: FadeInImage(
                          placeholder: AssetImage("assets/nouser.jpg"),
                          image: (viewModel.iconAssetPath != '')
                              ? NetworkImage(
                                  "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${viewModel.iconAssetPath}")
                              : AssetImage("assets/nouser.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 6.0),
            child: Opacity(
              opacity: viewModel.activePercent,
              child:Text("$childName",
                    style: TextStyle(
                        color: Color(0xFFB39DDB), fontSize: 15.0)
                ),
            ),
          ),
        ],
      ),
    );
  }
}

class ChildPicContainerViewModel {
  final String iconAssetPath;
  final Color color;
  final bool isHollow;
  final double activePercent;
  final int containerIdentifier;

  ChildPicContainerViewModel(
    this.iconAssetPath,
    this.color,
    this.isHollow,
    this.activePercent,
    this.containerIdentifier,
  );
}
