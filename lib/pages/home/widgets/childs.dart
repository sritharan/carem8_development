import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/widgets/child_indicator.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:carem8/pages/home/modals/summary_view_modal.dart';
import 'package:carem8/pages/home/widgets/horizontal_photo_list.dart';
import 'package:carem8/pages/home/widgets/page_dragger.dart';
import 'package:carem8/pages/home/widgets/daily_summary_card.dart';
import 'package:flutter/material.dart';

class Child extends StatefulWidget {
  final ChildViewModal viewModel;
  final double percentVisible;
  final SlideDirection slideDirection;
  final SlideUpdate slideUpdate;

  Child({
    this.viewModel,
    this.slideDirection,
    this.slideUpdate,
    this.percentVisible = 1.0,
  });

  @override
  _ChildState createState() => _ChildState();
}

class _ChildState extends State<Child> {
  double animatePercent = 0.0;
  List<SummaryViewModal> childSummary = [];
  void doAnimation() {
    if (widget.slideDirection == SlideDirection.leftToRight) {
      setState(() {
        animatePercent = 40.0 * (1.0 - widget.percentVisible);
      });
    } else if (widget.slideDirection == SlideDirection.rightToLeft) {
      setState(() {
        animatePercent = -40.0 * (1.0 - widget.percentVisible);
      });
    } else {
      setState(() {
        animatePercent = 0.0;
      });
    }
  }

  @override
  void initState() {
    childSummary = [
      SummaryViewModal(
        assetPath: "assets/iconsetpng/cubes.png",
        type: labelsConfig["dailyJournalsLabel"],
        childSummaryModal: widget.viewModel.childSummaryVM,
        function: widget.viewModel.navigateFunctions[0],
      ),
      SummaryViewModal(
        assetPath: "assets/iconsetpng/game.png",
        type: labelsConfig["observationLabel"],
        childSummaryModal: widget.viewModel.childSummaryVM,
        function: widget.viewModel.navigateFunctions[1],),
      SummaryViewModal(
        assetPath: "assets/iconsetpng/notepad_dailychart.png",
        type: labelsConfig["dailyChartLabel"],
        childSummaryModal: widget.viewModel.childSummaryVM,
        function: widget.viewModel.navigateFunctions[2],),
      SummaryViewModal(
        assetPath: "assets/iconsetpng/user.png",
        type: labelsConfig["noteToCenterLabel"],
        childSummaryModal: widget.viewModel.childSummaryVM,
        function: widget.viewModel.navigateFunctions[3],),
      SummaryViewModal(
        assetPath: "assets/iconsetpng/abacus.png",
        type: labelsConfig["learningStoryLabel"],
        childSummaryModal: widget.viewModel.childSummaryVM,
        function: widget.viewModel.navigateFunctions[4],),
      SummaryViewModal(
        assetPath: "assets/iconsetpng/first-aid-kit.png",
        type: labelsConfig["medicationLabel"],
        childSummaryModal: widget.viewModel.childSummaryVM,
        function: widget.viewModel.navigateFunctions[5],),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //TODO: get the required child only and send the required data to summaryCard
    childSummary.forEach((item) {
      item.childSummaryModal = widget.viewModel.childSummaryVM;
    });
    doAnimation();
    return new Container(
        width: double.infinity,
        child: new Opacity(
          opacity: widget.percentVisible,
          child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new Transform(
                  transform:
                      new Matrix4.translationValues(animatePercent, 0.0, 0.0),
                  child: InkWell(
                    onTap: () {
                      widget.viewModel.navigateFunctions[0]();
                    },
                    child: DailySummaryCard(
                      summary: childSummary[0],
                      opacityValue: widget.percentVisible,
                    ),
                  ),
                ),
                new Transform(
                  transform:
                      new Matrix4.translationValues(animatePercent, 0.0, 0.0),
                  child: InkWell(
                    onTap: () {
                      widget.viewModel.navigateFunctions[1]();
                    },
                    child: DailySummaryCard(
                      summary: childSummary[1],
                      opacityValue: widget.percentVisible,
                    ),
                  ),
                ),
                new Transform(
                  transform:
                      new Matrix4.translationValues(animatePercent, 0.0, 0.0),
                  child: InkWell(
                    onTap: () {
                      widget.viewModel.navigateFunctions[2]();
                    },
                    child: DailySummaryCard(
                      summary: childSummary[2],
                      opacityValue: widget.percentVisible,
                      isDailyChart: true,
                    ),
                  ),
                ),
                new Transform(
                  transform:
                      new Matrix4.translationValues(animatePercent, 0.0, 0.0),
                  child: InkWell(
                    onTap: () {
                      widget.viewModel.navigateFunctions[3]();
                    },
                    child: DailySummaryCard(
                      summary: childSummary[3],
                      opacityValue: widget.percentVisible,
                    ),
                  ),
                ),
                new Transform(
                  transform:
                      new Matrix4.translationValues(animatePercent, 0.0, 0.0),
                  child: InkWell(
                    onTap: () {
                      widget.viewModel.navigateFunctions[4]();
                    },
                    child: DailySummaryCard(
                      summary: childSummary[4],
                      opacityValue: widget.percentVisible,
                    ),
                  ),
                ),
                new Transform(
                  transform:
                  new Matrix4.translationValues(animatePercent, 0.0, 0.0),
                  child: InkWell(
                    onTap: () {
                      widget.viewModel.navigateFunctions[5]();
                    },
                    child: DailySummaryCard(
                      summary: childSummary[5],
                      opacityValue: widget.percentVisible,
                    ),
                  ),
                ),
                (widget.viewModel.childSummaryVM != null && widget.viewModel.childSummaryVM.childPhotoModals != null && widget.viewModel.childSummaryVM.childPhotoModals.length > 0) ? new Transform(
                  transform:
                      new Matrix4.translationValues(animatePercent, 0.0, 0.0),
                  child: InkWell(onTap: () {
                    widget.viewModel.navigateFunctions[6]();
                  }, child: PhotoHorizontalList(images: widget.viewModel.childSummaryVM?.childPhotoModals,)),
                ) : SizedBox(),
              ]),
        ));
  }
}