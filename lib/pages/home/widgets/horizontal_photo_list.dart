import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/modals/child_photos_modal.dart';
import 'package:flutter/material.dart';
import 'package:carem8/utils/commonutils/zoomable_image_page_journal.dart';

class PhotoHorizontalList extends StatelessWidget {
  final List<ChildPhotoModal> images;
  PhotoHorizontalList({this.images});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 6.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Row(
              children: <Widget>[
                Image.asset(
                  "assets/iconsetpng/image.png",
                  width: 20.0,
                  height: 20.0,
                ),
                SizedBox(
                  width: 6.0,
                ),
                Expanded(child: Text("Photos", style: TextStyle(fontSize: 18.0))),
                Text(labelsConfig["galleryLabel"], style: TextStyle(fontSize: 16.0, color: Colors.black87)),
                Icon(Icons.arrow_right, size: 24.0, color: Colors.black87),
              ],
            ),
          ),
          Container(
            height: 90.0,
            padding: EdgeInsets.only(left: 2.0),
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: (images != null) ? images.length : 0,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) =>
                              new ZoomableImagePage_Journal(images, image: images[index].fileUrl)),
                      );
                    },
                    child: Container(
                        width: 85.0,
                        height: 90.0,
                        margin: const EdgeInsets.all(5.0),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.purple),
                          borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(6.0)),
                          child: FadeInImage(
                            fit: BoxFit.cover,
                            width: 80.0,
                            height: 80.0,
                            placeholder: AssetImage("assets/nophoto.jpg"),
                            image: NetworkImage(images[index].fileUrl),
                          ),
                        )
                    ),
                  );
                }
            ),
          ),
        ],
      ),
    );
  }
}