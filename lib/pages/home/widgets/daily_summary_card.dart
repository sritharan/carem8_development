import 'package:flutter/material.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:flutter_html/flutter_html.dart';
import 'package:carem8/pages/home/modals/summary_view_modal.dart';
import 'package:carem8/pages/home/widgets/summary_stepper.dart';

class DailySummaryCard extends StatelessWidget {
  final SummaryViewModal summary;
  final double opacityValue;
  final bool isDailyChart;

  DailySummaryCard(
      {this.summary, this.opacityValue, this.isDailyChart = false});

  Widget _buildCardDataWidgets(
      {String type,
      String title,
      String subtitle,
      bool isDailyChart = false,
      bool isHtml = false,
      BuildContext context}) {
    Pattern pattern = r'^(([^<>()[\];:\s@\"])+(\.[^<>()[\];:\s@\"]+))';
    RegExp regex = new RegExp(pattern);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 6.0),
      child: Column(
        children: <Widget>[
          Card(
            elevation: 0.5,
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(right: 8.0, top: 3.0),
                        child: (summary.assetPath != null)
                            ? Image.asset(
                                summary.assetPath,
                                width: 40.0, //60
                                height: 40.0,
                              )
                            : SizedBox(),
                      ),
                      SizedBox(width: 10.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Opacity(
                              opacity: opacityValue,
                              child:
                                  Text(type, style: TextStyle(fontSize: 18.0))),
                          SizedBox(height: 1.3),
                          !isDailyChart
                              ? (title != "" && title != null)
                                  ? Opacity(
                                      opacity: opacityValue,
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                1.4,
                                        child: isHtml
                                            ? Html(
                                                data: html2md.convert(title),
                                                defaultTextStyle: TextStyle(
                                                    fontSize: 14.0,
                                                    color: Colors.grey,
                                                    fontWeight:
                                                        FontWeight.w400))
                                            : Text(title,
                                                style: TextStyle(
                                                    fontSize: 14.0,
                                                    color: Colors.grey,
                                                    fontWeight:
                                                        FontWeight.w400)),
                                      ),
                                    )
                                  : SizedBox()
                              : SizedBox(),
                          SizedBox(height: 1.0),
                          !isDailyChart
                              ? Opacity(
                                  opacity: opacityValue,
                                  child: (subtitle != "" && subtitle != null)
                                      ? (isHtml) // && regex.hasMatch(subtitle))
                                          ? SizedBox(
                                              width: MediaQuery.of(context).size.width / 1.42,
                                              height: (subtitle.length < 100) ? 20.0 : (subtitle.length < 200) ? 46.0 : 77.0,
                                              child: Html(
                                                  data: html2md
                                                      .convert(subtitle), defaultTextStyle: TextStyle(
                                                  fontSize: 13.0),))
                                          : (subtitle != "" && subtitle != null)
                                              ? SizedBox(
                                                  width: MediaQuery.of(context).size.width / 1.42,
                                                  child: Text(
                                                    subtitle,
                                                    maxLines: 5,
                                                    overflow: TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                        fontSize: 13.0),
                                                  ),
                                                )
                                              : SizedBox()
                                      : SizedBox(),
                                )
                              : SizedBox(),
                          Opacity(
                            opacity: opacityValue,
                            child: isDailyChart
                                ? (summary.childSummaryModal.dailyChartModal !=
                                        null)
                                    ? SummaryStepper(
                                        dailyChartModal: summary
                                            .childSummaryModal.dailyChartModal)
                                    : SizedBox()
                                : SizedBox(),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _checkAndBuildCard(BuildContext context) {
    switch (summary.type) {
      case "Daily Journals":
        if (summary.childSummaryModal?.journal != null)
          return _buildCardDataWidgets(
              type: summary.type,
              title: summary.childSummaryModal.journal.title,
              subtitle: summary.childSummaryModal.journal.whatHappened,
              isHtml: true,
              context: context);
        else
          return SizedBox();
        break;
      case "Observations":
        if (summary.childSummaryModal?.journey != null)
          return _buildCardDataWidgets(
            type: summary.type,
            title: summary.childSummaryModal.journey.title,
            subtitle: summary.childSummaryModal.journey.observation,
            isHtml: true,
            context: context,
          );
        else
          return SizedBox();
        break;
      case "Daily Chart":
        if (summary.childSummaryModal?.dailyChartModal != null) {
          if (summary.childSummaryModal.dailyChartModal.dailyChartSleepCheckList.length > 0 ||
              summary.childSummaryModal.dailyChartModal.dailyChartFoodList
                      .length >
                  0 ||
              summary.childSummaryModal.dailyChartModal.dailyChartRestList
                      .length >
                  0 ||
              summary.childSummaryModal.dailyChartModal.dailyChartBottleFeedList
                      .length >
                  0 ||
              summary.childSummaryModal.dailyChartModal.dailyChartSunScreenList
                      .length >
                  0 ||
              summary.childSummaryModal.dailyChartModal.dailyChartNappyList
                      .length >
                  0)
            return _buildCardDataWidgets(
                type: summary.type,
                title: "",
                subtitle: "",
                isDailyChart: true,
                context: context);
          else
            return SizedBox();
        } else {
          return SizedBox();
        }
        break;
      case "Note To Center":
        if (summary.childSummaryModal?.childNote != null)
          return _buildCardDataWidgets(
              type: summary.type,
              title: summary.childSummaryModal.childNote.notes,
              subtitle: summary.childSummaryModal.childNote.username ?? "",
              context: context);
        else
          return SizedBox();
        break;
      case "Learning Story":
        if (summary.childSummaryModal?.learningStoryModal != null) {
          String educatorName =
              summary.childSummaryModal.learningStoryModal.educatorFullName;
          return _buildCardDataWidgets(
              type: summary.type,
              title: summary.childSummaryModal.learningStoryModal.title,
              subtitle: (educatorName != null) ? "Educator: $educatorName" : "",
              context: context);
        } else
          return SizedBox();
        break;
      case "Medications":
        if (summary.childSummaryModal?.medicationModal != null)
          return _buildCardDataWidgets(
              type: summary.type,
              title: summary.childSummaryModal.medicationModal.name,
              subtitle:
                (summary.childSummaryModal.medicationModal.requestStatus == "0") ? "Pending" : "Ongoing",
              context: context);
        else
          return SizedBox();
        break;
      default:
        return SizedBox();
    }
  }

  @override
  Widget build(BuildContext context) {
    return _checkAndBuildCard(context);
  }
}
