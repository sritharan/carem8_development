import 'package:carem8/data/database_helper.dart';
import 'package:carem8/models/user.dart';

abstract class HomePageContract {
  void onDisplayUserInfo(User user);
  void onErrorUserInfo();
  void onLogoutUser();

//  void onUpdateJwt();
}

class HomePagePresenter {
  HomePageContract _view;
  DatabaseHelper databaseHelper = DatabaseHelper();

  HomePagePresenter(this._view);

  getUserInfo() {
    databaseHelper.getFirstUser().then((User user) {
      _view.onDisplayUserInfo(user);
    }).catchError((error, StackTrace t) {
      print('getUserInfo $error');
      _view.onErrorUserInfo();
    });
  }

  getUserLogout() {
    databaseHelper.deleteUser().then((res) {
      print(res);
      _view.onLogoutUser();
    }).catchError((error, StackTrace t) {
      _view.onErrorUserInfo();
    });
  }
// working bt not updating jwt

//  updateJwt(String jwt) {
//    databaseHelper.updateJwt(jwt).then((res) {
//      print(res);
//      _view.onUpdateJwt();
//    });
//  }
}
