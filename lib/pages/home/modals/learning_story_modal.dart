class LearningStoryModal {
  final int storyId;
  final int educatorId;
  final String educatorFullName;
  final String educatorUsername;
  final String educatorEmail;
  final String isStuff;
  final String title;
  final String publishedDate;
  final String date;
  final int commentCount;
  final int unreadCommentCount;
  final String createdAt;

  LearningStoryModal({
    this.storyId,
    this.educatorId,
    this.educatorFullName,
    this.educatorUsername,
    this.educatorEmail,
    this.isStuff,
    this.title,
    this.publishedDate,
    this.date,
    this.commentCount,
    this.unreadCommentCount,
    this.createdAt,
  });

  LearningStoryModal.fromJson(Map<String, dynamic> map)
      : storyId = map["id"],
        educatorId = map["educator"]["id"],
        educatorFullName = map["educator"]["fullname"],
        educatorUsername = map["educator"]["username"],
        educatorEmail = map["educator"]["email"],
        isStuff = map["educator"]["isStaff"],
        title = map["title"],
        publishedDate = map["published_date"],
        date = map["date"],
        commentCount = map["comment_count"],
        unreadCommentCount = map["unread_comment_count"],
        createdAt = map["created_at"];
}
