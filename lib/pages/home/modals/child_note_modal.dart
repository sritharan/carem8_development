class ChildNoteModal {
  final int childNoteId;
  final int parentId;
  final String notes;
  final String center;
  final String fullName;
  final String username;
  final String createdAt;

  ChildNoteModal({
    this.childNoteId,
    this.parentId,
    this.notes,
    this.center,
    this.fullName,
    this.username,
    this.createdAt,
  });

  ChildNoteModal.fromJson(Map<String, dynamic> map)
      : childNoteId = map["id"],
        parentId = map["parent_id"]["id"],
        center = map["parent_id"]["center"],
        fullName = map["parent_id"]["fullname"],
        username = map["parent_id"]["username"],
        notes = map["notes"],
        createdAt = map["created_at"];
}