class DailyChartModal {
  final List<DailyChartFoodList> dailyChartFoodList;
  final List<DailyChartSleepCheckList> dailyChartSleepCheckList;
  final List<DailyChartNappyList> dailyChartNappyList;
  final List<DailyChartSunScreenList> dailyChartSunScreenList;
  final List<DailyChartBottleFeedList> dailyChartBottleFeedList;
  final List<DailyChartRestList> dailyChartRestList;

  DailyChartModal({
    this.dailyChartFoodList,
    this.dailyChartSleepCheckList,
    this.dailyChartNappyList,
    this.dailyChartSunScreenList,
    this.dailyChartBottleFeedList,
    this.dailyChartRestList,
  });

  DailyChartModal.fromJson(Map<String, dynamic> map)
      : dailyChartFoodList = map["dailychart"]["dailyChartFoodList"],
        dailyChartSleepCheckList =
            map["dailychart"]["dailyChartSleepCheckList"],
        dailyChartNappyList = map["dailychart"]["dailyChartNappyList"],
        dailyChartSunScreenList = map["dailychart"]["dailyChartSunScreenList"],
        dailyChartBottleFeedList =
            map["dailychart"]["dailyChartBottlefeedList"],
        dailyChartRestList = map["dailychart"]["dailyChartRestList"];
}

class DailyChartRestList {
  final int id;
  final int childProfileId;
  final int roomId;
  final String fullName;
  final String username;
  final String date;
  final String createdAt;
  final String startTime;
  final String endTime;
  final String restType;
  final String trainer;

  DailyChartRestList({
    this.id,
    this.childProfileId,
    this.roomId,
    this.fullName,
    this.username,
    this.date,
    this.createdAt,
    this.startTime,
    this.endTime,
    this.restType,
    this.trainer,
  });

  DailyChartRestList.fromJson(Map<String, dynamic> map)
      : id = map["id"],
        childProfileId = map["child_profile_id"],
        roomId = map["room_id"],
        date = map["date"],
        fullName = map["created_by"]["fullname"],
        username = map["created_by"]["username"],
        createdAt = map["created_by"]["created_at"],
        startTime = map["dailyRestTimeList"][0]["start_time"],
        endTime = map["dailyRestTimeList"][0]["end_time"],
        restType = map["dailyRestTimeList"][0]["resttype"],
        trainer = map["dailyRestTimeList"][0]["trainer"];
}

class DailyChartBottleFeedList {
  final int id;
  final int childProfileId;
  final int roomId;
  final String fullName;
  final String username;
  final String date;
  final String createdAt;
  final String bottleType;
  final String bottleQty;
  final String bottleTime;
  final String trainer;

  DailyChartBottleFeedList({
    this.id,
    this.childProfileId,
    this.roomId,
    this.fullName,
    this.username,
    this.date,
    this.createdAt,
    this.bottleType,
    this.bottleQty,
    this.bottleTime,
    this.trainer,
  });

  DailyChartBottleFeedList.fromJson(Map<String, dynamic> map)
      : id = map["id"],
        childProfileId = map["child_profile_id"],
        roomId = map["room_id"],
        date = map["date"],
        fullName = map["created_by"]["fullname"],
        username = map["created_by"]["username"],
        createdAt = map["created_by"]["created_at"],
        bottleType = map["dailyChartBottlefeedList"][0]["bottletype"],
        bottleQty = map["dailyChartBottlefeedList"][0]["bottle_qty"],
        bottleTime = map["dailyChartBottlefeedList"][0]["bottle_time"],
        trainer = map["dailyChartBottlefeedList"][0]["trainer"];
}

class DailyChartSunScreenList {
  final int id;
  final int childProfileId;
  final int roomId;
  final int createdBy;
  final String date;
  final int categoryId;
  final String categoryName;
  final int educatorId;
  final String educatorName;
  final bool type;

  DailyChartSunScreenList({
    this.id,
    this.childProfileId,
    this.roomId,
    this.createdBy,
    this.date,
    this.categoryId,
    this.categoryName,
    this.educatorId,
    this.educatorName,
    this.type,
  });

  DailyChartSunScreenList.fromJson(Map<String, dynamic> map)
      : id = map["id"],
        childProfileId = map["child_profile_id"],
        roomId = map["room_id"],
        date = map["date"],
        createdBy = map["created_by"],
        categoryId = map["category_id"],
        categoryName = map["category_name"],
        educatorId = map["educator_id"],
        educatorName = map["educator_name"],
        type = map["type"];
}

class DailyChartNappyList {
  final int id;
  final int childProfileId;
  final int roomId;
  final String fullName;
  final String username;
  final String createdAt;
  final String date;
  final String trainer;
  final String trainingType;
  final String time;
  final String catagory;
  final String note;

  DailyChartNappyList({
    this.id,
    this.childProfileId,
    this.roomId,
    this.fullName,
    this.username,
    this.createdAt,
    this.date,
    this.trainer,
    this.trainingType,
    this.time,
    this.catagory,
    this.note,
  });

  DailyChartNappyList.fromJson(Map<String, dynamic> map)
      : id = map["id"],
        childProfileId = map["child_profile_id"],
        roomId = map["room_id"],
        date = map["date"],
        fullName = map["created_by"]["fullname"],
        username = map["created_by"]["username"],
        createdAt = map["created_by"]["created_at"],
        trainer = map["dailyChartNappyList"][0]["trainer"],
        trainingType = map["dailyChartNappyList"][0]["trainingtype"],
        time = map["dailyChartNappyList"][0]["time"],
        catagory = map["dailyChartNappyList"][0]["catagory"],
        note = map["dailyChartNappyList"][0]["note"];
}

class DailyChartSleepCheckList {
  final int id;
  final int childProfileId;
  final int roomId;
  final String fullName;
  final String username;
  final String createdAt;
  final String onSleep;
  final String date;
  final String trainer;
  final String time;

  DailyChartSleepCheckList({
    this.id,
    this.childProfileId,
    this.roomId,
    this.fullName,
    this.username,
    this.createdAt,
    this.onSleep,
    this.date,
    this.trainer,
    this.time,
  });

  DailyChartSleepCheckList.fromJson(Map<String, dynamic> map)
      : id = map["id"],
        childProfileId = map["child_profile_id"],
        roomId = map["room_id"],
        date = map["date"],
        fullName = map["created_by"]["fullname"],
        username = map["created_by"]["username"],
        createdAt = map["created_by"]["created_at"],
        onSleep = map["onsleep"],
        trainer = map["dailyChartSleepCheckList"][0]["trainer"],
        time = map["dailyChartSleepCheckList"][0]["time"];
}

class DailyChartFoodList {
  final int id;
  final int childProfileId;
  final int roomId;
  final String fullName;
  final String username;
  final String createdAt;
  final int menuId;
  final int menuWeek;
  final String date;
  final String menuCategory;
  final String serveType;
  final String foodMenu;

  DailyChartFoodList({
    this.id,
    this.childProfileId,
    this.roomId,
    this.fullName,
    this.username,
    this.createdAt,
    this.menuId,
    this.menuWeek,
    this.date,
    this.menuCategory,
    this.serveType,
    this.foodMenu,
  });

  DailyChartFoodList.fromJson(Map<String, dynamic> map)
      : id = map["id"],
        childProfileId = map["child_profile_id"],
        roomId = map["room_id"],
        date = map["date"],
        fullName = map["created_by"]["fullname"],
        username = map["created_by"]["username"],
        createdAt = map["created_by"]["created_at"],
        menuId = map["menu_id"],
        menuWeek = map["menu_week"],
        menuCategory = map["dailychartfoodlist"][0]["MenuCategory"],
        serveType = map["dailychartfoodlist"][0]["Servetype"],
        foodMenu = map["dailychartfoodlist"][0]["FoodMenu"];
}
