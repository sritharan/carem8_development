import 'package:carem8/pages/home/modals/child_note_modal.dart';
import 'package:carem8/pages/home/modals/child_photos_modal.dart';
import 'package:carem8/pages/home/modals/daily_chart_modal.dart';
import 'package:carem8/pages/home/modals/journal_modal.dart';
import 'package:carem8/pages/home/modals/journey_modal.dart';
import 'package:carem8/pages/home/modals/learning_story_modal.dart';
import 'package:carem8/pages/home/modals/medication_modal.dart';

class ChildSummaryModal {
  final String childId;
  final String userId;
  final JournalModal journal;
  final JourneyModal journey;
  final ChildNoteModal childNote;
  final DailyChartModal dailyChartModal;
  final LearningStoryModal learningStoryModal;
  final MedicationModal medicationModal;
  final List<ChildPhotoModal> childPhotoModals;

  ChildSummaryModal({
    this.childId,
    this.userId,
    this.journal,
    this.journey,
    this.childNote,
    this.dailyChartModal,
    this.learningStoryModal,
    this.medicationModal,
    this.childPhotoModals,
  });
}
