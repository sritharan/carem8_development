class JournalModal {
  final int journalId;
  final int staffX;
  final String title;
  final String whatHappened;
  final String date;
  final int postType;
  final int commentCount;
  final int unreadCommentCount;
  final String createdAt;

  JournalModal({
    this.journalId,
    this.staffX,
    this.title,
    this.whatHappened,
    this.date,
    this.postType,
    this.commentCount,
    this.unreadCommentCount,
    this.createdAt,
  });

  JournalModal.fromJson(Map<String, dynamic> map) :
    journalId = map["id"],
    staffX = map["staffx"],
    title = map["journal_title"],
    whatHappened = map["whathappened"],
    date = map["date"],
    postType = map["posttype"],
    commentCount = map["comment_count"],
    unreadCommentCount = map["unread_comment_count"],
    createdAt = map["created_at"];
}