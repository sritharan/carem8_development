class MedicationModal {
  final int medicationId;
  final String name;
  final String datePrescribed;
  final String requestStatus;
  final String endingOn;
  final String createdAt;

  MedicationModal({
    this.medicationId,
    this.name,
    this.datePrescribed,
    this.requestStatus,
    this.endingOn,
    this.createdAt,
  });

  MedicationModal.fromJson(Map<String, dynamic> map)
      : medicationId = map["id"],
        name = map["medication_name"],
        datePrescribed = map["date_prescribed"],
        requestStatus = map["request_status"],
        endingOn = map["ending_on"],
        createdAt = map["created_at"];
}
