import 'package:carem8/pages/home/modals/child_summary_modal.dart';
import 'package:flutter/material.dart';

class ChildViewModal {
  final int id;
  final Color color;
  final String iconAssetPath;
  final String firstName;
  final String lastName;
  final String dob;
  final String attendance;
  final String gender;
  final room;
  ChildSummaryModal childSummaryVM;
  List<Function> navigateFunctions;

  ChildViewModal({
    this.id,
    this.color,
    this.iconAssetPath,
    this.firstName,
    this.lastName,
    this.dob,
    this.attendance,
    this.gender,
    this.room,
    this.childSummaryVM,
    this.navigateFunctions,
  });

  @override
  String toString() =>
      "Id: $id, FirstName: $firstName, LastName: $lastName, IconPath: $iconAssetPath";
}
