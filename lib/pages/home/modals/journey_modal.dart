class JourneyModal {
  int childId;
  final int journeyId;
  final String title;
  final String observation;
  final String date;
  final int postType;
  final String createdAt;

  JourneyModal({
    this.childId,
    this.journeyId,
    this.title,
    this.observation,
    this.date,
    this.postType,
    this.createdAt,
  });

  JourneyModal.fromJson(Map<String, dynamic> map)
      : journeyId = map["id"],
        title = map["journey_title"],
        observation = map["observation"],
        date = map["date"],
        postType = map["posttype"],
        createdAt = map["created_at"];
}