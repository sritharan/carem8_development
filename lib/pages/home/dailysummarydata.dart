import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
//import 'package:photo_view/photo_view.dart';

class DailysummaryData extends StatelessWidget {
  final data;
  int i;
  int childid;
  DailysummaryData(this.data, this.i, this.childid);

  @override
  Widget build(BuildContext context) {
    var parsedDate = DateTime.parse(data["created_at"]);
    var date = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);

    print('dailysummarydata >> data: $data');
    return new Center(
        child: Column(
      children: <Widget>[
        new Text(data),
//            new Container(
//              child: new SideHeaderListView(
//                itemCount: data.length,
//                padding: new EdgeInsets.all(16.0),
//                itemExtend: 80.0,
//                headerBuilder: (BuildContext context, int index) {
//                  return new SizedBox(width: 32.0,child: new Text(data[index]['title']));
//                },
//                itemBuilder: (BuildContext context, int index) {
//                  return new Text(data[index]['title'], style: Theme.of(context).textTheme.headline,);
//                },
//                hasSameHeader: (int a, int b) {
//                  return data[a] == data[b];
//                },
//              ),
//            ),
//            new Padding(padding: EdgeInsets.only(top:100.0)),
        new ListTile(
//              leading: sender["image"] != null
//                  ? CircleAvatar(
//                backgroundImage: NetworkImage(sender["image"], scale: 10.0),
//                radius: 25.0,
//              )
//                  : CircleAvatar(
//                backgroundImage: AssetImage("assets/nophoto.jpg"),
//                radius: 25.0,
//              ),
          title: new Text(
            data['title'],
            style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 14.0),
          ),
          subtitle: new Text(
            data['description'],
            style: new TextStyle(fontWeight: FontWeight.w500, fontSize: 10.0),
          ),
          onTap: () {
            print("Have to navigate to ${data["navigator"]}");
          },
        ),
        Divider(),
      ],
    ));
  }
}
