import 'dart:convert';
import 'package:meta/meta.dart';

class DailysummaryUI {
  DailysummaryUI({

    // No data state
    @required this.nodata,
    @required this.empty_state_message,
    // Journal
    @required this.id,
    @required this.journal_title,
    @required this.whathappeneddata,
    // Observation
    @required this.journey_title,
    @required this.observationdata,
    //DailyChartFoodList
    @required this.MenuCategory,
    @required this.FoodMenu,
    @required this.Servetype,
    //DailyChartNappyList
    @required this.catagory,
    @required this.nappytrainer,
    @required this.nappytime,
    //DailyChartBottlefeedList
    @required this.bottle_qty,
    @required this.bottletype,
    @required this.bottletrainer,
    @required this.bottle_time,
    //DailyChartSleepCheckList
    @required this.sleeptime,
    @required this.sleepchecktrainer,
    //Childnote
    @required this.notes,
    //Learningstory
    @required this.learningstorytitle,
    @required this.educator,
    @required this.date,
    //Medication
    @required this.medicationtitle,
    //Gallery
    @required this.album,
    @required this.caption,
  });

    // No data state
    final String nodata;
    final String empty_state_message;
    // Journal
    final String id;
    final String journal_title;
    final String whathappeneddata;
    // Observation
    final String journey_title;
    final String observationdata;
    //DailyChartFoodList
    final String MenuCategory;
    final String FoodMenu;
    final String Servetype;
    //DailyChartNappyList
    final String catagory;
    final String nappytrainer;
    final String nappytime;
    //DailyChartBottlefeedList
    final String bottle_qty;
    final String bottletype;
    final String bottletrainer;
    final String bottle_time;
    //DailyChartSleepCheckList
    final String sleeptime;
    final String sleepchecktrainer;
    //Childnote
    final String notes;
    //Learningstory
    final String learningstorytitle;
    final String educator;
    final String date;
    //Medication
    final String medicationtitle;
    //Gallery
    final String album;
    final String caption;

  static List<DailysummaryUI> allFromResponse(String response) {
    var decodedJson = json.decode(response)();
    print('decode ->>> $decodedJson');
    return decodedJson['results']
        .cast<Map<String, dynamic>>()
        .map((obj) => DailysummaryUI.fromJson(obj))
        .toList()
        .cast<DailysummaryUI>();
  }

  static DailysummaryUI fromMap(Map map) {

    return new DailysummaryUI(

      // No data state
      nodata: map['nodata'],
      empty_state_message: map['empty_state_message'],
      // Journal
      id: map['id'],
      journal_title: map['journal_title'],
      whathappeneddata: map['whathappeneddata'],
      // Observation
      journey_title: map['journey_title'],
      observationdata: map['observationdata'],
      //DailyChartFoodList
      MenuCategory: map['MenuCategory'],
      FoodMenu: map['FoodMenu'],
      Servetype: map['Servetype'],
      //DailyChartNappyList
      catagory: map['catagory'],
      nappytrainer: map['nappytrainer'],
      nappytime: map['nappytime'],
      //DailyChartBottlefeedList
      bottle_qty: map['bottle_qty'],
      bottletype: map['bottletype'],
      bottletrainer: map['bottletrainer'],
      bottle_time: map['bottle_time'],
      //DailyChartSleepCheckList
      sleeptime: map['sleeptime'],
      sleepchecktrainer: map['sleepchecktrainer'],
      //Childnote
      notes: map['notes'],
      //Learningstory
      learningstorytitle: map['learningstorytitle'],
      educator: map['educator'],
      date: map['date'],
      //Medication
      medicationtitle: map['medicationtitle'],
      //Gallery
      album: map['album'],
      caption: map['caption'],
    );
  }

  factory DailysummaryUI.fromJson(Map value) {

    return DailysummaryUI(
      // No data state
      nodata: value['nodata'],
      empty_state_message: value['empty_state_message'],
      // Journal
      id: value['id'],
      journal_title: value['journal_title'],
      whathappeneddata: value['whathappeneddata'],
      // Observation
      journey_title: value['journey_title'],
      observationdata: value['observationdata'],
      //DailyChartFoodList
      MenuCategory: value['MenuCategory'],
      FoodMenu: value['FoodMenu'],
      Servetype: value['Servetype'],
      //DailyChartNappyList
      catagory: value['catagory'],
      nappytrainer: value['nappytrainer'],
      nappytime: value['nappytime'],
      //DailyChartBottlefeedList
      bottle_qty: value['bottle_qty'],
      bottletype: value['bottletype'],
      bottletrainer: value['bottletrainer'],
      bottle_time: value['bottle_time'],
      //DailyChartSleepCheckList
      sleeptime: value['sleeptime'],
      sleepchecktrainer: value['sleepchecktrainer'],
      //Childnote
      notes: value['notes'],
      //Learningstory
      learningstorytitle: value['learningstorytitle'],
      educator: value['educator'],
      date: value['date'],
      //Medication
      medicationtitle: value['medicationtitle'],
      //Gallery
      album: value['album'],
      caption: value['caption'],
    );
  }

}

List<DailysummaryUI> tasks = [

];