import 'dart:math';

import 'package:flutter/material.dart';
import 'package:carem8/pages/home/api.dart';

class CardsDemo extends StatefulWidget {
  @override
  _CardsDemoState createState() => new _CardsDemoState();
}

class _CardsDemoState extends State<CardsDemo> {
  final List<MaterialColor> _colors = [Colors.blue, Colors.indigo, Colors.red];
  @override
  Widget build(BuildContext context) {
    final items = List<ListItem>.generate(
      1200,
      (i) => i % 6 == 0
          ? HeadingItem("Heading $i")
          : MessageItem("Sender $i", "Message body $i"),
    );
    return new Scaffold(
//      body: new Column(
//        children: <Widget>[
//          new Flexible()
//
//        ],
//      ),
        body: ListView.builder(
      // Let the ListView know how many items it needs to build
      itemCount: items.length,
      // Provide a builder function. This is where the magic happens! We'll
      // convert each item into a Widget based on the type of item it is.
      itemBuilder: (context, index) {
        final item = items[index];

        if (item is HeadingItem) {
          return ListTile(
            title: Text(
              item.heading,
              style: Theme.of(context).textTheme.headline,
            ),
          );
        } else if (item is MessageItem) {
          return ListTile(
            title: Text(item.sender),
            subtitle: Text(item.body),
          );
        }
      },
    )
    );
  }

  Widget _buildCard(CardModel card) {
    List<Widget> columnData = <Widget>[];

    if (card.isHeaderAvailable) {
      columnData.add(
        new Padding(
          padding: const EdgeInsets.only(bottom: 8.0, left: 8.0, right: 8.0),
          child: new Text(
            card.headerText,
            style: new TextStyle(fontSize: 24.0, fontWeight: FontWeight.w500),
          ),
        ),
      );
    }

    for (int i = 0; i < card.allText.length; i++)
      columnData.add(
        new Text(
          card.allText[i],
          style: new TextStyle(fontSize: 22.0),
        ),
      );

    return new Card(
      child: new Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0),
        child: Column(children: columnData),
      ),
    );
  }

  List<Widget> _buildCards() {
    List<Widget> cards = [];
    for (int i = 0; i < sampleCards.length; i++) {
      cards.add(_buildCard(sampleCards[i]));
    }

    return cards;
  }
}

// The base class for the different types of items the List can contain
abstract class ListItem {}

// A ListItem that contains data to display a heading
class HeadingItem implements ListItem {
  final String heading;

  HeadingItem(this.heading);
}

// A ListItem that contains data to display a message
class MessageItem implements ListItem {
  final String sender;
  final String body;

  MessageItem(this.sender, this.body);
}

Widget _buildCard(CardModel card) {
  List<Widget> columnData = <Widget>[];

  if (card.isHeaderAvailable) {
    columnData.add(
      new Padding(
        padding: const EdgeInsets.only(bottom: 8.0, left: 8.0, right: 8.0),
        child: new Text(
          card.headerText,
          style: new TextStyle(fontSize: 24.0, fontWeight: FontWeight.w500),
        ),
      ),
    );
  }

  for (int i = 0; i < card.allText.length; i++)
    columnData.add(
      new Text(
        card.allText[i],
        style: new TextStyle(fontSize: 22.0),
      ),
    );

  return new Card(
    child: new Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: Column(children: columnData),
    ),
  );
}

List<Widget> _buildCards() {
  List<Widget> cards = [];
  for (int i = 0; i < sampleCards.length; i++) {
    cards.add(_buildCard(sampleCards[i]));
  }

  return cards;
}

class CardModel {
  final String headerText;
  final List<String> allText;
  final bool isHeaderAvailable;

  CardModel(
      {this.headerText = "", this.allText, this.isHeaderAvailable = false});
}

List<CardModel> sampleCards = [
  new CardModel(allText: ["Card 1 Text"]),
  new CardModel(
      isHeaderAvailable: true,
      headerText: "Card 2 Header",
      allText: ["Card 2 Text Line 1", "Card 2 Text Line 2"]),
  new CardModel(allText: ["Card 3 Text"]),
];
