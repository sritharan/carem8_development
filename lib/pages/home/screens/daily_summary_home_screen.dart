import 'dart:async';

import 'package:flutter/material.dart';
import 'package:carem8/pages/home/modals/child_summary_modal.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:carem8/pages/home/modals/summary_view_modal.dart';
import 'package:carem8/pages/home/services/daily_summary_service.dart';
import 'package:carem8/pages/home/widgets/child_indicator.dart';
import 'package:carem8/pages/home/widgets/childs.dart';
import 'package:carem8/pages/home/widgets/page_dragger.dart';

class DailySummaryHomeScreen extends StatefulWidget {
  @override
  _DailySummaryHomeScreenState createState() => _DailySummaryHomeScreenState();
}

class _DailySummaryHomeScreenState extends State<DailySummaryHomeScreen> with TickerProviderStateMixin {
  BuildContext ctx;
  StreamController<SlideUpdate> slideUpdateStream;
  AnimatedPageDragger animatedPageDragger;

  int activeIndex = 0;
  int nextPageIndex = 0;
  SlideDirection slideDirection = SlideDirection.none;
  double slidePercent = 0.0;

  SlideUpdate slideUpdate =
  SlideUpdate(UpdateType.doneDragging, SlideDirection.none, 0.0);

  //fetch children data to a List from api
  List<ChildViewModal> childrenData = [];
  List<SummaryViewModal> childSummary = [];
  bool isLoading = true;

  _DailySummaryHomeScreenState() {
    fetchData();
    slideUpdateStream = new StreamController<SlideUpdate>();

    slideUpdateStream.stream.listen((SlideUpdate event) {
      setState(() {
        slideUpdate = event;
        if (event.updateType == UpdateType.dragging) {
          slideDirection = event.direction;
          slidePercent = event.slidePercent;

          if (slideDirection == SlideDirection.leftToRight) {
            nextPageIndex = activeIndex - 1;
          } else if (slideDirection == SlideDirection.rightToLeft) {
            nextPageIndex = activeIndex + 1;
          } else {
            nextPageIndex = activeIndex;
          }
        } else if (event.updateType == UpdateType.doneDragging) {
          if (slidePercent > 0.5) {
            animatedPageDragger = new AnimatedPageDragger(
              slideDirection: slideDirection,
              transitionGoal: TransitionGoal.open,
              slidePercent: slidePercent,
              slideUpdateStream: slideUpdateStream,
              vsync: this,
            );
          } else {
            animatedPageDragger = new AnimatedPageDragger(
              slideDirection: slideDirection,
              transitionGoal: TransitionGoal.close,
              slidePercent: slidePercent,
              slideUpdateStream: slideUpdateStream,
              vsync: this,
            );

            nextPageIndex = activeIndex;
          }

          animatedPageDragger.run();
        } else if (event.updateType == UpdateType.animating) {
          slideDirection = event.direction;
          slidePercent = event.slidePercent;
        } else if (event.updateType == UpdateType.doneAnimating) {
          activeIndex = nextPageIndex;

          slideDirection = SlideDirection.none;
          slidePercent = 0.0;

          animatedPageDragger.dispose();
        }
      });
    });
  }

  void fetchData() async {
    bool isImageAvailable = true;

    final bodyData = {
      "email": "deepika@proitzen.com",
      "password": "ucsc@123",
      "client_id": "preproduction"
    };

    await getChildren(bodyData: bodyData).then((data) {
      childrenData = data["childremarks"].map<ChildViewModal>((element) {
        precacheImage(NetworkImage("http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${element["image"]}"), ctx, onError: (err, stackT) => isImageAvailable = false);
        return ChildViewModal(
            id: element["id"],
            iconAssetPath: isImageAvailable ? element["image"] : '',
            firstName: element["firstname"],
            lastName: element["lastname"]);
      }).toList();
    });

    for (int i = 0; i < childrenData.length; i++) {
      ChildSummaryModal childSummaryModal = await fetchDailySummaryData(childId: childrenData[i].id);
      setState(() {
        childrenData[i].childSummaryVM = childSummaryModal;
      });
      print(childrenData[i].id);
      print(childrenData[i].childSummaryVM.journal.title);
    }

    setState(() {
      activeIndex = childrenData.length > 2 ? 1 : 0;
      isLoading = false;
    });
  }

  Widget buildParentWidget() {
    return isLoading
        ? Center(child: CircularProgressIndicator())
        : ListView(
      children: <Widget>[
        ChildIndicator(
          viewModel: new ChildIndicatorViewModel(
            childrenData,
            activeIndex,
            slideDirection,
            slidePercent,
          ),
        ),
        Child(
          viewModel: (childrenData.length > 0) ? childrenData[activeIndex] : ChildViewModal(), //childrenData[activeIndex],
          slideDirection: slideDirection,
          slideUpdate: slideUpdate,
          percentVisible: (slideDirection == SlideDirection.none)
              ? 1.0
              : slidePercent, //slideUpdateType == UpdateType.doneAnimating &&
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    ctx = context;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0.0,
        title: Text(
          "Daily Summary",
          style: TextStyle(color: Color(0xFFB39DDB), fontSize: 18.5),
        ),
        centerTitle: true,
        leading: IconButton(
            icon: Icon(
              Icons.menu,
              color: Color(0xFFB39DDB),
            ),
            onPressed: null),
      ),
      body: Stack(
        children: <Widget>[
          PageDragger(
            canDragLeftToRight: activeIndex > 0,
            canDragRightToLeft: activeIndex < childrenData.length - 1,
            slideUpdateStream: this.slideUpdateStream,
            child: buildParentWidget(),
          ),
        ],
      ),
    );
  }
}