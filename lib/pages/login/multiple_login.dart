import 'dart:convert';

import 'package:carem8/data/database_helper.dart';
import 'package:carem8/pages/home/dailysummarydatalist.dart';
import 'package:carem8/pages/home/modals/child_summary_modal.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:carem8/pages/home/services/daily_summary_service.dart';

import 'package:carem8/pages/login/login_presenter.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/newsfeed/newsfeedlist.dart';
import 'package:carem8/utils/clientId.dart';
import 'package:flutter/material.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/Theme.dart' as Theme;
import 'package:carem8/utils/network_util.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class MultipleLogin extends StatefulWidget {
  final password;
  var res;
  MultipleLogin(this.password, this.res);

  @override
  MultipleLoginState createState() => MultipleLoginState(password, res);
}

class MultipleLoginState extends State<MultipleLogin>
    implements LoginPageContract {
  final password;
  var res;
  var _isLoading = false;
  List id = new List();
  List email = new List();
  List clients = List();
  List client_id = List();
  List center = List();
  List image = List();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  LoginPagePresenter _presenter;
  var devicetoken;

  List<ChildViewModal> childrenData = [];
  BuildContext ctx;

  MultipleLoginState(this.password, this.res) {
    _presenter = LoginPagePresenter(this);
  }

  // Push
  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();


  String _email, _password;

  getClientId() {
    setState(() {
      _isLoading = true;
    });
    print('Responce $res');

    if (res.length == 1) {
      var user = res[0]["user"];
      id.add(user["id"]);
      email.add(user["email"]);
      client_id.add(user["client_id"]);
      center.add(user["center"]);
      image.add(user["image"]);
      clients.add(user['client']);

//      var email = user["email"];
//      var center = user["center"];
//      var clien_id = user["client_id"];
//      var id=user["id"];
//      var image=user["image"];
    } else {
      for (int i = 0; i < res.length; i++) {
        var clientId = new Login.fromJson(res[i]);
        print('With in for loop${res[i]['client']}');
        id.add(clientId.id);
        email.add(clientId.email);
        clients.add(res[i]['client']);
        client_id.add(clientId.clientId);
        center.add(clientId.center);
        image.add(clientId.image);
      }
    }
  }

  @override
  void initState() {
    this.getClientId();
    super.initState();
    _firebaseMessaging.autoInitEnabled();
    // TODO: get push token and send to login
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message){
        print('onMessage $message');
      },
      onResume: (Map<String, dynamic> message){
        print('onResume $message');
      },
      onLaunch: (Map<String, dynamic> message){
        print('onLaunch $message');
      },
    );
    _firebaseMessaging.getToken().then((token){
      print('device token : $token');
      devicetoken = token;
    });
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }
  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );
  void _submit(result) {
    print('_submit while dologin $result');
    print(result);
    setState(() {
      _isLoading = true;
    });

    _presenter.doLogin(result);
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  // Weblogin using client id
  Future webloginWithEmail(email, password, clientid) async {
    print('called webloginWithEmail');
    _email = email;
    _password = password;
    print("ISLOAD=$_isLoading");

    print('Login Data >>>>>>>> $_email$_password');
    try {
      setState(() {
        _isLoading = true;
      });
      final LOGIN_URL = 'http://${clientid}.carem8.com.au/api/v1/auth';
      NetworkUtil _netutil = new NetworkUtil();
      print('LOGIN_URL $LOGIN_URL');
      _netutil.post(LOGIN_URL, body: {"email": email, "password": password,"devicetoken":devicetoken,"device":"Android"}).then((res) {
        print(res);
        try {
          if (res != null) {
            List decodedList = json.decode(res);

            if (decodedList.length == 1) {
              print("navigate to single");

              var user = decodedList[0]["user"];

//              _presenter.doLogin(result);
            } else {
              print("navigate to multiple");
              setState(() {
                _isLoading = false;
//                Navigator.pushReplacement(
//                    context,
//                    new MaterialPageRoute(
//                        builder: (context) =>
//                        new MultipleLogin(password, decodedList)));
              });
            }
          } else {
            final bar = SnackBar(
              content: Text(
                  "Sorry, we couldn't find entered login credentials, please try again!"),
              duration: Duration(seconds: 3),
              backgroundColor: Colors.redAccent,
            );
            scaffoldKey.currentState.showSnackBar(bar);
            setState(() {
              _isLoading = false;
            });
          }
        } catch (e) {
          print(e);
        }
      });
    }
    catch(e){
      print(e);
    }
  }


  @override
  Widget build(BuildContext context) {
    ctx = context;
    var listView = new ListView.builder(
        itemCount: this.id != null ? this.id.length : 0,
        itemBuilder: (context, i) {
          return (clients[i]['name'] != null || center[i] != null) ? Card(
            elevation: 2.0,
            semanticContainer: false,
            child: FlatButton(
              child: clients[i] != null
                  ? (clients[i]['name'] != null) ? ListTile(
                      leading: (clients[i]['logo'] != null) ? CircleAvatar(
                        //                  radius: 20.0,
                        backgroundImage:
                            NetworkImage(clients[i]['logo'], scale: 10.0),
                      ) : SizedBox(),
                      title: new Text(
                        clients[i]['name'],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 14.0),
                      ),
                    ) : SizedBox()
                  : (center[i] != null) ? new ListTile(
                      leading: CircleAvatar(
                        //                  radius: 50.0,
                        backgroundImage: AssetImage("assets/nophoto.jpg"),
                      ),
                      title: new Text(
                        center[i],
                        style: new TextStyle(
                            fontWeight: FontWeight.w500, fontSize: 14.0),
                      ),
                    ) : SizedBox(),
              onPressed: () {
                var result = {
                  "email": email[i],
                  "password": password,
                  "client_id": client_id[i],
                  "center": center[i],
                  "image": image[i]
                };
                _isLoading = false;
                _submit(result);
              },
            ),
          ) : SizedBox();
        });

    return new Scaffold(
      appBar: new AppBar(
        title: Text("Choose Your Center"),
        centerTitle: true,
        elevation: 0.0,
      ),
      key: scaffoldKey,
      body: id.length > 0
          ? Center(
              child: new Container(
                padding: EdgeInsets.all(16.0),
                child: _isLoading ? new Center(child: listView) : progress,
              ),
            )
          : new Container(
              child: FlatButton(
                child: Text("Press here to Login"),
                onPressed: () {
//                          have to navigate to login page
                },
              ),
            ),
    );
  }

  Future fetchData(List children) async {
    bool isImageAvailable = true;

    await getRefreshToken(childId: children[0]["id"].toString());
    childrenData = children.map<ChildViewModal>((element) {
      precacheImage(NetworkImage("http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${element["image"]}"), ctx, onError: (err, stackT) => isImageAvailable = false);
      return ChildViewModal(
        id: element["id"],
        iconAssetPath: isImageAvailable ? element["image"] : '',
        firstName: element["firstname"],
        lastName: element["lastname"],
        room: element["room"],
        gender: element["gender"],
        attendance: element["attendance"],
        dob: element["dob"],);
    }).toList();

    for (int i = 0; i < childrenData.length; i++) {
      await fetchDailySummaryData(childId: childrenData[i].id, clientId: "preproduction").then((value) {
        ChildSummaryModal childSummaryModal = value;
        if (childSummaryModal.childPhotoModals.length > 0 && i < 4)
          precacheImage(NetworkImage(childSummaryModal.childPhotoModals[i].fileUrl), ctx, onError: (err, trace) {print(err);});
        setState(() {
          childrenData[i].childSummaryVM = childSummaryModal;
//          childrenData[i].navigateFunctions = navigationFunctions;
        });
      });
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void onLoginSuccess(User user) async {
    print("on login multiple");
    var db = new DatabaseHelper();
    print('email >${user.email}');
    print('password >${user.password}');
    print('client_id >${user.client_id}');
    await db.saveUser(user);
//    await db.saveChildrenData(user.);
    _showSnackBar(user.toString());

    setState(() {
      _isLoading = false;
    });

    final parsed = json.decode(user.center);
    var childDetails = parsed[0];
    await fetchData(childDetails['children']);
      Navigator.pushReplacement(
        context,
        new MaterialPageRoute(
            builder: (context) => new NewsFeedList(
              childrenData: childrenData,
            )));
  }

  @override
  void onLoginError(String error) {
    _showSnackBar(error);
    setState(() => _isLoading = false);
  }
}
