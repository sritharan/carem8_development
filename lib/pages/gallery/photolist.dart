import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/gallery/viewphotos.dart';
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/pages/home/modals/child_view_modal.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/utils/network_util.dart';
import 'package:carem8/Theme.dart' as Theme;

class PhotoList extends StatefulWidget {
  final ChildViewModal childData;
  final jwt;
  PhotoList(this.childData, this.jwt);
  @override
  PhotoListState createState() => PhotoListState();
}

class PhotoListState extends State<PhotoList> implements HomePageContract {
  var photoData, photoList;
  var appuser, jwt, userId, clientId, childId;
  bool load = false;
  HomePagePresenter _presenter;
  PhotoListState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(labelsConfig["galleryLabel"]),
        centerTitle: true,
        backgroundColor: Theme.Colors.appdarkcolour,
      ),
      body: load
          ? SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
//
//
//                  Container(
//                    padding: EdgeInsets.all(10.0),
//                    child: Card(
//                      child: Container(
//                        padding: EdgeInsets.all(10.0),
//                        child: Column(
//                          children: <Widget>[
//                            Row(
//                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                              children: <Widget>[
//                                Icon(Icons.image),
//                                Expanded(
//                                    child: Text(
//                                  "Newsfeed",
//                                  style: TextStyle(fontSize: 18.0),
//                                ))
//                              ],
//                            ),
//                            Container(height: 5.0),
//                            Stack(
////                              alignment: Alignment(1.0, 1.0),
//                              children: <Widget>[
//                                photoData["newsfeed"]["url"] == null
//                                    ? Container(
//                                        height: screenSize.width,
//                                        width: screenSize.width,
//                                        child:
//                                            Image.asset("assets/nophoto.jpg"),
//                                      )
//                                    : Container(
//                                        width: screenSize.width,
//                                        height: screenSize.width / 2.0,
//                                        child: new CachedNetworkImage(
//                                          imageUrl: photoData["newsfeed"]
//                                              ["url"],
//                                          placeholder:
//                                              new CupertinoActivityIndicator(),
//                                          fit: BoxFit.fitWidth,
//                                          errorWidget: new Image.asset(
//                                              "assets/nophoto.jpg"),
//                                          fadeOutDuration:
//                                              new Duration(seconds: 1),
//                                          fadeInDuration:
//                                              new Duration(seconds: 3),
//                                        )),
//                                Positioned(
//                                  right: 20.0,
//                                  top: 0.0,
//                                  child: Container(
//                                    child: Center(
//                                      child: Text(
//                                        "+${photoData["newsfeed"]["count"] - 4}",
//                                        style: TextStyle(
//                                            fontSize: 20.0,
//                                            fontWeight: FontWeight.bold),
//                                      ),
//                                    ),
//                                    height: screenSize.width / 2.0,
//                                    width: screenSize.width / 8,
//                                    padding: EdgeInsets.all(4.0),
//                                    decoration: BoxDecoration(
//                                        shape: BoxShape.circle,
//                                        color:
//                                            Color.fromARGB(150, 71, 150, 236)),
//                                  ),
//                                ),
//                              ],
//                            ),
//                            Container(height: 5.0),
//                            Row(
//                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                              children: <Widget>[
//                                new Expanded(
//                                    child: Container(
//                                  height: screenSize.width / 5,
//                                  decoration: new BoxDecoration(
//                                      image: new DecorationImage(
//                                          image: new NetworkImage(
//                                              photoData["observation"]["url"]),
//                                          fit: BoxFit.cover)),
//                                )),
//                                new SizedBox(
//                                  width: 5.0,
//                                ),
//                                new Expanded(
//                                    child: Container(
//                                  height: screenSize.width / 5,
//                                  decoration: new BoxDecoration(
//                                      image: new DecorationImage(
//                                          image: new NetworkImage(
//                                              photoData["journal"]["url"]),
//                                          fit: BoxFit.cover)),
//                                )),
//                                new SizedBox(
//                                  width: 5.0,
//                                ),
//                                new Expanded(
//                                    child: Container(
//                                  height: screenSize.width / 5,
//                                  decoration: new BoxDecoration(
//                                      image: new DecorationImage(
//                                          image: new NetworkImage(
//                                              photoData["journal"]["url"]),
//                                          fit: BoxFit.cover)),
//                                )),
//                                /*  new CachedNetworkImage(
//                                  height: 200.0,
//                                  width: 200.0,
//                                  imageUrl: photoData["newsfeed"]["url"],
//                                  placeholder: new CupertinoActivityIndicator(),
//                                  errorWidget:
//                                      new Image.asset("assets/nophoto.jpg"),
//                                  fadeOutDuration: new Duration(seconds: 1),
//                                  fadeInDuration: new Duration(seconds: 3),
//                                ),
//                                new CachedNetworkImage(
//                                  height: 200.0,
//                                  width: 200.0,
//                                  imageUrl: photoData["newsfeed"]["url"],
//                                  placeholder: new CupertinoActivityIndicator(),
//                                  errorWidget:
//                                      new Image.asset("assets/nophoto.jpg"),
//                                  fadeOutDuration: new Duration(seconds: 1),
//                                  fadeInDuration: new Duration(seconds: 3),
//                                ),
//                                new CachedNetworkImage(
//                                  height: 200.0,
//                                  width: 200.0,
//                                  imageUrl: photoData["newsfeed"]["url"],
//                                  placeholder: new CupertinoActivityIndicator(),
//                                  errorWidget:
//                                      new Image.asset("assets/nophoto.jpg"),
//                                  fadeOutDuration: new Duration(seconds: 1),
//                                  fadeInDuration: new Duration(seconds: 3),
//                                )*/
//                              ],
//                            )
//                          ],
//                        ),
//                      ),
//                    ),
//                  ),
//
//                  ///
//                  ///
//                  ///
//                  /* Container(
//                    padding: EdgeInsets.all(10.0),
//                    child: Card(
//                      child: Container(
//                        padding: EdgeInsets.all(10.0),
//                        child: Column(
//                          children: <Widget>[
//                            Row(
//                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                              children: <Widget>[
//                                Icon(Icons.image),
//                                Expanded(
//                                    child: Text(
//                                  "Newsfeed",
//                                  style: TextStyle(fontSize: 18.0),
//                                ))
//                              ],
//                            ),
//                            Container(height: 5.0),
//                            photoData["newsfeed"]["url"] == null
//                                ? Container(
//                                    height: screenSize.width,
//                                    width: screenSize.width,
//                                    child: Image.asset("assets/nophoto.jpg"),
//                                  )
//                                : Container(
//                                    width: screenSize.width,
//                                    height: screenSize.width / 2.0,
//                                    child: new CachedNetworkImage(
//                                      imageUrl: photoData["newsfeed"]["url"],
//                                      placeholder:
//                                          new CupertinoActivityIndicator(),
//                                      fit: BoxFit.fitWidth,
//                                      errorWidget:
//                                          new Image.asset("assets/nophoto.jpg"),
//                                      fadeOutDuration: new Duration(seconds: 1),
//                                      fadeInDuration: new Duration(seconds: 3),
//                                    )),
//                            Container(height: 5.0),
//                            Row(
//                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                              children: <Widget>[
//                                new Expanded(
//                                    child: Container(
//                                  height: screenSize.width / 5,
//                                  decoration: new BoxDecoration(
//                                      image: new DecorationImage(
//                                          image: new NetworkImage(
//                                              photoData["observation"]["url"]),
//                                          fit: BoxFit.cover)),
//                                )),
//                                new SizedBox(
//                                  width: 5.0,
//                                ),
//                                new Expanded(
//                                    child: Container(
//                                  height: screenSize.width / 5,
//                                  decoration: new BoxDecoration(
//                                      image: new DecorationImage(
//                                          image: new NetworkImage(
//                                              photoData["journal"]["url"]),
//                                          fit: BoxFit.cover)),
//                                )),
//                                new SizedBox(
//                                  width: 5.0,
//                                ),
//                                new Expanded(
//                                    child: Container(
//                                  height: screenSize.width / 5,
//                                  child: Stack(
//                                    children: <Widget>[
//                                      Container(
//                                        child: Center(
//                                          child: Text(
//                                            "+${photoData["newsfeed"]["count"] - 3}",
//                                            style: TextStyle(
//                                                fontSize: 20.0,
//                                                fontWeight: FontWeight.bold),
//                                          ),
//                                        ),
//                                        padding: EdgeInsets.all(4.0),
//                                        decoration: BoxDecoration(
//                                          color: Theme.Colors.appcolour
//                                              .withOpacity(0.95),
//                                        ),
//                                      )
//                                    ],
//                                  ),
//                                  decoration: new BoxDecoration(
//                                      image: new DecorationImage(
//                                          image: new NetworkImage(
//                                              photoData["journal"]["url"]),
//                                          fit: BoxFit.cover)
//                                      ),
//                                )),
//                              ],
//                            )
//                          ],
//                        ),
//                      ),
//                    ),
//                  ),*/
//                  ///
//                  ///
//                  ///
//
//
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      ///
                      ///                   newsFeed part
                      ///
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .push(new MaterialPageRoute<Null>(
                              builder: (BuildContext context) {
                                return GridList(
                                    widget.childData, jwt, 'newsfeed');
                              },
                            ));
                          },
                          child: Container(
                            child: Card(
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.all(5.0),
                                          child: Image.asset(
                                            "assets/iconsetpng/newspaper2.png",
                                            width: screenSize.width / 14.0,
                                            height: screenSize.width / 14.0,
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                          labelsConfig["newsFeedLabel"],
                                          style: TextStyle(fontSize: 16.0),
                                        ))
                                      ],
                                    ),
//
                                    Container(height: 4.0),
//
                                    Stack(
                                      alignment: Alignment(0.0, 0.0),
                                      children: <Widget>[
                                        photoData["newsfeedImageCount"] <= 1
                                            ? Container(
                                                width: screenSize.width / 2.0,
                                                height: screenSize.width / 2.0 +
                                                    4.0 +
                                                    screenSize.width / 10,
                                                child: photoData[
                                                            "newsfeedImageCount"] ==
                                                        0
                                                    ? Image.asset(
                                                        "assets/nophoto.jpg",
                                                        fit: BoxFit.cover,
                                                      )
                                                    : new CachedNetworkImage(
                                                        imageUrl: photoData[
                                                                "newsfeed"][0]
                                                            ["url"],
                                                        placeholder:
                                                            new CupertinoActivityIndicator(),
                                                        fit: BoxFit.cover,
                                                        errorWidget:
                                                            new Image.asset(
                                                          "assets/cube_feed_noimage.png",
                                                          fit: BoxFit.cover,
                                                        ),
                                                        fadeOutDuration:
                                                            new Duration(
                                                                seconds: 1),
                                                        fadeInDuration:
                                                            new Duration(
                                                                seconds: 3),
                                                      ))
                                            : Container(
                                                width: screenSize.width / 2.0,
                                                height: screenSize.width / 2.0,
                                                child: photoData[
                                                            "newsfeedImageCount"] ==
                                                        0
                                                    ? Image.asset(
                                                        "assets/nophoto.jpg",
                                                        fit: BoxFit.cover,
                                                      )
                                                    : new CachedNetworkImage(
                                                        imageUrl: photoData[
                                                                "newsfeed"][0]
                                                            ["url"],
                                                        placeholder:
                                                            new CupertinoActivityIndicator(),
                                                        fit: BoxFit.cover,
                                                        errorWidget:
                                                            new Image.asset(
                                                          "assets/cube_feed_noimage.png",
                                                          fit: BoxFit.cover,
                                                        ),
                                                        fadeOutDuration:
                                                            new Duration(
                                                                seconds: 1),
                                                        fadeInDuration:
                                                            new Duration(
                                                                seconds: 3),
                                                      )),
//
//
                                        photoData["newsfeedImageCount"] > 4
                                            ? Container(
                                                child: Center(
                                                  child: Text(
                                                    "+${photoData["newsfeedImageCount"] - 4}",
                                                    style: TextStyle(
                                                        color: Colors.black
                                                            .withOpacity(0.7),
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                ),
                                                height: screenSize.width / 6.5,
                                                width: screenSize.width / 6.5,
                                                padding: EdgeInsets.all(4.0),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Theme
                                                        .Colors.appcolour
                                                        .withOpacity(0.7)),
                                              )
                                            : photoData["newsfeedImageCount"] >=
                                                    0
                                                ? Container(
                                                    child: Center(
                                                      child: Text(
                                                        "${photoData["newsfeedImageCount"]}",
                                                        style: TextStyle(
                                                            color: Colors.black
                                                                .withOpacity(
                                                                    0.7),
                                                            fontSize: 16.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      ),
                                                    ),
                                                    height:
                                                        screenSize.width / 6.5,
                                                    width:
                                                        screenSize.width / 6.5,
                                                    padding:
                                                        EdgeInsets.all(4.0),
                                                    decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: Theme
                                                            .Colors.appcolour
                                                            .withOpacity(0.7)),
                                                  )
                                                : Container(),
                                      ],
                                    ),
//
//                                    Container(height: 5.0),
//
                                    photoData["newsfeedImageCount"] >= 4
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                top: 4.0, bottom: 4.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image: new DecorationImage(
                                                          image: photoData["newsfeed"]
                                                                          [1]
                                                                      ["url"] !=
                                                                  null
                                                              ? new NetworkImage(
                                                                  photoData[
                                                                          "newsfeed"]
                                                                      [
                                                                      1]["url"])
                                                              : AssetImage(
                                                                  "assets/cube_feed_noimage.png"),
                                                          fit: BoxFit.cover)),
                                                )),
                                                new SizedBox(
                                                  width: 4.0,
                                                ),
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image:
                                                          new DecorationImage(
                                                              image: photoData["newsfeed"]
                                                                              [
                                                                              2]
                                                                          [
                                                                          "url"] !=
                                                                      null
                                                                  ? new NetworkImage(
                                                                      photoData["newsfeed"]
                                                                              [
                                                                              2]
                                                                          [
                                                                          "url"])
                                                                  : AssetImage(
                                                                      "assets/cube_feed_noimage.png"),
                                                              fit: BoxFit
                                                                  .cover)),
                                                )),
                                                new SizedBox(
                                                  width: 4.0,
                                                ),
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image:
                                                          new DecorationImage(
                                                              image: photoData["newsfeed"]
                                                                              [
                                                                              3]
                                                                          [
                                                                          "url"] !=
                                                                      null
                                                                  ? new NetworkImage(
                                                                      photoData["newsfeed"]
                                                                              [
                                                                              3]
                                                                          [
                                                                          "url"])
                                                                  : AssetImage(
                                                                      "assets/cube_feed_noimage.png"),
                                                              fit: BoxFit
                                                                  .cover)),
                                                )),
                                              ],
                                            ),
                                          )
                                        : photoData["newsfeedImageCount"] < 4 &&
                                                photoData[
                                                        "newsfeedImageCount"] >
                                                    1
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    top: 4.0, bottom: 4.0),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: <Widget>[
                                                    new Expanded(
                                                        child: Container(
                                                      height:
                                                          screenSize.width / 10,
                                                      decoration:
                                                          new BoxDecoration(
                                                              image: new DecorationImage(
                                                                  image: photoData["newsfeed"][1]
                                                                              [
                                                                              "url"] !=
                                                                          null
                                                                      ? new NetworkImage(photoData["newsfeed"]
                                                                              [
                                                                              1]
                                                                          [
                                                                          "url"])
                                                                      : AssetImage(
                                                                          "assets/cube_feed_noimage.png"),
                                                                  fit: BoxFit
                                                                      .cover)),
                                                    )),
                                                    new SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    new Expanded(
                                                        child: photoData[
                                                                    "newsfeedImageCount"] >
                                                                2
                                                            ? Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                                decoration: new BoxDecoration(
                                                                    image: new DecorationImage(
                                                                        image: photoData["newsfeed"][2]["url"] != null ? new NetworkImage(photoData["newsfeed"][2]["url"]) : AssetImage("assets/cube_feed_noimage.png"),
                                                                        fit: BoxFit.cover)),
                                                              )
                                                            : Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                              )),
                                                    new SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    new Expanded(
                                                        child: photoData[
                                                                    "newsfeedImageCount"] >
                                                                3
                                                            ? Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                                decoration: new BoxDecoration(
                                                                    image: new DecorationImage(
                                                                        image: photoData["newsfeed"][3]["url"] != null ? new NetworkImage(photoData["newsfeed"][3]["url"]) : AssetImage("assets/cube_feed_noimage.png"),
                                                                        fit: BoxFit.cover)),
                                                              )
                                                            : Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10)),
                                                  ],
                                                ),
                                              )
                                            : Container()
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                      ///
                      ///                   observation part
                      ///

                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .push(new MaterialPageRoute<Null>(
                              builder: (BuildContext context) {
                                return GridList(
                                    widget.childData, jwt, 'journey');
                              },
                            ));
                          },
                          child: Container(
                            child: Card(
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.all(5.0),
                                          child: Image.asset(
                                            "assets/iconsetpng/game.png",
                                            width: screenSize.width / 14.0,
                                            height: screenSize.width / 14.0,
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            labelsConfig["observationLabel"],
                                            style: TextStyle(fontSize: 16.0),
                                        ))
                                      ],
                                    ),
//
                                    Container(height: 4.0),
//
                                    Stack(
                                      alignment: Alignment(0.0, 0.0),
                                      children: <Widget>[
                                        photoData["observationImageCount"] <= 1
                                            ? Container(
                                                width: screenSize.width / 2.0,
                                                height: screenSize.width / 2.0 +
                                                    4.0 +
                                                    screenSize.width / 10,
                                                child: photoData[
                                                            "observationImageCount"] ==
                                                        0
                                                    ? Image.asset(
                                                        "assets/nophoto.jpg",
                                                        fit: BoxFit.cover,
                                                      )
                                                    : new CachedNetworkImage(
                                                        imageUrl: photoData[
                                                                "observation"]
                                                            [0]["url"],
                                                        placeholder:
                                                            new CupertinoActivityIndicator(),
                                                        fit: BoxFit.cover,
                                                        errorWidget:
                                                            new Image.asset(
                                                          "assets/cube_feed_noimage.png",
                                                          fit: BoxFit.cover,
                                                        ),
                                                        fadeOutDuration:
                                                            new Duration(
                                                                seconds: 1),
                                                        fadeInDuration:
                                                            new Duration(
                                                                seconds: 3),
                                                      ))
                                            : Container(
                                                width: screenSize.width / 2.0,
                                                height: screenSize.width / 2.0,
                                                child: photoData[
                                                            "observationImageCount"] ==
                                                        0
                                                    ? Image.asset(
                                                        "assets/nophoto.jpg",
                                                        fit: BoxFit.cover,
                                                      )
                                                    : new CachedNetworkImage(
                                                        imageUrl: photoData[
                                                                "observation"]
                                                            [0]["url"],
                                                        placeholder:
                                                            new CupertinoActivityIndicator(),
                                                        fit: BoxFit.cover,
                                                        errorWidget:
                                                            new Image.asset(
                                                          "assets/cube_feed_noimage.png",
                                                          fit: BoxFit.cover,
                                                        ),
                                                        fadeOutDuration:
                                                            new Duration(
                                                                seconds: 1),
                                                        fadeInDuration:
                                                            new Duration(
                                                                seconds: 3),
                                                      )),
//
//
                                        photoData["observationImageCount"] > 4
                                            ? Container(
                                                child: Center(
                                                  child: Text(
                                                    "+${photoData["observationImageCount"] - 4}",
                                                    style: TextStyle(
                                                        color: Colors.black
                                                            .withOpacity(0.7),
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                ),
                                                height: screenSize.width / 6.5,
                                                width: screenSize.width / 6.5,
                                                padding: EdgeInsets.all(4.0),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Theme
                                                        .Colors.appcolour
                                                        .withOpacity(0.7)),
                                              )
                                            : photoData["observationImageCount"] >=
                                                    0
                                                ? Container(
                                                    child: Center(
                                                      child: Text(
                                                        "${photoData["observationImageCount"]}",
                                                        style: TextStyle(
                                                            color: Colors.black
                                                                .withOpacity(
                                                                    0.7),
                                                            fontSize: 16.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      ),
                                                    ),
                                                    height:
                                                        screenSize.width / 6.5,
                                                    width:
                                                        screenSize.width / 6.5,
                                                    padding:
                                                        EdgeInsets.all(4.0),
                                                    decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: Theme
                                                            .Colors.appcolour
                                                            .withOpacity(0.7)),
                                                  )
                                                : Container(),
                                      ],
                                    ),
//
//                                    Container(height: 5.0),
//
                                    photoData["observationImageCount"] >= 4
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                top: 4.0, bottom: 4.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image: new DecorationImage(
                                                          image: photoData["observation"]
                                                                          [1]
                                                                      ["url"] !=
                                                                  null
                                                              ? new NetworkImage(
                                                                  photoData[
                                                                          "observation"]
                                                                      [
                                                                      1]["url"])
                                                              : AssetImage(
                                                                  "assets/cube_feed_noimage.png"),
                                                          fit: BoxFit.cover)),
                                                )),
                                                new SizedBox(
                                                  width: 4.0,
                                                ),
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image:
                                                          new DecorationImage(
                                                              image: photoData["observation"]
                                                                              [
                                                                              2]
                                                                          [
                                                                          "url"] !=
                                                                      null
                                                                  ? new NetworkImage(
                                                                      photoData["observation"]
                                                                              [
                                                                              2]
                                                                          [
                                                                          "url"])
                                                                  : AssetImage(
                                                                      "assets/cube_feed_noimage.png"),
                                                              fit: BoxFit
                                                                  .cover)),
                                                )),
                                                new SizedBox(
                                                  width: 4.0,
                                                ),
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image:
                                                          new DecorationImage(
                                                              image: photoData["observation"]
                                                                              [
                                                                              3]
                                                                          [
                                                                          "url"] !=
                                                                      null
                                                                  ? new NetworkImage(
                                                                      photoData["observation"]
                                                                              [
                                                                              3]
                                                                          [
                                                                          "url"])
                                                                  : AssetImage(
                                                                      "assets/cube_feed_noimage.png"),
                                                              fit: BoxFit
                                                                  .cover)),
                                                )),
                                              ],
                                            ),
                                          )
                                        : photoData["observationImageCount"] <
                                                    4 &&
                                                photoData[
                                                        "observationImageCount"] >
                                                    1
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    top: 4.0, bottom: 4.0),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: <Widget>[
                                                    new Expanded(
                                                        child: Container(
                                                      height:
                                                          screenSize.width / 10,
                                                      decoration:
                                                          new BoxDecoration(
                                                              image: new DecorationImage(
                                                                  image: photoData["observation"][1]
                                                                              [
                                                                              "url"] !=
                                                                          null
                                                                      ? new NetworkImage(photoData["observation"]
                                                                              [
                                                                              1]
                                                                          [
                                                                          "url"])
                                                                      : AssetImage(
                                                                          "assets/cube_feed_noimage.png"),
                                                                  fit: BoxFit
                                                                      .cover)),
                                                    )),
                                                    new SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    new Expanded(
                                                        child: photoData[
                                                                    "observationImageCount"] >
                                                                2
                                                            ? Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                                decoration: new BoxDecoration(
                                                                    image: new DecorationImage(
                                                                        image: photoData["observation"][2]["url"] != null ? new NetworkImage(photoData["observation"][2]["url"]) : AssetImage("assets/cube_feed_noimage.png"),
                                                                        fit: BoxFit.cover)),
                                                              )
                                                            : Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                              )),
                                                    new SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    new Expanded(
                                                        child: photoData[
                                                                    "observationImageCount"] >
                                                                3
                                                            ? Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                                decoration: new BoxDecoration(
                                                                    image: new DecorationImage(
                                                                        image: photoData["observation"][3]["url"] != null ? new NetworkImage(photoData["observation"][3]["url"]) : AssetImage("assets/cube_feed_noimage.png"),
                                                                        fit: BoxFit.cover)),
                                                              )
                                                            : Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10)),
                                                  ],
                                                ),
                                              )
                                            : Container()
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      ///
                      ///                   learningStory part
                      ///
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .push(new MaterialPageRoute<Null>(
                              builder: (BuildContext context) {
                                return GridList(
                                    widget.childData, jwt, 'learningstory');
                              },
                            ));
                          },
                          child: Container(
                            child: Card(
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.all(5.0),
                                          child: Image.asset(
                                            "assets/iconsetpng/abacus.png",
                                            width: screenSize.width / 14.0,
                                            height: screenSize.width / 14.0,
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            labelsConfig["learningStoryLabel"],
                                            style: TextStyle(fontSize: 16.0),
                                        ))
                                      ],
                                    ),
//
                                    Container(height: 4.0),
//
                                    Stack(
                                      alignment: Alignment(0.0, 0.0),
                                      children: <Widget>[
                                        photoData["learningstoryImageCount"] <=
                                                1
                                            ? Container(
                                                width: screenSize.width / 2.0,
                                                height: screenSize.width / 2.0 +
                                                    screenSize.width / 10,
                                                child: photoData[
                                                            "learningstoryImageCount"] ==
                                                        0
                                                    ? Image.asset(
                                                        "assets/nophoto.jpg",
                                                        fit: BoxFit.cover,
                                                      )
                                                    : new CachedNetworkImage(
                                                        imageUrl: photoData[
                                                                "learningstory"]
                                                            [0]["url"],
                                                        placeholder:
                                                            new CupertinoActivityIndicator(),
                                                        fit: BoxFit.cover,
                                                        errorWidget:
                                                            new Image.asset(
                                                          "assets/cube_feed_noimage.png",
                                                          fit: BoxFit.cover,
                                                        ),
                                                        fadeOutDuration:
                                                            new Duration(
                                                                seconds: 1),
                                                        fadeInDuration:
                                                            new Duration(
                                                                seconds: 3),
                                                      ))
                                            : Container(
                                                width: screenSize.width / 2.0,
                                                height: screenSize.width / 2.0,
                                                child: photoData[
                                                            "learningstoryImageCount"] ==
                                                        0
                                                    ? Image.asset(
                                                        "assets/nophoto.jpg",
                                                        fit: BoxFit.cover,
                                                      )
                                                    : new CachedNetworkImage(
                                                        imageUrl: photoData[
                                                                "learningstory"]
                                                            [0]["url"],
                                                        placeholder:
                                                            new CupertinoActivityIndicator(),
                                                        fit: BoxFit.cover,
                                                        errorWidget:
                                                            new Image.asset(
                                                          "assets/cube_feed_noimage.png",
                                                          fit: BoxFit.cover,
                                                        ),
                                                        fadeOutDuration:
                                                            new Duration(
                                                                seconds: 1),
                                                        fadeInDuration:
                                                            new Duration(
                                                                seconds: 3),
                                                      )),
//
//
                                        photoData["learningstoryImageCount"] > 4
                                            ? Container(
                                                child: Center(
                                                  child: Text(
                                                    "+${photoData["learningstoryImageCount"] - 4}",
                                                    style: TextStyle(
                                                        color: Colors.black
                                                            .withOpacity(0.7),
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                ),
                                                height: screenSize.width / 6.5,
                                                width: screenSize.width / 6.5,
                                                padding: EdgeInsets.all(4.0),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Theme
                                                        .Colors.appcolour
                                                        .withOpacity(0.7)),
                                              )
                                            : photoData["learningstoryImageCount"] >=
                                                    0
                                                ? Container(
                                                    child: Center(
                                                      child: Text(
                                                        "${photoData["learningstoryImageCount"]}",
                                                        style: TextStyle(
                                                            color: Colors.black
                                                                .withOpacity(
                                                                    0.7),
                                                            fontSize: 16.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      ),
                                                    ),
                                                    height:
                                                        screenSize.width / 6.5,
                                                    width:
                                                        screenSize.width / 6.5,
                                                    padding:
                                                        EdgeInsets.all(4.0),
                                                    decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: Theme
                                                            .Colors.appcolour
                                                            .withOpacity(0.7)),
                                                  )
                                                : Container(),
                                      ],
                                    ),
//
//                                    Container(height: 5.0),
//
                                    photoData["learningstoryImageCount"] >= 4
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                top: 4.0, bottom: 4.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image: new DecorationImage(
                                                          image: photoData["learningstory"]
                                                                          [1]
                                                                      ["url"] !=
                                                                  null
                                                              ? new NetworkImage(
                                                                  photoData[
                                                                          "learningstory"]
                                                                      [
                                                                      1]["url"])
                                                              : AssetImage(
                                                                  "assets/cube_feed_noimage.png"),
                                                          fit: BoxFit.cover)),
                                                )),
                                                new SizedBox(
                                                  width: 4.0,
                                                ),
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image:
                                                          new DecorationImage(
                                                              image: photoData["learningstory"]
                                                                              [
                                                                              2]
                                                                          [
                                                                          "url"] !=
                                                                      null
                                                                  ? new NetworkImage(
                                                                      photoData["learningstory"]
                                                                              [
                                                                              2]
                                                                          [
                                                                          "url"])
                                                                  : AssetImage(
                                                                      "assets/cube_feed_noimage.png"),
                                                              fit: BoxFit
                                                                  .cover)),
                                                )),
                                                new SizedBox(
                                                  width: 4.0,
                                                ),
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image:
                                                          new DecorationImage(
                                                              image: photoData["learningstory"]
                                                                              [
                                                                              3]
                                                                          [
                                                                          "url"] !=
                                                                      null
                                                                  ? new NetworkImage(
                                                                      photoData["learningstory"]
                                                                              [
                                                                              3]
                                                                          [
                                                                          "url"])
                                                                  : AssetImage(
                                                                      "assets/cube_feed_noimage.png"),
                                                              fit: BoxFit
                                                                  .cover)),
                                                )),
                                              ],
                                            ),
                                          )
                                        : photoData["learningstoryImageCount"] <
                                                    4 &&
                                                photoData[
                                                        "learningstoryImageCount"] >
                                                    1
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    top: 4.0, bottom: 4.0),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: <Widget>[
                                                    new Expanded(
                                                        child: Container(
                                                      height:
                                                          screenSize.width / 10,
                                                      decoration:
                                                          new BoxDecoration(

                                                              image: new DecorationImage(
                                                                  image: photoData["learningstory"][1]
                                                                              [
                                                                              "url"] !=
                                                                          null
                                                                      ? new NetworkImage(photoData["learningstory"]
                                                                              [
                                                                              1]
                                                                          [
                                                                          "url"])
                                                                      : AssetImage(
                                                                          "assets/cube_feed_noimage.png"),
                                                                  fit: BoxFit
                                                                      .cover)),
                                                    )),
                                                    new SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    new Expanded(
                                                        child: photoData[
                                                                    "learningstoryImageCount"] >
                                                                2
                                                            ? Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                                decoration: new BoxDecoration(
                                                                    image: new DecorationImage(
                                                                        image: photoData["learningstory"][2]["url"] != null ? new NetworkImage(photoData["learningstory"][2]["url"]) : AssetImage("assets/cube_feed_noimage.png"),
                                                                        fit: BoxFit.cover)),
                                                              )
                                                            : Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                              )),
                                                    new SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    new Expanded(
                                                        child: photoData[
                                                                    "learningstoryImageCount"] >
                                                                3
                                                            ? Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                                decoration: new BoxDecoration(
                                                                    image: new DecorationImage(
                                                                        image: photoData["learningstory"][3]["url"] != null ? new NetworkImage(photoData["learningstory"][3]["url"]) : AssetImage("assets/cube_feed_noimage.png"),
                                                                        fit: BoxFit.cover)),
                                                              )
                                                            : Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10)),
                                                  ],
                                                ),
                                              )
                                            : Container()
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                      ///
                      ///                   journal part
                      ///

                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .push(new MaterialPageRoute<Null>(
                              builder: (BuildContext context) {
                                return GridList(
                                    widget.childData, jwt, 'journal');
                              },
                            ));
                          },
                          child: Container(
                            child: Card(
                              child: Container(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.all(5.0),
                                          child: Image.asset(
                                            "assets/iconsetpng/cubes.png",
                                            width: screenSize.width / 14.0,
                                            height: screenSize.width / 14.0,
                                          ),
                                        ),
                                        Expanded(
                                          child: Text(
                                            labelsConfig["journalsLabel"],
                                            style: TextStyle(fontSize: 16.0),
                                        ))
                                      ],
                                    ),
//
                                    Container(height: 4.0),
//
                                    Stack(
                                      alignment: Alignment(0.0, 0.0),
                                      children: <Widget>[
                                        photoData["journalImageCount"] <= 1
                                            ? Container(
                                                width: screenSize.width / 2.0,
                                                height: screenSize.width / 2.0 +
                                                    screenSize.width / 10,
                                                child: photoData[
                                                            "journalImageCount"] ==
                                                        0
                                                    ? Image.asset(
                                                        "assets/nophoto.jpg",
                                                        fit: BoxFit.cover,
                                                      )
                                                    : new CachedNetworkImage(
                                                        imageUrl:
                                                            photoData["journal"]
                                                                [0]["url"],
                                                        placeholder:
                                                            new CupertinoActivityIndicator(),
                                                        fit: BoxFit.cover,
                                                        errorWidget:
                                                            new Image.asset(
                                                          "assets/cube_feed_noimage.png",
                                                          fit: BoxFit.cover,
                                                        ),
                                                        fadeOutDuration:
                                                            new Duration(
                                                                seconds: 1),
                                                        fadeInDuration:
                                                            new Duration(
                                                                seconds: 3),
                                                      ))
                                            : Container(
                                                width: screenSize.width / 2.0,
                                                height: screenSize.width / 2.0,
                                                child: photoData[
                                                            "journalImageCount"] ==
                                                        0
                                                    ? Image.asset(
                                                        "assets/nophoto.jpg",
                                                        fit: BoxFit.cover,
                                                      )
                                                    : new CachedNetworkImage(
                                                        imageUrl:
                                                            photoData["journal"]
                                                                [0]["url"],
                                                        placeholder:
                                                            new CupertinoActivityIndicator(),
                                                        fit: BoxFit.cover,
                                                        errorWidget:
                                                            new Image.asset(
                                                          "assets/cube_feed_noimage.png",
                                                          fit: BoxFit.cover,
                                                        ),
                                                        fadeOutDuration:
                                                            new Duration(
                                                                seconds: 1),
                                                        fadeInDuration:
                                                            new Duration(
                                                                seconds: 3),
                                                      )),
//
//
                                        photoData["journalImageCount"] > 4
                                            ? Container(
                                                child: Center(
                                                  child: Text(
                                                    "+${photoData["journalImageCount"] - 4}",
                                                    style: TextStyle(
                                                        color: Colors.black
                                                            .withOpacity(0.7),
                                                        fontSize: 16.0,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                ),
                                                height: screenSize.width / 6.5,
                                                width: screenSize.width / 6.5,
                                                padding: EdgeInsets.all(4.0),
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Theme
                                                        .Colors.appcolour
                                                        .withOpacity(0.7)),
                                              )
                                            : photoData["journalImageCount"] >=
                                                    0
                                                ? Container(
                                                    child: Center(
                                                      child: Text(
                                                        "${photoData["journalImageCount"]}",
                                                        style: TextStyle(
                                                            color: Colors.black
                                                                .withOpacity(
                                                                    0.7),
                                                            fontSize: 16.0,
                                                            fontWeight:
                                                                FontWeight
                                                                    .w600),
                                                      ),
                                                    ),
                                                    height:
                                                        screenSize.width / 6.5,
                                                    width:
                                                        screenSize.width / 6.5,
                                                    padding:
                                                        EdgeInsets.all(4.0),
                                                    decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: Theme
                                                            .Colors.appcolour
                                                            .withOpacity(0.7)),
                                                  )
                                                : Container(),
                                      ],
                                    ),
//
//                                    Container(height: 5.0),
//
                                    photoData["journalImageCount"] >= 4
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                top: 4.0, bottom: 4.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image: new DecorationImage(
                                                          image: photoData["journal"]
                                                                          [1]
                                                                      ["url"] !=
                                                                  null
                                                              ? new NetworkImage(
                                                                  photoData[
                                                                          "journal"]
                                                                      [
                                                                      1]["url"])
                                                              : AssetImage(
                                                                  "assets/cube_feed_noimage.png"),
                                                          fit: BoxFit.cover)),
                                                )),
                                                new SizedBox(
                                                  width: 4.0,
                                                ),
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image:
                                                          new DecorationImage(
                                                              image: photoData["journal"]
                                                                              [
                                                                              2]
                                                                          [
                                                                          "url"] !=
                                                                      null
                                                                  ? new NetworkImage(
                                                                      photoData["journal"]
                                                                              [
                                                                              2]
                                                                          [
                                                                          "url"])
                                                                  : AssetImage(
                                                                      "assets/cube_feed_noimage.png"),
                                                              fit: BoxFit
                                                                  .cover)),
                                                )),
                                                new SizedBox(
                                                  width: 4.0,
                                                ),
                                                new Expanded(
                                                    child: Container(
                                                  height: screenSize.width / 10,
                                                  decoration: new BoxDecoration(
                                                      image:
                                                          new DecorationImage(
                                                              image: photoData["journal"]
                                                                              [
                                                                              3]
                                                                          [
                                                                          "url"] !=
                                                                      null
                                                                  ? new NetworkImage(
                                                                      photoData["journal"]
                                                                              [
                                                                              3]
                                                                          [
                                                                          "url"])
                                                                  : AssetImage(
                                                                      "assets/cube_feed_noimage.png"),
                                                              fit: BoxFit
                                                                  .cover)),
                                                )),
                                              ],
                                            ),
                                          )
                                        : photoData["journalImageCount"] < 4 &&
                                                photoData["journalImageCount"] >
                                                    1
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    top: 4.0, bottom: 4.0),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: <Widget>[
                                                    new Expanded(
                                                        child: Container(
                                                      height:
                                                          screenSize.width / 10,
                                                      decoration:
                                                          new BoxDecoration(
                                                              image: new DecorationImage(
                                                                  image: photoData["journal"][1]
                                                                              [
                                                                              "url"] !=
                                                                          null
                                                                      ? new NetworkImage(photoData["journal"]
                                                                              [
                                                                              1]
                                                                          [
                                                                          "url"])
                                                                      : AssetImage(
                                                                          "assets/cube_feed_noimage.png"),
                                                                  fit: BoxFit
                                                                      .cover)),
                                                    )),
                                                    new SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    new Expanded(
                                                        child: photoData[
                                                                    "journalImageCount"] >
                                                                2
                                                            ? Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                                decoration: new BoxDecoration(
                                                                    image: new DecorationImage(
                                                                        image: photoData["journal"][2]["url"] != null ? new NetworkImage(photoData["journal"][2]["url"]) : AssetImage("assets/cube_feed_noimage.png"),
                                                                        fit: BoxFit.cover)),
                                                              )
                                                            : Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                              )),
                                                    new SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    new Expanded(
                                                        child: photoData[
                                                                    "journalImageCount"] >
                                                                3
                                                            ? Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10,
                                                                decoration: new BoxDecoration(
                                                                    image: new DecorationImage(
                                                                        image: photoData["journal"][3]["url"] != null ? new NetworkImage(photoData["journal"][3]["url"]) : AssetImage("assets/cube_feed_noimage.png"),
                                                                        fit: BoxFit.cover)),
                                                              )
                                                            : Container(
                                                                height: screenSize
                                                                        .width /
                                                                    10)),
                                                  ],
                                                ),
                                              )
                                            : Container()
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),

                  /*Card(
                    child: photoData["observation"]["url"] == null
                        ? Container(
                            child: Image.asset("assets/nophoto.jpg"),
                          )
                        : Container(
                            child: new CachedNetworkImage(
                            imageUrl: photoData["observation"]["url"],
                            placeholder: new CupertinoActivityIndicator(),
                            errorWidget: new Image.asset("assets/nophoto.jpg"),
                            fadeOutDuration: new Duration(seconds: 1),
                            fadeInDuration: new Duration(seconds: 3),
                          )),
                  ),
                  Card(
                    child: photoData["learningstory"]["url"] == null
                        ? Container(
                            child: Image.asset("assets/nophoto.jpg"),
                          )
                        : Container(
                            child: new CachedNetworkImage(
                            imageUrl: photoData["learningstory"]["url"],
                            placeholder: new CupertinoActivityIndicator(),
                            errorWidget: new Image.asset("assets/nophoto.jpg"),
                            fadeOutDuration: new Duration(seconds: 1),
                            fadeInDuration: new Duration(seconds: 3),
                          )),
                  ),
                  Card(
                    child: photoData["journal"]["url"] == null
                        ? Container(
                            child: Image.asset("assets/nophoto.jpg"),
                          )
                        : Container(
                            child: new CachedNetworkImage(
                              imageUrl: photoData["journal"]["url"],
                              placeholder: new CupertinoActivityIndicator(),
                              errorWidget:
                                  new Image.asset("assets/nophoto.jpg"),
                              fadeOutDuration: new Duration(seconds: 1),
                              fadeInDuration: new Duration(seconds: 3),
                            ),
                          ),
                  ),*/
                  /*      FlatButton(
                onPressed: () {
                  Navigator.of(context).push(new MaterialPageRoute<Null>(
                      builder: (BuildContext context) => GridListDemo()));
                },
                child: Card(
                  child: photoData["newsfeed"]["url"] == null
                      ? Container(
                          child: Image.asset("assets/nophoto.jpg"),
                        )
                      : Container(
                          child: new CachedNetworkImage(
                          imageUrl: photoData["newsfeed"]["url"],
                          placeholder: new CupertinoActivityIndicator(),
                          errorWidget:
                              new Image.asset("assets/nophoto.jpg"),
                          fadeOutDuration: new Duration(seconds: 1),
                          fadeInDuration: new Duration(seconds: 3),
                        )),
                ),
              ),*/
                ],
              ),
            )
          : progress,
    );
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  getPhotoData() {
    print("getPhotoData");
    String getCommentUrl =
        'https://apicare.carem8.com/flutter/v4.4.4/getgallery/$childId?clientid=$clientId';
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(getCommentUrl, headers: headers).then((response) {
      print('res get ${response.body}');
      photoList = json.decode(response.body);

      print('map $photoData');
      print(response.statusCode);
      if (response.statusCode == 200) {
        photoData = photoList;
        print(photoData);
//        print(photoData["observationImageCount"]);
        setState(() {
          load = true;
        });
      } else if (response.statusCode == 500 &&
          photoList["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      }
    });
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      getPhotoData();
    });
  }

  @override
  void onDisplayUserInfo(User user) {
    childId = widget.childData.id;
    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = widget.jwt;
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];

      print("iddd $userId");
      print(clientId);
      getPhotoData();
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }

    // TODO: implement onDisplayUserInfo
  }

  @override
  void onErrorUserInfo() {
    // TODO: implement onErrorUserInfo
  }

  @override
  void onLogoutUser() {
    // TODO: implement onLogoutUser
  }
}
