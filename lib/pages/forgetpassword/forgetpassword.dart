import 'dart:convert';
//import 'dart:ui';
import 'package:carem8/Theme.dart' as Theme;
import 'package:flutter/material.dart';
import 'package:carem8/pages/forgetpassword/multiplecenter.dart';
import 'package:carem8/pages/login/login.dart';
import 'package:carem8/utils/network_util.dart';

class ForgetPassword extends StatefulWidget {
  @override
  ForgetPasswordState createState() => ForgetPasswordState();
}

class ForgetPasswordState extends State<ForgetPassword> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  bool _isLoading = false;
  String email;
  bool _isvalidEmail;
  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value)) {
//      return 'Enter Valid Email';
      showInSnackBar("Enter Valid Email");
      _isvalidEmail = true;
    } else {
      _isvalidEmail = false;
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    var submitBtn = new GestureDetector(
      onTap: submit,
      child: new Padding(
//        padding: EdgeInsets.only(top: 15.0),
        padding: const EdgeInsets.all(5.0),
        child: new Container(
          width: screenSize.width,
          margin: new EdgeInsets.only(
              left: 10.0, right: 10.0, bottom: 2.0),
          height: 50.0,
          alignment: FractionalOffset.center,
          decoration: new BoxDecoration(
//        color: const Color.fromRGBO(247, 64, 106, 1.0),
            color: Theme.Colors.forgotsubmitbutton,
            borderRadius: new BorderRadius.all(const Radius.circular(20.0)),
          ),
          child: new Text(
            "SUBMIT",
            style: new TextStyle(
              color: Theme.Colors.forgotbuttontext,
              fontSize: 20.0,
              fontWeight: FontWeight.w300,
              letterSpacing: 0.3,
            ),
          ),
        ),
      ),
    );

    final mediaQueryData = MediaQuery.of(context);
    double width = MediaQuery.of(context).size.width;
    double width65 = width * 0.40;
    var forgetForm = new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          margin: new EdgeInsets.only(top: 50.0),
          child: new SizedBox(
            height: 100.0,
            child: new Image.asset(
              "assets/Kinderm8.png",
              height: 20.0,
            ),
          ),
        ),
//        new Padding(
//          padding: const EdgeInsets.only(top: 10.0),
//          child: new Container(
//            width: 120.0,
//            height: 120.0,
//            decoration: new BoxDecoration(
//              shape: BoxShape.circle,
//              image: new DecorationImage(
//                  image: new AssetImage("assets/Kinderm8.png"), fit: BoxFit.cover),
//            ),
//          ),
//        ),
        new Padding(
          padding: const EdgeInsets.all(5.0),
          child: new Center(
            child: new Container(
              margin: new EdgeInsets.only(
                  left: 10.0,
                  right: 10.0),
              child: new Text(
                "Please enter your email to reset your password.",
                style: new TextStyle(
                  color: Theme.Colors.forgotlabel,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w300,
//              letterSpacing: 0.3,
                ),
              ),
            ),
          ),
        ),
        new Form(
          key: formKey,
          child: new Column(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(15.0),
//                child: new TextFormField(
//                  onSaved: (val) => email = val,
//                  validator: validateEmail,
////                  validator: (val) {
////                    return val.length < 10
////                        ? "Username must have atleast 10 chars"
////                        : null;
////                  },
//                  decoration: new InputDecoration(labelText: "Email",),
//                  keyboardType: TextInputType.emailAddress,
//                ),
//              ),
                child: new TextFormField(
                  onSaved: (val) => email = val,
                  validator: validateEmail,
                  style: new TextStyle(
                      color: Theme.Colors.logintext, fontSize: 18.0),
                  decoration: new InputDecoration(
                    prefixIcon: new Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: new Icon(
                        Icons.email,
                        size: 20.0,
                      ),
                    ),
                    contentPadding: EdgeInsets.all(12.0),
                    labelText: "Email",
                    labelStyle: new TextStyle(
                        fontSize: 20.0, color: Theme.Colors.textboxborder),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0),
                        borderSide:
                            new BorderSide(color: Theme.Colors.textboxborder)),
                  ),
                ),
              ),
            ],
          ),
        ),
        _isLoading ? new CircularProgressIndicator() : submitBtn,
      ],
    );

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          "Forgot Password",
          style: new TextStyle(
            color: Theme.Colors.app_white,
            fontWeight: FontWeight.w300,
//              letterSpacing: 0.3,
          ),
        ),
        centerTitle: true,
        backgroundColor: Theme.Colors.appBarGradientEnd,
      ),
      key: scaffoldKey,
      body: SingleChildScrollView(
        child: new Container(
//        decoration: new BoxDecoration(
//          image: new DecorationImage(
//              image: new AssetImage("assets/Kinderm8_kindergarten.jpg"),
//              fit: BoxFit.cover),
//        ),
          child: new Center(
            child: new Container(
              child: Column(
                children: <Widget>[
                  forgetForm,
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(value),
      backgroundColor: Colors.redAccent,
    ));
  }

  submit() {
    final form = formKey.currentState;
    if (form.validate()) {
      print('_isvalidEmail >>>$_isvalidEmail');
      if (!_isvalidEmail) {
        setState(() {
          _isLoading = true;
          form.save();
          resetPassword(email);
        });
      }
    }
  }

  void _showDialog(text) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Password Reset"),
          content: new Text(text),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/login', (Route<dynamic> route) => false);
              },
            )
          ],
        );
      },
    );
  }

  resetPassword(email) {
    final LOGIN_URL = 'https://apicare.carem8.com/v2.1.0/forgotpassword';

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.post(LOGIN_URL, body: {"email": email}).then((res) {
      print("res $res");
      List decodedList = json.decode(res);
      print("*****");
      print(decodedList.length);
      print(decodedList);

      try {
        if (decodedList.length != 0 && decodedList[0] == 333) {
          print("Please check your email address and try again");
          setState(() {
            _showDialog("Please check your email address and try again.");
          });
        } else if (decodedList.length != 0 && decodedList[0] == true) {
          print("Your password was reset successfully,Please check your email");
          setState(() {
            _showDialog(
                "Your password was reset successfully,Please check your email.");
          });
        } else if (decodedList.length != 0 && decodedList[0] == false) {
          print("Something went wrong,Please try again later");
          setState(() {
            _showDialog("Somthing went wrong,Please try again later.");
          });
        } else if (decodedList.length >= 0 && decodedList[0] != 333) {
          print("Choose your center...");
          setState(() {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) =>
                        new MultipleCenter(email, decodedList)));
          });
        } else {
          print("error happened..");
          return null;
        }
      } catch (e) {
        print(e);
      }

      setState(() {
        _isLoading = false;
      });
    });
  }
}
