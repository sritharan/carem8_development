// To parse this JSON data, do
//
//     final forgetCenter = forgetCenterFromJson(jsonString);

import 'dart:convert';

ForgetCenter forgetCenterFromJson(String str) {
  final jsonData = json.decode(str);
  return ForgetCenter.fromJson(jsonData);
}

String forgetCenterToJson(ForgetCenter data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class ForgetCenter {
  int id;
  String fullname;
  String secondEmail;
  String username;
  String password;
  String isAdmin;
  String isStaff;
  String email;
  String image;
  String status;
  String loginaccess;
  String needSecEmail;
  String clientId;
  String center;
  Client client;
  bool isparent;

  ForgetCenter({
    this.id,
    this.fullname,
    this.secondEmail,
    this.username,
    this.password,
    this.isAdmin,
    this.isStaff,
    this.email,
    this.image,
    this.status,
    this.loginaccess,
    this.needSecEmail,
    this.clientId,
    this.center,
    this.client,
    this.isparent,
  });

  factory ForgetCenter.fromJson(Map<String, dynamic> json) => new ForgetCenter(
    id: json["id"],
    fullname: json["fullname"],
    secondEmail: json["second_email"],
    username: json["username"],
    password: json["password"],
    isAdmin: json["isAdmin"],
    isStaff: json["isStaff"],
    email: json["email"],
    image: json["image"],
    status: json["status"],
    loginaccess: json["loginaccess"],
    needSecEmail: json["need_sec_email"],
    clientId: json["client_id"],
    center: json["center"],
    client: Client.fromJson(json["client"]),
    isparent: json["isparent"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fullname": fullname,
    "second_email": secondEmail,
    "username": username,
    "password": password,
    "isAdmin": isAdmin,
    "isStaff": isStaff,
    "email": email,
    "image": image,
    "status": status,
    "loginaccess": loginaccess,
    "need_sec_email": needSecEmail,
    "client_id": clientId,
    "center": center,
    "client": client.toJson(),
    "isparent": isparent,
  };
}

class Client {
  int id;
  String clientid;
  String name;
  String connectionString;
  String authUri;
  bool kiosk;
  bool active;
  String pincode;
  String logo;
  bool kisokparentemailnotification;
  bool signature;
  bool centerLoginRequired;
  String timezone;
  String kinderm8Version;
  bool parentKioskEnable;
  bool staffKioskEnable;
  bool enableAllRoomsChildChecking;
  bool enableAllRoomsStaffChecking;
  bool staffap;

  Client({
    this.id,
    this.clientid,
    this.name,
    this.connectionString,
    this.authUri,
    this.kiosk,
    this.active,
    this.pincode,
    this.logo,
    this.kisokparentemailnotification,
    this.signature,
    this.centerLoginRequired,
    this.timezone,
    this.kinderm8Version,
    this.parentKioskEnable,
    this.staffKioskEnable,
    this.enableAllRoomsChildChecking,
    this.enableAllRoomsStaffChecking,
    this.staffap,
  });

  factory Client.fromJson(Map<String, dynamic> json) => new Client(
    id: json["id"],
    clientid: json["clientid"],
    name: json["name"],
    connectionString: json["connectionString"],
    authUri: json["AuthURI"],
    kiosk: json["kiosk"],
    active: json["active"],
    pincode: json["pincode"],
    logo: json["logo"],
    kisokparentemailnotification: json["kisokparentemailnotification"],
    signature: json["signature"],
    centerLoginRequired: json["center_login_required"],
    timezone: json["timezone"],
    kinderm8Version: json["kinderm8version"],
    parentKioskEnable: json["parent_kiosk_enable"],
    staffKioskEnable: json["staff_kiosk_enable"],
    enableAllRoomsChildChecking: json["enable_all_rooms_child_checking"],
    enableAllRoomsStaffChecking: json["enable_all_rooms_staff_checking"],
    staffap: json["staffap"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "clientid": clientid,
    "name": name,
    "connectionString": connectionString,
    "AuthURI": authUri,
    "kiosk": kiosk,
    "active": active,
    "pincode": pincode,
    "logo": logo,
    "kisokparentemailnotification": kisokparentemailnotification,
    "signature": signature,
    "center_login_required": centerLoginRequired,
    "timezone": timezone,
    "kinderm8version": kinderm8Version,
    "parent_kiosk_enable": parentKioskEnable,
    "staff_kiosk_enable": staffKioskEnable,
    "enable_all_rooms_child_checking": enableAllRoomsChildChecking,
    "enable_all_rooms_staff_checking": enableAllRoomsStaffChecking,
    "staffap": staffap,
  };
}
