import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:carem8/models/user.dart';
import 'package:carem8/pages/home/home_presenter.dart';
import 'package:carem8/utils/commonutils/progress.dart';
import 'package:carem8/Theme.dart' as Theme;
import 'package:carem8/utils/network_util.dart';

class LearningStoryComment extends StatefulWidget {
  final singleData;
  final jwt;
  LearningStoryComment(this.singleData, this.jwt);
  @override
  LearningStoryCommentState createState() => LearningStoryCommentState();
}

class LearningStoryCommentState extends State<LearningStoryComment>
    implements HomePageContract {
  var learningstoryId;
  var commentData, commentList;
  bool deleteLoading = true;
  var deleteUserId, submitUserId, suMainUserId;
  bool submission = true;
  bool mainSubmit = true;

  var appuser, jwt, userId, clientId;
  HomePagePresenter _presenter;

  final formKey1 = new GlobalKey<FormState>();
  final formKey2 = new GlobalKey<FormState>();
  bool load = false;
  var image, fullname, singlecomment, date, commentlist, cliId;
  final TextEditingController _textController1 = new TextEditingController();
  final TextEditingController _textController2 = new TextEditingController();
  var commentId, repliedCommentId;
  bool isreply = false;
  var showFieldId;

  LearningStoryCommentState() {
    _presenter = new HomePagePresenter(this);
    _presenter.getUserInfo();
  }

  var progress = new ProgressBar(
//    backgroundColor: Theme.Colors.progressbackground ,
    color: Theme.Colors.appcolour,
    containerColor: Theme.Colors.appcolour,
    borderRadius: 5.0,
    text: 'Loading...',
  );

  getCommentData() {
    print("getCommentData");
    String getCommentUrl =
        "https://apicare.carem8.com/v2.1.0/getLearningStoryComments/$learningstoryId/$userId?clientid=$clientId";
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.get(getCommentUrl, headers: headers).then((response) {
      print('res get ${response.body}');
      commentData = json.decode(response.body);

      print('map $commentData');
      print(response.statusCode);
      if (response.statusCode == 200) {
        commentList = commentData;
        print(commentList);
        _textController2.clear();
        _textController1.clear();
        setState(() {
//          deleteLoading = true;
          load = true;
        });
        mainSubmit = true;
        deleteLoading = true;
        deleteUserId = null;
        submitUserId = null;
        isreply = false;
        showFieldId = null;
        submission = true;
      } else if (response.statusCode == 500 &&
          commentData["errorType"] == 'ExpiredJwtException') {
        print("retrying...");
        getRefreshToken();
      }
    });
  }

  getRefreshToken() {
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$userId&clientid=$clientId';

    NetworkUtil _netutil = new NetworkUtil();

    _netutil.get(_refreshTokenUrl).then((response) {
      print('refresh get ${response.body}');
      var refreshJwtToken;
      try {
        refreshJwtToken = json.decode(response.body);
      } catch (e) {
        print('That string was null!');
      }
      this.jwt = refreshJwtToken;

      getCommentData();
    });
  }

//
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.Colors.appcolour,
      ),
      body: load
          ? new Container(
              child: Column(
                children: <Widget>[
                  new Expanded(
                      child: commentList.length > 0
                          ? new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Expanded(
                                  child: new Container(
                                    child: new ListView.builder(
                                      itemCount: this.commentList != null
                                          ? (this.commentList.length)
                                          : 0,
                                      itemBuilder: (context, i) {
                                        final singlecomment =
                                            this.commentList[i];

                                        var firstComments =
                                            singlecomment["comment_text"];
                                        var userDetails = singlecomment["user"];
                                        var replyComments =
                                            singlecomment["comment_reply"];
//
                                        var commentUserId = userDetails["id"];
                                        var image = userDetails["image"];
                                        var fullname = userDetails["fullname"];
//
                                        var parsedDate = DateTime.parse(
                                            singlecomment["created_at"]);
                                        var date = new DateFormat.yMMMMEEEEd()
                                            .add_jm()
                                            .format(parsedDate);

                                        commentId = singlecomment["id"];

                                        return new Center(
                                            child: Card(
                                          child: Column(
                                            children: <Widget>[
                                              new ListTile(
                                                  leading: image != null
                                                      ? CircleAvatar(
                                                          backgroundImage:
                                                              NetworkImage(
                                                                  image,
                                                                  scale: 10.0),
                                                          radius: 25.0,
                                                        )
                                                      : CircleAvatar(
                                                          backgroundImage:
                                                              AssetImage(
                                                                  "assets/nophoto.jpg"),
                                                          radius: 25.0,
                                                        ),
                                                  isThreeLine: true,
                                                  trailing:
                                                      userId == commentUserId
                                                          ? new GestureDetector(
                                                              child: deleteUserId !=
                                                                      singlecomment[
                                                                          'id']
                                                                  ? Icon(Icons
                                                                      .delete)
                                                                  : CircularProgressIndicator(
                                                                      strokeWidth:
                                                                          1.0,
                                                                    ),
                                                              onTap: () {
                                                                deleteLoading
                                                                    ? showModalBottomSheet<
                                                                            void>(
                                                                        context:
                                                                            context,
                                                                        builder:
                                                                            (BuildContext
                                                                                context) {
                                                                          return Container(
                                                                              height: 150.0,
                                                                              width: screenSize.width,
                                                                              child: Padding(
                                                                                padding: const EdgeInsets.all(32.0),
                                                                                child: Column(
                                                                                  children: <Widget>[
                                                                                    new Padding(
                                                                                      padding: const EdgeInsets.all(2.0),
                                                                                      child: new Text(
                                                                                        "Do you wish to delete",
                                                                                        style: new TextStyle(fontSize: 22.0, color: Theme.Colors.appsupportlabel, fontStyle: FontStyle.normal),
                                                                                      ),
                                                                                    ),
                                                                                    new GestureDetector(
                                                                                      onTap: () {
                                                                                        if (deleteLoading) {
                                                                                          deleteUserId = singlecomment['id'];
                                                                                          print(singlecomment);
                                                                                          print(singlecomment['id']);
                                                                                          Navigator.of(context).pop();
                                                                                          deleteComment(singlecomment['id']);
                                                                                          deleteLoading = false;
                                                                                        } else {
                                                                                          print("deleting...........");
                                                                                        }
                                                                                      },
                                                                                      child: new Container(
                                                                                        width: screenSize.width / 2,
                                                                                        margin: new EdgeInsets.only(top: 10.0, left: 5.0, right: 5.0, bottom: 10.0),
                                                                                        height: 30.0,
                                                                                        decoration: new BoxDecoration(color: Theme.Colors.buttonicon_deletecolor, borderRadius: new BorderRadius.all(new Radius.circular(10.0))),
                                                                                        child: new Center(
                                                                                            child: new Text(
                                                                                          "Delete",
                                                                                          style: new TextStyle(color: Colors.white, fontSize: 22.0),
                                                                                        )),
                                                                                      ),
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                              ));
                                                                        })
                                                                    : showModalBottomSheet<
                                                                            void>(
                                                                        context:
                                                                            context,
                                                                        builder:
                                                                            (BuildContext
                                                                                context) {
                                                                          Navigator.of(context)
                                                                              .pop();
                                                                          print(
                                                                              "deleting");
                                                                        });
                                                              },
                                                            )
                                                          : null,
//
//
//
                                                  title: new Text(
                                                    fullname,
                                                    style: new TextStyle(
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 14.0),
                                                  ),
                                                  subtitle: Container(
                                                      child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .stretch,
                                                    children: <Widget>[
                                                      Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 5.0),
                                                        child: new Text(
                                                          date,
                                                          style: new TextStyle(
                                                              color: Colors.grey
                                                                  .shade500,
                                                              fontSize: 10.0),
                                                        ),
                                                      ),
                                                      Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 5.0),
                                                        child: Row(
                                                          children: <Widget>[
                                                            Flexible(
                                                              child: Text(
                                                                  " $firstComments",
                                                                  style: new TextStyle(
                                                                      color: Colors
                                                                          .black,
                                                                      fontSize:
                                                                          12.0)),
                                                            )
                                                          ],
                                                        ),
                                                      )
                                                    ],
                                                  ))),
//
//                                  /
                                              isreply != true
                                                  ? GestureDetector(
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        children: <Widget>[
                                                          Icon(Icons.reply,size: 25.0, color: Theme.Colors.appcolour),
                                                          Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      right:
                                                                          1.0)),
                                                          Text("Reply",style: new TextStyle(
                                                            fontSize: 16.0,
                                                            color: Theme.Colors.app_dark[400],
                                                            fontWeight: FontWeight.w600,
                                                          )),
                                                          Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      right:
                                                                          5.0)),
                                                        ],
                                                      ),
                                                      onTap: () {
                                                        setState(() {
//                                                          print(
//                                                              "before $showFieldId");
                                                          isreply = true;
                                                          showFieldId =
                                                              singlecomment[
                                                                  "id"];
//                                                          print(
//                                                              "after $showFieldId");
//                                                          print(singlecomment[
//                                                              "journal_id"]);
//
//                                                          print(singlecomment[
//                                                              "id"]);
                                                        });
                                                      })
                                                  : showFieldId ==
                                                          singlecomment["id"]
                                                      ? GestureDetector(
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            children: <Widget>[
                                                              Icon(
                                                                  Icons.cancel,size: 25.0, color: Theme.Colors.appcolour),
                                                              Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          right:
                                                                              1.0)),
                                                              Text("Cancel",style: new TextStyle(
                                                                fontSize: 16.0,
                                                                color: Theme.Colors.app_dark[400],
                                                                fontWeight: FontWeight.w600,
                                                              )),
                                                              Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          right:
                                                                              5.0)),
                                                            ],
                                                          ),
                                                          onTap: () {
                                                            print(
                                                                "Cancelling..");
                                                            setState(() {
                                                              isreply = false;
                                                              showFieldId =
                                                                  null;
                                                            });
                                                          },
                                                        )
                                                      : GestureDetector(
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            children: <Widget>[
                                                              Icon(Icons.reply),
                                                              Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          right:
                                                                              1.0)),
                                                              Text("reply"),
                                                              Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          right:
                                                                              5.0)),
                                                            ],
                                                          ),
                                                          onTap: () {
                                                            setState(() {
                                                              print(
                                                                  "before $showFieldId");
                                                              isreply = true;
                                                              showFieldId =
                                                                  singlecomment[
                                                                      "id"];
//                                                              submitUserId =
//                                                                  singlecomment[
//                                                                      "id"];
                                                              print(
                                                                  "after $showFieldId");
                                                              print(
                                                                  singlecomment[
                                                                      "id"]);
                                                            });
                                                            print(
                                                                "setup and submit reply");
                                                          }),
//                                    GestureDetector(
//                                      child: Icon(Icons.reply),
//                                      onTap: () {
//                                        setState(() {
////                                                  showTextField=true;
//                                          showFieldId = singlecomment["id"];
//                                          print(showFieldId);
////                                                  print(singlecomment["id"]);
//                                        });
//                                        print("setup and submit reply");
//                                      },
//                                    ),
                                              Divider(
                                                height: 0.0,
                                              ),
//
//                                  /

                                              Container(
                                                child: Column(
                                                  children: <Widget>[
                                                    ListView.builder(
                                                      shrinkWrap: true,
                                                      physics:
                                                          ClampingScrollPhysics(),
                                                      itemCount:
                                                          replyComments != null
                                                              ? (replyComments
                                                                  .length)
                                                              : 0,
                                                      itemBuilder:
                                                          (context, index) {
                                                        final singleRepliedComment =
                                                            replyComments[
                                                                index];

                                                        var repliedComment =
                                                            singleRepliedComment[
                                                                "comment_text"];
                                                        var repliedUserDetails =
                                                            singleRepliedComment[
                                                                "user"];

//
                                                        var repliedCommentUserId =
                                                            repliedUserDetails[
                                                                "id"];
                                                        var repliedImage =
                                                            repliedUserDetails[
                                                                "image"];
                                                        var repliedFullname =
                                                            repliedUserDetails[
                                                                "fullname"];
//

//
                                                        var parseDate = DateTime.parse(
                                                            singleRepliedComment[
                                                                "created_at"]);
                                                        var repliedDate =
                                                            new DateFormat
                                                                    .yMMMMEEEEd()
                                                                .add_jm()
                                                                .format(
                                                                    parseDate);

                                                        repliedCommentId =
                                                            singleRepliedComment[
                                                                "id"];

                                                        return new Center(
                                                            child: Column(
                                                          children: <Widget>[
                                                            new ListTile(
                                                                leading: repliedImage !=
                                                                        null
                                                                    ? CircleAvatar(
                                                                        backgroundImage: NetworkImage(
                                                                            repliedImage,
                                                                            scale:
                                                                                10.0),
                                                                        radius:
                                                                            25.0,
                                                                      )
                                                                    : CircleAvatar(
                                                                        backgroundImage:
                                                                            AssetImage("assets/nophoto.jpg"),
                                                                        radius:
                                                                            25.0,
                                                                      ),
                                                                isThreeLine:
                                                                    true,
                                                                trailing: userId ==
                                                                        repliedCommentUserId
                                                                    ? new GestureDetector(
                                                                        child: deleteUserId !=
                                                                                singleRepliedComment['id']
                                                                            ? Icon(Icons.delete)
                                                                            : CircularProgressIndicator(
                                                                                strokeWidth: 1.0,
                                                                              ),
                                                                        onTap:
                                                                            () {
                                                                          deleteLoading
                                                                              ? showModalBottomSheet<void>(
                                                                                  context: context,
                                                                                  builder: (BuildContext context) {
                                                                                    return Container(
                                                                                        height: 150.0,
                                                                                        width: screenSize.width,
                                                                                        child: Padding(
                                                                                          padding: const EdgeInsets.all(32.0),
                                                                                          child: Column(
                                                                                            children: <Widget>[
                                                                                              new Padding(
                                                                                                padding: const EdgeInsets.all(2.0),
                                                                                                child: new Text(
                                                                                                  "Do you wish to delete",
                                                                                                  style: new TextStyle(fontSize: 22.0, color: Theme.Colors.appsupportlabel, fontStyle: FontStyle.normal),
                                                                                                ),
                                                                                              ),
                                                                                              new GestureDetector(
                                                                                                onTap: () {
                                                                                                  if (deleteLoading) {
                                                                                                    deleteUserId = singleRepliedComment['id'];
                                                                                                    print(singleRepliedComment);
                                                                                                    print(singleRepliedComment['id']);
                                                                                                    Navigator.of(context).pop();
                                                                                                    deleteComment(singleRepliedComment['id']);
                                                                                                    deleteLoading = false;
                                                                                                  } else {
                                                                                                    print("deleting...........");
                                                                                                  }
                                                                                                },
                                                                                                child: new Container(
                                                                                                  width: screenSize.width / 2,
                                                                                                  margin: new EdgeInsets.only(top: 10.0, left: 5.0, right: 5.0, bottom: 10.0),
                                                                                                  height: 30.0,
                                                                                                  decoration: new BoxDecoration(color: Theme.Colors.buttonicon_deletecolor, borderRadius: new BorderRadius.all(new Radius.circular(10.0))),
                                                                                                  child: new Center(
                                                                                                      child: new Text(
                                                                                                    "Delete",
                                                                                                    style: new TextStyle(color: Colors.white, fontSize: 22.0),
                                                                                                  )),
                                                                                                ),
                                                                                              ),
                                                                                            ],
                                                                                          ),
                                                                                        ));
                                                                                  })
                                                                              : showModalBottomSheet<void>(
                                                                                  context: context,
                                                                                  builder: (BuildContext context) {
                                                                                    print("deleting");
                                                                                  });
                                                                        },
                                                                      )
                                                                    : null,
                                                                title: new Text(
                                                                  repliedFullname,
                                                                  style: new TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500,
                                                                      fontSize:
                                                                          14.0),
                                                                ),
                                                                subtitle:
                                                                    Container(
                                                                        child:
                                                                            Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .stretch,
                                                                  children: <
                                                                      Widget>[
                                                                    Container(
                                                                      padding: EdgeInsets
                                                                          .only(
                                                                              top: 5.0),
                                                                      child:
                                                                          new Text(
                                                                        repliedDate,
                                                                        style: new TextStyle(
                                                                            color:
                                                                                Colors.grey.shade500,
                                                                            fontSize: 10.0),
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      padding: EdgeInsets
                                                                          .only(
                                                                              top: 5.0),
                                                                      child:
                                                                          Row(
                                                                        children: <
                                                                            Widget>[
                                                                          Flexible(
                                                                            child:
                                                                                Text(" $repliedComment", style: new TextStyle(color: Colors.black, fontSize: 12.0)),
                                                                          )
                                                                        ],
                                                                      ),
                                                                    )
                                                                  ],
                                                                ))),
                                                            Divider(),
                                                          ],
                                                        ));
                                                      },
                                                    ),
                                                    showFieldId ==
                                                            singlecomment["id"]
                                                        ? new Container(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    5.0),
                                                            child: new Row(
                                                              children: <
                                                                  Widget>[
                                                                new Flexible(
                                                                  child: new Form(
                                                                      key: formKey2,
                                                                      child: TextFormField(
                                                                        autofocus:
                                                                            true,
                                                                        maxLines:
                                                                            1,
                                                                        controller:
                                                                            _textController2,
                                                                        validator:
                                                                            (val) {
                                                                          return val.length == 0
                                                                              ? "Comment must not be null"
                                                                              : null;
                                                                        },
                                                                        decoration: new InputDecoration(
                                                                            fillColor: Colors.grey[300],
                                                                            contentPadding: new EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 10.0),
                                                                            border: InputBorder.none,
//                                                                            border: new OutlineInputBorder(
//                                                                              borderRadius: new BorderRadius.circular(0.0),
//                                                                            ),
                                                                            hintText: "Reply Here"),
                                                                      )),
                                                                ),
                                                                new RaisedButton(
                                                                  onPressed:
                                                                      () {
                                                                    if (submission) {
                                                                      print(singlecomment[
                                                                          "id"]);

                                                                      setState(
                                                                          () {
                                                                        submitUserId =
                                                                            singlecomment["id"];
                                                                      });
                                                                      submission =
                                                                          false;
                                                                      submitReplyComment(
                                                                          singlecomment[
                                                                              "id"]);
                                                                      print(
                                                                          submission);
                                                                    } else {
                                                                      print(
                                                                          "submitting !$submission");
                                                                    }
                                                                  },
                                                                  child: submitUserId !=
                                                                          singlecomment[
                                                                              "id"]
                                                                      ? Column(
                                                                          // Replace with a Row for horizontal icon + text
                                                                          children: <
                                                                              Widget>[
                                                                            Icon(
                                                                              Icons.send,
                                                                              color: Theme.Colors.buttonicon_darkcolor,
                                                                            ),
                                                                            Text("Submit",
                                                                                style: new TextStyle(fontSize: 12.0, color: Theme.Colors.buttonicon_color))
                                                                          ],
                                                                        )
                                                                      : CircularProgressIndicator(
                                                                          strokeWidth:
                                                                              2.0,
                                                                        ),
                                                                  color: Theme
                                                                      .Colors
                                                                      .appmenuicon,
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(
                                                                              10.0),
                                                                )
                                                              ],
                                                            ),
                                                          )
                                                        : Container(),
                                                  ],
                                                ),
                                                padding: EdgeInsets.only(
                                                    left: screenSize.width / 8),
                                              ),
                                            ],
                                          ),
                                        ));

                                        ////
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            )
                          : Center(
                              child: Text("No Comments yet."),
                            )),
//
//
//
//
                  !isreply
                      ? new Container(
                          decoration: BoxDecoration(
                            border: new Border.all(
                                color: Theme.Colors.appcolour,
                                width: 1.0,
                                style: BorderStyle.solid),
//                            borderRadius:
//                                new BorderRadius.all(new Radius.circular(20.0)),
                            borderRadius: new BorderRadius.only(
                              topLeft: new Radius.circular(20.0),
                              topRight: new Radius.circular(20.0),
                          )
                          ),
//                          padding: EdgeInsets.all(5.0),
                          child: new Row(
                            children: <Widget>[
                              new Flexible(
                                child: new Form(
                                    key: formKey1,
                                    child: TextFormField(
                                      maxLines: 1,
                                      controller: _textController1,
                                      validator: (val) {
                                        return val.length == 0
                                            ? "Comment must not be null"
                                            : null;
                                      },
                                      decoration: new InputDecoration(
//                                  border:
                                        fillColor: Colors.grey[300],
//                                  filled: true,
                                        contentPadding: new EdgeInsets.fromLTRB(
                                            10.0, 30.0, 10.0, 10.0),
                                        border: InputBorder.none,
//                                        border: new OutlineInputBorder(
//                                          borderRadius:
//                                              new BorderRadius.circular(0.0),
//                                        ),
                                      ),
                                    )),
                              ),
                              new RaisedButton(
                                onPressed: () {
                                  if (mainSubmit) {
                                    setState(() {
                                      mainSubmit = false;
                                    });
                                    submitMainComment();
//                                    submission = false;

                                  } else {

                                    print("main submission $mainSubmit");
                                  }
                                },
                                shape: new RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.only(
                                        topRight: new Radius.circular(20.0),
//                                        bottomRight: new Radius.circular(20.0)
                                    )
                                ),
                                color: Theme.Colors.appmenuicon,
                                padding: EdgeInsets.all(10.0),
                                child: mainSubmit
                                    ? Container(
                                      child: Column(
                                          // Replace with a Row for horizontal icon + text
                                          children: <Widget>[
                                            Icon(
                                              Icons.send,
                                              color: Theme
                                                  .Colors.buttonicon_darkcolor,
                                            ),
                                            Text("Submit",
                                                style: new TextStyle(
                                                    fontSize: 12.0,
                                                    color: Theme
                                                        .Colors.buttonicon_color))
                                          ],
                                        ),
                                    )
                                    : CircularProgressIndicator(
                                        strokeWidth: 2.0,
                                      ),
                              )
                            ],
                          ),
                        )
                      : Container(),
                ],
              ),
            )
          : progress,
    );
  }

  void deleteComment(deletecommentid) async {
    print("###############$deletecommentid##### userId $userId");
    setState(() {});
    String deleteUrl =
        'http://13.210.72.124:7070/v2.1.0/deleteLearningStoryComments?clientid=$clientId';
    var body = {"id": deletecommentid, "user_id": userId};
    var headers = {"x-authorization": jwt.toString()};

    NetworkUtil _netutil = new NetworkUtil();
    _netutil.delete(deleteUrl, headers: headers, body: body).then((res) {
      print(res);

      getCommentData();
    });
  }

//
//
  submitReplyComment(cm_id) {
    final form = formKey2.currentState;
    if (form.validate()) {
//      setState(() => load = false);
      form.save();
      final submitcommentUrl =
          'http://13.210.72.124:7070/v2.1.0/createLearningStoryComments?clientid=$clientId';
      var body = {
        "story_id": learningstoryId,
        "user_id": userId,
        "parent_cm_id": cm_id,
        "comment_text": _textController2.text
      };
      var headers = {"x-authorization": jwt.toString()};

      NetworkUtil _netUtil = new NetworkUtil();
      _netUtil
          .post(submitcommentUrl,
              body: body, headers: headers, encoding: jwt.toString())
          .then((res) {
        print(res);
//        var result = json.decode(res);
        getCommentData();
      });
    } else {
      print("else");
      submitUserId = null;
      submission = true;
    }
  }

//
  submitMainComment() {
    final form = formKey1.currentState;
    if (form.validate()) {
      form.save();

      final submitcommentUrl =
          'http://13.210.72.124:7070/v2.1.1/createLearningStoryComments?clientid=$clientId';
      var body = {
        "story_id": learningstoryId,
        "user_id": userId,
        "parent_cm_id": 0,
        "comment_text": _textController1.text
      };
      var headers = {"x-authorization": jwt.toString()};

      NetworkUtil _netUtil = new NetworkUtil();
      _netUtil
          .post(submitcommentUrl,
              body: body, headers: headers, encoding: jwt.toString())
          .then((res) {
        print(res);
//        var result = json.decode(res);
//        _textController1.clear();
        getCommentData();
      });
    } else {
      print("else");
      mainSubmit = true;
    }
  }

//
  @override
  void onDisplayUserInfo(User user) {
    learningstoryId = widget.singleData["id"];

    appuser = user.center;
    try {
      final parsed = json.decode(appuser);
      var appusers = parsed[0];
      print(appusers);
      jwt = widget.jwt;
      print("******$jwt");
      var users = appusers["user"];
      clientId = users["client_id"];
      userId = users["id"];

      print("iddd $userId");
      print(clientId);
      getCommentData();
    } catch (e) {
      print(e);
      print("That string didn't look like Json.");
    }
  }

  @override
  void onErrorUserInfo() {}

  @override
  void onLogoutUser() {}
}
