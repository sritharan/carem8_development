import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:carem8/pages/learningstory/detail_webview.dart';
import 'package:carem8/pages/learningstory/learningstorycomment.dart';
import 'package:carem8/pages/newsfeed/learningoutcomes.dart';
import 'package:carem8/pages/newsfeed/learningtags.dart';
import 'package:carem8/pages/observation/observationcomment.dart';
import 'package:photo_view/photo_view.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:carem8/Theme.dart' as Theme;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
//import 'package:flutter_native_web/flutterwebview.dart';

//import 'package:device_calendar/device_calendar.dart';
import 'Package:intl/Date_symbol_data_local.Dart';
import 'package:date_format/date_format.dart';
//
//import 'package:flutter_inappbrowser/flutter_inappbrowser.dart';

class LearningStoryDetails extends StatefulWidget {
  final singlestorydata;
  final jwt;
  LearningStoryDetails(this.singlestorydata, this.jwt);
  @override
  _LearningStoryState createState() {
    return new _LearningStoryState(singlestorydata,jwt);
  }
}

class _LearningStoryState extends State<LearningStoryDetails>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  ScrollController scrollController;
  Animation<Color> colorTween1;
  Animation<Color> colorTween2;

  final singlestorydata,jwt;
  _LearningStoryState(this.singlestorydata,this.jwt);

  var learningTag, learningOutcomeDetail;

//  DeviceCalendarPlugin _deviceCalendarPlugin;

  final double statusBarSize = 24.0;
  final double imageSize = 264.0;
  double readPerc = 0.0;


  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
//  Calendar _calendar;


  var color, splitColor, tag;
  int colorCode;

//  WebController webController;
// WebviewScaffold webviewScaffold;
//  void onWebCreated(webController) {
//    this.webController = webController;
//    this.webController.loadUrl("https://veed.me/blog/favorite-video-marketing-campaigns-bonus-meet-team/");
//    this.webController.onPageStarted.listen((url) =>
//        print("Loading $url")
//    );
//    this.webController.onPageFinished.listen((url) =>
//        print("Finished loading $url")
//    );
//  }




//  var now = DateTime.now();
//  var berlinWallFell = DateTime.utc(1989, 11, 9);
//  var moonLanding = DateTime.parse("1969-07-20 20:18:04Z");

  // Event
//  Event _event;
  DateTime _startDate;
  TimeOfDay _startTime;

  DateTime _endDate;
  TimeOfDay _endTime;

  bool _autovalidate = false;

  @override
  void initState() {
    super.initState();
  }

//  InAppWebViewController webView;
  String url = "";
  double progress = 0;


//  learningTags() {
//    learningTag = singlestorydata["learning_tags"];
//    learningOutcomeDetail = singlestorydata["outcomes"];
//    if (learningTag.length > 5) {
//      for (int i = 0; i < 5; i++) {
//        return Container(
//          width: 60.0,
//          height: 25.0,
//          child: ListView.builder(
//              scrollDirection: Axis.horizontal,
//              itemCount: 5,
//              itemBuilder: (context, i) {
//                var color = learningTag[i]["color_code"].toString();
//                var splitColor = '0xFF' + color.substring(1);
//                int colorCode = int.parse(splitColor);
//                return Container(
//                  child: new CircleAvatar(
//                    backgroundColor: Color(colorCode),
//                    radius: 6.0,
//                  ),
//                );
//              }),
//        );
//      }
//    } else {
//      for (int i = 0; i < learningTag.length; i++) {
//        return Container(
//          width: 60.0,
//          height: 25.0,
//          child: ListView.builder(
//              scrollDirection: Axis.horizontal,
//              itemCount: learningTag.length,
//              itemBuilder: (context, i) {
//                var color = learningTag[i]["color_code"].toString();
//                var splitColor = '0xFF' + color.substring(1);
//                int colorCode = int.parse(splitColor);
//                return new CircleAvatar(
//                  backgroundColor: Color(colorCode),
//                  radius: 6.0,
//                );
//              }),
//        );
//      }
//    }
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: const EdgeInsets.all(5.0),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.blueAccent)
                  ),
                  child: DetailWebView("https://veed.me/blog/favorite-video-marketing-campaigns-bonus-meet-team/", singleStoryData: singlestorydata, jwt: jwt,),
//                  InAppWebView(
//                    initialUrl: "https://veed.me/blog/favorite-video-marketing-campaigns-bonus-meet-team/",
//                    initialHeaders: {
//
//                    },
//                    initialOptions: {
//
//                    },
//                    onWebViewCreated: (InAppWebViewController controller) {
//                      webView = controller;
//                    },
//                    onLoadStart: (InAppWebViewController controller, String url) {
//                      print("started $url");
//                      setState(() {
//                        this.url = url;
//                      });
//                    },
//                    onProgressChanged: (InAppWebViewController controller, int progress) {
//                      setState(() {
//                        this.progress = progress/100;
//                        print(this.progress);
//                      });
//                    },
                  ),
                ),
            ],
              ),
          ),
//        ),
      );
  }

  @override
  Widget ___build(BuildContext context) {
    var S3URL = "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/";
//    final flutterWebviewPlugin = new FlutterWebviewPlugin();
    String url = "https://veed.me/blog/favorite-video-marketing-campaigns-bonus-meet-team/";
//    flutterWebviewPlugin.launch(url,
//      rect: new Rect.fromLTWH(
//        0.0,
//        0.0,
//        MediaQuery.of(context).size.width,
//        300.0,
//      ),
//    );
    final Size screenSize = MediaQuery.of(context).size;

//    FlutterWebView flutterWebView = new FlutterWebView(
//      onWebCreated: onWebCreated,
//    );

    return  new Scaffold(
      appBar: AppBar(title: Container(
        height: 20.0,
          child: new Text("Learnig story detail")
      ),
        backgroundColor: Theme.Colors.appcolour,
      ),
      body: new SingleChildScrollView(
//        child: new WebviewScaffold(
//          url: url,
//          appBar: AppBar(title: new Text("Learnig story detail"),
//          backgroundColor: Theme.Colors.appcolour,
//          ),
//          withZoom: true,
//          withLocalStorage: true,
//
//        ),
//      child: new Scaffold(
////          appBar: AppBar(title: new Text("Learnig story detail"),
////          backgroundColor: Theme.Colors.appcolour,
////          ),
////          body: flutterWebView,
//        ),
      ),
    );
  }

  @override
  Widget _build(BuildContext context) {
    var S3URL = "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/";
    final flutterWebviewPlugin = new FlutterWebviewPlugin();
    String url = "https://veed.me/blog/favorite-video-marketing-campaigns-bonus-meet-team/";
    flutterWebviewPlugin.launch(url,
      rect: new Rect.fromLTWH(
        0.0,
        0.0,
        MediaQuery.of(context).size.width,
        300.0,
      ),
    );
    final Size screenSize = MediaQuery.of(context).size;

    return new Scaffold(
        body: new Hero(
          tag: 'Detailprogram',
          child: new Material(
            color: Colors.white,
            child:
            new Stack // I need to use a stack because otherwise the appbar blocks the content even if transparent
              (
              children: <Widget>[
                /// Image and text
                new ListView(
                  padding: new EdgeInsets.all(0.0),
                  controller: scrollController,
                  children: <Widget>[
                  // https://veed.me/blog/favorite-video-marketing-campaigns-bonus-meet-team/
                    new SingleChildScrollView(
                      child: new WebviewScaffold(
                        url: url,
//                        appBar: new AppBar(
//                          title: const Text('Widget webview'),
//                        ),
                        withZoom: true,
                        withLocalStorage: true,
//                        hidden: true,
                      ),
                    ),
                  ],
                ),

                /// Appbar
                new Align(
                  alignment: Alignment.topCenter,
                  child: new SizedBox.fromSize(
                    size: new Size.fromHeight(90.0),
                    child: new Stack(
                      alignment: Alignment.centerLeft,
                      children: <Widget>[
                        new SizedBox.expand // If the user scrolled over the image
                          (
                          child: new Material(
                            color: colorTween1.value,
                          ),
                        ),
                        new SizedBox.fromSize // TODO: Animate the reveal
                          (
                          size: new Size.fromWidth(
                              MediaQuery.of(context).size.width * readPerc),
                          child: new Material(
                            color: colorTween2.value,
                          ),
                        ),
                        new Align(
                            alignment: Alignment.center,
                            child: new SizedBox.expand(
                              child:
                              new Material // So we see the ripple when clicked on the iconbutton
                                (
                                color: Colors.transparent,
                                child: new Container(
                                    margin: new EdgeInsets.only(
                                        top: 24.0), // For the status bar
                                    child: new Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        new IconButton(
                                          onPressed: () =>
                                              Navigator.of(context).pop(),
                                          icon: new Icon(Icons.arrow_back,
                                              color: Colors.white),
                                        ),
                                        Center(
                                          child: new Text("Learning story detail view",
                                              textAlign: TextAlign.center,
                                              style: new TextStyle(
                                                  fontSize: 14.0,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w700)),
                                        ),
                                      ],
                                    )),
                              ),
                            ))
                      ],
                    ),
                  ),
                ),

                /// Fade bottom text
                new Align(
                  alignment: Alignment.bottomCenter,
                  child: new SizedBox.fromSize(
                      size: new Size.fromHeight(100.0),
                      child: new Container(
                        decoration: new BoxDecoration(
                            gradient: new LinearGradient(
                                colors: [Colors.white12, Colors.white],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                stops: [0.0, 1.0])),
                      )),
                )
              ],
            ),
          ),
        ));
  }

  String _validateTitle(String value) {
    if (value.isEmpty) {
      return 'Name is required.';
    }

    return null;
  }


}
