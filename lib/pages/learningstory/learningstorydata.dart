import 'package:cached_network_image/cached_network_image.dart';
import 'package:carem8/utils/commonutils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:carem8/Theme.dart' as Theme;
import 'package:html_unescape/html_unescape.dart';
import 'package:carem8/pages/learningstory/detail_webview.dart';
import 'package:carem8/pages/learningstory/detaillearningstory.dart';
import 'package:carem8/pages/learningstory/learningstorycomment.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:flutter_html_textview/flutter_html_textview.dart';
import 'package:carem8/utils/commonutils/utils.dart' as util;

class LearningStoryData extends StatelessWidget {
  final singleData;
  final jwt;

  LearningStoryData(this.singleData, this.jwt);

  @override
  Widget build(BuildContext context) {
    var S3URL = "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/";
    final Size screenSize = MediaQuery.of(context).size;
    var unescape = new HtmlUnescape();
//    var fromData = unescape.convert(singleData['id']);
//    var convertedData = fromData.substring(5, fromData.length - 6);
//    var observation = convertedData.split('&');

    var learningstory_title = singleData["id"];
    var date = singleData["date"];
    var parsedDate = DateTime.parse(date);
    var convertedDate = new DateFormat.yMMMMEEEEd().add_jm().format(parsedDate);

    var created_at = convertedDate.split(' ');
    String learningdata_firstHalf;
    String learningdata_secondHalf;
    var learningdata_html, learningdata_data;
    if (singleData != '') {
      if (singleData['data'] != null) {
        /* // HTML to Text */
        learningdata_html = html2md.convert(singleData['data']);
        learningdata_data = HtmlTextView(data: learningdata_html);

        if (learningdata_html.length > 150) {
          learningdata_firstHalf = learningdata_html.substring(0, 150);
          learningdata_secondHalf =
              learningdata_html.substring(150, learningdata_html.length);
        } else {
          learningdata_firstHalf = learningdata_html;
          learningdata_secondHalf = "";
        }
      }
    }
    var learningdata = html2md.convert(singleData['data']);


    if (singleData['settings'] != null) {
      var setting = singleData['settings'];

      var attarray = setting.split(';');
      print(attarray);
      var setting_bg = attarray;
    }

    var LS_title = learningdata.length > 150 ? Html(data: learningdata.substring(0, 150) + '...') : Html(data: learningdata);
    return GestureDetector(
      onTap: () {
        print("navigate to detail view..");
        Navigator.of(context).push(new MaterialPageRoute<Null>(
          builder: (BuildContext context) {
//            return LearningStoryDetails(singleData, jwt);//TODO: I have removed learningstory details file reference here, if you need add this
            return DetailWebView("https://veed.me/blog/favorite-video-marketing-campaigns-bonus-meet-team/", singleStoryData: singleData, jwt: jwt,);
          },
        ));
      },
      child: Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Container(
          decoration: BoxDecoration(color: Colors.white),
//          decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
          child: Column(
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
//                  new Container(child: new Text(singleData.toString())),
//                  Container(
//                      margin: const EdgeInsets.only(right: 15.0),
//                      width: 100.0,
//                      height: 100.0,
//                      child: singleData["journeygallery"].length > 0
//                          ? new CachedNetworkImage(
//                              fit: BoxFit.fitHeight,
//                              imageUrl: S3URL +
//                                  singleData["journeygallery"][0]['url'],
//                              placeholder: new CupertinoActivityIndicator(),
//                              errorWidget:
//                                  new Image.asset("assets/nophoto.jpg"),
//                              fadeOutDuration: new Duration(seconds: 1),
//                              fadeInDuration: new Duration(seconds: 3),
//                            )
//                          : Center(
//                              child: new Column(
//                                mainAxisSize: MainAxisSize.min,
//                                children: <Widget>[
//                                  new Image.asset(
//                                    "assets/iconsetpng/game.png",
//                                    width: 50.0,
//                                    height: 50.0,
//                                  ),
//                                ],
//                              ),
//                            )
//                  ),
                  Expanded(
                    child: Container(
                      decoration: new BoxDecoration(
                        image: new DecorationImage(
                          image: new AssetImage("images/background.jpg"),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
//                        new Text(singleData['settings'].toString()),
                          new ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 10.0),
                              leading: Container(
                                padding: EdgeInsets.only(right: 12.0),
                                decoration: new BoxDecoration(
                                    border: new Border(
                                        right: new BorderSide(
                                            width: 1.0, color: Theme.Colors.appcolour))),
                                child: new CircleAvatar(
                                  backgroundColor: Theme.Colors.app_white,
                                  radius: 20.0,
                                  backgroundImage:
                                  singleData['educator']["image"] != null
                                      ? new NetworkImage(
                                      singleData['educator']["image"])
                                      : new AssetImage(
                                    "assets/nophoto.jpg",
                                  ),
                                ),
                              ),
                              title: Text(
                                "${singleData['educator']["fullname"]}",
                                style: TextStyle(
                                    color: Theme.Colors.appcolour,
                                    fontWeight: FontWeight.bold),
                              ),
                              // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),
                              subtitle: Row(
                                children: <Widget>[
                                  Text("on ${created_at[0].substring(0, 3)},${created_at[1].substring(0, 3)},${created_at[2]}${created_at[3]}",
                                      style: TextStyle(color: Theme.Colors.app_dark))
                                ],
                              ),
//                              trailing: Icon(Icons.keyboard_arrow_right,
//                                  color: Theme.Colors.appcolour, size: 30.0)
                          ),
//                          new WebviewScaffold(
//                              url: new Uri.dataFromString("<html><body>hello world</body></html>", mimeType: 'text/html').toString()),
                          new Container(
                              padding: new EdgeInsets.only(
                                  left: 5.0, top: 2.0, right: 2.0, bottom: 2.0),
                              height: 100.0,
                              margin: const EdgeInsets.only(right: 10.0),
                              child: learningdata.length > 150
                                  ? Html(data: learningdata.substring(0, 150) + '...')
                                  : Html(data: learningdata)
                          ),
                          new Padding(
                            padding: EdgeInsets.only(left: 10.0),
                            child: new Chip(
//                              avatar: new CircleAvatar(
//                                backgroundColor: Theme.Colors.app_white,
//                                radius: 30.0,
//                                backgroundImage:
//                                    singleData['educator']["image"] != null
//                                        ? new NetworkImage(
//                                            singleData['educator']["image"])
//                                        : new AssetImage(
//                                            "assets/nophoto.jpg",
//                                          ),
//                              ),
                              label: Text(
                                timesAgo(parsedDate), style: TextStyle(color: Theme.Colors.app_white),
                              ),
//                            backgroundColor: Color(colorCode),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
//
              Container(
                padding: EdgeInsets.all(4.0),
                color: Theme.Colors.appcolour,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
//                crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
//                        padding: EdgeInsets.only(left: 3.0),
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute<Null>(
                            builder: (BuildContext context) {
                              return LearningStoryComment(singleData, jwt);
                            },
//                                fullscreenDialog: true
                          ));
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
//                      crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
//                          Padding(padding: EdgeInsets.only(left: 10.0)),
                            new Icon(Icons.comment,
                                size: 12.0, color: Theme.Colors.app_white),
                            Container(
//                          padding: EdgeInsets.only(left: 10.0),
                              child: singleData["comment_count"] != 0
                                  ? singleData["comment_count"] == 1
                                      ? Text(
                                          "${singleData["comment_count"]} comment",
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Theme.Colors.app_white))
                                      : Text(
                                          "${singleData["comment_count"]} comments",
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Theme.Colors.app_white))
                                  : Text(' No Comments',
                                      style: TextStyle(
                                          fontSize: 12.0,
                                          color: Theme.Colors.app_white)),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
//                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Icon(FontAwesomeIcons.clock,
                            size: 12.0, color: Theme.Colors.app_white),
                        Container(
                          padding: EdgeInsets.all(2.0),
                          child: Text(
                            'on ${created_at[0].substring(0, 3)},${created_at[1].substring(0, 3)},${created_at[2]}${created_at[3]}',
                            style: TextStyle(
                                fontSize: 12.0, color: Theme.Colors.app_white),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
