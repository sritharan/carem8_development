import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:carem8/Theme.dart' as Theme;
import 'package:carem8/pages/home/data/config.dart';
import 'package:carem8/pages/learningstory/learningstorycomment.dart';
import 'package:html_unescape/html_unescape.dart';

class DetailWebView extends StatelessWidget {

  DetailWebView(this.url, {this.singleStoryData = "", this.jwt = ""});

  final String url;
  final String jwt;
  final singleStoryData;

  final flutterWebViewPlugin = FlutterWebviewPlugin();

  String loadLocal() {
    var unescape = new HtmlUnescape();
    var text = unescape.convert("&lt;div class=\"item-header\"&gt;  &lt;div class=\"row\"&gt;   &lt;div class=\"lheader\"&gt;    &lt;p&gt;drawing day &lt;/p&gt;   &lt;/div&gt;  &lt;/div&gt; &lt;/div&gt; &lt;div class=\"row\"&gt;  &lt;div class=\"col story_video\"&gt;   &lt;iframe frameborder=\"0\" allowfullscreen src=\"https://youtube.com/embed/wC4owOOHsZU\"&gt;&lt;/iframe&gt;  &lt;/div&gt; &lt;/div&gt; &lt;div class=\"row\"&gt;  &lt;div class=\"col story_video\"&gt;   &lt;iframe frameborder=\"0\" allowfullscreen src=\"https://youtube.com/embed/prDzi0XyuQ4\"&gt;&lt;/iframe&gt;  &lt;/div&gt; &lt;/div&gt; &lt;div class=\"row\"&gt;  &lt;div class=\"col story_video\"&gt;   &lt;iframe frameborder=\"0\" allowfullscreen src=\"https://youtube.com/embed/sIlDNONyrZo\"&gt;&lt;/iframe&gt;  &lt;/div&gt; &lt;/div&gt;");

    return """
      <!DOCTYPE html>
    <html>
      <body>

      <h2>HTML Images</h2>
      <p>HTML images are defined with the img tag:</p>

      <img src="https://news.usc.edu/files/2016/03/Sample_Steven-1-824x549.jpg" alt="W3Schools.com" width="104" height="142">
      $text
      </body>
    </html>
      """;
  }

  @override
  Widget build(BuildContext context) {

    return WebviewScaffold(
      url: new Uri.dataFromString(loadLocal(), mimeType: 'text/html').toString(), // maybe you Uri.dataFromString(snapshot.data, mimeType: 'text/html', encoding: Encoding.getByName("UTF-8")).toString()
      withJavascript: true,
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, 55.0),//I have changed the app bar height here you can change or remove preferredSize: Asanka
        child: AppBar(
          title: Container(
              height: 25.0,
              child: new Text(labelsConfig["learningStoryDetailLabel"])
          ),
          backgroundColor: Theme.Colors.appcolour,
          actions: <Widget>[
//          new IconButton(
//            onPressed: () =>
//                Navigator.of(context).pop(),
//            icon: new Icon(Icons.arrow_back,
//                color: Colors.white),
//          ),
//          Center(
//            child: new Text("Learnig story detail" ,style: TextStyle(
//                fontSize: 20.0,
//                color: Theme
//                    .Colors
//                    .app_white)),
//          ),
            new IconButton(
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(
                    new MaterialPageRoute<Null>(
                      builder:
                          (BuildContext context) {
                        return LearningStoryComment(
                            singleStoryData,
                            jwt);
                      },
                    ));
              },
              icon: CircleAvatar(
                backgroundColor: Colors.white,
                child: new Icon(
                  Icons.comment,
                  color: Theme
                      .Colors.appcolour,
                  size: 25.0,
                ),
              ),
            ),
          ],
        ),
      ),
      withZoom: true,
      withLocalStorage: true,
      bottomNavigationBar: BottomAppBar(
        child: Row(
          children: <Widget>[
            IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                flutterWebViewPlugin.goBack();
              },
            ),
            IconButton(
              icon: const Icon(Icons.arrow_forward_ios),
              onPressed: () {
                flutterWebViewPlugin.goForward();
              },
            ),
            IconButton(
              icon: const Icon(Icons.autorenew),
              onPressed: () {
                flutterWebViewPlugin.reload();
              },
            ),
          ],
        ),
      ),
    );
  }
}
