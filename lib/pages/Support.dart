import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carem8/Theme.dart' as Theme;

class Kinderm8Support extends StatefulWidget {
  @override
  _Kinderm8SupportState createState() => new _Kinderm8SupportState();
}

class _Kinderm8SupportState extends State<Kinderm8Support> {

  BuildContext _ctx;
  bool _isLoading = false;
  FocusNode _focusNode ;
  String aboutUs =
      "Parents can connect to their child’s learning, from anywhere, at any time. Follow what’s happening in real-time and get instant access to information, updates and reports. Track their progress and contribute to their learning and development in a meaningful way.";
  final TextEditingController _feedbackController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _focusNode = new FocusNode();
  }

  @override
  Widget build(BuildContext context) {

    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
        backgroundColor: Theme.Colors.appsupportbackground,
        appBar: new AppBar(
          title: new GestureDetector(
            onLongPress: () {},
            child: new Text(
              "Ask us anything, or share your feedback.",
              style: new TextStyle(color: Theme.Colors.appsupportheader,fontSize: 16.0,),
            ),
          ),
          centerTitle: false,
        ),
        resizeToAvoidBottomPadding: false,
        body: new SingleChildScrollView(
          child: new Column(
            children: <Widget>[
              new Container(
                height: 100.0,
                child: new Center(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
//                      new Container(
//                        margin: new EdgeInsets.only(top: 20.0, bottom: 0.0),
////                          height: 40.0,
////                          width: 40.0,
////                          child: new Image.asset(
////                              'assets/logo.png'
////                          )
//                      ),
//                      new SizedBox(
//                        height: 100.0,
//                        child: new Image.asset(
//                          "assets/Kinderm8.png",
//                          height: 20.0,
//                        ),
//                      ),
                    ],
                  ),
                ),
              ),
              new Container(
                margin: new EdgeInsets.only(left: 5.0, right: 5.0),
                decoration: new BoxDecoration(
                    color: Colors.white,
                    borderRadius: new BorderRadius.only(
                        topLeft: new Radius.circular(20.0),
                        topRight: new Radius.circular(20.0))),
//                constraints: const BoxConstraints(maxHeight: 400.0),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new SizedBox(
                      height: 150.0,
                      child: new Image.asset(
                        "assets/Kinderm8.png",
                        height: 20.0,
                      ),
                    ),
                    new Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: new Text(
                        "Hi, we're Kinder m8 ",
                        style: new TextStyle(
                            fontSize: 18.0,
                            color: Theme.Colors.appsupportlabel,
                            fontStyle: FontStyle.normal),
                      ),
                    ),
                    new Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: new Text(
                        "We are here for you...",
                        style: new TextStyle(
                            fontSize: 14.0,
                            color: Theme.Colors.appsupportlabel,
                            fontStyle: FontStyle.italic),
                      ),
                    ),
                    new Container(
                      height: 60.0,
//                      margin: new EdgeInsets.only(top: 5.0),
                      child: new Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: new Container(
                          width: screenSize.width,
                          margin: new EdgeInsets.only(
                              left: 10.0, right: 10.0, bottom: 2.0),
                          height: 60.0,
                          decoration: new BoxDecoration(
                              borderRadius: new BorderRadius.all(
                                  new Radius.circular(10.0))),
                          child: new TextFormField(
                            controller: _feedbackController,
                            style: new TextStyle(
                                color: Theme.Colors.logintext, fontSize: 18.0),
                            decoration: new InputDecoration(
//                              prefixIcon: new Padding(
//                                padding: const EdgeInsets.all(5.0),
//                                child: new Icon(
//                                  Icons.email,
//                                  size: 20.0,
//                                ),
//                              ),
//                              contentPadding: EdgeInsets.all(12.0),
                              labelText: "Your feedback",
                              labelStyle: new TextStyle(
                                  fontSize: 20.0,
                                  color: Theme.Colors.textboxborder),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: new BorderSide(
                                      color: Theme.Colors.textboxborder)),
                            ),
                            maxLines: 3,
                            onFieldSubmitted: (String textInput) {
                              FocusScope.of(context).requestFocus(_focusNode);
                            },
                          ),
                        ),
                      ),
                    ),
                    new Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                        children: <Widget>[
//                        ],
                      ),
                    ),
                    new Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                        children: <Widget>[
//                        ],
                      ),
                    ),
                    _isLoading
                        ? new CircularProgressIndicator()
                        : new InkWell(
                      onTap: () {
//                        _submit();
                        setState(() {
                          _isLoading = true;
                        });
//                        loginWithEmail(
//                            _emailController.text, _passController.text);
                      },
                      child: new Container(
                        height: 60.0,
                        margin: new EdgeInsets.only(top: 5.0),
                        child: new Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: new Container(
                            width: screenSize.width,
                            margin: new EdgeInsets.only(
                                left: 10.0, right: 10.0, bottom: 2.0),
                            height: 60.0,
                            decoration: new BoxDecoration(
                                color: Theme.Colors.loginbutton,
                                borderRadius: new BorderRadius.all(
                                    new Radius.circular(20.0))),
                            child: new Center(
                                child: new Text(
                                  "Submit",
                                  style: new TextStyle(
                                      color: Colors.white, fontSize: 20.0),
                                )),
                          ),
                        ),
                      ),
                    ),
                    new Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new Container(
                            height: 1.0,
                            width: 100.0,
                            color: Colors.black26,
                          ),
                          new Text(
                            "",
                            style: new TextStyle(
                              fontSize: 14.0,
                              color: Colors.black,
                            ),
                          ),
                          new Container(
                            height: 1.0,
                            width: 100.0,
                            color: Colors.black26,
                          ),
                        ],
                      ),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[

                        new Column(
                          children: <Widget>[
                            new Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: new Text(
                                "V 5.0",
                                style: new TextStyle(
                                    fontSize: 18.0,
                                    color: Theme.Colors.versionlabel),
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
//        body: new SingleChildScrollView(
//          child: new Container(
//            height: screenSize.height,
////            margin: new EdgeInsets.only(top: 50.0, bottom: 0.0),
////          constraints: const BoxConstraints(maxHeight: 400.0),
////          margin: new EdgeInsets.all(5.0),
//            decoration: new BoxDecoration(
//                color: Colors.white,
//                borderRadius: new BorderRadius.only(
//                    topLeft: new Radius.circular(20.0),
//                    topRight: new Radius.circular(20.0))
//            ),
//            child: new Center(
//                child: new Column(
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  children: <Widget>[
//                    new SizedBox(
//                      height: 100.0,
//                      child: new Image.asset("assets/logo.png"),
//                    ),
//                    new Padding(
//                      padding: const EdgeInsets.all(2.0),
//                      child: new Text(
//                        "Hi, we're Kinder m8 ",
//                        style: new TextStyle(
//                            fontSize: 18.0,
//                            color: Theme.Colors.appsupportlabel,
//                            fontStyle: FontStyle.normal),
//                      ),
//                    ),
//                    new Padding(
//                      padding: const EdgeInsets.all(2.0),
//                      child: new Text(
//                        "We are here for you...",
//                        style: new TextStyle(
//                            fontSize: 14.0,
//                            color: Theme.Colors.appsupportlabel,
//                            fontStyle: FontStyle.italic),
//                      ),
//                    ),
////                  new Padding(
////                    padding: const EdgeInsets.all(15.0),
////                    child: new Text(
////                      aboutUs,
////                      style: new TextStyle(
////                          fontSize: 15.0,
////                          color: Theme.Colors.appsupportcontent,
////                          fontStyle: FontStyle.normal,
////                          wordSpacing: 2.0),
////                    ),
////                  ),
//                    new Padding(
//                      padding: const EdgeInsets.all(15.0),
//                      child: new TextFormField(
//                        decoration: const InputDecoration(
//                          border: OutlineInputBorder(),
//                        hintText: 'Tell us about..',
////                          helperText: 'Keep it short :).',
//                          labelText: 'Your feedback',
//                        ),
//                        maxLines: 3,
//                        onFieldSubmitted: (String textInput) {
//                          FocusScope.of(context).requestFocus(_focusNode);
//                        },
//                      ),
//                    ),
//                    _isLoading
//                        ? new CircularProgressIndicator()
//                        : new InkWell(
//                      onTap: () {
////                        _submit();
//                        setState(() {
//                          _isLoading = true;
//                        });
////                      loginWithEmail(
////                          _emailController.text, _passController.text);
//                      },
//                      child: new Container(
//                        height: 60.0,
//                        margin: new EdgeInsets.only(top: 5.0),
//                        child: new Padding(
//                          padding: const EdgeInsets.all(5.0),
//                          child: new Container(
//                            width: screenSize.width,
//                            margin: new EdgeInsets.only(
//                                left: 10.0, right: 10.0, bottom: 2.0),
//                            height: 60.0,
//                            decoration: new BoxDecoration(
//                                color: Theme.Colors.loginbutton,
//                                borderRadius: new BorderRadius.all(
//                                    new Radius.circular(20.0))),
//                            child: new Center(
//                                child: new Text(
//                                  "Submit",
//                                  style: new TextStyle(
//                                      color: Colors.white, fontSize: 20.0),
//                                )),
//                          ),
//                        ),
//                      ),
//                    ),
////                  new RaisedButton(
////                    textColor: Colors.white,
////                    color: Theme.Colors.appsupportbutton,
////                    child: new Text("SUBMIT"),
////                    onPressed: () {
//////                      handleOnSubmit();
////                    },
////                  ),
//                  ],
//                )),
//          ),
//        ));
  }
}