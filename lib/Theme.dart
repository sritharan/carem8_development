import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
final ThemeData themeData = new ThemeData(
    brightness: Brightness.light,
    cardColor: Colors.appcolour,
    dividerColor: Colors.appcolour,
    backgroundColor: Colors.appcolour,
    primaryColor: _MyColors.theme[500],
    primaryColorBrightness: Brightness.light,
    secondaryHeaderColor: Colors.appcolour,
    accentColor: _MyColors.accent[500],
    primaryTextTheme: new Typography(platform: defaultTargetPlatform).white,
    primaryIconTheme: const IconThemeData(color: Colors.appcolour),
    accentIconTheme: const IconThemeData(color: Colors.appcolour)
);

class _MyColors {
  _MyColors._(); // this basically makes it so you can instantiate this class
  static const Map<int, Color> theme = const <int, Color>{
    500: const Color(0xFF7E57C2),
    600: const Color(0xFF7C20A0),
  };

  static const Map<int, Color> accent = const <int, Color>{
    500: const Color(0xFFB39DDB),
  };
}

// Formats
 /* static const Color mainBlue = const Color.fromRGBO(77, 123, 243, 1.0); */
/* static const Color appcolour = const Color(0xFFB39DDB); */
class Colors {

  const Colors();
  static const _baseBlue = 0xFF9168ed;
  static const _basePink = 0xFFf54295;

  static const MaterialColor app_blue = const MaterialColor(
    _baseBlue,
    const <int, Color>{
      50: const Color(0xFFeee5fc),
      100: const Color(0xFFd1bff6),
      200: const Color(0xFFb295f1),
      300: const Color(_baseBlue),
      400: const Color(0xFF7644e9),
      500: const Color(0xFF5519e4),
      600: const Color(0xFF4815de),
      700: const Color(0xFF3009d5),
      800: const Color(0xFF0000d0),
      900: const Color(0xFF0000ca),
    },
  );

  static const MaterialColor app_dark = const MaterialColor(
    0xFF444444,
    const <int, Color>{
      50: const Color(0xFF444444),
      100: const Color(0xFF888888),
      200: const Color(0xFF777777),
      300: const Color(0xFF666666),
      400: const Color(0xFF555555),
      500: const Color(0xFF444444),
      600: const Color(0xFF333333),
      700: const Color(0xFF222222),
      800: const Color(0xFF111111),
      900: const Color(0xFF000000),
    },
  );

  static const MaterialColor app_white = const MaterialColor(
    0xFFFFFFFF,
    const <int, Color>{
      50: const Color(0xFFFFFFFF),
      100: const Color(0xFFFFFFFF),
      200: const Color(0xFFFFFFFF),
      300: const Color(0xFFFFFFFF),
      400: const Color(0xFFFFFFFF),
      500: const Color(0xFFFFFFFF),
      600: const Color(0xFFFDFDFD),
      700: const Color(0xFFFAFAFA),
      800: const Color(0xFFF6F6F6),
      900: const Color(0xFFF0F0F0),
    },
  );

  static const Color appcolour = const Color(0xFF1AA4DE);
  static const Color appdarkcolour = const Color(0xFF6FBE46);

  static const Color appseconderycolour = const Color(0xFF1AA4DE);

  static const Color progressbackground = const Color(0xFF6FBE46);
  static const Color progresscontent = const Color(0xFF1AA4DE);

  static const Color appsupportheader = const Color(0xFF6FBE46);
  static const Color appsupportlabel = const Color(0xFF1AA4DE);
  static const Color appsupportcontent = const Color(0xFF6FBE46);
  static const Color appsupportbutton = const Color(0xFF6FBE46);
  static const Color appsupportbackground = const Color(0xFF6FBE46);

  static const Color appsplashscreen = const Color(0xFF404E54);
  static const Color appBarTitle = const Color(0xFFFFFFFF);
  static const Color appBarIconColor = const Color(0xFFFFFFFF);
  static const Color appBarDetailBackground = const Color(0xFF1AA4DE);
  static const Color appBarGradientStart = const Color(0xFF404E54);
  static const Color appBarGradientEnd = const Color(0xFF404E54);

  static const Color loginbackground = const Color(0xFF6FBE46);
  static const Color logintext = const Color(0xFF1AA4DE);
  static const Color loginlabel = const Color(0xFF1AA4DE);
  static const Color textboxborder = const Color(0xFF6FBE46);
  static const Color loginbutton = const Color(0xFF1AA4DE);
  static const Color versionlabel = const Color(0xFF1AA4DE);

  static const Color forgotbackground = const Color(0xFF1AA4DE);
  static const Color forgotlabel = const Color(0xFF1AA4DE);
  static const Color forgotsubmitbutton = const Color(0xFF1AA4DE);
  static const Color forgotbuttontext = const Color(0xFFFFFFFF);

  static const Color appmenuicon = const Color(0xFF6FBE46);

  static const Color buttonicon_color = const Color(0xFF1AA4DE);
  static const Color buttonicon_lightcolor = const Color(0xFF6FBE46);
  static const Color buttonicon_darkcolor = const Color(0xFF1A89B8);

  static const Color buttonicon_addcolor = const Color(0xFF6FBE46);
  static const Color buttonicon_deletecolor = const Color(0xFFff6f69);
  static const Color buttonicon_updatecolor = const Color(0xFF6FBE46);
  static const Color buttonicon_morecolor = const Color(0xFF6FBE46);

  static const Color eventappbar = const Color(0xFF6FBE46);
  static const Color eventtabbubble = const Color(0xFF1A89B8);
  static const Color eventtabtitle = const Color(0xFFFFFFFF);

  static const Color eventrsvp_yes = const Color(0xFFF1DB967);
  static const Color eventrsvp_maybe = const Color(0xFFFFC433);
  static const Color eventrsvp_no= const Color(0xFFff6f69);

  static const Color smalltext = const Color(0xFF1AA4DE);
  static const Color mediumtext = const Color(0xFF1AA4DE);
  static const Color bigtext = const Color(0xFF1AA4DE);

  static const Color maintitle = const Color(0xFF1AA4DE);
  static const Color secondmaintitle = const Color(0xFF1AA4DE);
  static const Color thirdmaintitle = const Color(0xFF1AA4DE);

  static const Color subtitle = const Color(0xFF1AA4DE);
  static const Color secondsubtitle = const Color(0xFF1AA4DE);
  static const Color thirdsubtitle = const Color(0xFF1AA4DE);

  static const Color listtitle = const Color(0xFF1AA4DE);
  static const Color listsubtitle = const Color(0xFF1AA4DE);
  static const Color listsubcontent = const Color(0xFFB39DDB);
  static const Color listcontent = const Color(0xFFB39DDB);

  static const Color darkGrey = const Color(0xFF666666);
  static const Color bodyText = const Color(0xFF9d9d9d);
  static const Color boldText = const Color(0xFF222222);
  static const Color lightGrey = const Color(0xFFd1d1d1);
  static const Color background = const Color(0xFFE8E8E8);

  static const Color form_requiredicon = const Color(0xFFff6f69);

  static const Color medication_acknowledged = const Color(0xFFFFC433);
  static const Color medication_pending = const Color(0xFF7E57C2);
  static const Color medication_completed = const Color(0xFF77DD77);


}

class TextStyles {

  const TextStyles();

  static const TextStyle appBarTitle = const TextStyle(
    color: Colors.appBarTitle,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    fontSize: 36.0
  );
}

// Useage
// GoogleMaterialColors materialColors = new GoogleMaterialColors();
// Color usedColor;
// Random random = new Random();
// usedColor = materialColors.getLightColor(random.nextInt(5));

//decoration: BoxDecoration(
//color: (personenError
//? materialColors.getLightColor(3).withOpacity(0.3) //Red
//: null
//),
//borderRadius: BorderRadius.all(Radius.circular(20.0)),
//shape: BoxShape.rectangle
//),
class GoogleMaterialColors{

  static List<Color> colorList = [
    Color(0xFF34a853), //Green
    Color(0xFFfbbc04), //Yellow
    Color(0xFF4285f4), //Blue
    Color(0xFFea4335), //Red
    Color(0xFFa142f4) //Purple
  ];

  Color getLightColor(int number){
    return colorList[number];
  }

  List<Color> getColorList(int number){
    List<Color> returnColor = [colorList[number], colorList[number].withOpacity(0.2)];
    return returnColor;
  }
}
