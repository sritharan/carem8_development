import 'package:flutter/material.dart';
import 'package:carem8/routes.dart';
import 'package:carem8/utils/connectivity.dart';


void main() {
  // To check Flutter Error
//  FlutterError.onError = (FlutterErrorDetails details) {
//    if (!details.silent) {
//      // it'd be nice to be able to rethrow here and preserve the stack trace...
//      throw details.exception;
//    }
//  };
//  ConnectionStatusSingleton connectionStatus = ConnectionStatusSingleton.getInstance();
//  connectionStatus.initialize();

  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'CareM8',
//      theme: GeoTheme.build(),
      theme: new ThemeData(primarySwatch: Colors.green, primaryColor: Colors.blue),
      // All routes are configured in lib/routes
      routes: routes,
    );
  }
}

class GeoTheme {
  static ThemeData build() {
    return ThemeData(
      fontFamily: 'Montserrat',
      primaryColor: Colors.white,
      primaryColorBrightness: Brightness.light,
      primaryIconTheme: const IconThemeData(
        color: Colors.blue,
      ),
    );
  }
}

