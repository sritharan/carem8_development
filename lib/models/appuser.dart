// To parse this JSON data, do
//
//     final appUser = appUserFromJson(jsonString);

import 'dart:convert';

List<AppUser> appUserFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<AppUser>.from(jsonData.map((x) => AppUser.fromJson(x)));
}

String appUserToJson(List<AppUser> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class AppUser {
  Users user;
  String jwt;
  Globalconfig globalconfig;
  List<Child> children;

  AppUser({
    this.user,
    this.jwt,
    this.globalconfig,
    this.children,
  });

  factory AppUser.fromJson(Map<String, dynamic> json) => new AppUser(
    user: Users.fromJson(json["user"]),
    jwt: json["jwt"],
    globalconfig: Globalconfig.fromJson(json["globalconfig"]),
    children: new List<Child>.from(json["children"].map((x) => Child.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "user": user.toJson(),
    "jwt": jwt,
    "globalconfig": globalconfig.toJson(),
    "children": new List<dynamic>.from(children.map((x) => x.toJson())),
  };
}

class Child {
  int id;
  int roomId;
  String firstname;
  String middlename;
  String lastname;
  String age;
  String dob;
  String gender;
  String attendance;
  String image;
  String status;
  String createdAt;
  int count;
  bool ispick;
  int attendanceId;

  Child({
    this.id,
    this.roomId,
    this.firstname,
    this.middlename,
    this.lastname,
    this.age,
    this.dob,
    this.gender,
    this.attendance,
    this.image,
    this.status,
    this.createdAt,
    this.count,
    this.ispick,
    this.attendanceId,
  });

  factory Child.fromJson(Map<String, dynamic> json) => new Child(
    id: json["id"],
    roomId: json["room_id"],
    firstname: json["firstname"],
    middlename: json["middlename"],
    lastname: json["lastname"],
    age: json["age"],
    dob: json["dob"],
    gender: json["gender"],
    attendance: json["attendance"],
    image: json["image"],
    status: json["status"],
    createdAt: json["created_at"],
    count: json["count"],
    ispick: json["ispick"],
    attendanceId: json["attendance_id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "room_id": roomId,
    "firstname": firstname,
    "middlename": middlename,
    "lastname": lastname,
    "age": age,
    "dob": dob,
    "gender": gender,
    "attendance": attendance,
    "image": image,
    "status": status,
    "created_at": createdAt,
    "count": count,
    "ispick": ispick,
    "attendance_id": attendanceId,
  };
}

class Globalconfig {
  Common common;
  NavigationSet navigationSet;
  RightSideBar rightSideBar;
  UserProfile userProfile;
  CentreProfile centreProfile;
  PrivacyPolicy privacyPolicy;
  Notification notification;
  DashboardSection dashboardSection;
  DashboardCommonNotificationArea dashboardCommonNotificationArea;
  ActivityLogsSection activityLogsSection;
  EmailLogsSection emailLogsSection;
  EventsSection eventsSection;
  GeneralFormSection generalFormSection;
  PoliciesSection policiesSection;
  SubjectAreaSection subjectAreaSection;
  NewsletterSection newsletterSection;
  KioskSection kioskSection;
  KioskActivitySection kioskActivitySection;
  KioskStaffCheckinCheckoutSection kioskStaffCheckinCheckoutSection;
  UserManagementSection userManagementSection;
  ChildProfile childProfile;
  ChildIncidentSection childIncidentSection;
  ChildManagementSection childManagementSection;
  CenterManagementRoomSection centerManagementRoomSection;
  CenterManagementJournalSection centerManagementJournalSection;
  CenterManagementStorySection centerManagementStorySection;
  CenterManagementJourneySection centerManagementJourneySection;
  CenterManagementFollowupSection centerManagementFollowupSection;
  CenterManagementDailyChartSection centerManagementDailyChartSection;
  NewsfeedSection newsfeedSection;
  LearningsetSection learningsetSection;
  LearningtagSection learningtagSection;
  ReportingSection reportingSection;
  PrivateMessageSection privateMessageSection;
  ClientSection clientSection;
  DateFormats dateFormats;
  PrinciplesSection principlesSection;
  PracticesSection practicesSection;
  EmailSection emailSection;
  StaffDocumentsUploadSection staffDocumentsUploadSection;
  MedicationSection medicationSection;
  ReminderSettingsSection reminderSettingsSection;
  UserActivitySection userActivitySection;
  NonPrescribedMedicationSection nonPrescribedMedicationSection;
  ProgramPlanningSection programPlanningSection;
  ImageRearrangeSection imageRearrangeSection;
  TextTagSection textTagSection;
  ManageCentreNotificationSection manageCentreNotificationSection;
  ErrorMessages errorMessages;
  ApplicationSettings applicationSettings;
  JournalFollowup journalFollowup;
  LearningstoryFollowup learningstoryFollowup;

  Globalconfig({
    this.common,
    this.navigationSet,
    this.rightSideBar,
    this.userProfile,
    this.centreProfile,
    this.privacyPolicy,
    this.notification,
    this.dashboardSection,
    this.dashboardCommonNotificationArea,
    this.activityLogsSection,
    this.emailLogsSection,
    this.eventsSection,
    this.generalFormSection,
    this.policiesSection,
    this.subjectAreaSection,
    this.newsletterSection,
    this.kioskSection,
    this.kioskActivitySection,
    this.kioskStaffCheckinCheckoutSection,
    this.userManagementSection,
    this.childProfile,
    this.childIncidentSection,
    this.childManagementSection,
    this.centerManagementRoomSection,
    this.centerManagementJournalSection,
    this.centerManagementStorySection,
    this.centerManagementJourneySection,
    this.centerManagementFollowupSection,
    this.centerManagementDailyChartSection,
    this.newsfeedSection,
    this.learningsetSection,
    this.learningtagSection,
    this.reportingSection,
    this.privateMessageSection,
    this.clientSection,
    this.dateFormats,
    this.principlesSection,
    this.practicesSection,
    this.emailSection,
    this.staffDocumentsUploadSection,
    this.medicationSection,
    this.reminderSettingsSection,
    this.userActivitySection,
    this.nonPrescribedMedicationSection,
    this.programPlanningSection,
    this.imageRearrangeSection,
    this.textTagSection,
    this.manageCentreNotificationSection,
    this.errorMessages,
    this.applicationSettings,
    this.journalFollowup,
    this.learningstoryFollowup,
  });

  factory Globalconfig.fromJson(Map<String, dynamic> json) => new Globalconfig(
    common: Common.fromJson(json["common"]),
    navigationSet: NavigationSet.fromJson(json["navigation_set"]),
    rightSideBar: RightSideBar.fromJson(json["right_side_bar"]),
    userProfile: UserProfile.fromJson(json["user_profile"]),
    centreProfile: CentreProfile.fromJson(json["centre_profile"]),
    privacyPolicy: PrivacyPolicy.fromJson(json["privacy_policy"]),
    notification: Notification.fromJson(json["notification"]),
    dashboardSection: DashboardSection.fromJson(json["dashboard_section"]),
    dashboardCommonNotificationArea: DashboardCommonNotificationArea.fromJson(json["dashboard_common_notification_area"]),
    activityLogsSection: ActivityLogsSection.fromJson(json["activity_logs_section"]),
    emailLogsSection: EmailLogsSection.fromJson(json["email_logs_section"]),
    eventsSection: EventsSection.fromJson(json["events_section"]),
    generalFormSection: GeneralFormSection.fromJson(json["general_form_section"]),
    policiesSection: PoliciesSection.fromJson(json["policies_section"]),
    subjectAreaSection: SubjectAreaSection.fromJson(json["subject_area_section"]),
    newsletterSection: NewsletterSection.fromJson(json["newsletter_section"]),
    kioskSection: KioskSection.fromJson(json["kiosk_section"]),
    kioskActivitySection: KioskActivitySection.fromJson(json["kiosk_activity_section"]),
    kioskStaffCheckinCheckoutSection: KioskStaffCheckinCheckoutSection.fromJson(json["kiosk_staff_checkin_checkout_section"]),
    userManagementSection: UserManagementSection.fromJson(json["user_management_section"]),
    childProfile: ChildProfile.fromJson(json["child_profile"]),
    childIncidentSection: ChildIncidentSection.fromJson(json["child_incident_section"]),
    childManagementSection: ChildManagementSection.fromJson(json["child_management_section"]),
    centerManagementRoomSection: CenterManagementRoomSection.fromJson(json["center_management_room_section"]),
    centerManagementJournalSection: CenterManagementJournalSection.fromJson(json["center_management_journal_section"]),
    centerManagementStorySection: CenterManagementStorySection.fromJson(json["center_management_story_section"]),
    centerManagementJourneySection: CenterManagementJourneySection.fromJson(json["center_management_journey_section"]),
    centerManagementFollowupSection: CenterManagementFollowupSection.fromJson(json["center_management_followup_section"]),
    centerManagementDailyChartSection: CenterManagementDailyChartSection.fromJson(json["center_management_daily_chart_section"]),
    newsfeedSection: NewsfeedSection.fromJson(json["newsfeed_section"]),
    learningsetSection: LearningsetSection.fromJson(json["learningset_section"]),
    learningtagSection: LearningtagSection.fromJson(json["learningtag_section"]),
    reportingSection: ReportingSection.fromJson(json["reporting_section"]),
    privateMessageSection: PrivateMessageSection.fromJson(json["private_message_section"]),
    clientSection: ClientSection.fromJson(json["client_section"]),
    dateFormats: DateFormats.fromJson(json["date_formats"]),
    principlesSection: PrinciplesSection.fromJson(json["principles_section"]),
    practicesSection: PracticesSection.fromJson(json["practices_section"]),
    emailSection: EmailSection.fromJson(json["email_section"]),
    staffDocumentsUploadSection: StaffDocumentsUploadSection.fromJson(json["staff_documents_upload_section"]),
    medicationSection: MedicationSection.fromJson(json["medication_section"]),
    reminderSettingsSection: ReminderSettingsSection.fromJson(json["reminder_settings_section"]),
    userActivitySection: UserActivitySection.fromJson(json["user_activity_section"]),
    nonPrescribedMedicationSection: NonPrescribedMedicationSection.fromJson(json["non_prescribed_medication_section"]),
    programPlanningSection: ProgramPlanningSection.fromJson(json["program_planning_section"]),
    imageRearrangeSection: ImageRearrangeSection.fromJson(json["image_rearrange_section"]),
    textTagSection: TextTagSection.fromJson(json["text_tag_section"]),
    manageCentreNotificationSection: ManageCentreNotificationSection.fromJson(json["manage_centre_notification_section"]),
    errorMessages: ErrorMessages.fromJson(json["error_messages"]),
    applicationSettings: ApplicationSettings.fromJson(json["application_settings"]),
    journalFollowup: JournalFollowup.fromJson(json["journal_followup"]),
    learningstoryFollowup: LearningstoryFollowup.fromJson(json["learningstory_followup"]),
  );

  Map<String, dynamic> toJson() => {
    "common": common.toJson(),
    "navigation_set": navigationSet.toJson(),
    "right_side_bar": rightSideBar.toJson(),
    "user_profile": userProfile.toJson(),
    "centre_profile": centreProfile.toJson(),
    "privacy_policy": privacyPolicy.toJson(),
    "notification": notification.toJson(),
    "dashboard_section": dashboardSection.toJson(),
    "dashboard_common_notification_area": dashboardCommonNotificationArea.toJson(),
    "activity_logs_section": activityLogsSection.toJson(),
    "email_logs_section": emailLogsSection.toJson(),
    "events_section": eventsSection.toJson(),
    "general_form_section": generalFormSection.toJson(),
    "policies_section": policiesSection.toJson(),
    "subject_area_section": subjectAreaSection.toJson(),
    "newsletter_section": newsletterSection.toJson(),
    "kiosk_section": kioskSection.toJson(),
    "kiosk_activity_section": kioskActivitySection.toJson(),
    "kiosk_staff_checkin_checkout_section": kioskStaffCheckinCheckoutSection.toJson(),
    "user_management_section": userManagementSection.toJson(),
    "child_profile": childProfile.toJson(),
    "child_incident_section": childIncidentSection.toJson(),
    "child_management_section": childManagementSection.toJson(),
    "center_management_room_section": centerManagementRoomSection.toJson(),
    "center_management_journal_section": centerManagementJournalSection.toJson(),
    "center_management_story_section": centerManagementStorySection.toJson(),
    "center_management_journey_section": centerManagementJourneySection.toJson(),
    "center_management_followup_section": centerManagementFollowupSection.toJson(),
    "center_management_daily_chart_section": centerManagementDailyChartSection.toJson(),
    "newsfeed_section": newsfeedSection.toJson(),
    "learningset_section": learningsetSection.toJson(),
    "learningtag_section": learningtagSection.toJson(),
    "reporting_section": reportingSection.toJson(),
    "private_message_section": privateMessageSection.toJson(),
    "client_section": clientSection.toJson(),
    "date_formats": dateFormats.toJson(),
    "principles_section": principlesSection.toJson(),
    "practices_section": practicesSection.toJson(),
    "email_section": emailSection.toJson(),
    "staff_documents_upload_section": staffDocumentsUploadSection.toJson(),
    "medication_section": medicationSection.toJson(),
    "reminder_settings_section": reminderSettingsSection.toJson(),
    "user_activity_section": userActivitySection.toJson(),
    "non_prescribed_medication_section": nonPrescribedMedicationSection.toJson(),
    "program_planning_section": programPlanningSection.toJson(),
    "image_rearrange_section": imageRearrangeSection.toJson(),
    "text_tag_section": textTagSection.toJson(),
    "manage_centre_notification_section": manageCentreNotificationSection.toJson(),
    "error_messages": errorMessages.toJson(),
    "application_settings": applicationSettings.toJson(),
    "journal_followup": journalFollowup.toJson(),
    "learningstory_followup": learningstoryFollowup.toJson(),
  };
}

class ActivityLogsSection {
  String navAlogLabel;
  String navAlogIndexTitle;
  String navAlogFilterByParent;
  String navAlogFilterByStaff;
  String navAlogMessageEmpty;

  ActivityLogsSection({
    this.navAlogLabel,
    this.navAlogIndexTitle,
    this.navAlogFilterByParent,
    this.navAlogFilterByStaff,
    this.navAlogMessageEmpty,
  });

  factory ActivityLogsSection.fromJson(Map<String, dynamic> json) => new ActivityLogsSection(
    navAlogLabel: json["nav_alog_label"],
    navAlogIndexTitle: json["nav_alog_index_title"],
    navAlogFilterByParent: json["nav_alog_filter_by_parent"],
    navAlogFilterByStaff: json["nav_alog_filter_by_staff"],
    navAlogMessageEmpty: json["nav_alog_message_empty"],
  );

  Map<String, dynamic> toJson() => {
    "nav_alog_label": navAlogLabel,
    "nav_alog_index_title": navAlogIndexTitle,
    "nav_alog_filter_by_parent": navAlogFilterByParent,
    "nav_alog_filter_by_staff": navAlogFilterByStaff,
    "nav_alog_message_empty": navAlogMessageEmpty,
  };
}

class ApplicationSettings {
  String enablePostfeedComments;
  String dailychartSleepCheckShowCheckBox;
  String journalReportImageLimit;
  String enableMasterNavPolicies;
  String disableEducatorCanAccessAllStaffReflections;
  String enableMasterNavProgramPlanning;
  String enableMasterNavStaffDocumentUpload;
  String passwordResetTokenErorMsg;
  String enableLearningStoryPhotoDownload;
  String enableClientDownloadJournal;
  String journeyReportFollowupImageLimit;
  String subjectAreaMaxTagCharLimit;
  String learningStoryDocMaxFileLimit;
  String enableMasterDailychartNappyTime;
  String enableMasterNavJournalFollowup;
  String enableMasterNavPrinciples;
  String enableMasterNavLearningStory;
  String enableMasterNavLearningTags;
  String enableMasterNavForms;
  String enableDashboardClock;
  String learningStoryImageTotalMaxFileLimit;
  String enableMasterNavKioskStaffManualAttendance;
  String parentHomepageTabOrder;
  String storgeUploadsLocal;
  String learningStoryVideoFileSizeLimitV1;
  String enableNewsfeedPhotoDownload;
  String storgeAwsBaseurl;
  String enableMasterNavKioskParent;
  String enableJournalPhotoDownload;
  String storgeClientName;
  String newsletterTotalMaxFileUploadSize;
  String allowedTotalMaxFileUploadSize;
  String enableMasterNavDevelopmentMilestone;
  String noReplyMail;
  String journalImageLimit;
  String uploadAudioFileTypes;
  String portfolioReportImageLimitUpload;
  String foodMenuWeeks;
  String newsFeedVideoFileSizeLimitV1;
  String passwordUpdateTokenExpireTimeV1;
  String enableAccessManagement;
  String enableMasterNavUlh;
  String observationAdditionlTextField1;
  String observationAdditionlTextField2;
  String journalFollowupImageLimit;
  String enableMasterNavJournalComment;
  String enableLearningStoryComments;
  String enableMasterNavNewsletterCreate;
  String journeyImageLimit;
  String learningStoryImageFileSizeLimit;
  String uploadVideoFileTypes;
  String enableObservationPhotoDownload;
  String transitiontoSchoolImageLimit;
  String enableMasterNavElog;
  String storyReportImageLimit;
  String journalImageLimitUpload;
  String uploadImageFileTypes;
  String enableAllRoomsStaffChecking;
  String journeyFollowupImageLimit;
  String enableMasterNavKioskStaff;
  String journalFollowupImageLimitUpload;
  String enableMasterNavSubjectAreas;
  String policiesTotalMaxFileUploadSize;
  String generalformsTotalMaxFileUploadSize;
  String enableMasterNavDailychart;
  String transitiontoSchoolImageSizeLimit;
  String portfolioReportMonthLimit;
  String enableSaturday;
  String storyFollowupImageLimitUpload;
  String enableMasterNavKioskChildManualAttendance;
  String serverCommonLocation;
  String documentTagMaxTagCharLimit;
  String enableAllRoomsChildChecking;
  String enableTransitionSchool;
  String enableStaffPincodeCreate;
  String childdocumentTotalMaxFileUploadSize;
  String journalReportFollowupImageLimit;
  String journeyReportImageLimit;
  String timezone;
  String enableMasterNavStoryFollowup;
  String enablePostfeedLikes;
  String privateMessageFileLimit;
  String enableMasterStaffAttendance;
  String postfeedImageLimitUpload;
  String dailychartSleepCheckAgeLimitMonths1;
  String numberOfParentsPerChildLimit;
  String staffdocumentTotalMaxFileUploadSize;
  String enableSunday;
  String privateMessageFileSizeLimit;
  String learningStoryAudioFileSizeLimit;
  String journeyImageLimitUpload;
  String newsFeedVideoFolderPath;
  String storgeAwsBucket;
  String enableMasterNavPractices;
  String followupImageLimitUpload;
  String dailychartSleepCheckTimeInterval;
  String portfolioReportImageSizeLimit;
  String enableMasterNavNewsletter;
  String journalAdditionlTextField2;
  String parentHomepageDailySummaryShowUpto;
  String journalAdditionlTextField1;
  String enableMasterNavAlog;

  ApplicationSettings({
    this.enablePostfeedComments,
    this.dailychartSleepCheckShowCheckBox,
    this.journalReportImageLimit,
    this.enableMasterNavPolicies,
    this.disableEducatorCanAccessAllStaffReflections,
    this.enableMasterNavProgramPlanning,
    this.enableMasterNavStaffDocumentUpload,
    this.passwordResetTokenErorMsg,
    this.enableLearningStoryPhotoDownload,
    this.enableClientDownloadJournal,
    this.journeyReportFollowupImageLimit,
    this.subjectAreaMaxTagCharLimit,
    this.learningStoryDocMaxFileLimit,
    this.enableMasterDailychartNappyTime,
    this.enableMasterNavJournalFollowup,
    this.enableMasterNavPrinciples,
    this.enableMasterNavLearningStory,
    this.enableMasterNavLearningTags,
    this.enableMasterNavForms,
    this.enableDashboardClock,
    this.learningStoryImageTotalMaxFileLimit,
    this.enableMasterNavKioskStaffManualAttendance,
    this.parentHomepageTabOrder,
    this.storgeUploadsLocal,
    this.learningStoryVideoFileSizeLimitV1,
    this.enableNewsfeedPhotoDownload,
    this.storgeAwsBaseurl,
    this.enableMasterNavKioskParent,
    this.enableJournalPhotoDownload,
    this.storgeClientName,
    this.newsletterTotalMaxFileUploadSize,
    this.allowedTotalMaxFileUploadSize,
    this.enableMasterNavDevelopmentMilestone,
    this.noReplyMail,
    this.journalImageLimit,
    this.uploadAudioFileTypes,
    this.portfolioReportImageLimitUpload,
    this.foodMenuWeeks,
    this.newsFeedVideoFileSizeLimitV1,
    this.passwordUpdateTokenExpireTimeV1,
    this.enableAccessManagement,
    this.enableMasterNavUlh,
    this.observationAdditionlTextField1,
    this.observationAdditionlTextField2,
    this.journalFollowupImageLimit,
    this.enableMasterNavJournalComment,
    this.enableLearningStoryComments,
    this.enableMasterNavNewsletterCreate,
    this.journeyImageLimit,
    this.learningStoryImageFileSizeLimit,
    this.uploadVideoFileTypes,
    this.enableObservationPhotoDownload,
    this.transitiontoSchoolImageLimit,
    this.enableMasterNavElog,
    this.storyReportImageLimit,
    this.journalImageLimitUpload,
    this.uploadImageFileTypes,
    this.enableAllRoomsStaffChecking,
    this.journeyFollowupImageLimit,
    this.enableMasterNavKioskStaff,
    this.journalFollowupImageLimitUpload,
    this.enableMasterNavSubjectAreas,
    this.policiesTotalMaxFileUploadSize,
    this.generalformsTotalMaxFileUploadSize,
    this.enableMasterNavDailychart,
    this.transitiontoSchoolImageSizeLimit,
    this.portfolioReportMonthLimit,
    this.enableSaturday,
    this.storyFollowupImageLimitUpload,
    this.enableMasterNavKioskChildManualAttendance,
    this.serverCommonLocation,
    this.documentTagMaxTagCharLimit,
    this.enableAllRoomsChildChecking,
    this.enableTransitionSchool,
    this.enableStaffPincodeCreate,
    this.childdocumentTotalMaxFileUploadSize,
    this.journalReportFollowupImageLimit,
    this.journeyReportImageLimit,
    this.timezone,
    this.enableMasterNavStoryFollowup,
    this.enablePostfeedLikes,
    this.privateMessageFileLimit,
    this.enableMasterStaffAttendance,
    this.postfeedImageLimitUpload,
    this.dailychartSleepCheckAgeLimitMonths1,
    this.numberOfParentsPerChildLimit,
    this.staffdocumentTotalMaxFileUploadSize,
    this.enableSunday,
    this.privateMessageFileSizeLimit,
    this.learningStoryAudioFileSizeLimit,
    this.journeyImageLimitUpload,
    this.newsFeedVideoFolderPath,
    this.storgeAwsBucket,
    this.enableMasterNavPractices,
    this.followupImageLimitUpload,
    this.dailychartSleepCheckTimeInterval,
    this.portfolioReportImageSizeLimit,
    this.enableMasterNavNewsletter,
    this.journalAdditionlTextField2,
    this.parentHomepageDailySummaryShowUpto,
    this.journalAdditionlTextField1,
    this.enableMasterNavAlog,
  });

  factory ApplicationSettings.fromJson(Map<String, dynamic> json) => new ApplicationSettings(
    enablePostfeedComments: json["enable_postfeed_comments"],
    dailychartSleepCheckShowCheckBox: json["dailychart_sleep_check_show_check_box"],
    journalReportImageLimit: json["journal_report_image_limit"],
    enableMasterNavPolicies: json["enable_master_nav_policies"],
    disableEducatorCanAccessAllStaffReflections: json["disable_educator_can_access_all_staff_reflections"],
    enableMasterNavProgramPlanning: json["enable_master_nav_program_planning"],
    enableMasterNavStaffDocumentUpload: json["enable_master_nav_staff_document_upload"],
    passwordResetTokenErorMsg: json["password_reset_token_eror_msg"],
    enableLearningStoryPhotoDownload: json["enable_learning_story_photo_download"],
    enableClientDownloadJournal: json["enable_client_download_journal"],
    journeyReportFollowupImageLimit: json["journey_report_followup_image_limit"],
    subjectAreaMaxTagCharLimit: json["subject_area_max_tag_char_limit"],
    learningStoryDocMaxFileLimit: json["learning_story_doc_max_file_limit"],
    enableMasterDailychartNappyTime: json["enable_master_dailychart_nappy_time"],
    enableMasterNavJournalFollowup: json["enable_master_nav_journal_followup"],
    enableMasterNavPrinciples: json["enable_master_nav_principles"],
    enableMasterNavLearningStory: json["enable_master_nav_learning_story"],
    enableMasterNavLearningTags: json["enable_master_nav_learning_tags"],
    enableMasterNavForms: json["enable_master_nav_forms"],
    enableDashboardClock: json["enable_dashboard_clock"],
    learningStoryImageTotalMaxFileLimit: json["learning_story_image_total_max_file_limit"],
    enableMasterNavKioskStaffManualAttendance: json["enable_master_nav_kiosk_staff_manual_attendance"],
    parentHomepageTabOrder: json["parent_homepage_tab_order"],
    storgeUploadsLocal: json["storge_uploads_local"],
    learningStoryVideoFileSizeLimitV1: json["learning_story_video_file_size_limit_v1"],
    enableNewsfeedPhotoDownload: json["enable_newsfeed_photo_download"],
    storgeAwsBaseurl: json["storge_aws_baseurl"],
    enableMasterNavKioskParent: json["enable_master_nav_kiosk_parent"],
    enableJournalPhotoDownload: json["enable_journal_photo_download"],
    storgeClientName: json["storge_client_name"],
    newsletterTotalMaxFileUploadSize: json["newsletter_total_max_file_upload_size"],
    allowedTotalMaxFileUploadSize: json["allowed_total_max_file_upload_size"],
    enableMasterNavDevelopmentMilestone: json["enable_master_nav_development_milestone"],
    noReplyMail: json["no_reply_mail"],
    journalImageLimit: json["journal_image_limit"],
    uploadAudioFileTypes: json["upload_audio_file_types"],
    portfolioReportImageLimitUpload: json["portfolio_report_image_limit_upload"],
    foodMenuWeeks: json["food_menu_weeks"],
    newsFeedVideoFileSizeLimitV1: json["news_feed_video_file_size_limit_v1"],
    passwordUpdateTokenExpireTimeV1: json["password_update_token_expire_time_v1"],
    enableAccessManagement: json["enable_access_management"],
    enableMasterNavUlh: json["enable_master_nav_ulh"],
    observationAdditionlTextField1: json["observation_additionl_text_field_1"],
    observationAdditionlTextField2: json["observation_additionl_text_field_2"],
    journalFollowupImageLimit: json["journal_followup_image_limit"],
    enableMasterNavJournalComment: json["enable_master_nav_journal_comment"],
    enableLearningStoryComments: json["enable_learning_story_comments"],
    enableMasterNavNewsletterCreate: json["enable_master_nav_newsletter_create"],
    journeyImageLimit: json["journey_image_limit"],
    learningStoryImageFileSizeLimit: json["learning_story_image_file_size_limit"],
    uploadVideoFileTypes: json["upload_video_file_types"],
    enableObservationPhotoDownload: json["enable_observation_photo_download"],
    transitiontoSchoolImageLimit: json["transitionto_school_image_limit"],
    enableMasterNavElog: json["enable_master_nav_elog"],
    storyReportImageLimit: json["story_report_image_limit"],
    journalImageLimitUpload: json["journal_image_limit_upload"],
    uploadImageFileTypes: json["upload_image_file_types"],
    enableAllRoomsStaffChecking: json["enable_all_rooms_staff_checking"],
    journeyFollowupImageLimit: json["journey_followup_image_limit"],
    enableMasterNavKioskStaff: json["enable_master_nav_kiosk_staff"],
    journalFollowupImageLimitUpload: json["journal_followup_image_limit_upload"],
    enableMasterNavSubjectAreas: json["enable_master_nav_subject_areas"],
    policiesTotalMaxFileUploadSize: json["policies_total_max_file_upload_size"],
    generalformsTotalMaxFileUploadSize: json["generalforms_total_max_file_upload_size"],
    enableMasterNavDailychart: json["enable_master_nav_dailychart"],
    transitiontoSchoolImageSizeLimit: json["transitionto_school_image_size_limit"],
    portfolioReportMonthLimit: json["portfolio-report-month-limit"],
    enableSaturday: json["enable_saturday"],
    storyFollowupImageLimitUpload: json["story_followup_image_limit_upload"],
    enableMasterNavKioskChildManualAttendance: json["enable_master_nav_kiosk_child_manual_attendance"],
    serverCommonLocation: json["server_common_location"],
    documentTagMaxTagCharLimit: json["document_tag_max_tag_char_limit"],
    enableAllRoomsChildChecking: json["enable_all_rooms_child_checking"],
    enableTransitionSchool: json["enable_transition_school"],
    enableStaffPincodeCreate: json["enable_staff_pincode_create"],
    childdocumentTotalMaxFileUploadSize: json["childdocument_total_max_file_upload_size"],
    journalReportFollowupImageLimit: json["journal_report_followup_image_limit"],
    journeyReportImageLimit: json["journey_report_image_limit"],
    timezone: json["timezone"],
    enableMasterNavStoryFollowup: json["enable_master_nav_story_followup"],
    enablePostfeedLikes: json["enable_postfeed_likes"],
    privateMessageFileLimit: json["private_message_file_limit"],
    enableMasterStaffAttendance: json["enable_master_staff_attendance"],
    postfeedImageLimitUpload: json["postfeed_image_limit_upload"],
    dailychartSleepCheckAgeLimitMonths1: json["dailychart_sleep_check_age_limit_months_1"],
    numberOfParentsPerChildLimit: json["number_of_parents_per_child_limit"],
    staffdocumentTotalMaxFileUploadSize: json["staffdocument_total_max_file_upload_size"],
    enableSunday: json["enable_sunday"],
    privateMessageFileSizeLimit: json["private_message_file_size_limit"],
    learningStoryAudioFileSizeLimit: json["learning_story_audio_file_size_limit"],
    journeyImageLimitUpload: json["journey_image_limit_upload"],
    newsFeedVideoFolderPath: json["news_feed_video_folder_path"],
    storgeAwsBucket: json["storge_aws_bucket"],
    enableMasterNavPractices: json["enable_master_nav_practices"],
    followupImageLimitUpload: json["followup_image_limit_upload"],
    dailychartSleepCheckTimeInterval: json["dailychart_sleep_check_time_interval"],
    portfolioReportImageSizeLimit: json["portfolio_report_image_size_limit"],
    enableMasterNavNewsletter: json["enable_master_nav_newsletter"],
    journalAdditionlTextField2: json["journal_additionl_text_field_2"],
    parentHomepageDailySummaryShowUpto: json["parent_homepage_daily_summary_show_upto"],
    journalAdditionlTextField1: json["journal_additionl_text_field_1"],
    enableMasterNavAlog: json["enable_master_nav_alog"],
  );

  Map<String, dynamic> toJson() => {
    "enable_postfeed_comments": enablePostfeedComments,
    "dailychart_sleep_check_show_check_box": dailychartSleepCheckShowCheckBox,
    "journal_report_image_limit": journalReportImageLimit,
    "enable_master_nav_policies": enableMasterNavPolicies,
    "disable_educator_can_access_all_staff_reflections": disableEducatorCanAccessAllStaffReflections,
    "enable_master_nav_program_planning": enableMasterNavProgramPlanning,
    "enable_master_nav_staff_document_upload": enableMasterNavStaffDocumentUpload,
    "password_reset_token_eror_msg": passwordResetTokenErorMsg,
    "enable_learning_story_photo_download": enableLearningStoryPhotoDownload,
    "enable_client_download_journal": enableClientDownloadJournal,
    "journey_report_followup_image_limit": journeyReportFollowupImageLimit,
    "subject_area_max_tag_char_limit": subjectAreaMaxTagCharLimit,
    "learning_story_doc_max_file_limit": learningStoryDocMaxFileLimit,
    "enable_master_dailychart_nappy_time": enableMasterDailychartNappyTime,
    "enable_master_nav_journal_followup": enableMasterNavJournalFollowup,
    "enable_master_nav_principles": enableMasterNavPrinciples,
    "enable_master_nav_learning_story": enableMasterNavLearningStory,
    "enable_master_nav_learning_tags": enableMasterNavLearningTags,
    "enable_master_nav_forms": enableMasterNavForms,
    "enable_dashboard_clock": enableDashboardClock,
    "learning_story_image_total_max_file_limit": learningStoryImageTotalMaxFileLimit,
    "enable_master_nav_kiosk_staff_manual_attendance": enableMasterNavKioskStaffManualAttendance,
    "parent_homepage_tab_order": parentHomepageTabOrder,
    "storge_uploads_local": storgeUploadsLocal,
    "learning_story_video_file_size_limit_v1": learningStoryVideoFileSizeLimitV1,
    "enable_newsfeed_photo_download": enableNewsfeedPhotoDownload,
    "storge_aws_baseurl": storgeAwsBaseurl,
    "enable_master_nav_kiosk_parent": enableMasterNavKioskParent,
    "enable_journal_photo_download": enableJournalPhotoDownload,
    "storge_client_name": storgeClientName,
    "newsletter_total_max_file_upload_size": newsletterTotalMaxFileUploadSize,
    "allowed_total_max_file_upload_size": allowedTotalMaxFileUploadSize,
    "enable_master_nav_development_milestone": enableMasterNavDevelopmentMilestone,
    "no_reply_mail": noReplyMail,
    "journal_image_limit": journalImageLimit,
    "upload_audio_file_types": uploadAudioFileTypes,
    "portfolio_report_image_limit_upload": portfolioReportImageLimitUpload,
    "food_menu_weeks": foodMenuWeeks,
    "news_feed_video_file_size_limit_v1": newsFeedVideoFileSizeLimitV1,
    "password_update_token_expire_time_v1": passwordUpdateTokenExpireTimeV1,
    "enable_access_management": enableAccessManagement,
    "enable_master_nav_ulh": enableMasterNavUlh,
    "observation_additionl_text_field_1": observationAdditionlTextField1,
    "observation_additionl_text_field_2": observationAdditionlTextField2,
    "journal_followup_image_limit": journalFollowupImageLimit,
    "enable_master_nav_journal_comment": enableMasterNavJournalComment,
    "enable_learning_story_comments": enableLearningStoryComments,
    "enable_master_nav_newsletter_create": enableMasterNavNewsletterCreate,
    "journey_image_limit": journeyImageLimit,
    "learning_story_image_file_size_limit": learningStoryImageFileSizeLimit,
    "upload_video_file_types": uploadVideoFileTypes,
    "enable_observation_photo_download": enableObservationPhotoDownload,
    "transitionto_school_image_limit": transitiontoSchoolImageLimit,
    "enable_master_nav_elog": enableMasterNavElog,
    "story_report_image_limit": storyReportImageLimit,
    "journal_image_limit_upload": journalImageLimitUpload,
    "upload_image_file_types": uploadImageFileTypes,
    "enable_all_rooms_staff_checking": enableAllRoomsStaffChecking,
    "journey_followup_image_limit": journeyFollowupImageLimit,
    "enable_master_nav_kiosk_staff": enableMasterNavKioskStaff,
    "journal_followup_image_limit_upload": journalFollowupImageLimitUpload,
    "enable_master_nav_subject_areas": enableMasterNavSubjectAreas,
    "policies_total_max_file_upload_size": policiesTotalMaxFileUploadSize,
    "generalforms_total_max_file_upload_size": generalformsTotalMaxFileUploadSize,
    "enable_master_nav_dailychart": enableMasterNavDailychart,
    "transitionto_school_image_size_limit": transitiontoSchoolImageSizeLimit,
    "portfolio-report-month-limit": portfolioReportMonthLimit,
    "enable_saturday": enableSaturday,
    "story_followup_image_limit_upload": storyFollowupImageLimitUpload,
    "enable_master_nav_kiosk_child_manual_attendance": enableMasterNavKioskChildManualAttendance,
    "server_common_location": serverCommonLocation,
    "document_tag_max_tag_char_limit": documentTagMaxTagCharLimit,
    "enable_all_rooms_child_checking": enableAllRoomsChildChecking,
    "enable_transition_school": enableTransitionSchool,
    "enable_staff_pincode_create": enableStaffPincodeCreate,
    "childdocument_total_max_file_upload_size": childdocumentTotalMaxFileUploadSize,
    "journal_report_followup_image_limit": journalReportFollowupImageLimit,
    "journey_report_image_limit": journeyReportImageLimit,
    "timezone": timezone,
    "enable_master_nav_story_followup": enableMasterNavStoryFollowup,
    "enable_postfeed_likes": enablePostfeedLikes,
    "private_message_file_limit": privateMessageFileLimit,
    "enable_master_staff_attendance": enableMasterStaffAttendance,
    "postfeed_image_limit_upload": postfeedImageLimitUpload,
    "dailychart_sleep_check_age_limit_months_1": dailychartSleepCheckAgeLimitMonths1,
    "number_of_parents_per_child_limit": numberOfParentsPerChildLimit,
    "staffdocument_total_max_file_upload_size": staffdocumentTotalMaxFileUploadSize,
    "enable_sunday": enableSunday,
    "private_message_file_size_limit": privateMessageFileSizeLimit,
    "learning_story_audio_file_size_limit": learningStoryAudioFileSizeLimit,
    "journey_image_limit_upload": journeyImageLimitUpload,
    "news_feed_video_folder_path": newsFeedVideoFolderPath,
    "storge_aws_bucket": storgeAwsBucket,
    "enable_master_nav_practices": enableMasterNavPractices,
    "followup_image_limit_upload": followupImageLimitUpload,
    "dailychart_sleep_check_time_interval": dailychartSleepCheckTimeInterval,
    "portfolio_report_image_size_limit": portfolioReportImageSizeLimit,
    "enable_master_nav_newsletter": enableMasterNavNewsletter,
    "journal_additionl_text_field_2": journalAdditionlTextField2,
    "parent_homepage_daily_summary_show_upto": parentHomepageDailySummaryShowUpto,
    "journal_additionl_text_field_1": journalAdditionlTextField1,
    "enable_master_nav_alog": enableMasterNavAlog,
  };
}

class CenterManagementDailyChartSection {
  String navCentermanagementDchartOptionLabelMenucategory;
  String navCentermanagementDchartFormToilettrainingTitle;
  String navCentermanagementDchartViewIndexTitleEmptyMessage;
  String navCentermanagementAttendanceCheckLogLabel;
  String navCentermanagementDchartToilettrinningLabel;
  String navCentermanagementDchartIndexRoomTitle;
  String navCentermanagementDchartViewIndexTitleBottleFeed;
  String navCentermanagementDchartOptionLabelServetypeApplyBtn;
  String navCentermanagementDchartFormResttimeLabel;
  String navCentermanagementDchartFoodAlertNochildWeekdayMessage;
  String navCentermanagementDchartFoodRecipeIndexTitle;
  String navCentermanagementAttendanceCheckTabLabelOut;
  String navCentermanagementDchartFoodAlertNochildMessage;
  String navCentermanagementDchartViewIndexTitleToilettraining;
  String navCentermanagementDchartFoodRecipeLabelMenuset;
  String navCentermanagementDchartCurrentviewIndexTitle;
  String navCentermanagementDchartCurrentdetailedviewLabel;
  String navCentermanagementDchartBottlefeed;
  String navCentermanagementDchartOptionLabelResttimetype;
  String navCentermanagementDchartFoodLabel;
  String navCentermanagementDchartRoomCountAlertMessage;
  String navCentermanagementDchartViewIndexTitleFood;
  String navCentermanagementDchartLabel;
  String navCentermanagementDchartButtonAddnew;
  String navCentermanagementDchartViewIndexTitleResttime;
  String navCentermanagementDchartChildCountAlertMessage;
  String navCentermanagementAttendanceCheckIndexTitle;
  String navCentermanagementDchartCurrentviewReportLabelNappy;
  String navCentermanagementDchartOptionIndexTitle;
  String navCentermanagementDchartOptionLabelResttimetypeApplyBtn;
  String navCentermanagementDchartCurrentdetailedviewIndexTitle;
  String navCentermanagementDchartCurrentviewReportLabelFood;
  String navCentermanagementAttendanceCheckLabelNew;
  String navCentermanagementAttendanceCheckTabLabelIn;
  String navCentermanagementDchartOptionLabel;
  String navCentermanagementDchartSettingsType;
  String navCentermanagementDchartOptionLabelToilettrainingtype;
  String navCentermanagementDchartOptionLabelToilettrainingcategory;
  String navCentermanagementDchartSleepCheckLabel;
  String navEntermanagementDchartMessageEmpty;
  String navCentermanagementDchartCurrentviewLabel;
  String navCentermanagementDchartIndexTitle;
  String navCentermanagementDchartSettingsLabel;
  String navCentermanagementDchartFoodAlertNomenuSelectedMessage;
  String navCentermanagementDchartMenuSettingLabel;
  String navCentermanagementDchartCurrentviewReportLabelResttime;
  String navCentermanagementDchartOptionLabelServetype;
  String navCentermanagementDchartSleepCheck;
  String navCentermanagementDchartFoodRecipeLabelRecipe;
  String navCentermanagementDchartViewIndexTitle;
  String navCentermanagementDchartFormResttimeTitle;
  String navCentermanagementDchartFormFoodTitle;
  String navCentermanagementDchartBottlefeedType;
  String navCentermanagementDchartFoodNoitemMessage;
  String navCentermanagementDchartFoodRecipeLabel;
  String navCentermanagementDchartResttimeLabel;
  String navCentermanagementDchartBottlefeedLabel;

  CenterManagementDailyChartSection({
    this.navCentermanagementDchartOptionLabelMenucategory,
    this.navCentermanagementDchartFormToilettrainingTitle,
    this.navCentermanagementDchartViewIndexTitleEmptyMessage,
    this.navCentermanagementAttendanceCheckLogLabel,
    this.navCentermanagementDchartToilettrinningLabel,
    this.navCentermanagementDchartIndexRoomTitle,
    this.navCentermanagementDchartViewIndexTitleBottleFeed,
    this.navCentermanagementDchartOptionLabelServetypeApplyBtn,
    this.navCentermanagementDchartFormResttimeLabel,
    this.navCentermanagementDchartFoodAlertNochildWeekdayMessage,
    this.navCentermanagementDchartFoodRecipeIndexTitle,
    this.navCentermanagementAttendanceCheckTabLabelOut,
    this.navCentermanagementDchartFoodAlertNochildMessage,
    this.navCentermanagementDchartViewIndexTitleToilettraining,
    this.navCentermanagementDchartFoodRecipeLabelMenuset,
    this.navCentermanagementDchartCurrentviewIndexTitle,
    this.navCentermanagementDchartCurrentdetailedviewLabel,
    this.navCentermanagementDchartBottlefeed,
    this.navCentermanagementDchartOptionLabelResttimetype,
    this.navCentermanagementDchartFoodLabel,
    this.navCentermanagementDchartRoomCountAlertMessage,
    this.navCentermanagementDchartViewIndexTitleFood,
    this.navCentermanagementDchartLabel,
    this.navCentermanagementDchartButtonAddnew,
    this.navCentermanagementDchartViewIndexTitleResttime,
    this.navCentermanagementDchartChildCountAlertMessage,
    this.navCentermanagementAttendanceCheckIndexTitle,
    this.navCentermanagementDchartCurrentviewReportLabelNappy,
    this.navCentermanagementDchartOptionIndexTitle,
    this.navCentermanagementDchartOptionLabelResttimetypeApplyBtn,
    this.navCentermanagementDchartCurrentdetailedviewIndexTitle,
    this.navCentermanagementDchartCurrentviewReportLabelFood,
    this.navCentermanagementAttendanceCheckLabelNew,
    this.navCentermanagementAttendanceCheckTabLabelIn,
    this.navCentermanagementDchartOptionLabel,
    this.navCentermanagementDchartSettingsType,
    this.navCentermanagementDchartOptionLabelToilettrainingtype,
    this.navCentermanagementDchartOptionLabelToilettrainingcategory,
    this.navCentermanagementDchartSleepCheckLabel,
    this.navEntermanagementDchartMessageEmpty,
    this.navCentermanagementDchartCurrentviewLabel,
    this.navCentermanagementDchartIndexTitle,
    this.navCentermanagementDchartSettingsLabel,
    this.navCentermanagementDchartFoodAlertNomenuSelectedMessage,
    this.navCentermanagementDchartMenuSettingLabel,
    this.navCentermanagementDchartCurrentviewReportLabelResttime,
    this.navCentermanagementDchartOptionLabelServetype,
    this.navCentermanagementDchartSleepCheck,
    this.navCentermanagementDchartFoodRecipeLabelRecipe,
    this.navCentermanagementDchartViewIndexTitle,
    this.navCentermanagementDchartFormResttimeTitle,
    this.navCentermanagementDchartFormFoodTitle,
    this.navCentermanagementDchartBottlefeedType,
    this.navCentermanagementDchartFoodNoitemMessage,
    this.navCentermanagementDchartFoodRecipeLabel,
    this.navCentermanagementDchartResttimeLabel,
    this.navCentermanagementDchartBottlefeedLabel,
  });

  factory CenterManagementDailyChartSection.fromJson(Map<String, dynamic> json) => new CenterManagementDailyChartSection(
    navCentermanagementDchartOptionLabelMenucategory: json["nav_centermanagement_dchart_option_label_menucategory"],
    navCentermanagementDchartFormToilettrainingTitle: json["nav_centermanagement_dchart_form_toilettraining_title"],
    navCentermanagementDchartViewIndexTitleEmptyMessage: json["nav_centermanagement_dchart_view_index_title_empty_message"],
    navCentermanagementAttendanceCheckLogLabel: json["nav_centermanagement_attendance_check_log_label"],
    navCentermanagementDchartToilettrinningLabel: json["nav_centermanagement_dchart_toilettrinning_label"],
    navCentermanagementDchartIndexRoomTitle: json["nav_centermanagement_dchart_index_room_title"],
    navCentermanagementDchartViewIndexTitleBottleFeed: json["nav_centermanagement_dchart_view_index_title_bottle_feed"],
    navCentermanagementDchartOptionLabelServetypeApplyBtn: json["nav_centermanagement_dchart_option_label_servetype_apply_btn"],
    navCentermanagementDchartFormResttimeLabel: json["nav_centermanagement_dchart_form_resttime_label"],
    navCentermanagementDchartFoodAlertNochildWeekdayMessage: json["nav_centermanagement_dchart_food_alert_nochild_weekday_message"],
    navCentermanagementDchartFoodRecipeIndexTitle: json["nav_centermanagement_dchart_food_recipe_index_title"],
    navCentermanagementAttendanceCheckTabLabelOut: json["nav_centermanagement_attendance_check_tab_label_out"],
    navCentermanagementDchartFoodAlertNochildMessage: json["nav_centermanagement_dchart_food_alert_nochild_message"],
    navCentermanagementDchartViewIndexTitleToilettraining: json["nav_centermanagement_dchart_view_index_title_toilettraining"],
    navCentermanagementDchartFoodRecipeLabelMenuset: json["nav_centermanagement_dchart_food_recipe_label_menuset"],
    navCentermanagementDchartCurrentviewIndexTitle: json["nav_centermanagement_dchart_currentview_index_title"],
    navCentermanagementDchartCurrentdetailedviewLabel: json["nav_centermanagement_dchart_currentdetailedview_label"],
    navCentermanagementDchartBottlefeed: json["nav_centermanagement_dchart_bottlefeed"],
    navCentermanagementDchartOptionLabelResttimetype: json["nav_centermanagement_dchart_option_label_resttimetype"],
    navCentermanagementDchartFoodLabel: json["nav_centermanagement_dchart_food_label"],
    navCentermanagementDchartRoomCountAlertMessage: json["nav_centermanagement_dchart_room_count_alert_message"],
    navCentermanagementDchartViewIndexTitleFood: json["nav_centermanagement_dchart_view_index_title_food"],
    navCentermanagementDchartLabel: json["nav_centermanagement_dchart_label"],
    navCentermanagementDchartButtonAddnew: json["nav_centermanagement_dchart_button_addnew"],
    navCentermanagementDchartViewIndexTitleResttime: json["nav_centermanagement_dchart_view_index_title_resttime"],
    navCentermanagementDchartChildCountAlertMessage: json["nav_centermanagement_dchart_child_count_alert_message"],
    navCentermanagementAttendanceCheckIndexTitle: json["nav_centermanagement_attendance_check_index_title"],
    navCentermanagementDchartCurrentviewReportLabelNappy: json["nav_centermanagement_dchart_currentview_report_label_nappy"],
    navCentermanagementDchartOptionIndexTitle: json["nav_centermanagement_dchart_option_index_title"],
    navCentermanagementDchartOptionLabelResttimetypeApplyBtn: json["nav_centermanagement_dchart_option_label_resttimetype_apply_btn"],
    navCentermanagementDchartCurrentdetailedviewIndexTitle: json["nav_centermanagement_dchart_currentdetailedview_index_title"],
    navCentermanagementDchartCurrentviewReportLabelFood: json["nav_centermanagement_dchart_currentview_report_label_food"],
    navCentermanagementAttendanceCheckLabelNew: json["nav_centermanagement_attendance_check_label_new"],
    navCentermanagementAttendanceCheckTabLabelIn: json["nav_centermanagement_attendance_check_tab_label_in"],
    navCentermanagementDchartOptionLabel: json["nav_centermanagement_dchart_option_label"],
    navCentermanagementDchartSettingsType: json["nav_centermanagement_dchart_settings_type"],
    navCentermanagementDchartOptionLabelToilettrainingtype: json["nav_centermanagement_dchart_option_label_toilettrainingtype"],
    navCentermanagementDchartOptionLabelToilettrainingcategory: json["nav_centermanagement_dchart_option_label_toilettrainingcategory"],
    navCentermanagementDchartSleepCheckLabel: json["nav_centermanagement_dchart_sleep_check_label"],
    navEntermanagementDchartMessageEmpty: json["nav_entermanagement_dchart_message_empty"],
    navCentermanagementDchartCurrentviewLabel: json["nav_centermanagement_dchart_currentview_label"],
    navCentermanagementDchartIndexTitle: json["nav_centermanagement_dchart_index_title"],
    navCentermanagementDchartSettingsLabel: json["nav_centermanagement_dchart_settings_label"],
    navCentermanagementDchartFoodAlertNomenuSelectedMessage: json["nav_centermanagement_dchart_food_alert_nomenu_selected_message"],
    navCentermanagementDchartMenuSettingLabel: json["nav_centermanagement_dchart_menu_setting_label"],
    navCentermanagementDchartCurrentviewReportLabelResttime: json["nav_centermanagement_dchart_currentview_report_label_resttime"],
    navCentermanagementDchartOptionLabelServetype: json["nav_centermanagement_dchart_option_label_servetype"],
    navCentermanagementDchartSleepCheck: json["nav_centermanagement_dchart_sleep_check"],
    navCentermanagementDchartFoodRecipeLabelRecipe: json["nav_centermanagement_dchart_food_recipe_label_recipe"],
    navCentermanagementDchartViewIndexTitle: json["nav_centermanagement_dchart_view_index_title"],
    navCentermanagementDchartFormResttimeTitle: json["nav_centermanagement_dchart_form_resttime_title"],
    navCentermanagementDchartFormFoodTitle: json["nav_centermanagement_dchart_form_food_title"],
    navCentermanagementDchartBottlefeedType: json["nav_centermanagement_dchart_bottlefeed_type"],
    navCentermanagementDchartFoodNoitemMessage: json["nav_centermanagement_dchart_food_noitem_message"],
    navCentermanagementDchartFoodRecipeLabel: json["nav_centermanagement_dchart_food_recipe_label"],
    navCentermanagementDchartResttimeLabel: json["nav_centermanagement_dchart_resttime_label"],
    navCentermanagementDchartBottlefeedLabel: json["nav_centermanagement_dchart_bottlefeed_label"],
  );

  Map<String, dynamic> toJson() => {
    "nav_centermanagement_dchart_option_label_menucategory": navCentermanagementDchartOptionLabelMenucategory,
    "nav_centermanagement_dchart_form_toilettraining_title": navCentermanagementDchartFormToilettrainingTitle,
    "nav_centermanagement_dchart_view_index_title_empty_message": navCentermanagementDchartViewIndexTitleEmptyMessage,
    "nav_centermanagement_attendance_check_log_label": navCentermanagementAttendanceCheckLogLabel,
    "nav_centermanagement_dchart_toilettrinning_label": navCentermanagementDchartToilettrinningLabel,
    "nav_centermanagement_dchart_index_room_title": navCentermanagementDchartIndexRoomTitle,
    "nav_centermanagement_dchart_view_index_title_bottle_feed": navCentermanagementDchartViewIndexTitleBottleFeed,
    "nav_centermanagement_dchart_option_label_servetype_apply_btn": navCentermanagementDchartOptionLabelServetypeApplyBtn,
    "nav_centermanagement_dchart_form_resttime_label": navCentermanagementDchartFormResttimeLabel,
    "nav_centermanagement_dchart_food_alert_nochild_weekday_message": navCentermanagementDchartFoodAlertNochildWeekdayMessage,
    "nav_centermanagement_dchart_food_recipe_index_title": navCentermanagementDchartFoodRecipeIndexTitle,
    "nav_centermanagement_attendance_check_tab_label_out": navCentermanagementAttendanceCheckTabLabelOut,
    "nav_centermanagement_dchart_food_alert_nochild_message": navCentermanagementDchartFoodAlertNochildMessage,
    "nav_centermanagement_dchart_view_index_title_toilettraining": navCentermanagementDchartViewIndexTitleToilettraining,
    "nav_centermanagement_dchart_food_recipe_label_menuset": navCentermanagementDchartFoodRecipeLabelMenuset,
    "nav_centermanagement_dchart_currentview_index_title": navCentermanagementDchartCurrentviewIndexTitle,
    "nav_centermanagement_dchart_currentdetailedview_label": navCentermanagementDchartCurrentdetailedviewLabel,
    "nav_centermanagement_dchart_bottlefeed": navCentermanagementDchartBottlefeed,
    "nav_centermanagement_dchart_option_label_resttimetype": navCentermanagementDchartOptionLabelResttimetype,
    "nav_centermanagement_dchart_food_label": navCentermanagementDchartFoodLabel,
    "nav_centermanagement_dchart_room_count_alert_message": navCentermanagementDchartRoomCountAlertMessage,
    "nav_centermanagement_dchart_view_index_title_food": navCentermanagementDchartViewIndexTitleFood,
    "nav_centermanagement_dchart_label": navCentermanagementDchartLabel,
    "nav_centermanagement_dchart_button_addnew": navCentermanagementDchartButtonAddnew,
    "nav_centermanagement_dchart_view_index_title_resttime": navCentermanagementDchartViewIndexTitleResttime,
    "nav_centermanagement_dchart_child_count_alert_message": navCentermanagementDchartChildCountAlertMessage,
    "nav_centermanagement_attendance_check_index_title": navCentermanagementAttendanceCheckIndexTitle,
    "nav_centermanagement_dchart_currentview_report_label_nappy": navCentermanagementDchartCurrentviewReportLabelNappy,
    "nav_centermanagement_dchart_option_index_title": navCentermanagementDchartOptionIndexTitle,
    "nav_centermanagement_dchart_option_label_resttimetype_apply_btn": navCentermanagementDchartOptionLabelResttimetypeApplyBtn,
    "nav_centermanagement_dchart_currentdetailedview_index_title": navCentermanagementDchartCurrentdetailedviewIndexTitle,
    "nav_centermanagement_dchart_currentview_report_label_food": navCentermanagementDchartCurrentviewReportLabelFood,
    "nav_centermanagement_attendance_check_label_new": navCentermanagementAttendanceCheckLabelNew,
    "nav_centermanagement_attendance_check_tab_label_in": navCentermanagementAttendanceCheckTabLabelIn,
    "nav_centermanagement_dchart_option_label": navCentermanagementDchartOptionLabel,
    "nav_centermanagement_dchart_settings_type": navCentermanagementDchartSettingsType,
    "nav_centermanagement_dchart_option_label_toilettrainingtype": navCentermanagementDchartOptionLabelToilettrainingtype,
    "nav_centermanagement_dchart_option_label_toilettrainingcategory": navCentermanagementDchartOptionLabelToilettrainingcategory,
    "nav_centermanagement_dchart_sleep_check_label": navCentermanagementDchartSleepCheckLabel,
    "nav_entermanagement_dchart_message_empty": navEntermanagementDchartMessageEmpty,
    "nav_centermanagement_dchart_currentview_label": navCentermanagementDchartCurrentviewLabel,
    "nav_centermanagement_dchart_index_title": navCentermanagementDchartIndexTitle,
    "nav_centermanagement_dchart_settings_label": navCentermanagementDchartSettingsLabel,
    "nav_centermanagement_dchart_food_alert_nomenu_selected_message": navCentermanagementDchartFoodAlertNomenuSelectedMessage,
    "nav_centermanagement_dchart_menu_setting_label": navCentermanagementDchartMenuSettingLabel,
    "nav_centermanagement_dchart_currentview_report_label_resttime": navCentermanagementDchartCurrentviewReportLabelResttime,
    "nav_centermanagement_dchart_option_label_servetype": navCentermanagementDchartOptionLabelServetype,
    "nav_centermanagement_dchart_sleep_check": navCentermanagementDchartSleepCheck,
    "nav_centermanagement_dchart_food_recipe_label_recipe": navCentermanagementDchartFoodRecipeLabelRecipe,
    "nav_centermanagement_dchart_view_index_title": navCentermanagementDchartViewIndexTitle,
    "nav_centermanagement_dchart_form_resttime_title": navCentermanagementDchartFormResttimeTitle,
    "nav_centermanagement_dchart_form_food_title": navCentermanagementDchartFormFoodTitle,
    "nav_centermanagement_dchart_bottlefeed_type": navCentermanagementDchartBottlefeedType,
    "nav_centermanagement_dchart_food_noitem_message": navCentermanagementDchartFoodNoitemMessage,
    "nav_centermanagement_dchart_food_recipe_label": navCentermanagementDchartFoodRecipeLabel,
    "nav_centermanagement_dchart_resttime_label": navCentermanagementDchartResttimeLabel,
    "nav_centermanagement_dchart_bottlefeed_label": navCentermanagementDchartBottlefeedLabel,
  };
}

class CenterManagementFollowupSection {
  String navCentermanagementFollowupButton;
  String navCentermanagementFollowup;
  String navCentermanagementFollowupButtonAddnew;
  String navCentermanagementFollowupButtonView;
  String navCentermanagementFollowupIndexTitle;
  String navCentermanagementStaffReflection;
  String navCentermanagementStaffReflectionAddnew;
  String navCentermanagementStaffReflectionIndexTitle;
  String navCentermanagementFollowupButtonAddnewDraft;
  String navCentermanagementStaffReflectionView;
  String navCentermanagementStaffReflectionLabel;

  CenterManagementFollowupSection({
    this.navCentermanagementFollowupButton,
    this.navCentermanagementFollowup,
    this.navCentermanagementFollowupButtonAddnew,
    this.navCentermanagementFollowupButtonView,
    this.navCentermanagementFollowupIndexTitle,
    this.navCentermanagementStaffReflection,
    this.navCentermanagementStaffReflectionAddnew,
    this.navCentermanagementStaffReflectionIndexTitle,
    this.navCentermanagementFollowupButtonAddnewDraft,
    this.navCentermanagementStaffReflectionView,
    this.navCentermanagementStaffReflectionLabel,
  });

  factory CenterManagementFollowupSection.fromJson(Map<String, dynamic> json) => new CenterManagementFollowupSection(
    navCentermanagementFollowupButton: json["nav_centermanagement_followup_button"],
    navCentermanagementFollowup: json["nav_centermanagement_followup"],
    navCentermanagementFollowupButtonAddnew: json["nav_centermanagement_followup_button_addnew"],
    navCentermanagementFollowupButtonView: json["nav_centermanagement_followup_button_view"],
    navCentermanagementFollowupIndexTitle: json["nav_centermanagement_followup_index_title"],
    navCentermanagementStaffReflection: json["nav_centermanagement_staff_reflection"],
    navCentermanagementStaffReflectionAddnew: json["nav_centermanagement_staff_reflection_addnew"],
    navCentermanagementStaffReflectionIndexTitle: json["nav_centermanagement_staff_reflection_index_title"],
    navCentermanagementFollowupButtonAddnewDraft: json["nav_centermanagement_followup_button_addnew_draft"],
    navCentermanagementStaffReflectionView: json["nav_centermanagement_staff_reflection_view"],
    navCentermanagementStaffReflectionLabel: json["nav_centermanagement_staff_reflection_label"],
  );

  Map<String, dynamic> toJson() => {
    "nav_centermanagement_followup_button": navCentermanagementFollowupButton,
    "nav_centermanagement_followup": navCentermanagementFollowup,
    "nav_centermanagement_followup_button_addnew": navCentermanagementFollowupButtonAddnew,
    "nav_centermanagement_followup_button_view": navCentermanagementFollowupButtonView,
    "nav_centermanagement_followup_index_title": navCentermanagementFollowupIndexTitle,
    "nav_centermanagement_staff_reflection": navCentermanagementStaffReflection,
    "nav_centermanagement_staff_reflection_addnew": navCentermanagementStaffReflectionAddnew,
    "nav_centermanagement_staff_reflection_index_title": navCentermanagementStaffReflectionIndexTitle,
    "nav_centermanagement_followup_button_addnew_draft": navCentermanagementFollowupButtonAddnewDraft,
    "nav_centermanagement_staff_reflection_view": navCentermanagementStaffReflectionView,
    "nav_centermanagement_staff_reflection_label": navCentermanagementStaffReflectionLabel,
  };
}

class CenterManagementJournalSection {
  String navCentermanagementJournalWarningMessage;
  String navCentermanagementJournalDraftTitle;
  String navCentermanagementJournalFormLabelLearningtags;
  String navCentermanagementJournalFormLabelFollowup;
  String navCentermanagementJournalFormLabelEnvironment;
  String navCentermanagementJournalFormLabelInterpretation;
  String navCentermanagementJournalImagesMessageEmpty;
  String navCentermanagementJournalButtonAddnewDraft;
  String navCentermanagementJournalFormLabelLearningtagsFollowup;
  String navCentermanagementJournalViewComment;
  String navCentermanagementJournalLabel;
  String navCentermanagementJournalReportLabelChildlist;
  String navCentermanagementJournalFormLabelWhathappened;
  String navCentermanagementJournalViewCommentButton;
  String navCentermanagementJournalFormLabelRoom;
  String navCentermanagementJournalButtonAddnew;
  String navCentermanagementJournalLabelTableRoom;
  String navCentermanagementJournalReportSingleTitle;
  String navCentermanagementJournalLabelFollowup;
  String navCentermanagementJournalFormLabelObservationdate;
  String navCentermanagementJournalIndexTitle;
  String navCentermanagementJournalFormLabelStatus;
  String navCentermanagementJournalFormLabelFollowupImages;
  String navCentermanagementJournalFormLabelWhathappen;
  String navCentermanagementJournalFormLabelReflection;
  String navCentermanagementJournalFormLabelNewLearningtags;
  String navCentermanagementJournalViewCommentModalTitle;
  String navCentermanagementJournalFormLabelOption;
  String navCentermanagementJournalFormLabelFollowupdate;
  String navCentermanagementJournalFormLabelEducator;
  String navCentermanagementJournalViewCommentEmptyMessage;
  String navCentermanagementJournalLabelDraft;
  String navCentermanagementJournalLabelFilter;

  CenterManagementJournalSection({
    this.navCentermanagementJournalWarningMessage,
    this.navCentermanagementJournalDraftTitle,
    this.navCentermanagementJournalFormLabelLearningtags,
    this.navCentermanagementJournalFormLabelFollowup,
    this.navCentermanagementJournalFormLabelEnvironment,
    this.navCentermanagementJournalFormLabelInterpretation,
    this.navCentermanagementJournalImagesMessageEmpty,
    this.navCentermanagementJournalButtonAddnewDraft,
    this.navCentermanagementJournalFormLabelLearningtagsFollowup,
    this.navCentermanagementJournalViewComment,
    this.navCentermanagementJournalLabel,
    this.navCentermanagementJournalReportLabelChildlist,
    this.navCentermanagementJournalFormLabelWhathappened,
    this.navCentermanagementJournalViewCommentButton,
    this.navCentermanagementJournalFormLabelRoom,
    this.navCentermanagementJournalButtonAddnew,
    this.navCentermanagementJournalLabelTableRoom,
    this.navCentermanagementJournalReportSingleTitle,
    this.navCentermanagementJournalLabelFollowup,
    this.navCentermanagementJournalFormLabelObservationdate,
    this.navCentermanagementJournalIndexTitle,
    this.navCentermanagementJournalFormLabelStatus,
    this.navCentermanagementJournalFormLabelFollowupImages,
    this.navCentermanagementJournalFormLabelWhathappen,
    this.navCentermanagementJournalFormLabelReflection,
    this.navCentermanagementJournalFormLabelNewLearningtags,
    this.navCentermanagementJournalViewCommentModalTitle,
    this.navCentermanagementJournalFormLabelOption,
    this.navCentermanagementJournalFormLabelFollowupdate,
    this.navCentermanagementJournalFormLabelEducator,
    this.navCentermanagementJournalViewCommentEmptyMessage,
    this.navCentermanagementJournalLabelDraft,
    this.navCentermanagementJournalLabelFilter,
  });

  factory CenterManagementJournalSection.fromJson(Map<String, dynamic> json) => new CenterManagementJournalSection(
    navCentermanagementJournalWarningMessage: json["nav_centermanagement_journal_warning_message"],
    navCentermanagementJournalDraftTitle: json["nav_centermanagement_journal_draft_title"],
    navCentermanagementJournalFormLabelLearningtags: json["nav_centermanagement_journal_form_label_learningtags"],
    navCentermanagementJournalFormLabelFollowup: json["nav_centermanagement_journal_form_label_followup"],
    navCentermanagementJournalFormLabelEnvironment: json["nav_centermanagement_journal_form_label_environment"],
    navCentermanagementJournalFormLabelInterpretation: json["nav_centermanagement_journal_form_label_interpretation"],
    navCentermanagementJournalImagesMessageEmpty: json["nav_centermanagement_journal_images_message_empty"],
    navCentermanagementJournalButtonAddnewDraft: json["nav_centermanagement_journal_button_addnew_draft"],
    navCentermanagementJournalFormLabelLearningtagsFollowup: json["nav_centermanagement_journal_form_label_learningtags_followup"],
    navCentermanagementJournalViewComment: json["nav_centermanagement_journal_view_comment"],
    navCentermanagementJournalLabel: json["nav_centermanagement_journal_label"],
    navCentermanagementJournalReportLabelChildlist: json["nav_centermanagement_journal_report_label_childlist"],
    navCentermanagementJournalFormLabelWhathappened: json["nav_centermanagement_journal_form_label_whathappened"],
    navCentermanagementJournalViewCommentButton: json["nav_centermanagement_journal_view_comment_button"],
    navCentermanagementJournalFormLabelRoom: json["nav_centermanagement_journal_form_label_room"],
    navCentermanagementJournalButtonAddnew: json["nav_centermanagement_journal_button_addnew"],
    navCentermanagementJournalLabelTableRoom: json["nav_centermanagement_journal_label_table_room"],
    navCentermanagementJournalReportSingleTitle: json["nav_centermanagement_journal_report_single_title"],
    navCentermanagementJournalLabelFollowup: json["nav_centermanagement_journal_label_followup"],
    navCentermanagementJournalFormLabelObservationdate: json["nav_centermanagement_journal_form_label_observationdate"],
    navCentermanagementJournalIndexTitle: json["nav_centermanagement_journal_index_title"],
    navCentermanagementJournalFormLabelStatus: json["nav_centermanagement_journal_form_label_status"],
    navCentermanagementJournalFormLabelFollowupImages: json["nav_centermanagement_journal_form_label_followup_images"],
    navCentermanagementJournalFormLabelWhathappen: json["nav_centermanagement_journal_form_label_whathappen"],
    navCentermanagementJournalFormLabelReflection: json["nav_centermanagement_journal_form_label_reflection"],
    navCentermanagementJournalFormLabelNewLearningtags: json["nav_centermanagement_journal_form_label_new_learningtags"],
    navCentermanagementJournalViewCommentModalTitle: json["nav_centermanagement_journal_view_comment_modal_title"],
    navCentermanagementJournalFormLabelOption: json["nav_centermanagement_journal_form_label_option"],
    navCentermanagementJournalFormLabelFollowupdate: json["nav_centermanagement_journal_form_label_followupdate"],
    navCentermanagementJournalFormLabelEducator: json["nav_centermanagement_journal_form_label_educator"],
    navCentermanagementJournalViewCommentEmptyMessage: json["nav_centermanagement_journal_view_comment_empty_message"],
    navCentermanagementJournalLabelDraft: json["nav_centermanagement_journal_label_draft"],
    navCentermanagementJournalLabelFilter: json["nav_centermanagement_journal_label_filter"],
  );

  Map<String, dynamic> toJson() => {
    "nav_centermanagement_journal_warning_message": navCentermanagementJournalWarningMessage,
    "nav_centermanagement_journal_draft_title": navCentermanagementJournalDraftTitle,
    "nav_centermanagement_journal_form_label_learningtags": navCentermanagementJournalFormLabelLearningtags,
    "nav_centermanagement_journal_form_label_followup": navCentermanagementJournalFormLabelFollowup,
    "nav_centermanagement_journal_form_label_environment": navCentermanagementJournalFormLabelEnvironment,
    "nav_centermanagement_journal_form_label_interpretation": navCentermanagementJournalFormLabelInterpretation,
    "nav_centermanagement_journal_images_message_empty": navCentermanagementJournalImagesMessageEmpty,
    "nav_centermanagement_journal_button_addnew_draft": navCentermanagementJournalButtonAddnewDraft,
    "nav_centermanagement_journal_form_label_learningtags_followup": navCentermanagementJournalFormLabelLearningtagsFollowup,
    "nav_centermanagement_journal_view_comment": navCentermanagementJournalViewComment,
    "nav_centermanagement_journal_label": navCentermanagementJournalLabel,
    "nav_centermanagement_journal_report_label_childlist": navCentermanagementJournalReportLabelChildlist,
    "nav_centermanagement_journal_form_label_whathappened": navCentermanagementJournalFormLabelWhathappened,
    "nav_centermanagement_journal_view_comment_button": navCentermanagementJournalViewCommentButton,
    "nav_centermanagement_journal_form_label_room": navCentermanagementJournalFormLabelRoom,
    "nav_centermanagement_journal_button_addnew": navCentermanagementJournalButtonAddnew,
    "nav_centermanagement_journal_label_table_room": navCentermanagementJournalLabelTableRoom,
    "nav_centermanagement_journal_report_single_title": navCentermanagementJournalReportSingleTitle,
    "nav_centermanagement_journal_label_followup": navCentermanagementJournalLabelFollowup,
    "nav_centermanagement_journal_form_label_observationdate": navCentermanagementJournalFormLabelObservationdate,
    "nav_centermanagement_journal_index_title": navCentermanagementJournalIndexTitle,
    "nav_centermanagement_journal_form_label_status": navCentermanagementJournalFormLabelStatus,
    "nav_centermanagement_journal_form_label_followup_images": navCentermanagementJournalFormLabelFollowupImages,
    "nav_centermanagement_journal_form_label_whathappen": navCentermanagementJournalFormLabelWhathappen,
    "nav_centermanagement_journal_form_label_reflection": navCentermanagementJournalFormLabelReflection,
    "nav_centermanagement_journal_form_label_new_learningtags": navCentermanagementJournalFormLabelNewLearningtags,
    "nav_centermanagement_journal_view_comment_modal_title": navCentermanagementJournalViewCommentModalTitle,
    "nav_centermanagement_journal_form_label_option": navCentermanagementJournalFormLabelOption,
    "nav_centermanagement_journal_form_label_followupdate": navCentermanagementJournalFormLabelFollowupdate,
    "nav_centermanagement_journal_form_label_educator": navCentermanagementJournalFormLabelEducator,
    "nav_centermanagement_journal_view_comment_empty_message": navCentermanagementJournalViewCommentEmptyMessage,
    "nav_centermanagement_journal_label_draft": navCentermanagementJournalLabelDraft,
    "nav_centermanagement_journal_label_filter": navCentermanagementJournalLabelFilter,
  };
}

class CenterManagementJourneySection {
  String navCentermanagementJourneyFormLabelFollowupdate;
  String navStafreflectionFormLabelLearningtags;
  String navStafreflectionFormLabelDate;
  String navCentermanagementJourneyButtonAddnewDraft;
  String navCentermanagementJourneyLabel;
  String navCentermanagementJourneyFormLabelWhathappened;
  String navCentermanagementJourneyLabelFollowup;
  String navCentermanagementJourneyFormLabelLearningtagsFollowup;
  String navCentermanagementJourneyFormLabelObserver;
  String navCentermanagementJourneyLabelTableRoom;
  String navCentermanagementJournalImagesMessageEmpty;
  String navCentermanagementJourneyLabelDraft;
  String navCentermanagementJournalVideosMessageEmpty;
  String navCentermanagementJourneyButtonAddnew;
  String navCentermanagementJourneyFormLabelStatus;
  String navCentermanagementJourneyFormLabelObservationdate;
  String navCentermanagementJourneyFormLabelInterpretation;
  String navStafreflectionFormLabelEducator;
  String navCentermanagementJourneyFormLabelFollowup;
  String navCentermanagementJournalPdfsMessageEmpty;
  String navCentermanagementJourneyWarningMessage;
  String navCentermanagementNewsletterReportSingleTitle;
  String navCentermanagementJourneyFormLabelReflection;
  String navCentermanagementJourneyFormLabelPractice;
  String navCentermanagementLastModifiedBy;
  String navCentermanagementCreatedBy;
  String navCentermanagementJourneyFormLabelLearningtags;
  String navCentermanagementJourneyFormLabelPrinciples;
  String navStafreflectionFormLabelLearningOutcomes;
  String navCentermanagementJourneyDraftTitle;
  String navCentermanagementJourneyFormLabelNewLearningtags;
  String navCentermanagementJourneyLabelFilter;
  String navCentermanagementJourneyFormLabelFollowupImages;
  String navCentermanagementJourneyImagesMessageEmpty;
  String navCentermanagementJourneyFormLabelObservation;
  String navCentermanagementJourneyIndexTitle;
  String navStafreflectionFormLabelStatus;
  String navCentermanagementJourneyReportSingleTitle;

  CenterManagementJourneySection({
    this.navCentermanagementJourneyFormLabelFollowupdate,
    this.navStafreflectionFormLabelLearningtags,
    this.navStafreflectionFormLabelDate,
    this.navCentermanagementJourneyButtonAddnewDraft,
    this.navCentermanagementJourneyLabel,
    this.navCentermanagementJourneyFormLabelWhathappened,
    this.navCentermanagementJourneyLabelFollowup,
    this.navCentermanagementJourneyFormLabelLearningtagsFollowup,
    this.navCentermanagementJourneyFormLabelObserver,
    this.navCentermanagementJourneyLabelTableRoom,
    this.navCentermanagementJournalImagesMessageEmpty,
    this.navCentermanagementJourneyLabelDraft,
    this.navCentermanagementJournalVideosMessageEmpty,
    this.navCentermanagementJourneyButtonAddnew,
    this.navCentermanagementJourneyFormLabelStatus,
    this.navCentermanagementJourneyFormLabelObservationdate,
    this.navCentermanagementJourneyFormLabelInterpretation,
    this.navStafreflectionFormLabelEducator,
    this.navCentermanagementJourneyFormLabelFollowup,
    this.navCentermanagementJournalPdfsMessageEmpty,
    this.navCentermanagementJourneyWarningMessage,
    this.navCentermanagementNewsletterReportSingleTitle,
    this.navCentermanagementJourneyFormLabelReflection,
    this.navCentermanagementJourneyFormLabelPractice,
    this.navCentermanagementLastModifiedBy,
    this.navCentermanagementCreatedBy,
    this.navCentermanagementJourneyFormLabelLearningtags,
    this.navCentermanagementJourneyFormLabelPrinciples,
    this.navStafreflectionFormLabelLearningOutcomes,
    this.navCentermanagementJourneyDraftTitle,
    this.navCentermanagementJourneyFormLabelNewLearningtags,
    this.navCentermanagementJourneyLabelFilter,
    this.navCentermanagementJourneyFormLabelFollowupImages,
    this.navCentermanagementJourneyImagesMessageEmpty,
    this.navCentermanagementJourneyFormLabelObservation,
    this.navCentermanagementJourneyIndexTitle,
    this.navStafreflectionFormLabelStatus,
    this.navCentermanagementJourneyReportSingleTitle,
  });

  factory CenterManagementJourneySection.fromJson(Map<String, dynamic> json) => new CenterManagementJourneySection(
    navCentermanagementJourneyFormLabelFollowupdate: json["nav_centermanagement_journey_form_label_followupdate"],
    navStafreflectionFormLabelLearningtags: json["nav_stafreflection_form_label_learningtags"],
    navStafreflectionFormLabelDate: json["nav_stafreflection_form_label_date"],
    navCentermanagementJourneyButtonAddnewDraft: json["nav_centermanagement_journey_button_addnew_draft"],
    navCentermanagementJourneyLabel: json["nav_centermanagement_journey_label"],
    navCentermanagementJourneyFormLabelWhathappened: json["nav_centermanagement_journey_form_label_whathappened"],
    navCentermanagementJourneyLabelFollowup: json["nav_centermanagement_journey_label_followup"],
    navCentermanagementJourneyFormLabelLearningtagsFollowup: json["nav_centermanagement_journey_form_label_learningtags_followup"],
    navCentermanagementJourneyFormLabelObserver: json["nav_centermanagement_journey_form_label_observer"],
    navCentermanagementJourneyLabelTableRoom: json["nav_centermanagement_journey_label_table_room"],
    navCentermanagementJournalImagesMessageEmpty: json["nav_centermanagement_journal_images_message_empty"],
    navCentermanagementJourneyLabelDraft: json["nav_centermanagement_journey_label_draft"],
    navCentermanagementJournalVideosMessageEmpty: json["nav_centermanagement_journal_videos_message_empty"],
    navCentermanagementJourneyButtonAddnew: json["nav_centermanagement_journey_button_addnew"],
    navCentermanagementJourneyFormLabelStatus: json["nav_centermanagement_journey_form_label_status"],
    navCentermanagementJourneyFormLabelObservationdate: json["nav_centermanagement_journey_form_label_observationdate"],
    navCentermanagementJourneyFormLabelInterpretation: json["nav_centermanagement_journey_form_label_interpretation"],
    navStafreflectionFormLabelEducator: json["nav_stafreflection_form_label_educator"],
    navCentermanagementJourneyFormLabelFollowup: json["nav_centermanagement_journey_form_label_followup"],
    navCentermanagementJournalPdfsMessageEmpty: json["nav_centermanagement_journal_pdfs_message_empty"],
    navCentermanagementJourneyWarningMessage: json["nav_centermanagement_journey_warning_message"],
    navCentermanagementNewsletterReportSingleTitle: json["nav_centermanagement_newsletter_report_single_title"],
    navCentermanagementJourneyFormLabelReflection: json["nav_centermanagement_journey_form_label_reflection"],
    navCentermanagementJourneyFormLabelPractice: json["nav_centermanagement_journey_form_label_practice"],
    navCentermanagementLastModifiedBy: json["nav_centermanagement_last_modified_by"],
    navCentermanagementCreatedBy: json["nav_centermanagement_created_by"],
    navCentermanagementJourneyFormLabelLearningtags: json["nav_centermanagement_journey_form_label_learningtags"],
    navCentermanagementJourneyFormLabelPrinciples: json["nav_centermanagement_journey_form_label_principles"],
    navStafreflectionFormLabelLearningOutcomes: json["nav_stafreflection_form_label_learning_outcomes"],
    navCentermanagementJourneyDraftTitle: json["nav_centermanagement_journey_draft_title"],
    navCentermanagementJourneyFormLabelNewLearningtags: json["nav_centermanagement_journey_form_label_new_learningtags"],
    navCentermanagementJourneyLabelFilter: json["nav_centermanagement_journey_label_filter"],
    navCentermanagementJourneyFormLabelFollowupImages: json["nav_centermanagement_journey_form_label_followup_images"],
    navCentermanagementJourneyImagesMessageEmpty: json["nav_centermanagement_journey_images_message_empty"],
    navCentermanagementJourneyFormLabelObservation: json["nav_centermanagement_journey_form_label_observation"],
    navCentermanagementJourneyIndexTitle: json["nav_centermanagement_journey_index_title"],
    navStafreflectionFormLabelStatus: json["nav_stafreflection_form_label_status"],
    navCentermanagementJourneyReportSingleTitle: json["nav_centermanagement_journey_report_single_title"],
  );

  Map<String, dynamic> toJson() => {
    "nav_centermanagement_journey_form_label_followupdate": navCentermanagementJourneyFormLabelFollowupdate,
    "nav_stafreflection_form_label_learningtags": navStafreflectionFormLabelLearningtags,
    "nav_stafreflection_form_label_date": navStafreflectionFormLabelDate,
    "nav_centermanagement_journey_button_addnew_draft": navCentermanagementJourneyButtonAddnewDraft,
    "nav_centermanagement_journey_label": navCentermanagementJourneyLabel,
    "nav_centermanagement_journey_form_label_whathappened": navCentermanagementJourneyFormLabelWhathappened,
    "nav_centermanagement_journey_label_followup": navCentermanagementJourneyLabelFollowup,
    "nav_centermanagement_journey_form_label_learningtags_followup": navCentermanagementJourneyFormLabelLearningtagsFollowup,
    "nav_centermanagement_journey_form_label_observer": navCentermanagementJourneyFormLabelObserver,
    "nav_centermanagement_journey_label_table_room": navCentermanagementJourneyLabelTableRoom,
    "nav_centermanagement_journal_images_message_empty": navCentermanagementJournalImagesMessageEmpty,
    "nav_centermanagement_journey_label_draft": navCentermanagementJourneyLabelDraft,
    "nav_centermanagement_journal_videos_message_empty": navCentermanagementJournalVideosMessageEmpty,
    "nav_centermanagement_journey_button_addnew": navCentermanagementJourneyButtonAddnew,
    "nav_centermanagement_journey_form_label_status": navCentermanagementJourneyFormLabelStatus,
    "nav_centermanagement_journey_form_label_observationdate": navCentermanagementJourneyFormLabelObservationdate,
    "nav_centermanagement_journey_form_label_interpretation": navCentermanagementJourneyFormLabelInterpretation,
    "nav_stafreflection_form_label_educator": navStafreflectionFormLabelEducator,
    "nav_centermanagement_journey_form_label_followup": navCentermanagementJourneyFormLabelFollowup,
    "nav_centermanagement_journal_pdfs_message_empty": navCentermanagementJournalPdfsMessageEmpty,
    "nav_centermanagement_journey_warning_message": navCentermanagementJourneyWarningMessage,
    "nav_centermanagement_newsletter_report_single_title": navCentermanagementNewsletterReportSingleTitle,
    "nav_centermanagement_journey_form_label_reflection": navCentermanagementJourneyFormLabelReflection,
    "nav_centermanagement_journey_form_label_practice": navCentermanagementJourneyFormLabelPractice,
    "nav_centermanagement_last_modified_by": navCentermanagementLastModifiedBy,
    "nav_centermanagement_created_by": navCentermanagementCreatedBy,
    "nav_centermanagement_journey_form_label_learningtags": navCentermanagementJourneyFormLabelLearningtags,
    "nav_centermanagement_journey_form_label_principles": navCentermanagementJourneyFormLabelPrinciples,
    "nav_stafreflection_form_label_learning_outcomes": navStafreflectionFormLabelLearningOutcomes,
    "nav_centermanagement_journey_draft_title": navCentermanagementJourneyDraftTitle,
    "nav_centermanagement_journey_form_label_new_learningtags": navCentermanagementJourneyFormLabelNewLearningtags,
    "nav_centermanagement_journey_label_filter": navCentermanagementJourneyLabelFilter,
    "nav_centermanagement_journey_form_label_followup_images": navCentermanagementJourneyFormLabelFollowupImages,
    "nav_centermanagement_journey_images_message_empty": navCentermanagementJourneyImagesMessageEmpty,
    "nav_centermanagement_journey_form_label_observation": navCentermanagementJourneyFormLabelObservation,
    "nav_centermanagement_journey_index_title": navCentermanagementJourneyIndexTitle,
    "nav_stafreflection_form_label_status": navStafreflectionFormLabelStatus,
    "nav_centermanagement_journey_report_single_title": navCentermanagementJourneyReportSingleTitle,
  };
}

class CenterManagementRoomSection {
  String navCentermanagementRoomSleepEnabled;
  String navCentermanagementRoomButtonAddnew;
  String navCentermanagementRoomIndexTitle;
  String navCentermanagementRoomIndexTableSleep;
  String navCentermanagementRoomFormLabelStaff;
  String navCentermanagementRoomLabel;

  CenterManagementRoomSection({
    this.navCentermanagementRoomSleepEnabled,
    this.navCentermanagementRoomButtonAddnew,
    this.navCentermanagementRoomIndexTitle,
    this.navCentermanagementRoomIndexTableSleep,
    this.navCentermanagementRoomFormLabelStaff,
    this.navCentermanagementRoomLabel,
  });

  factory CenterManagementRoomSection.fromJson(Map<String, dynamic> json) => new CenterManagementRoomSection(
    navCentermanagementRoomSleepEnabled: json["nav_centermanagement_room_sleep_enabled"],
    navCentermanagementRoomButtonAddnew: json["nav_centermanagement_room_button_addnew"],
    navCentermanagementRoomIndexTitle: json["nav_centermanagement_room_index_title"],
    navCentermanagementRoomIndexTableSleep: json["nav_centermanagement_room_index_table_sleep"],
    navCentermanagementRoomFormLabelStaff: json["nav_centermanagement_room_form_label_staff"],
    navCentermanagementRoomLabel: json["nav_centermanagement_room_label"],
  );

  Map<String, dynamic> toJson() => {
    "nav_centermanagement_room_sleep_enabled": navCentermanagementRoomSleepEnabled,
    "nav_centermanagement_room_button_addnew": navCentermanagementRoomButtonAddnew,
    "nav_centermanagement_room_index_title": navCentermanagementRoomIndexTitle,
    "nav_centermanagement_room_index_table_sleep": navCentermanagementRoomIndexTableSleep,
    "nav_centermanagement_room_form_label_staff": navCentermanagementRoomFormLabelStaff,
    "nav_centermanagement_room_label": navCentermanagementRoomLabel,
  };
}

class CenterManagementStorySection {
  String storyWidgetPublishEditWarningMessage;
  String navLearningsetButtonAddText;
  String navLearningsetButtonAddVideoNewTitle;
  String navCentermanagementStoryWidgetDrawingName;
  String navLearningsetButtonAddPdf;
  String navCentermanagementStoryWarningMessage;
  String navCentermanagementStoryBgSettingTitle;
  String navCentermanagementStoryWidgetImageName;
  String navCentermanagementStoryFormLabelWhathappened;
  String navCentermanagementStoryFormLabelObservationdate;
  String navCentermanagementStoryWidgetAudioName;
  String navCentermanagementStoryWidgetTitleName;
  String navCentermanagementStoryFormLabelFollowup;
  String navLearningsetButtonAddImageNewTitle;
  String navCentermanagementStoryWidgetVideoName;
  String navCentermanagementStoryFormLabelStatus;
  String navCentermanagementStoryLabel;
  String navCentermanagementStoryParentViewComment;
  String navUploadfilesIndexTitle;
  String navCentermanagementStoryReportSingleTitle;
  String navCentermanagementStoryFormLabelInterpretation;
  String navCentermanagementStoryContentTitle;
  String navLearningsetButtonAddPdfNewTitle;
  String navCentermanagementStoryFormLabelFollowupImages;
  String navCentermanagementStoryIndexTitle;
  String navCentermanagementStoryParentViewCommentEmptyMessage;
  String navCentermanagementStoryLabelFilter;
  String navCentermanagementStoryParentViewCommentModalTitle;
  String navCentermanagementStoryFormLabelStory;
  String navCentermanagementStoryFormLabelFollowupdate;
  String navCentermanagementStoryFormLabelLearningtagsFollowup;
  String navCentermanagementStoryWidgetPhotoName;
  String navCentermanagementStoryButtonAddnew;
  String navCentermanagementStoryBgNewTitle;
  String navLearningsetButtonAddImage;
  String navCentermanagementStoryWidgetParaName;
  String navCentermanagementStoryBgEditTitle;
  String navLearningsetButtonAddVideo;
  String navCentermanagementStoryFormLabelStaff;
  String storyWidgetPublishTagImageNoChildMessage;
  String navCentermanagementStoryParentViewCommentButton;
  String navCentermanagementStoryLabelFollowup;
  String navCentermanagementStoryFormLabelObserver;
  String navCentermanagementStoryLabelDraft;
  String navCentermanagementStoryButtonAddnewDraft;
  String navCentermanagementStoryWidgetLinkName;
  String navCentermanagementStoryWidgetTitle;
  String navCentermanagementStoryWidgetPdfDocName;
  String navCentermanagementStoryDraftTitle;
  String navCentermanagementStoryImagesMessageEmpty;

  CenterManagementStorySection({
    this.storyWidgetPublishEditWarningMessage,
    this.navLearningsetButtonAddText,
    this.navLearningsetButtonAddVideoNewTitle,
    this.navCentermanagementStoryWidgetDrawingName,
    this.navLearningsetButtonAddPdf,
    this.navCentermanagementStoryWarningMessage,
    this.navCentermanagementStoryBgSettingTitle,
    this.navCentermanagementStoryWidgetImageName,
    this.navCentermanagementStoryFormLabelWhathappened,
    this.navCentermanagementStoryFormLabelObservationdate,
    this.navCentermanagementStoryWidgetAudioName,
    this.navCentermanagementStoryWidgetTitleName,
    this.navCentermanagementStoryFormLabelFollowup,
    this.navLearningsetButtonAddImageNewTitle,
    this.navCentermanagementStoryWidgetVideoName,
    this.navCentermanagementStoryFormLabelStatus,
    this.navCentermanagementStoryLabel,
    this.navCentermanagementStoryParentViewComment,
    this.navUploadfilesIndexTitle,
    this.navCentermanagementStoryReportSingleTitle,
    this.navCentermanagementStoryFormLabelInterpretation,
    this.navCentermanagementStoryContentTitle,
    this.navLearningsetButtonAddPdfNewTitle,
    this.navCentermanagementStoryFormLabelFollowupImages,
    this.navCentermanagementStoryIndexTitle,
    this.navCentermanagementStoryParentViewCommentEmptyMessage,
    this.navCentermanagementStoryLabelFilter,
    this.navCentermanagementStoryParentViewCommentModalTitle,
    this.navCentermanagementStoryFormLabelStory,
    this.navCentermanagementStoryFormLabelFollowupdate,
    this.navCentermanagementStoryFormLabelLearningtagsFollowup,
    this.navCentermanagementStoryWidgetPhotoName,
    this.navCentermanagementStoryButtonAddnew,
    this.navCentermanagementStoryBgNewTitle,
    this.navLearningsetButtonAddImage,
    this.navCentermanagementStoryWidgetParaName,
    this.navCentermanagementStoryBgEditTitle,
    this.navLearningsetButtonAddVideo,
    this.navCentermanagementStoryFormLabelStaff,
    this.storyWidgetPublishTagImageNoChildMessage,
    this.navCentermanagementStoryParentViewCommentButton,
    this.navCentermanagementStoryLabelFollowup,
    this.navCentermanagementStoryFormLabelObserver,
    this.navCentermanagementStoryLabelDraft,
    this.navCentermanagementStoryButtonAddnewDraft,
    this.navCentermanagementStoryWidgetLinkName,
    this.navCentermanagementStoryWidgetTitle,
    this.navCentermanagementStoryWidgetPdfDocName,
    this.navCentermanagementStoryDraftTitle,
    this.navCentermanagementStoryImagesMessageEmpty,
  });

  factory CenterManagementStorySection.fromJson(Map<String, dynamic> json) => new CenterManagementStorySection(
    storyWidgetPublishEditWarningMessage: json["story_widget_publish_edit_warning_message"],
    navLearningsetButtonAddText: json["nav_learningset_button_add_text"],
    navLearningsetButtonAddVideoNewTitle: json["nav_learningset_button_add_video_new_title"],
    navCentermanagementStoryWidgetDrawingName: json["nav_centermanagement_story_widget_drawing_name"],
    navLearningsetButtonAddPdf: json["nav_learningset_button_add_pdf"],
    navCentermanagementStoryWarningMessage: json["nav_centermanagement_story_warning_message"],
    navCentermanagementStoryBgSettingTitle: json["nav_centermanagement_story_bg_setting_title"],
    navCentermanagementStoryWidgetImageName: json["nav_centermanagement_story_widget_image_name"],
    navCentermanagementStoryFormLabelWhathappened: json["nav_centermanagement_story_form_label_whathappened"],
    navCentermanagementStoryFormLabelObservationdate: json["nav_centermanagement_story_form_label_observationdate"],
    navCentermanagementStoryWidgetAudioName: json["nav_centermanagement_story_widget_audio_name"],
    navCentermanagementStoryWidgetTitleName: json["nav_centermanagement_story_widget_title_name"],
    navCentermanagementStoryFormLabelFollowup: json["nav_centermanagement_story_form_label_followup"],
    navLearningsetButtonAddImageNewTitle: json["nav_learningset_button_add_image_new_title"],
    navCentermanagementStoryWidgetVideoName: json["nav_centermanagement_story_widget_video_name"],
    navCentermanagementStoryFormLabelStatus: json["nav_centermanagement_story_form_label_status"],
    navCentermanagementStoryLabel: json["nav_centermanagement_story_label"],
    navCentermanagementStoryParentViewComment: json["nav_centermanagement_story_parent_view_comment"],
    navUploadfilesIndexTitle: json["nav_uploadfiles_index_title"],
    navCentermanagementStoryReportSingleTitle: json["nav_centermanagement_story_report_single_title"],
    navCentermanagementStoryFormLabelInterpretation: json["nav_centermanagement_story_form_label_interpretation"],
    navCentermanagementStoryContentTitle: json["nav_centermanagement_story_content_title"],
    navLearningsetButtonAddPdfNewTitle: json["nav_learningset_button_add_pdf_new_title"],
    navCentermanagementStoryFormLabelFollowupImages: json["nav_centermanagement_story_form_label_followup_images"],
    navCentermanagementStoryIndexTitle: json["nav_centermanagement_story_index_title"],
    navCentermanagementStoryParentViewCommentEmptyMessage: json["nav_centermanagement_story_parent_view_comment_empty_message"],
    navCentermanagementStoryLabelFilter: json["nav_centermanagement_story_label_filter"],
    navCentermanagementStoryParentViewCommentModalTitle: json["nav_centermanagement_story_parent_view_comment_modal_title"],
    navCentermanagementStoryFormLabelStory: json["nav_centermanagement_story_form_label_story"],
    navCentermanagementStoryFormLabelFollowupdate: json["nav_centermanagement_story_form_label_followupdate"],
    navCentermanagementStoryFormLabelLearningtagsFollowup: json["nav_centermanagement_story_form_label_learningtags_followup"],
    navCentermanagementStoryWidgetPhotoName: json["nav_centermanagement_story_widget_photo_name"],
    navCentermanagementStoryButtonAddnew: json["nav_centermanagement_story_button_addnew"],
    navCentermanagementStoryBgNewTitle: json["nav_centermanagement_story_bg_new_title"],
    navLearningsetButtonAddImage: json["nav_learningset_button_add_image"],
    navCentermanagementStoryWidgetParaName: json["nav_centermanagement_story_widget_para_name"],
    navCentermanagementStoryBgEditTitle: json["nav_centermanagement_story_bg_edit_title"],
    navLearningsetButtonAddVideo: json["nav_learningset_button_add_video"],
    navCentermanagementStoryFormLabelStaff: json["nav_centermanagement_story_form_label_staff"],
    storyWidgetPublishTagImageNoChildMessage: json["story_widget_publish_tag_image_no_child_message"],
    navCentermanagementStoryParentViewCommentButton: json["nav_centermanagement_story_parent_view_comment_button"],
    navCentermanagementStoryLabelFollowup: json["nav_centermanagement_story_label_followup"],
    navCentermanagementStoryFormLabelObserver: json["nav_centermanagement_story_form_label_observer"],
    navCentermanagementStoryLabelDraft: json["nav_centermanagement_story_label_draft"],
    navCentermanagementStoryButtonAddnewDraft: json["nav_centermanagement_story_button_addnew_draft"],
    navCentermanagementStoryWidgetLinkName: json["nav_centermanagement_story_widget_link_name"],
    navCentermanagementStoryWidgetTitle: json["nav_centermanagement_story_widget_title"],
    navCentermanagementStoryWidgetPdfDocName: json["nav_centermanagement_story_widget_pdf_doc_name"],
    navCentermanagementStoryDraftTitle: json["nav_centermanagement_story_draft_title"],
    navCentermanagementStoryImagesMessageEmpty: json["nav_centermanagement_story_images_message_empty"],
  );

  Map<String, dynamic> toJson() => {
    "story_widget_publish_edit_warning_message": storyWidgetPublishEditWarningMessage,
    "nav_learningset_button_add_text": navLearningsetButtonAddText,
    "nav_learningset_button_add_video_new_title": navLearningsetButtonAddVideoNewTitle,
    "nav_centermanagement_story_widget_drawing_name": navCentermanagementStoryWidgetDrawingName,
    "nav_learningset_button_add_pdf": navLearningsetButtonAddPdf,
    "nav_centermanagement_story_warning_message": navCentermanagementStoryWarningMessage,
    "nav_centermanagement_story_bg_setting_title": navCentermanagementStoryBgSettingTitle,
    "nav_centermanagement_story_widget_image_name": navCentermanagementStoryWidgetImageName,
    "nav_centermanagement_story_form_label_whathappened": navCentermanagementStoryFormLabelWhathappened,
    "nav_centermanagement_story_form_label_observationdate": navCentermanagementStoryFormLabelObservationdate,
    "nav_centermanagement_story_widget_audio_name": navCentermanagementStoryWidgetAudioName,
    "nav_centermanagement_story_widget_title_name": navCentermanagementStoryWidgetTitleName,
    "nav_centermanagement_story_form_label_followup": navCentermanagementStoryFormLabelFollowup,
    "nav_learningset_button_add_image_new_title": navLearningsetButtonAddImageNewTitle,
    "nav_centermanagement_story_widget_video_name": navCentermanagementStoryWidgetVideoName,
    "nav_centermanagement_story_form_label_status": navCentermanagementStoryFormLabelStatus,
    "nav_centermanagement_story_label": navCentermanagementStoryLabel,
    "nav_centermanagement_story_parent_view_comment": navCentermanagementStoryParentViewComment,
    "nav_uploadfiles_index_title": navUploadfilesIndexTitle,
    "nav_centermanagement_story_report_single_title": navCentermanagementStoryReportSingleTitle,
    "nav_centermanagement_story_form_label_interpretation": navCentermanagementStoryFormLabelInterpretation,
    "nav_centermanagement_story_content_title": navCentermanagementStoryContentTitle,
    "nav_learningset_button_add_pdf_new_title": navLearningsetButtonAddPdfNewTitle,
    "nav_centermanagement_story_form_label_followup_images": navCentermanagementStoryFormLabelFollowupImages,
    "nav_centermanagement_story_index_title": navCentermanagementStoryIndexTitle,
    "nav_centermanagement_story_parent_view_comment_empty_message": navCentermanagementStoryParentViewCommentEmptyMessage,
    "nav_centermanagement_story_label_filter": navCentermanagementStoryLabelFilter,
    "nav_centermanagement_story_parent_view_comment_modal_title": navCentermanagementStoryParentViewCommentModalTitle,
    "nav_centermanagement_story_form_label_story": navCentermanagementStoryFormLabelStory,
    "nav_centermanagement_story_form_label_followupdate": navCentermanagementStoryFormLabelFollowupdate,
    "nav_centermanagement_story_form_label_learningtags_followup": navCentermanagementStoryFormLabelLearningtagsFollowup,
    "nav_centermanagement_story_widget_photo_name": navCentermanagementStoryWidgetPhotoName,
    "nav_centermanagement_story_button_addnew": navCentermanagementStoryButtonAddnew,
    "nav_centermanagement_story_bg_new_title": navCentermanagementStoryBgNewTitle,
    "nav_learningset_button_add_image": navLearningsetButtonAddImage,
    "nav_centermanagement_story_widget_para_name": navCentermanagementStoryWidgetParaName,
    "nav_centermanagement_story_bg_edit_title": navCentermanagementStoryBgEditTitle,
    "nav_learningset_button_add_video": navLearningsetButtonAddVideo,
    "nav_centermanagement_story_form_label_staff": navCentermanagementStoryFormLabelStaff,
    "story_widget_publish_tag_image_no_child_message": storyWidgetPublishTagImageNoChildMessage,
    "nav_centermanagement_story_parent_view_comment_button": navCentermanagementStoryParentViewCommentButton,
    "nav_centermanagement_story_label_followup": navCentermanagementStoryLabelFollowup,
    "nav_centermanagement_story_form_label_observer": navCentermanagementStoryFormLabelObserver,
    "nav_centermanagement_story_label_draft": navCentermanagementStoryLabelDraft,
    "nav_centermanagement_story_button_addnew_draft": navCentermanagementStoryButtonAddnewDraft,
    "nav_centermanagement_story_widget_link_name": navCentermanagementStoryWidgetLinkName,
    "nav_centermanagement_story_widget_title": navCentermanagementStoryWidgetTitle,
    "nav_centermanagement_story_widget_pdf_doc_name": navCentermanagementStoryWidgetPdfDocName,
    "nav_centermanagement_story_draft_title": navCentermanagementStoryDraftTitle,
    "nav_centermanagement_story_images_message_empty": navCentermanagementStoryImagesMessageEmpty,
  };
}

class CentreProfile {
  String navCentresettingsEmailTemplateLabel;
  String navCentresettingsBusinessInfoTabLabel;
  String navCentresettingsIndexTitle;
  String navCentresettingsPrintlayoutTabLabel;
  String navCentresettingsEmailTemplateTabLabel;
  String navCentresettingsLabel;
  String navCentresettingsEmailTemplateResetButtonText;
  String navCentresettingsBusinessCoverImageTabLabel;
  String navCentresettingsBusinessLogoTabLabel;
  String navCentresettingsEmailSignatureTabLabel;

  CentreProfile({
    this.navCentresettingsEmailTemplateLabel,
    this.navCentresettingsBusinessInfoTabLabel,
    this.navCentresettingsIndexTitle,
    this.navCentresettingsPrintlayoutTabLabel,
    this.navCentresettingsEmailTemplateTabLabel,
    this.navCentresettingsLabel,
    this.navCentresettingsEmailTemplateResetButtonText,
    this.navCentresettingsBusinessCoverImageTabLabel,
    this.navCentresettingsBusinessLogoTabLabel,
    this.navCentresettingsEmailSignatureTabLabel,
  });

  factory CentreProfile.fromJson(Map<String, dynamic> json) => new CentreProfile(
    navCentresettingsEmailTemplateLabel: json["nav_centresettings_email_template_label"],
    navCentresettingsBusinessInfoTabLabel: json["nav_centresettings_business_info_tab_label"],
    navCentresettingsIndexTitle: json["nav_centresettings_index_title"],
    navCentresettingsPrintlayoutTabLabel: json["nav_centresettings_printlayout_tab_label"],
    navCentresettingsEmailTemplateTabLabel: json["nav_centresettings_email_template_tab_label"],
    navCentresettingsLabel: json["nav_centresettings_label"],
    navCentresettingsEmailTemplateResetButtonText: json["nav_centresettings_email_template_reset_button_text"],
    navCentresettingsBusinessCoverImageTabLabel: json["nav_centresettings_business_cover_image_tab_label"],
    navCentresettingsBusinessLogoTabLabel: json["nav_centresettings_business_logo_tab_label"],
    navCentresettingsEmailSignatureTabLabel: json["nav_centresettings_email_signature_tab_label"],
  );

  Map<String, dynamic> toJson() => {
    "nav_centresettings_email_template_label": navCentresettingsEmailTemplateLabel,
    "nav_centresettings_business_info_tab_label": navCentresettingsBusinessInfoTabLabel,
    "nav_centresettings_index_title": navCentresettingsIndexTitle,
    "nav_centresettings_printlayout_tab_label": navCentresettingsPrintlayoutTabLabel,
    "nav_centresettings_email_template_tab_label": navCentresettingsEmailTemplateTabLabel,
    "nav_centresettings_label": navCentresettingsLabel,
    "nav_centresettings_email_template_reset_button_text": navCentresettingsEmailTemplateResetButtonText,
    "nav_centresettings_business_cover_image_tab_label": navCentresettingsBusinessCoverImageTabLabel,
    "nav_centresettings_business_logo_tab_label": navCentresettingsBusinessLogoTabLabel,
    "nav_centresettings_email_signature_tab_label": navCentresettingsEmailSignatureTabLabel,
  };
}

class ChildIncidentSection {
  String navFormIncidentLabelPersonCompletingRole;
  String navFormIncidentLabelParentSignature;
  String navFormIncidentSectionHeadingAdditionalNotes;
  String injuries;
  String navFormIncidentLabelStaffSignature;
  String navFormIncidentLabelChildPicked;
  String navFormIncidentLabelCauseOfInjury;
  String navFormIncidentLabelProductStructure;
  String navFormIncidentLabelCircumstancesAppearedTaken;
  String navFormIncidentLabelParentInformedComment;
  String navDailyJourneyComment;
  String navFormIncidentLabelGeneralActivity;
  String navFormIncidentLabelPersonCompletingSignature;
  String navFormIncidentSectionHeadingParentAcknowledge;
  String navFormIncidentLabelParentInformed;
  String navFormIncidentLabelCircumstancesAppearedPlaceholder;
  String navFormIncidentLabelParentAcknoledgeSignature;
  String navFormIncidentLabelDetailsMedicalServiceName;
  String navFormIncidentLabelIncidentTime;
  String navFormIncidentSectionHeadingNominatedSupervisor;
  String navFormIncidentLabelOtherAgcSignature;
  String navFormIncidentLabelCircumstances;
  String navFormIncidentLabelNominatedSupervisorSignature;
  String navFormIncidentSectionHeadingNotifications;
  String navFormIncidentLabelIncidentWitnessSignature;
  String navFormIncidentLabelReportToDec;
  String navFormIncidentSectionHeadingIncidentDetails;
  String navFormIncidentLabelCircumstancesAppearedTakenPlaveholder;
  String navFormIncidentLabelDetailsMedicalServiceContacted;
  String navFormIncidentLabelIncidentLocation;
  String navFormIncidentSectionHeadingActionTaken;
  String navFormIncidentLabelParentAcknoledge;
  String navFormIncidentLabelDetailsActionTaken;
  String navFormIncidentLabelFirstaidProvide;
  String navFormIncidentLabelRegulatoryAuthoritySignature;
  String navFormIncidentLabelChildPickedByParent;
  String navFormIncidentLabelCircumstancesAppeared;
  String navFormIncidentLabelMedicationAdministered;
  String navFormIncidentLabelPersonCompletingForm;
  String navFormIncidentLabelAdditionalNotes;
  String navFormIncidentLabelDetailsMedicalServiceContactedDate;
  String navFormIncidentLabelStaffOnduty;
  String navFormIncidentLabelTimeRecodeMade;
  String navFormIncidentLabelChildPickedByOther;
  String navFormIncidentLabelParentInformedDate;
  String navFormIncidentLabelChildPickedTime;
  String navFormIncidentLabelParentAcknoledgeText;
  String navFormIncidentLabelManagerSignature;
  String navFormIncidentLabelDetailsActionTakenPlaceholder;
  String navFormIncidentLabelDateRecodeMade;
  String navFormIncidentLabelDetailsMedicalServiceContactedTime;
  String navFormIncidentLabelDetailsMedicalServiceDetails;
  String navFormIncidentLabelDetailsStepsTakenPrevent;
  String navFormIncidentSectionHeadingPersonCompleting;
  String navFormIncidentLabelInjuryPlaces;
  String navFormIncidentLabelParentInformedTime;
  String navFormIncidentLabelNatureInjure;
  String navFormIncidentLabelParentInformedBy;
  String navFormIncidentLabelIncidentNameWitness;
  String navFormIncidentLabelDetailsMedicalServiceNature;
  String navFormIncidentLabelIncidentDate;
  String navFormIncidentLabelParentContactedImmediately;

  ChildIncidentSection({
    this.navFormIncidentLabelPersonCompletingRole,
    this.navFormIncidentLabelParentSignature,
    this.navFormIncidentSectionHeadingAdditionalNotes,
    this.injuries,
    this.navFormIncidentLabelStaffSignature,
    this.navFormIncidentLabelChildPicked,
    this.navFormIncidentLabelCauseOfInjury,
    this.navFormIncidentLabelProductStructure,
    this.navFormIncidentLabelCircumstancesAppearedTaken,
    this.navFormIncidentLabelParentInformedComment,
    this.navDailyJourneyComment,
    this.navFormIncidentLabelGeneralActivity,
    this.navFormIncidentLabelPersonCompletingSignature,
    this.navFormIncidentSectionHeadingParentAcknowledge,
    this.navFormIncidentLabelParentInformed,
    this.navFormIncidentLabelCircumstancesAppearedPlaceholder,
    this.navFormIncidentLabelParentAcknoledgeSignature,
    this.navFormIncidentLabelDetailsMedicalServiceName,
    this.navFormIncidentLabelIncidentTime,
    this.navFormIncidentSectionHeadingNominatedSupervisor,
    this.navFormIncidentLabelOtherAgcSignature,
    this.navFormIncidentLabelCircumstances,
    this.navFormIncidentLabelNominatedSupervisorSignature,
    this.navFormIncidentSectionHeadingNotifications,
    this.navFormIncidentLabelIncidentWitnessSignature,
    this.navFormIncidentLabelReportToDec,
    this.navFormIncidentSectionHeadingIncidentDetails,
    this.navFormIncidentLabelCircumstancesAppearedTakenPlaveholder,
    this.navFormIncidentLabelDetailsMedicalServiceContacted,
    this.navFormIncidentLabelIncidentLocation,
    this.navFormIncidentSectionHeadingActionTaken,
    this.navFormIncidentLabelParentAcknoledge,
    this.navFormIncidentLabelDetailsActionTaken,
    this.navFormIncidentLabelFirstaidProvide,
    this.navFormIncidentLabelRegulatoryAuthoritySignature,
    this.navFormIncidentLabelChildPickedByParent,
    this.navFormIncidentLabelCircumstancesAppeared,
    this.navFormIncidentLabelMedicationAdministered,
    this.navFormIncidentLabelPersonCompletingForm,
    this.navFormIncidentLabelAdditionalNotes,
    this.navFormIncidentLabelDetailsMedicalServiceContactedDate,
    this.navFormIncidentLabelStaffOnduty,
    this.navFormIncidentLabelTimeRecodeMade,
    this.navFormIncidentLabelChildPickedByOther,
    this.navFormIncidentLabelParentInformedDate,
    this.navFormIncidentLabelChildPickedTime,
    this.navFormIncidentLabelParentAcknoledgeText,
    this.navFormIncidentLabelManagerSignature,
    this.navFormIncidentLabelDetailsActionTakenPlaceholder,
    this.navFormIncidentLabelDateRecodeMade,
    this.navFormIncidentLabelDetailsMedicalServiceContactedTime,
    this.navFormIncidentLabelDetailsMedicalServiceDetails,
    this.navFormIncidentLabelDetailsStepsTakenPrevent,
    this.navFormIncidentSectionHeadingPersonCompleting,
    this.navFormIncidentLabelInjuryPlaces,
    this.navFormIncidentLabelParentInformedTime,
    this.navFormIncidentLabelNatureInjure,
    this.navFormIncidentLabelParentInformedBy,
    this.navFormIncidentLabelIncidentNameWitness,
    this.navFormIncidentLabelDetailsMedicalServiceNature,
    this.navFormIncidentLabelIncidentDate,
    this.navFormIncidentLabelParentContactedImmediately,
  });

  factory ChildIncidentSection.fromJson(Map<String, dynamic> json) => new ChildIncidentSection(
    navFormIncidentLabelPersonCompletingRole: json["nav_form_incident_label_person_completing_role"],
    navFormIncidentLabelParentSignature: json["nav_form_incident_label_parent_signature"],
    navFormIncidentSectionHeadingAdditionalNotes: json["nav_form_incident_section_heading_additional_notes"],
    injuries: json["injuries"],
    navFormIncidentLabelStaffSignature: json["nav_form_incident_label_staff_signature"],
    navFormIncidentLabelChildPicked: json["nav_form_incident_label_child_picked"],
    navFormIncidentLabelCauseOfInjury: json["nav_form_incident_label_cause_of_injury"],
    navFormIncidentLabelProductStructure: json["nav_form_incident_label_product_structure"],
    navFormIncidentLabelCircumstancesAppearedTaken: json["nav_form_incident_label_circumstances_appeared_taken"],
    navFormIncidentLabelParentInformedComment: json["nav_form_incident_label_parent_informed_comment"],
    navDailyJourneyComment: json["nav_daily_journey_comment"],
    navFormIncidentLabelGeneralActivity: json["nav_form_incident_label_general_activity"],
    navFormIncidentLabelPersonCompletingSignature: json["nav_form_incident_label_person_completing_signature"],
    navFormIncidentSectionHeadingParentAcknowledge: json["nav_form_incident_section_heading_parent_acknowledge"],
    navFormIncidentLabelParentInformed: json["nav_form_incident_label_parent_informed"],
    navFormIncidentLabelCircumstancesAppearedPlaceholder: json["nav_form_incident_label_circumstances_appeared_placeholder"],
    navFormIncidentLabelParentAcknoledgeSignature: json["nav_form_incident_label_parent_acknoledge_signature"],
    navFormIncidentLabelDetailsMedicalServiceName: json["nav_form_incident_label_details_medical_service_name"],
    navFormIncidentLabelIncidentTime: json["nav_form_incident_label_incident_time"],
    navFormIncidentSectionHeadingNominatedSupervisor: json["nav_form_incident_section_heading_nominated_supervisor"],
    navFormIncidentLabelOtherAgcSignature: json["nav_form_incident_label_other_agc_signature"],
    navFormIncidentLabelCircumstances: json["nav_form_incident_label_circumstances"],
    navFormIncidentLabelNominatedSupervisorSignature: json["nav_form_incident_label_nominated_supervisor_signature"],
    navFormIncidentSectionHeadingNotifications: json["nav_form_incident_section_heading_notifications"],
    navFormIncidentLabelIncidentWitnessSignature: json["nav_form_incident_label_incident_witness_signature"],
    navFormIncidentLabelReportToDec: json["nav_form_incident_label_report_to_dec"],
    navFormIncidentSectionHeadingIncidentDetails: json["nav_form_incident_section_heading_incident_details"],
    navFormIncidentLabelCircumstancesAppearedTakenPlaveholder: json["nav_form_incident_label_circumstances_appeared_taken_plaveholder"],
    navFormIncidentLabelDetailsMedicalServiceContacted: json["nav_form_incident_label_details_medical_service_contacted"],
    navFormIncidentLabelIncidentLocation: json["nav_form_incident_label_incident_location"],
    navFormIncidentSectionHeadingActionTaken: json["nav_form_incident_section_heading_action_taken"],
    navFormIncidentLabelParentAcknoledge: json["nav_form_incident_label_parent_acknoledge"],
    navFormIncidentLabelDetailsActionTaken: json["nav_form_incident_label_details_action_taken"],
    navFormIncidentLabelFirstaidProvide: json["nav_form_incident_label_firstaid_provide"],
    navFormIncidentLabelRegulatoryAuthoritySignature: json["nav_form_incident_label_regulatory_authority_signature"],
    navFormIncidentLabelChildPickedByParent: json["nav_form_incident_label_child_picked_by_parent"],
    navFormIncidentLabelCircumstancesAppeared: json["nav_form_incident_label_circumstances_appeared"],
    navFormIncidentLabelMedicationAdministered: json["nav_form_incident_label_medication_administered"],
    navFormIncidentLabelPersonCompletingForm: json["nav_form_incident_label_person_completing_form"],
    navFormIncidentLabelAdditionalNotes: json["nav_form_incident_label_additional_notes"],
    navFormIncidentLabelDetailsMedicalServiceContactedDate: json["nav_form_incident_label_details_medical_service_contacted_date"],
    navFormIncidentLabelStaffOnduty: json["nav_form_incident_label_staff_onduty"],
    navFormIncidentLabelTimeRecodeMade: json["nav_form_incident_label_time_recode_made"],
    navFormIncidentLabelChildPickedByOther: json["nav_form_incident_label_child_picked_by_other"],
    navFormIncidentLabelParentInformedDate: json["nav_form_incident_label_parent_informed_date"],
    navFormIncidentLabelChildPickedTime: json["nav_form_incident_label_child_picked_time"],
    navFormIncidentLabelParentAcknoledgeText: json["nav_form_incident_label_parent_acknoledge_text"],
    navFormIncidentLabelManagerSignature: json["nav_form_incident_label_manager_signature"],
    navFormIncidentLabelDetailsActionTakenPlaceholder: json["nav_form_incident_label_details_action_taken_placeholder"],
    navFormIncidentLabelDateRecodeMade: json["nav_form_incident_label_date_recode_made"],
    navFormIncidentLabelDetailsMedicalServiceContactedTime: json["nav_form_incident_label_details_medical_service_contacted_time"],
    navFormIncidentLabelDetailsMedicalServiceDetails: json["nav_form_incident_label_details_medical_service_details"],
    navFormIncidentLabelDetailsStepsTakenPrevent: json["nav_form_incident_label_details_steps_taken_prevent"],
    navFormIncidentSectionHeadingPersonCompleting: json["nav_form_incident_section_heading_person_completing"],
    navFormIncidentLabelInjuryPlaces: json["nav_form_incident_label_injury_places"],
    navFormIncidentLabelParentInformedTime: json["nav_form_incident_label_parent_informed_time"],
    navFormIncidentLabelNatureInjure: json["nav_form_incident_label_nature_injure"],
    navFormIncidentLabelParentInformedBy: json["nav_form_incident_label_parent_informed_by"],
    navFormIncidentLabelIncidentNameWitness: json["nav_form_incident_label_incident_name_witness"],
    navFormIncidentLabelDetailsMedicalServiceNature: json["nav_form_incident_label_details_medical_service_nature"],
    navFormIncidentLabelIncidentDate: json["nav_form_incident_label_incident_date"],
    navFormIncidentLabelParentContactedImmediately: json["nav_form_incident_label_parent_contacted_immediately"],
  );

  Map<String, dynamic> toJson() => {
    "nav_form_incident_label_person_completing_role": navFormIncidentLabelPersonCompletingRole,
    "nav_form_incident_label_parent_signature": navFormIncidentLabelParentSignature,
    "nav_form_incident_section_heading_additional_notes": navFormIncidentSectionHeadingAdditionalNotes,
    "injuries": injuries,
    "nav_form_incident_label_staff_signature": navFormIncidentLabelStaffSignature,
    "nav_form_incident_label_child_picked": navFormIncidentLabelChildPicked,
    "nav_form_incident_label_cause_of_injury": navFormIncidentLabelCauseOfInjury,
    "nav_form_incident_label_product_structure": navFormIncidentLabelProductStructure,
    "nav_form_incident_label_circumstances_appeared_taken": navFormIncidentLabelCircumstancesAppearedTaken,
    "nav_form_incident_label_parent_informed_comment": navFormIncidentLabelParentInformedComment,
    "nav_daily_journey_comment": navDailyJourneyComment,
    "nav_form_incident_label_general_activity": navFormIncidentLabelGeneralActivity,
    "nav_form_incident_label_person_completing_signature": navFormIncidentLabelPersonCompletingSignature,
    "nav_form_incident_section_heading_parent_acknowledge": navFormIncidentSectionHeadingParentAcknowledge,
    "nav_form_incident_label_parent_informed": navFormIncidentLabelParentInformed,
    "nav_form_incident_label_circumstances_appeared_placeholder": navFormIncidentLabelCircumstancesAppearedPlaceholder,
    "nav_form_incident_label_parent_acknoledge_signature": navFormIncidentLabelParentAcknoledgeSignature,
    "nav_form_incident_label_details_medical_service_name": navFormIncidentLabelDetailsMedicalServiceName,
    "nav_form_incident_label_incident_time": navFormIncidentLabelIncidentTime,
    "nav_form_incident_section_heading_nominated_supervisor": navFormIncidentSectionHeadingNominatedSupervisor,
    "nav_form_incident_label_other_agc_signature": navFormIncidentLabelOtherAgcSignature,
    "nav_form_incident_label_circumstances": navFormIncidentLabelCircumstances,
    "nav_form_incident_label_nominated_supervisor_signature": navFormIncidentLabelNominatedSupervisorSignature,
    "nav_form_incident_section_heading_notifications": navFormIncidentSectionHeadingNotifications,
    "nav_form_incident_label_incident_witness_signature": navFormIncidentLabelIncidentWitnessSignature,
    "nav_form_incident_label_report_to_dec": navFormIncidentLabelReportToDec,
    "nav_form_incident_section_heading_incident_details": navFormIncidentSectionHeadingIncidentDetails,
    "nav_form_incident_label_circumstances_appeared_taken_plaveholder": navFormIncidentLabelCircumstancesAppearedTakenPlaveholder,
    "nav_form_incident_label_details_medical_service_contacted": navFormIncidentLabelDetailsMedicalServiceContacted,
    "nav_form_incident_label_incident_location": navFormIncidentLabelIncidentLocation,
    "nav_form_incident_section_heading_action_taken": navFormIncidentSectionHeadingActionTaken,
    "nav_form_incident_label_parent_acknoledge": navFormIncidentLabelParentAcknoledge,
    "nav_form_incident_label_details_action_taken": navFormIncidentLabelDetailsActionTaken,
    "nav_form_incident_label_firstaid_provide": navFormIncidentLabelFirstaidProvide,
    "nav_form_incident_label_regulatory_authority_signature": navFormIncidentLabelRegulatoryAuthoritySignature,
    "nav_form_incident_label_child_picked_by_parent": navFormIncidentLabelChildPickedByParent,
    "nav_form_incident_label_circumstances_appeared": navFormIncidentLabelCircumstancesAppeared,
    "nav_form_incident_label_medication_administered": navFormIncidentLabelMedicationAdministered,
    "nav_form_incident_label_person_completing_form": navFormIncidentLabelPersonCompletingForm,
    "nav_form_incident_label_additional_notes": navFormIncidentLabelAdditionalNotes,
    "nav_form_incident_label_details_medical_service_contacted_date": navFormIncidentLabelDetailsMedicalServiceContactedDate,
    "nav_form_incident_label_staff_onduty": navFormIncidentLabelStaffOnduty,
    "nav_form_incident_label_time_recode_made": navFormIncidentLabelTimeRecodeMade,
    "nav_form_incident_label_child_picked_by_other": navFormIncidentLabelChildPickedByOther,
    "nav_form_incident_label_parent_informed_date": navFormIncidentLabelParentInformedDate,
    "nav_form_incident_label_child_picked_time": navFormIncidentLabelChildPickedTime,
    "nav_form_incident_label_parent_acknoledge_text": navFormIncidentLabelParentAcknoledgeText,
    "nav_form_incident_label_manager_signature": navFormIncidentLabelManagerSignature,
    "nav_form_incident_label_details_action_taken_placeholder": navFormIncidentLabelDetailsActionTakenPlaceholder,
    "nav_form_incident_label_date_recode_made": navFormIncidentLabelDateRecodeMade,
    "nav_form_incident_label_details_medical_service_contacted_time": navFormIncidentLabelDetailsMedicalServiceContactedTime,
    "nav_form_incident_label_details_medical_service_details": navFormIncidentLabelDetailsMedicalServiceDetails,
    "nav_form_incident_label_details_steps_taken_prevent": navFormIncidentLabelDetailsStepsTakenPrevent,
    "nav_form_incident_section_heading_person_completing": navFormIncidentSectionHeadingPersonCompleting,
    "nav_form_incident_label_injury_places": navFormIncidentLabelInjuryPlaces,
    "nav_form_incident_label_parent_informed_time": navFormIncidentLabelParentInformedTime,
    "nav_form_incident_label_nature_injure": navFormIncidentLabelNatureInjure,
    "nav_form_incident_label_parent_informed_by": navFormIncidentLabelParentInformedBy,
    "nav_form_incident_label_incident_name_witness": navFormIncidentLabelIncidentNameWitness,
    "nav_form_incident_label_details_medical_service_nature": navFormIncidentLabelDetailsMedicalServiceNature,
    "nav_form_incident_label_incident_date": navFormIncidentLabelIncidentDate,
    "nav_form_incident_label_parent_contacted_immediately": navFormIncidentLabelParentContactedImmediately,
  };
}

class ChildManagementSection {
  String navCentermanagementTodoLabel;
  String navCentermanagementRemarksActivityLog;
  String navCentermanagementProfileLabel;
  String navCentermanagementTodoDescriptionLabel;
  String navCentermanagementMedicationLabel;
  String navDevelopmentalMilestoneFormLabelEducatorAssets;
  String navCentermanagementIncidentTabLabel;
  String navCentermanagementIllnessLabel;
  String navDevelopmentalMilestoneFormLabelObserve;
  String navCentermanagementIncidentLabel;
  String navCentermanagementMedicationTabLabel;
  String navCentermanagementRemarksTabLabelS;
  String navCentermanagementAccidentTabLabel;
  String navCentermanagementDevelopmentalMilestoneLabel;
  String navCentermanagementDevelopmentalMilestoneTabLabel;
  String navCentermanagementTransitionToschoolButtonAdd;
  String navCentermanagementPanadolAdministrationTabLabel;
  String navCentermanagementAsthmaButtonAdd;
  String navDevelopmentalMilestoneFormLabelEducator;
  String navCentermanagementPhotosAlbumJourney;
  String navCentermanagementAccidentButtonAdd;
  String navCentermanagementIncidentButtonAdd;
  String navCentermanagementTransitionToschoolLabel;
  String navCentermanagementPhotosAlbumStory;
  String navCentermanagementMedicationButtonAdd;
  String navCentermanagementPhotosAlbumNewsfeed;
  String navCentermanagementPhotosAlbumJournal;
  String navCentermanagementUploaddocButtonAdd;
  String navCentermanagementTodoButtonAdd;
  String navCentermanagementChildButtonEdit;
  String navCentermanagementIllnessTabLabel;
  String navCentermanagementAccidentLabel;
  String navCentermanagementProfileTabLabel;
  String navDevelopmentalMilestoneFormLabelSkillCategory;
  String navCentermanagementMedicationTabLabelS;
  String navCentermanagementTodoTabLabel;
  String navCentermanagementUploaddocTabLabel;
  String navDevelopmentalMilestoneFormLabelSkill;
  String navCentermanagementAsthmaTabLabel;
  String navCentermanagementPanadolAdministrationLabel;
  String navCentermanagementIllnessButtonAdd;
  String navCentermanagementRemarksTabLabel;
  String navCentermanagementNonprescribedMedicationTabLabel;
  String navDevelopmentalMilestoneFormLabelDate;
  String navDevelopmentalMilestoneFormLabelEylf;
  String navCentermanagementTodoDateofLabel;
  String navCentermanagementUploaddocLabel;
  String navCentermanagementNonprescribedMedicationLabel;
  String navCentermanagementUploaddocActivityLog;
  String navCentermanagementAsthmaLabel;
  String navCentermanagementObservationTabLabel;
  String navDevelopmentalMilestoneFormLabelTitle;
  String navDevelopmentalMilestoneFormLabelExtendedLearning;
  String navCentermanagementMilestoneTabLabel;
  String navDevelopmentalMilestoneFormLabelAgegroup;
  String navCentermanagementTransitionToschoolTabLabel;
  String navCentermanagementTodoTitleLabel;
  String navCentermanagementPhotosTabLabel;
  String navCentermanagementTodoStatusLabel;
  String navCentermanagementPanadolAdministrationButtonAdd;
  String navDevelopmentalMilestoneFormLabelAnswer;

  ChildManagementSection({
    this.navCentermanagementTodoLabel,
    this.navCentermanagementRemarksActivityLog,
    this.navCentermanagementProfileLabel,
    this.navCentermanagementTodoDescriptionLabel,
    this.navCentermanagementMedicationLabel,
    this.navDevelopmentalMilestoneFormLabelEducatorAssets,
    this.navCentermanagementIncidentTabLabel,
    this.navCentermanagementIllnessLabel,
    this.navDevelopmentalMilestoneFormLabelObserve,
    this.navCentermanagementIncidentLabel,
    this.navCentermanagementMedicationTabLabel,
    this.navCentermanagementRemarksTabLabelS,
    this.navCentermanagementAccidentTabLabel,
    this.navCentermanagementDevelopmentalMilestoneLabel,
    this.navCentermanagementDevelopmentalMilestoneTabLabel,
    this.navCentermanagementTransitionToschoolButtonAdd,
    this.navCentermanagementPanadolAdministrationTabLabel,
    this.navCentermanagementAsthmaButtonAdd,
    this.navDevelopmentalMilestoneFormLabelEducator,
    this.navCentermanagementPhotosAlbumJourney,
    this.navCentermanagementAccidentButtonAdd,
    this.navCentermanagementIncidentButtonAdd,
    this.navCentermanagementTransitionToschoolLabel,
    this.navCentermanagementPhotosAlbumStory,
    this.navCentermanagementMedicationButtonAdd,
    this.navCentermanagementPhotosAlbumNewsfeed,
    this.navCentermanagementPhotosAlbumJournal,
    this.navCentermanagementUploaddocButtonAdd,
    this.navCentermanagementTodoButtonAdd,
    this.navCentermanagementChildButtonEdit,
    this.navCentermanagementIllnessTabLabel,
    this.navCentermanagementAccidentLabel,
    this.navCentermanagementProfileTabLabel,
    this.navDevelopmentalMilestoneFormLabelSkillCategory,
    this.navCentermanagementMedicationTabLabelS,
    this.navCentermanagementTodoTabLabel,
    this.navCentermanagementUploaddocTabLabel,
    this.navDevelopmentalMilestoneFormLabelSkill,
    this.navCentermanagementAsthmaTabLabel,
    this.navCentermanagementPanadolAdministrationLabel,
    this.navCentermanagementIllnessButtonAdd,
    this.navCentermanagementRemarksTabLabel,
    this.navCentermanagementNonprescribedMedicationTabLabel,
    this.navDevelopmentalMilestoneFormLabelDate,
    this.navDevelopmentalMilestoneFormLabelEylf,
    this.navCentermanagementTodoDateofLabel,
    this.navCentermanagementUploaddocLabel,
    this.navCentermanagementNonprescribedMedicationLabel,
    this.navCentermanagementUploaddocActivityLog,
    this.navCentermanagementAsthmaLabel,
    this.navCentermanagementObservationTabLabel,
    this.navDevelopmentalMilestoneFormLabelTitle,
    this.navDevelopmentalMilestoneFormLabelExtendedLearning,
    this.navCentermanagementMilestoneTabLabel,
    this.navDevelopmentalMilestoneFormLabelAgegroup,
    this.navCentermanagementTransitionToschoolTabLabel,
    this.navCentermanagementTodoTitleLabel,
    this.navCentermanagementPhotosTabLabel,
    this.navCentermanagementTodoStatusLabel,
    this.navCentermanagementPanadolAdministrationButtonAdd,
    this.navDevelopmentalMilestoneFormLabelAnswer,
  });

  factory ChildManagementSection.fromJson(Map<String, dynamic> json) => new ChildManagementSection(
    navCentermanagementTodoLabel: json["nav_centermanagement_todo_label"],
    navCentermanagementRemarksActivityLog: json["nav_centermanagement_remarks_activity_log"],
    navCentermanagementProfileLabel: json["nav_centermanagement_profile_label"],
    navCentermanagementTodoDescriptionLabel: json["nav_centermanagement_todo_description_label"],
    navCentermanagementMedicationLabel: json["nav_centermanagement_medication_label"],
    navDevelopmentalMilestoneFormLabelEducatorAssets: json["nav_developmental_milestone_form_label_educator_assets"],
    navCentermanagementIncidentTabLabel: json["nav_centermanagement_incident_tab_label"],
    navCentermanagementIllnessLabel: json["nav_centermanagement_illness_label"],
    navDevelopmentalMilestoneFormLabelObserve: json["nav_developmental_milestone_form_label_observe"],
    navCentermanagementIncidentLabel: json["nav_centermanagement_incident_label"],
    navCentermanagementMedicationTabLabel: json["nav_centermanagement_medication_tab_label"],
    navCentermanagementRemarksTabLabelS: json["nav_centermanagement_remarks_tab_label_s"],
    navCentermanagementAccidentTabLabel: json["nav_centermanagement_accident_tab_label"],
    navCentermanagementDevelopmentalMilestoneLabel: json["nav_centermanagement_developmental_milestone_label"],
    navCentermanagementDevelopmentalMilestoneTabLabel: json["nav_centermanagement_developmental_milestone_tab_label"],
    navCentermanagementTransitionToschoolButtonAdd: json["nav_centermanagement_transition_toschool_button_add"],
    navCentermanagementPanadolAdministrationTabLabel: json["nav_centermanagement_panadol_administration_tab_label"],
    navCentermanagementAsthmaButtonAdd: json["nav_centermanagement_asthma_button_add"],
    navDevelopmentalMilestoneFormLabelEducator: json["nav_developmental_milestone_form_label_educator"],
    navCentermanagementPhotosAlbumJourney: json["nav_centermanagement_photos_album_journey"],
    navCentermanagementAccidentButtonAdd: json["nav_centermanagement_accident_button_add"],
    navCentermanagementIncidentButtonAdd: json["nav_centermanagement_incident_button_add"],
    navCentermanagementTransitionToschoolLabel: json["nav_centermanagement_transition_toschool_label"],
    navCentermanagementPhotosAlbumStory: json["nav_centermanagement_photos_album_story"],
    navCentermanagementMedicationButtonAdd: json["nav_centermanagement_medication_button_add"],
    navCentermanagementPhotosAlbumNewsfeed: json["nav_centermanagement_photos_album_newsfeed"],
    navCentermanagementPhotosAlbumJournal: json["nav_centermanagement_photos_album_journal"],
    navCentermanagementUploaddocButtonAdd: json["nav_centermanagement_uploaddoc_button_add"],
    navCentermanagementTodoButtonAdd: json["nav_centermanagement_todo_button_add"],
    navCentermanagementChildButtonEdit: json["nav_centermanagement_child_button_edit"],
    navCentermanagementIllnessTabLabel: json["nav_centermanagement_illness_tab_label"],
    navCentermanagementAccidentLabel: json["nav_centermanagement_accident_label"],
    navCentermanagementProfileTabLabel: json["nav_centermanagement_profile_tab_label"],
    navDevelopmentalMilestoneFormLabelSkillCategory: json["nav_developmental_milestone_form_label_skill_category"],
    navCentermanagementMedicationTabLabelS: json["nav_centermanagement_medication_tab_label_s"],
    navCentermanagementTodoTabLabel: json["nav_centermanagement_todo_tab_label"],
    navCentermanagementUploaddocTabLabel: json["nav_centermanagement_uploaddoc_tab_label"],
    navDevelopmentalMilestoneFormLabelSkill: json["nav_developmental_milestone_form_label_skill"],
    navCentermanagementAsthmaTabLabel: json["nav_centermanagement_asthma_tab_label"],
    navCentermanagementPanadolAdministrationLabel: json["nav_centermanagement_panadol_administration_label"],
    navCentermanagementIllnessButtonAdd: json["nav_centermanagement_illness_button_add"],
    navCentermanagementRemarksTabLabel: json["nav_centermanagement_remarks_tab_label"],
    navCentermanagementNonprescribedMedicationTabLabel: json["nav_centermanagement_nonprescribed_medication_tab_label"],
    navDevelopmentalMilestoneFormLabelDate: json["nav_developmental_milestone_form_label_date"],
    navDevelopmentalMilestoneFormLabelEylf: json["nav_developmental_milestone_form_label_eylf"],
    navCentermanagementTodoDateofLabel: json["nav_centermanagement_todo_dateof_label"],
    navCentermanagementUploaddocLabel: json["nav_centermanagement_uploaddoc_label"],
    navCentermanagementNonprescribedMedicationLabel: json["nav_centermanagement_nonprescribed_medication_label"],
    navCentermanagementUploaddocActivityLog: json["nav_centermanagement_uploaddoc_activity_log"],
    navCentermanagementAsthmaLabel: json["nav_centermanagement_asthma_label"],
    navCentermanagementObservationTabLabel: json["nav_centermanagement_observation_tab_label"],
    navDevelopmentalMilestoneFormLabelTitle: json["nav_developmental_milestone_form_label_title"],
    navDevelopmentalMilestoneFormLabelExtendedLearning: json["nav_developmental_milestone_form_label_extended_learning"],
    navCentermanagementMilestoneTabLabel: json["nav_centermanagement_milestone_tab_label"],
    navDevelopmentalMilestoneFormLabelAgegroup: json["nav_developmental_milestone_form_label_agegroup"],
    navCentermanagementTransitionToschoolTabLabel: json["nav_centermanagement_transition_toschool_tab_label"],
    navCentermanagementTodoTitleLabel: json["nav_centermanagement_todo_title_label"],
    navCentermanagementPhotosTabLabel: json["nav_centermanagement_photos_tab_label"],
    navCentermanagementTodoStatusLabel: json["nav_centermanagement_todo_status_label"],
    navCentermanagementPanadolAdministrationButtonAdd: json["nav_centermanagement_panadol_administration_button_add"],
    navDevelopmentalMilestoneFormLabelAnswer: json["nav_developmental_milestone_form_label_answer"],
  );

  Map<String, dynamic> toJson() => {
    "nav_centermanagement_todo_label": navCentermanagementTodoLabel,
    "nav_centermanagement_remarks_activity_log": navCentermanagementRemarksActivityLog,
    "nav_centermanagement_profile_label": navCentermanagementProfileLabel,
    "nav_centermanagement_todo_description_label": navCentermanagementTodoDescriptionLabel,
    "nav_centermanagement_medication_label": navCentermanagementMedicationLabel,
    "nav_developmental_milestone_form_label_educator_assets": navDevelopmentalMilestoneFormLabelEducatorAssets,
    "nav_centermanagement_incident_tab_label": navCentermanagementIncidentTabLabel,
    "nav_centermanagement_illness_label": navCentermanagementIllnessLabel,
    "nav_developmental_milestone_form_label_observe": navDevelopmentalMilestoneFormLabelObserve,
    "nav_centermanagement_incident_label": navCentermanagementIncidentLabel,
    "nav_centermanagement_medication_tab_label": navCentermanagementMedicationTabLabel,
    "nav_centermanagement_remarks_tab_label_s": navCentermanagementRemarksTabLabelS,
    "nav_centermanagement_accident_tab_label": navCentermanagementAccidentTabLabel,
    "nav_centermanagement_developmental_milestone_label": navCentermanagementDevelopmentalMilestoneLabel,
    "nav_centermanagement_developmental_milestone_tab_label": navCentermanagementDevelopmentalMilestoneTabLabel,
    "nav_centermanagement_transition_toschool_button_add": navCentermanagementTransitionToschoolButtonAdd,
    "nav_centermanagement_panadol_administration_tab_label": navCentermanagementPanadolAdministrationTabLabel,
    "nav_centermanagement_asthma_button_add": navCentermanagementAsthmaButtonAdd,
    "nav_developmental_milestone_form_label_educator": navDevelopmentalMilestoneFormLabelEducator,
    "nav_centermanagement_photos_album_journey": navCentermanagementPhotosAlbumJourney,
    "nav_centermanagement_accident_button_add": navCentermanagementAccidentButtonAdd,
    "nav_centermanagement_incident_button_add": navCentermanagementIncidentButtonAdd,
    "nav_centermanagement_transition_toschool_label": navCentermanagementTransitionToschoolLabel,
    "nav_centermanagement_photos_album_story": navCentermanagementPhotosAlbumStory,
    "nav_centermanagement_medication_button_add": navCentermanagementMedicationButtonAdd,
    "nav_centermanagement_photos_album_newsfeed": navCentermanagementPhotosAlbumNewsfeed,
    "nav_centermanagement_photos_album_journal": navCentermanagementPhotosAlbumJournal,
    "nav_centermanagement_uploaddoc_button_add": navCentermanagementUploaddocButtonAdd,
    "nav_centermanagement_todo_button_add": navCentermanagementTodoButtonAdd,
    "nav_centermanagement_child_button_edit": navCentermanagementChildButtonEdit,
    "nav_centermanagement_illness_tab_label": navCentermanagementIllnessTabLabel,
    "nav_centermanagement_accident_label": navCentermanagementAccidentLabel,
    "nav_centermanagement_profile_tab_label": navCentermanagementProfileTabLabel,
    "nav_developmental_milestone_form_label_skill_category": navDevelopmentalMilestoneFormLabelSkillCategory,
    "nav_centermanagement_medication_tab_label_s": navCentermanagementMedicationTabLabelS,
    "nav_centermanagement_todo_tab_label": navCentermanagementTodoTabLabel,
    "nav_centermanagement_uploaddoc_tab_label": navCentermanagementUploaddocTabLabel,
    "nav_developmental_milestone_form_label_skill": navDevelopmentalMilestoneFormLabelSkill,
    "nav_centermanagement_asthma_tab_label": navCentermanagementAsthmaTabLabel,
    "nav_centermanagement_panadol_administration_label": navCentermanagementPanadolAdministrationLabel,
    "nav_centermanagement_illness_button_add": navCentermanagementIllnessButtonAdd,
    "nav_centermanagement_remarks_tab_label": navCentermanagementRemarksTabLabel,
    "nav_centermanagement_nonprescribed_medication_tab_label": navCentermanagementNonprescribedMedicationTabLabel,
    "nav_developmental_milestone_form_label_date": navDevelopmentalMilestoneFormLabelDate,
    "nav_developmental_milestone_form_label_eylf": navDevelopmentalMilestoneFormLabelEylf,
    "nav_centermanagement_todo_dateof_label": navCentermanagementTodoDateofLabel,
    "nav_centermanagement_uploaddoc_label": navCentermanagementUploaddocLabel,
    "nav_centermanagement_nonprescribed_medication_label": navCentermanagementNonprescribedMedicationLabel,
    "nav_centermanagement_uploaddoc_activity_log": navCentermanagementUploaddocActivityLog,
    "nav_centermanagement_asthma_label": navCentermanagementAsthmaLabel,
    "nav_centermanagement_observation_tab_label": navCentermanagementObservationTabLabel,
    "nav_developmental_milestone_form_label_title": navDevelopmentalMilestoneFormLabelTitle,
    "nav_developmental_milestone_form_label_extended_learning": navDevelopmentalMilestoneFormLabelExtendedLearning,
    "nav_centermanagement_milestone_tab_label": navCentermanagementMilestoneTabLabel,
    "nav_developmental_milestone_form_label_agegroup": navDevelopmentalMilestoneFormLabelAgegroup,
    "nav_centermanagement_transition_toschool_tab_label": navCentermanagementTransitionToschoolTabLabel,
    "nav_centermanagement_todo_title_label": navCentermanagementTodoTitleLabel,
    "nav_centermanagement_photos_tab_label": navCentermanagementPhotosTabLabel,
    "nav_centermanagement_todo_status_label": navCentermanagementTodoStatusLabel,
    "nav_centermanagement_panadol_administration_button_add": navCentermanagementPanadolAdministrationButtonAdd,
    "nav_developmental_milestone_form_label_answer": navDevelopmentalMilestoneFormLabelAnswer,
  };
}

class ChildProfile {
  String navReportAllergyLblCentre;
  String navCentermanagementChildEmergencyContactEditTitle;
  String navCentermanagementChildEmergencyContactLblFullname;
  String navCentermanagementQuicklinkLabel;
  String navCentermanagementChildEmergencyContactsMaxlimit;
  String navCentermanagementChildEmergencyContactsTitle;
  String navCentermanagementChildEmergencyContactLblPhone;
  String navCentermanagementChildHistoryMessageEmpty;
  String navCentermanagementInternalNote;
  String navReportAllergyLblMainType;
  String navCentermanagementChildAllergyEditTitle;
  String navCentermanagementChildIndexTitle;
  String navCentermanagementChildMessageEmpty;
  String navCentermanagementInternalNoteEmptyMessage;
  String navCentermanagementChildSingleTitle;
  String navCentermanagementChildHistoryIndexTitle;
  String navCentermanagementChildEmergencyContactLblRelationship;
  String navReportAlleryPdfTitle;
  String navCentermanagementChildImplementationMessageEmpty;
  String navCentermanagementChildHistoryLabel;
  String navCentermanagementChildAllergyMainTitle;
  String navCentermanagementChildButtonAddnew;
  String navCentermanagementChildButtonViewHistory;
  String navCentermanagementChildEmergencyContactCreateTitle;
  String navCentermanagementChildFormLabelBottleFeed;
  String navCentermanagementModelNewInternalnoteTitle;
  String navCentermanagementChildRemarksMessageEmpty;
  String navCentermanagementChildAllergyLabelType;
  String navCentermanagementChildEmergencyContactLblType;
  String navCentermanagementChildFormLabelNappyChange;
  String navCentermanagementChildLabel;
  String navCentermanagementInternalNoteLabel;
  String navCentermanagementChildEmergencyContactLblMobile;
  String navCentermanagementChildAllergyTabTitle;
  String navCentermanagementChildAllergySingleTitle;
  String navCentermanagementChildAllergyLabelDescription;
  String navCentermanagementChildPhotosEmpty;
  String navReportAllergyLblType;
  String navCentermanagementChildAllergyCreateButton;
  String navCentermanagementChildButtonNewInternalNote;
  String navCentermanagementChildEmergencyContactTitle;
  String navCentermanagementChildEmergencyContactsTypes;

  ChildProfile({
    this.navReportAllergyLblCentre,
    this.navCentermanagementChildEmergencyContactEditTitle,
    this.navCentermanagementChildEmergencyContactLblFullname,
    this.navCentermanagementQuicklinkLabel,
    this.navCentermanagementChildEmergencyContactsMaxlimit,
    this.navCentermanagementChildEmergencyContactsTitle,
    this.navCentermanagementChildEmergencyContactLblPhone,
    this.navCentermanagementChildHistoryMessageEmpty,
    this.navCentermanagementInternalNote,
    this.navReportAllergyLblMainType,
    this.navCentermanagementChildAllergyEditTitle,
    this.navCentermanagementChildIndexTitle,
    this.navCentermanagementChildMessageEmpty,
    this.navCentermanagementInternalNoteEmptyMessage,
    this.navCentermanagementChildSingleTitle,
    this.navCentermanagementChildHistoryIndexTitle,
    this.navCentermanagementChildEmergencyContactLblRelationship,
    this.navReportAlleryPdfTitle,
    this.navCentermanagementChildImplementationMessageEmpty,
    this.navCentermanagementChildHistoryLabel,
    this.navCentermanagementChildAllergyMainTitle,
    this.navCentermanagementChildButtonAddnew,
    this.navCentermanagementChildButtonViewHistory,
    this.navCentermanagementChildEmergencyContactCreateTitle,
    this.navCentermanagementChildFormLabelBottleFeed,
    this.navCentermanagementModelNewInternalnoteTitle,
    this.navCentermanagementChildRemarksMessageEmpty,
    this.navCentermanagementChildAllergyLabelType,
    this.navCentermanagementChildEmergencyContactLblType,
    this.navCentermanagementChildFormLabelNappyChange,
    this.navCentermanagementChildLabel,
    this.navCentermanagementInternalNoteLabel,
    this.navCentermanagementChildEmergencyContactLblMobile,
    this.navCentermanagementChildAllergyTabTitle,
    this.navCentermanagementChildAllergySingleTitle,
    this.navCentermanagementChildAllergyLabelDescription,
    this.navCentermanagementChildPhotosEmpty,
    this.navReportAllergyLblType,
    this.navCentermanagementChildAllergyCreateButton,
    this.navCentermanagementChildButtonNewInternalNote,
    this.navCentermanagementChildEmergencyContactTitle,
    this.navCentermanagementChildEmergencyContactsTypes,
  });

  factory ChildProfile.fromJson(Map<String, dynamic> json) => new ChildProfile(
    navReportAllergyLblCentre: json["nav_report_allergy_lbl_centre"],
    navCentermanagementChildEmergencyContactEditTitle: json["nav_centermanagement_child_emergency_contact_edit_title"],
    navCentermanagementChildEmergencyContactLblFullname: json["nav_centermanagement_child_emergency_contact_lbl_fullname"],
    navCentermanagementQuicklinkLabel: json["nav_centermanagement_quicklink_label"],
    navCentermanagementChildEmergencyContactsMaxlimit: json["nav_centermanagement_child_emergency_contacts_maxlimit"],
    navCentermanagementChildEmergencyContactsTitle: json["nav_centermanagement_child_emergency_contacts_title"],
    navCentermanagementChildEmergencyContactLblPhone: json["nav_centermanagement_child_emergency_contact_lbl_phone"],
    navCentermanagementChildHistoryMessageEmpty: json["nav_centermanagement_child_history_message_empty"],
    navCentermanagementInternalNote: json["nav_centermanagement_internal_note"],
    navReportAllergyLblMainType: json["nav_report_allergy_lbl_main_type"],
    navCentermanagementChildAllergyEditTitle: json["nav_centermanagement_child_allergy_edit_title"],
    navCentermanagementChildIndexTitle: json["nav_centermanagement_child_index_title"],
    navCentermanagementChildMessageEmpty: json["nav_centermanagement_child_message_empty"],
    navCentermanagementInternalNoteEmptyMessage: json["nav_centermanagement_internal_note_empty_message"],
    navCentermanagementChildSingleTitle: json["nav_centermanagement_child_single_title"],
    navCentermanagementChildHistoryIndexTitle: json["nav_centermanagement_child_history_index_title"],
    navCentermanagementChildEmergencyContactLblRelationship: json["nav_centermanagement_child_emergency_contact_lbl_relationship"],
    navReportAlleryPdfTitle: json["nav_report_allery_pdf_title"],
    navCentermanagementChildImplementationMessageEmpty: json["nav_centermanagement_child_implementation_message_empty"],
    navCentermanagementChildHistoryLabel: json["nav_centermanagement_child_history_label"],
    navCentermanagementChildAllergyMainTitle: json["nav_centermanagement_child_allergy_main_title"],
    navCentermanagementChildButtonAddnew: json["nav_centermanagement_child_button_addnew"],
    navCentermanagementChildButtonViewHistory: json["nav_centermanagement_child_button_view_history"],
    navCentermanagementChildEmergencyContactCreateTitle: json["nav_centermanagement_child_emergency_contact_create_title"],
    navCentermanagementChildFormLabelBottleFeed: json["nav_centermanagement_child_form_label_bottle_feed"],
    navCentermanagementModelNewInternalnoteTitle: json["nav_centermanagement_model_new_internalnote_title"],
    navCentermanagementChildRemarksMessageEmpty: json["nav_centermanagement_child_remarks_message_empty"],
    navCentermanagementChildAllergyLabelType: json["nav_centermanagement_child_allergy_label_type"],
    navCentermanagementChildEmergencyContactLblType: json["nav_centermanagement_child_emergency_contact_lbl_type"],
    navCentermanagementChildFormLabelNappyChange: json["nav_centermanagement_child_form_label_nappy_change"],
    navCentermanagementChildLabel: json["nav_centermanagement_child_label"],
    navCentermanagementInternalNoteLabel: json["nav_centermanagement_internal_note_label"],
    navCentermanagementChildEmergencyContactLblMobile: json["nav_centermanagement_child_emergency_contact_lbl_mobile"],
    navCentermanagementChildAllergyTabTitle: json["nav_centermanagement_child_allergy_tab_title"],
    navCentermanagementChildAllergySingleTitle: json["nav_centermanagement_child_allergy_single_title"],
    navCentermanagementChildAllergyLabelDescription: json["nav_centermanagement_child_allergy_label_description"],
    navCentermanagementChildPhotosEmpty: json["nav_centermanagement_child_photos_empty"],
    navReportAllergyLblType: json["nav_report_allergy_lbl_type"],
    navCentermanagementChildAllergyCreateButton: json["nav_centermanagement_child_allergy_create_button"],
    navCentermanagementChildButtonNewInternalNote: json["nav_centermanagement_child_button_new_internal_note"],
    navCentermanagementChildEmergencyContactTitle: json["nav_centermanagement_child_emergency_contact_title"],
    navCentermanagementChildEmergencyContactsTypes: json["nav_centermanagement_child_emergency_contacts_types"],
  );

  Map<String, dynamic> toJson() => {
    "nav_report_allergy_lbl_centre": navReportAllergyLblCentre,
    "nav_centermanagement_child_emergency_contact_edit_title": navCentermanagementChildEmergencyContactEditTitle,
    "nav_centermanagement_child_emergency_contact_lbl_fullname": navCentermanagementChildEmergencyContactLblFullname,
    "nav_centermanagement_quicklink_label": navCentermanagementQuicklinkLabel,
    "nav_centermanagement_child_emergency_contacts_maxlimit": navCentermanagementChildEmergencyContactsMaxlimit,
    "nav_centermanagement_child_emergency_contacts_title": navCentermanagementChildEmergencyContactsTitle,
    "nav_centermanagement_child_emergency_contact_lbl_phone": navCentermanagementChildEmergencyContactLblPhone,
    "nav_centermanagement_child_history_message_empty": navCentermanagementChildHistoryMessageEmpty,
    "nav_centermanagement_internal_note": navCentermanagementInternalNote,
    "nav_report_allergy_lbl_main_type": navReportAllergyLblMainType,
    "nav_centermanagement_child_allergy_edit_title": navCentermanagementChildAllergyEditTitle,
    "nav_centermanagement_child_index_title": navCentermanagementChildIndexTitle,
    "nav_centermanagement_child_message_empty": navCentermanagementChildMessageEmpty,
    "nav_centermanagement_internal_note_empty_message": navCentermanagementInternalNoteEmptyMessage,
    "nav_centermanagement_child_single_title": navCentermanagementChildSingleTitle,
    "nav_centermanagement_child_history_index_title": navCentermanagementChildHistoryIndexTitle,
    "nav_centermanagement_child_emergency_contact_lbl_relationship": navCentermanagementChildEmergencyContactLblRelationship,
    "nav_report_allery_pdf_title": navReportAlleryPdfTitle,
    "nav_centermanagement_child_implementation_message_empty": navCentermanagementChildImplementationMessageEmpty,
    "nav_centermanagement_child_history_label": navCentermanagementChildHistoryLabel,
    "nav_centermanagement_child_allergy_main_title": navCentermanagementChildAllergyMainTitle,
    "nav_centermanagement_child_button_addnew": navCentermanagementChildButtonAddnew,
    "nav_centermanagement_child_button_view_history": navCentermanagementChildButtonViewHistory,
    "nav_centermanagement_child_emergency_contact_create_title": navCentermanagementChildEmergencyContactCreateTitle,
    "nav_centermanagement_child_form_label_bottle_feed": navCentermanagementChildFormLabelBottleFeed,
    "nav_centermanagement_model_new_internalnote_title": navCentermanagementModelNewInternalnoteTitle,
    "nav_centermanagement_child_remarks_message_empty": navCentermanagementChildRemarksMessageEmpty,
    "nav_centermanagement_child_allergy_label_type": navCentermanagementChildAllergyLabelType,
    "nav_centermanagement_child_emergency_contact_lbl_type": navCentermanagementChildEmergencyContactLblType,
    "nav_centermanagement_child_form_label_nappy_change": navCentermanagementChildFormLabelNappyChange,
    "nav_centermanagement_child_label": navCentermanagementChildLabel,
    "nav_centermanagement_internal_note_label": navCentermanagementInternalNoteLabel,
    "nav_centermanagement_child_emergency_contact_lbl_mobile": navCentermanagementChildEmergencyContactLblMobile,
    "nav_centermanagement_child_allergy_tab_title": navCentermanagementChildAllergyTabTitle,
    "nav_centermanagement_child_allergy_single_title": navCentermanagementChildAllergySingleTitle,
    "nav_centermanagement_child_allergy_label_description": navCentermanagementChildAllergyLabelDescription,
    "nav_centermanagement_child_photos_empty": navCentermanagementChildPhotosEmpty,
    "nav_report_allergy_lbl_type": navReportAllergyLblType,
    "nav_centermanagement_child_allergy_create_button": navCentermanagementChildAllergyCreateButton,
    "nav_centermanagement_child_button_new_internal_note": navCentermanagementChildButtonNewInternalNote,
    "nav_centermanagement_child_emergency_contact_title": navCentermanagementChildEmergencyContactTitle,
    "nav_centermanagement_child_emergency_contacts_types": navCentermanagementChildEmergencyContactsTypes,
  };
}

class ClientSection {
  String navClientMobileJournal;
  String navClientNotificationJournalComments;
  String navClientJournal;
  String navClientPhotosJournalTab;
  String navClientOtherfeeds;
  String parentDailySummaryPhotoLimit;
  String navClientEvents;
  String navClientEventsTab;
  String navClientReports;
  String noteNotificationEdit;
  String navClientProfile;
  String navClientPostfeed;
  String navClientNotification;
  String navClientPhotos;
  String navClientPhotosObservatiosTab;
  String navClientDailySummary;
  String navClientLearningStory;
  String navClientDailychart;
  String navClientChildnoteAdd;
  String navClientPhotosStoryTab;
  String navClientHome;
  String navClientJourney;
  String navClientNotificationLearningStoryComments;
  String navClientChildlist;
  String navClientTagphotos;
  String navClientPhotosNewsfeedTab;

  ClientSection({
    this.navClientMobileJournal,
    this.navClientNotificationJournalComments,
    this.navClientJournal,
    this.navClientPhotosJournalTab,
    this.navClientOtherfeeds,
    this.parentDailySummaryPhotoLimit,
    this.navClientEvents,
    this.navClientEventsTab,
    this.navClientReports,
    this.noteNotificationEdit,
    this.navClientProfile,
    this.navClientPostfeed,
    this.navClientNotification,
    this.navClientPhotos,
    this.navClientPhotosObservatiosTab,
    this.navClientDailySummary,
    this.navClientLearningStory,
    this.navClientDailychart,
    this.navClientChildnoteAdd,
    this.navClientPhotosStoryTab,
    this.navClientHome,
    this.navClientJourney,
    this.navClientNotificationLearningStoryComments,
    this.navClientChildlist,
    this.navClientTagphotos,
    this.navClientPhotosNewsfeedTab,
  });

  factory ClientSection.fromJson(Map<String, dynamic> json) => new ClientSection(
    navClientMobileJournal: json["nav_client_mobile_journal"],
    navClientNotificationJournalComments: json["nav_client_notification_journal_comments"],
    navClientJournal: json["nav_client_journal"],
    navClientPhotosJournalTab: json["nav_client_photos_journal_tab"],
    navClientOtherfeeds: json["nav_client_otherfeeds"],
    parentDailySummaryPhotoLimit: json["parent_daily_summary_photo_limit"],
    navClientEvents: json["nav_client_events"],
    navClientEventsTab: json["nav_client_events_tab"],
    navClientReports: json["nav_client_reports"],
    noteNotificationEdit: json["note_notification_edit"],
    navClientProfile: json["nav_client_profile"],
    navClientPostfeed: json["nav_client_postfeed"],
    navClientNotification: json["nav_client_notification"],
    navClientPhotos: json["nav_client_photos"],
    navClientPhotosObservatiosTab: json["nav_client_photos_observatios_tab"],
    navClientDailySummary: json["nav_client_daily_summary"],
    navClientLearningStory: json["nav_client_learning_story"],
    navClientDailychart: json["nav_client_dailychart"],
    navClientChildnoteAdd: json["nav_client_childnote_add"],
    navClientPhotosStoryTab: json["nav_client_photos_story_tab"],
    navClientHome: json["nav_client_home"],
    navClientJourney: json["nav_client_journey"],
    navClientNotificationLearningStoryComments: json["nav_client_notification_learning_story_comments"],
    navClientChildlist: json["nav_client_childlist"],
    navClientTagphotos: json["nav_client_tagphotos"],
    navClientPhotosNewsfeedTab: json["nav_client_photos_newsfeed_tab"],
  );

  Map<String, dynamic> toJson() => {
    "nav_client_mobile_journal": navClientMobileJournal,
    "nav_client_notification_journal_comments": navClientNotificationJournalComments,
    "nav_client_journal": navClientJournal,
    "nav_client_photos_journal_tab": navClientPhotosJournalTab,
    "nav_client_otherfeeds": navClientOtherfeeds,
    "parent_daily_summary_photo_limit": parentDailySummaryPhotoLimit,
    "nav_client_events": navClientEvents,
    "nav_client_events_tab": navClientEventsTab,
    "nav_client_reports": navClientReports,
    "note_notification_edit": noteNotificationEdit,
    "nav_client_profile": navClientProfile,
    "nav_client_postfeed": navClientPostfeed,
    "nav_client_notification": navClientNotification,
    "nav_client_photos": navClientPhotos,
    "nav_client_photos_observatios_tab": navClientPhotosObservatiosTab,
    "nav_client_daily_summary": navClientDailySummary,
    "nav_client_learning_story": navClientLearningStory,
    "nav_client_dailychart": navClientDailychart,
    "nav_client_childnote_add": navClientChildnoteAdd,
    "nav_client_photos_story_tab": navClientPhotosStoryTab,
    "nav_client_home": navClientHome,
    "nav_client_journey": navClientJourney,
    "nav_client_notification_learning_story_comments": navClientNotificationLearningStoryComments,
    "nav_client_childlist": navClientChildlist,
    "nav_client_tagphotos": navClientTagphotos,
    "nav_client_photos_newsfeed_tab": navClientPhotosNewsfeedTab,
  };
}

class Common {
  String sysWeekendMessage;
  String sysDebug;
  String sysMessageNotExists;
  String sysDataLossMessage;
  String sysClearTempData;
  String sysRequestErrorMessage;
  String sysImageUploadMessage;
  String sysBackButton;
  String sysNodataInitalMessage;
  String sysErrorRouteNotAvailable;
  String sysImageBrokenLinksMessage;
  String sysErrorBadUrl;
  String sysMessageEmpty;
  String sysPrintButton;
  String sysErrorNotAvailable;
  String sysEmptyStateMessage;
  String sysImageUploadLimitExceededMessage;

  Common({
    this.sysWeekendMessage,
    this.sysDebug,
    this.sysMessageNotExists,
    this.sysDataLossMessage,
    this.sysClearTempData,
    this.sysRequestErrorMessage,
    this.sysImageUploadMessage,
    this.sysBackButton,
    this.sysNodataInitalMessage,
    this.sysErrorRouteNotAvailable,
    this.sysImageBrokenLinksMessage,
    this.sysErrorBadUrl,
    this.sysMessageEmpty,
    this.sysPrintButton,
    this.sysErrorNotAvailable,
    this.sysEmptyStateMessage,
    this.sysImageUploadLimitExceededMessage,
  });

  factory Common.fromJson(Map<String, dynamic> json) => new Common(
    sysWeekendMessage: json["sys_weekend_message"],
    sysDebug: json["sys_debug"],
    sysMessageNotExists: json["sys_message_not_exists"],
    sysDataLossMessage: json["sys_data_loss_message"],
    sysClearTempData: json["sys_clear_temp_data"],
    sysRequestErrorMessage: json["sys_request_error_message"],
    sysImageUploadMessage: json["sys_image_upload_message"],
    sysBackButton: json["sys_back_button"],
    sysNodataInitalMessage: json["sys_nodata_inital_message"],
    sysErrorRouteNotAvailable: json["sys_error_route_not_available"],
    sysImageBrokenLinksMessage: json["sys_image_broken_links_message"],
    sysErrorBadUrl: json["sys_error_bad_url"],
    sysMessageEmpty: json["sys_message_empty"],
    sysPrintButton: json["sys_print_button"],
    sysErrorNotAvailable: json["sys_error_not_available"],
    sysEmptyStateMessage: json["sys_empty_state_message"],
    sysImageUploadLimitExceededMessage: json["sys_image_upload_limit_exceeded_message"],
  );

  Map<String, dynamic> toJson() => {
    "sys_weekend_message": sysWeekendMessage,
    "sys_debug": sysDebug,
    "sys_message_not_exists": sysMessageNotExists,
    "sys_data_loss_message": sysDataLossMessage,
    "sys_clear_temp_data": sysClearTempData,
    "sys_request_error_message": sysRequestErrorMessage,
    "sys_image_upload_message": sysImageUploadMessage,
    "sys_back_button": sysBackButton,
    "sys_nodata_inital_message": sysNodataInitalMessage,
    "sys_error_route_not_available": sysErrorRouteNotAvailable,
    "sys_image_broken_links_message": sysImageBrokenLinksMessage,
    "sys_error_bad_url": sysErrorBadUrl,
    "sys_message_empty": sysMessageEmpty,
    "sys_print_button": sysPrintButton,
    "sys_error_not_available": sysErrorNotAvailable,
    "sys_empty_state_message": sysEmptyStateMessage,
    "sys_image_upload_limit_exceeded_message": sysImageUploadLimitExceededMessage,
  };
}

class DashboardCommonNotificationArea {
  String navDashboardDncDefCount;
  String navDashboardDncDefType;
  String navDashboardDncTopHeading;
  String navDashboardDncDefLimit;

  DashboardCommonNotificationArea({
    this.navDashboardDncDefCount,
    this.navDashboardDncDefType,
    this.navDashboardDncTopHeading,
    this.navDashboardDncDefLimit,
  });

  factory DashboardCommonNotificationArea.fromJson(Map<String, dynamic> json) => new DashboardCommonNotificationArea(
    navDashboardDncDefCount: json["nav_dashboard_dnc_def_count"],
    navDashboardDncDefType: json["nav_dashboard_dnc_def_type"],
    navDashboardDncTopHeading: json["nav_dashboard_dnc_top_heading"],
    navDashboardDncDefLimit: json["nav_dashboard_dnc_def_limit"],
  );

  Map<String, dynamic> toJson() => {
    "nav_dashboard_dnc_def_count": navDashboardDncDefCount,
    "nav_dashboard_dnc_def_type": navDashboardDncDefType,
    "nav_dashboard_dnc_top_heading": navDashboardDncTopHeading,
    "nav_dashboard_dnc_def_limit": navDashboardDncDefLimit,
  };
}

class DashboardSection {
  String dashboardNotificationsObservation;
  String dashboardNotificationsLearningStories;
  String navCentermanagementDashboardImplementationsFollowupRemainderLabel;
  String dashboardNotificationsImplementation;
  String dashboardNotificationsRemark;
  String navCentermanagementActivitylogImplementationsLabel;
  String dashboardNotificationsStoryFollowup;
  String dashboardNotificationsNewsfeeds;
  String dashboardNotificationsJournalFollowup;
  String dashboardNotificationsJournal;
  String navCentermanagementDashboardBirthdayLabel;
  String dashboardNotificationsCreatedSecPrescribedMedication;
  String dashboardNotificationsFollowup;
  String dashboardNotificationsEvent;
  String dashboardNotificationsMedication;
  String navCentermanagementDashboardImplementationsLabel;
  String dashboardNotificationsEvents;
  String dashboardNotificationsIncident;
  String dashboardNotificationsIllness;
  String dashboardNotificationsAsthmaMedication;
  String dashboardNotificationsTransitiontoschool;
  String navCentermanagementDashboardBoxLabel;
  String dashboardNotificationsOngoingMedication;
  String navCentermanagementDashboardImplementationsRemainderLabel;
  String dashboardNotificationsPanadoladministration;
  String navDashboardJourneyMessageEmpty;
  String dashboardNotificationsInternalnotes;
  String navDashboardEventMessageEmpty;
  String navDashboardBirthdayMessageEmpty;
  String dashboardNotificationsUploadchilddocuments;
  String dashboardNotificationsInternalnote;
  String dashboardNotificationsIncidentReport;
  String dashboardNotificationsMedicationRequest;
  String dashboardNotificationsNewsfeed;
  String dashboardNotificationsMedicationReport;
  String dashboardNotificationsLearningStory;
  String navDashboardJournalMessageEmpty;
  String dashboardNotificationsChilddocument;
  String dashboardNotificationsChilddocuments;

  DashboardSection({
    this.dashboardNotificationsObservation,
    this.dashboardNotificationsLearningStories,
    this.navCentermanagementDashboardImplementationsFollowupRemainderLabel,
    this.dashboardNotificationsImplementation,
    this.dashboardNotificationsRemark,
    this.navCentermanagementActivitylogImplementationsLabel,
    this.dashboardNotificationsStoryFollowup,
    this.dashboardNotificationsNewsfeeds,
    this.dashboardNotificationsJournalFollowup,
    this.dashboardNotificationsJournal,
    this.navCentermanagementDashboardBirthdayLabel,
    this.dashboardNotificationsCreatedSecPrescribedMedication,
    this.dashboardNotificationsFollowup,
    this.dashboardNotificationsEvent,
    this.dashboardNotificationsMedication,
    this.navCentermanagementDashboardImplementationsLabel,
    this.dashboardNotificationsEvents,
    this.dashboardNotificationsIncident,
    this.dashboardNotificationsIllness,
    this.dashboardNotificationsAsthmaMedication,
    this.dashboardNotificationsTransitiontoschool,
    this.navCentermanagementDashboardBoxLabel,
    this.dashboardNotificationsOngoingMedication,
    this.navCentermanagementDashboardImplementationsRemainderLabel,
    this.dashboardNotificationsPanadoladministration,
    this.navDashboardJourneyMessageEmpty,
    this.dashboardNotificationsInternalnotes,
    this.navDashboardEventMessageEmpty,
    this.navDashboardBirthdayMessageEmpty,
    this.dashboardNotificationsUploadchilddocuments,
    this.dashboardNotificationsInternalnote,
    this.dashboardNotificationsIncidentReport,
    this.dashboardNotificationsMedicationRequest,
    this.dashboardNotificationsNewsfeed,
    this.dashboardNotificationsMedicationReport,
    this.dashboardNotificationsLearningStory,
    this.navDashboardJournalMessageEmpty,
    this.dashboardNotificationsChilddocument,
    this.dashboardNotificationsChilddocuments,
  });

  factory DashboardSection.fromJson(Map<String, dynamic> json) => new DashboardSection(
    dashboardNotificationsObservation: json["dashboard_notifications_observation"],
    dashboardNotificationsLearningStories: json["dashboard_notifications_learning_stories"],
    navCentermanagementDashboardImplementationsFollowupRemainderLabel: json["nav_centermanagement_dashboard_implementations_followup_remainder_label"],
    dashboardNotificationsImplementation: json["dashboard_notifications_implementation"],
    dashboardNotificationsRemark: json["dashboard_notifications_remark"],
    navCentermanagementActivitylogImplementationsLabel: json["nav_centermanagement_activitylog_implementations_label"],
    dashboardNotificationsStoryFollowup: json["dashboard_notifications_story_followup"],
    dashboardNotificationsNewsfeeds: json["dashboard_notifications_newsfeeds"],
    dashboardNotificationsJournalFollowup: json["dashboard_notifications_journal_followup"],
    dashboardNotificationsJournal: json["dashboard_notifications_journal"],
    navCentermanagementDashboardBirthdayLabel: json["nav_centermanagement_dashboard_birthday_label"],
    dashboardNotificationsCreatedSecPrescribedMedication: json["dashboard_notifications_created_sec_prescribed_medication"],
    dashboardNotificationsFollowup: json["dashboard_notifications_followup"],
    dashboardNotificationsEvent: json["dashboard_notifications_event"],
    dashboardNotificationsMedication: json["dashboard_notifications_medication"],
    navCentermanagementDashboardImplementationsLabel: json["nav_centermanagement_dashboard_implementations_label"],
    dashboardNotificationsEvents: json["dashboard_notifications_events"],
    dashboardNotificationsIncident: json["dashboard_notifications_incident"],
    dashboardNotificationsIllness: json["dashboard_notifications_illness"],
    dashboardNotificationsAsthmaMedication: json["dashboard_notifications_asthma_medication"],
    dashboardNotificationsTransitiontoschool: json["dashboard_notifications_transitiontoschool"],
    navCentermanagementDashboardBoxLabel: json["nav_centermanagement_dashboard_box_label"],
    dashboardNotificationsOngoingMedication: json["dashboard_notifications_ongoing_medication"],
    navCentermanagementDashboardImplementationsRemainderLabel: json["nav_centermanagement_dashboard_implementations_remainder_label"],
    dashboardNotificationsPanadoladministration: json["dashboard_notifications_panadoladministration"],
    navDashboardJourneyMessageEmpty: json["nav_dashboard_journey_message_empty"],
    dashboardNotificationsInternalnotes: json["dashboard_notifications_internalnotes"],
    navDashboardEventMessageEmpty: json["nav_dashboard_event_message_empty"],
    navDashboardBirthdayMessageEmpty: json["nav_dashboard_birthday_message_empty"],
    dashboardNotificationsUploadchilddocuments: json["dashboard_notifications_uploadchilddocuments"],
    dashboardNotificationsInternalnote: json["dashboard_notifications_internalnote"],
    dashboardNotificationsIncidentReport: json["dashboard_notifications_incident_report"],
    dashboardNotificationsMedicationRequest: json["dashboard_notifications_medication_request"],
    dashboardNotificationsNewsfeed: json["dashboard_notifications_newsfeed"],
    dashboardNotificationsMedicationReport: json["dashboard_notifications_medication_report"],
    dashboardNotificationsLearningStory: json["dashboard_notifications_learning_story"],
    navDashboardJournalMessageEmpty: json["nav_dashboard_journal_message_empty"],
    dashboardNotificationsChilddocument: json["dashboard_notifications_childdocument"],
    dashboardNotificationsChilddocuments: json["dashboard_notifications_childdocuments"],
  );

  Map<String, dynamic> toJson() => {
    "dashboard_notifications_observation": dashboardNotificationsObservation,
    "dashboard_notifications_learning_stories": dashboardNotificationsLearningStories,
    "nav_centermanagement_dashboard_implementations_followup_remainder_label": navCentermanagementDashboardImplementationsFollowupRemainderLabel,
    "dashboard_notifications_implementation": dashboardNotificationsImplementation,
    "dashboard_notifications_remark": dashboardNotificationsRemark,
    "nav_centermanagement_activitylog_implementations_label": navCentermanagementActivitylogImplementationsLabel,
    "dashboard_notifications_story_followup": dashboardNotificationsStoryFollowup,
    "dashboard_notifications_newsfeeds": dashboardNotificationsNewsfeeds,
    "dashboard_notifications_journal_followup": dashboardNotificationsJournalFollowup,
    "dashboard_notifications_journal": dashboardNotificationsJournal,
    "nav_centermanagement_dashboard_birthday_label": navCentermanagementDashboardBirthdayLabel,
    "dashboard_notifications_created_sec_prescribed_medication": dashboardNotificationsCreatedSecPrescribedMedication,
    "dashboard_notifications_followup": dashboardNotificationsFollowup,
    "dashboard_notifications_event": dashboardNotificationsEvent,
    "dashboard_notifications_medication": dashboardNotificationsMedication,
    "nav_centermanagement_dashboard_implementations_label": navCentermanagementDashboardImplementationsLabel,
    "dashboard_notifications_events": dashboardNotificationsEvents,
    "dashboard_notifications_incident": dashboardNotificationsIncident,
    "dashboard_notifications_illness": dashboardNotificationsIllness,
    "dashboard_notifications_asthma_medication": dashboardNotificationsAsthmaMedication,
    "dashboard_notifications_transitiontoschool": dashboardNotificationsTransitiontoschool,
    "nav_centermanagement_dashboard_box_label": navCentermanagementDashboardBoxLabel,
    "dashboard_notifications_ongoing_medication": dashboardNotificationsOngoingMedication,
    "nav_centermanagement_dashboard_implementations_remainder_label": navCentermanagementDashboardImplementationsRemainderLabel,
    "dashboard_notifications_panadoladministration": dashboardNotificationsPanadoladministration,
    "nav_dashboard_journey_message_empty": navDashboardJourneyMessageEmpty,
    "dashboard_notifications_internalnotes": dashboardNotificationsInternalnotes,
    "nav_dashboard_event_message_empty": navDashboardEventMessageEmpty,
    "nav_dashboard_birthday_message_empty": navDashboardBirthdayMessageEmpty,
    "dashboard_notifications_uploadchilddocuments": dashboardNotificationsUploadchilddocuments,
    "dashboard_notifications_internalnote": dashboardNotificationsInternalnote,
    "dashboard_notifications_incident_report": dashboardNotificationsIncidentReport,
    "dashboard_notifications_medication_request": dashboardNotificationsMedicationRequest,
    "dashboard_notifications_newsfeed": dashboardNotificationsNewsfeed,
    "dashboard_notifications_medication_report": dashboardNotificationsMedicationReport,
    "dashboard_notifications_learning_story": dashboardNotificationsLearningStory,
    "nav_dashboard_journal_message_empty": navDashboardJournalMessageEmpty,
    "dashboard_notifications_childdocument": dashboardNotificationsChilddocument,
    "dashboard_notifications_childdocuments": dashboardNotificationsChilddocuments,
  };
}

class DateFormats {
  String mobileDateFormatFullDateShortTime;
  String globalDateFormat;
  String mobileDateFormatDate;
  String globalDateFormatFullDate;
  String mobileDateFormatFullDateTime;
  String globalDateFormatDateObjectDatepicker;
  String globalDateFormatMomentjs;
  String globalDateTimeFormat12Hr;
  String globalTransitionToSchoolDateFormatLabel;
  String mobileDateFormatFullDate;
  String globalDateFormatSqlSearch;
  String globalChildAgeFormatFull;
  String globalDateFormatDateObject;
  String globalDateFormatFullDateCarbon;
  String globalDateTimeFormat;
  String globalChildAgeFormatShort;
  String globalDateFormatDateObjectDatepickerFull;
  String mobileTimeFormat;

  DateFormats({
    this.mobileDateFormatFullDateShortTime,
    this.globalDateFormat,
    this.mobileDateFormatDate,
    this.globalDateFormatFullDate,
    this.mobileDateFormatFullDateTime,
    this.globalDateFormatDateObjectDatepicker,
    this.globalDateFormatMomentjs,
    this.globalDateTimeFormat12Hr,
    this.globalTransitionToSchoolDateFormatLabel,
    this.mobileDateFormatFullDate,
    this.globalDateFormatSqlSearch,
    this.globalChildAgeFormatFull,
    this.globalDateFormatDateObject,
    this.globalDateFormatFullDateCarbon,
    this.globalDateTimeFormat,
    this.globalChildAgeFormatShort,
    this.globalDateFormatDateObjectDatepickerFull,
    this.mobileTimeFormat,
  });

  factory DateFormats.fromJson(Map<String, dynamic> json) => new DateFormats(
    mobileDateFormatFullDateShortTime: json["mobile_date_format_full_date_short_time"],
    globalDateFormat: json["global_date_format"],
    mobileDateFormatDate: json["mobile_date_format_date"],
    globalDateFormatFullDate: json["global_date_format_full_date"],
    mobileDateFormatFullDateTime: json["mobile_date_format_full_date_time"],
    globalDateFormatDateObjectDatepicker: json["global_date_format_date_object_datepicker"],
    globalDateFormatMomentjs: json["global_date_format_momentjs"],
    globalDateTimeFormat12Hr: json["global_date_time_format_12hr"],
    globalTransitionToSchoolDateFormatLabel: json["global_transition_to_school_date_format_label"],
    mobileDateFormatFullDate: json["mobile_date_format_full_date"],
    globalDateFormatSqlSearch: json["global_date_format_sql_search"],
    globalChildAgeFormatFull: json["global_child_age_format_full"],
    globalDateFormatDateObject: json["global_date_format_date_object"],
    globalDateFormatFullDateCarbon: json["global_date_format_full_date_carbon"],
    globalDateTimeFormat: json["global_date_time_format"],
    globalChildAgeFormatShort: json["global_child_age_format_short"],
    globalDateFormatDateObjectDatepickerFull: json["global_date_format_date_object_datepicker_full"],
    mobileTimeFormat: json["mobile_time_format"],
  );

  Map<String, dynamic> toJson() => {
    "mobile_date_format_full_date_short_time": mobileDateFormatFullDateShortTime,
    "global_date_format": globalDateFormat,
    "mobile_date_format_date": mobileDateFormatDate,
    "global_date_format_full_date": globalDateFormatFullDate,
    "mobile_date_format_full_date_time": mobileDateFormatFullDateTime,
    "global_date_format_date_object_datepicker": globalDateFormatDateObjectDatepicker,
    "global_date_format_momentjs": globalDateFormatMomentjs,
    "global_date_time_format_12hr": globalDateTimeFormat12Hr,
    "global_transition_to_school_date_format_label": globalTransitionToSchoolDateFormatLabel,
    "mobile_date_format_full_date": mobileDateFormatFullDate,
    "global_date_format_sql_search": globalDateFormatSqlSearch,
    "global_child_age_format_full": globalChildAgeFormatFull,
    "global_date_format_date_object": globalDateFormatDateObject,
    "global_date_format_full_date_carbon": globalDateFormatFullDateCarbon,
    "global_date_time_format": globalDateTimeFormat,
    "global_child_age_format_short": globalChildAgeFormatShort,
    "global_date_format_date_object_datepicker_full": globalDateFormatDateObjectDatepickerFull,
    "mobile_time_format": mobileTimeFormat,
  };
}

class EmailLogsSection {
  String navElogIndexTitle;
  String navElogMessageEmpty;
  String navElogLabel;

  EmailLogsSection({
    this.navElogIndexTitle,
    this.navElogMessageEmpty,
    this.navElogLabel,
  });

  factory EmailLogsSection.fromJson(Map<String, dynamic> json) => new EmailLogsSection(
    navElogIndexTitle: json["nav_elog_index_title"],
    navElogMessageEmpty: json["nav_elog_message_empty"],
    navElogLabel: json["nav_elog_label"],
  );

  Map<String, dynamic> toJson() => {
    "nav_elog_index_title": navElogIndexTitle,
    "nav_elog_message_empty": navElogMessageEmpty,
    "nav_elog_label": navElogLabel,
  };
}

class EmailSection {
  String navEmailSubjectJourney;

  EmailSection({
    this.navEmailSubjectJourney,
  });

  factory EmailSection.fromJson(Map<String, dynamic> json) => new EmailSection(
    navEmailSubjectJourney: json["nav_email_subject_journey"],
  );

  Map<String, dynamic> toJson() => {
    "nav_email_subject_journey": navEmailSubjectJourney,
  };
}

class ErrorMessages {
  String messageRecordUpdateError;
  String messageRecordDeletedImage;
  String messageRecordUpdated;
  String messageRecordDeleteErrorImage;
  String messageRecordCreateError;
  String errorMessageGlobalChildParentsTrashed;
  String messageRecordCreated;

  ErrorMessages({
    this.messageRecordUpdateError,
    this.messageRecordDeletedImage,
    this.messageRecordUpdated,
    this.messageRecordDeleteErrorImage,
    this.messageRecordCreateError,
    this.errorMessageGlobalChildParentsTrashed,
    this.messageRecordCreated,
  });

  factory ErrorMessages.fromJson(Map<String, dynamic> json) => new ErrorMessages(
    messageRecordUpdateError: json["message_record_update_error"],
    messageRecordDeletedImage: json["message_record_deleted_image"],
    messageRecordUpdated: json["message_record_updated"],
    messageRecordDeleteErrorImage: json["message_record_delete_error_image"],
    messageRecordCreateError: json["message_record_create_error"],
    errorMessageGlobalChildParentsTrashed: json["error_message_global_child_parents_trashed"],
    messageRecordCreated: json["message_record_created"],
  );

  Map<String, dynamic> toJson() => {
    "message_record_update_error": messageRecordUpdateError,
    "message_record_deleted_image": messageRecordDeletedImage,
    "message_record_updated": messageRecordUpdated,
    "message_record_delete_error_image": messageRecordDeleteErrorImage,
    "message_record_create_error": messageRecordCreateError,
    "error_message_global_child_parents_trashed": errorMessageGlobalChildParentsTrashed,
    "message_record_created": messageRecordCreated,
  };
}

class EventsSection {
  String navEventIndexTitle;
  String navEventLabel;
  String navEventEventModalViewTitle;
  String navEventButtonAddnew;
  String navEventTableLabel;

  EventsSection({
    this.navEventIndexTitle,
    this.navEventLabel,
    this.navEventEventModalViewTitle,
    this.navEventButtonAddnew,
    this.navEventTableLabel,
  });

  factory EventsSection.fromJson(Map<String, dynamic> json) => new EventsSection(
    navEventIndexTitle: json["nav_event_index_title"],
    navEventLabel: json["nav_event_label"],
    navEventEventModalViewTitle: json["nav_event_event_modal_view_title"],
    navEventButtonAddnew: json["nav_event_button_addnew"],
    navEventTableLabel: json["nav_event_table_label"],
  );

  Map<String, dynamic> toJson() => {
    "nav_event_index_title": navEventIndexTitle,
    "nav_event_label": navEventLabel,
    "nav_event_event_modal_view_title": navEventEventModalViewTitle,
    "nav_event_button_addnew": navEventButtonAddnew,
    "nav_event_table_label": navEventTableLabel,
  };
}

class GeneralFormSection {
  String navGformMessageEmpty;
  String navGformModalTitle;
  String navGformLabel;
  String navGformIndexTitle;
  String navGformButtonAddnew;

  GeneralFormSection({
    this.navGformMessageEmpty,
    this.navGformModalTitle,
    this.navGformLabel,
    this.navGformIndexTitle,
    this.navGformButtonAddnew,
  });

  factory GeneralFormSection.fromJson(Map<String, dynamic> json) => new GeneralFormSection(
    navGformMessageEmpty: json["nav_gform_message_empty"],
    navGformModalTitle: json["nav_gform_modal_title"],
    navGformLabel: json["nav_gform_label"],
    navGformIndexTitle: json["nav_gform_index_title"],
    navGformButtonAddnew: json["nav_gform_button_addnew"],
  );

  Map<String, dynamic> toJson() => {
    "nav_gform_message_empty": navGformMessageEmpty,
    "nav_gform_modal_title": navGformModalTitle,
    "nav_gform_label": navGformLabel,
    "nav_gform_index_title": navGformIndexTitle,
    "nav_gform_button_addnew": navGformButtonAddnew,
  };
}

class ImageRearrangeSection {
  String labelImageRearrangeHelpText;
  String labelImageRearrangeButtonText;
  String labelImageRearrangeTitle;

  ImageRearrangeSection({
    this.labelImageRearrangeHelpText,
    this.labelImageRearrangeButtonText,
    this.labelImageRearrangeTitle,
  });

  factory ImageRearrangeSection.fromJson(Map<String, dynamic> json) => new ImageRearrangeSection(
    labelImageRearrangeHelpText: json["label_image_rearrange_help_text"],
    labelImageRearrangeButtonText: json["label_image_rearrange_button_text"],
    labelImageRearrangeTitle: json["label_image_rearrange_title"],
  );

  Map<String, dynamic> toJson() => {
    "label_image_rearrange_help_text": labelImageRearrangeHelpText,
    "label_image_rearrange_button_text": labelImageRearrangeButtonText,
    "label_image_rearrange_title": labelImageRearrangeTitle,
  };
}

class JournalFollowup {
  String navCentermanagementJournalFollowupButtonView;
  String navCentermanagementJournalFollowupIndexTitle;
  String navCentermanagementJournalFollowupButton;
  String navCentermanagementJournalFollowupButtonAddnewDraft;
  String navCentermanagementJournalFollowup;
  String navCentermanagementJournalFollowupButtonAddnew;

  JournalFollowup({
    this.navCentermanagementJournalFollowupButtonView,
    this.navCentermanagementJournalFollowupIndexTitle,
    this.navCentermanagementJournalFollowupButton,
    this.navCentermanagementJournalFollowupButtonAddnewDraft,
    this.navCentermanagementJournalFollowup,
    this.navCentermanagementJournalFollowupButtonAddnew,
  });

  factory JournalFollowup.fromJson(Map<String, dynamic> json) => new JournalFollowup(
    navCentermanagementJournalFollowupButtonView: json["nav_centermanagement_journal_followup_button_view"],
    navCentermanagementJournalFollowupIndexTitle: json["nav_centermanagement_journal_followup_index_title"],
    navCentermanagementJournalFollowupButton: json["nav_centermanagement_journal_followup_button"],
    navCentermanagementJournalFollowupButtonAddnewDraft: json["nav_centermanagement_journal_followup_button_addnew_draft"],
    navCentermanagementJournalFollowup: json["nav_centermanagement_journal_followup"],
    navCentermanagementJournalFollowupButtonAddnew: json["nav_centermanagement_journal_followup_button_addnew"],
  );

  Map<String, dynamic> toJson() => {
    "nav_centermanagement_journal_followup_button_view": navCentermanagementJournalFollowupButtonView,
    "nav_centermanagement_journal_followup_index_title": navCentermanagementJournalFollowupIndexTitle,
    "nav_centermanagement_journal_followup_button": navCentermanagementJournalFollowupButton,
    "nav_centermanagement_journal_followup_button_addnew_draft": navCentermanagementJournalFollowupButtonAddnewDraft,
    "nav_centermanagement_journal_followup": navCentermanagementJournalFollowup,
    "nav_centermanagement_journal_followup_button_addnew": navCentermanagementJournalFollowupButtonAddnew,
  };
}

class KioskActivitySection {
  String kioskActivityLblTitle;
  String navKioskActivities;
  String navKioskActivityNewBtn;
  String navKioskActivityNewHeading;
  String navKioskActivityEditHeading;
  String kioskActivityLblDetails;
  String navKioskActivity;

  KioskActivitySection({
    this.kioskActivityLblTitle,
    this.navKioskActivities,
    this.navKioskActivityNewBtn,
    this.navKioskActivityNewHeading,
    this.navKioskActivityEditHeading,
    this.kioskActivityLblDetails,
    this.navKioskActivity,
  });

  factory KioskActivitySection.fromJson(Map<String, dynamic> json) => new KioskActivitySection(
    kioskActivityLblTitle: json["kiosk_activity_lbl_title"],
    navKioskActivities: json["nav_kiosk_activities"],
    navKioskActivityNewBtn: json["nav_kiosk_activity_new_btn"],
    navKioskActivityNewHeading: json["nav_kiosk_activity_new_heading"],
    navKioskActivityEditHeading: json["nav_kiosk_activity_edit_heading"],
    kioskActivityLblDetails: json["kiosk_activity_lbl_details"],
    navKioskActivity: json["nav_kiosk_activity"],
  );

  Map<String, dynamic> toJson() => {
    "kiosk_activity_lbl_title": kioskActivityLblTitle,
    "nav_kiosk_activities": navKioskActivities,
    "nav_kiosk_activity_new_btn": navKioskActivityNewBtn,
    "nav_kiosk_activity_new_heading": navKioskActivityNewHeading,
    "nav_kiosk_activity_edit_heading": navKioskActivityEditHeading,
    "kiosk_activity_lbl_details": kioskActivityLblDetails,
    "nav_kiosk_activity": navKioskActivity,
  };
}

class KioskSection {
  String navKioskDashboardLeftTitleChild;
  String navKioskDashboardIndexTitle;
  String navKioskPincodeButtonAddnew;
  String navKioskUserSettingsTabQualificationsLbl;
  String navKioskAttendanceCheckLabel;
  String navKioskButtonAddnew;
  String navKioskPincodeLabelFilter;
  String navKioskReportTitle;
  String navKioskFilterLabelOptionChild;
  String navKioskPincodeLabelFilterParent;
  String navReportKioskWeeklytimesheetPdfTitle;
  String navKioskUserSettingsTab;
  String navKioskFilterLabelOptionStaff;
  String navKioskPincodeLabel;
  String navKioskStaffManualAttendanceManage;
  String navKioskUserSettingsTabLeaderLbl;
  String navKioskPincodeIndexTitle;
  String navKioskDropAlert;
  String navKioskStaffManualAttendanceManageStaff;
  String navKioskUserSettingsTabWorkingHoursLbl;
  String navKioskTimelineReportLeaderLbl;
  String navKioskDashboardLeftTitleStaff;
  String navKioskLabel;
  String navKioskFilterLabelView;
  String navKioskChildManualAttendanceManageChild;
  String navKioskSummaryReportPdfTitle;
  String navReportKioskTimelinePdfTitle;
  String navKioskSettingsTitle;
  String navKioskUserSettingsTabPositionLbl;
  String navKioskChildManualAttendanceManage;
  String navKioskPincodeLabelFilterStaff;
  String navKioskDashboardLabel;
  String navKioskUserSettingsTabWorkingHoursHelperLbl;
  String navKioskUserSettingsTabLunchtimepaidLbl;
  String navKioskIndexTitle;
  String navKioskPincodeFormLabelTeacher;
  String navKioskPincodeFormLabel;
  String navKioskStaffAttendanceReportTitle;
  String navKioskSummaryReportTitle;

  KioskSection({
    this.navKioskDashboardLeftTitleChild,
    this.navKioskDashboardIndexTitle,
    this.navKioskPincodeButtonAddnew,
    this.navKioskUserSettingsTabQualificationsLbl,
    this.navKioskAttendanceCheckLabel,
    this.navKioskButtonAddnew,
    this.navKioskPincodeLabelFilter,
    this.navKioskReportTitle,
    this.navKioskFilterLabelOptionChild,
    this.navKioskPincodeLabelFilterParent,
    this.navReportKioskWeeklytimesheetPdfTitle,
    this.navKioskUserSettingsTab,
    this.navKioskFilterLabelOptionStaff,
    this.navKioskPincodeLabel,
    this.navKioskStaffManualAttendanceManage,
    this.navKioskUserSettingsTabLeaderLbl,
    this.navKioskPincodeIndexTitle,
    this.navKioskDropAlert,
    this.navKioskStaffManualAttendanceManageStaff,
    this.navKioskUserSettingsTabWorkingHoursLbl,
    this.navKioskTimelineReportLeaderLbl,
    this.navKioskDashboardLeftTitleStaff,
    this.navKioskLabel,
    this.navKioskFilterLabelView,
    this.navKioskChildManualAttendanceManageChild,
    this.navKioskSummaryReportPdfTitle,
    this.navReportKioskTimelinePdfTitle,
    this.navKioskSettingsTitle,
    this.navKioskUserSettingsTabPositionLbl,
    this.navKioskChildManualAttendanceManage,
    this.navKioskPincodeLabelFilterStaff,
    this.navKioskDashboardLabel,
    this.navKioskUserSettingsTabWorkingHoursHelperLbl,
    this.navKioskUserSettingsTabLunchtimepaidLbl,
    this.navKioskIndexTitle,
    this.navKioskPincodeFormLabelTeacher,
    this.navKioskPincodeFormLabel,
    this.navKioskStaffAttendanceReportTitle,
    this.navKioskSummaryReportTitle,
  });

  factory KioskSection.fromJson(Map<String, dynamic> json) => new KioskSection(
    navKioskDashboardLeftTitleChild: json["nav_kiosk_dashboard_left_title_child"],
    navKioskDashboardIndexTitle: json["nav_kiosk_dashboard_index_title"],
    navKioskPincodeButtonAddnew: json["nav_kiosk_pincode_button_addnew"],
    navKioskUserSettingsTabQualificationsLbl: json["nav_kiosk_user_settings_tab_qualifications_lbl"],
    navKioskAttendanceCheckLabel: json["nav_kiosk_attendance_check_label"],
    navKioskButtonAddnew: json["nav_kiosk_button_addnew"],
    navKioskPincodeLabelFilter: json["nav_kiosk_pincode_label_filter"],
    navKioskReportTitle: json["nav_kiosk_report_title"],
    navKioskFilterLabelOptionChild: json["nav_kiosk_filter_label_option_child"],
    navKioskPincodeLabelFilterParent: json["nav_kiosk_pincode_label_filter_parent"],
    navReportKioskWeeklytimesheetPdfTitle: json["nav_report_kiosk_weeklytimesheet_pdf_title"],
    navKioskUserSettingsTab: json["nav_kiosk_user_settings_tab"],
    navKioskFilterLabelOptionStaff: json["nav_kiosk_filter_label_option_staff"],
    navKioskPincodeLabel: json["nav_kiosk_pincode_label"],
    navKioskStaffManualAttendanceManage: json["nav_kiosk_staff_manual_attendance_manage"],
    navKioskUserSettingsTabLeaderLbl: json["nav_kiosk_user_settings_tab_leader_lbl"],
    navKioskPincodeIndexTitle: json["nav_kiosk_pincode_index_title"],
    navKioskDropAlert: json["nav_kiosk_drop_alert"],
    navKioskStaffManualAttendanceManageStaff: json["nav_kiosk_staff_manual_attendance_manage_staff"],
    navKioskUserSettingsTabWorkingHoursLbl: json["nav_kiosk_user_settings_tab_working_hours_lbl"],
    navKioskTimelineReportLeaderLbl: json["nav_kiosk_timeline_report_leader_lbl"],
    navKioskDashboardLeftTitleStaff: json["nav_kiosk_dashboard_left_title_staff"],
    navKioskLabel: json["nav_kiosk_label"],
    navKioskFilterLabelView: json["nav_kiosk_filter_label_view"],
    navKioskChildManualAttendanceManageChild: json["nav_kiosk_child_manual_attendance_manage_child"],
    navKioskSummaryReportPdfTitle: json["nav_kiosk_summary_report_pdf_title"],
    navReportKioskTimelinePdfTitle: json["nav_report_kiosk_timeline_pdf_title"],
    navKioskSettingsTitle: json["nav_kiosk_settings_title"],
    navKioskUserSettingsTabPositionLbl: json["nav_kiosk_user_settings_tab_position_lbl"],
    navKioskChildManualAttendanceManage: json["nav_kiosk_child_manual_attendance_manage"],
    navKioskPincodeLabelFilterStaff: json["nav_kiosk_pincode_label_filter_staff"],
    navKioskDashboardLabel: json["nav_kiosk_dashboard_label"],
    navKioskUserSettingsTabWorkingHoursHelperLbl: json["nav_kiosk_user_settings_tab_working_hours_helper_lbl"],
    navKioskUserSettingsTabLunchtimepaidLbl: json["nav_kiosk_user_settings_tab_lunchtimepaid_lbl"],
    navKioskIndexTitle: json["nav_kiosk_index_title"],
    navKioskPincodeFormLabelTeacher: json["nav_kiosk_pincode_form_label_teacher"],
    navKioskPincodeFormLabel: json["nav_kiosk_pincode_form_label"],
    navKioskStaffAttendanceReportTitle: json["nav_kiosk_staff_attendance_report_title"],
    navKioskSummaryReportTitle: json["nav_kiosk_summary_report_title"],
  );

  Map<String, dynamic> toJson() => {
    "nav_kiosk_dashboard_left_title_child": navKioskDashboardLeftTitleChild,
    "nav_kiosk_dashboard_index_title": navKioskDashboardIndexTitle,
    "nav_kiosk_pincode_button_addnew": navKioskPincodeButtonAddnew,
    "nav_kiosk_user_settings_tab_qualifications_lbl": navKioskUserSettingsTabQualificationsLbl,
    "nav_kiosk_attendance_check_label": navKioskAttendanceCheckLabel,
    "nav_kiosk_button_addnew": navKioskButtonAddnew,
    "nav_kiosk_pincode_label_filter": navKioskPincodeLabelFilter,
    "nav_kiosk_report_title": navKioskReportTitle,
    "nav_kiosk_filter_label_option_child": navKioskFilterLabelOptionChild,
    "nav_kiosk_pincode_label_filter_parent": navKioskPincodeLabelFilterParent,
    "nav_report_kiosk_weeklytimesheet_pdf_title": navReportKioskWeeklytimesheetPdfTitle,
    "nav_kiosk_user_settings_tab": navKioskUserSettingsTab,
    "nav_kiosk_filter_label_option_staff": navKioskFilterLabelOptionStaff,
    "nav_kiosk_pincode_label": navKioskPincodeLabel,
    "nav_kiosk_staff_manual_attendance_manage": navKioskStaffManualAttendanceManage,
    "nav_kiosk_user_settings_tab_leader_lbl": navKioskUserSettingsTabLeaderLbl,
    "nav_kiosk_pincode_index_title": navKioskPincodeIndexTitle,
    "nav_kiosk_drop_alert": navKioskDropAlert,
    "nav_kiosk_staff_manual_attendance_manage_staff": navKioskStaffManualAttendanceManageStaff,
    "nav_kiosk_user_settings_tab_working_hours_lbl": navKioskUserSettingsTabWorkingHoursLbl,
    "nav_kiosk_timeline_report_leader_lbl": navKioskTimelineReportLeaderLbl,
    "nav_kiosk_dashboard_left_title_staff": navKioskDashboardLeftTitleStaff,
    "nav_kiosk_label": navKioskLabel,
    "nav_kiosk_filter_label_view": navKioskFilterLabelView,
    "nav_kiosk_child_manual_attendance_manage_child": navKioskChildManualAttendanceManageChild,
    "nav_kiosk_summary_report_pdf_title": navKioskSummaryReportPdfTitle,
    "nav_report_kiosk_timeline_pdf_title": navReportKioskTimelinePdfTitle,
    "nav_kiosk_settings_title": navKioskSettingsTitle,
    "nav_kiosk_user_settings_tab_position_lbl": navKioskUserSettingsTabPositionLbl,
    "nav_kiosk_child_manual_attendance_manage": navKioskChildManualAttendanceManage,
    "nav_kiosk_pincode_label_filter_staff": navKioskPincodeLabelFilterStaff,
    "nav_kiosk_dashboard_label": navKioskDashboardLabel,
    "nav_kiosk_user_settings_tab_working_hours_helper_lbl": navKioskUserSettingsTabWorkingHoursHelperLbl,
    "nav_kiosk_user_settings_tab_lunchtimepaid_lbl": navKioskUserSettingsTabLunchtimepaidLbl,
    "nav_kiosk_index_title": navKioskIndexTitle,
    "nav_kiosk_pincode_form_label_teacher": navKioskPincodeFormLabelTeacher,
    "nav_kiosk_pincode_form_label": navKioskPincodeFormLabel,
    "nav_kiosk_staff_attendance_report_title": navKioskStaffAttendanceReportTitle,
    "nav_kiosk_summary_report_title": navKioskSummaryReportTitle,
  };
}

class KioskStaffCheckinCheckoutSection {
  String kioskStaffcheckinActivityCompleteDay;
  String kioskStaffcheckinDashboardTitle;
  String kioskStaffcheckinActivityLabel;
  String kioskStaffcheckinModalTitle;
  String kioskStaffcheckinActivityConfirmation;

  KioskStaffCheckinCheckoutSection({
    this.kioskStaffcheckinActivityCompleteDay,
    this.kioskStaffcheckinDashboardTitle,
    this.kioskStaffcheckinActivityLabel,
    this.kioskStaffcheckinModalTitle,
    this.kioskStaffcheckinActivityConfirmation,
  });

  factory KioskStaffCheckinCheckoutSection.fromJson(Map<String, dynamic> json) => new KioskStaffCheckinCheckoutSection(
    kioskStaffcheckinActivityCompleteDay: json["kiosk_staffcheckin_activity_complete_day"],
    kioskStaffcheckinDashboardTitle: json["kiosk_staffcheckin_dashboard_title"],
    kioskStaffcheckinActivityLabel: json["kiosk_staffcheckin_activity_label"],
    kioskStaffcheckinModalTitle: json["kiosk_staffcheckin_modal_title"],
    kioskStaffcheckinActivityConfirmation: json["kiosk_staffcheckin_activity_confirmation"],
  );

  Map<String, dynamic> toJson() => {
    "kiosk_staffcheckin_activity_complete_day": kioskStaffcheckinActivityCompleteDay,
    "kiosk_staffcheckin_dashboard_title": kioskStaffcheckinDashboardTitle,
    "kiosk_staffcheckin_activity_label": kioskStaffcheckinActivityLabel,
    "kiosk_staffcheckin_modal_title": kioskStaffcheckinModalTitle,
    "kiosk_staffcheckin_activity_confirmation": kioskStaffcheckinActivityConfirmation,
  };
}

class LearningsetSection {
  String navLearningsetMessageEmpty;
  String navLearningsetButtonAdd;
  String navLearningsetLabelTopSelect;
  String navLearningsetDefaultTitle;
  String navPracticesModalNewTitle;
  String navLearningsetLabel;
  String navPriciplesModalNewTitle;
  String navLearningsetIndexTitle;
  String navLearningsetDefaultPara;
  String navLearningsetModalNewTitle;

  LearningsetSection({
    this.navLearningsetMessageEmpty,
    this.navLearningsetButtonAdd,
    this.navLearningsetLabelTopSelect,
    this.navLearningsetDefaultTitle,
    this.navPracticesModalNewTitle,
    this.navLearningsetLabel,
    this.navPriciplesModalNewTitle,
    this.navLearningsetIndexTitle,
    this.navLearningsetDefaultPara,
    this.navLearningsetModalNewTitle,
  });

  factory LearningsetSection.fromJson(Map<String, dynamic> json) => new LearningsetSection(
    navLearningsetMessageEmpty: json["nav_learningset_message_empty"],
    navLearningsetButtonAdd: json["nav_learningset_button_add"],
    navLearningsetLabelTopSelect: json["nav_learningset_label_top_select"],
    navLearningsetDefaultTitle: json["nav_learningset_default_title"],
    navPracticesModalNewTitle: json["nav_practices_modal_new_title"],
    navLearningsetLabel: json["nav_learningset_label"],
    navPriciplesModalNewTitle: json["nav_priciples_modal_new_title"],
    navLearningsetIndexTitle: json["nav_learningset_index_title"],
    navLearningsetDefaultPara: json["nav_learningset_default_para"],
    navLearningsetModalNewTitle: json["nav_learningset_modal_new_title"],
  );

  Map<String, dynamic> toJson() => {
    "nav_learningset_message_empty": navLearningsetMessageEmpty,
    "nav_learningset_button_add": navLearningsetButtonAdd,
    "nav_learningset_label_top_select": navLearningsetLabelTopSelect,
    "nav_learningset_default_title": navLearningsetDefaultTitle,
    "nav_practices_modal_new_title": navPracticesModalNewTitle,
    "nav_learningset_label": navLearningsetLabel,
    "nav_priciples_modal_new_title": navPriciplesModalNewTitle,
    "nav_learningset_index_title": navLearningsetIndexTitle,
    "nav_learningset_default_para": navLearningsetDefaultPara,
    "nav_learningset_modal_new_title": navLearningsetModalNewTitle,
  };
}

class LearningstoryFollowup {
  String navCentermanagementStoryFollowupButtonAddnew;
  String navCentermanagementStoryFollowup;
  String navCentermanagementStoryFollowupButtonAddnewDraft;
  String navCentermanagementStoryFollowupIndexTitle;
  String navCentermanagementStoryFollowupButton;
  String navCentermanagementStoryFollowupButtonView;

  LearningstoryFollowup({
    this.navCentermanagementStoryFollowupButtonAddnew,
    this.navCentermanagementStoryFollowup,
    this.navCentermanagementStoryFollowupButtonAddnewDraft,
    this.navCentermanagementStoryFollowupIndexTitle,
    this.navCentermanagementStoryFollowupButton,
    this.navCentermanagementStoryFollowupButtonView,
  });

  factory LearningstoryFollowup.fromJson(Map<String, dynamic> json) => new LearningstoryFollowup(
    navCentermanagementStoryFollowupButtonAddnew: json["nav_centermanagement_story_followup_button_addnew"],
    navCentermanagementStoryFollowup: json["nav_centermanagement_story_followup"],
    navCentermanagementStoryFollowupButtonAddnewDraft: json["nav_centermanagement_story_followup_button_addnew_draft"],
    navCentermanagementStoryFollowupIndexTitle: json["nav_centermanagement_story_followup_index_title"],
    navCentermanagementStoryFollowupButton: json["nav_centermanagement_story_followup_button"],
    navCentermanagementStoryFollowupButtonView: json["nav_centermanagement_story_followup_button_view"],
  );

  Map<String, dynamic> toJson() => {
    "nav_centermanagement_story_followup_button_addnew": navCentermanagementStoryFollowupButtonAddnew,
    "nav_centermanagement_story_followup": navCentermanagementStoryFollowup,
    "nav_centermanagement_story_followup_button_addnew_draft": navCentermanagementStoryFollowupButtonAddnewDraft,
    "nav_centermanagement_story_followup_index_title": navCentermanagementStoryFollowupIndexTitle,
    "nav_centermanagement_story_followup_button": navCentermanagementStoryFollowupButton,
    "nav_centermanagement_story_followup_button_view": navCentermanagementStoryFollowupButtonView,
  };
}

class LearningtagSection {
  String navLearningTagsButtonAdd;
  String navLearningTagsLabel;
  String navLearningTagLabel;
  String navLearningTagsLabelName;
  String navLearningTagsLabelColor;
  String navLearningTagsButtonEdit;

  LearningtagSection({
    this.navLearningTagsButtonAdd,
    this.navLearningTagsLabel,
    this.navLearningTagLabel,
    this.navLearningTagsLabelName,
    this.navLearningTagsLabelColor,
    this.navLearningTagsButtonEdit,
  });

  factory LearningtagSection.fromJson(Map<String, dynamic> json) => new LearningtagSection(
    navLearningTagsButtonAdd: json["nav_learning_tags_button_add"],
    navLearningTagsLabel: json["nav_learning_tags_label"],
    navLearningTagLabel: json["nav_learning_tag_label"],
    navLearningTagsLabelName: json["nav_learning_tags_label_name"],
    navLearningTagsLabelColor: json["nav_learning_tags_label_color"],
    navLearningTagsButtonEdit: json["nav_learning_tags_button_edit"],
  );

  Map<String, dynamic> toJson() => {
    "nav_learning_tags_button_add": navLearningTagsButtonAdd,
    "nav_learning_tags_label": navLearningTagsLabel,
    "nav_learning_tag_label": navLearningTagLabel,
    "nav_learning_tags_label_name": navLearningTagsLabelName,
    "nav_learning_tags_label_color": navLearningTagsLabelColor,
    "nav_learning_tags_button_edit": navLearningTagsButtonEdit,
  };
}

class ManageCentreNotificationSection {
  String labelNotificaitonTypeEnableTabTitle;
  String labelNotificaitonTypeEnableTabParent;
  String labelNotificaitonTypeEnableTabNtftype;
  String labelNotificaitonTypeEnableTabParentMobile;
  String labelNotificaitonTypeEnableTabStaff;

  ManageCentreNotificationSection({
    this.labelNotificaitonTypeEnableTabTitle,
    this.labelNotificaitonTypeEnableTabParent,
    this.labelNotificaitonTypeEnableTabNtftype,
    this.labelNotificaitonTypeEnableTabParentMobile,
    this.labelNotificaitonTypeEnableTabStaff,
  });

  factory ManageCentreNotificationSection.fromJson(Map<String, dynamic> json) => new ManageCentreNotificationSection(
    labelNotificaitonTypeEnableTabTitle: json["label_notificaiton_type_enable_tab_title"],
    labelNotificaitonTypeEnableTabParent: json["label_notificaiton_type_enable_tab_parent"],
    labelNotificaitonTypeEnableTabNtftype: json["label_notificaiton_type_enable_tab_ntftype"],
    labelNotificaitonTypeEnableTabParentMobile: json["label_notificaiton_type_enable_tab_parent_mobile"],
    labelNotificaitonTypeEnableTabStaff: json["label_notificaiton_type_enable_tab_staff"],
  );

  Map<String, dynamic> toJson() => {
    "label_notificaiton_type_enable_tab_title": labelNotificaitonTypeEnableTabTitle,
    "label_notificaiton_type_enable_tab_parent": labelNotificaitonTypeEnableTabParent,
    "label_notificaiton_type_enable_tab_ntftype": labelNotificaitonTypeEnableTabNtftype,
    "label_notificaiton_type_enable_tab_parent_mobile": labelNotificaitonTypeEnableTabParentMobile,
    "label_notificaiton_type_enable_tab_staff": labelNotificaitonTypeEnableTabStaff,
  };
}

class MedicationSection {
  String medicationLabelFuthercomments;
  String medicationLabelTimes;
  String administeredMedicationHistorySubMedicationsPageTitle;
  String medicationLabelDosageofmedication;
  String medicationLabelCreateHelpertext;
  String navMedicationAction;
  String medicationLabelStaffsignature;
  String medicationLabelDosageofmedicationPlaceholder;
  String medicationLabelStaffchecksignature;
  String navMedicationmanagementDchartOptionLabelAcknowledgedHistory;
  String administeredMedicationSubMedicationsCloneTitle;
  String navMedicationmanagementDchartOptionLabelActionEdit;
  String medicationLabelMedicalpractitionerShort;
  String medicationLabelTimemedicationwasadministered;
  String medicationLabelInstructions;
  String navMedicationActionAddBtn;
  String navMedicationmanagementDchartOptionLabelActionRepeat;
  String medicationLabelExpirydate;
  String navMedicationManageTitle;
  String navMedicationmanagementDchartOptionLabelPendingHistory;
  String medicationMainTitle;
  String medicationLabelHastheparentbeennotified;
  String navMedicationActionIndexTitle;
  String medicationLabelActionstaffchecksignature;
  String navMedicationmanagementDchartOptionLabelActionAdd;
  String medicationLabelMedicationinoriginalpackagingShort;
  String navMedicationAcknowledge;
  String administeredMedicationSubMedicationsCloneBtnTitle;
  String medicationLabelNameofmedication;
  String medicationLabelLastadministered;
  String medicationLabelStaffwitness;
  String medicationLabelDossgivenbystaff;
  String navMedicationActionHistoryBtn;
  String medicationLabelMedicalpractitioner;
  String medicationLabelMethodtobeadministered;
  String medicationLabelEndingon;
  String medicationLabelActionstaffsignature;
  String medicationLabelMaindate;
  String medicationLabelMedicationadministered;
  String medicationLabelReasonformedication;
  String administeredMedicationHistorySubMedicationsTitle;
  String navMedicationManage;
  String medicationLabelComments;
  String administeredMedicationMainTitle;
  String navMedicationmanagementDchartOptionLabelActionHistory;
  String medicationLabelAcknowledge;
  String medicationLabelMethodtobeadministeredPlaceholder;
  String administeredMedicationMainDateTitle;
  String medicationLabelParentsignature;
  String medicationLabelStorage;
  String medicationLabelDateprescribed;
  String medicationLabelStartingon;
  String medicationLabelStaffwhoadministered;
  String medicationLabelMedicationinoriginalpackaging;

  MedicationSection({
    this.medicationLabelFuthercomments,
    this.medicationLabelTimes,
    this.administeredMedicationHistorySubMedicationsPageTitle,
    this.medicationLabelDosageofmedication,
    this.medicationLabelCreateHelpertext,
    this.navMedicationAction,
    this.medicationLabelStaffsignature,
    this.medicationLabelDosageofmedicationPlaceholder,
    this.medicationLabelStaffchecksignature,
    this.navMedicationmanagementDchartOptionLabelAcknowledgedHistory,
    this.administeredMedicationSubMedicationsCloneTitle,
    this.navMedicationmanagementDchartOptionLabelActionEdit,
    this.medicationLabelMedicalpractitionerShort,
    this.medicationLabelTimemedicationwasadministered,
    this.medicationLabelInstructions,
    this.navMedicationActionAddBtn,
    this.navMedicationmanagementDchartOptionLabelActionRepeat,
    this.medicationLabelExpirydate,
    this.navMedicationManageTitle,
    this.navMedicationmanagementDchartOptionLabelPendingHistory,
    this.medicationMainTitle,
    this.medicationLabelHastheparentbeennotified,
    this.navMedicationActionIndexTitle,
    this.medicationLabelActionstaffchecksignature,
    this.navMedicationmanagementDchartOptionLabelActionAdd,
    this.medicationLabelMedicationinoriginalpackagingShort,
    this.navMedicationAcknowledge,
    this.administeredMedicationSubMedicationsCloneBtnTitle,
    this.medicationLabelNameofmedication,
    this.medicationLabelLastadministered,
    this.medicationLabelStaffwitness,
    this.medicationLabelDossgivenbystaff,
    this.navMedicationActionHistoryBtn,
    this.medicationLabelMedicalpractitioner,
    this.medicationLabelMethodtobeadministered,
    this.medicationLabelEndingon,
    this.medicationLabelActionstaffsignature,
    this.medicationLabelMaindate,
    this.medicationLabelMedicationadministered,
    this.medicationLabelReasonformedication,
    this.administeredMedicationHistorySubMedicationsTitle,
    this.navMedicationManage,
    this.medicationLabelComments,
    this.administeredMedicationMainTitle,
    this.navMedicationmanagementDchartOptionLabelActionHistory,
    this.medicationLabelAcknowledge,
    this.medicationLabelMethodtobeadministeredPlaceholder,
    this.administeredMedicationMainDateTitle,
    this.medicationLabelParentsignature,
    this.medicationLabelStorage,
    this.medicationLabelDateprescribed,
    this.medicationLabelStartingon,
    this.medicationLabelStaffwhoadministered,
    this.medicationLabelMedicationinoriginalpackaging,
  });

  factory MedicationSection.fromJson(Map<String, dynamic> json) => new MedicationSection(
    medicationLabelFuthercomments: json["medication_label_futhercomments"],
    medicationLabelTimes: json["medication_label_times"],
    administeredMedicationHistorySubMedicationsPageTitle: json["administered_medication_history_sub_medications_page_title"],
    medicationLabelDosageofmedication: json["medication_label_dosageofmedication"],
    medicationLabelCreateHelpertext: json["medication_label_create_helpertext"],
    navMedicationAction: json["nav_medication_action"],
    medicationLabelStaffsignature: json["medication_label_staffsignature"],
    medicationLabelDosageofmedicationPlaceholder: json["medication_label_dosageofmedication_placeholder"],
    medicationLabelStaffchecksignature: json["medication_label_staffchecksignature"],
    navMedicationmanagementDchartOptionLabelAcknowledgedHistory: json["nav_medicationmanagement_dchart_option_label_acknowledged_history"],
    administeredMedicationSubMedicationsCloneTitle: json["administered_medication_sub_medications_clone_title"],
    navMedicationmanagementDchartOptionLabelActionEdit: json["nav_medicationmanagement_dchart_option_label_action_edit"],
    medicationLabelMedicalpractitionerShort: json["medication_label_medicalpractitioner_short"],
    medicationLabelTimemedicationwasadministered: json["medication_label_timemedicationwasadministered"],
    medicationLabelInstructions: json["medication_label_instructions"],
    navMedicationActionAddBtn: json["nav_medication_action_add_btn"],
    navMedicationmanagementDchartOptionLabelActionRepeat: json["nav_medicationmanagement_dchart_option_label_action_repeat"],
    medicationLabelExpirydate: json["medication_label_expirydate"],
    navMedicationManageTitle: json["nav_medication_manage_title"],
    navMedicationmanagementDchartOptionLabelPendingHistory: json["nav_medicationmanagement_dchart_option_label_pending_history"],
    medicationMainTitle: json["medication_main_title"],
    medicationLabelHastheparentbeennotified: json["medication_label_hastheparentbeennotified"],
    navMedicationActionIndexTitle: json["nav_medication_action_index_title"],
    medicationLabelActionstaffchecksignature: json["medication_label_actionstaffchecksignature"],
    navMedicationmanagementDchartOptionLabelActionAdd: json["nav_medicationmanagement_dchart_option_label_action_add"],
    medicationLabelMedicationinoriginalpackagingShort: json["medication_label_medicationinoriginalpackaging_short"],
    navMedicationAcknowledge: json["nav_medication_acknowledge"],
    administeredMedicationSubMedicationsCloneBtnTitle: json["administered_medication_sub_medications_clone_btn_title"],
    medicationLabelNameofmedication: json["medication_label_nameofmedication"],
    medicationLabelLastadministered: json["medication_label_lastadministered"],
    medicationLabelStaffwitness: json["medication_label_staffwitness"],
    medicationLabelDossgivenbystaff: json["medication_label_dossgivenbystaff"],
    navMedicationActionHistoryBtn: json["nav_medication_action_history_btn"],
    medicationLabelMedicalpractitioner: json["medication_label_medicalpractitioner"],
    medicationLabelMethodtobeadministered: json["medication_label_methodtobeadministered"],
    medicationLabelEndingon: json["medication_label_endingon"],
    medicationLabelActionstaffsignature: json["medication_label_actionstaffsignature"],
    medicationLabelMaindate: json["medication_label_maindate"],
    medicationLabelMedicationadministered: json["medication_label_medicationadministered"],
    medicationLabelReasonformedication: json["medication_label_reasonformedication"],
    administeredMedicationHistorySubMedicationsTitle: json["administered_medication_history_sub_medications_title"],
    navMedicationManage: json["nav_medication_manage"],
    medicationLabelComments: json["medication_label_comments"],
    administeredMedicationMainTitle: json["administered_medication_main_title"],
    navMedicationmanagementDchartOptionLabelActionHistory: json["nav_medicationmanagement_dchart_option_label_action_history"],
    medicationLabelAcknowledge: json["medication_label_acknowledge"],
    medicationLabelMethodtobeadministeredPlaceholder: json["medication_label_methodtobeadministered_placeholder"],
    administeredMedicationMainDateTitle: json["administered_medication_main_date_title"],
    medicationLabelParentsignature: json["medication_label_parentsignature"],
    medicationLabelStorage: json["medication_label_storage"],
    medicationLabelDateprescribed: json["medication_label_dateprescribed"],
    medicationLabelStartingon: json["medication_label_startingon"],
    medicationLabelStaffwhoadministered: json["medication_label_staffwhoadministered"],
    medicationLabelMedicationinoriginalpackaging: json["medication_label_medicationinoriginalpackaging"],
  );

  Map<String, dynamic> toJson() => {
    "medication_label_futhercomments": medicationLabelFuthercomments,
    "medication_label_times": medicationLabelTimes,
    "administered_medication_history_sub_medications_page_title": administeredMedicationHistorySubMedicationsPageTitle,
    "medication_label_dosageofmedication": medicationLabelDosageofmedication,
    "medication_label_create_helpertext": medicationLabelCreateHelpertext,
    "nav_medication_action": navMedicationAction,
    "medication_label_staffsignature": medicationLabelStaffsignature,
    "medication_label_dosageofmedication_placeholder": medicationLabelDosageofmedicationPlaceholder,
    "medication_label_staffchecksignature": medicationLabelStaffchecksignature,
    "nav_medicationmanagement_dchart_option_label_acknowledged_history": navMedicationmanagementDchartOptionLabelAcknowledgedHistory,
    "administered_medication_sub_medications_clone_title": administeredMedicationSubMedicationsCloneTitle,
    "nav_medicationmanagement_dchart_option_label_action_edit": navMedicationmanagementDchartOptionLabelActionEdit,
    "medication_label_medicalpractitioner_short": medicationLabelMedicalpractitionerShort,
    "medication_label_timemedicationwasadministered": medicationLabelTimemedicationwasadministered,
    "medication_label_instructions": medicationLabelInstructions,
    "nav_medication_action_add_btn": navMedicationActionAddBtn,
    "nav_medicationmanagement_dchart_option_label_action_repeat": navMedicationmanagementDchartOptionLabelActionRepeat,
    "medication_label_expirydate": medicationLabelExpirydate,
    "nav_medication_manage_title": navMedicationManageTitle,
    "nav_medicationmanagement_dchart_option_label_pending_history": navMedicationmanagementDchartOptionLabelPendingHistory,
    "medication_main_title": medicationMainTitle,
    "medication_label_hastheparentbeennotified": medicationLabelHastheparentbeennotified,
    "nav_medication_action_index_title": navMedicationActionIndexTitle,
    "medication_label_actionstaffchecksignature": medicationLabelActionstaffchecksignature,
    "nav_medicationmanagement_dchart_option_label_action_add": navMedicationmanagementDchartOptionLabelActionAdd,
    "medication_label_medicationinoriginalpackaging_short": medicationLabelMedicationinoriginalpackagingShort,
    "nav_medication_acknowledge": navMedicationAcknowledge,
    "administered_medication_sub_medications_clone_btn_title": administeredMedicationSubMedicationsCloneBtnTitle,
    "medication_label_nameofmedication": medicationLabelNameofmedication,
    "medication_label_lastadministered": medicationLabelLastadministered,
    "medication_label_staffwitness": medicationLabelStaffwitness,
    "medication_label_dossgivenbystaff": medicationLabelDossgivenbystaff,
    "nav_medication_action_history_btn": navMedicationActionHistoryBtn,
    "medication_label_medicalpractitioner": medicationLabelMedicalpractitioner,
    "medication_label_methodtobeadministered": medicationLabelMethodtobeadministered,
    "medication_label_endingon": medicationLabelEndingon,
    "medication_label_actionstaffsignature": medicationLabelActionstaffsignature,
    "medication_label_maindate": medicationLabelMaindate,
    "medication_label_medicationadministered": medicationLabelMedicationadministered,
    "medication_label_reasonformedication": medicationLabelReasonformedication,
    "administered_medication_history_sub_medications_title": administeredMedicationHistorySubMedicationsTitle,
    "nav_medication_manage": navMedicationManage,
    "medication_label_comments": medicationLabelComments,
    "administered_medication_main_title": administeredMedicationMainTitle,
    "nav_medicationmanagement_dchart_option_label_action_history": navMedicationmanagementDchartOptionLabelActionHistory,
    "medication_label_acknowledge": medicationLabelAcknowledge,
    "medication_label_methodtobeadministered_placeholder": medicationLabelMethodtobeadministeredPlaceholder,
    "administered_medication_main_date_title": administeredMedicationMainDateTitle,
    "medication_label_parentsignature": medicationLabelParentsignature,
    "medication_label_storage": medicationLabelStorage,
    "medication_label_dateprescribed": medicationLabelDateprescribed,
    "medication_label_startingon": medicationLabelStartingon,
    "medication_label_staffwhoadministered": medicationLabelStaffwhoadministered,
    "medication_label_medicationinoriginalpackaging": medicationLabelMedicationinoriginalpackaging,
  };
}

class NavigationSet {
  String navKioskAttendance;
  String navStaffAttendanceManualAttendance;
  String navEvents;
  String navCentermanagementJournal;
  String navStaffAttendance;
  String navLogs;
  String navKioskGuardian;
  String navCentermanagementJourney;
  String navCurriculumQip;
  String navCentermanagementLearningsetMain;
  String navPrivacyPolicy;
  String navDocuments;
  String navKioskStaffAttendanceMenuLable;
  String navCentermanagementPractices;
  String navUserManagementPincode;
  String navCentermanagementPrinciples;
  String navStaffDocument;
  String navStaffAttendanceReport;
  String navCentreSettings;
  String navDashboard;
  String navUserManagement;
  String navKioskReports;
  String navCentermanagementNotification;
  String navMessagePortal;
  String navUserManagementStaff;
  String navReportsNewsfeeds;
  String navReportsDailychart;
  String navKioskDashboard;
  String navCentermanagementPostfeeds;
  String navCentermanagementDailychart;
  String navPolicies;
  String navReportsReportsModule;
  String navEmailLog;
  String navCentermanagementRoom;
  String navNewsletter;
  String navUserManagementGuardian;
  String navUserSettings;
  String navReportsOutcome;
  String navActivities;
  String navChildManagement;
  String navKiosk;
  String navCentermanagementLearningset;
  String navKioskTimeline;
  String navKioskWeeklytimesheetReport;
  String navActivitylog;
  String navKioskChildAttendance;
  String navReportsJournal;
  String navCentermanagementFocusPlaning;
  String navReports;
  String navCurriculum;
  String navUserManagementParent;
  String navKioskStaffManualAttendance;
  String navKioskSettings;
  String navKioskMainManualAttendanceManage;
  String navKioskPincode;
  String navKioskChildReports;
  String navStaffAttendanceStaffActivities;
  String navCentermanagement;
  String navKioskSummary;
  String navCentermanagementStory;
  String navUahl;
  String navCentermanagementSubjectareas;
  String navReportsChildAllergies;
  String navReportsPortfolio;
  String navReportsJourney;
  String navGeneralforms;
  String navReportsLearningTag;
  String navStaffDocumentSingle;

  NavigationSet({
    this.navKioskAttendance,
    this.navStaffAttendanceManualAttendance,
    this.navEvents,
    this.navCentermanagementJournal,
    this.navStaffAttendance,
    this.navLogs,
    this.navKioskGuardian,
    this.navCentermanagementJourney,
    this.navCurriculumQip,
    this.navCentermanagementLearningsetMain,
    this.navPrivacyPolicy,
    this.navDocuments,
    this.navKioskStaffAttendanceMenuLable,
    this.navCentermanagementPractices,
    this.navUserManagementPincode,
    this.navCentermanagementPrinciples,
    this.navStaffDocument,
    this.navStaffAttendanceReport,
    this.navCentreSettings,
    this.navDashboard,
    this.navUserManagement,
    this.navKioskReports,
    this.navCentermanagementNotification,
    this.navMessagePortal,
    this.navUserManagementStaff,
    this.navReportsNewsfeeds,
    this.navReportsDailychart,
    this.navKioskDashboard,
    this.navCentermanagementPostfeeds,
    this.navCentermanagementDailychart,
    this.navPolicies,
    this.navReportsReportsModule,
    this.navEmailLog,
    this.navCentermanagementRoom,
    this.navNewsletter,
    this.navUserManagementGuardian,
    this.navUserSettings,
    this.navReportsOutcome,
    this.navActivities,
    this.navChildManagement,
    this.navKiosk,
    this.navCentermanagementLearningset,
    this.navKioskTimeline,
    this.navKioskWeeklytimesheetReport,
    this.navActivitylog,
    this.navKioskChildAttendance,
    this.navReportsJournal,
    this.navCentermanagementFocusPlaning,
    this.navReports,
    this.navCurriculum,
    this.navUserManagementParent,
    this.navKioskStaffManualAttendance,
    this.navKioskSettings,
    this.navKioskMainManualAttendanceManage,
    this.navKioskPincode,
    this.navKioskChildReports,
    this.navStaffAttendanceStaffActivities,
    this.navCentermanagement,
    this.navKioskSummary,
    this.navCentermanagementStory,
    this.navUahl,
    this.navCentermanagementSubjectareas,
    this.navReportsChildAllergies,
    this.navReportsPortfolio,
    this.navReportsJourney,
    this.navGeneralforms,
    this.navReportsLearningTag,
    this.navStaffDocumentSingle,
  });

  factory NavigationSet.fromJson(Map<String, dynamic> json) => new NavigationSet(
    navKioskAttendance: json["nav_kiosk_attendance"],
    navStaffAttendanceManualAttendance: json["nav_staff_attendance_manual_attendance"],
    navEvents: json["nav_events"],
    navCentermanagementJournal: json["nav_centermanagement_journal"],
    navStaffAttendance: json["nav_staff_attendance"],
    navLogs: json["nav_logs"],
    navKioskGuardian: json["nav_kiosk_guardian"],
    navCentermanagementJourney: json["nav_centermanagement_journey"],
    navCurriculumQip: json["nav_curriculum_QIP"],
    navCentermanagementLearningsetMain: json["nav_centermanagement_learningset_main"],
    navPrivacyPolicy: json["nav_privacy_policy"],
    navDocuments: json["nav_documents"],
    navKioskStaffAttendanceMenuLable: json["nav_kiosk_staff_attendance_menu_lable"],
    navCentermanagementPractices: json["nav_centermanagement_practices"],
    navUserManagementPincode: json["nav_user_management_pincode"],
    navCentermanagementPrinciples: json["nav_centermanagement_principles"],
    navStaffDocument: json["nav_staff_document"],
    navStaffAttendanceReport: json["nav_staff_attendance_report"],
    navCentreSettings: json["nav_centre_settings"],
    navDashboard: json["nav_dashboard"],
    navUserManagement: json["nav_user_management"],
    navKioskReports: json["nav_kiosk_reports"],
    navCentermanagementNotification: json["nav_centermanagement_notification"],
    navMessagePortal: json["nav_message_portal"],
    navUserManagementStaff: json["nav_user_management_staff"],
    navReportsNewsfeeds: json["nav_reports_newsfeeds"],
    navReportsDailychart: json["nav_reports_dailychart"],
    navKioskDashboard: json["nav_kiosk_dashboard"],
    navCentermanagementPostfeeds: json["nav_centermanagement_postfeeds"],
    navCentermanagementDailychart: json["nav_centermanagement_dailychart"],
    navPolicies: json["nav_policies"],
    navReportsReportsModule: json["nav_reports_reports_module"],
    navEmailLog: json["nav_emailLog"],
    navCentermanagementRoom: json["nav_centermanagement_room"],
    navNewsletter: json["nav_newsletter"],
    navUserManagementGuardian: json["nav_user_management_guardian"],
    navUserSettings: json["nav_user_settings"],
    navReportsOutcome: json["nav_reports_outcome"],
    navActivities: json["nav_activities"],
    navChildManagement: json["nav_child_management"],
    navKiosk: json["nav_kiosk"],
    navCentermanagementLearningset: json["nav_centermanagement_learningset"],
    navKioskTimeline: json["nav_kiosk_timeline"],
    navKioskWeeklytimesheetReport: json["nav_kiosk_weeklytimesheet_report"],
    navActivitylog: json["nav_activitylog"],
    navKioskChildAttendance: json["nav_kiosk_child_attendance"],
    navReportsJournal: json["nav_reports_journal"],
    navCentermanagementFocusPlaning: json["nav_centermanagement_focus_planing"],
    navReports: json["nav_reports"],
    navCurriculum: json["nav_curriculum"],
    navUserManagementParent: json["nav_user_management_parent"],
    navKioskStaffManualAttendance: json["nav_kiosk_staff_manual_attendance"],
    navKioskSettings: json["nav_kiosk_settings"],
    navKioskMainManualAttendanceManage: json["nav_kiosk_main_manual_attendance_manage"],
    navKioskPincode: json["nav_kiosk_pincode"],
    navKioskChildReports: json["nav_kiosk_child_reports"],
    navStaffAttendanceStaffActivities: json["nav_staff_attendance_staff_activities"],
    navCentermanagement: json["nav_centermanagement"],
    navKioskSummary: json["nav_kiosk_summary"],
    navCentermanagementStory: json["nav_centermanagement_story"],
    navUahl: json["nav_uahl"],
    navCentermanagementSubjectareas: json["nav_centermanagement_subjectareas"],
    navReportsChildAllergies: json["nav_reports_child_allergies"],
    navReportsPortfolio: json["nav_reports_portfolio"],
    navReportsJourney: json["nav_reports_journey"],
    navGeneralforms: json["nav_generalforms"],
    navReportsLearningTag: json["nav_reports_learning_tag"],
    navStaffDocumentSingle: json["nav_staff_document_single"],
  );

  Map<String, dynamic> toJson() => {
    "nav_kiosk_attendance": navKioskAttendance,
    "nav_staff_attendance_manual_attendance": navStaffAttendanceManualAttendance,
    "nav_events": navEvents,
    "nav_centermanagement_journal": navCentermanagementJournal,
    "nav_staff_attendance": navStaffAttendance,
    "nav_logs": navLogs,
    "nav_kiosk_guardian": navKioskGuardian,
    "nav_centermanagement_journey": navCentermanagementJourney,
    "nav_curriculum_QIP": navCurriculumQip,
    "nav_centermanagement_learningset_main": navCentermanagementLearningsetMain,
    "nav_privacy_policy": navPrivacyPolicy,
    "nav_documents": navDocuments,
    "nav_kiosk_staff_attendance_menu_lable": navKioskStaffAttendanceMenuLable,
    "nav_centermanagement_practices": navCentermanagementPractices,
    "nav_user_management_pincode": navUserManagementPincode,
    "nav_centermanagement_principles": navCentermanagementPrinciples,
    "nav_staff_document": navStaffDocument,
    "nav_staff_attendance_report": navStaffAttendanceReport,
    "nav_centre_settings": navCentreSettings,
    "nav_dashboard": navDashboard,
    "nav_user_management": navUserManagement,
    "nav_kiosk_reports": navKioskReports,
    "nav_centermanagement_notification": navCentermanagementNotification,
    "nav_message_portal": navMessagePortal,
    "nav_user_management_staff": navUserManagementStaff,
    "nav_reports_newsfeeds": navReportsNewsfeeds,
    "nav_reports_dailychart": navReportsDailychart,
    "nav_kiosk_dashboard": navKioskDashboard,
    "nav_centermanagement_postfeeds": navCentermanagementPostfeeds,
    "nav_centermanagement_dailychart": navCentermanagementDailychart,
    "nav_policies": navPolicies,
    "nav_reports_reports_module": navReportsReportsModule,
    "nav_emailLog": navEmailLog,
    "nav_centermanagement_room": navCentermanagementRoom,
    "nav_newsletter": navNewsletter,
    "nav_user_management_guardian": navUserManagementGuardian,
    "nav_user_settings": navUserSettings,
    "nav_reports_outcome": navReportsOutcome,
    "nav_activities": navActivities,
    "nav_child_management": navChildManagement,
    "nav_kiosk": navKiosk,
    "nav_centermanagement_learningset": navCentermanagementLearningset,
    "nav_kiosk_timeline": navKioskTimeline,
    "nav_kiosk_weeklytimesheet_report": navKioskWeeklytimesheetReport,
    "nav_activitylog": navActivitylog,
    "nav_kiosk_child_attendance": navKioskChildAttendance,
    "nav_reports_journal": navReportsJournal,
    "nav_centermanagement_focus_planing": navCentermanagementFocusPlaning,
    "nav_reports": navReports,
    "nav_curriculum": navCurriculum,
    "nav_user_management_parent": navUserManagementParent,
    "nav_kiosk_staff_manual_attendance": navKioskStaffManualAttendance,
    "nav_kiosk_settings": navKioskSettings,
    "nav_kiosk_main_manual_attendance_manage": navKioskMainManualAttendanceManage,
    "nav_kiosk_pincode": navKioskPincode,
    "nav_kiosk_child_reports": navKioskChildReports,
    "nav_staff_attendance_staff_activities": navStaffAttendanceStaffActivities,
    "nav_centermanagement": navCentermanagement,
    "nav_kiosk_summary": navKioskSummary,
    "nav_centermanagement_story": navCentermanagementStory,
    "nav_uahl": navUahl,
    "nav_centermanagement_subjectareas": navCentermanagementSubjectareas,
    "nav_reports_child_allergies": navReportsChildAllergies,
    "nav_reports_portfolio": navReportsPortfolio,
    "nav_reports_journey": navReportsJourney,
    "nav_generalforms": navGeneralforms,
    "nav_reports_learning_tag": navReportsLearningTag,
    "nav_staff_document_single": navStaffDocumentSingle,
  };
}

class NewsfeedSection {
  String navNewsfeedPostLabelDescription;
  String navNewsfeedPostPlaceholderTitle;
  String navNewsfeedWarningMessage;
  String navNewsfeedLabel;
  String navNewsfeedPostRestoreLabel;
  String navNewsfeedPostLabelPublishRadio;
  String navNewsfeedPostLabelTagRoom;
  String navNewsfeedPostLabelDraftRadio;
  String navNewsfeedTabWall;
  String navNewsfeedIndexTitle;
  String navNewsfeedTabManage;
  String navNewsfeedManageIndexTitle;
  String navNewsfeedEditTitle;
  String navNewsfeedPostButton;
  String navNewsfeedPostUpdateButton;
  String navNewsfeedPostDeleteLabel;
  String navNewsfeedPostPlaceholderDescription;
  String navNewsfeedPostLabelTitle;
  String navNewsfeedPostLabelTagChild;

  NewsfeedSection({
    this.navNewsfeedPostLabelDescription,
    this.navNewsfeedPostPlaceholderTitle,
    this.navNewsfeedWarningMessage,
    this.navNewsfeedLabel,
    this.navNewsfeedPostRestoreLabel,
    this.navNewsfeedPostLabelPublishRadio,
    this.navNewsfeedPostLabelTagRoom,
    this.navNewsfeedPostLabelDraftRadio,
    this.navNewsfeedTabWall,
    this.navNewsfeedIndexTitle,
    this.navNewsfeedTabManage,
    this.navNewsfeedManageIndexTitle,
    this.navNewsfeedEditTitle,
    this.navNewsfeedPostButton,
    this.navNewsfeedPostUpdateButton,
    this.navNewsfeedPostDeleteLabel,
    this.navNewsfeedPostPlaceholderDescription,
    this.navNewsfeedPostLabelTitle,
    this.navNewsfeedPostLabelTagChild,
  });

  factory NewsfeedSection.fromJson(Map<String, dynamic> json) => new NewsfeedSection(
    navNewsfeedPostLabelDescription: json["nav_newsfeed_post_label_description"],
    navNewsfeedPostPlaceholderTitle: json["nav_newsfeed_post_placeholder_title"],
    navNewsfeedWarningMessage: json["nav_newsfeed_warning_message"],
    navNewsfeedLabel: json["nav_newsfeed_label"],
    navNewsfeedPostRestoreLabel: json["nav_newsfeed_post_restore_label"],
    navNewsfeedPostLabelPublishRadio: json["nav_newsfeed_post_label_publish_radio"],
    navNewsfeedPostLabelTagRoom: json["nav_newsfeed_post_label_tag_room"],
    navNewsfeedPostLabelDraftRadio: json["nav_newsfeed_post_label_draft_radio"],
    navNewsfeedTabWall: json["nav_newsfeed_tab_wall"],
    navNewsfeedIndexTitle: json["nav_newsfeed_index_title"],
    navNewsfeedTabManage: json["nav_newsfeed_tab_manage"],
    navNewsfeedManageIndexTitle: json["nav_newsfeed_manage_index_title"],
    navNewsfeedEditTitle: json["nav_newsfeed_edit_title"],
    navNewsfeedPostButton: json["nav_newsfeed_post_button"],
    navNewsfeedPostUpdateButton: json["nav_newsfeed_post_update_button"],
    navNewsfeedPostDeleteLabel: json["nav_newsfeed_post_delete_label"],
    navNewsfeedPostPlaceholderDescription: json["nav_newsfeed_post_placeholder_description"],
    navNewsfeedPostLabelTitle: json["nav_newsfeed_post_label_title"],
    navNewsfeedPostLabelTagChild: json["nav_newsfeed_post_label_tag_child"],
  );

  Map<String, dynamic> toJson() => {
    "nav_newsfeed_post_label_description": navNewsfeedPostLabelDescription,
    "nav_newsfeed_post_placeholder_title": navNewsfeedPostPlaceholderTitle,
    "nav_newsfeed_warning_message": navNewsfeedWarningMessage,
    "nav_newsfeed_label": navNewsfeedLabel,
    "nav_newsfeed_post_restore_label": navNewsfeedPostRestoreLabel,
    "nav_newsfeed_post_label_publish_radio": navNewsfeedPostLabelPublishRadio,
    "nav_newsfeed_post_label_tag_room": navNewsfeedPostLabelTagRoom,
    "nav_newsfeed_post_label_draft_radio": navNewsfeedPostLabelDraftRadio,
    "nav_newsfeed_tab_wall": navNewsfeedTabWall,
    "nav_newsfeed_index_title": navNewsfeedIndexTitle,
    "nav_newsfeed_tab_manage": navNewsfeedTabManage,
    "nav_newsfeed_manage_index_title": navNewsfeedManageIndexTitle,
    "nav_newsfeed_edit_title": navNewsfeedEditTitle,
    "nav_newsfeed_post_button": navNewsfeedPostButton,
    "nav_newsfeed_post_update_button": navNewsfeedPostUpdateButton,
    "nav_newsfeed_post_delete_label": navNewsfeedPostDeleteLabel,
    "nav_newsfeed_post_placeholder_description": navNewsfeedPostPlaceholderDescription,
    "nav_newsfeed_post_label_title": navNewsfeedPostLabelTitle,
    "nav_newsfeed_post_label_tag_child": navNewsfeedPostLabelTagChild,
  };
}

class NewsletterSection {
  String navNewsletterDraftTitle;
  String navNewsletterCreateTitle;
  String navNewsletterIndex;
  String navNewsletterCreatePreviewLabel;
  String navNewsletterButtonAddnew;
  String navNewsletterIndexTitle;
  String navNewsletterLabel;
  String navNewsletterMessageEmpty;
  String navNewsletterModalTitle;

  NewsletterSection({
    this.navNewsletterDraftTitle,
    this.navNewsletterCreateTitle,
    this.navNewsletterIndex,
    this.navNewsletterCreatePreviewLabel,
    this.navNewsletterButtonAddnew,
    this.navNewsletterIndexTitle,
    this.navNewsletterLabel,
    this.navNewsletterMessageEmpty,
    this.navNewsletterModalTitle,
  });

  factory NewsletterSection.fromJson(Map<String, dynamic> json) => new NewsletterSection(
    navNewsletterDraftTitle: json["nav_newsletter_draft_title"],
    navNewsletterCreateTitle: json["nav_newsletter_create_title"],
    navNewsletterIndex: json["nav_newsletter_index"],
    navNewsletterCreatePreviewLabel: json["nav_newsletter_create_preview_label"],
    navNewsletterButtonAddnew: json["nav_newsletter_button_addnew"],
    navNewsletterIndexTitle: json["nav_newsletter_index_title"],
    navNewsletterLabel: json["nav_newsletter_label"],
    navNewsletterMessageEmpty: json["nav_newsletter_message_empty"],
    navNewsletterModalTitle: json["nav_newsletter_modal_title"],
  );

  Map<String, dynamic> toJson() => {
    "nav_newsletter_draft_title": navNewsletterDraftTitle,
    "nav_newsletter_create_title": navNewsletterCreateTitle,
    "nav_newsletter_index": navNewsletterIndex,
    "nav_newsletter_create_preview_label": navNewsletterCreatePreviewLabel,
    "nav_newsletter_button_addnew": navNewsletterButtonAddnew,
    "nav_newsletter_index_title": navNewsletterIndexTitle,
    "nav_newsletter_label": navNewsletterLabel,
    "nav_newsletter_message_empty": navNewsletterMessageEmpty,
    "nav_newsletter_modal_title": navNewsletterModalTitle,
  };
}

class NonPrescribedMedicationSection {
  String nonprescribedMedicationTitle;
  String nonprescribedMedicationTitlePlural;
  String navCentermanagementNonprescribedMedicationNotificationLabel;
  String nonprescribedMedicationLabelCircumstances;
  String nonprescribedMedicationLabelDateAuthorized;
  String nonprescribedMedicationLabelParentsignature;
  String nonprescribedMedicationLabelMedicationToAdministered;
  String nonprescribedMedicationLabelHelpertext;
  String nonprescribedMedicationLabelGiveUnderCircumstances;

  NonPrescribedMedicationSection({
    this.nonprescribedMedicationTitle,
    this.nonprescribedMedicationTitlePlural,
    this.navCentermanagementNonprescribedMedicationNotificationLabel,
    this.nonprescribedMedicationLabelCircumstances,
    this.nonprescribedMedicationLabelDateAuthorized,
    this.nonprescribedMedicationLabelParentsignature,
    this.nonprescribedMedicationLabelMedicationToAdministered,
    this.nonprescribedMedicationLabelHelpertext,
    this.nonprescribedMedicationLabelGiveUnderCircumstances,
  });

  factory NonPrescribedMedicationSection.fromJson(Map<String, dynamic> json) => new NonPrescribedMedicationSection(
    nonprescribedMedicationTitle: json["nonprescribed_medication_title"],
    nonprescribedMedicationTitlePlural: json["nonprescribed_medication_title_plural"],
    navCentermanagementNonprescribedMedicationNotificationLabel: json["nav_centermanagement_nonprescribed_medication_notification_label"],
    nonprescribedMedicationLabelCircumstances: json["nonprescribed_medication_label_circumstances"],
    nonprescribedMedicationLabelDateAuthorized: json["nonprescribed_medication_label_date_authorized"],
    nonprescribedMedicationLabelParentsignature: json["nonprescribed_medication_label_parentsignature"],
    nonprescribedMedicationLabelMedicationToAdministered: json["nonprescribed_medication_label_medication_to_administered"],
    nonprescribedMedicationLabelHelpertext: json["nonprescribed_medication_label_helpertext"],
    nonprescribedMedicationLabelGiveUnderCircumstances: json["nonprescribed_medication_label_give_under_circumstances"],
  );

  Map<String, dynamic> toJson() => {
    "nonprescribed_medication_title": nonprescribedMedicationTitle,
    "nonprescribed_medication_title_plural": nonprescribedMedicationTitlePlural,
    "nav_centermanagement_nonprescribed_medication_notification_label": navCentermanagementNonprescribedMedicationNotificationLabel,
    "nonprescribed_medication_label_circumstances": nonprescribedMedicationLabelCircumstances,
    "nonprescribed_medication_label_date_authorized": nonprescribedMedicationLabelDateAuthorized,
    "nonprescribed_medication_label_parentsignature": nonprescribedMedicationLabelParentsignature,
    "nonprescribed_medication_label_medication_to_administered": nonprescribedMedicationLabelMedicationToAdministered,
    "nonprescribed_medication_label_helpertext": nonprescribedMedicationLabelHelpertext,
    "nonprescribed_medication_label_give_under_circumstances": nonprescribedMedicationLabelGiveUnderCircumstances,
  };
}

class Notification {
  String navNotificationModalCommonLabel;
  String navNotificationModalWebLabel;
  String navNotificationModalTitle;
  String navNotificationModalMobileLabel;
  String navNotificationResetButtonText;
  String navNotificationResetContentText;
  String navNotificationLabel;
  String navNotificationResetLabel;
  String navNotificationMessageEmpty;
  String navNotificationIndexTitle;

  Notification({
    this.navNotificationModalCommonLabel,
    this.navNotificationModalWebLabel,
    this.navNotificationModalTitle,
    this.navNotificationModalMobileLabel,
    this.navNotificationResetButtonText,
    this.navNotificationResetContentText,
    this.navNotificationLabel,
    this.navNotificationResetLabel,
    this.navNotificationMessageEmpty,
    this.navNotificationIndexTitle,
  });

  factory Notification.fromJson(Map<String, dynamic> json) => new Notification(
    navNotificationModalCommonLabel: json["nav_notification_modal_common_label"],
    navNotificationModalWebLabel: json["nav_notification_modal_web_label"],
    navNotificationModalTitle: json["nav_notification_modal_title"],
    navNotificationModalMobileLabel: json["nav_notification_modal_mobile_label"],
    navNotificationResetButtonText: json["nav_notification_reset_button_text"],
    navNotificationResetContentText: json["nav_notification_reset_content_text"],
    navNotificationLabel: json["nav_notification_label"],
    navNotificationResetLabel: json["nav_notification_reset_label"],
    navNotificationMessageEmpty: json["nav_notification_message_empty"],
    navNotificationIndexTitle: json["nav_notification_index_title"],
  );

  Map<String, dynamic> toJson() => {
    "nav_notification_modal_common_label": navNotificationModalCommonLabel,
    "nav_notification_modal_web_label": navNotificationModalWebLabel,
    "nav_notification_modal_title": navNotificationModalTitle,
    "nav_notification_modal_mobile_label": navNotificationModalMobileLabel,
    "nav_notification_reset_button_text": navNotificationResetButtonText,
    "nav_notification_reset_content_text": navNotificationResetContentText,
    "nav_notification_label": navNotificationLabel,
    "nav_notification_reset_label": navNotificationResetLabel,
    "nav_notification_message_empty": navNotificationMessageEmpty,
    "nav_notification_index_title": navNotificationIndexTitle,
  };
}

class PoliciesSection {
  String navPolicieMessageEmpty;
  String navPolicieModalTitle;
  String navPolicieLabel;
  String navPolicieButtonAddnew;
  String navPolicieIndexTitle;

  PoliciesSection({
    this.navPolicieMessageEmpty,
    this.navPolicieModalTitle,
    this.navPolicieLabel,
    this.navPolicieButtonAddnew,
    this.navPolicieIndexTitle,
  });

  factory PoliciesSection.fromJson(Map<String, dynamic> json) => new PoliciesSection(
    navPolicieMessageEmpty: json["nav_policie_message_empty"],
    navPolicieModalTitle: json["nav_policie_modal_title"],
    navPolicieLabel: json["nav_policie_label"],
    navPolicieButtonAddnew: json["nav_policie_button_addnew"],
    navPolicieIndexTitle: json["nav_policie_index_title"],
  );

  Map<String, dynamic> toJson() => {
    "nav_policie_message_empty": navPolicieMessageEmpty,
    "nav_policie_modal_title": navPolicieModalTitle,
    "nav_policie_label": navPolicieLabel,
    "nav_policie_button_addnew": navPolicieButtonAddnew,
    "nav_policie_index_title": navPolicieIndexTitle,
  };
}

class PracticesSection {
  String navPracticesButtonAdd;
  String navPracticesLabel;
  String navPracticesIndexTitle;

  PracticesSection({
    this.navPracticesButtonAdd,
    this.navPracticesLabel,
    this.navPracticesIndexTitle,
  });

  factory PracticesSection.fromJson(Map<String, dynamic> json) => new PracticesSection(
    navPracticesButtonAdd: json["nav_practices_button_add"],
    navPracticesLabel: json["nav_practices_label"],
    navPracticesIndexTitle: json["nav_practices_index_title"],
  );

  Map<String, dynamic> toJson() => {
    "nav_practices_button_add": navPracticesButtonAdd,
    "nav_practices_label": navPracticesLabel,
    "nav_practices_index_title": navPracticesIndexTitle,
  };
}

class PrinciplesSection {
  String navPrinciplesLabel;
  String navPrinciplesButtonAdd;
  String navPrinciplesIndexTitle;

  PrinciplesSection({
    this.navPrinciplesLabel,
    this.navPrinciplesButtonAdd,
    this.navPrinciplesIndexTitle,
  });

  factory PrinciplesSection.fromJson(Map<String, dynamic> json) => new PrinciplesSection(
    navPrinciplesLabel: json["nav_principles_label"],
    navPrinciplesButtonAdd: json["nav_principles_button_add"],
    navPrinciplesIndexTitle: json["nav_principles_index_title"],
  );

  Map<String, dynamic> toJson() => {
    "nav_principles_label": navPrinciplesLabel,
    "nav_principles_button_add": navPrinciplesButtonAdd,
    "nav_principles_index_title": navPrinciplesIndexTitle,
  };
}

class PrivacyPolicy {
  String navPrivacyPolicyIndexTitle;

  PrivacyPolicy({
    this.navPrivacyPolicyIndexTitle,
  });

  factory PrivacyPolicy.fromJson(Map<String, dynamic> json) => new PrivacyPolicy(
    navPrivacyPolicyIndexTitle: json["nav_privacy_policy_index_title"],
  );

  Map<String, dynamic> toJson() => {
    "nav_privacy_policy_index_title": navPrivacyPolicyIndexTitle,
  };
}

class PrivateMessageSection {
  String navMessageportalLabelRoom;
  String navMessageportalIndexTitle;
  String navMessageportalNoMessage;
  String navMessageportalModalTitle;
  String navMessageportalLabelCenter;
  String navMessageportalMessageEmpty;
  String navMessageportalLabelParent;
  String navMessageportalButtonNew;
  String navMessageportalLabelStaff;
  String navMessageportalActiveLogLabel;
  String navMessageportalLabel;
  String navMessageportalLabelAdmin;

  PrivateMessageSection({
    this.navMessageportalLabelRoom,
    this.navMessageportalIndexTitle,
    this.navMessageportalNoMessage,
    this.navMessageportalModalTitle,
    this.navMessageportalLabelCenter,
    this.navMessageportalMessageEmpty,
    this.navMessageportalLabelParent,
    this.navMessageportalButtonNew,
    this.navMessageportalLabelStaff,
    this.navMessageportalActiveLogLabel,
    this.navMessageportalLabel,
    this.navMessageportalLabelAdmin,
  });

  factory PrivateMessageSection.fromJson(Map<String, dynamic> json) => new PrivateMessageSection(
    navMessageportalLabelRoom: json["nav_messageportal_label_room"],
    navMessageportalIndexTitle: json["nav_messageportal_index_title"],
    navMessageportalNoMessage: json["nav_messageportal_no_message"],
    navMessageportalModalTitle: json["nav_messageportal_modal_title"],
    navMessageportalLabelCenter: json["nav_messageportal_label_center"],
    navMessageportalMessageEmpty: json["nav_messageportal_message_empty"],
    navMessageportalLabelParent: json["nav_messageportal_label_parent"],
    navMessageportalButtonNew: json["nav_messageportal_button_new"],
    navMessageportalLabelStaff: json["nav_messageportal_label_staff"],
    navMessageportalActiveLogLabel: json["nav_messageportal_active_log_label"],
    navMessageportalLabel: json["nav_messageportal_label"],
    navMessageportalLabelAdmin: json["nav_messageportal_label_admin"],
  );

  Map<String, dynamic> toJson() => {
    "nav_messageportal_label_room": navMessageportalLabelRoom,
    "nav_messageportal_index_title": navMessageportalIndexTitle,
    "nav_messageportal_no_message": navMessageportalNoMessage,
    "nav_messageportal_modal_title": navMessageportalModalTitle,
    "nav_messageportal_label_center": navMessageportalLabelCenter,
    "nav_messageportal_message_empty": navMessageportalMessageEmpty,
    "nav_messageportal_label_parent": navMessageportalLabelParent,
    "nav_messageportal_button_new": navMessageportalButtonNew,
    "nav_messageportal_label_staff": navMessageportalLabelStaff,
    "nav_messageportal_active_log_label": navMessageportalActiveLogLabel,
    "nav_messageportal_label": navMessageportalLabel,
    "nav_messageportal_label_admin": navMessageportalLabelAdmin,
  };
}

class ProgramPlanningSection {
  String plannedExperienceObservationDefValues;
  String labelProgramPlanSharewithparents;
  String navProgramPlanDeleteactivity;
  String labelProgramPlanExperience;
  String navProgramPlanningMainHeading;
  String labelProgramPlanPdfTitleLbl;
  String labelExperiencesTabHelperText;
  String labelProgramPlanDescription;
  String labelProgramPlanPexWeeklyReportHeading;
  String labelProgramPlanSubLinkEmpty;
  String labelProgramPlanRepeats;
  String labelExperiencesTabTitle;
  String labelProgramPlanSharedwithparents;
  String labelProgramPlanEndingDate;
  String labelProgramPlanProgramtitle;
  String labelProgramPlanSubLink;
  String labelProgramPlanStartingDate;
  String labelProgramPlanConvertto;
  String labelProgramPlanObservationfollowuptitle;
  String labelProgramPlanTo;
  String labelProgramPlanPdfProgramLbl;
  String labelProgramPlanClientNotification;
  String labelProgramPlanPExperiencesTitle;
  String labelProgramPlanClientListTabHeading;
  String labelProgramPlanChoosecustomtimerange;
  String labelExperiencesTabFieldtype;
  String labelExperiencesTabCurriculamType;
  String plannedExperienceNewsfeedDefValues;
  String plannedExperienceDynamicField2;
  String labelProgramPlanLsfollowuptitle;
  String navProgramPlanMainHeading;
  String labelProgramPlanFrom;
  String labelProgramPlanPdftypeFilter3;
  String plannedExperienceJournalAdt1;
  String labelProgramPlanAllday;
  String labelProgramPlanPdftypeFilter4;
  String labelExperiencesTabCurriculamFields;
  String plannedExperienceJournalAdt2;
  String labelProgramPlanEducator;
  String labelProgramPlanPallWeeklyReportHeading;
  String labelProgramPlanPdftypeFilter1;
  String labelProgramPlanPdftypeFilter2;
  String plannedExperienceObservationAdt2;
  String labelProgramPlanProgramstatus;
  String labelProgramPlanCreatedSubLinks;
  String labelProgramPlanReportTypeFilter;
  String labelProgramPlanType;
  String plannedExperienceObservationAdt1;
  String labelProgramPlanTitle;
  String labelProgramPlanCompletedProgramsList;
  String labelProgramPlanShareallactivities;
  String labelProgramPlanCreatedby;
  String labelProgramPlanPdftypeFilter4New;
  String programPlanDefaultView;
  String labelProgramPlanNewlearningtags;
  String labelProgramPlanTime;
  String labelProgramPlanPexSareaWeeklyReportHeading;
  String labelProgramPlanStaff;
  String labelProgramPlanDashboardFilter;
  String labelProgramPlanProgramstatusmanuallychange;
  String labelProgramPlanReportHeading;
  String labelProgramPlanPdftypeFilter3New;
  String labelProgramPlanSubjectarea;
  String labelProgramPlanRoom;
  String labelProgramPlanPallSareaWeeklyReportHeading;
  String labelProgramPlanLearningtags;
  String labelProgramPlanJournalfollowuptitle;
  String navProgramPlanning;
  String plannedExperienceJournalDefValues;
  String labelProgramPlanRepeat;
  String navProgramPlanManageactivity;
  String labelProgramPlanChildren;
  String labelProgramPlanClientListHeading;
  String navProgramPlanMainHeadingPlural;

  ProgramPlanningSection({
    this.plannedExperienceObservationDefValues,
    this.labelProgramPlanSharewithparents,
    this.navProgramPlanDeleteactivity,
    this.labelProgramPlanExperience,
    this.navProgramPlanningMainHeading,
    this.labelProgramPlanPdfTitleLbl,
    this.labelExperiencesTabHelperText,
    this.labelProgramPlanDescription,
    this.labelProgramPlanPexWeeklyReportHeading,
    this.labelProgramPlanSubLinkEmpty,
    this.labelProgramPlanRepeats,
    this.labelExperiencesTabTitle,
    this.labelProgramPlanSharedwithparents,
    this.labelProgramPlanEndingDate,
    this.labelProgramPlanProgramtitle,
    this.labelProgramPlanSubLink,
    this.labelProgramPlanStartingDate,
    this.labelProgramPlanConvertto,
    this.labelProgramPlanObservationfollowuptitle,
    this.labelProgramPlanTo,
    this.labelProgramPlanPdfProgramLbl,
    this.labelProgramPlanClientNotification,
    this.labelProgramPlanPExperiencesTitle,
    this.labelProgramPlanClientListTabHeading,
    this.labelProgramPlanChoosecustomtimerange,
    this.labelExperiencesTabFieldtype,
    this.labelExperiencesTabCurriculamType,
    this.plannedExperienceNewsfeedDefValues,
    this.plannedExperienceDynamicField2,
    this.labelProgramPlanLsfollowuptitle,
    this.navProgramPlanMainHeading,
    this.labelProgramPlanFrom,
    this.labelProgramPlanPdftypeFilter3,
    this.plannedExperienceJournalAdt1,
    this.labelProgramPlanAllday,
    this.labelProgramPlanPdftypeFilter4,
    this.labelExperiencesTabCurriculamFields,
    this.plannedExperienceJournalAdt2,
    this.labelProgramPlanEducator,
    this.labelProgramPlanPallWeeklyReportHeading,
    this.labelProgramPlanPdftypeFilter1,
    this.labelProgramPlanPdftypeFilter2,
    this.plannedExperienceObservationAdt2,
    this.labelProgramPlanProgramstatus,
    this.labelProgramPlanCreatedSubLinks,
    this.labelProgramPlanReportTypeFilter,
    this.labelProgramPlanType,
    this.plannedExperienceObservationAdt1,
    this.labelProgramPlanTitle,
    this.labelProgramPlanCompletedProgramsList,
    this.labelProgramPlanShareallactivities,
    this.labelProgramPlanCreatedby,
    this.labelProgramPlanPdftypeFilter4New,
    this.programPlanDefaultView,
    this.labelProgramPlanNewlearningtags,
    this.labelProgramPlanTime,
    this.labelProgramPlanPexSareaWeeklyReportHeading,
    this.labelProgramPlanStaff,
    this.labelProgramPlanDashboardFilter,
    this.labelProgramPlanProgramstatusmanuallychange,
    this.labelProgramPlanReportHeading,
    this.labelProgramPlanPdftypeFilter3New,
    this.labelProgramPlanSubjectarea,
    this.labelProgramPlanRoom,
    this.labelProgramPlanPallSareaWeeklyReportHeading,
    this.labelProgramPlanLearningtags,
    this.labelProgramPlanJournalfollowuptitle,
    this.navProgramPlanning,
    this.plannedExperienceJournalDefValues,
    this.labelProgramPlanRepeat,
    this.navProgramPlanManageactivity,
    this.labelProgramPlanChildren,
    this.labelProgramPlanClientListHeading,
    this.navProgramPlanMainHeadingPlural,
  });

  factory ProgramPlanningSection.fromJson(Map<String, dynamic> json) => new ProgramPlanningSection(
    plannedExperienceObservationDefValues: json["planned_experience_observation_def_values"],
    labelProgramPlanSharewithparents: json["label_program_plan_sharewithparents"],
    navProgramPlanDeleteactivity: json["nav_program_plan_deleteactivity"],
    labelProgramPlanExperience: json["label_program_plan_experience"],
    navProgramPlanningMainHeading: json["nav_program_planning_main_heading"],
    labelProgramPlanPdfTitleLbl: json["label_program_plan_pdf_title_lbl"],
    labelExperiencesTabHelperText: json["label_experiences_tab_helper_text"],
    labelProgramPlanDescription: json["label_program_plan_description"],
    labelProgramPlanPexWeeklyReportHeading: json["label_program_plan_pex_weekly_report_heading"],
    labelProgramPlanSubLinkEmpty: json["label_program_plan_sub_link_empty"],
    labelProgramPlanRepeats: json["label_program_plan_repeats"],
    labelExperiencesTabTitle: json["label_experiences_tab_title"],
    labelProgramPlanSharedwithparents: json["label_program_plan_sharedwithparents"],
    labelProgramPlanEndingDate: json["label_program_plan_ending_date"],
    labelProgramPlanProgramtitle: json["label_program_plan_programtitle"],
    labelProgramPlanSubLink: json["label_program_plan_sub_link"],
    labelProgramPlanStartingDate: json["label_program_plan_starting_date"],
    labelProgramPlanConvertto: json["label_program_plan_convertto"],
    labelProgramPlanObservationfollowuptitle: json["label_program_plan_observationfollowuptitle"],
    labelProgramPlanTo: json["label_program_plan_to"],
    labelProgramPlanPdfProgramLbl: json["label_program_plan_pdf_program_lbl"],
    labelProgramPlanClientNotification: json["label_program_plan_client_notification"],
    labelProgramPlanPExperiencesTitle: json["label_program_plan_p_experiences_title"],
    labelProgramPlanClientListTabHeading: json["label_program_plan_client_list_tab_heading"],
    labelProgramPlanChoosecustomtimerange: json["label_program_plan_choosecustomtimerange"],
    labelExperiencesTabFieldtype: json["label_experiences_tab_fieldtype"],
    labelExperiencesTabCurriculamType: json["label_experiences_tab_curriculam_type"],
    plannedExperienceNewsfeedDefValues: json["planned_experience_newsfeed_def_values"],
    plannedExperienceDynamicField2: json["planned_experience_dynamic_field_2"],
    labelProgramPlanLsfollowuptitle: json["label_program_plan_lsfollowuptitle"],
    navProgramPlanMainHeading: json["nav_program_plan_main_heading"],
    labelProgramPlanFrom: json["label_program_plan_from"],
    labelProgramPlanPdftypeFilter3: json["label_program_plan_pdftype_filter3"],
    plannedExperienceJournalAdt1: json["planned_experience_journal_adt1"],
    labelProgramPlanAllday: json["label_program_plan_allday"],
    labelProgramPlanPdftypeFilter4: json["label_program_plan_pdftype_filter4"],
    labelExperiencesTabCurriculamFields: json["label_experiences_tab_curriculam_fields"],
    plannedExperienceJournalAdt2: json["planned_experience_journal_adt2"],
    labelProgramPlanEducator: json["label_program_plan_educator"],
    labelProgramPlanPallWeeklyReportHeading: json["label_program_plan_pall_weekly_report_heading"],
    labelProgramPlanPdftypeFilter1: json["label_program_plan_pdftype_filter1"],
    labelProgramPlanPdftypeFilter2: json["label_program_plan_pdftype_filter2"],
    plannedExperienceObservationAdt2: json["planned_experience_observation_adt2"],
    labelProgramPlanProgramstatus: json["label_program_plan_programstatus"],
    labelProgramPlanCreatedSubLinks: json["label_program_plan_created_sub_links"],
    labelProgramPlanReportTypeFilter: json["label_program_plan_report_type_filter"],
    labelProgramPlanType: json["label_program_plan_type"],
    plannedExperienceObservationAdt1: json["planned_experience_observation_adt1"],
    labelProgramPlanTitle: json["label_program_plan_title"],
    labelProgramPlanCompletedProgramsList: json["label_program_plan_completed_programs_list"],
    labelProgramPlanShareallactivities: json["label_program_plan_shareallactivities"],
    labelProgramPlanCreatedby: json["label_program_plan_createdby"],
    labelProgramPlanPdftypeFilter4New: json["label_program_plan_pdftype_filter4_new"],
    programPlanDefaultView: json["program_plan_default_view"],
    labelProgramPlanNewlearningtags: json["label_program_plan_newlearningtags"],
    labelProgramPlanTime: json["label_program_plan_time"],
    labelProgramPlanPexSareaWeeklyReportHeading: json["label_program_plan_pex_sarea_weekly_report_heading"],
    labelProgramPlanStaff: json["label_program_plan_staff"],
    labelProgramPlanDashboardFilter: json["label_program_plan_dashboard_filter"],
    labelProgramPlanProgramstatusmanuallychange: json["label_program_plan_programstatusmanuallychange"],
    labelProgramPlanReportHeading: json["label_program_plan_report_heading"],
    labelProgramPlanPdftypeFilter3New: json["label_program_plan_pdftype_filter3_new"],
    labelProgramPlanSubjectarea: json["label_program_plan_subjectarea"],
    labelProgramPlanRoom: json["label_program_plan_room"],
    labelProgramPlanPallSareaWeeklyReportHeading: json["label_program_plan_pall_sarea_weekly_report_heading"],
    labelProgramPlanLearningtags: json["label_program_plan_learningtags"],
    labelProgramPlanJournalfollowuptitle: json["label_program_plan_journalfollowuptitle"],
    navProgramPlanning: json["nav_program_planning"],
    plannedExperienceJournalDefValues: json["planned_experience_journal_def_values"],
    labelProgramPlanRepeat: json["label_program_plan_repeat"],
    navProgramPlanManageactivity: json["nav_program_plan_manageactivity"],
    labelProgramPlanChildren: json["label_program_plan_children"],
    labelProgramPlanClientListHeading: json["label_program_plan_client_list_heading"],
    navProgramPlanMainHeadingPlural: json["nav_program_plan_main_heading_plural"],
  );

  Map<String, dynamic> toJson() => {
    "planned_experience_observation_def_values": plannedExperienceObservationDefValues,
    "label_program_plan_sharewithparents": labelProgramPlanSharewithparents,
    "nav_program_plan_deleteactivity": navProgramPlanDeleteactivity,
    "label_program_plan_experience": labelProgramPlanExperience,
    "nav_program_planning_main_heading": navProgramPlanningMainHeading,
    "label_program_plan_pdf_title_lbl": labelProgramPlanPdfTitleLbl,
    "label_experiences_tab_helper_text": labelExperiencesTabHelperText,
    "label_program_plan_description": labelProgramPlanDescription,
    "label_program_plan_pex_weekly_report_heading": labelProgramPlanPexWeeklyReportHeading,
    "label_program_plan_sub_link_empty": labelProgramPlanSubLinkEmpty,
    "label_program_plan_repeats": labelProgramPlanRepeats,
    "label_experiences_tab_title": labelExperiencesTabTitle,
    "label_program_plan_sharedwithparents": labelProgramPlanSharedwithparents,
    "label_program_plan_ending_date": labelProgramPlanEndingDate,
    "label_program_plan_programtitle": labelProgramPlanProgramtitle,
    "label_program_plan_sub_link": labelProgramPlanSubLink,
    "label_program_plan_starting_date": labelProgramPlanStartingDate,
    "label_program_plan_convertto": labelProgramPlanConvertto,
    "label_program_plan_observationfollowuptitle": labelProgramPlanObservationfollowuptitle,
    "label_program_plan_to": labelProgramPlanTo,
    "label_program_plan_pdf_program_lbl": labelProgramPlanPdfProgramLbl,
    "label_program_plan_client_notification": labelProgramPlanClientNotification,
    "label_program_plan_p_experiences_title": labelProgramPlanPExperiencesTitle,
    "label_program_plan_client_list_tab_heading": labelProgramPlanClientListTabHeading,
    "label_program_plan_choosecustomtimerange": labelProgramPlanChoosecustomtimerange,
    "label_experiences_tab_fieldtype": labelExperiencesTabFieldtype,
    "label_experiences_tab_curriculam_type": labelExperiencesTabCurriculamType,
    "planned_experience_newsfeed_def_values": plannedExperienceNewsfeedDefValues,
    "planned_experience_dynamic_field_2": plannedExperienceDynamicField2,
    "label_program_plan_lsfollowuptitle": labelProgramPlanLsfollowuptitle,
    "nav_program_plan_main_heading": navProgramPlanMainHeading,
    "label_program_plan_from": labelProgramPlanFrom,
    "label_program_plan_pdftype_filter3": labelProgramPlanPdftypeFilter3,
    "planned_experience_journal_adt1": plannedExperienceJournalAdt1,
    "label_program_plan_allday": labelProgramPlanAllday,
    "label_program_plan_pdftype_filter4": labelProgramPlanPdftypeFilter4,
    "label_experiences_tab_curriculam_fields": labelExperiencesTabCurriculamFields,
    "planned_experience_journal_adt2": plannedExperienceJournalAdt2,
    "label_program_plan_educator": labelProgramPlanEducator,
    "label_program_plan_pall_weekly_report_heading": labelProgramPlanPallWeeklyReportHeading,
    "label_program_plan_pdftype_filter1": labelProgramPlanPdftypeFilter1,
    "label_program_plan_pdftype_filter2": labelProgramPlanPdftypeFilter2,
    "planned_experience_observation_adt2": plannedExperienceObservationAdt2,
    "label_program_plan_programstatus": labelProgramPlanProgramstatus,
    "label_program_plan_created_sub_links": labelProgramPlanCreatedSubLinks,
    "label_program_plan_report_type_filter": labelProgramPlanReportTypeFilter,
    "label_program_plan_type": labelProgramPlanType,
    "planned_experience_observation_adt1": plannedExperienceObservationAdt1,
    "label_program_plan_title": labelProgramPlanTitle,
    "label_program_plan_completed_programs_list": labelProgramPlanCompletedProgramsList,
    "label_program_plan_shareallactivities": labelProgramPlanShareallactivities,
    "label_program_plan_createdby": labelProgramPlanCreatedby,
    "label_program_plan_pdftype_filter4_new": labelProgramPlanPdftypeFilter4New,
    "program_plan_default_view": programPlanDefaultView,
    "label_program_plan_newlearningtags": labelProgramPlanNewlearningtags,
    "label_program_plan_time": labelProgramPlanTime,
    "label_program_plan_pex_sarea_weekly_report_heading": labelProgramPlanPexSareaWeeklyReportHeading,
    "label_program_plan_staff": labelProgramPlanStaff,
    "label_program_plan_dashboard_filter": labelProgramPlanDashboardFilter,
    "label_program_plan_programstatusmanuallychange": labelProgramPlanProgramstatusmanuallychange,
    "label_program_plan_report_heading": labelProgramPlanReportHeading,
    "label_program_plan_pdftype_filter3_new": labelProgramPlanPdftypeFilter3New,
    "label_program_plan_subjectarea": labelProgramPlanSubjectarea,
    "label_program_plan_room": labelProgramPlanRoom,
    "label_program_plan_pall_sarea_weekly_report_heading": labelProgramPlanPallSareaWeeklyReportHeading,
    "label_program_plan_learningtags": labelProgramPlanLearningtags,
    "label_program_plan_journalfollowuptitle": labelProgramPlanJournalfollowuptitle,
    "nav_program_planning": navProgramPlanning,
    "planned_experience_journal_def_values": plannedExperienceJournalDefValues,
    "label_program_plan_repeat": labelProgramPlanRepeat,
    "nav_program_plan_manageactivity": navProgramPlanManageactivity,
    "label_program_plan_children": labelProgramPlanChildren,
    "label_program_plan_client_list_heading": labelProgramPlanClientListHeading,
    "nav_program_plan_main_heading_plural": navProgramPlanMainHeadingPlural,
  };
}

class ReminderSettingsSection {
  String medicationRemindersOpenAuto;
  String medicationRemindersTopHeading;
  String medicationRemindersTopToggleBtnDelayed;
  String medicationRemindersFirstrequestSeconds;
  String medicationRemindersBeforenowMinutes;
  String medicationRemindersAfternowMinutes;
  String medicationRemindersTopToggleBtnFuture;

  ReminderSettingsSection({
    this.medicationRemindersOpenAuto,
    this.medicationRemindersTopHeading,
    this.medicationRemindersTopToggleBtnDelayed,
    this.medicationRemindersFirstrequestSeconds,
    this.medicationRemindersBeforenowMinutes,
    this.medicationRemindersAfternowMinutes,
    this.medicationRemindersTopToggleBtnFuture,
  });

  factory ReminderSettingsSection.fromJson(Map<String, dynamic> json) => new ReminderSettingsSection(
    medicationRemindersOpenAuto: json["medication_reminders_open_auto"],
    medicationRemindersTopHeading: json["medication_reminders_top_heading"],
    medicationRemindersTopToggleBtnDelayed: json["medication_reminders_top_toggle_btn_delayed"],
    medicationRemindersFirstrequestSeconds: json["medication_reminders_firstrequest_seconds"],
    medicationRemindersBeforenowMinutes: json["medication_reminders_beforenow_minutes"],
    medicationRemindersAfternowMinutes: json["medication_reminders_afternow_minutes"],
    medicationRemindersTopToggleBtnFuture: json["medication_reminders_top_toggle_btn_future"],
  );

  Map<String, dynamic> toJson() => {
    "medication_reminders_open_auto": medicationRemindersOpenAuto,
    "medication_reminders_top_heading": medicationRemindersTopHeading,
    "medication_reminders_top_toggle_btn_delayed": medicationRemindersTopToggleBtnDelayed,
    "medication_reminders_firstrequest_seconds": medicationRemindersFirstrequestSeconds,
    "medication_reminders_beforenow_minutes": medicationRemindersBeforenowMinutes,
    "medication_reminders_afternow_minutes": medicationRemindersAfternowMinutes,
    "medication_reminders_top_toggle_btn_future": medicationRemindersTopToggleBtnFuture,
  };
}

class ReportingSection {
  String navReportModuleLabel;
  String navReportNewsfeedIndexTitle;
  String navReportOutcomesHeading;
  String navReportJourneyPdfTitle;
  String navReportDailychartPdfTitle;
  String navReportNewsfeedPdfMaxImgCount;
  String navReportLearningTagsLabel;
  String navReportsPortfolioLable;
  String navReportJournalLabel;
  String navReportJourneyLabel;
  String navReportOutcomesIndexTitle;
  String navReportJourneyIndexTitle;
  String navReportOutcomesLabel;
  String navReportsPortfolioIndexTitle;
  String navReportModuleIndexTitle;
  String navReportNewsfeedLabel;
  String navReportNewsfeedPdfTitle;
  String navReportDailychartIndexTitle;
  String navReportLearningTagsIndexTitle;
  String navReportJournalPdfTitle;
  String navReportModuleSelect0;
  String navReportsLearningStoryLable;
  String navReportModuleSelect2;
  String navReportLearningOutcomeDateRangeMonths;
  String navReportModuleSelect1;
  String navReportModulePdfTitle;
  String navReportModuleSelect4;
  String navReportJournalIndexTitle;
  String navReportModuleSelect3;
  String navReportDailychartLabel;
  String navReportLearningTagsHeading;
  String navReportModuleSelect5;

  ReportingSection({
    this.navReportModuleLabel,
    this.navReportNewsfeedIndexTitle,
    this.navReportOutcomesHeading,
    this.navReportJourneyPdfTitle,
    this.navReportDailychartPdfTitle,
    this.navReportNewsfeedPdfMaxImgCount,
    this.navReportLearningTagsLabel,
    this.navReportsPortfolioLable,
    this.navReportJournalLabel,
    this.navReportJourneyLabel,
    this.navReportOutcomesIndexTitle,
    this.navReportJourneyIndexTitle,
    this.navReportOutcomesLabel,
    this.navReportsPortfolioIndexTitle,
    this.navReportModuleIndexTitle,
    this.navReportNewsfeedLabel,
    this.navReportNewsfeedPdfTitle,
    this.navReportDailychartIndexTitle,
    this.navReportLearningTagsIndexTitle,
    this.navReportJournalPdfTitle,
    this.navReportModuleSelect0,
    this.navReportsLearningStoryLable,
    this.navReportModuleSelect2,
    this.navReportLearningOutcomeDateRangeMonths,
    this.navReportModuleSelect1,
    this.navReportModulePdfTitle,
    this.navReportModuleSelect4,
    this.navReportJournalIndexTitle,
    this.navReportModuleSelect3,
    this.navReportDailychartLabel,
    this.navReportLearningTagsHeading,
    this.navReportModuleSelect5,
  });

  factory ReportingSection.fromJson(Map<String, dynamic> json) => new ReportingSection(
    navReportModuleLabel: json["nav_report_module_label"],
    navReportNewsfeedIndexTitle: json["nav_report_newsfeed_index_title"],
    navReportOutcomesHeading: json["nav_report_outcomes_heading"],
    navReportJourneyPdfTitle: json["nav_report_journey_pdf_title"],
    navReportDailychartPdfTitle: json["nav_report_dailychart_pdf_title"],
    navReportNewsfeedPdfMaxImgCount: json["nav_report_newsfeed_pdf_max_img_count"],
    navReportLearningTagsLabel: json["nav_report_learning_tags_label"],
    navReportsPortfolioLable: json["nav_reports_portfolio_lable"],
    navReportJournalLabel: json["nav_report_journal_label"],
    navReportJourneyLabel: json["nav_report_journey_label"],
    navReportOutcomesIndexTitle: json["nav_report_outcomes_index_title"],
    navReportJourneyIndexTitle: json["nav_report_journey_index_title"],
    navReportOutcomesLabel: json["nav_report_outcomes_label"],
    navReportsPortfolioIndexTitle: json["nav_reports_portfolio_index_title"],
    navReportModuleIndexTitle: json["nav_report_module_index_title"],
    navReportNewsfeedLabel: json["nav_report_newsfeed_label"],
    navReportNewsfeedPdfTitle: json["nav_report_newsfeed_pdf_title"],
    navReportDailychartIndexTitle: json["nav_report_dailychart_index_title"],
    navReportLearningTagsIndexTitle: json["nav_report_learning_tags_index_title"],
    navReportJournalPdfTitle: json["nav_report_journal_pdf_title"],
    navReportModuleSelect0: json["nav_report_module_select_0"],
    navReportsLearningStoryLable: json["nav_reports_learning_story_lable"],
    navReportModuleSelect2: json["nav_report_module_select_2"],
    navReportLearningOutcomeDateRangeMonths: json["nav_report_learning_outcome_date_range_months"],
    navReportModuleSelect1: json["nav_report_module_select_1"],
    navReportModulePdfTitle: json["nav_report_module_pdf_title"],
    navReportModuleSelect4: json["nav_report_module_select_4"],
    navReportJournalIndexTitle: json["nav_report_journal_index_title"],
    navReportModuleSelect3: json["nav_report_module_select_3"],
    navReportDailychartLabel: json["nav_report_dailychart_label"],
    navReportLearningTagsHeading: json["nav_report_learning_tags_heading"],
    navReportModuleSelect5: json["nav_report_module_select_5"],
  );

  Map<String, dynamic> toJson() => {
    "nav_report_module_label": navReportModuleLabel,
    "nav_report_newsfeed_index_title": navReportNewsfeedIndexTitle,
    "nav_report_outcomes_heading": navReportOutcomesHeading,
    "nav_report_journey_pdf_title": navReportJourneyPdfTitle,
    "nav_report_dailychart_pdf_title": navReportDailychartPdfTitle,
    "nav_report_newsfeed_pdf_max_img_count": navReportNewsfeedPdfMaxImgCount,
    "nav_report_learning_tags_label": navReportLearningTagsLabel,
    "nav_reports_portfolio_lable": navReportsPortfolioLable,
    "nav_report_journal_label": navReportJournalLabel,
    "nav_report_journey_label": navReportJourneyLabel,
    "nav_report_outcomes_index_title": navReportOutcomesIndexTitle,
    "nav_report_journey_index_title": navReportJourneyIndexTitle,
    "nav_report_outcomes_label": navReportOutcomesLabel,
    "nav_reports_portfolio_index_title": navReportsPortfolioIndexTitle,
    "nav_report_module_index_title": navReportModuleIndexTitle,
    "nav_report_newsfeed_label": navReportNewsfeedLabel,
    "nav_report_newsfeed_pdf_title": navReportNewsfeedPdfTitle,
    "nav_report_dailychart_index_title": navReportDailychartIndexTitle,
    "nav_report_learning_tags_index_title": navReportLearningTagsIndexTitle,
    "nav_report_journal_pdf_title": navReportJournalPdfTitle,
    "nav_report_module_select_0": navReportModuleSelect0,
    "nav_reports_learning_story_lable": navReportsLearningStoryLable,
    "nav_report_module_select_2": navReportModuleSelect2,
    "nav_report_learning_outcome_date_range_months": navReportLearningOutcomeDateRangeMonths,
    "nav_report_module_select_1": navReportModuleSelect1,
    "nav_report_module_pdf_title": navReportModulePdfTitle,
    "nav_report_module_select_4": navReportModuleSelect4,
    "nav_report_journal_index_title": navReportJournalIndexTitle,
    "nav_report_module_select_3": navReportModuleSelect3,
    "nav_report_dailychart_label": navReportDailychartLabel,
    "nav_report_learning_tags_heading": navReportLearningTagsHeading,
    "nav_report_module_select_5": navReportModuleSelect5,
  };
}

class RightSideBar {
  String navRightbarTitleRemarks;
  String navRightbarTitleRemarksClient;
  String navRightbarTitleComments;

  RightSideBar({
    this.navRightbarTitleRemarks,
    this.navRightbarTitleRemarksClient,
    this.navRightbarTitleComments,
  });

  factory RightSideBar.fromJson(Map<String, dynamic> json) => new RightSideBar(
    navRightbarTitleRemarks: json["nav_rightbar_title_remarks"],
    navRightbarTitleRemarksClient: json["nav_rightbar_title_remarks_client"],
    navRightbarTitleComments: json["nav_rightbar_title_comments"],
  );

  Map<String, dynamic> toJson() => {
    "nav_rightbar_title_remarks": navRightbarTitleRemarks,
    "nav_rightbar_title_remarks_client": navRightbarTitleRemarksClient,
    "nav_rightbar_title_comments": navRightbarTitleComments,
  };
}

class StaffDocumentsUploadSection {
  String navClientPrescribedMedicationCreate;
  String navStaffDocModelTitle;
  String navClientMedicationCreate;
  String navClientNonprescribedMedicationCreate;
  String navStsffDocLable;
  String navStaffDocIndexTitle;
  String navStaffDocButtonAddnew;
  String activityLogMedicationRequest;
  String navClientMedications;

  StaffDocumentsUploadSection({
    this.navClientPrescribedMedicationCreate,
    this.navStaffDocModelTitle,
    this.navClientMedicationCreate,
    this.navClientNonprescribedMedicationCreate,
    this.navStsffDocLable,
    this.navStaffDocIndexTitle,
    this.navStaffDocButtonAddnew,
    this.activityLogMedicationRequest,
    this.navClientMedications,
  });

  factory StaffDocumentsUploadSection.fromJson(Map<String, dynamic> json) => new StaffDocumentsUploadSection(
    navClientPrescribedMedicationCreate: json["nav_client_prescribed_medication_create"],
    navStaffDocModelTitle: json["nav_staff_doc_model_title"],
    navClientMedicationCreate: json["nav_client_medication_create"],
    navClientNonprescribedMedicationCreate: json["nav_client_nonprescribed_medication_create"],
    navStsffDocLable: json["nav_stsff_doc_lable"],
    navStaffDocIndexTitle: json["nav_staff_doc_index_title"],
    navStaffDocButtonAddnew: json["nav_staff_doc_button_addnew"],
    activityLogMedicationRequest: json["activity_log_medication_request"],
    navClientMedications: json["nav_client_medications"],
  );

  Map<String, dynamic> toJson() => {
    "nav_client_prescribed_medication_create": navClientPrescribedMedicationCreate,
    "nav_staff_doc_model_title": navStaffDocModelTitle,
    "nav_client_medication_create": navClientMedicationCreate,
    "nav_client_nonprescribed_medication_create": navClientNonprescribedMedicationCreate,
    "nav_stsff_doc_lable": navStsffDocLable,
    "nav_staff_doc_index_title": navStaffDocIndexTitle,
    "nav_staff_doc_button_addnew": navStaffDocButtonAddnew,
    "activity_log_medication_request": activityLogMedicationRequest,
    "nav_client_medications": navClientMedications,
  };
}

class SubjectAreaSection {
  String labelSareaNewArea;
  String navSubjectareaIndexTitle;
  String labelSareaCommonTitle;

  SubjectAreaSection({
    this.labelSareaNewArea,
    this.navSubjectareaIndexTitle,
    this.labelSareaCommonTitle,
  });

  factory SubjectAreaSection.fromJson(Map<String, dynamic> json) => new SubjectAreaSection(
    labelSareaNewArea: json["label_sarea_newArea"],
    navSubjectareaIndexTitle: json["nav_subjectarea_index_title"],
    labelSareaCommonTitle: json["label_sarea_common_title"],
  );

  Map<String, dynamic> toJson() => {
    "label_sarea_newArea": labelSareaNewArea,
    "nav_subjectarea_index_title": navSubjectareaIndexTitle,
    "label_sarea_common_title": labelSareaCommonTitle,
  };
}

class TextTagSection {
  String labelTtagsCommonTitle;
  String labelTtagsNewtag;
  String labelTtagsCommonTitleSingle;
  String labelTtagsTabTitle;

  TextTagSection({
    this.labelTtagsCommonTitle,
    this.labelTtagsNewtag,
    this.labelTtagsCommonTitleSingle,
    this.labelTtagsTabTitle,
  });

  factory TextTagSection.fromJson(Map<String, dynamic> json) => new TextTagSection(
    labelTtagsCommonTitle: json["label_ttags_common_title"],
    labelTtagsNewtag: json["label_ttags_newtag"],
    labelTtagsCommonTitleSingle: json["label_ttags_common_title_single"],
    labelTtagsTabTitle: json["label_ttags_tab_title"],
  );

  Map<String, dynamic> toJson() => {
    "label_ttags_common_title": labelTtagsCommonTitle,
    "label_ttags_newtag": labelTtagsNewtag,
    "label_ttags_common_title_single": labelTtagsCommonTitleSingle,
    "label_ttags_tab_title": labelTtagsTabTitle,
  };
}

class UserActivitySection {
  String userActivityHistoryLblRoom;
  String userActivityHistoryLblSelectadate;
  String userActivityHistoryLblSelectaweek;
  String userActivityHistoryLblSelectUserTypeActive;
  String userActivityHistoryLblTotalparentcount;
  String userActivityHistoryLblParent;
  String userActivityHistoryLblTotalroomcount;
  String userActivityHistoryLblSelectUserTypeInactive;
  String userActivityHistoryLblSelectparent;
  String userActivityHistoryLblTotalactiveparentcount;
  String userActivityHistoryLblCentre;
  String userActivityHistoryLblSelectUserType;

  UserActivitySection({
    this.userActivityHistoryLblRoom,
    this.userActivityHistoryLblSelectadate,
    this.userActivityHistoryLblSelectaweek,
    this.userActivityHistoryLblSelectUserTypeActive,
    this.userActivityHistoryLblTotalparentcount,
    this.userActivityHistoryLblParent,
    this.userActivityHistoryLblTotalroomcount,
    this.userActivityHistoryLblSelectUserTypeInactive,
    this.userActivityHistoryLblSelectparent,
    this.userActivityHistoryLblTotalactiveparentcount,
    this.userActivityHistoryLblCentre,
    this.userActivityHistoryLblSelectUserType,
  });

  factory UserActivitySection.fromJson(Map<String, dynamic> json) => new UserActivitySection(
    userActivityHistoryLblRoom: json["user_activity_history_lbl_room"],
    userActivityHistoryLblSelectadate: json["user_activity_history_lbl_selectadate"],
    userActivityHistoryLblSelectaweek: json["user_activity_history_lbl_selectaweek"],
    userActivityHistoryLblSelectUserTypeActive: json["user_activity_history_lbl_select_user_type_active"],
    userActivityHistoryLblTotalparentcount: json["user_activity_history_lbl_totalparentcount"],
    userActivityHistoryLblParent: json["user_activity_history_lbl_parent"],
    userActivityHistoryLblTotalroomcount: json["user_activity_history_lbl_totalroomcount"],
    userActivityHistoryLblSelectUserTypeInactive: json["user_activity_history_lbl_select_user_type_inactive"],
    userActivityHistoryLblSelectparent: json["user_activity_history_lbl_selectparent"],
    userActivityHistoryLblTotalactiveparentcount: json["user_activity_history_lbl_totalactiveparentcount"],
    userActivityHistoryLblCentre: json["user_activity_history_lbl_centre"],
    userActivityHistoryLblSelectUserType: json["user_activity_history_lbl_select_user_type"],
  );

  Map<String, dynamic> toJson() => {
    "user_activity_history_lbl_room": userActivityHistoryLblRoom,
    "user_activity_history_lbl_selectadate": userActivityHistoryLblSelectadate,
    "user_activity_history_lbl_selectaweek": userActivityHistoryLblSelectaweek,
    "user_activity_history_lbl_select_user_type_active": userActivityHistoryLblSelectUserTypeActive,
    "user_activity_history_lbl_totalparentcount": userActivityHistoryLblTotalparentcount,
    "user_activity_history_lbl_parent": userActivityHistoryLblParent,
    "user_activity_history_lbl_totalroomcount": userActivityHistoryLblTotalroomcount,
    "user_activity_history_lbl_select_user_type_inactive": userActivityHistoryLblSelectUserTypeInactive,
    "user_activity_history_lbl_selectparent": userActivityHistoryLblSelectparent,
    "user_activity_history_lbl_totalactiveparentcount": userActivityHistoryLblTotalactiveparentcount,
    "user_activity_history_lbl_centre": userActivityHistoryLblCentre,
    "user_activity_history_lbl_select_user_type": userActivityHistoryLblSelectUserType,
  };
}

class UserManagementSection {
  String navCentermanagementStaffNotAssignMessage;
  String navCentermanagementParentIndexTitle;
  String navCentermanagementStaffFormLabel;
  String navCentermanagementParentLabel;
  String navCentermanagementParentFormLabel;
  String navCentermanagementGuardianLabel;
  String navCentermanagementGuardianFormLabel;
  String navCentermanagementStaffIndexTitle;
  String navCentermanagementStaffButtonAddnew;
  String navCentermanagementGuardianButtonAddnew;
  String navCentermanagementStaffLabel;
  String navCentermanagementGuardianIndexTitle;
  String navCentermanagementParentButtonAddnew;
  String navCentermanagementStaffFormLabelRoom;

  UserManagementSection({
    this.navCentermanagementStaffNotAssignMessage,
    this.navCentermanagementParentIndexTitle,
    this.navCentermanagementStaffFormLabel,
    this.navCentermanagementParentLabel,
    this.navCentermanagementParentFormLabel,
    this.navCentermanagementGuardianLabel,
    this.navCentermanagementGuardianFormLabel,
    this.navCentermanagementStaffIndexTitle,
    this.navCentermanagementStaffButtonAddnew,
    this.navCentermanagementGuardianButtonAddnew,
    this.navCentermanagementStaffLabel,
    this.navCentermanagementGuardianIndexTitle,
    this.navCentermanagementParentButtonAddnew,
    this.navCentermanagementStaffFormLabelRoom,
  });

  factory UserManagementSection.fromJson(Map<String, dynamic> json) => new UserManagementSection(
    navCentermanagementStaffNotAssignMessage: json["nav_centermanagement_staff_not_assign_message"],
    navCentermanagementParentIndexTitle: json["nav_centermanagement_parent_index_title"],
    navCentermanagementStaffFormLabel: json["nav_centermanagement_staff_form_label"],
    navCentermanagementParentLabel: json["nav_centermanagement_parent_label"],
    navCentermanagementParentFormLabel: json["nav_centermanagement_parent_form_label"],
    navCentermanagementGuardianLabel: json["nav_centermanagement_guardian_label"],
    navCentermanagementGuardianFormLabel: json["nav_centermanagement_guardian_form_label"],
    navCentermanagementStaffIndexTitle: json["nav_centermanagement_staff_index_title"],
    navCentermanagementStaffButtonAddnew: json["nav_centermanagement_staff_button_addnew"],
    navCentermanagementGuardianButtonAddnew: json["nav_centermanagement_guardian_button_addnew"],
    navCentermanagementStaffLabel: json["nav_centermanagement_staff_label"],
    navCentermanagementGuardianIndexTitle: json["nav_centermanagement_guardian_index_title"],
    navCentermanagementParentButtonAddnew: json["nav_centermanagement_parent_button_addnew"],
    navCentermanagementStaffFormLabelRoom: json["nav_centermanagement_staff_form_label_room"],
  );

  Map<String, dynamic> toJson() => {
    "nav_centermanagement_staff_not_assign_message": navCentermanagementStaffNotAssignMessage,
    "nav_centermanagement_parent_index_title": navCentermanagementParentIndexTitle,
    "nav_centermanagement_staff_form_label": navCentermanagementStaffFormLabel,
    "nav_centermanagement_parent_label": navCentermanagementParentLabel,
    "nav_centermanagement_parent_form_label": navCentermanagementParentFormLabel,
    "nav_centermanagement_guardian_label": navCentermanagementGuardianLabel,
    "nav_centermanagement_guardian_form_label": navCentermanagementGuardianFormLabel,
    "nav_centermanagement_staff_index_title": navCentermanagementStaffIndexTitle,
    "nav_centermanagement_staff_button_addnew": navCentermanagementStaffButtonAddnew,
    "nav_centermanagement_guardian_button_addnew": navCentermanagementGuardianButtonAddnew,
    "nav_centermanagement_staff_label": navCentermanagementStaffLabel,
    "nav_centermanagement_guardian_index_title": navCentermanagementGuardianIndexTitle,
    "nav_centermanagement_parent_button_addnew": navCentermanagementParentButtonAddnew,
    "nav_centermanagement_staff_form_label_room": navCentermanagementStaffFormLabelRoom,
  };
}

class UserProfile {
  String navUserprofileIndexTitle;
  String navUserprofileLabel;

  UserProfile({
    this.navUserprofileIndexTitle,
    this.navUserprofileLabel,
  });

  factory UserProfile.fromJson(Map<String, dynamic> json) => new UserProfile(
    navUserprofileIndexTitle: json["nav_userprofile_index_title"],
    navUserprofileLabel: json["nav_userprofile_label"],
  );

  Map<String, dynamic> toJson() => {
    "nav_userprofile_index_title": navUserprofileIndexTitle,
    "nav_userprofile_label": navUserprofileLabel,
  };
}

class Users {
  int id;
  String fullname;
  String secondEmail;
  String username;
  String isAdmin;
  String isStaff;
  String token;
  String email;
  String image;
  String status;
  String loginaccess;
  String needSecEmail;
  String clientId;
  String center;
  Client client;
  String iOSversion;
  String androidVersion;
  bool isparent;
  String createdAt;

  Users({
    this.id,
    this.fullname,
    this.secondEmail,
    this.username,
    this.isAdmin,
    this.isStaff,
    this.token,
    this.email,
    this.image,
    this.status,
    this.loginaccess,
    this.needSecEmail,
    this.clientId,
    this.center,
    this.client,
    this.iOSversion,
    this.androidVersion,
    this.isparent,
    this.createdAt,
  });

  factory Users.fromJson(Map<String, dynamic> json) => new Users(
    id: json["id"],
    fullname: json["fullname"],
    secondEmail: json["second_email"],
    username: json["username"],
    isAdmin: json["isAdmin"],
    isStaff: json["isStaff"],
    token: json["token"],
    email: json["email"],
    image: json["image"],
    status: json["status"],
    loginaccess: json["loginaccess"],
    needSecEmail: json["need_sec_email"],
    clientId: json["client_id"],
    center: json["center"],
    client: Client.fromJson(json["client"]),
    iOSversion: json["iOSversion"],
    androidVersion: json["androidVersion"],
    isparent: json["isparent"],
    createdAt: json["created_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fullname": fullname,
    "second_email": secondEmail,
    "username": username,
    "isAdmin": isAdmin,
    "isStaff": isStaff,
    "token": token,
    "email": email,
    "image": image,
    "status": status,
    "loginaccess": loginaccess,
    "need_sec_email": needSecEmail,
    "client_id": clientId,
    "center": center,
    "client": client.toJson(),
    "iOSversion": iOSversion,
    "androidVersion": androidVersion,
    "isparent": isparent,
    "created_at": createdAt,
  };
}

class Client {
  int id;
  String clientid;
  String name;
  String connectionString;
  String authUri;
  bool kiosk;
  bool active;
  String pincode;
  String logo;
  bool kisokparentemailnotification;
  bool signature;
  bool centerLoginRequired;
  String timezone;
  String androidversion;
  String iosversion;
  String kinderm8Version;
  bool parentKioskEnable;
  bool staffKioskEnable;
  bool enableAllRoomsChildChecking;
  bool enableAllRoomsStaffChecking;
  bool staffap;

  Client({
    this.id,
    this.clientid,
    this.name,
    this.connectionString,
    this.authUri,
    this.kiosk,
    this.active,
    this.pincode,
    this.logo,
    this.kisokparentemailnotification,
    this.signature,
    this.centerLoginRequired,
    this.timezone,
    this.androidversion,
    this.iosversion,
    this.kinderm8Version,
    this.parentKioskEnable,
    this.staffKioskEnable,
    this.enableAllRoomsChildChecking,
    this.enableAllRoomsStaffChecking,
    this.staffap,
  });

  factory Client.fromJson(Map<String, dynamic> json) => new Client(
    id: json["id"],
    clientid: json["clientid"],
    name: json["name"],
    connectionString: json["connectionString"],
    authUri: json["AuthURI"],
    kiosk: json["kiosk"],
    active: json["active"],
    pincode: json["pincode"],
    logo: json["logo"],
    kisokparentemailnotification: json["kisokparentemailnotification"],
    signature: json["signature"],
    centerLoginRequired: json["center_login_required"],
    timezone: json["timezone"],
    androidversion: json["androidversion"],
    iosversion: json["iosversion"],
    kinderm8Version: json["kinderm8version"],
    parentKioskEnable: json["parent_kiosk_enable"],
    staffKioskEnable: json["staff_kiosk_enable"],
    enableAllRoomsChildChecking: json["enable_all_rooms_child_checking"],
    enableAllRoomsStaffChecking: json["enable_all_rooms_staff_checking"],
    staffap: json["staffap"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "clientid": clientid,
    "name": name,
    "connectionString": connectionString,
    "AuthURI": authUri,
    "kiosk": kiosk,
    "active": active,
    "pincode": pincode,
    "logo": logo,
    "kisokparentemailnotification": kisokparentemailnotification,
    "signature": signature,
    "center_login_required": centerLoginRequired,
    "timezone": timezone,
    "androidversion": androidversion,
    "iosversion": iosversion,
    "kinderm8version": kinderm8Version,
    "parent_kiosk_enable": parentKioskEnable,
    "staff_kiosk_enable": staffKioskEnable,
    "enable_all_rooms_child_checking": enableAllRoomsChildChecking,
    "enable_all_rooms_staff_checking": enableAllRoomsStaffChecking,
    "staffap": staffap,
  };
}
