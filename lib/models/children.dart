class Children {
  int _id;
  int _roomId;
  String _firstname;
  String _middlename;
  String _lastname;
  String _age;
  String _dob;
  String _gender;
  String _attendance;
  String _image;
  String _status;
  String _createdAt;
  int _count;
  bool _ispick;
  int _attendanceId;

  Children(
      this._id,
      this._roomId,
      this._firstname,
      this._middlename,
      this._lastname,
      this._age,
      this._dob,
      this._gender,
      this._attendance,
      this._image,
      this._status,
      this._createdAt,
      this._count,
      this._ispick,
      this._attendanceId,
      );

  Children.map(dynamic json) {
    this._id = json["id"];
    this._roomId = json["room_id"];
    this._firstname = json["firstname"];
    this._middlename = json["middlename"];
    this._lastname = json["lastname"];
    this._age = json["age"];
    this._dob = json["dob"];
    this._gender = json["gender"];
    this._attendance = json["attendance"];
    this._image = json["image"];
    this._status = json["status"];
    this._createdAt = json["created_at"];
    this._count = json["count"];
    this._ispick = json["ispick"];
    this._attendanceId = json["attendance_id"];

    print("hi $_id $_lastname");
  }

  int get id => _id;
  int get roomId => _roomId;
  String get client_id => _firstname;
  String get center => _middlename;
  String get email => _lastname;
  String get password => _age;
  String get dob => _dob;
  String get gender => _gender;
  String get attendance => _attendance;
  String get image => _image;
  String get status => _status;
  String get createdAt => _createdAt;
  int get count => _count;
  bool get ispick => _ispick;
  int get attendanceId => _attendanceId;

  Map<String, dynamic> toMap() {
    print("user.tomap 11");
    var map = new Map<String, dynamic>();
    map["id"] = _id;
    map["roomId"] = _roomId;
    map["firstname"] = _firstname;
    map["middlename"] = _middlename;
    map["lastname"] = _lastname;
//    map["age"] = _age;
//    map["dob"] = _dob;
//    map["gender"] = _gender;
    map["attendance"] = _attendance;
    map["image"] = _image;
//    map["status"] = _status;
//    map["createdAt"] = _createdAt;
//    map["count"] = _count;
//    map["ispick"] = _ispick;
//    map["attendanceId"] = _attendanceId;

    return map;
  }
}
