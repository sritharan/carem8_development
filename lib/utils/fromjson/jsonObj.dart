import 'dart:convert';

List<Login> loginFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<Login>.from(jsonData.map((x) => Login.fromJson(x)));
}

String loginToJson(List<Login> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class Login {
  UserData user;
  String jwt;
  List<Child> children;

  Login({
    this.user,
    this.jwt,
    this.children,
  });

  factory Login.fromJson(Map<String, dynamic> json) => new Login(
    user: UserData.fromJson(json["user"]),
    jwt: json["jwt"],
    children: new List<Child>.from(
        json["children"].map((x) => Child.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "user": user.toJson(),
    "jwt": jwt,
    "children": new List<dynamic>.from(children.map((x) => x.toJson())),
  };
}

class Child {
  int id;
  int roomId;
  String firstname;
  String middlename;
  String lastname;
  String age;
  String dob;
  String gender;
  String attendance;
  String image;
  String status;
  String createdAt;
  int count;
  bool ispick;
  int attendanceId;

  Child({
    this.id,
    this.roomId,
    this.firstname,
    this.middlename,
    this.lastname,
    this.age,
    this.dob,
    this.gender,
    this.attendance,
    this.image,
    this.status,
    this.createdAt,
    this.count,
    this.ispick,
    this.attendanceId,
  });

  factory Child.fromJson(Map<String, dynamic> json) => new Child(
    id: json["id"],
    roomId: json["room_id"],
    firstname: json["firstname"],
    middlename: json["middlename"],
    lastname: json["lastname"],
    age: json["age"],
    dob: json["dob"],
    gender: json["gender"],
    attendance: json["attendance"],
    image: json["image"],
    status: json["status"],
    createdAt: json["created_at"],
    count: json["count"],
    ispick: json["ispick"],
    attendanceId: json["attendance_id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "room_id": roomId,
    "firstname": firstname,
    "middlename": middlename,
    "lastname": lastname,
    "age": age,
    "dob": dob,
    "gender": gender,
    "attendance": attendance,
    "image": image,
    "status": status,
    "created_at": createdAt,
    "count": count,
    "ispick": ispick,
    "attendance_id": attendanceId,
  };
}

class UserData {
  int id;
  String fullname;
  String secondEmail;
  String username;
  String isAdmin;
  String isStaff;
  String token;
  String email;
  String image;
  String status;
  String loginaccess;
  String needSecEmail;
  String clientId;
  String center;
  Client client;
  String iOSversion;
  String androidVersion;
  bool isparent;
  String createdAt;

  UserData({
    this.id,
    this.fullname,
    this.secondEmail,
    this.username,
    this.isAdmin,
    this.isStaff,
    this.token,
    this.email,
    this.image,
    this.status,
    this.loginaccess,
    this.needSecEmail,
    this.clientId,
    this.center,
    this.client,
    this.iOSversion,
    this.androidVersion,
    this.isparent,
    this.createdAt,
  });

  factory UserData.fromJson(Map<String, dynamic> json) => new UserData(
    id: json["id"],
    fullname: json["fullname"],
    secondEmail: json["second_email"],
    username: json["username"],
    isAdmin: json["isAdmin"],
    isStaff: json["isStaff"],
    token: json["token"],
    email: json["email"],
    image: json["image"],
    status: json["status"],
    loginaccess: json["loginaccess"],
    needSecEmail: json["need_sec_email"],
    clientId: json["client_id"],
    center: json["center"],
    client: Client.fromJson(json["client"]),
    iOSversion: json["iOSversion"],
    androidVersion: json["androidVersion"],
    isparent: json["isparent"],
    createdAt: json["created_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fullname": fullname,
    "second_email": secondEmail,
    "username": username,
    "isAdmin": isAdmin,
    "isStaff": isStaff,
    "token": token,
    "email": email,
    "image": image,
    "status": status,
    "loginaccess": loginaccess,
    "need_sec_email": needSecEmail,
    "client_id": clientId,
    "center": center,
    "client": client.toJson(),
    "iOSversion": iOSversion,
    "androidVersion": androidVersion,
    "isparent": isparent,
    "created_at": createdAt,
  };
}

class Client {
  int id;
  String clientid;
  String name;
  String connectionString;
  String authUri;
  bool kiosk;
  bool active;
  String pincode;
  String logo;
  bool kisokparentemailnotification;
  bool signature;
  bool centerLoginRequired;
  String timezone;
  String androidversion;
  String iosversion;
  String kinderm8Version;
  bool parentKioskEnable;
  bool staffKioskEnable;
  bool enableAllRoomsChildChecking;
  bool enableAllRoomsStaffChecking;
  bool staffap;

  Client({
    this.id,
    this.clientid,
    this.name,
    this.connectionString,
    this.authUri,
    this.kiosk,
    this.active,
    this.pincode,
    this.logo,
    this.kisokparentemailnotification,
    this.signature,
    this.centerLoginRequired,
    this.timezone,
    this.androidversion,
    this.iosversion,
    this.kinderm8Version,
    this.parentKioskEnable,
    this.staffKioskEnable,
    this.enableAllRoomsChildChecking,
    this.enableAllRoomsStaffChecking,
    this.staffap,
  });

  factory Client.fromJson(Map<String, dynamic> json) => new Client(
    id: json["id"],
    clientid: json["clientid"],
    name: json["name"],
    connectionString: json["connectionString"],
    authUri: json["AuthURI"],
    kiosk: json["kiosk"],
    active: json["active"],
    pincode: json["pincode"],
    logo: json["logo"],
    kisokparentemailnotification: json["kisokparentemailnotification"],
    signature: json["signature"],
    centerLoginRequired: json["center_login_required"],
    timezone: json["timezone"],
    androidversion: json["androidversion"],
    iosversion: json["iosversion"],
    kinderm8Version: json["kinderm8version"],
    parentKioskEnable: json["parent_kiosk_enable"],
    staffKioskEnable: json["staff_kiosk_enable"],
    enableAllRoomsChildChecking: json["enable_all_rooms_child_checking"],
    enableAllRoomsStaffChecking: json["enable_all_rooms_staff_checking"],
    staffap: json["staffap"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "clientid": clientid,
    "name": name,
    "connectionString": connectionString,
    "AuthURI": authUri,
    "kiosk": kiosk,
    "active": active,
    "pincode": pincode,
    "logo": logo,
    "kisokparentemailnotification": kisokparentemailnotification,
    "signature": signature,
    "center_login_required": centerLoginRequired,
    "timezone": timezone,
    "androidversion": androidversion,
    "iosversion": iosversion,
    "kinderm8version": kinderm8Version,
    "parent_kiosk_enable": parentKioskEnable,
    "staff_kiosk_enable": staffKioskEnable,
    "enable_all_rooms_child_checking": enableAllRoomsChildChecking,
    "enable_all_rooms_staff_checking": enableAllRoomsStaffChecking,
    "staffap": staffap,
  };
}
