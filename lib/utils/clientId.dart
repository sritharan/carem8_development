import 'dart:convert';

List<Login> loginFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<Login>.from(jsonData.map((x) => Login.fromJson(x)));
}

String loginToJson(List<Login> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class Login {
  int id;
  String email;
  String clientId;
  String center;
  String image;

  Login({
    this.id,
    this.email,
    this.clientId,
    this.center,
    this.image,
  });

  factory Login.fromJson(Map<String, dynamic> json) => new Login(
    id: json["id"],
    email: json["email"],
    clientId: json["client_id"],
    center: json["center"],
    image: json["image"] == null ? null : json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "email": email,
    "client_id": clientId,
    "center": center,
    "image": image == null ? null : image,
  };
}
