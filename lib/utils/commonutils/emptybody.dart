import 'package:flutter/material.dart';

class EmptyBody extends StatelessWidget {
  final String title;

  EmptyBody(this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(title),
      ),
    );
  }
}
