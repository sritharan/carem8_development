import 'dart:async';
import 'package:carem8/Theme.dart' as Theme;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carem8/utils/commonutils/load_image.dart';
import 'package:carem8/utils/commonutils/saveimage.dart';
import 'package:zoomable_image/zoomable_image.dart';
//import 'package:photo_view/photo_view.dart';

//import 'package:flutter_swiper/flutter_swiper.dart';
//import 'package:pinch_zoom_image/pinch_zoom_image.dart';

class ZoomableImagePage_Observation extends StatefulWidget {
  final data;

  ZoomableImagePage_Observation(this.data);

  @override
  ZoomableImagePage_observationState createState() {
    return new ZoomableImagePage_observationState();
  }
}

class ZoomableImagePage_observationState extends State<ZoomableImagePage_Observation> {
  Future<Null> _launched;

  Widget _showResult(BuildContext context, AsyncSnapshot<Null> snapshot) {
    if (!snapshot.hasError) {
      return Text('Image is saved');
    } else {
      return const Text('Unable to save image');
    }
  }

  Future<Null> _saveNetworkImage(String url) async {
    try {
//      await SaveFile().saveImage(url);
      var date = new DateTime.now();
      var imagedownloaddate = new DateTime(date.year, date.month, date.day);
      await SaveFile().saveImagewithDate(url,imagedownloaddate);
    } on Error catch (e) {
      throw 'Error has occured while saving';
    }
  }

  @override
  Widget build(BuildContext context) {
    print("HHHHHHHHH${widget.data}");
    var finalURLs = widget.data;
    print(widget.data);
//    var finalURL = widget.data['imagesnew']['imagedetails'][0]['fileurl'];
//    var caption = widget.data['imagesnew']['imagedetails'][0]['caption'];
//    if (data1["post_img_url"] == null || data1["post_img_url"] == "") {
//      //do nothing
//    } else {
//      var pri = data1['imagesnew']['imag edetails'];
////      var pri = data1["post_img_url"].split(':;');
//      if (pri.length > 0) {
//        finalURL = "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${pri[0]}";
//      }
//    }

    List<Widget> widgets = [];
    for (int index = 0; index < finalURLs.length; index++) {
      widgets.add(
        Scaffold(
          appBar: new AppBar(
            title: finalURLs.length > 1
                ? new Text("${index + 1} / ${finalURLs.length}",
                    style: TextStyle(
                      color: Colors.white,
                    ))
                : new Text(''),
            elevation: 0.0,
            backgroundColor: Theme.Colors.app_dark[900],
            leading: new IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: new Icon(Icons.close, color: Theme.Colors.appcolour),
            ),
            actions: <Widget>[
              new IconButton(
                onPressed: () {
                  print('Going to Download this >> ${finalURLs[index]['fileurl']}');
                  _saveNetworkImage(finalURLs[index]['fileurl']);
                },
                icon: new Icon(Icons.file_download,
                    color: Theme.Colors.appcolour),
              ),
            ],
          ),
          body: new Stack(
            children: <Widget>[
//              new Container(
//                  child: PhotoViewGallery(
//                pageOptions: <PhotoViewGalleryPageOptions>[
//                  PhotoViewGalleryPageOptions(
//                    imageProvider: CachedNetworkImageProvider(finalURLs[0]),
//                    minScale: PhotoViewComputedScale.contained * 1.0,
//                    maxScale: PhotoViewComputedScale.covered * 1.1,
//                    heroTag: "",
//                  ),
//                ],
//                backgroundColor: Theme.Colors.app_dark[900],
//              )),
              /*  new Align(
              alignment: Alignment.bottomRight,
              child: new Column(
                // I need to add a column to set the MainAxisSize to min,
                // otherwise the appbar takes all the screen and the image is no more clickable
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new AppBar(
                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    leading:
                        new Container(), // Overrides the go back arrow icon button
                    actions: <Widget>[
                      Expanded(
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Expanded(
                              child: new Text(finalURLs[index]['caption'],
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12.0)),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),*/
            ],
          ),
        ),
      );
    }
    return new Scaffold(
      backgroundColor: Theme.Colors.app_dark[900],
      body: new SizedBox.expand(
        child: new PageView(children: widgets

            /* This is the old code for zoom */
//          children: <Widget>[
////            new Align(
////              alignment: Alignment.center,
////              child: new Hero(
////                tag: finalURL,
////                child:
////                    new ZoomableImage(new NetworkImage(finalURL), scale: 3.0),
////              ),
////            ),
//
//            new Container(
//                child: PhotoViewGallery(
//              pageOptions: <PhotoViewGalleryPageOptions>[
//                PhotoViewGalleryPageOptions(
//                  imageProvider: NetworkImage(finalURLs[0]['fileurl']),
//                  minScale: PhotoViewComputedScale.contained * 0.3,
//                  maxScale: PhotoViewComputedScale.covered * 1.1,
//                  heroTag: "tag3",
//                ),
//              ],
//              backgroundColor: Colors.black87,
//            )),
//            new Align(
//              alignment: Alignment.topCenter,
//              child: new Column(
//                // I need to add a column to set the MainAxisSize to min,
//                // otherwise the appbar takes all the screen and the image is no more clickable
//                mainAxisAlignment: MainAxisAlignment.start,
//                mainAxisSize: MainAxisSize.min,
//                children: <Widget>[
//                  new AppBar(
//                    elevation: 0.0,
//                    backgroundColor: Colors.transparent,
//                    leading:
//                        new Container(), // Overrides the go back arrow icon button
//                    actions: <Widget>[
//                      new IconButton(
//                          icon: Icon(Icons.save),
//                          onPressed: () {
//                            setState(() {
//                              print('_saveNetworkImage $finalURL');
//                              _launched = _saveNetworkImage(finalURL);
//                            });
//                          }),
//                      new FutureBuilder<Null>(
//                          future: _launched, builder: _showResult),
//                      new IconButton(
//                        onPressed: () {
//                          print('ServerLocalImage $finalURL');
//                          new ServerLocalImage(finalURL);
//                        },
//                        icon:
//                            new Icon(Icons.file_download, color: Colors.white),
//                      ),
//                      new IconButton(
//                        onPressed: () {
//                          setState(() {
//                            Navigator.of(context).pop();
//                          });
//                        },
//                        icon: new Icon(Icons.close, color: Colors.white),
//                      ),
//                    ],
//                  ),
//                ],
//              ),
//            ),
//            new Align(
//              alignment: Alignment.bottomRight,
//              child: new Column(
//                // I need to add a column to set the MainAxisSize to min,
//                // otherwise the appbar takes all the screen and the image is no more clickable
//                mainAxisAlignment: MainAxisAlignment.start,
//                mainAxisSize: MainAxisSize.min,
//                children: <Widget>[
//                  new AppBar(
//                    elevation: 0.0,
//                    backgroundColor: Colors.transparent,
//                    leading:
//                        new Container(), // Overrides the go back arrow icon button
//                    actions: <Widget>[
//                      Expanded(
//                        child: new Column(
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          children: <Widget>[
//                            new Text(caption,
//                                style: TextStyle(
//                                    color: Colors.white, fontSize: 12.0)),
////                            new Container(
////                              margin: const EdgeInsets.only(bottom: 5.0),
////                              child: new Text(caption),
////                            ),
//                          ],
//                        ),
//                      ),
//                    ],
//                  ),
//                ],
//              ),
//            ),
//          ],
            /* End of old code for save and close */
            ),
      ),
    );
  }

//  @override
//  Widget build_(BuildContext context) {
////    var finalURL = widget.data['imagesnew']['imagedetails'][0]['fileurl'];
//    var finalURL = widget.data['imagesnew']['imagedetails'];
//    return new Scaffold(
//        body: new Swiper(
//          itemBuilder: (BuildContext context, int index) {
////            return new Image.network(
////              finalURL[index]['fileurl'],
////              fit: BoxFit.contain,
////            );
//          return new Container(
//            margin: const EdgeInsets.only(right: 15.0),
//            width: 100.0,
//            height: 150.0,
//            child: finalURL[index]['fileurl'] != ""
//                ? new CachedNetworkImage(
//              imageUrl: finalURL[index]['fileurl'],
//              placeholder: new CupertinoActivityIndicator(),
//              errorWidget: new Icon(Icons.error),
//              fadeOutDuration: new Duration(seconds: 0),
//              fadeInDuration: new Duration(seconds: 1),
//            ) : null,
//          );
//          },
////          indicatorLayout: PageIndicatorLayout.COLOR,
////          autoplay: true,
//          itemCount: finalURL.length,
////          pagination: new SwiperPagination(),
//          control: new SwiperControl(),
//        ));
//  }
//}

}
