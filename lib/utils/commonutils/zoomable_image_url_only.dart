import 'dart:async';
import 'dart:io';

import 'package:carem8/Theme.dart' as Theme;
import 'package:carem8/utils/commonutils/saveimage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:zoomable_image/zoomable_image.dart';
//import 'package:photo_view/photo_view.dart';

//import 'package:flutter_swiper/flutter_swiper.dart';
//import 'package:pinch_zoom_image/pinch_zoom_image.dart';

class ZoomableImagePage_Url extends StatefulWidget {
  final data;
  final int i;

  ZoomableImagePage_Url(this.data, this.i);

  @override
  ZoomableImagePageUrlState createState() {
    return new ZoomableImagePageUrlState();
  }
}

class ZoomableImagePageUrlState extends State<ZoomableImagePage_Url> {
  var photoFullData;

  Future<Null> _launched;

  static const platform = const MethodChannel('proitzen.kinderm8/s3_image');

  Widget _showResult(BuildContext context, AsyncSnapshot<Null> snapshot) {
    if (!snapshot.hasError) {
      return Text('Image is saved');
    } else {
      return const Text('Unable to save image');
    }
  }

  Future<Null> _saveNetworkImage(String url) async {
    try {
      await SaveFile().saveImage(url);
    } on Error catch (e) {
      throw 'Error has occured while saving';
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    photoFullData = widget.data;
  }

  Future<void> _photoDownloadLevel(
      MethodChannel channel, String finalUrl) async {
    try {
      final String result = await channel
          .invokeMethod('getPhotoDownloadLevel', {"url": finalUrl});
    } on PlatformException catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
//    var finalURL = widget.data['imagesnew']['imagedetails'][0]['fileurl'];
    print('Images list with caption ${photoFullData}');
//    var caption = widget.data['imagesnew']['imagedetails'][0]['caption'];
//    if (data1["post_img_url"] == null || data1["post_img_url"] == "") {
//      //do nothing
//    } else {
//      var pri = data1['imagesnew']['imag edetails'];
////      var pri = data1["post_img_url"].split(':;');
//      if (pri.length > 0) {
//        finalURL = "http://carem8bucket.s3-ap-southeast-2.amazonaws.com/${pri[0]}";
//      }
//    }

    List<Widget> widgets = [];

    /// if we assign widget.i for index then we can view from selected images but couldn't
    /// see the previous images
    ///
    /// in page view we couldn't show the added widget from it's index
    ///

    String finalUrl;
    for (int index = widget.i; index < photoFullData.length; index++) {
//      print(photoFullData[index]);
      widgets.add(
        Scaffold(
          appBar: new AppBar(
            title: photoFullData.length > 1
                ? new Text("${index + 1} / ${photoFullData.length}",
                    style: TextStyle(
                      color: Colors.white,
                    ))
                : new Text(''),
            elevation: 0.0,
            backgroundColor: Theme.Colors.app_dark[900],
            leading: new IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: new Icon(Icons.close, color: Theme.Colors.appdarkcolour),
            ),
            actions: <Widget>[
              new IconButton(
                onPressed: () async {
                  String url = photoFullData[index]['fileurl'];
                  if (url != null || url != "") {
                    var pri = url.split(':;');

                    if (pri.length > 0) {
                      finalUrl = url;
                    }
                  }
                  if (Platform.isAndroid) {
                    await SimplePermissions.requestPermission(
                            Permission.WriteExternalStorage)
                        .then((value) {
                      print("request :" + value.toString());
                    });
                    _photoDownloadLevel(platform, finalUrl);
                  } else {
                    final ios = const MethodChannel('kinder.flutter/s3');
                    _photoDownloadLevel(ios, finalUrl);
                  }
                },
                icon: new Icon(Icons.file_download,
                    color: Theme.Colors.appcolour),
              ),
            ],
          ),
          body: new Stack(
            children: <Widget>[
              new Container(
                child: ZoomableImage(
                  NetworkImage(photoFullData[index]['fileurl']),
                  placeholder:
                      const Center(child: const CircularProgressIndicator()),
                ),
                /*PhotoViewGallery(
                pageOptions: <PhotoViewGalleryPageOptions>[
                  PhotoViewGalleryPageOptions(
//                    imageProvider: NetworkImage(finalURLs[index]),
                    imageProvider: NetworkImage(
                        'http://carem8bucket.s3-ap-southeast-2.amazonaws.com/img/postuploads/production_1536758523-sun-hat-pink-apple-baby_large.jpg'),
                    minScale: PhotoViewComputedScale.contained * 1.0,
                    maxScale: PhotoViewComputedScale.covered * 1.1,
                    heroTag: "",
                  ),
                ],
                backgroundColor: Theme.Colors.app_dark[900],
              )*/
              ),
              new Align(
                alignment: Alignment.bottomRight,
                child: new Column(
                  // I need to add a column to set the MainAxisSize to min,
                  // otherwise the appbar takes all the screen and the image is no more clickable
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new AppBar(
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      leading: new Container(),
                      // Overrides the go back arrow icon button
                      actions: <Widget>[
                        Expanded(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              photoFullData[index]["caption"] != null &&
                                      photoFullData[index]["caption"] != ""
                                  ? new Expanded(
                                      child: new Text(
                                          photoFullData[index]['caption'],
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12.0)),
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }
    return new Scaffold(
      backgroundColor: Theme.Colors.app_dark[900],
      body: new SizedBox.expand(
        /*ZoomableImage(
          NetworkImage(
              'http://carem8bucket.s3-ap-southeast-2.amazonaws.com/img/postuploads/production_1536758523-sun-hat-pink-apple-baby_large.jpg'),
          placeholder: const Center(child: const CircularProgressIndicator()),
        ),*/
        child: new PageView(
          children: widgets,
        ),
      ),
    );
  }
}
