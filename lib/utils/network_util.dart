import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';

class NetworkUtil {
  List id, center, image = List();
  String jwt, clientId, userId;

  static NetworkUtil _instance = new NetworkUtil.internal();
  NetworkUtil.internal();
  factory NetworkUtil() => _instance;


  Future<dynamic> get(String url, {Map headers, body, encoding}) async {
    print('get url     - $url');
    print('get body    - $body');
    print('get headers - $headers');
    final response = await http.get(Uri.encodeFull(url), headers: headers);
    return response;
  }

  Future<dynamic> getJson(String url, {Map headers, body, encoding}) async {
    print(Uri.encodeFull(url));
    final response = await http.get(Uri.encodeFull(url), headers: {
      "CONTENT_TYPE": 'application/json',
      "ACCEPT": 'application/json'
    });
    return response;
  }
  
  Future<dynamic> delete(String url, {Map headers, body, encoding}) async {
//    print('delete - url$url');
//    print('delete - body$body');
//    print('delete - headers$headers');
    var dio = new Dio();
    dio.options.baseUrl =url;
    dio.options.headers = headers;

    Response response = await dio.delete(url,data: body,options: dio.options);
    print(response.data);
    print(response.statusCode);

    return response;
  }
  


  Future<dynamic> post(String url, {Map headers, body, encoding}) async {
    print(url);
    print(headers);
    print(body);
    print("jwt= $encoding");
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set("x-authorization", encoding);
    request.add(utf8.encode(json.encode(body)));
    HttpClientResponse response = await request.close();
    print(response.statusCode);

    ///    Single client
    try {
      if (response.statusCode == 200) {
        var reply = await response.transform(utf8.decoder).join();
//      List decodedList = json.decode(reply);
//      print(decodedList.length);
//      print("******$reply");
        httpClient.close();
        print("replyin util $reply ");
        return reply;
      }

      ///     Multiple client
      else if (response.statusCode == 202) {
        var reply = await response.transform(utf8.decoder).join();
        httpClient.close();
        return reply;
      } else if (response.statusCode == 500) {
        print("${response.statusCode} & jwt expire");
        return 1;
      } else {
        print("response.statusCode  ${response.statusCode}");
        print("error");
        return null;
      }
    } catch (e) {
      print("errorrr $e");
    }
  }

  Future<dynamic> web_post(String url, {Map headers, body, encoding}) async {
    print(url);
    print(headers);
    print(body);
//    print("jwt= $encoding");
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
//    request.headers.set("x-authorization", encoding);
    request.add(utf8.encode(json.encode(body)));
    HttpClientResponse response = await request.close();
    print(response.statusCode);

    ///    Single client
    try {
      if (response.statusCode == 200) {
        var reply = await response.transform(utf8.decoder).join();
//      List decodedList = json.decode(reply);
//      print(decodedList.length);
//      print("******$reply");
        httpClient.close();
        print("replyin util $reply ");
        return reply;
      }

      ///     Multiple client
      else if (response.statusCode == 202) {
        var reply = await response.transform(utf8.decoder).join();
        httpClient.close();
        return reply;
      } else if (response.statusCode == 500) {
        print("${response.statusCode} & jwt expire");
        return 1;
      } else {
        print("response.statusCode  ${response.statusCode}");
        print("error");
        return null;
      }
    } catch (e) {
      print("errorrr $e");
    }
  }


  Future<http.StreamedResponse> apiDeleteRequest(String url, {Map headers, body, encoding}) async {
    final client = http.Client();
    try {
      final response = await client.send(http.Request("DELETE", Uri.parse(url))
        ..headers["Content-Type"] = "application/json"
        ..headers["x-authorization"] = headers["x-authorization"]
        ..body = body);
      if (response.statusCode == 200) {
        print("delete");
        return response;
      } else if (response.statusCode == 500) {
        print("token");
        getRefreshToken(userId, clientId);
        apiDeleteRequest(url, headers: headers, body: body, encoding: encoding);
      }
    } catch (e, t) {
      print(t);
      return null;
    } finally {
      client.close();
    }
  }

  void getRefreshToken(String userId, String clientId) async {
    String _refreshTokenUrl =
        'https://apicare.carem8.com/v2.1.0/jwt/refresh-token?userid=$id&clientid=$clientId';

    NetworkUtil _netUtil = new NetworkUtil();

    await _netUtil.get(_refreshTokenUrl).then((response) {
      try {
        jwt = json.decode(response.body).toString();
      } catch (e) {
        print(e);
      }
    });
  }
}
