import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:carem8/Theme.dart' as kinderm8Theme;
import 'package:simple_permissions/simple_permissions.dart';

class WebUrlPreview extends StatefulWidget {
  WebUrlPreview(this.fileurlencode, {this.fileUrl});

  final fileurlencode;
  final String fileUrl;
  @override
  _WebUrlPreviewState createState() => _WebUrlPreviewState();
}

class _WebUrlPreviewState extends State<WebUrlPreview> {
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  Future<void> pdfDownload(
      MethodChannel channel, String finalUrl) async {
    try {
      final String result = await channel
          .invokeMethod('pdfDownload', {"url": finalUrl});
      print("$result");
    } on PlatformException catch (e) {
      print(e);
    }
  }

  @override
  void dispose() {
    flutterWebviewPlugin.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
            url: widget.fileurlencode,
          appBar: AppBar(
            automaticallyImplyLeading: false, // Don't show the leading button
            backgroundColor: kinderm8Theme.Colors.appsplashscreen,
            leading: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: Icon(Icons.arrow_back, color: Colors.white),
            ),
            actions:  <Widget>[
                IconButton(
                    icon: Icon(Icons.file_download, color: Colors.white),
                    onPressed: () async {
                      final platform = const MethodChannel('proitzen.kinderm8/s3_image');
                      if(Platform.isAndroid){
                        await SimplePermissions.requestPermission(
                            Permission.WriteExternalStorage)
                            .then((value) {
                          pdfDownload(platform, widget.fileUrl);
                        });
                      } else {
                        final ios = const MethodChannel('kinder.flutter/s3');
                        pdfDownload(ios, widget.fileUrl);
                      }
                    }
                ),
              ],
          ),
            withZoom: true,
            withLocalStorage: true,
    );
  }
}